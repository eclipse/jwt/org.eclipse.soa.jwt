/**
 * File:    ATLTransformation.java
 * Created: 04.03.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012 University of Augsburg
 * 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation for JWT 2 BPMN
 *    Florian Lautenbacher, University of Augsburg, Germany
 *    	- adapted for STP-IM
 *******************************************************************************/

package org.eclipse.jwt.transformation.stpim.internal;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jwt.transformations.api.TransformationException;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModel;
import org.eclipse.m2m.atl.drivers.emf4atl.AtlEMFModelHandler;
import org.eclipse.m2m.atl.engine.vm.AtlLauncher;
import org.eclipse.m2m.atl.engine.vm.AtlModelHandler;
import org.eclipse.m2m.atl.engine.vm.ModelLoader;

/**
 * Should implement a template method pattern...
 */
public class ATLTransformation
{

	private static ATLTransformation instance = null;

	private final static String resourcePath = "/org/eclipse/jwt/transformation/stpim/internal/resources/"; //$NON-NLS-1$

	private AtlEMFModelHandler modelHandler;
	private ModelLoader modelLoader;

	private InputStream JWT_ModelResource;
	private InputStream STPIM_ModelResource;
	private URL JWT2STPIM_TransfoResource;
	private ASMEMFModel jwtMetamodel;
	private ASMEMFModel stpimMetamodel;


	static public ATLTransformation getInstance()
	{
		if (instance == null)
			instance = new ATLTransformation();
		return instance;
	}


	private void createResources() throws TransformationException
	{
		if (STPIM_ModelResource == null)
		{
			modelHandler = (AtlEMFModelHandler) AtlEMFModelHandler
					.getDefault(AtlModelHandler.AMH_EMF);
			modelLoader = modelHandler.createModelLoader();

			String mm_path = "/org/eclipse/jwt/meta/ecore/JWTMetaModel.ecore";

			JWT_ModelResource = getClass().getResourceAsStream(mm_path);//$NON-NLS-1$
			STPIM_ModelResource = getClass().getResourceAsStream(
					resourcePath + "stpmodel.ecore");//$NON-NLS-1$

			JWT2STPIM_TransfoResource = ATLTransformation.class
					.getResource("resources/JWT2STPIM.asm");//$NON-NLS-1$
		}
	}


	private Map<String, Object> getMetamodels() throws TransformationException
	{
		Map<String, Object> models = new HashMap<String, Object>();

		if (jwtMetamodel == null)
		{
			try
			{
				createResources();
				jwtMetamodel = (ASMEMFModel) modelLoader.loadModel(
						"jwt", modelLoader.getMOF(), JWT_ModelResource);//$NON-NLS-1$
				stpimMetamodel = (ASMEMFModel) modelLoader.loadModel(
						"stpmodel", modelLoader.getMOF(), STPIM_ModelResource);//$NON-NLS-1$
			}
			catch (IOException ex)
			{
				throw new TransformationException(ex.getMessage(), ex);
			}
		}
		models.put("jwt", jwtMetamodel);//$NON-NLS-1$
		models.put("stpmodel", stpimMetamodel);//$NON-NLS-1$
		return models;
	}


	public void jwt2stpim(String inFilePath, String outFilePath)
	{
		try
		{
			Map<String, Object> models = getMetamodels();

			// get/create models
			ASMEMFModel jwtInputModel = (ASMEMFModel) modelLoader.loadModel(
					"IN", jwtMetamodel, new FileInputStream(inFilePath));//$NON-NLS-1$
			ASMEMFModel stpimOutputModel = (ASMEMFModel) modelLoader
					.newModel(
							"OUT", URI.createFileURI(outFilePath).toFileString(), stpimMetamodel);//$NON-NLS-1$
			// load models
			models.put("IN", jwtInputModel);//$NON-NLS-1$
			models.put("OUT", stpimOutputModel);//$NON-NLS-1$
			// launch
			AtlLauncher.getDefault().launch(this.JWT2STPIM_TransfoResource,
					Collections.EMPTY_MAP, models, Collections.EMPTY_MAP,
					Collections.EMPTY_LIST, Collections.EMPTY_MAP);

			modelLoader.save(stpimOutputModel, new File(outFilePath).toURI()
					.toURL().toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	public void stpim2jwt(String inFilePath, String outFilePath)
	{
		throw new UnsupportedOperationException("not yet implemented!"); //$NON-NLS-1$
	}

}
