/**
 * File:    Jwt2StpImService.java
 * Created: 04.05.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012 University of Augsburg
 * 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, University of Augsburg, Germany
 *      - creation and implementation
 *******************************************************************************/


package org.eclipse.jwt.transformation.stpim;


import java.io.IOException;

import org.eclipse.jwt.transformation.stpim.internal.ATLTransformation;
import org.eclipse.jwt.transformations.api.TransformationException;
import org.eclipse.jwt.transformations.api.TransformationService;

/**
 * This {@link TransformationService} transforms a JWT workflow file into
 * an STP intermediate model definition.
 * 
 * It uses an ATL transformation
 * @author Florian Lautenbacher
 * @since 0.6
 */
public class Jwt2StpImService extends TransformationService {

	@Override
	public void transform(String inFilePath, String outFilePath)
			throws IOException, TransformationException {
		ATLTransformation.getInstance().jwt2stpim(inFilePath, outFilePath);
	}

}
