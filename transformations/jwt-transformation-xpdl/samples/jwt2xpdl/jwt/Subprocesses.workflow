<?xml version="1.0" encoding="UTF-8"?>
<core:Model xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:core="org.eclipse.jwt/core" xmlns:processes="org.eclipse.jwt/processes" name="Subprocesses" author="Guillaume Decarnin" version="1.2.0" fileversion="0.4.0">
  <elements xsi:type="processes:Activity" name="SubprocessesDiagram">
    <nodes xsi:type="processes:Action" name="Init" in="//@elements.0/@edges.0" out="//@elements.0/@edges.2">
      <Location x="126" y="47"/>
      <Size width="124" height="31"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="Finalize" in="//@elements.0/@edges.4" out="//@elements.0/@edges.1">
      <Location x="466" y="47"/>
      <Size width="114" height="32"/>
    </nodes>
    <nodes xsi:type="processes:InitialNode" out="//@elements.0/@edges.0">
      <Location x="62" y="51"/>
    </nodes>
    <nodes xsi:type="processes:FinalNode" in="//@elements.0/@edges.1">
      <Location x="643" y="51"/>
    </nodes>
    <nodes xsi:type="processes:StructuredActivityNode" name="Subprocess1" in="//@elements.0/@edges.2" out="//@elements.0/@edges.3">
      <nodes xsi:type="processes:InitialNode" out="//@elements.0/@nodes.4/@edges.0">
        <Location x="90" y="73"/>
      </nodes>
      <nodes xsi:type="processes:FinalNode" in="//@elements.0/@nodes.4/@edges.1">
        <Location x="90" y="235"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Action1A" in="//@elements.0/@nodes.4/@edges.0" out="//@elements.0/@nodes.4/@edges.1">
        <Location x="42" y="159"/>
        <Size width="121" height="31"/>
      </nodes>
      <edges source="//@elements.0/@nodes.4/@nodes.0" target="//@elements.0/@nodes.4/@nodes.2"/>
      <edges source="//@elements.0/@nodes.4/@nodes.2" target="//@elements.0/@nodes.4/@nodes.1"/>
      <Location x="62" y="140"/>
      <Size width="252" height="344"/>
    </nodes>
    <nodes xsi:type="processes:StructuredActivityNode" name="Subprocess2" in="//@elements.0/@edges.3" out="//@elements.0/@edges.4">
      <nodes xsi:type="processes:InitialNode" out="//@elements.0/@nodes.5/@edges.0">
        <Location x="133" y="78"/>
      </nodes>
      <nodes xsi:type="processes:FinalNode" in="//@elements.0/@nodes.5/@edges.1">
        <Location x="132" y="214"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Action2A" in="//@elements.0/@nodes.5/@edges.0" out="//@elements.0/@nodes.5/@edges.1">
        <Location x="77" y="140"/>
        <Size width="132" height="28"/>
      </nodes>
      <edges source="//@elements.0/@nodes.5/@nodes.0" target="//@elements.0/@nodes.5/@nodes.2"/>
      <edges source="//@elements.0/@nodes.5/@nodes.2" target="//@elements.0/@nodes.5/@nodes.1"/>
      <Location x="378" y="140"/>
      <Size width="289" height="344"/>
    </nodes>
    <edges source="//@elements.0/@nodes.2" target="//@elements.0/@nodes.0"/>
    <edges source="//@elements.0/@nodes.1" target="//@elements.0/@nodes.3"/>
    <edges source="//@elements.0/@nodes.0" target="//@elements.0/@nodes.4"/>
    <edges source="//@elements.0/@nodes.4" target="//@elements.0/@nodes.5"/>
    <edges source="//@elements.0/@nodes.5" target="//@elements.0/@nodes.1"/>
  </elements>
</core:Model>
