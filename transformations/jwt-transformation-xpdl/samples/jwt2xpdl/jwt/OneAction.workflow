<?xml version="1.0" encoding="UTF-8"?>
<core:Model xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:core="org.eclipse.jwt/core" xmlns:organisations="org.eclipse.jwt/organisations" xmlns:processes="org.eclipse.jwt/processes" name="OneAction" author="Guillaume Decarnin" version="1.2.0" fileversion="0.4.0">
  <elements xsi:type="processes:Activity" name="OneActionDiagram">
    <ownedComment text="This is a basic activity"/>
    <nodes xsi:type="processes:InitialNode" out="//@elements.0/@edges.0">
      <Location x="364" y="95"/>
    </nodes>
    <nodes xsi:type="processes:FinalNode" in="//@elements.0/@edges.1">
      <Location x="364" y="308"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="Action1" in="//@elements.0/@edges.0" out="//@elements.0/@edges.1" performedBy="//@elements.1" referenceEdges="//@elements.0/@referenceEdges.0">
      <Location x="298" y="196"/>
      <Size width="155" height="29"/>
    </nodes>
    <edges source="//@elements.0/@nodes.0" target="//@elements.0/@nodes.2"/>
    <edges source="//@elements.0/@nodes.2" target="//@elements.0/@nodes.1"/>
    <references reference="//@elements.1" referenceEdges="//@elements.0/@referenceEdges.0">
      <Location x="168" y="191"/>
    </references>
    <referenceEdges reference="//@elements.0/@references.0" action="//@elements.0/@nodes.2"/>
  </elements>
  <elements xsi:type="organisations:Role" name="bsoa" icon=""/>
</core:Model>
