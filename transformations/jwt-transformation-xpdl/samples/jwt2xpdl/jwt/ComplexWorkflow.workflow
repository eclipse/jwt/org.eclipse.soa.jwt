<?xml version="1.0" encoding="UTF-8"?>
<core:Model xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:application="org.eclipse.jwt/application" xmlns:core="org.eclipse.jwt/core" xmlns:data="org.eclipse.jwt/data" xmlns:organisations="org.eclipse.jwt/organisations" xmlns:processes="org.eclipse.jwt/processes" name="ComplexWorkflow" author="Guillaume Decarnin" version="1.0.2" description="This is the documentation.&#xA;&#xA;Paragraph 2." fileversion="0.4.0">
  <elements xsi:type="processes:Activity" name="ComplexWorkflowDiagram">
    <nodes xsi:type="processes:Action" name="Action1" in="//@elements.0/@edges.1" out="//@elements.0/@edges.0" performedBy="//@elements.1" outputs="//@elements.4" referenceEdges="//@elements.0/@referenceEdges.0 //@elements.0/@referenceEdges.6">
      <Location x="223" y="91"/>
      <Size width="178" height="31"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="Action2" icon="/home/gdecarnin/images/icon.png" in="//@elements.0/@edges.8" out="//@elements.0/@edges.2" executedBy="//@elements.5" inputs="//@elements.4 //@elements.7" referenceEdges="//@elements.0/@referenceEdges.4 //@elements.0/@referenceEdges.5 //@elements.0/@referenceEdges.7">
      <Location x="223" y="201"/>
      <Size width="178" height="31"/>
      <mappings name="datatest"/>
      <mappings name="data2" icon="/home/gdecarnin/images/icon.png"/>
    </nodes>
    <nodes xsi:type="processes:InitialNode" out="//@elements.0/@edges.1">
      <Location x="302" y="19"/>
    </nodes>
    <nodes xsi:type="processes:FinalNode" in="//@elements.0/@edges.7">
      <Location x="300" y="492"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="Action3A" in="//@elements.0/@edges.3" out="//@elements.0/@edges.6" performedBy="//@elements.2" executedBy="//@elements.9" referenceEdges="//@elements.0/@referenceEdges.2 //@elements.0/@referenceEdges.8">
      <Location x="90" y="378"/>
      <Size width="136" height="31"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="Action3B" in="//@elements.0/@edges.4" out="//@elements.0/@edges.5" performedBy="//@elements.2" referenceEdges="//@elements.0/@referenceEdges.3">
      <Location x="400" y="378"/>
      <Size width="136" height="31"/>
    </nodes>
    <nodes xsi:type="processes:DecisionNode" name="Split" in="//@elements.0/@edges.2" out="//@elements.0/@edges.3 //@elements.0/@edges.4">
      <ownedComment/>
      <Location x="301" y="294"/>
      <Size width="3" height="8"/>
    </nodes>
    <nodes xsi:type="processes:MergeNode" name="Merge" in="//@elements.0/@edges.5 //@elements.0/@edges.6" out="//@elements.0/@edges.7">
      <Location x="300" y="448"/>
    </nodes>
    <nodes xsi:type="processes:StructuredActivityNode" in="//@elements.0/@edges.0" out="//@elements.0/@edges.8">
      <nodes xsi:type="processes:Action" name="SubprocessAction1" in="//@elements.0/@nodes.8/@edges.0" out="//@elements.0/@nodes.8/@edges.1">
        <Location x="19" y="81"/>
        <Size width="166" height="31"/>
      </nodes>
      <nodes xsi:type="processes:InitialNode" out="//@elements.0/@nodes.8/@edges.0">
        <Location x="90" y="16"/>
      </nodes>
      <nodes xsi:type="processes:FinalNode" in="//@elements.0/@nodes.8/@edges.2">
        <Location x="91" y="221"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="SubprocessAction2" in="//@elements.0/@nodes.8/@edges.1" out="//@elements.0/@nodes.8/@edges.2">
        <Location x="24" y="154"/>
        <Size width="159" height="31"/>
      </nodes>
      <edges source="//@elements.0/@nodes.8/@nodes.1" target="//@elements.0/@nodes.8/@nodes.0"/>
      <edges source="//@elements.0/@nodes.8/@nodes.0" target="//@elements.0/@nodes.8/@nodes.3"/>
      <edges source="//@elements.0/@nodes.8/@nodes.3" target="//@elements.0/@nodes.8/@nodes.2"/>
      <Location x="500" y="19"/>
      <Size width="243" height="290"/>
    </nodes>
    <edges source="//@elements.0/@nodes.0" target="//@elements.0/@nodes.8"/>
    <edges source="//@elements.0/@nodes.2" target="//@elements.0/@nodes.0"/>
    <edges source="//@elements.0/@nodes.1" target="//@elements.0/@nodes.6"/>
    <edges source="//@elements.0/@nodes.6" target="//@elements.0/@nodes.4">
      <guard shortdescription="datatest == false">
        <detailedSpecification attribute="datatest" value="false"/>
      </guard>
    </edges>
    <edges source="//@elements.0/@nodes.6" target="//@elements.0/@nodes.5">
      <guard shortdescription="datatest == true">
        <detailedSpecification attribute="datatest" value="true"/>
      </guard>
    </edges>
    <edges source="//@elements.0/@nodes.5" target="//@elements.0/@nodes.7"/>
    <edges source="//@elements.0/@nodes.4" target="//@elements.0/@nodes.7"/>
    <edges source="//@elements.0/@nodes.7" target="//@elements.0/@nodes.3"/>
    <edges source="//@elements.0/@nodes.8" target="//@elements.0/@nodes.1"/>
    <references reference="//@elements.9" referenceEdges="//@elements.0/@referenceEdges.8">
      <Location x="118" y="291"/>
    </references>
    <references reference="//@elements.2" referenceEdges="//@elements.0/@referenceEdges.2 //@elements.0/@referenceEdges.3">
      <Location x="14" y="483"/>
    </references>
    <references reference="//@elements.1" referenceEdges="//@elements.0/@referenceEdges.0">
      <Location x="14" y="86"/>
    </references>
    <references reference="//@elements.5" referenceEdges="//@elements.0/@referenceEdges.4">
      <Location x="293" y="140"/>
    </references>
    <references reference="//@elements.4" referenceEdges="//@elements.0/@referenceEdges.5 //@elements.0/@referenceEdges.6">
      <Location x="72" y="140"/>
    </references>
    <references reference="//@elements.7" referenceEdges="//@elements.0/@referenceEdges.7">
      <Location x="79" y="196"/>
    </references>
    <referenceEdges reference="//@elements.0/@references.2" action="//@elements.0/@nodes.0"/>
    <referenceEdges/>
    <referenceEdges reference="//@elements.0/@references.1" action="//@elements.0/@nodes.4"/>
    <referenceEdges reference="//@elements.0/@references.1" action="//@elements.0/@nodes.5"/>
    <referenceEdges reference="//@elements.0/@references.3" action="//@elements.0/@nodes.1"/>
    <referenceEdges reference="//@elements.0/@references.4" action="//@elements.0/@nodes.1" direction="in"/>
    <referenceEdges reference="//@elements.0/@references.4" action="//@elements.0/@nodes.0" direction="out"/>
    <referenceEdges reference="//@elements.0/@references.5" action="//@elements.0/@nodes.1" direction="in"/>
    <referenceEdges reference="//@elements.0/@references.0" action="//@elements.0/@nodes.4"/>
  </elements>
  <elements xsi:type="organisations:Role" name="manager"/>
  <elements xsi:type="organisations:Role" name="operator"/>
  <elements xsi:type="data:DataType" name="typestring" icon=""/>
  <elements xsi:type="data:Data" name="datatest" icon="" value="" dataType="//@elements.3"/>
  <elements xsi:type="application:Application" name="Appli1" icon="" jarArchive="/home/gdecarnin/myapp.jar" javaClass="hero.hook.SuperHook" method="main"/>
  <elements xsi:type="data:DataType" name="AddressType" icon=""/>
  <elements xsi:type="data:Data" name="address" icon="" value="127 rue BM" dataType="//@elements.6"/>
  <elements xsi:type="processes:Activity" name="ProcessRegister" icon="">
    <nodes xsi:type="processes:ActivityLinkNode" linksto="//@elements.0">
      <Location x="287" y="175"/>
      <Size width="271" height="122"/>
    </nodes>
  </elements>
  <elements xsi:type="application:WebServiceApplication" name="WebService1" Interface="interface-ws" Operation="operation-ws"/>
</core:Model>
