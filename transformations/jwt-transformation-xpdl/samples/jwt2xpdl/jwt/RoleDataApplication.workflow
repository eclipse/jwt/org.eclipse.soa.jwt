<?xml version="1.0" encoding="UTF-8"?>
<core:Model xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:application="org.eclipse.jwt/application" xmlns:core="org.eclipse.jwt/core" xmlns:data="org.eclipse.jwt/data" xmlns:organisations="org.eclipse.jwt/organisations" xmlns:processes="org.eclipse.jwt/processes" name="RoleDataApplication" author="Guillaume Decarnin" version="1.2.0" fileversion="0.4.0">
  <subpackages name="Applications">
    <ownedComment text="The standard package for applications"/>
    <elements xsi:type="application:Application" name="appli1" icon="" jarArchive="" javaClass="hero.hook.Hook1" method=""/>
  </subpackages>
  <subpackages name="Roles">
    <ownedComment text="The standard package for roles"/>
    <elements xsi:type="organisations:Role" name="manager" icon=""/>
  </subpackages>
  <subpackages name="Data">
    <ownedComment text="The standard package for data"/>
    <subpackages name="Datatypes">
      <ownedComment text="The standard package for datatypes"/>
      <elements xsi:type="data:DataType" name="URL"/>
      <elements xsi:type="data:DataType" name="dioParameter"/>
      <elements xsi:type="data:DataType" name="qualifier"/>
      <elements xsi:type="data:DataType" name="searchquery"/>
      <elements xsi:type="data:DataType" name="filename"/>
    </subpackages>
    <elements xsi:type="data:Data" name="data1" icon="" value="" dataType="//@subpackages.2/@subpackages.0/@elements.1"/>
  </subpackages>
  <elements xsi:type="processes:Activity" name="RoleDataApplicationDiagram">
    <ownedComment text="This is a basic activity"/>
    <nodes xsi:type="processes:InitialNode" out="//@elements.0/@edges.0">
      <Location x="340" y="140"/>
    </nodes>
    <nodes xsi:type="processes:FinalNode" in="//@elements.0/@edges.2">
      <Location x="340" y="392"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="Action1" in="//@elements.0/@edges.0" out="//@elements.0/@edges.1" performedBy="//@subpackages.1/@elements.0" executedBy="//@subpackages.0/@elements.0" inputs="//@subpackages.2/@elements.0" referenceEdges="//@elements.0/@referenceEdges.0 //@elements.0/@referenceEdges.3 //@elements.0/@referenceEdges.6">
      <Location x="243" y="224"/>
      <Size width="218" height="31"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="Action2" in="//@elements.0/@edges.1" out="//@elements.0/@edges.2" performedBy="//@elements.1" executedBy="//@elements.3" inputs="//@subpackages.2/@elements.0" outputs="//@elements.2" referenceEdges="//@elements.0/@referenceEdges.1 //@elements.0/@referenceEdges.4 //@elements.0/@referenceEdges.5 //@elements.0/@referenceEdges.7">
      <Location x="243" y="308"/>
      <Size width="218" height="31"/>
    </nodes>
    <edges source="//@elements.0/@nodes.0" target="//@elements.0/@nodes.2"/>
    <edges source="//@elements.0/@nodes.2" target="//@elements.0/@nodes.3"/>
    <edges source="//@elements.0/@nodes.3" target="//@elements.0/@nodes.1"/>
    <references reference="//@elements.3" referenceEdges="//@elements.0/@referenceEdges.7">
      <Location x="546" y="383"/>
    </references>
    <references reference="//@subpackages.0/@elements.0" referenceEdges="//@elements.0/@referenceEdges.6">
      <Location x="546" y="131"/>
    </references>
    <references reference="//@elements.2" referenceEdges="//@elements.0/@referenceEdges.5">
      <Location x="546" y="303"/>
    </references>
    <references reference="//@subpackages.2/@elements.0" referenceEdges="//@elements.0/@referenceEdges.3 //@elements.0/@referenceEdges.4">
      <Location x="546" y="219"/>
    </references>
    <references reference="//@elements.1" referenceEdges="//@elements.0/@referenceEdges.1">
      <Location x="84" y="303"/>
    </references>
    <references reference="//@subpackages.1/@elements.0" referenceEdges="//@elements.0/@referenceEdges.0">
      <Location x="84" y="219"/>
    </references>
    <referenceEdges reference="//@elements.0/@references.5" action="//@elements.0/@nodes.2"/>
    <referenceEdges reference="//@elements.0/@references.4" action="//@elements.0/@nodes.3"/>
    <referenceEdges/>
    <referenceEdges reference="//@elements.0/@references.3" action="//@elements.0/@nodes.2" direction="in"/>
    <referenceEdges reference="//@elements.0/@references.3" action="//@elements.0/@nodes.3" direction="in"/>
    <referenceEdges reference="//@elements.0/@references.2" action="//@elements.0/@nodes.3" direction="out"/>
    <referenceEdges reference="//@elements.0/@references.1" action="//@elements.0/@nodes.2"/>
    <referenceEdges reference="//@elements.0/@references.0" action="//@elements.0/@nodes.3"/>
  </elements>
  <elements xsi:type="organisations:Role" name="operator" icon=""/>
  <elements xsi:type="data:Data" name="data2" icon="" value="" dataType="//@subpackages.2/@subpackages.0/@elements.1"/>
  <elements xsi:type="application:Application" name="appli2" icon="" jarArchive="" javaClass="hero.hook.Hook2" method=""/>
</core:Model>
