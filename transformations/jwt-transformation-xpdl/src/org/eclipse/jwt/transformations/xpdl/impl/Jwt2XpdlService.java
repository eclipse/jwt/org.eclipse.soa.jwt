/**
 * File:    Jwt2XpdlService.java
 * Created: 21.03.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2010  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Guillaume Decarnin, Open Wide, Lyon, France
 *      - Creation and implementation
 *    Mickael Istria, Open Wide, Lyon France
 *      - Refactoring to use XSLT tools plugin
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - #327474 - made all aspect properties available within XSLT
 *******************************************************************************/
package org.eclipse.jwt.transformations.xpdl.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import org.eclipse.jwt.transformations.api.TransformationException;
import org.eclipse.jwt.transformations.api.TransformationService;
import org.eclipse.jwt.transformations.properties.ExtractPropertiesService;
import org.eclipse.jwt.transformations.xslt.tools.CustomXslFunctions;
import org.eclipse.jwt.transformations.xslt.tools.XSLTProcessor;

/**
 * This implementation of {@link TransformationService} is actually
 * a transformation from JWT workflow file to XPDL file.
 * 
 * It uses XSL Transformation.
 * 
 * @author Guillaume Decarnin - Open Wide
 * @since 0.6
 */
public class Jwt2XpdlService extends TransformationService {
	
	public Jwt2XpdlService() {}

	public static final String XSL_XPDL_RESOURCE_PART1 = "/org/eclipse/jwt/transformations/xpdl/impl/jwt2xpdl-part1.xsl"; //$NON-NLS-1$
	public static final String XSL_XPDL_RESOURCE_PART2 = "/org/eclipse/jwt/transformations/xpdl/impl/jwt2xpdl-part2.xsl"; //$NON-NLS-1$

	/**
	 * TODO activate commented code to also export resources and aspect properties
	 * @param inFilePath
	 * @param outFilePath
	 * @throws IOException
	 * @throws TransformationException
	 */
	public void transform(String inFilePath, String outFilePath) throws IOException, TransformationException {
		// exporting properties and resources :
		String propertiesFile = File.createTempFile("jwt", "properties").getAbsolutePath(); //$NON-NLS-1$ //$NON-NLS-2$
		ExtractPropertiesService propertiesExtractor = new ExtractPropertiesService(); 
		propertiesExtractor.transform(inFilePath, propertiesFile);
		Map<String, String> resources = propertiesExtractor.getAttachedResources();
		
		// preparing them to make them available from within XSL transformations :
		//Model jwtModel = getJWTModel(inFilePath);
		Properties properties = new Properties();
		properties.load(new BufferedInputStream(new FileInputStream(propertiesFile)));
		
		// 2 stage XSL transformation with available, threaded properties and resources :
		CustomXslFunctions.setContext(properties, resources);
		InputStream xslResource1 = getClass().getResourceAsStream(XSL_XPDL_RESOURCE_PART1);
		InputStream xslResource2 = getClass().getResourceAsStream(XSL_XPDL_RESOURCE_PART2);
		new XSLTProcessor().processXSLT(inFilePath, outFilePath, xslResource1, xslResource2);
		CustomXslFunctions.setContext(null, null);
	}

}
