<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
 * File:    jwt2xpdl-part1.xsl
 * Created: 21.03.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Guillaume Decarnin, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
-->
<xsl:stylesheet version="1.0"
		xmlns:xmi="http://www.omg.org/XMI"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns:core="org.eclipse.jwt/core"
		xmlns:data="org.eclipse.jwt/data"
		xmlns:organisations="org.eclipse.jwt/organisations"
		xmlns:processes="org.eclipse.jwt/processes"
		xmlns:jwt2xpdl="http://xml.apache.org/xalan/java/org.eclipse.jwt.transformations.xslt.tools.CustomXslFunctions">
    <xsl:output method="XML" encoding="UTF-8" indent="yes" version="1.0" omit-xml-declaration="no"/>
	
	<xsl:template match="/">
		<!-- send all names to the Java class for correcting missing or duplicated @name -->
		<xsl:for-each select="//nodes[(@xsi:type = 'processes:Action' or @xsi:type = 'processes:StructuredActivityNode' or @xsi:type = 'processes:FinalNode') and @name != '']">
			<xsl:value-of select="jwt2xpdl:declareName(@name)"/><!-- this value-of writes nothing, just a Java call -->
		</xsl:for-each>
		
		<xsl:apply-templates/>
	</xsl:template>
    
    <!--
    	Warning: if many templates are concerned about the same element,
    	the last matching template is applied.
    	So this generic "*" template must be placed first.
    -->
    <xsl:template match="*">
		<xsl:element name="{name(.)}">
			<xsl:call-template name="copy-attributes"/>
			<xsl:apply-templates/>
		</xsl:element>
    </xsl:template>
    
    
    <xsl:template match="nodes[@xsi:type = 'processes:Action' and (not(@name) or @name = '')]">
    	<xsl:call-template name="copy-and-add-name">
    		<xsl:with-param name="name"><xsl:value-of select="jwt2xpdl:generateActivityName()"/></xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    
    
    <xsl:template match="nodes[(@xsi:type = 'processes:ForkNode' or @xsi:type = 'processes:DecisionNode' or @xsi:type = 'processes:MergeNode' or @xsi:type = 'processes:JoinNode') and (not(@name) or @name = '')]">
    	<xsl:call-template name="copy-and-add-name">
    		<xsl:with-param name="name"><xsl:value-of select="jwt2xpdl:generateRouteName()"/></xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    
    
    <xsl:template match="nodes[@xsi:type = 'processes:StructuredActivityNode' and (not(@name) or @name = '')]">
    	<xsl:call-template name="copy-and-add-name">
    		<xsl:with-param name="name"><xsl:value-of select="jwt2xpdl:generateBlockName()"/></xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    
    
    <xsl:template match="nodes[@xsi:type = 'processes:InitialNode']">
    	<xsl:call-template name="copy-and-add-name">
    		<xsl:with-param name="name"/><!-- empty name because Bonita doesn't need initial node nor transition with initial node -->
    	</xsl:call-template>
    </xsl:template>
    
    
    <xsl:template match="nodes[@xsi:type = 'processes:FinalNode']">
    	<xsl:call-template name="copy-and-add-name">
    		<xsl:with-param name="name"><xsl:value-of select="jwt2xpdl:generateFinalName()"/></xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
	
	
    <xsl:template name="copy-attributes">
    	<xsl:for-each select="@*">
			<xsl:copy-of select="."/>
		</xsl:for-each>
    </xsl:template>
	
	
	<xsl:template name="copy-and-add-name">
		<xsl:param name="name"/>
		
		<xsl:element name="{name(.)}">
			<xsl:call-template name="copy-attributes"/>
			
			<!-- add generated @name (throws warning but it's ok) -->
			<xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
			
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
    
</xsl:stylesheet>
