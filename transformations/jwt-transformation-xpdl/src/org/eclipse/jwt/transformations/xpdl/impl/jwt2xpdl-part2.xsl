<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
 * File:    jwt2xpdl-part2.xsl
 * Created: 21.03.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Guillaume Decarnin, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
-->
<xsl:stylesheet version="1.0"
		xmlns="http://www.wfmc.org/2002/XPDL1.0" 
		xmlns:xmi="http://www.omg.org/XMI"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns:core="org.eclipse.jwt/core"
		xmlns:data="org.eclipse.jwt/data"
		xmlns:organisations="org.eclipse.jwt/organisations"
		xmlns:processes="org.eclipse.jwt/processes"
		xmlns:jwt2xpdl="http://xml.apache.org/xalan/java/org.eclipse.jwt.transformations.xslt.tools.CustomXslFunctions">
    <xsl:output method="XML" encoding="UTF-8" indent="yes" version="1.0" omit-xml-declaration="no"/>
	<!-- 
		XPDL sample:
		http://www.wfmc.org/standards/docs/xpdl_sample/sample%20workflow%20process.xpdl
		XSD:
		http://wfmc.org/standards/docs/TC-1025_schema_10_xpdl.xsd
		
		TODO eventualy add a @mode to the templates for specific XPDL implementations
			 (ex.: mode="bonita" or mode="no-extended")
		
		TODO no BonitaEnd in ActivitySets
		
		TODO all data fields are declared as process properties (no activity properties)
		
		TODO cycle must be iteration for Bonita, not transition
		
		TODO JWT Event
	-->
    <xsl:template match="/core:Model">
    	<!-- $packageName : name of the first process -->
		<xsl:variable name="packageName" select="@name"/>

		<Package
				xmlns="http://www.wfmc.org/2002/XPDL1.0"
				xmlns:xpdl="http://www.wfmc.org/2002/XPDL1.0"
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
				xsi:schemaLocation="http://www.wfmc.org/2002/XPDL1.0 http://wfmc.org/standards/docs/TC-1025_schema_10_xpdl.xsd"
				Id="{$packageName}"
				Name="{$packageName}">
			<PackageHeader>
				<XPDLVersion>1.0</XPDLVersion>
				<Vendor>JWT</Vendor>
				<Created><xsl:value-of select="jwt2xpdl:getDate()"/></Created>
				<xsl:if test="@description != ''">
					<Description><xsl:value-of select="@description"/></Description>
				</xsl:if>
				<!-- TODO
	            <xsd:element ref="xpdl:Documentation" minOccurs="0"/>
	            <xsd:element ref="xpdl:PriorityUnit" minOccurs="0"/>
	            <xsd:element ref="xpdl:CostUnit" minOccurs="0"/>
				-->
			</PackageHeader>
			
			<RedefinableHeader>
				<!-- TODO @PublicationStatus : UNDER_REVISION | RELEASED | UNDER_TEST -->
				<Author><xsl:value-of select="@author"/></Author>
				<Version><xsl:value-of select="@version"/></Version>
				<!-- TODO
				<xsd:element ref="xpdl:Codepage" minOccurs="0"/>
				<xsd:element ref="xpdl:Countrykey" minOccurs="0"/>
				<xsd:element ref="xpdl:Responsibles" minOccurs="0"/>
				-->
			</RedefinableHeader>
			
			<ConformanceClass GraphConformance="NON_BLOCKED"/>
			<!-- TODO @GraphConformance : FULL_BLOCKED | LOOP_BLOCKED | NON_BLOCKED -->
			
			<!-- TODO Script
		      <xsd:complexType>
		         <xsd:attribute name="Type" type="xsd:string" use="required"/>
		         <xsd:attribute name="Version" type="xsd:string" use="optional"/>
		         <xsd:attribute name="Grammar" type="xsd:anyURI" use="optional"/>
		      </xsd:complexType>
			-->
			
			<!-- TODO ExternalPackages
				<ExternalPackage>
			      <xsd:complexType>
			         <xsd:sequence>
			            <xsd:element ref="xpdl:ExtendedAttributes" minOccurs="0"/>
			         </xsd:sequence>
			         <xsd:attribute name="href" type="xsd:string"/>
			      </xsd:complexType>
				</ExternalPackage>
		    -->
			
			<!-- TODO TypeDeclarations
				<TypeDeclaration>
			      <xsd:complexType>
			         <xsd:sequence>
			            <xsd:group ref="xpdl:DataTypes"/>
			            <xsd:element ref="xpdl:Description" minOccurs="0"/>
			            <xsd:element ref="xpdl:ExtendedAttributes" minOccurs="0"/>
			         </xsd:sequence>
			         <xsd:attribute name="Id" type="xsd:ID" use="required"/>
			         <xsd:attribute name="Name" type="xsd:string"/>
			      </xsd:complexType>
				</TypeDeclaration>
			-->
			
			<!-- TODO Applications are in WorkflowProcess
			<xsl:if test="elements[@xsi:type = 'application:Application']">
				<Applications>
					<xsl:apply-templates select="elements[@xsi:type = 'application:Application']"/>
				</Applications>
			</xsl:if>
			-->
			
			<!-- Package scope is not used by Bonita (DataFieds are in WorkflowProcess).
			<xsl:if test="elements[@xsi:type = 'data:Data']">
				<DataFields>
					<xsl:apply-templates select="elements[@xsi:type = 'data:Data']"/>
				</DataFields>
			</xsl:if> -->
			
			<WorkflowProcesses>
				<xsl:apply-templates select="elements[@xsi:type = 'processes:Activity']"/>
				<!-- same out for elements and subpackages/elements? -->
				<!-- TODO test subpackages -->
				<xsl:apply-templates select="subpackages/elements[@xsi:type = 'processes:Activity']"/>
			</WorkflowProcesses>
			
			<ExtendedAttributes>
				<ExtendedAttribute Name="MadeBy" Value="JWT2XPDL"/>
				<ExtendedAttribute Name="View" Value="Activity"/><!-- View value : "Activity" or "Participant" -->
			</ExtendedAttributes>
		</Package>
    </xsl:template>


    <xsl:template match="elements[@xsi:type = 'organisations:Role']">
	    <Participant Id="{@name}" Name="{@name}">
			<ParticipantType Type="HUMAN"/>
			<!-- TODO Type
                  <xsd:enumeration value="RESOURCE_SET"/>
                  <xsd:enumeration value="RESOURCE"/>
                  <xsd:enumeration value="ROLE"/>
                  <xsd:enumeration value="ORGANIZATIONAL_UNIT"/>
                  <xsd:enumeration value="HUMAN"/>
                  <xsd:enumeration value="SYSTEM"/>
			-->
			<!-- TODO
	            <xsd:element ref="xpdl:Description" minOccurs="0"/>
	            <xsd:element ref="xpdl:ExternalReference" minOccurs="0"/>
			-->
			<ExtendedAttributes>
				<ExtendedAttribute Name="NewParticipant" Value="true"/><!-- TODO useless? -->
				<xsl:if test="@icon != ''">
					<ExtendedAttribute Name="icon" Value="{@icon}"/>
				</xsl:if>
			</ExtendedAttributes>
		</Participant>
    </xsl:template>


	<xsl:template match="elements[@xsi:type = 'data:Data']">
		<DataField Id="{@name}" Name="{@name}">
			<!-- TODO @IsArray="TRUE | FALSE" -->
			<DataType>
				<!-- TODO use @dataType="//@elements.3"
			      <xsd:choice>
			         <xsd:element ref="xpdl:BasicType"/>
			         <xsd:element ref="xpdl:DeclaredType"/>
			         <xsd:element ref="xpdl:SchemaType"/>
			         <xsd:element ref="xpdl:ExternalReference"/>
			         <xsd:element ref="xpdl:RecordType"/>
			         <xsd:element ref="xpdl:UnionType"/>
			         <xsd:element ref="xpdl:EnumerationType"/>
			         <xsd:element ref="xpdl:ArrayType"/>
			         <xsd:element ref="xpdl:ListType"/>
			      </xsd:choice>
				-->
				<BasicType Type="STRING"/>
				<!-- TODO Type
					<xsd:enumeration value="STRING"/>
					<xsd:enumeration value="FLOAT"/>
					<xsd:enumeration value="INTEGER"/>
					<xsd:enumeration value="REFERENCE"/>
					<xsd:enumeration value="DATETIME"/>
					<xsd:enumeration value="BOOLEAN"/>
					<xsd:enumeration value="PERFORMER"/>
				-->
			</DataType>
			<InitialValue><xsl:value-of select="@value"/></InitialValue>
   			<!--
   			TODO
            <xsd:element ref="xpdl:Length" minOccurs="0"/>
            <xsd:element ref="xpdl:Description" minOccurs="0"/>
			-->
			<ExtendedAttributes>
				<xsl:if test="@icon != ''">
					<ExtendedAttribute Name="icon" Value="{@icon}"/>
				</xsl:if>
       			<!-- TODO problem with activity properties: names must be unique in XPDL
			    		  solution: all properties are process properties
           		<ExtendedAttribute Name="PropertyActivity" />
           		-->
			</ExtendedAttributes>
		</DataField>
    </xsl:template>


	<xsl:template match="elements[@xsi:type = 'application:Application']">
		<Application Id="{@name}" Name="{@name}">
			<!-- TODO
			<xsd:element ref="xpdl:Description" minOccurs="0"/>
			<xsd:choice>
				<xsd:element ref="xpdl:FormalParameters"/>
				<xsd:element ref="xpdl:ExternalReference" minOccurs="0"/>
			</xsd:choice>
			-->
			<ExtendedAttributes>
				<!-- TODO better way of using a Java application? -->
				<ExtendedAttribute Name="jarArchive" Value="{@jarArchive}"/>
				<ExtendedAttribute Name="javaClass" Value="{@javaClass}"/>
				<ExtendedAttribute Name="method" Value="{@method}"/>
				<xsl:if test="@icon != ''">
					<ExtendedAttribute Name="icon" Value="{@icon}"/>
				</xsl:if>
			</ExtendedAttributes>
		</Application>
    </xsl:template>


    <xsl:template match="elements[@xsi:type = 'processes:Activity']">
    	<xsl:value-of select="jwt2xpdl:declareName(/core:Model/@fileversion)"/>
		<WorkflowProcess Id="{@name}" Name="{@name}" AccessLevel="PUBLIC">
	    	<!-- TODO AccessLevel : PUBLIC | PRIVATE -->
			<ProcessHeader/>
			
			<RedefinableHeader>
				<Version><xsl:value-of select="/core:Model/@version"/></Version>
			</RedefinableHeader>
			
			<!-- TODO FormalParameters -->
			
			<DataFields>
				<xsl:apply-templates select="//elements[@xsi:type = 'data:Data']"/>
			</DataFields>
			
			<Participants>
				<xsl:apply-templates select="//elements[@xsi:type = 'organisations:Role']"/>
			</Participants>
			
			<xsl:if test="//elements[@xsi:type = 'application:Application']">
				<Applications>
					<xsl:apply-templates select="//elements[@xsi:type = 'application:Application']"/>
				</Applications>
			</xsl:if>
			
			<xsl:if test="//nodes[@xsi:type = 'processes:StructuredActivityNode']">
				<ActivitySets>
					<xsl:for-each select="//nodes[@xsi:type = 'processes:StructuredActivityNode']">
						<xsl:call-template name="activity-set"/>
					</xsl:for-each>
				</ActivitySets>
			</xsl:if>
			
			<Activities>
				<xsl:apply-templates select="nodes"/>
			</Activities>
			
			<Transitions>
				<xsl:apply-templates select="edges"/>
			</Transitions>
			
			<!-- TODO ExtendedAttributes -->
		</WorkflowProcess>
    </xsl:template>


    <xsl:template match="nodes[@xsi:type = 'processes:InitialNode']">
    	<!-- nothing to do -->
    </xsl:template>
    
    
    <xsl:template match="nodes[@xsi:type = 'processes:Action']">
    	<xsl:call-template name="activity">
	    	<xsl:with-param name="type">node</xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    
    
    <xsl:template match="nodes[@xsi:type = 'processes:DecisionNode']">
    	<xsl:call-template name="activity">
	    	<xsl:with-param name="type">decision</xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    
    <xsl:template match="nodes[@xsi:type = 'processes:ForkNode']">
    	<xsl:call-template name="activity">
	    	<xsl:with-param name="type">fork</xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    
    <xsl:template match="nodes[@xsi:type = 'processes:MergeNode']">
    	<xsl:call-template name="activity">
	    	<xsl:with-param name="type">merge</xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    
    
    <xsl:template match="nodes[@xsi:type = 'processes:JoinNode']">
    	<xsl:call-template name="activity">
	    	<xsl:with-param name="type">join</xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    

    <xsl:template match="nodes[@xsi:type = 'processes:StructuredActivityNode']">
    	<xsl:call-template name="activity">
	    	<xsl:with-param name="type">block</xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    
	<!-- TODO external (file) subflow
    <xsl:template match="nodes[@xsi:type = 'processes:StructuredActivityNode']">
    	<xsl:call-template name="activity">
	    	<xsl:with-param name="type">subprocess</xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    -->
    
    <xsl:template match="nodes[@xsi:type = 'processes:FinalNode']">
    	<xsl:call-template name="activity">
	    	<xsl:with-param name="type">final</xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    
    
    <xsl:template name="activity">
	    <!-- type : node | decision | fork | merge | join | block | subprocess | final -->
    	<xsl:param name="type"/>
		
		<Activity>
		    <xsl:choose>
		    	<xsl:when test="not(@name) or @name = ''">
		    		<!-- Id and Name must be not empty -->
		    		<xsl:attribute name="Id">ActivityId</xsl:attribute>
					<xsl:attribute name="Name">ActivityName</xsl:attribute>
		    	</xsl:when>
		    	<xsl:otherwise>
		    		<xsl:attribute name="Id"><xsl:value-of select="@name"/></xsl:attribute>
					<xsl:attribute name="Name"><xsl:value-of select="@name"/></xsl:attribute>
		    	</xsl:otherwise>
	    	</xsl:choose>
			
			<!-- TODO
			@executedBy: application
			
            <xsd:element ref="xpdl:Description" minOccurs="0"/>
            <xsd:element ref="xpdl:Limit" minOccurs="0"/>
			-->
			
			<!-- Route | BlockActivity | Implementation -->
			<xsl:if test="$type = 'decision' or $type = 'fork' or $type = 'merge' or $type = 'join'">
				<Route/>
			</xsl:if>
			
			<xsl:if test="$type = 'block'">
				<BlockActivity BlockId="{@name}_ACTIVITYBLOCK"/>
			</xsl:if>
			
			<xsl:if test="$type = 'node' or $type = 'final'">
				<Implementation>
					<No/>
	            </Implementation>
			</xsl:if>
			
			<xsl:if test="$type = 'subprocess'">
				<Implementation>
		            <SubFlow Id="{nodes[1]/@name}" Execution="SYNCHR">
				    	<!-- TODO
				    	Execution : SYNCHR | ASYNCHR
	    	            <ActualParameters>
	    	            	<ActualParameter>STRING</ActualParameter>
	    	            </ActualParameters>
	    	            -->
    	            </SubFlow>
	            </Implementation>
			</xsl:if>
			<!-- TODO
				<Implementation>
		            <Tool/> maxOccurs="unbounded"
	            </Implementation>
		            
	            Tool
               	 <xsd:sequence>
		            <xsd:element ref="xpdl:ActualParameters" minOccurs="0"/>
		            <xsd:element ref="xpdl:Description" minOccurs="0"/>
		            <xsd:element ref="xpdl:ExtendedAttributes" minOccurs="0"/>
		         </xsd:sequence>
		         <xsd:attribute name="Id" type="xsd:NMTOKEN" use="required"/>
		         <xsd:attribute name="Type">
		            <xsd:simpleType>
		               <xsd:restriction base="xsd:NMTOKEN">
		                  <xsd:enumeration value="APPLICATION"/>
		                  <xsd:enumeration value="PROCEDURE"/>
		               </xsd:restriction>
		            </xsd:simpleType>
		         </xsd:attribute>
			-->
			
			<xsl:if test="@performedBy">
				<Performer><xsl:value-of select="jwt2xpdl:getValueFromPath(@performedBy, /core:Model)"/></Performer>
			</xsl:if>
			
			<StartMode>
			    <xsl:choose>
			    	<xsl:when test="$type = 'node' and @performedBy != ''">
						<Manual/>
			    	</xsl:when>
			    	<xsl:otherwise>
			    		<!-- automatic : node without performer and decision, fork, merge, subprocess, final -->
						<Automatic/>
			    	</xsl:otherwise>
			    </xsl:choose>
			</StartMode>
			
			<!-- TODO
            <xsd:element ref="xpdl:FinishMode" minOccurs="0"/>
            <xsd:element ref="xpdl:Priority" minOccurs="0"/>
            <xsd:element ref="xpdl:Deadline" minOccurs="0" maxOccurs="unbounded"/>
            <xsd:element ref="xpdl:SimulationInformation" minOccurs="0"/>
            -->
            
            <xsl:if test="@icon != ''">
            	<Icon><xsl:value-of select="@icon"/></Icon>
            </xsl:if>
            
            <!-- TODO
            <xsd:element ref="xpdl:Documentation" minOccurs="0"/>
			-->
			
			<TransitionRestrictions>
				<TransitionRestriction>
					<xsl:choose>
						<xsl:when test="$type = 'decision' or $type = 'fork'">
				    		<Split>
								<xsl:choose>
									<xsl:when test="$type = 'decision'">
			    						<xsl:attribute name="Type">XOR</xsl:attribute>
									</xsl:when>
									<xsl:otherwise><!-- fork -->
			    						<xsl:attribute name="Type">AND</xsl:attribute>
									</xsl:otherwise>
								</xsl:choose>
								
				    			<TransitionRefs>
				    				<!-- TODO -->
				    				<TransitionRef Id="ref"/>
				    			</TransitionRefs>
				    		</Split>
						</xsl:when>

						<xsl:otherwise><!-- not decision and not fork -->
				    		<Join>
								<xsl:choose>
									<xsl:when test="$type = 'join'">
			    						<xsl:attribute name="Type">AND</xsl:attribute>
									</xsl:when>
									<xsl:otherwise>
			    						<xsl:attribute name="Type">XOR</xsl:attribute>
									</xsl:otherwise>
								</xsl:choose>
				    		</Join>
						</xsl:otherwise>
					</xsl:choose>
				</TransitionRestriction>
			</TransitionRestrictions>
			
			<ExtendedAttributes>
				<ExtendedAttribute Name="XOffsetParticipantView" Value="0"/><!-- TODO -->
				<ExtendedAttribute Name="YOffsetParticipantView" Value="0"/><!-- TODO -->
				<ExtendedAttribute Name="XOffset" Value="{Location/@x}"/>
				<ExtendedAttribute Name="YOffset" Value="{Location/@y}"/>
				
			    <xsl:choose>
			    	<xsl:when test="Size/@height and Size/@height > 20">
				    	<ExtendedAttribute Name="CellHeight" Value="{Size/@height}"/>
			    	</xsl:when>
			    	<xsl:otherwise>
				    	<ExtendedAttribute Name="CellHeight" Value="30"/>
			    	</xsl:otherwise>
			    </xsl:choose>
			    
			    <xsl:choose>
			    	<xsl:when test="Size/@width and Size/@width > 100">
				    	<ExtendedAttribute Name="CellWidth" Value="{Size/@width}"/>
			    	</xsl:when>
			    	<xsl:otherwise>
				    	<ExtendedAttribute Name="CellWidth" Value="150"/>
			    	</xsl:otherwise>
			    </xsl:choose>
			    
				<xsl:if test="@executedBy != ''">
			    	<xsl:variable name="javaClass"><xsl:value-of select="jwt2xpdl:getValueFromPath(@executedBy, /core:Model, 'javaClass')"/></xsl:variable>
			    	<xsl:if test="starts-with($javaClass, 'hero.hook.')">
						<ExtendedAttribute Name="hook" Value="{$javaClass}">
							<HookEventName>afterTerminate</HookEventName><!-- TODO not always afterTerminate -->
						</ExtendedAttribute>
					</xsl:if>
			    </xsl:if>

			    <!-- data -->
			    <!-- TODO problem with activity properties: names must be unique in XPDL
			    		  solution: all properties are process properties
			    <xsl:if test="@inputs != ''">
			    	<xsl:for-each select="jwt2xpdl:getValuesFromPath(@inputs, /core:Model)/*">
				    	<ExtendedAttribute Name="property" Value="{text()}">
							<Propagated>No</Propagated>
						</ExtendedAttribute>
			    	</xsl:for-each>
			    </xsl:if>
			    <xsl:if test="@outputs != ''">
			    	<xsl:for-each select="jwt2xpdl:getValuesFromPath(@outputs, /core:Model)/*">
				    	<ExtendedAttribute Name="property" Value="{text()}">
							<Propagated>Yes</Propagated>
						</ExtendedAttribute>
			    	</xsl:for-each>
			    </xsl:if>
				-->

			    <!-- TODO the transitions who go back in the past (cycle) must be translated as iteration for Bonita
		    	<ExtendedAttribute Name="Iteration" Value="!datafield.equals(&quot;&quot;)">
			    	<To>ActivityX</To>
			    	<ExtendedAttributes>
				    	<ExtendedAttribute Name="BreakPoint" Value="533.0-156.0" />
				    	<ExtendedAttribute Name="BreakPoint" Value="395.0-147.0" />
				    	<ExtendedAttribute Name="BreakPoint" Value="318.5-103.0" />
			    	</ExtendedAttributes>
		    	</ExtendedAttribute>
			    -->
			    
			    <xsl:if test="$type = 'subprocess'">
					<!-- TODO subprocess
					<ExtendedAttribute Name="File" Value="..."/>
					<ExtendedAttribute Name="SubFlowVersion" Value="..."/>
					<ExtendedAttribute Name="SubFlowName" Value="..."/>
					-->
				</xsl:if>
			</ExtendedAttributes>
		</Activity>
	</xsl:template>


    <xsl:template name="activity-set">
    	<ActivitySet Id="{@name}_ACTIVITYBLOCK">
    		<Activities>
    			<xsl:apply-templates select="nodes"/>
			</Activities>
			
			<Transitions>
				<xsl:apply-templates select="edges"/>
			</Transitions>
    	</ActivitySet>
    </xsl:template>


    <xsl:template match="edges">
	    <!--
	    	traduction of @source and @target:
	    	//@elements.0/@nodes.1 -> //elements[1]/nodes[2]/@name -> evaluate XPath
	    -->
	    <xsl:variable name="source" select="jwt2xpdl:getValueFromPath(@source, /core:Model)"/>

	    <xsl:if test="$source != ''">
		    <xsl:variable name="target" select="jwt2xpdl:getValueFromPath(@target, /core:Model)"/>
		    <xsl:variable name="id" select="translate(concat($source, '_', $target), ' ', '_')"/>
	    	
			<Transition Id="{$id}" Name="{$source}_{$target}" From="{$source}" To="{$target}">
				<!--
					JWT:
			        <guard name="Disprove" textualdescription="Disprove" shortdescription="isApproved.isApproved == false">
			          <detailedSpecification data="//@subpackages.2/@subpackages.1/@elements.4" attribute="isApproved" value="false"/>
			        </guard>
			        shortdescription="(Branche.Branche == tests &amp;&amp; isApproved.isApproved != true) || Reisemittel.Reisemittel == ok"
			        
			        XPDL:
			        <Condition Type="CONDITION">isApproved.equals("true")</Condition>
         			<Condition Type="CONDITION">(Branche.equals("tests") &amp;&amp; !isApproved.equals("true")) || Reisemittel.equals("ok")</Condition>
				-->
				<xsl:if test="guard">
					<!-- <xsl:variable name="data" select="jwt2xpdl:getValueFromPath(guard/detailedSpecification/@data, /core:Model)"/> -->
					<!--
					<xsl:variable name="attribute" select="guard/detailedSpecification/@attribute"/>
					<xsl:variable name="value" select="guard/detailedSpecification/@value"/>
					<Condition Type="CONDITION"><xsl:value-of select="concat($attribute, '.equals(&quot;', $value, '&quot;)')"/></Condition>
					-->
					<Condition Type="CONDITION"><xsl:value-of select="jwt2xpdl:getCondition(guard/@shortdescription, guard, /core:Model)"/></Condition>
				</xsl:if>
			</Transition>
		</xsl:if>
    </xsl:template>

</xsl:stylesheet>
