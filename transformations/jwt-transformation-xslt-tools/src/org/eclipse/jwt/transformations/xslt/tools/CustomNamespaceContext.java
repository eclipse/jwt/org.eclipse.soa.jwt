/**
 * File:    CustomNamespaceContext.java
 * Created: 21.03.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Guillaume Decarnin, Open Wide, Lyon, France
 *      - Creation and implementation
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Refactoring to include it in common XSLT tools plugin
 *******************************************************************************/
package org.eclipse.jwt.transformations.xslt.tools;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.namespace.NamespaceContext;

/**
 * @author Guillaume Decarnin - Open Wide
 * @since 0.6
 */
public class CustomNamespaceContext implements NamespaceContext {

    private final Map<String, String> mapUri;

    private final Map<String, String> mapPrefix;

    /**
     * @param mapUri
     *            key = prefix ; value = namespaceURI
     */
    public CustomNamespaceContext(final Map<String, String> mapUri) {
        this.mapUri = mapUri;

        // reverted map
        this.mapPrefix = new HashMap<String, String>();
        for (final Entry<String, String> entry : mapUri.entrySet()) {
            this.mapPrefix.put(entry.getValue(), entry.getKey());
        }
    }

    /**
     * Namespace with one URI.
     * 
     * @param prefix
     * @param uri
     */
    public CustomNamespaceContext(final String prefix, final String uri) {
        this.mapUri = new HashMap<String, String>();
        this.mapUri.put(prefix, uri);

        // reverted map
        this.mapPrefix = new HashMap<String, String>();
        this.mapPrefix.put(uri, prefix);
    }

    /**
     * @param prefix
     */
    public final String getNamespaceURI(final String prefix) {
        return this.mapUri.get(prefix);
    }

    /**
     * @param namespaceURI
     */
    public final String getPrefix(final String namespaceURI) {
        return this.mapPrefix.get(namespaceURI);
    }

    /**
     * @param namespaceURI
     */
    public final Iterator<String> getPrefixes(final String namespaceURI) {
        return this.mapPrefix.values().iterator();
    }
}
