JWT Java Workflow Tooling
JWT Tools for XSLT based transformations

Copyright (c)
Open Wide SA  <www.openwide.fr> 

All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Committers from this project come from
Open Wide SA, Lyon, France
