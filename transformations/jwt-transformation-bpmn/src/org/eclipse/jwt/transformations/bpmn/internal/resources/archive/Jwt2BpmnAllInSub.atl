-- Copyright (c) 2005-2012 Obeo
-- 
-- All rights reserved. This program and the accompanying materials
-- are made available under the terms of the Eclipse Public License 1.0
-- which accompanies this distribution, and is available at
-- http://www.eclipse.org/legal/epl-v10.html
--
-- Contributors:
--    Obeo - initial API and implementation

module Jwt2BpmnAllInSub; -- Module Template
create OUT : bpmn from IN : jwt;

-- helpers

-- This helper stores an integer value used to constructs each undefined name
-- CONTEXT: thisModule
-- RETURN: Integer
helper def: curId : Integer = 0;

-- This helper computes the value to be assigned to the new name of a generated
-- SequenceEdge's name. It increments the value stored by the "curId" helper and returns its
-- new value as a String.
-- CONTEXT: thisModule
-- RETURN: String
helper def: getId() : String =
thisModule.refSetValue('curId', thisModule.curId + 1).curId.toString();

-- This helper builds the name of a sequenceEdge. It looks for the names of the source/target 
-- and then it adds the guard's short description if it exists.
helper context jwt!"model::processes::ActivityEdge" def: getName():String=
	let source : String = 
		if self.source.oclIsTypeOf(jwt!"model::processes::InitialNode")
		then 'Init'
		else if self.source.name.oclIsUndefined()
			then 'defaultName'+thisModule.getId()
			else self.source.name
			endif
		endif  
	in let target : String = 
		if self.target.oclIsTypeOf(jwt!"model::processes::FinalNode")
		then 'End'
		else if self.target.oclIsTypeOf(jwt!"model::processes::MergeNode")
			then self.target.out.first().target.name
			else if self.target.name.oclIsUndefined()
				then 'defaultName'+thisModule.getId()
				else self.target.name
			    endif
			endif
		endif
	in 	source+'2'+target+ if self.guard.oclIsUndefined()
		then ''
		else ', guard : '+self.guard.shortdescription
		endif;

-- This helper returns the BPMN DirectionType translated from a jwt EdgeDirection of a ReferenceEdge.
helper context jwt!"model::processes::ReferenceEdge" def: getDirection():bpmn!DirectionType=
	if self.direction = #default
	then #None -- ??
	else if self.direction = #none
		then #None
		else if self.direction = #inout
			then #Both
			else if self.direction = #out
				then #To
				else #From -- self = #in  
				endif
			endif
		endif
	endif;

-- This helper returns a String representation of the BPMN DirectionType translated from a jwt EdgeDirection of a ReferenceEdge.
helper context jwt!"model::processes::ReferenceEdge" def: getDirectionString():bpmn!DirectionType=
	if self.direction = #default
	then 'default' -- ??
	else if self.direction = #none
		then 'none'
		else if self.direction = #inout
			then 'inout'
			else if self.direction = #out
				then 'out'
				else 'in' -- self = #in  
				endif
			endif
		endif
	endif;

-- This helper looks for the "Role" of an ActivityNode (Except for InitialNode, Event) : in jwt, only Action can be "perfomedBy" a Role but in bpmn, 
-- each Activity can be placed in a Lane (translated from Role) so we have to look for the Role of the Action preceding the ActivityNode
helper context jwt!"model::processes::ActivityNode" def: getSourceRole():jwt!"model::organisations::Role"=
	if self.oclIsTypeOf(jwt!"model::processes::Action")
	then self.performedBy
	else if self.in -> exists( e| e.oclIsTypeOf(jwt!"model::processes::ActivityEdge"))
		then self.in.first().source.getSourceRole()
		else OclUndefined
		endif
	endif;

-- This helper looks for the "Role" of an ActivityNode (InitialNode, Event) : in jwt, only Action can be "perfomedBy" a Role but in bpmn, 
-- each Activity can be placed in a Lane (translated from Role) so we have to look for the Role of the first Action linked to an InitialNode or an Event
-- This Action can only be connected in the outgoing edge because these two types (pb with Event see spec) don't have incoming edges.
helper context jwt!"model::processes::ActivityNode" def: getTargetRole():jwt!"model::organisations::Role"=
	if self.oclIsTypeOf(jwt!"model::processes::Action")
	then self.performedBy
	else if self.out -> exists( e| e.oclIsTypeOf(jwt!"model::processes::ActivityEdge"))
			then self.out.first().target.getTargetRole()
			else OclUndefined
		endif
	endif;

-- This helper checks if an ActivityEdge will be translated. 
-- MergedNode are not translated : there incoming edges are directly connected to the BPMN Task 
-- corresponding to its single outcoming JWT Action.
helper context jwt!"model::processes::ActivityEdge" def: isConvertible():Boolean=
	not self.source.oclIsTypeOf(jwt!"model::processes::MergeNode");
	
-- This helper checks if a data will be translated. 
-- We will translate only data which are associated to JWT Actions in the model
helper context jwt!"model::data::Data" def: isConvertible():Boolean=
	jwt!"model::processes::ReferenceEdge".allInstances() -> select(
		ref |ref.isValid()) -> exists( 
			refEdge | refEdge.reference.reference = self);

-- This helper checks if an application will be translated. 
-- We will translate only application which are associated to JWT Actions in the model
helper context jwt!"model::application::Application" def: isConvertible():Boolean=
	jwt!"model::processes::ReferenceEdge".allInstances() -> select(
		ref |ref.isValid()) -> exists( 
			refEdge | refEdge.reference.reference = self);

-- This helper checks if a ReferenceEdge will be translated.
-- ie : for the moment only ReferenceEdge from Data is translated into Association
helper context jwt!"model::processes::ReferenceEdge" def: isConvertible():Boolean=
	if self.isValid() 
	then self.reference.reference.oclIsTypeOf(jwt!"model::data::Data")
	else false
	endif;

-- This helper checks if a ReferenceEdge is valid.
-- ie : it is valid if its reference Reference and the ReferenceableElement reference of this Reference are defined
helper context jwt!"model::processes::ReferenceEdge" def: isValid():Boolean=
	if self.reference.oclIsUndefined()
	then false
	else if self.reference.reference.oclIsUndefined()
		then false
		else true
		endif
	endif;

--helper context jwt!"model::processes::ActivityEdge" def: isConvertibleIntoFlowLink():Boolean=
	-- TODO Later
	-- not self.isConvertibleIntoMessageLink();
    -- or : the jwt elements which will be translated into different pools have to be specified
	-- in order to be able to navigate in the jwt model and to separate element from different pool by message links
	-- 'source.getPool' = 'target.getPool' else messagelink
	
	-- first idea
	--if self.source.oclIsTypeOf(jwt!"model::processes::Action")
	--then 
	--	if self.target.oclIsTypeOf(jwt!"model::processes::Action") 
	--	then
	--	-- test for lanes not for pools...
	--		self.source.performedBy=self.target.performedBy 
	--	else
	--		-- other Nodes are in the same role : control Node, add a test??
	--		-- case of Final Node, Decision Node,...
	--		true
	--	endif
	--else
	--	-- case of Initial Node linked to an Action
	--	if self.target.oclIsTypeOf(jwt!"model::processes::Action") 
	--	then
	--		--test
	--		true
	---	else
	--		false
	--	endif
	--endif;



--helper context jwt!"model::processes::ActivityEdge" def: isConvertibleIntoMessagingLink():Boolean=
	--not self.isConvertibleIntoFlowLink();
	--if self.source.oclIsTypeOf(jwt!"model::processes::Action") and 
	--self.target.oclIsTypeOf(jwt!"model::processes::Action") 
	--then 
	--	not self.source.performedBy=self.target.performedBy 
	--else
	--	false
	--endif;




-- rules 

rule Model2BPMNDiagram {
	from input : jwt!"model::core::Model"
	to output : bpmn!BpmnDiagram (
		name <- input.name,
		-- todo later when pool will be used to single out different execution environmetns
		--messages<-jwt!"model::processes::ActivityEdge".allInstances()->select(e | e.isConvertibleIntoMessagingLink()),
		
		pools <- pool),
		-- TODO later : rule to transform jwt element (Activiy = diagrams or one Pool per Activity which is not a subProcess of another activity ????)
		-- when pool will be used to single out different execution environments
		-- test to add in after each eAllInstances() in order to take only elements of this pool...
		pool: bpmn!Pool (
			name <- 'Pool', 
			
			-- Role can be found in input.elemets or in subpackages : 
			--lanes <- input.elements -> select( e1 | e1.oclIsTypeOf(jwt!"model::organisations::Role") ) -> flatten(),
			--lanes <- input.subpackages -> collect( e | e.elements -> select( e1 | e1.oclIsTypeOf(jwt!"model::organisations::Role") ) ) -> flatten(),
			lanes <- jwt!"model::organisations::Role".allInstances(), -- TODO Later : select role if actions of this pool are performed by it
			
			artifacts <- jwt!"model::data::Data".allInstances(),
				-- only convertible data will be translated, so the following lines aren't necessary
				-- -> select(
				-- d | jwt!"model::processes::ReferenceEdge".allInstances() -> select(
				-- 	ref |ref.isConvertible()) -> exists( 
				--		refEdge | refEdge.reference.reference = d)),
			
			-- add Actions, InitialNodes, FinalNodes, and other ActivityNode transformed by a rule
			-- but only those which are not in a subprocess
			vertices <- jwt!"model::processes::Activity".allInstances()-> reject(
				e| jwt!"model::processes::ActivityLinkNode".allInstances() ->exists(
					link| link.linksto=e)) -> collect(
						activity | activity.nodes)->flatten(),
				--jwt!"model::processes::ActivityNode".allInstances(),

			sequenceEdges <- jwt!"model::processes::Activity".allInstances()-> reject(
				e| jwt!"model::processes::ActivityLinkNode".allInstances() -> exists(
					link| link.linksto=e)) -> collect(
						activity | activity.edges)->flatten()
				-- -> select(e | e.isConvertible()) -- not necessary because only convertible activity edges are translated -> select(e | e.isConvertible())	
			-- TODO : later -> reject( e | e.isConvertibleIntoMessagingLink())
					--   or -> select( e | e.isConvertibleIntoFlowLink())
	)
}


-- Translate JWT Role by BPMN Lane
rule Role2Lane {
	from input : jwt!"model::organisations::Role" 
	to output : bpmn!Lane (
		name		<-	input.name,
        -- add actions
		activities	<-	jwt!"model::processes::Action".allInstances() -> select(
			e | e.oclIsTypeOf(jwt!"model::processes::Action") and e.performedBy = input)
		
		-- add initial nodes and events diectly connected to a task/action, other control nodes will be added if they are the target of an activity whose source is in this lane
		--activities	<- jwt!"model::processes::ActivityEdge".allInstances()-> select(
		--	e | e.oclIsTypeOf(jwt!"model::processes::ActivityEdge")
		--		and e.isConvertible()
		--		and ( e.source.oclIsTypeOf(jwt!"model::processes::InitialNode") 
		--			 or e.source.oclIsTypeOf(jwt!"model::events::Event" ))
		--		and jwt!"model::processes::Action".allInstances() -> exists(
		--			action | action = e.target and action.performedBy = input)
		--	) -> collect(e1 | e1.source),

		-- add final nodes  and other control nodes which are targets of activity edges of this lane
		-- theses  control nodes will be added if they are the target of an activity whose source is an action performed by the role/lane
		--activities	<-	jwt!"model::processes::ActivityEdge".allInstances()-> select(
		--	e | e.oclIsTypeOf(jwt!"model::processes::ActivityEdge")
		--	and e.isConvertible() -- TODO Later : and e.isConvertibleIntoFlowLink()
		--	and not e.target.oclIsTypeOf(jwt!"model::processes::MergeNode")
		--	and jwt!"model::processes::Action".allInstances() -> exists(
		--		action | action = e.source and action.performedBy = input)
		--	) -> collect(e1 | e1.target) -> reject ( tgt | tgt.oclIsTypeOf(jwt!"model::processes::Action"))
	)
}


-- Translate JWT Action by BPMN TAsk
rule Action2Task {
	from input : jwt!"model::processes::Action"
	to output : bpmn!Activity (
		name <- input.name,
		activityType <-	#Task,
		eAnnotations <- jwt!"model::application::Application".allInstances() -> select(
			appli | input.executedBy = appli)-> collect(e|thisModule.Application2TaskAnnotation(e))
	)
}


-- This lazy rule is called by Action2Task :
-- with a normal rule : one application -> one EAnnotations
-- but we want an eAnotation on each Task/ection executedBy this application
lazy rule Application2TaskAnnotation {
	from input : jwt!"model::application::Application" (input.isConvertible())
	to outputs : bpmn!EAnnotation (
		source <- 'org.eclipse.jwt/processes',
		--references <- a,
		details <- Sequence{ApName, ApJavaClass, ApIcon, ApJarFile}
	),		
	ApName: bpmn!EStringToStringMapEntry (
		key <- 'jwt_performedByApplication_name',
		value <- if input.name.oclIsUndefined()
				then 'no found name'
				else input.name
 			endif
	),
	ApJavaClass: bpmn!EStringToStringMapEntry (
		key <- 'jwt_performedByApplication_javaClass',
		value <- if input.javaClass.oclIsUndefined()
				then 'no found javaClass'
				else input.javaClass
				endif
	),
	ApIcon: bpmn!EStringToStringMapEntry (
		key <- 'jwt_performedByApplication_icon',
		value <- if input.icon.oclIsUndefined()
				then 'no found icon'
				else input.icon
				endif
	),
	ApJarFile: bpmn!EStringToStringMapEntry (
		key <- 'jwt_performedByApplication_jarFile',
		value <- if input.jarArchive.oclIsUndefined()
				then 'no found jarFile'
				else if input.jarArchive = ''
					then 'no found jarFile'
					else input.jarArchive
					endif
				endif
	)
}


-- for each JWT Activity Edge, if it is between two Actions handled by the same role create a SequenceEdge, 
-- else create a MessagingEdge (because in BPMN a Flow can't link Tasks in different pools)
-- MP : Role <-> Lane
rule ActivityEdge2FlowLink {
	from input : jwt!"model::processes::ActivityEdge" (input.isConvertible())-- TODO later : (input.isConvertibleIntoFlowLink())
	to output : bpmn!SequenceEdge(
		name <-  input.getName() ,
		source <- jwt!"model::processes::ActivityNode".allInstances()->select ( 
				e| e=input.source).first(),
		target <- if input.target.oclIsTypeOf(jwt!"model::processes::MergeNode")
				  then input.target.out.first().target
				  else jwt!"model::processes::ActivityNode".allInstances()->select ( 
				  	e | e=input.target).first()
				  endif,
		eAnnotations <- jwt!"model::processes::Guard".allInstances() -> select(
			e| e = input.guard)
--	),
--	annotation: bpmn!EAnnotation (
--		source <- 'org.eclipse.jwt/processes',
--		references <- input,
--		details <- detail
--	),		
--	detail: bpmn!EStringToStringMapEntry (
--		key <- 'jwt_guard',
--		value <- if input.guard.oclIsUndefined()
--				then 'no found guard'
--				else input.guard.shortdescription
--				endif
	)
}

rule Guard2SequenceEAnnotation {
	from input : jwt!"model::processes::Guard"
	to output : bpmn!EAnnotation (
		source <- 'org.eclipse.jwt/processes',
		references <- jwt!"model::processes::ActivityEdge".allInstances() -> select(
			e| input = e.guard),
		details <- detail
	),		
	detail: bpmn!EStringToStringMapEntry (
		key <- 'jwt_guard',
		value <- input.shortdescription
	)	
}


-- for each JWT Activity Edge, if it is between two Actions handled by the same role (MP or 2 roles in the same pool) create a SequenceEdge, 
-- else create a MessagingEdge (because in BPMN a Flow can't link Tasks in different pools)
-- MP : Role <-> Lane TODO Later when pools will be used to single out different exectution environments

  --rule ActivityEdge2MessageLink {
  --	from input : jwt!"model::processes::ActivityEdge" (input.isConvertibleIntoMessagingLink())
  --	to output : bpmn!MessagingEdge(
  --		source <- jwt!"model::processes::Action".allInstances()->select ( 
  --				e | e.oclIsTypeOf(jwt!"model::processes::Action") and e=input.source).first(),			
  --		target <- jwt!"model::processes::Action".allInstances()->select ( 
  --				e | e.oclIsTypeOf(jwt!"model::processes::Action") and e=input.target).first()
  --	)
  --}


-- for each JWT Decision node, create an STP BPMN GatewayDataBasedExclusive
-- if such a JWT Decision node has an incoming edge from an JWT Action performed 
-- by another role, create an intermediary Task in the same pool and link it by a 
-- Message link with the Task corresponding to this JWT Action ; idem for a JWT Decision node 
-- that has an outcoming edge towards an Action performed by another role.

-- MP : a Decision Node does'nt have a Lane or Pool, it cannot be "permormedBy"
-- 		so the GatewayDataBaseExclusive will be performed by its source's role, it will 
--      belong to its source Lane
rule DecisionNode2GatewayDataBasedExclusive {
	from input : jwt!"model::processes::DecisionNode"
	to output : bpmn!Activity (
		name <- input.name, 
		activityType  <- #GatewayDataBasedExclusive,
		lane <- input.getSourceRole()
		--input.in.first().source.performedBy
	)
}


-- Don't translate JWT Merge node but directly connect its incoming edges to the BPMN Task 
-- corresponding to its single outcoming JWT Action.
-- no rule here : connections already done in rule ActivityEdge2FlowLink
--rule MergeNode2IncomingEdgesToTask {
--	from input : jwt!"model::processes::MergeNode"
--	to output : distinct bpmn!SequenceEdge foreach(a in input.in)  (
--		source <- a.source, 
--		target <- input.out.first()
--	)
--}

-- for each JWT Data create a Data Object
-- annotations are added because , only used Data are translated
rule Data2DataObject {
	from input : jwt!"model::data::Data" (input.isConvertible())
	to output : bpmn!DataObject	(
		name	<-	input.name,
		eAnnotations <- annotation
	),
	annotation: bpmn!EAnnotation (
		source <- 'org.eclipse.jwt/processes',
		references <- input,
		details <- detail
	),		
	detail: bpmn!EStringToStringMapEntry (
		key <- 'jwt_dataType',
		value <- if input.dataType.oclIsUndefined()
				then 'no found DataType'
				else input.dataType.name
				endif
	)
}


-- Translate references to (i.e. input and / or output of a JWT Action) it by 
-- BPMN Associations targeting the corresponding BPMN Tasks. 
-- !! Difference with Spec !!! translate as direction and not annotation : JWT Data reference attributes (like input, output or both) 
rule ReferenceEdge2Association {
	from input : jwt!"model::processes::ReferenceEdge" (input.isConvertible())
	to output : bpmn!Association (
		source <- input.reference.reference,
		target <- input.action ,
		direction <- input.getDirection(),
		eAnnotations <- annotation
	),
	annotation: bpmn!EAnnotation (
		source <- 'org.eclipse.jwt/processes',
		references <- input,
		details <- detail
	),		
	detail: bpmn!EStringToStringMapEntry (
		key <- 'jwt_direction',
		value <- input.getDirectionString()
	)
}


-- add Empty Start event as expressed in JWT by start Activity Node (in the pool corresponding to the linked Task)
-- Translate JWT InitialNode by BPMN EventStartEmpty
rule InitialNode2EventStartEmpty {
	from input : jwt!"model::processes::InitialNode"
	to output : bpmn!Activity (
		name	<-	input.name,		
		activityType	<-	#EventStartEmpty,
		lane <- input.getTargetRole()
		--input.out.first().target.performedBy
	)
}


-- add Terminate End event as expressed in JWT by end Activity Node (in the pool corresponding to the linked Task)
-- Translate JWT FinalNode by BPMN EventEndTerminate
rule FinalNode2EventEndTerminate {
	from input : jwt!"model::processes::FinalNode"
	to output : bpmn!Activity (
		name	<-	input.name,
		activityType	<-	#EventEndTerminate,
		lane <- input.getSourceRole()
		--input.in.first().source.performedBy
	)
}


-- Todo later because only one pool for the moment :
-- in each pool, add additional Terminate End events where they are missing (since there should 
-- be end events in each pool and not only globally as in JWT. One algorithm to achieve this 
-- is as follows : in each pool, for each Task with no outcoming Flow link but one or more outgoing 
-- Message links (NB. this corresponds in JWT to an Action that will followed by another Action 
-- performed by a different role : the process "quits" temporarily this pool ; though no outgoing 
-- message link either could be allowed, but that would mean the original JWT process is not valid), 
-- create a Terminate End Event.
-- ...


-- map JWT Events to BPMN Intermediate Events
--  * to Empty Event (TODO or Message Event, other BPMN Events can't be produced)
--  * and connect them by a Flow link to the BPMN Task corresponding to their single outcoming edge's target Action
rule Event2IntermediateEvent {
	from input :  jwt!"model::events::Event"
	to output : bpmn!Activity (
		name <- input.name,
		activityType <- #EventIntermediateEmpty,
		lane <- input.out.first().target.performedBy
	)
}


-- Translate JWT EmbeddedSubProcess (= StructuredActivity) into BPMN SubProcess
rule StructuredActivity2SubProcess {
	from input : jwt!"model::processes::StructuredActivityNode"
	to output : bpmn!SubProcess (
		name <- input.name,
		activityType <- #Task, -- with #SubProcess diagram generation does not works
		--On peut retrouver dans SubProcess tout ce qu'il y a dans Pool!!
		--containment : pb -> un Vertex ne peut �tre contenu que dans un seul Graph (Pool ou SubProcess),
		vertices <- input.nodes,
		sequenceEdges <- input.edges
		-- -> select(e | e.isConvertible()) -- not necessary because only convertible activity edges are translated -> select(e | e.isConvertible())	
		-- TODO : later -> reject( e | e.isConvertibleIntoMessagingLink())
		--   or -> select( e | e.isConvertibleIntoFlowLink())	
	)
}

-- Translate JWT SubProcessCall (= ActivityLinkNode) into BPMN SubProcess
rule ActivityLinkNode2SubProcess {
	from input : jwt!"model::processes::ActivityLinkNode"
	to output : bpmn!SubProcess (
		name <- input.name,
		activityType <- #Task, -- with #SubProcess diagram generation does not works
		--On peut retrouver dans SubProcess tout ce qu'il y a dans Pool!!
		--containment : pb -> un Vertex ne peut �tre contenu que dans un seul Graph (Pool ou SubProcess),
		vertices <- input.linksto.nodes,
		sequenceEdges <- input.linksto.edges
		-- -> select(e | e.isConvertible()) -- not necessary because only convertible activity edges are translated -> select(e | e.isConvertible())	
		-- TODO : later -> reject( e | e.isConvertibleIntoMessagingLink())
		--   or -> select( e | e.isConvertibleIntoFlowLink())	
	)
}







