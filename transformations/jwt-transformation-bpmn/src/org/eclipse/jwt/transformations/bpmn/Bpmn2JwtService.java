/**
 * File:    Bpmn2JwtService.java
 * Created: 04.03.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012  Open Wide (www.openwide.fr)
 * 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/


package org.eclipse.jwt.transformations.bpmn;

import java.io.IOException;

import org.eclipse.jwt.transformations.api.TransformationException;
import org.eclipse.jwt.transformations.api.TransformationService;
import org.eclipse.jwt.transformations.bpmn.internal.ATLTransformation;

/**
 * This {@link TransformationService} should implement a BPMN (in the sense
 * of STP-BPMN) file into a JWT file. However it currently does nothing.
 * @author mistria
 *
 * @since 0.6
 */
public class Bpmn2JwtService extends TransformationService {

	@Override
	public void transform(String inFilePath, String outFilePath)
			throws IOException, TransformationException {
		ATLTransformation.getInstance().bpmn2jwt(inFilePath, outFilePath);
	}

}
