--
-- File:   BPMN 2 JWT
-- Created: 07.05.2009
--
--
--------------------------------------------------------------------------------
-- Copyright (c) 2005-2012
-- Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
--
-- All rights reserved. This program and the accompanying materials
-- are made available under the terms of the Eclipse Public License v1.0
-- which accompanies this distribution, and is available at
-- http://www.eclipse.org/legal/epl-v10.html
--
-- Contributors:
--    Maxime Porhel, Obeo, Nantes, France
--      - creation and implementation
--    Florian Lautenbacher, University of Augsburg, Germany
--      - adaptation to new BPMN metamodel
--      - 2009-12: adaptation to outsourced view metamodel
--------------------------------------------------------------------------------

module BPMN2JWT;
create OUT : jwt from IN : bpmn;

-- helpers

helper context bpmn!Activity def : hasRole() : Boolean = 
	not self.lanes.oclIsUndefined();

-- This helper returns the BPMN DirectionType translated from a jwt EdgeDirection of a ReferenceEdge.
helper context bpmn!Association def: getDirection():jwt!EdgeDirection=
	if self.direction = #None
	then #none
	else if self.direction = #Both
		then #inout
		else if self.direction = #To
			then #out
			else #"in" -- self = #From 
			endif
		endif
	endif;-- default ??
	
-- rules		

-- Translata a BPMN Diagram to a JWT Model
rule BpmnDiagram2Model {
	from input : bpmn!BpmnDiagram
	to output : jwt!Model (
	   name <- input.name,
	   fileversion <- '1.2.0',
	   author <- 'Generated automatically from BPMN',
	   -- getting roles from lanes using Lane2Role
	   elements <- bpmn!Lane.allInstances(),
	   -- getting data from all DataObjects
	   elements <- bpmn!DataObject.allInstances(),
	   -- TODO applications at some point...
	   -- elements <- ...,
	   -- getting activities out of pools
	   elements <- input.pools,
	   -- ? getting subprocesses
       elements <- bpmn!SubProcess.allInstances() -> collect(e | thisModule.resolveTemp(e, 'activity') )
	)
}

-- Translate each Pool into JWT Activity
rule Pool2Activity {	
	from input : bpmn!Pool
	to output :  jwt!Activity (
		name <- input.name,
		
		-- since JWT 0.7.0 no references or referenceEdges anymore!!
		--references <- input.lanes -> collect(e | thisModule.resolveTemp(e,'reference')),
		--references <- input.vertices -> collect(v|v.associations -> collect(a|a.source))->flatten() -> select(e| e.oclIsTypeOf(bpmn!DataObject))-> collect(t | thisModule.resolveTemp(t,'reference') ),		

		-- getting nodes (ex. actions) from vertices (ex. activity)
		nodes <- input.vertices,

		--referenceEdges <- input.vertices -> select(e|e.oclIsTypeOf(bpmn!Activity) and e.activityType = #Task and e.lanes.notEmpty()) -> collect(e | thisModule.Task2RoleReferenceEdge(e)),
		--referenceEdges <- input.vertices -> collect(e|e.associations),

		-- getting edges (linking actions) from sequenceEdges (linking activities)
		edges <- input.sequenceEdges
	)
}


-- Translate each SubProcess into JWT Activity
-- Todo Later : a second Rule to translate subProcesses and Messge Link to other Pool
rule SubProcess2ActivityAndActivityLinkNode {
	from input : bpmn!SubProcess (input.activityType = #Task)
	to activityLinkNode : jwt!ActivityLinkNode (
		name <-input.name,
		linksto <- activity
	),
	activity : jwt!Activity(
		name <- input.name,
		--references <- if input.lanes.oclIsUndefined()
  			--		then Sequence {}
			--		else input.lanes -> collect(e|thisModule.resolveTemp(e,'reference'))
			--		endif,
		--references <- input.vertices -> collect(v|v.associations -> collect(a|a.source))->flatten() -> select(e| e.oclIsTypeOf(bpmn!DataObject))-> collect(t | thisModule.resolveTemp(t,'reference') ),		

		-- getting nodes (ex. actions) from vertices (ex. activity) 		
		nodes <- input.vertices,
		
		--referenceEdges <- input.vertices -> select(e|e.oclIsTypeOf(bpmn!Activity) and e.activityType = #Task and e.lanes.notEmpty()) -> collect(e | thisModule.Task2RoleReferenceEdge(e)),
		--referenceEdges <- input.vertices -> collect(e|e.associations),

		-- getting edges (linking actions) from sequenceEdges (linking activities)
		edges <- input.sequenceEdges	
	)
}

-- Translate BPMN Lane by JWT Reference reference
rule Lane2Role {
	from input : bpmn!Lane 
	to role : jwt!Role (
		name <-	input.name
	--),
    --reference : jwt!Reference (
	--	reference <- input
	)
}

-- Translate BPMN Task by JWT Action
rule Task2Action {
	from input : bpmn!Activity (input.activityType = #Task and not input.oclIsTypeOf(bpmn!SubProcess))
	to action : jwt!Action (
		name <- input.name,
		-- getting action-bound referenceEdge from activity (lanes)
	    performedBy <- input.lanes.first()
	)
}

-- Translate BPMN Lane reference of a Task by jwt ReferenceEdge
--lazy rule Task2RoleReferenceEdge {
--	from input : bpmn!Activity
--	to referenceEdge : jwt!ReferenceEdge (
--		direction <- #default,
--		action <- input,
		-- getting corresponding previously (in Pool2Activity) generated role reference
--		reference <- thisModule.resolveTemp(input.lanes.first(), 'reference')
--	)
--}

-- Translate BPMN EventStartEmpty by JWT InitialNode 
rule EventStartEmpty2InitialNode {
	from input : bpmn!Activity (input.activityType = #EventStartEmpty)
	to output : jwt!InitialNode (
		name	<-	input.name
	)
}

-- Translate BPMN EventEndTerminate by JWT FinalNode
rule EventEndTerminate2FinalNode {
	from input : bpmn!Activity (input.activityType	= #EventEndTerminate)
	to output : jwt!FinalNode (
		name	<-	input.name
	)
}

-- Translate BPMN GateWayDataBasedExclusive by JWT DecisionNode
rule GatewayDataBasedExclusive2DecisionNode {
	from input : bpmn!Activity (input.activityType = #GatewayDataBasedExclusive)
	to output : jwt!DecisionNode (
		name <- input.name
	)
}

-- Translate BPMN GatewayParallel by JWT ForkNode
rule GatewayParallel2ForkNode {
	from input : bpmn!Activity (input.activityType = #GatewayParallel)
	to output : jwt!ForkNode (
		name <- input.name
	)
}

rule FlowLink2ActivityEdge {
	from input : bpmn!SequenceEdge
	to output : jwt!ActivityEdge(
		source <- input.source,
		target <- input.target
	)
}

-- Translate BPMN DataObject by JWT Data and JWT Reference
rule DataObject2Data {
	from input : bpmn!DataObject
	to output : jwt!Data	(
		name	<-	input.name
	--), 
	--reference : jwt!Reference (
	--	reference <- input	
	)
}

--rule Association2ReferenceEdge {
--	from input : bpmn!Association
--	to output : jwt!ReferenceEdge (
--		action <- input.target,
--		direction <- input.getDirection(),
--		reference <- thisModule.resolveTemp(input.source, 'reference')
--	)
--}
