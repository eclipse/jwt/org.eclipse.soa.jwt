<?xml version="1.0" encoding="UTF-8"?>
<core:Model xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:application="org.eclipse.jwt/1.3.0/application" xmlns:core="org.eclipse.jwt/1.3.0/core" xmlns:data="org.eclipse.jwt/1.3.0/data" xmlns:processes="org.eclipse.jwt/1.3.0/processes" name="MainPackage" fileversion="1.3.0">
  <elements xsi:type="processes:Activity" name="">
    <nodes xsi:type="processes:Action" name="Consul stock amount" executedBy="//@elements.1" outputs="//@elements.2" referenceEdges="//@elements.0/@referenceEdges.0 //@elements.0/@referenceEdges.1">
      <Location x="182" y="196"/>
    </nodes>
    <references reference="//@elements.2" referenceEdges="//@elements.0/@referenceEdges.1">
      <Location x="378" y="192"/>
    </references>
    <references reference="//@elements.1" referenceEdges="//@elements.0/@referenceEdges.0">
      <Location x="240" y="98"/>
    </references>
    <referenceEdges reference="//@elements.0/@references.1" action="//@elements.0/@nodes.0"/>
    <referenceEdges reference="//@elements.0/@references.0" action="//@elements.0/@nodes.0" direction="out"/>
  </elements>
  <elements xsi:type="application:Application" name="stock" jarArchive="" javaClass="org.eclipse.jwt.demo.reappro.Stock"/>
  <elements xsi:type="data:Data" name="StockAmount"/>
</core:Model>
