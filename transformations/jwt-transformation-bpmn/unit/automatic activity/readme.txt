With sample reflective ecore model editor, you can see special entries in bpmn files, wich fit missing information with jwt model.

In this sample you can see :
- jwt__dataType
- jwt__direction 
- jwt__performedByApplication__name
- jwt__performedByApplication__javaClass
- jwt__performedByApplication__jarFile
- jwt__performedByApplication__icon