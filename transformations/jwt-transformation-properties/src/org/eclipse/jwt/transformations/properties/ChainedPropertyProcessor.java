/**
 * File:    ChainedPropertyProcessor.java
 * Created: 26.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.properties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * @since 0.7
 */
public class ChainedPropertyProcessor extends PropertyProcessor {

	private List<PropertyProcessor> propertyProcessors;

	public ChainedPropertyProcessor(EObject app, EStructuralFeature feature, List<PropertyProcessor> propertyProcessors) {
		super(app, feature);
		this.propertyProcessors = propertyProcessors;
	}

	@Override
	public Map<String, Object> getEntries() {
		Map<String, Object> res = new HashMap<String, Object>();
		for (PropertyProcessor propertyProcessor :  propertyProcessors) {
			Map<String, Object> entries = propertyProcessor.getEntries();
			if (entries != null)
				res.putAll(entries);
		}
		return res;
	}

	@Override
	public Map<String, String> getResources() {
		Map<String, String> res = new HashMap<String, String>();
		for (PropertyProcessor propertyProcessor :  propertyProcessors) {
			Map<String, String> resources = propertyProcessor.getResources();
			if (resources != null)
				res.putAll(resources);
		}
		return res;
	}

}
