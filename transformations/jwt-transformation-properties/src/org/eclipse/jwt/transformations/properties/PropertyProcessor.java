/**
 * File:    PropertyProcessor.java
 * Created: 26.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.properties;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * This class is intended to be concretized by clients of extension point
 * 'org.eclipse.jwt.transformations.properties.customProperties'.
 * 
 * It is responsible of adding key/value pairs and attaching resources to
 * the .properties file extraction.
 * 
 * @author mistria
 * @since 0.7
 */
public abstract class PropertyProcessor {

	protected EObject application;
	protected EStructuralFeature feature;
	
	protected PropertyProcessor(EObject application, EStructuralFeature feature) {
		this.application = application;
		this.feature = feature;
	}
	
	/**
	 * 
	 * @return The key/value entries to add to properties file
	 */
	public abstract Map<String, Object> getEntries();

	/**
	 * 
	 * @return a map whom keys are actual file system path of files
	 * to include as resources, and values are the name to use for 
	 * these files in the BAR archive
	 */
	public abstract Map<String, String> getResources();

}
