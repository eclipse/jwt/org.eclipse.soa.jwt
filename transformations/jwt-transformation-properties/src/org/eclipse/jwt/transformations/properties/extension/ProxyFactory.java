/**
 * File:    ProxyFactory.java
 * Created: 26.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.properties.extension;

import org.eclipse.core.runtime.IConfigurationElement;

/**
 * This interface is intended to be used for extension point precessing
 * in case we want to map to an EMF feature a factory class
 * @author mistria
 *
 * @param <T> The type of the factory
 * @since 1.0.0
 */
public interface ProxyFactory<T> {

	/**
	 * Instanciate a new Factory
	 * @param member
	 * @return
	 */
	public T createProxy(IConfigurationElement member);

}
