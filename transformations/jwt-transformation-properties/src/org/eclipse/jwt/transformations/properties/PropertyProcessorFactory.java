/**
 * File:    PropertyProcessorFactory.java
 * Created: 26.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.properties;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * Factory for {@link PropertyProcessor}
 * @author mistria
 * @since 0.7
 *
 */
public interface PropertyProcessorFactory {

	/**
	 * Creates a new {@link PropertyProcessor}
	 * @param app The {@link EObject} we are currently introspecting
	 * @param feature the current {@link EStructuralFeature} we are looking at on the object
	 * @return a new {@link PropertyProcessor} that will process this object for this feature
	 */
	public PropertyProcessor createPropertyProcessor(EObject app, EStructuralFeature feature);
}
