package org.eclipse.jwt.transformations.properties;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A simple property processor that does not return resources,
 * and return the model element key/value properties as entries
 * @author mistria
 *
 */
class SimplePropertyProcess extends PropertyProcessor {

	public SimplePropertyProcess(EObject app, EStructuralFeature feature) {
		super(app, feature);
	}

	public Map<String, Object> getEntries() {
		HashMap<String, Object> res = new HashMap<String, Object>();
		res.put(feature.getName(), application.eGet(feature));
		return res;
	}

	public Map<String, String> getResources() {
		// TODO Auto-generated method stub
		return null;
	}

}
