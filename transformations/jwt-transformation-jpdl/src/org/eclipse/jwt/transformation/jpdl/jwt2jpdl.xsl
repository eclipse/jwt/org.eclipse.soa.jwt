<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
 * File:    jwt2jpdl.xsl
 * Created: 15.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
-->
<xsl:stylesheet version="2.0"
		xmlns:xmi="http://www.omg.org/XMI"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns:core="org.eclipse.jwt/core"
		xmlns:data="org.eclipse.jwt/data"
		xmlns:organisations="org.eclipse.jwt/organisations"
		xmlns:processes="org.eclipse.jwt/processes"
		xmlns:jwtTools="http://xml.apache.org/xalan/java/org.eclipse.jwt.transformations.xslt.tools.CustomXslFunctions">
    <xsl:output method="xml" encoding="UTF-8" indent="yes" version="1.0" omit-xml-declaration="no"/>
	
	<xsl:variable name="modelRoot" select="/core:Model" />
	
	<xsl:template match="/core:Model">
		<process-definition name="{@name}" xmlns="urn:jbpm.org:jpdl-3.2">
			<xsl:apply-templates/>
		</process-definition>
	</xsl:template>
	
	<xsl:template match="elements[@xsi:type = 'organisations:Role']">
		<swimlane><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute></swimlane>
    </xsl:template>
	
	<xsl:template match="elements[@xsi:type = 'processes:Activity']">
		<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
		<xsl:apply-templates />	
    </xsl:template>
    
    <xsl:template name="commonNodes">
    	<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
    	<xsl:for-each select="jwtTools:getNodes(@out, /core:Model)/edges">
     		<transition name=""><xsl:attribute name="to"><xsl:value-of select="jwtTools:getValueFromPath(@target, $modelRoot)"/>
     		</xsl:attribute></transition>     	</xsl:for-each>
     </xsl:template>
     
    <xsl:template match="nodes[@xsi:type = 'processes:InitialNode']">
		<start-state>
			<xsl:call-template name="commonNodes"/>
     	</start-state>
    </xsl:template>

    <xsl:template match="nodes[@xsi:type = 'processes:FinalNode']">
    	<end-state name="end"/>
    </xsl:template>
    
    <!--
    	Warning: if many templates are concerned about the same element,
    	the last matching template is applied.
    	So this generic "*" template must be placed first.
    -->
    <!-- xsl:template match="*">
		<xsl:element name="{name(.)}">
			<xsl:call-template name="copy-attributes"/>
			<xsl:apply-templates/>
		</xsl:element>
    </xsl:template
-->
    
    <!-- Default action, overriden by next rules when has a role or an applicatin
-->
    <xsl:template match="nodes[@xsi:type = 'processes:Action']">
    	<state>
    		<xsl:call-template name="commonNodes" />
    	</state>
    </xsl:template>
    
    <!-- Human task -->
    <xsl:template match="nodes[@xsi:type = 'processes:Action' and @performedBy != '']">
    	<task-node>
    		<xsl:call-template name="commonNodes" />
    		<task>
    			<swimlane><xsl:attribute name="name"><xsl:value-of select="jwtTools:getValueFromPath(@performedBy, /core:Model)"/></xsl:attribute></swimlane>
    		</task>
    	</task-node>
    </xsl:template>
    
    <!-- Automatic task -->
    <xsl:template match="nodes[@xsi:type = 'processes:Action' and @executedBy != '']">
    	<node>
    		<xsl:call-template name="commonNodes" />
    		<action class="TODO"/> <!-- Same logic as Hooks, TaskEngineFrameworg... -->
    	</node>
    </xsl:template>
    
    <xsl:template match="nodes[@xsi:type = 'processes:ForkNode']">
    	<fork>
    		<xsl:call-template name="commonNodes" />
    	</fork>
    </xsl:template>
        
	   <xsl:template match="nodes[@xsi:type = 'processes:DecisionNode']">
	   	<decision>
	   		<xsl:call-template name="commonNodes" />
	   	</decision>
	   </xsl:template>
	   
	<xsl:template match="nodes[@xsi:type = 'processes:JoinNode']">
	   	<join>
	   		<xsl:call-template name="commonNodes" />
	   	</join>
	   </xsl:template>
    
    <xsl:template match="nodes[@xsi:type = 'processes:JoinNode']">
	   	<process-state>
	   		<xsl:call-template name="commonNodes" />
	   		<sub-process>
	   			<xsl:attribute name="name"><xsl:value-of select="jwtTools:getValueFromPath(@linksto, /core:Model)"/></xsl:attribute>
	   		</sub-process>
	   	</process-state>
	   </xsl:template>
    
</xsl:stylesheet>
