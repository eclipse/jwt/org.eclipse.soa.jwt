<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
 * File:    jpdl2jwt.xsl
 * Created: 22.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
-->
<xsl:stylesheet version="2.0"
		xmlns:xmi="http://www.omg.org/XMI"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns:core="org.eclipse.jwt/core"
		xmlns:jwtTools="http://xml.apache.org/xalan/java/org.eclipse.jwt.transformations.xslt.tools.CustomXslFunctions">
    <xsl:output method="xml" encoding="UTF-8" indent="yes" version="1.0" omit-xml-declaration="no"/>
    
    <xsl:template match="/">
    	<core:Model name="{/process-definition/@name}">
    		<xsl:apply-template />
    	</core:Model>
    </xsl:template>
    
	<xsl:template match="process-definition/swimlane">
		<elements xsi:type="organisations:Role" name="{@name}"/>
	</xsl:template>
    
	<xsl:template match="process-definition">
		<elements xsi:type="processes:Activity" name="{@name}">
    		<xsl:apply-templates />
    	</elements>
	</xsl:template>
	
	<xsl:template match="start-state">
		<nodes xsi:type="processes:InitialNode" name="{@name}">
			<xsl:apply-templates />
		</nodes>
	</xsl:template>

	<xsl:template match="end-state">
		<nodes xsi:type="processes:FinalNode" name="{@name}">
			<xsl:apply-templates />
		</nodes>
	</xsl:template>
	
	<xsl:template match="state">
		<nodes xsi:type="processes:Action" name="{@name}">
			<xsl:apply-templates />
		</nodes>
	</xsl:template>

	<xsl:template match="node">
		<nodes xsi:type="processes:Action" name="{@name}">
			<xsl:apply-templates />
		</nodes>
	</xsl:template>
	
	<xsl:template match="task-node">
		<nodes xsi:type="processes:Action" name="{@name}">
			<xsl:apply-templates />
		</nodes>
	</xsl:template>	
	
	<!-- xsl:template match="swimlane[local-name(..) != 'process-definition']">
	</xsl:template-->
 </xsl:stylesheet>