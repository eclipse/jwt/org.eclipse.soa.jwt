/**
 * File:    JwtToJpdlTransformation.java
 * Created: 15.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformation.jpdl;

import java.io.IOException;

import org.eclipse.jwt.transformations.api.TransformationException;
import org.eclipse.jwt.transformations.api.TransformationService;
import org.eclipse.jwt.transformations.xslt.tools.XSLTProcessor;

/**
 * This {@link TransformationService} implements a JWT to jPDL transformation,
 * using XSLT
 * @author mistria
 * @since 0.7
 */
public class JwtToJpdlTransformation extends TransformationService {

	@Override
	public void transform(String arg0, String arg1) throws IOException, TransformationException {
		XSLTProcessor xsltproc = new XSLTProcessor();
		xsltproc.processXSLT(arg0, arg1, getClass().getResourceAsStream("/org/eclipse/jwt/transformation/jpdl/jwt2jpdl.xsl"));
	}

}
