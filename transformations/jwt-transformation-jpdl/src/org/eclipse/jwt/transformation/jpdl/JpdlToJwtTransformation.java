package org.eclipse.jwt.transformation.jpdl;

import java.io.IOException;

import org.eclipse.jwt.transformations.api.TransformationException;
import org.eclipse.jwt.transformations.api.TransformationService;
import org.eclipse.jwt.transformations.xslt.tools.XSLTProcessor;

/**
 * This {@link TransformationService} should implement an jPDL to JWT
 * Transformation, but it currently does nothing
 * @author mistria
 * @since 0.7
 *
 */
public class JpdlToJwtTransformation extends TransformationService {

	@Override
	public void transform(String inFilePath, String outFilePath) throws IOException, TransformationException {
		XSLTProcessor xsltproc = new XSLTProcessor();
		xsltproc.processXSLT(inFilePath, outFilePath, getClass().getResourceAsStream("/org/eclipse/jwt/transformation/jpdl/jpdl2jwtl.xsl"));

	}

}
