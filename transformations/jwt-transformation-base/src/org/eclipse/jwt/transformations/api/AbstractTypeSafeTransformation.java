/**
 * File:    AbstractTypeSafeTransformation.java
 * Created: 29.06.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.api;

import java.io.IOException;

import org.eclipse.jwt.transformations.api.io.IODefinition.IOHandle;

/**
 * 
 * An abstract implementation of {@link Transformation} defining a type-safe
 * {@link #transform(Object, Object)} method.
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 * 
 * @param <InputType>
 *            Expected type of transformation input object.
 * @param <OutputType>
 *            Expected type of transformation output object.
 */
public abstract class AbstractTypeSafeTransformation<InputType, OutputType>
		extends AbstractTransformation {
	@Override
	public final void transform(Object rawInput, Object rawOutput)
			throws IllegalArgumentException, IOException,
			TransformationException {
		InputType input;
		OutputType output;

		try {
			input = getInput(rawInput);
			output = getOutput(rawOutput);
		} catch (IOException e) {
			throw e;
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (ClassCastException e) {
			throw new IllegalArgumentException(e);
		}

		doTransform(input, output);
	}

	/**
	 * A type-safe implementation of {@link #transform(Object, Object)}.
	 * 
	 * @see #transform(Object, Object)
	 * @see #getInput(Object)
	 * @see #getOutput(Object)
	 */
	protected abstract void doTransform(InputType input, OutputType output)
			throws IOException, TransformationException;

	/**
	 * Gets an input of the required type from an input reference provided by an
	 * {@link IOHandle}.
	 * <p>
	 * As the "input reference is a raw {@link Object} whose concrete type is to
	 * be inspected, this method may handle different type of parameters.
	 * 
	 * @param rawInput
	 *            An input reference whose type has to be inspected. This object
	 *            is guaranteed to have been obtained through the
	 *            {@link IOHandle#getValue()} method of an {@link IOHandle}
	 *            provided by a {@link #getCompatibleInputDefinitions()
	 *            compatible IO definition}.
	 * @return The converted input which will be passed as an argument to
	 *         {@link Transformation#doTransform(Object, Object)}.
	 * @throws IOException
	 * @throws IllegalArgumentException
	 *             If the input is deemed invalid.
	 * @throws ClassCastException
	 *             If some cast fails. These exceptions are handled by
	 *             {@link AbstractTransformation}.
	 */
	protected abstract InputType getInput(Object rawInput) throws IOException,
			IllegalArgumentException, ClassCastException;

	/**
	 * Same principle as {@link #getInput(Object)}, but applied to output.
	 * 
	 * @see #getInput(IOHandle)
	 */
	protected abstract OutputType getOutput(Object rawOutput)
			throws IOException, IllegalArgumentException, ClassCastException;
}
