/**
 * File:    Transformation.java
 * Created: 29.06.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.api;

import java.io.IOException;
import java.util.List;

import org.eclipse.jwt.transformations.api.io.IODefinition;
import org.eclipse.jwt.transformations.api.io.IODefinition.IOHandle;

/**
 * This interface is used by the extension point
 * "org.eclipse.jwt.transformations".
 * 
 * @noextend This interface generally not be implemented directly. Extend
 *           {@link AbstractTransformation} or
 *           {@link AbstractTypeSafeTransformation} instead.
 * 
 * @see AbstractTransformation
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 */
public interface Transformation {

	/**
	 * @return A short, human-readable name identifying this transformation.
	 */
	public String getName();

	/**
	 * @return A short paragraph describing the purpose of this transformation.
	 */
	public String getDescription();

	/**
	 * @return {@link Type#IMPORT} or {@link Type#EXPORT}, depending on the
	 *         category this transformation should be made available under.
	 */
	public Type getType();

	/**
	 * @return A read-only list of the {@link IODefinition}s providing a
	 *         compatible input reference to this transformation.
	 *         <p>
	 *         This must be non-null and non-empty, with non-null elements.
	 */
	public List<IODefinition> getCompatibleInputDefinitions();

	/**
	 * @return A read-only list of the {@link IODefinition}s providing a
	 *         compatible output reference to this transformation.
	 *         <p>
	 *         This must be non-null and non-empty, with non-null elements.
	 */
	public List<IODefinition> getCompatibleOutputDefinitions();

	/**
	 * Executes the transformation.
	 * 
	 * @param input
	 *            An input reference obtained through the
	 *            {@link IOHandle#getValue()} method of an {@link IOHandle}
	 *            provided by a {@link #getCompatibleInputDefinitions()
	 *            compatible input definition}.
	 * @param output
	 *            An output reference obtained through the
	 *            {@link IOHandle#getValue()} method of an {@link IOHandle}
	 *            provided by a {@link #getCompatibleOutputDefinitions()
	 *            compatible output definition}.
	 * @throws IllegalArgumentException
	 *             if <code>input</code> or <code>output</code> does not meet
	 *             the implementation's requirements.
	 * @throws IOException
	 *             if the implementation encountered an error while trying to
	 *             retrieve the input or output, or while trying to read or
	 *             write to them.
	 * @throws TransformationException
	 *             if the actual transformation operation failed for some
	 *             reason.
	 */
	public void transform(Object input, Object output)
			throws IllegalArgumentException, IOException,
			TransformationException;

	public enum Type {
		IMPORT("import"), //$NON-NLS-1$
		EXPORT("export"); //$NON-NLS-1$

		private String typeString;

		private Type(String typeString) {
			this.typeString = typeString;
		}

		@Override
		public String toString() {
			return typeString;
		}

		public static Type fromString(String typeString) {
			for (Type value : values()) {
				if (value.toString().equals(typeString)) {
					return value;
				}
			}
			return null;
		}
	}
}
