/**
 * File:    TransformationService.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - deprecation (added new, more generic {@link Transformation} interface)
 *******************************************************************************/
package org.eclipse.jwt.transformations.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jwt.transformations.api.io.AbstractIODefinition;
import org.eclipse.jwt.transformations.api.io.IODefinition;

/**
 * @deprecated Implement {@link Transformation} or (more commonly) extend
 *             {@link AbstractTransformation} instead
 * @author mistria
 * @since 0.6
 */
@Deprecated
public abstract class TransformationService extends
		AbstractTypeSafeTransformation<String, String> {
	private static final String JWT_DEFAULT_IODEFS_PREFIX = "org.eclipse.jwt.transformations.iodefinitions.default"; //$NON-NLS-1$
	private static final String DEFAULT_EXPORT_INPUT_WORKSPACE = JWT_DEFAULT_IODEFS_PREFIX
			+ ".export.input.workspace"; //$NON-NLS-1$
	private static final String DEFAULT_EXPORT_OUTPUT_WORKSPACE = JWT_DEFAULT_IODEFS_PREFIX
			+ ".export.output.workspace"; //$NON-NLS-1$
	private static final String DEFAULT_IMPORT_INPUT_WORKSPACE = JWT_DEFAULT_IODEFS_PREFIX
			+ ".import.input.workspace"; //$NON-NLS-1$
	private static final String DEFAULT_IMPORT_OUTPUT_WORKSPACE = JWT_DEFAULT_IODEFS_PREFIX
			+ ".import.output.workspace"; //$NON-NLS-1$
	private static final String DEFAULT_EXPORT_INPUT_LOCAL = JWT_DEFAULT_IODEFS_PREFIX
			+ ".export.input.local"; //$NON-NLS-1$
	private static final String DEFAULT_IMPORT_INPUT_LOCAL = JWT_DEFAULT_IODEFS_PREFIX
			+ ".import.input.local"; //$NON-NLS-1$
	private static final String DEFAULT_EXPORT_OUTPUT_LOCAL = JWT_DEFAULT_IODEFS_PREFIX
			+ ".export.output.local"; //$NON-NLS-1$
	private static final String DEFAULT_IMPORT_OUTPUT_LOCAL = JWT_DEFAULT_IODEFS_PREFIX
			+ ".import.output.local"; //$NON-NLS-1$

	@Deprecated
	public abstract void transform(String inFilePath, String outFilePath)
			throws IOException, TransformationException;

	@Deprecated
	public void setType(String type) {
		setType(Type.fromString(type));
	}

	@Deprecated
	public String getTye() {
		return getType().toString();
	}

	private static String extractPath(Object file) {
		if (file instanceof File) {
			return ((File) file).getPath();
		} else if (file instanceof IFile) {
			return ((IFile) file).getRawLocationURI().getPath();
		}

		return (String) file;
	}

	@Override
	protected final String getInput(Object rawInput) throws ClassCastException {
		return extractPath(rawInput);
	}

	@Override
	protected final String getOutput(Object rawOutput)
			throws ClassCastException, IOException {
		String path = extractPath(rawOutput);

		File destFile = new File(path);
		if (!destFile.exists()) {
			destFile.createNewFile();
		}

		return path;
	}

	@Override
	protected final void doTransform(String input, String output)
			throws TransformationException, IOException {
		transform(input, output);
	}

	private static IODefinition createIODefinition(String extensionID) {
		try {
			return AbstractIODefinition.createIODefinition(extensionID);
		} catch (CoreException e) {
			// Should not happen
			throw new RuntimeException(e);
		} catch (IOException e) {
			// Should not happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public final List<IODefinition> getCompatibleInputDefinitions() {
		List<IODefinition> result = super.getCompatibleInputDefinitions();
		if (result.isEmpty()) {
			result = new ArrayList<IODefinition>();
			switch (getType()) {
			case EXPORT:
				result.add(createIODefinition(DEFAULT_EXPORT_INPUT_LOCAL));
				result.add(createIODefinition(DEFAULT_EXPORT_INPUT_WORKSPACE));
				break;
			case IMPORT:
				result.add(createIODefinition(DEFAULT_IMPORT_INPUT_LOCAL));
				result.add(createIODefinition(DEFAULT_IMPORT_INPUT_WORKSPACE));
				break;
			}

		}
		return result;
	}

	@Override
	public final List<IODefinition> getCompatibleOutputDefinitions() {
		List<IODefinition> result = super.getCompatibleInputDefinitions();
		if (result.isEmpty()) {
			result = new ArrayList<IODefinition>();
			switch (getType()) {
			case EXPORT:
				result.add(createIODefinition(DEFAULT_EXPORT_OUTPUT_LOCAL));
				result.add(createIODefinition(DEFAULT_EXPORT_OUTPUT_WORKSPACE));
				break;
			case IMPORT:
				result.add(createIODefinition(DEFAULT_IMPORT_OUTPUT_LOCAL));
				result.add(createIODefinition(DEFAULT_IMPORT_OUTPUT_WORKSPACE));
				break;
			}
		}
		return result;
	}
}
