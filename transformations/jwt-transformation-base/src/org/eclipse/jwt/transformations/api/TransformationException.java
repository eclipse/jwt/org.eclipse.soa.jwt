/**
 * File:    TransformationException.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/

package org.eclipse.jwt.transformations.api;

/**
 * 
 * @author mistria
 * @since 0.6
 */
@SuppressWarnings("serial")
public class TransformationException extends Exception {

	public TransformationException(String message, Throwable cause) {
		super(message, cause);
		//this.setStackTrace(cause.getStackTrace());
	}

	public TransformationException(String message) {
		super(message);
	}

	
	
}
