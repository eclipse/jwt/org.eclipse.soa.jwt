/**
 * File:    AbstractIODefinition.java
 * Created: 29.06.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.api.io;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jwt.transformations.internal.init.Activator;
import org.eclipse.jwt.transformations.io.WorkflowEditorInstanceIODefinition;

/**
 * An abstract implementation of {@link IODefinition} handling the loading as an
 * extension.
 * 
 * @see WorkflowEditorInstanceIODefinition
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 */
public abstract class AbstractIODefinition implements IODefinition,
		IExecutableExtension {
	private static final String IODEFINITION_EXTENSION_POINT = "org.eclipse.jwt.transformations.iodefinitions"; //$NON-NLS-1$

	public static final String REFERENCE_CONFIGURATION_ELEMENT = "ioreference"; //$NON-NLS-1$
	public static final String DEFINITION_CONFIGURATION_ELEMENT = "iodefinition"; //$NON-NLS-1$
	public static final String PARAMETER_CONFIGURATION_ELEMENT = "parameter"; //$NON-NLS-1$

	private static final String REFERENCE_ID_CONFIGURATION_ATTRIBUTE = "id"; //$NON-NLS-1$

	public static final String DEFINITION_CLASS_CONFIGURATION_ATTRIBUTE = "class"; //$NON-NLS-1$
	private static final String DEFINITION_LABEL_CONFIGURATION_ATTRIBUTE = "label"; //$NON-NLS-1$

	private static final String PARAMETER_NAME_CONFIGURATION_ATTRIBUTE = "name"; //$NON-NLS-1$
	private static final String PARAMETER_VALUE_CONFIGURATION_ATTRIBUTE = "value"; //$NON-NLS-1$

	private String label;
	private Map<String, String> parameters;

	private static IExtensionPoint getExtensionPoint() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		return registry.getExtensionPoint(IODEFINITION_EXTENSION_POINT);
	}

	/**
	 * Creates an IODefinition from its extension ID.
	 * 
	 * @see #createIODefinition(IConfigurationElement)
	 * @param extensionID
	 *            The of the extension in which the IO was defined.
	 * @return An IODefinition (never null).
	 * @throws CoreException
	 * @throws IOException
	 *             If the extension with given ID wasn't found, or if the
	 *             IODefinition could not be created.
	 */
	public static IODefinition createIODefinition(String extensionID)
			throws CoreException, IOException {
		IExtensionPoint extensionPoint = getExtensionPoint();

		IExtension extension = extensionPoint.getExtension(extensionID);
		if (extension == null) {
			throw new IOException("An IODefinition could not be created"); //$NON-NLS-1$
		}

		return createIODefinition(extension.getConfigurationElements()[0]);
	}

	/**
	 * Creates an IODefinition from an
	 * {@value #DEFINITION_CONFIGURATION_ELEMENT} or an
	 * {@value #REFERENCE_CONFIGURATION_ELEMENT} configuration element.
	 * 
	 * @param config
	 * @return An IODefinition (never null).
	 * @throws CoreException
	 * @throws IOException
	 */
	public static IODefinition createIODefinition(IConfigurationElement config)
			throws CoreException, IOException {
		if (REFERENCE_CONFIGURATION_ELEMENT.equals(config.getName())) {
			// Resolve indirection
			return createIODefinition(config
					.getAttribute(REFERENCE_ID_CONFIGURATION_ATTRIBUTE));
		}

		Object created = config
				.createExecutableExtension(DEFINITION_CLASS_CONFIGURATION_ATTRIBUTE);

		if (created == null) {
			throw new IOException("An IODefinition could not be created"); //$NON-NLS-1$
		}
		if (!(created instanceof IODefinition)) {
			throw new IOException(
					"An IODefinition was found to have incorrect type"); //$NON-NLS-1$
		}

		return (IODefinition) created;
	}

	private static class InvalidIODefinitionException extends CoreException {
		private static final long serialVersionUID = -8978517553154150730L;

		private static String ERROR_FORMAT = "A registered IODefinition has invalid label [label:%s]"; //$NON-NLS-1$

		public InvalidIODefinitionException(AbstractIODefinition ioDefinition) {
			super(new Status(IStatus.ERROR, Activator.getDefault().getBundle()
					.getSymbolicName(), String.format(ERROR_FORMAT,
					ioDefinition.label)));
		}
	}

	public final void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
		label = config.getAttribute(DEFINITION_LABEL_CONFIGURATION_ATTRIBUTE);
		if (label == null) {
			throw new InvalidIODefinitionException(this);
		}

		parameters = new HashMap<String, String>();
		for (IConfigurationElement parameter : config
				.getChildren(PARAMETER_CONFIGURATION_ELEMENT)) {
			String key = parameter
					.getAttribute(PARAMETER_NAME_CONFIGURATION_ATTRIBUTE);
			String value = parameter
					.getAttribute(PARAMETER_VALUE_CONFIGURATION_ATTRIBUTE);
			parameters.put(key, value);
		}
	}

	public final String getLabel() {
		return label;
	}

	/**
	 * @param name
	 * @return The value of parameter <code>name</code>, or null if not set.
	 */
	protected final String getParameter(String name) {
		return parameters.get(name);
	}

	/**
	 * Sets parameter <code>name</code> to <code>value</code>, or unsets it if
	 * <code>value</code> is null.
	 * 
	 * @param name
	 *            The name of the parameter to be set.
	 * @param value
	 *            The value to set, or null if the parameter must be unset.
	 */
	public final void setParameter(String name, String value) {
		if (value == null) {
			parameters.remove(name);
		} else {
			parameters.put(name, value);
		}
	}
}
