/**
 * File:    AbstractTransformation.java
 * Created: 29.06.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jwt.transformations.api.io.AbstractIODefinition;
import org.eclipse.jwt.transformations.api.io.IODefinition;
import org.eclipse.jwt.transformations.internal.init.Activator;

/**
 * An abstract implementation of {@link Transformation} handling the loading as
 * an extension.
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 */
public abstract class AbstractTransformation implements Transformation,
		IExecutableExtension {
	private static final String TRANSFORMATION_EXTENSION_POINT = "org.eclipse.jwt.transformations"; //$NON-NLS-1$

	private static final String CLASS_CONFIGURATION_ATTRIBUTE = "class"; //$NON-NLS-1$
	private static final String NAME_CONFIGURATION_ATTRIBUTE = "name"; //$NON-NLS-1$
	private static final String DESCRIPTION_CONFIGURATION_ATTRIBUTE = "description"; //$NON-NLS-1$
	private static final String TYPE_CONFIGURATION_ATTRIBUTE = "type"; //$NON-NLS-1$
	private static final String INPUTS_CONFIGURATION_CHILD = "inputs"; //$NON-NLS-1$
	private static final String OUTPUTS_CONFIGURATION_CHILD = "outputs"; //$NON-NLS-1$

	private String name;
	private String description;
	private Type type;
	private final List<IODefinition> inputDefinitions = new ArrayList<IODefinition>();
	private final List<IODefinition> outputDefinitions = new ArrayList<IODefinition>();

	private static class InvalidTransformationException extends CoreException {
		private static final long serialVersionUID = -8978517553154150730L;

		private static String ERROR_FORMAT = "A registered Transformation is invalid [name:%s]"; //$NON-NLS-1$

		public InvalidTransformationException(
				AbstractTransformation transformation, Throwable cause) {
			super(new Status(IStatus.ERROR, Activator.getDefault().getBundle()
					.getSymbolicName(), String.format(ERROR_FORMAT,
					transformation.name), cause));
		}
	}

	public static IExtensionPoint getExtensionPoint() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		return registry.getExtensionPoint(TRANSFORMATION_EXTENSION_POINT);
	}

	/**
	 * Creates a transformation from a "transformation" configuration element.
	 * 
	 * @param config
	 * @return
	 * @throws CoreException
	 * @throws IOException
	 */
	public static Transformation createTransformation(
			IConfigurationElement config) throws CoreException, IOException {
		Object created = config
				.createExecutableExtension(CLASS_CONFIGURATION_ATTRIBUTE);

		if (created == null) {
			throw new IOException("A Transformation could not be created"); //$NON-NLS-1$
		}
		if (!(created instanceof Transformation)) {
			throw new IOException(
					"An Transformation was found to have incorrect type"); //$NON-NLS-1$
		}

		return (Transformation) created;
	}

	public abstract void transform(Object input, Object output)
			throws IllegalArgumentException, IOException,
			TransformationException;

	/**
	 * This maps to the {@value #NAME_CONFIGURATION_ATTRIBUTE} configuration
	 * attribute of the <code>transformations</code> extension point.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * This maps to the {@value #DESCRIPTION_CONFIGURATION_ATTRIBUTE}
	 * configuration attribute of the <code>transformations</code> extension
	 * point.
	 */
	public final String getDescription() {
		return description;
	}

	/**
	 * @deprecated Provided as a non-private member only for use in (deprecated)
	 *             {@link TransformationService}.
	 */
	@Deprecated
	protected void setDescription(String description) {
		this.description = description;
	}

	/**
	 * This maps to the {@value #TYPE_CONFIGURATION_ATTRIBUTE} configuration
	 * attribute of the <code>transformations</code> extension point.
	 */
	public final Type getType() {
		return type;
	}

	/**
	 * @deprecated Provided as a non-private member only for use in (deprecated)
	 *             {@link TransformationService}.
	 */
	@Deprecated
	protected void setType(Type type) {
		this.type = type;
	}

	public final void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
		name = config.getAttribute(NAME_CONFIGURATION_ATTRIBUTE);
		description = config.getAttribute(DESCRIPTION_CONFIGURATION_ATTRIBUTE);
		type = Type.fromString(config
				.getAttribute(TYPE_CONFIGURATION_ATTRIBUTE));

		if (config.getChildren(INPUTS_CONFIGURATION_CHILD).length > 0) {
			IConfigurationElement inputs = config
					.getChildren(INPUTS_CONFIGURATION_CHILD)[0];
			for (IConfigurationElement input : inputs.getChildren()) {
				inputDefinitions.add(extractIO(input));
			}
		}

		if (config.getChildren(OUTPUTS_CONFIGURATION_CHILD).length > 0) {
			IConfigurationElement outputs = config
					.getChildren(OUTPUTS_CONFIGURATION_CHILD)[0];
			for (IConfigurationElement output : outputs.getChildren()) {
				outputDefinitions.add(extractIO(output));
			}
		}
	}

	private IODefinition extractIO(IConfigurationElement element)
			throws CoreException {
		try {
			return AbstractIODefinition.createIODefinition(element);
		} catch (IOException e) {
			throw new InvalidTransformationException(this, e);
		}
	}

	/**
	 * This maps to the {@value #INPUTS_CONFIGURATION_CHILD} configuration
	 * elements of the <code>transformations</code> extension point.
	 * <p>
	 * It can be safely overridden by subclasses (for example to provide dynamic
	 * {@link IODefinition}s), so long as they include in the returned
	 * collection the elements returned by this method.
	 */
	public List<IODefinition> getCompatibleInputDefinitions() {
		return Collections.unmodifiableList(inputDefinitions);
	}

	/**
	 * This maps to the {@value #OUTPUTS_CONFIGURATION_CHILD} configuration
	 * elements of the <code>transformations</code> extension point.
	 * <p>
	 * It can be safely overridden by subclasses (for example to provide dynamic
	 * {@link IODefinition}s), so long as they include in the returned
	 * collection the elements returned by this method.
	 */
	public List<IODefinition> getCompatibleOutputDefinitions() {
		return Collections.unmodifiableList(outputDefinitions);
	}

}
