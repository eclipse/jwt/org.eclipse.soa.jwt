/**
 * File:    IODefinition.java
 * Created: 29.06.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.api.io;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.transformations.api.Transformation;
import org.eclipse.jwt.transformations.internal.wizard.TransformationWizard;
import org.eclipse.jwt.transformations.io.WorkflowEditorInstanceIODefinition;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * Objects of this type define a way for the user of referencing an
 * input/output. This includes:
 * <ul>
 * <li>a label ({@link #getLabel()})
 * <li>an SWT control factory (
 * {@link #createIOControl(Composite, ContentChangeListener, IStructuredSelection)}
 * ).
 * <li>a definition of the reference itself, {@link IOHandle}.
 * </ul>
 * <p>
 * Beware that while this interface is immutable, classes implementing it may
 * offer methods to mutate objects (for instance, setters like
 * {@link AbstractIODefinition#setParameter(String, String)}).
 * 
 * @noextend This interface should generally not be implemented directly. Extend
 *           {@link AbstractIODefinition} instead.
 * 
 * @see AbstractIODefinition
 * @see WorkflowEditorInstanceIODefinition
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 */
public interface IODefinition {

	/**
	 * @return A label allowing a human user to identify this IODefinition.
	 */
	public String getLabel();

	/**
	 * Creates a control that lets the user parameterize an input/output. An
	 * example would be a Text control designed to accept the local path of a
	 * file.
	 * 
	 * @param parent
	 *            The parent widget to be populated.
	 * @param pageListener
	 *            A listener that must be notified whenever the nested controls
	 *            change in a way that may change the value returned by
	 *            {@link IOHandle#getValue()} or
	 *            {@link IOHandle#hasAllRequiredInformation()}.
	 * @param currentSelection
	 *            The {@link IStructuredSelection selection} passed to the
	 *            import or export {@link TransformationWizard wizard}. The
	 *            first item, in particular, may be interesting as it may allow
	 *            implementations to extract the default content of the created
	 *            control.
	 * @return An {@link IOHandle} which allows to access the created control.
	 */
	public IOHandle createIOControl(Composite parent,
			ContentChangeListener pageListener,
			IStructuredSelection currentSelection);

	/**
	 * An IOHandle is a way, for clients, to manipulate a parameterized
	 * IODefinition in a way that is independent from the underlying control
	 * implementation.
	 * <p>
	 * In particular, it allows to {@link #getValue() access a value}
	 * representing the input/output currently referenced by the control. This
	 * value will be passed as a parameter to the
	 * {@link Transformation#transform(Object, Object)} method.
	 * 
	 * @see WorkflowEditorInstanceIODefinition
	 */
	public static interface IOHandle {

		/**
		 * This method is the point of the whole class: getting a value that
		 * references an input/output (i.e. a file path, an URL, an
		 * {@link InputStream}, ...).
		 * <p>
		 * Implementations will likely extract this value from the
		 * {@link #getControl() underlying control}. They should refine the
		 * return type for documentation purposes.
		 * <p>
		 * The behavior of this method when {@link #hasAllRequiredInformation()}
		 * returns <code>false</code> is undefined: it may throw a
		 * {@link RuntimeException}, or return garbage data, or return null, or
		 * whatever.
		 * 
		 * @return A value that will be passed to a {@link Transformation} as an
		 *         argument to the
		 *         {@link Transformation#transform(Object, Object)} method.
		 */
		public Object getValue();

		/**
		 * @return The control created by the
		 *         {@link IODefinition#createIOControl(Composite, ContentChangeListener, IStructuredSelection)}
		 *         method.
		 */
		public Control getControl();

		/**
		 * @return True if getValue() can safely be called, false otherwise.
		 */
		public boolean hasAllRequiredInformation();

		/**
		 * Performs necessary actions after a successful transformation.
		 * <p>
		 * The nature of these "necessary actions" is up to the implementation:
		 * this method can safely be implemented by an empty block.
		 */
		public void afterTransformation() throws CoreException, IOException;
	}

}
