/**
 * File:    TransformationWizardPage.java
 * Created: 12.12.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - refactored to avoid duplicated code and enable more generic transformations.
 *******************************************************************************/
package org.eclipse.jwt.transformations.internal.wizard;

import java.util.Collection;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.jwt.transformations.api.Transformation;
import org.eclipse.jwt.transformations.api.Transformation.Type;
import org.eclipse.jwt.transformations.api.io.IODefinition;
import org.eclipse.jwt.transformations.api.io.ContentChangeListener;
import org.eclipse.jwt.transformations.internal.TransformationsMessages;
import org.eclipse.jwt.transformations.internal.init.TransformationRegistry;
import org.eclipse.jwt.transformations.widgets.internal.IOGroup;
import org.eclipse.jwt.transformations.widgets.internal.TransformationGroup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;

public class TransformationWizardPage extends WizardPage {
	private static final String JWT_TRANSFORMATION_BASE_PLUGIN_ID = "org.eclipse.jwt.transformations.baseTransformation"; //$NON-NLS-1$

	private final Type transformationType;
	private final IStructuredSelection selection;
	private final String transformationComboText;
	private final String inputGroupText;
	private final String outputGroupText;

	private TransformationGroup transformationGroup;
	private IOGroup inputHandle;
	private IOGroup outputHandle;

	public TransformationWizardPage(Type transformationType,
			IStructuredSelection selection, String pageName, String pageTitle,
			String transformationComboText, String pageDescription,
			String inputGroupText, String outputGroupText) {
		super(pageName);
		this.setTitle(pageTitle);
		this.setDescription(pageDescription);

		this.transformationType = transformationType;
		this.selection = selection;
		this.transformationComboText = transformationComboText;
		this.inputGroupText = inputGroupText;
		this.outputGroupText = outputGroupText;
	}

	public void createControl(Composite parent) {
		if (this.getControl() == null) {
			Composite mainControl = new Composite(parent, SWT.NONE);
			mainControl.setLayoutData(new GridData(GridData.FILL_BOTH));

			mainControl.setLayout(new GridLayout());

			transformationGroup = new TransformationGroup(mainControl,
					new TransformationChangedListener(), TransformationRegistry
							.getInstance().getTransformationsByType(
									transformationType),
					transformationComboText);
			transformationGroup.getControl().setLayoutData(
					new GridData(GridData.FILL_BOTH));

			inputHandle = new IOGroup(mainControl, new CompletedPageListener(),
					selection, inputGroupText,
					TransformationsMessages.inputTypeComboText) {
				@Override
				protected Collection<IODefinition> extractIODefinitions(
						Transformation transformation) {
					return transformation.getCompatibleInputDefinitions();
				}
			};
			inputHandle.getControl().setLayoutData(
					new GridData(GridData.FILL_BOTH));

			outputHandle = new IOGroup(mainControl,
					new CompletedPageListener(), selection, outputGroupText,
					TransformationsMessages.outputTypeComboText) {
				@Override
				protected Collection<IODefinition> extractIODefinitions(
						Transformation transformation) {
					return transformation.getCompatibleOutputDefinitions();
				}
			};
			outputHandle.getControl().setLayoutData(
					new GridData(GridData.FILL_BOTH));

			setControl(mainControl);
			setPageComplete(hasAllRequiredInfo());
		}
	}

	public boolean finish() {
		boolean cancelled = PlatformUI.getWorkbench().saveAllEditors(true);
		if (!cancelled)
			return false;

		try {
			Transformation transformation = transformationGroup
					.getTransformation();

			transformation.transform(inputHandle.getValue(),
					outputHandle.getValue());

			inputHandle.afterTransformation();
			outputHandle.afterTransformation();

		} catch (Exception e1) {
			Status status = new Status(IStatus.ERROR,
					JWT_TRANSFORMATION_BASE_PLUGIN_ID, e1.getMessage(),
					e1.getCause());
			ErrorDialog
					.openError(
							getShell(),
							TransformationsMessages.transformationError_title,
							TransformationsMessages.transformationError_details,
							status);
			return false;
		}

		return true;
	}

	private void updateSelectedTransformation() {
		Transformation selectedTransformation = transformationGroup
				.getTransformation();
		inputHandle.setTransformation(selectedTransformation);
		outputHandle.setTransformation(selectedTransformation);
	}

	private class CompletedPageListener implements ContentChangeListener {
		public void contentChanged() {
			setPageComplete(hasAllRequiredInfo());
		}
	}

	private class TransformationChangedListener extends CompletedPageListener {
		@Override
		public void contentChanged() {
			updateSelectedTransformation();
			super.contentChanged();
		}
	}

	private boolean hasAllRequiredInfo() {
		return transformationGroup.getTransformation() != null
				&& inputHandle.hasAllRequiredInformation()
				&& outputHandle.hasAllRequiredInformation();
	}

}
