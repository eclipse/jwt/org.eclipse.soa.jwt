/**
 * File:    ExportHandler.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/

package org.eclipse.jwt.transformations.internal.actions;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.ui.IWorkbenchPart;



/**
 * does only export for now
 * @author mistria
 *
 */
public class ExportHandler extends WEActionHandler {

	public ExportHandler() {
		// requires open WEEditor
		super(true);
		setEnabled(true);
	}

	/**
	 * Refresh enabled state if selection has changed.
	 */
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ExportAction action = new ExportAction();
		action.run();

		return null;
	}
}
