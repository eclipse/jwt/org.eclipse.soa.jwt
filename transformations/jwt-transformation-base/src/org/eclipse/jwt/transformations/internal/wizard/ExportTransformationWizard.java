/**
 * File:    ExportTransformationWizard.java
 * Created: 26.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - refactored to avoid duplicated code
 *******************************************************************************/
package org.eclipse.jwt.transformations.internal.wizard;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.transformations.api.Transformation.Type;
import org.eclipse.jwt.transformations.internal.TransformationsMessages;

public class ExportTransformationWizard extends TransformationWizard {

	@Override
	protected TransformationWizardPage createMainPage(
			IStructuredSelection selectionAtInitTime) {
		return new TransformationWizardPage(Type.EXPORT, selectionAtInitTime,
				TransformationsMessages.exportWizard_pageName,
				TransformationsMessages.exportWizard_pageTitle,
				TransformationsMessages.chooseExportTransformation,
				TransformationsMessages.exportWizard_pageDescription,
				TransformationsMessages.inputWorkflow,
				TransformationsMessages.outputFileGroup);
	}

}
