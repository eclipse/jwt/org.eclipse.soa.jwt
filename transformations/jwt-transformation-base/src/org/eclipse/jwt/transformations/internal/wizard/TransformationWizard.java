/**
 * File:    TransformationWizard.java
 * Created: 29.06.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.internal.wizard;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

public abstract class TransformationWizard extends Wizard implements IImportWizard {

	private IStructuredSelection selectionAtInitTime;
	private TransformationWizardPage mainPage;

	public TransformationWizard() {
		super();
	}

	@Override
	public boolean performFinish() {
		return mainPage.finish();
	}

	@Override
	public void addPages() {
		super.addPages();
		mainPage = createMainPage(selectionAtInitTime);
		addPage(mainPage);
	}

	protected abstract TransformationWizardPage createMainPage(
			IStructuredSelection selectionAtInitTime);

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selectionAtInitTime = selection;
	}

}