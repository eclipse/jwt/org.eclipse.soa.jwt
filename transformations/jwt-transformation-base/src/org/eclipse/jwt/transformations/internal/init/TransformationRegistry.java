/**
 * File:    TransformationRegistry.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - refactored to enable more generic transformations
 *******************************************************************************/
package org.eclipse.jwt.transformations.internal.init;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.jwt.transformations.api.AbstractTransformation;
import org.eclipse.jwt.transformations.api.Transformation;
import org.eclipse.jwt.transformations.api.Transformation.Type;

public class TransformationRegistry {

	private static TransformationRegistry instance = null;

	private Set<Transformation> transformations = new HashSet<Transformation>();

	public static void createInstance() throws CoreException, IOException {
		if (instance == null) {
			instance = new TransformationRegistry();
		}
	}

	public static TransformationRegistry getInstance() {
		return instance;
	}

	private TransformationRegistry() throws CoreException, IOException {
		IConfigurationElement[] transformationElements = AbstractTransformation
				.getExtensionPoint().getConfigurationElements();

		// For each defined transformation
		for (IConfigurationElement element : transformationElements) {
			Transformation created = AbstractTransformation
					.createTransformation(element);
			transformations.add(created);
		}
	}

	public Collection<Transformation> getTransformations() {
		return Collections.unmodifiableSet(transformations);
	}

	public Collection<Transformation> getTransformationsByType(Type requiredType) {
		Collection<Transformation> filteredTransformations = new ArrayList<Transformation>();
		for (Transformation transformation : transformations) {
			if (transformation.getType().equals(requiredType)) {
				filteredTransformations.add(transformation);
			}
		}

		return filteredTransformations;
	}
}
