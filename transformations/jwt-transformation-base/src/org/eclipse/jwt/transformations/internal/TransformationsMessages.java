package org.eclipse.jwt.transformations.internal;

import org.eclipse.osgi.util.NLS;

public class TransformationsMessages extends NLS {
	private static String BUNDLE_NAME = "org/eclipse/jwt/transformations/internal/messages"; //$NON-NLS-1$

	public static String transformationAlreadyExists_title;
	public static String transformationAlreadyExists_details;
	public static String transformationError_title;
	public static String transformationError_details;
	public static String canNotOpenEditor_details;

	public static String ok;
	public static String abort;
	public static String browse;
	public static String transformationGroup;
	public static String outputFileGroup;

	public static String exportDialog_header;
	public static String exportDialog_title;
	public static String exportWizard_pageName;
	public static String exportWizard_pageTitle;
	public static String exportWizard_pageDescription;

	public static String inputWorkflow;
	public static String chooseExportTransformation;

	public static String useDefaultFile;
	public static String openCreatedFile;

	public static String importWizard_pageName;
	public static String importWizard_pageTitle;
	public static String importWizard_pageDescription;
	public static String importInputFileGroup;
	public static String chooseImportTransformation;
	public static String importDialog_title;
	public static String importDialog_header;

	public static String inputTypeComboText;
	public static String outputTypeComboText;

	public static String selectEditorInstance;
	public static String selectFile;


	static {
		NLS.initializeMessages(BUNDLE_NAME, TransformationsMessages.class);
	}
}
