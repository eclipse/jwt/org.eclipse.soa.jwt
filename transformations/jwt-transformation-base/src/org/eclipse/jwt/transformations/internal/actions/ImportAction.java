/**
 * File:    ExportAction.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation
 *      - Refactoring to fit with jwt-we extensions
 *      - Add of widgets to select the transformation
 *******************************************************************************/
package org.eclipse.jwt.transformations.internal.actions;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jwt.transformations.internal.wizard.ImportTransformationWizard;
import org.eclipse.jwt.we.editors.actions.external.WEExternalAction;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;


public class ImportAction extends WEExternalAction {

	@Override
	public ImageDescriptor getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.editors.actions.external.WEExternalAction#run()
	 */
	@Override
	public void run() {
		createGUI();
	}

	private static void createGUI() {
		final Display display = Display.getCurrent();
		final Shell shell = new Shell(display);

		ImportTransformationWizard importWizard = new ImportTransformationWizard();
		importWizard.init(PlatformUI.getWorkbench(), new StructuredSelection());
		WizardDialog dialog = new WizardDialog(shell, importWizard);
		dialog.create();
		dialog.open(); 
	}


}
