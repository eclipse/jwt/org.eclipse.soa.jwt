/**
 * File:    WorkflowEditorInstancePicker.java
 * Created: 11.07.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.widgets;

import org.eclipse.jwt.transformations.api.io.ContentChangeListener;
import org.eclipse.jwt.transformations.internal.TransformationsMessages;
import org.eclipse.jwt.transformations.widgets.internal.ObjectPickerCombo;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * A control allowing the user to choose a {@link WEEditor} instance among those
 * currently loaded.
 * <p>
 * Please note that, after starting the application, some of the editors that
 * were restored from a previous session might not be loaded, probably because
 * of a lazy loading strategy. Making these editors appear on screen (by
 * clicking on the corresponding tab) will trigger the loading.
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 * 
 */
public final class WorkflowEditorInstancePicker extends Composite {
	private final WEInstanceCombo combo;

	public WorkflowEditorInstancePicker(Composite parent, int style,
			ContentChangeListener pageListener) {
		super(parent, style);

		setLayout(new GridLayout(2, false));

		final Label label = new Label(this, SWT.READ_ONLY);
		label.setText(TransformationsMessages.selectEditorInstance);
		label.setLayoutData(new GridData());

		this.combo = new WEInstanceCombo(pageListener);
		combo.getControl()
				.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		combo.setData(GeneralHelper.getEditors());
		combo.select(GeneralHelper.getActiveInstance());

		combo.getControl().pack();
	}

	public WEEditor getSelection() {
		return combo.getSelection();
	}

	private class WEInstanceCombo extends ObjectPickerCombo<WEEditor, WEEditor> {
		private final ContentChangeListener pageListener;

		public WEInstanceCombo(ContentChangeListener pageListener) {
			super(WorkflowEditorInstancePicker.this, SWT.NONE);
			this.pageListener = pageListener;
		}

		@Override
		protected String extractLabel(WEEditor entry) {
			return entry.getTitle();
		}

		@Override
		protected WEEditor extractData(WEEditor entry) {
			return entry;
		}

		@Override
		protected void onSelectionChanged() {
			pageListener.contentChanged();
		}
	}
}
