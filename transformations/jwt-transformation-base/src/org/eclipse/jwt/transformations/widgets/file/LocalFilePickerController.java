/**
 * File:    LocalFilePickerController.java
 * Created: 11.07.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.widgets.file;

import java.io.File;
import java.net.URI;
import java.util.Collection;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jwt.transformations.api.io.ContentChangeListener;
import org.eclipse.jwt.transformations.internal.TransformationsMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

/**
 * An implementation of {@link AbstractFilePickerController} for java.io {@link File Files}.
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 * 
 */
public class LocalFilePickerController extends
		AbstractFilePickerController<File> {

	private static final char EXTENSION_SEPARATOR = '.';

	public LocalFilePickerController(ContentChangeListener pageListener,
			FileFilter filter, File defaultFile) {
		super(pageListener, filter, defaultFile);
	}

	@Override
	protected String fileToPath(File file) {
		if (file == null) {
			return ""; //$NON-NLS-1$
		}

		return file.getPath();
	}

	@Override
	protected File pathToFile(String path) {
		return new File(path);
	}

	@Override
	protected boolean isValid(File file, FileFilter filter) {
		if (file == null || "".equals(file.getPath())) //$NON-NLS-1$
			return false;

		if (!filter.getValidExtensions().isEmpty()) {
			int extensionIndex = file.getName()
					.lastIndexOf(EXTENSION_SEPARATOR);
			String extension = null;
			if (extensionIndex >= 0) {
				extension = file.getName().substring(extensionIndex);
			}
			if (filter.getValidExtensions().contains(extension)) {
				return false;
			}
		}

		if (file.exists() && !file.isFile()) {
			// Don't select directories
			return false;
		}

		return !filter.mustExist() || file.exists();
	}

	@Override
	protected File doSelectFileInDialog(FileFilter filter) {
		FileDialog dialog = new FileDialog(view.getShell(),
				filter.mustExist() ? SWT.OPEN : SWT.SAVE);
		dialog.setText(TransformationsMessages.selectFile);
		dialog.setFilterExtensions(getSwtFilterExtensions(filter));

		String newfilePath = dialog.open();
		File newFile = pathToFile(newfilePath);
		if (isValid(newFile, filter)) {
			return newFile;
		}

		return null;
	}

	private static String[] getSwtFilterExtensions(FileFilter filter) {
		Collection<String> validExtensions = filter.getValidExtensions();
		if (validExtensions.isEmpty()) {
			return new String[] { "*.*" }; //$NON-NLS-1$
		}

		String[] result = new String[validExtensions.size()];
		int index = 0;

		for (String extension : validExtensions) {
			result[index++] = "*." + extension; //$NON-NLS-1$
		}

		return result;
	}

	@Override
	public void openFile() throws CoreException {
		URI uri = getSelection().toURI();
		IFileStore fileStore = EFS.getStore(uri);
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		IDE.openEditorOnFileStore(page, fileStore);
	}
}
