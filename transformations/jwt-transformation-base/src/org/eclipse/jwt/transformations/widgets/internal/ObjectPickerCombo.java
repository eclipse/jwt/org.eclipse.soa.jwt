/**
 * File:    ObjectPickerCombo.java
 * Created: 11.07.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.widgets.internal;

import java.util.Collection;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

public abstract class ObjectPickerCombo<EntryType, DataType> {
	private final Combo actualCombo;
	private DataType selection = null;

	public ObjectPickerCombo(Composite parent, int style) {
		actualCombo = new Combo(parent, style | SWT.READ_ONLY);

		actualCombo.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				updateSelection();
			}
		});
	}

	public void setData(Collection<EntryType> entries) {
		actualCombo.removeAll();

		for (EntryType entry : entries) {
			int index = actualCombo.getItemCount();
			actualCombo.add(extractLabel(entry), index);

			// By using the index as a data key instead of the label, we
			// will avoid name conflicts
			actualCombo.setData(String.valueOf(index), extractData(entry));
		}
	}

	protected abstract String extractLabel(EntryType entry);

	protected abstract DataType extractData(EntryType entry);

	/**
	 * @return The actual SWT control owned by this object.
	 */
	public Combo getControl() {
		return actualCombo;
	}

	@SuppressWarnings("unchecked")
	private DataType getEntryData(int index) {
		return (DataType) actualCombo.getData(String.valueOf(index));
	}

	/**
	 * @return The currently selected object, or null if there is none.
	 */
	public DataType getSelection() {
		return selection;
	}

	/**
	 * Selects the entry corresponding to <code>object</code>.
	 * <p>
	 * If <code>object</code> is not part of any entry in this combo box, or is
	 * null, does nothing.
	 * <p>
	 * This method handles the call to {@link #updateSelection()}.
	 */
	public void select(DataType object) {
		if (object != null) {
			actualCombo.select(getFirstIndex(object));
			updateSelection();
		}
	}

	/**
	 * @param data
	 *            A non-null object potentially stored as a data in this combo
	 *            box.
	 * @return The index of the first (lowest index) entry which has an
	 *         associated object <code>object</code> such that
	 *         <code>data.equals(object)</code>, or -1 if there is no such
	 *         entry.
	 * @throws NullPointerException
	 *             if data is null and the combobox contains a least one item.
	 */
	private int getFirstIndex(DataType data) throws NullPointerException {
		for (int i = 0; i < actualCombo.getItemCount(); ++i) {
			if (data.equals(getEntryData(i))) {
				return i;
			}
		}

		return -1;
	}

	/**
	 * Should be called only when the {@link #getControl() underlying combo}'s
	 * selection is programmatically changed.
	 */
	public final void updateSelection() {
		int selectionIndex = actualCombo.getSelectionIndex();

		// This sets the selection to null if selectionIndex is -1
		selection = getEntryData(selectionIndex);

		onSelectionChanged();
	}

	protected abstract void onSelectionChanged();

}
