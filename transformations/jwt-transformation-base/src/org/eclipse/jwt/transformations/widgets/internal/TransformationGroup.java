/**
 * File:    TransformationGroup.java
 * Created: 29.06.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.widgets.internal;

import java.util.Collection;

import org.eclipse.jwt.transformations.api.Transformation;
import org.eclipse.jwt.transformations.api.io.ContentChangeListener;
import org.eclipse.jwt.transformations.internal.TransformationsMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

public class TransformationGroup {
	private static final int DESCRIPTION_WIDTH_HINT = 400;

	private final ContentChangeListener pageListener;
	private final Group delegate;
	final TransformationPickerCombo transformationsCombo;
	final Label descriptionLabel;

	private class TransformationPickerCombo extends
			ObjectPickerCombo<Transformation, Transformation> {

		public TransformationPickerCombo(String comboText) {
			super(delegate, SWT.NONE);
			getControl().setText(comboText);
			getControl().setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}

		@Override
		protected String extractLabel(Transformation entry) {
			return entry.getName();
		}

		@Override
		protected Transformation extractData(Transformation entry) {
			return entry;
		}

		@Override
		protected void onSelectionChanged() {
			Transformation selection = getSelection();
			if (selection != null) {
				descriptionLabel.setText(selection.getDescription());
			}

			delegate.layout(true);
			delegate.getParent().update();
			pageListener.contentChanged();
		}

	}

	public TransformationGroup(final Composite parent,
			final ContentChangeListener pageListener,
			Collection<Transformation> transformations, String comboText) {
		this.pageListener = pageListener;
		delegate = new Group(parent, SWT.NONE);
		delegate.setText(TransformationsMessages.transformationGroup);
		delegate.setLayout(new GridLayout(1, false));

		transformationsCombo = new TransformationPickerCombo(comboText);
		transformationsCombo.setData(transformations);

		descriptionLabel = new Label(delegate, SWT.LEFT | SWT.WRAP);
		GridData descriptionLabelLayoutData = new GridData(SWT.FILL, SWT.FILL,
				true, true);
		descriptionLabelLayoutData.widthHint = DESCRIPTION_WIDTH_HINT;
		descriptionLabel.setLayoutData(descriptionLabelLayoutData);
	}

	public Group getControl() {
		return delegate;
	}

	public Transformation getTransformation() {
		return transformationsCombo.getSelection();
	}

}
