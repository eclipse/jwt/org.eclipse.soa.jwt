/**
 * File:    AbstractFilePickerController.java
 * Created: 11.07.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.widgets.file;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jwt.transformations.api.io.ContentChangeListener;

/**
 * A controller in the MVC fashion, ensuring the link between the view (a
 * {@link FilePickerPathView}) and the model (an object of type
 * <code>FileType</code>).
 * <p>
 * This class provides the interface expected by {@link FilePickerPathView} and
 * generic implementation for type-independent methods, with some basic template
 * methods.
 * 
 * @see WorkspaceFilePickerController
 * @see LocalFilePickerController
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 * 
 * @param <FileType>
 *            The Model type.
 */
public abstract class AbstractFilePickerController<FileType> {
	private final ContentChangeListener pageListener;
	private final FileType defaultFile;
	private final FileFilter fileFilter;

	/** The model this controller maps to */
	private FileType selectedFile;
	/** The view controlled by this controller */
	protected FilePickerPathView view;

	public static class FileFilter {
		private Collection<String> validExtensions = new ArrayList<String>();
		private boolean mustExist;

		/**
		 * @param validExtensions
		 *            Only files with these extensions will be matched. If it is
		 *            empty, all files will be matched.
		 * @param mustExist
		 *            If true, only existing files will be matched.
		 */
		public FileFilter(Collection<String> validExtensions, boolean mustExist) {
			super();
			this.validExtensions.addAll(validExtensions);
			this.mustExist = mustExist;
		}

		/**
		 * @see FileFilter#FileFilter(Collection, boolean)
		 */
		public FileFilter(String[] validExtensions, boolean mustExist) {
			this(Arrays.asList(validExtensions), mustExist);
		}

		/**
		 * Creates a file filter matching all files.
		 */
		public FileFilter() {
			this(new String[] {}, false);
		}

		public Collection<String> getValidExtensions() {
			return validExtensions;
		}

		public boolean mustExist() {
			return mustExist;
		}
	}

	/**
	 * 
	 * @param pageListener
	 * @param filter
	 * @param defaultFile
	 *            The default value of the underlying file, or null if there's
	 *            none. If <code>defaultFile</code> is not valid according to
	 *            the {@link #isValid(FileType, FileFilter)} method with respect
	 *            to <code>defaultFile</code>, the default file is set to
	 *            <code>null</code> instead.
	 */
	public AbstractFilePickerController(ContentChangeListener pageListener,
			FileFilter filter, FileType defaultFile) {
		this.pageListener = pageListener;
		this.fileFilter = filter;
		this.defaultFile = isValid(defaultFile, this.fileFilter) ? defaultFile
				: null;
	}

	/**
	 * @param file
	 * @return The path of <code>file</code>, or an empty string if
	 *         <code>file</code> is <code>null</code>.
	 */
	protected abstract String fileToPath(FileType file);

	/**
	 * @param path
	 * @return The file located at <code>path</code>, or <code>null</code> if
	 *         <code>path</code> is incorrect (empty or <code>null</code>, for
	 *         instance).
	 */
	protected abstract FileType pathToFile(String path);

	/**
	 * @param file
	 *            A file to be tested, potentially <code>null</code>.
	 * @param filter
	 *            A non-null filter describing valid files.
	 * @return True if <code>file</code> is valid with respect to
	 *         <code>filter</code>.
	 */
	protected abstract boolean isValid(FileType file, FileFilter filter);

	/**
	 * Opens a pop-up dialog to allow the user to select a File.
	 * 
	 * @param filter
	 *            A non-null filter describing valid files. This filter is only
	 *            provided to help implementing classes to display only valid
	 *            candidates to the end user. The returned file need not be
	 *            valid, and will be tested anyway.
	 * 
	 * @return The selected file, or null if none was selected (due to user
	 *         cancellation, for instance).
	 */
	protected abstract FileType doSelectFileInDialog(FileFilter filter);

	/**
	 * Opens the current selection in an editor.
	 * 
	 * @throws CoreException
	 * @see WorkspaceFilePickerController
	 */
	public abstract void openFile() throws CoreException;

	public void setView(FilePickerPathView abstractFilePicker) {
		this.view = abstractFilePicker;
		view.setPath(fileToPath(selectedFile));
	}

	public FileType getSelection() {
		return selectedFile;
	}

	public void resetSelection() {
		setSelection(this.defaultFile);
	}

	public void selectFileInDialog() {
		FileType newFile = doSelectFileInDialog(fileFilter);
		setSelection(newFile);
	}

	public void changePath(String newPath) {
		setSelection(newPath);
	}

	private void setSelection(String newPath) {
		FileType newFile = pathToFile(newPath);
		if (isValid(newFile, fileFilter)) {
			this.selectedFile = newFile;
		} else {
			this.selectedFile = null;
		}

		view.setPath(newPath);
		pageListener.contentChanged();
	}

	private void setSelection(FileType newFile) {
		if (isValid(newFile, fileFilter)) {
			this.selectedFile = newFile;
			view.setPath(fileToPath(selectedFile));
			pageListener.contentChanged();
		}
	}
}