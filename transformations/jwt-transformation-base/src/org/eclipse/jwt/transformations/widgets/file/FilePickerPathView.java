/**
 * File:    FilePickerPathView.java
 * Created: 11.07.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.widgets.file;

import org.eclipse.jwt.transformations.internal.TransformationsMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

/**
 * A view in the MVC fashion, propagating user events to the controller (an
 * {@link AbstractFilePickerController} of unknown underlying type).
 * <p>
 * This class provides a simple view of a file, showing only a text field with
 * the file path and a "browse" button opening a dialog on click.
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 */
public class FilePickerPathView extends Composite {
	protected AbstractFilePickerController<?> controller;
	protected Text fileText;
	protected Button fileBrowse;

	public FilePickerPathView(Composite parent,
			AbstractFilePickerController<?> controller) {
		super(parent, SWT.NONE);

		this.controller = controller;

		setLayout(new GridLayout(2, false));

		init();
	}

	/**
	 * Called in the constructor to build the control.
	 */
	protected void init() {
		fileText = new Text(this, SWT.SINGLE | SWT.BORDER);
		GridData filegridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.HORIZONTAL_ALIGN_FILL);
		fileText.setLayoutData(filegridData);

		fileBrowse = new Button(this, SWT.NULL);
		fileBrowse.setText(TransformationsMessages.browse);

		fileBrowse.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				controller.selectFileInDialog();
			}
		});

		fileText.addListener(SWT.Modify, new Listener() {
			public void handleEvent(Event e) {
				// Avoid looping when the controller calls setPath()
				fileText.removeListener(SWT.Modify, this);
				controller.changePath(fileText.getText());
				fileText.addListener(SWT.Modify, this);
			}
		});

		controller.setView(this);
	}

	public void setPath(String path) {
		fileText.setText(path);
	}

	public String getPath() {
		return fileText.getText();
	}
}
