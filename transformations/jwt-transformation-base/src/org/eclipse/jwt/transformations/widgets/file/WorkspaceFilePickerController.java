/**
 * File:    LocalFilePickerController.java
 * Created: 11.07.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.widgets.file;

import java.net.URI;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jwt.transformations.api.io.ContentChangeListener;
import org.eclipse.jwt.transformations.internal.TransformationsMessages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

/**
 * An implementation of {@link AbstractFilePickerController} for Eclipse
 * {@link IFile org.eclipse.core.resources.IFile}.
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 * 
 */
public class WorkspaceFilePickerController extends
		AbstractFilePickerController<IFile> {
	private final IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace()
			.getRoot();

	public WorkspaceFilePickerController(ContentChangeListener pageListener,
			FileFilter filter, IFile defaultFile) {
		super(pageListener, filter, defaultFile);
	}

	@Override
	protected String fileToPath(IFile file) {
		if (file == null) {
			return ""; //$NON-NLS-1$
		}
		return file.getFullPath().toOSString();
	}

	@Override
	protected IFile pathToFile(String path) {
		try {
			return workspaceRoot.getFile(new Path(path));
		} catch (IllegalArgumentException ex) {
			return null;
		}
	}

	@Override
	protected boolean isValid(IFile file, FileFilter filter) {
		return file != null
				&& (filter.getValidExtensions().isEmpty() || filter
						.getValidExtensions().contains(file.getFileExtension()))
				&& (!filter.mustExist() || file.exists());
	}

	@Override
	protected IFile doSelectFileInDialog(final FileFilter filter) {
		ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
				view.getShell(), new WorkbenchLabelProvider(),
				new BaseWorkbenchContentProvider());
		dialog.setMessage(TransformationsMessages.selectFile);
		dialog.setInput(workspaceRoot);
		dialog.addFilter(new ViewerFilter() {
			@Override
			public boolean select(Viewer viewer, Object parentElement,
					Object element) {
				if (element instanceof IFile) {
					IFile file = (IFile) element;
					return isValid(file, filter);
				}

				return element instanceof IContainer;
			}
		});

		dialog.open();

		Object result = dialog.getFirstResult();
		if (result != null && result instanceof IFile) {
			return (IFile) result;
		} else {
			return null;
		}
	}

	@Override
	public void openFile() throws CoreException {
		URI uri = getSelection().getLocationURI();
		IFileStore fileStore = EFS.getStore(uri);
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		IDE.openEditorOnFileStore(page, fileStore);
	}
}
