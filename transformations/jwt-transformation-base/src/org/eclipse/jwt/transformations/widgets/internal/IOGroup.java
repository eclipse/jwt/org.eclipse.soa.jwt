/**
 * File:    IOGroup.java
 * Created: 29.06.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.widgets.internal;

import java.io.IOException;
import java.util.Collection;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.transformations.api.Transformation;
import org.eclipse.jwt.transformations.api.io.IODefinition;
import org.eclipse.jwt.transformations.api.io.IODefinition.IOHandle;
import org.eclipse.jwt.transformations.api.io.ContentChangeListener;
import org.eclipse.jwt.transformations.internal.wizard.TransformationWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;

/**
 * An SWT wrapper class implementing the layout and logic of the input and
 * output {@link Group groups} in the {@link TransformationWizardPage
 * transformation wizard}.
 * <p>
 * It serves as a {@link IOHandle} proxy, allowing the user to choose the wanted
 * input/output (using a combobox) and to parameterize it (using the
 * {@link IOHandle#getControl() IOHandle control}, while letting the wizard
 * unaware of what's going on (except when changing the selected
 * transformation).
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 */
public abstract class IOGroup implements IOHandle {
	private final Group delegateControl;
	private final ContentChangeListener pageListener;
	private final IStructuredSelection selection;
	private final String comboText;

	private IOHandlePickerCombo ioDefinitionCombo;

	private Composite ioStackComposite;
	private StackLayout ioStackLayout;

	private class IOHandlePickerCombo extends
			ObjectPickerCombo<IODefinition, IOHandle> {

		public IOHandlePickerCombo() {
			super(delegateControl, SWT.NONE);
			getControl().setText(comboText);
			getControl().setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		}

		@Override
		protected String extractLabel(IODefinition entry) {
			return entry.getLabel();
		}

		@Override
		protected IOHandle extractData(IODefinition entry) {
			IOHandle handle = entry.createIOControl(ioStackComposite,
					pageListener, selection);
			handle.getControl().pack();

			return handle;
		}

		@Override
		protected void onSelectionChanged() {
			if (getSelection() != null) {
				ioStackLayout.topControl = getSelection().getControl();
			} else {
				ioStackLayout.topControl = null;
			}
			ioStackComposite.layout();
			pageListener.contentChanged();
		}

	}

	public IOGroup(final Composite parent, final ContentChangeListener pageListener,
			IStructuredSelection selection, String text, String comboText) {
		delegateControl = new Group(parent, SWT.NONE);
		this.selection = selection;
		this.pageListener = pageListener;
		this.comboText = comboText;

		delegateControl.setText(text);
		delegateControl.setLayout(new GridLayout(1, false));

		setTransformation(null);
	}

	public void setTransformation(Transformation transformation) {
		removeChildren(delegateControl);

		ioDefinitionCombo = new IOHandlePickerCombo();

		ioStackComposite = new Composite(delegateControl, SWT.NONE);
		ioStackLayout = new StackLayout();
		ioStackComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
		ioStackComposite.setLayout(ioStackLayout);

		if (transformation != null) {
			ioDefinitionCombo.setData(extractIODefinitions(transformation));
			ioDefinitionCombo.getControl().select(0);
			ioDefinitionCombo.updateSelection();

			/*
			 * Lay out the entire shell after clearing the layout caches. This
			 * is required to take the new nested composites into account.
			 */
			delegateControl.getShell().layout(true, true);

			/* Resize the shell so it is wide enough for the nested IO controls */
			delegateControl.getShell().pack();
		}
	}

	private static void removeChildren(Composite composite) {
		for (Control child : composite.getChildren()) {
			child.dispose();
		}
	}

	protected abstract Collection<IODefinition> extractIODefinitions(
			Transformation transformation);

	public void afterTransformation() throws CoreException, IOException {
		ioDefinitionCombo.getSelection().afterTransformation();
	}

	public Group getControl() {
		return delegateControl;
	}

	public Object getValue() {
		return ioDefinitionCombo.getSelection().getValue();
	}

	public boolean hasAllRequiredInformation() {
		return ioDefinitionCombo.getSelection() != null
				&& ioDefinitionCombo.getSelection().hasAllRequiredInformation();
	}

}
