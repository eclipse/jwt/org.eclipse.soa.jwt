/**
 * File:    WorkflowEditorInstanceIODefinition.java
 * Created: 11.07.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.transformations.io;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.transformations.api.io.AbstractIODefinition;
import org.eclipse.jwt.transformations.api.io.ContentChangeListener;
import org.eclipse.jwt.transformations.api.io.IODefinition;
import org.eclipse.jwt.transformations.widgets.WorkflowEditorInstancePicker;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * An {@link IODefinition} whose {@link IOHandle} provides a {@link WEEditor}
 * instance to transformations.
 * <p>
 * The corresponding control is available publicly as
 * {@link WorkflowEditorInstancePicker}.
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 * 
 */
public final class WorkflowEditorInstanceIODefinition extends
		AbstractIODefinition {

	public WEInstanceIOHandle createIOControl(Composite parent,
			ContentChangeListener pageListener,
			IStructuredSelection currentSelection) {
		return new WEInstanceIOHandle(parent, pageListener, currentSelection);
	}

	public static class WEInstanceIOHandle implements IOHandle {
		private WorkflowEditorInstancePicker control;

		private WEInstanceIOHandle(Composite parent,
				ContentChangeListener pageListener,
				IStructuredSelection currentSelection) {
			this.control = new WorkflowEditorInstancePicker(parent, SWT.NONE,
					pageListener);
		}

		public WEEditor getValue() {
			return control.getSelection();
		}

		public Control getControl() {
			return control;
		}

		public boolean hasAllRequiredInformation() {
			return control.getSelection() != null;
		}

		public void afterTransformation() throws CoreException, IOException {
			// Nothing to do
		}
	}
}
