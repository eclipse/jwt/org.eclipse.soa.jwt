package org.eclipse.jwt.transformations.io;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.transformations.api.io.ContentChangeListener;
import org.eclipse.jwt.transformations.api.io.IODefinition;
import org.eclipse.jwt.transformations.widgets.file.AbstractFilePickerController;
import org.eclipse.jwt.transformations.widgets.file.WorkspaceFilePickerController;
import org.eclipse.jwt.transformations.widgets.file.AbstractFilePickerController.FileFilter;
import org.eclipse.jwt.we.misc.util.GeneralHelper;

/**
 * An {@link IODefinition} whose {@link IOHandle} provides an {@link IFile}
 * instance to transformations.
 * <p>
 * This definition accepts optional parameters defined in
 * {@link FileIODefinition}.
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 * 
 */
public class WorkspaceFileIODefinition extends FileIODefinition<IFile> {
	@Override
	protected AbstractFilePickerController<IFile> createController(
			ContentChangeListener pageListener, FileFilter fileFilter,
			IFile defaultFile) {
		return new WorkspaceFilePickerController(pageListener, fileFilter,
				defaultFile);
	}

	@Override
	protected IFile getDefaultFile(IStructuredSelection currentSelection,
			boolean useWorkflow, boolean useSelected) {
		Object firstItem = currentSelection.getFirstElement();

		if (useWorkflow && GeneralHelper.getActiveInstance() != null) {
			URI uri = GeneralHelper.getActiveInstance()
					.getCurrentActivitySheet().getActivityModel().eResource()
					.getURI();
			if (uri != null) {
				String platformString = uri.toPlatformString(true);
				if (platformString != null) {
					IPath path = new Path(platformString);
					return ResourcesPlugin.getWorkspace().getRoot()
							.getFile(path);
				}
			}
		}

		if (useSelected && firstItem instanceof IFile) {
			return (IFile) firstItem;
		}

		return null;
	}
}
