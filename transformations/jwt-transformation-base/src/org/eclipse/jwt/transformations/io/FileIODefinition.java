package org.eclipse.jwt.transformations.io;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.transformations.api.io.AbstractIODefinition;
import org.eclipse.jwt.transformations.api.io.ContentChangeListener;
import org.eclipse.jwt.transformations.api.io.IODefinition;
import org.eclipse.jwt.transformations.internal.TransformationsMessages;
import org.eclipse.jwt.transformations.widgets.file.AbstractFilePickerController;
import org.eclipse.jwt.transformations.widgets.file.FilePickerPathView;
import org.eclipse.jwt.transformations.widgets.file.AbstractFilePickerController.FileFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

/**
 * An abstract base class for an {@link IODefinition} whose {@link IOHandle}
 * provides a file instance to transformations. The file instance type is
 * parametrized as FileType.
 * <p>
 * This definition accepts six parameters, all of them optional:
 * <ul>
 * <li>{@value #EXTENSIONS_PARAMETER_NAME}: a comma-separated (",") list of
 * valid extensions (example: "exe,zip,jar"). If not provided, all extensions
 * are considered as valid.
 * <li>{@value #MUSTEXIST_PARAMETER_NAME}: "true" or "false", defaults to
 * "false". If "true", the IOHandle will only provide already existing files.
 * <li>{@value #OPEN_FILE_AFTER_EXEC_PARAMETER_NAME}: "true" or "false",
 * defaults to "false". If "true", the created control will show a checkbox
 * allowing the user to choose to automatically open the file after the
 * transformation. The checkbox text will be the value of the
 * {@value #OPEN_FILE_AFTER_EXEC_LABEL_PARAMETER_NAME} parameter, or a default
 * label if it isn't set.
 * <li>{@value #OPEN_FILE_AFTER_EXEC_LABEL_PARAMETER_NAME}: String. If defined,
 * the "open after transformation" checkbox label will be the value of this
 * parameter.
 * <li>{@value #USE_CURRENT_WORKFLOW_AS_DEFAULT_PARAMETER_NAME}: "true" or
 * "false", defaults to "false". If "true", the currently selected file, or the
 * workflow file shown on screen when choosing to export/import will be set as
 * the default, and the created control will feature a checkbox allowing the
 * user to switch between default and custom file. If both this option and
 * {@value #USE_SELECTED_FILE_AS_DEFAULT_PARAMETER_NAME} are set to "true", the
 * currently displayed workflow file will be used in priority, and the selected
 * file only if no workflow is currently displayed.
 * <li>{@value #USE_SELECTED_FILE_AS_DEFAULT_PARAMETER_NAME}: "true" or "false",
 * defaults to "false". If "true", the currently selected file when choosing to
 * export/import will be set as the default, and the created control will
 * feature a checkbox allowing the user to switch between default and custom
 * file.
 * <li>{@value #USE_DEFAULT_FILE_LABEL_PARAMETER_NAME}: String. If defined, the
 * checkbox text (see {@value #USE_CURRENT_WORKFLOW_AS_DEFAULT_PARAMETER_NAME}
 * and {@value #USE_SELECTED_FILE_AS_DEFAULT_PARAMETER_NAME} parameters) will be
 * the value of this parameter.
 * </ul>
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 * 
 * @param FileType
 *            The input/output reference type (i.e. the return type of
 *            {@link IOHandle#getValue()}).
 * 
 */
public abstract class FileIODefinition<FileType> extends AbstractIODefinition {
	public static final String EXTENSIONS_PARAMETER_NAME = "extensions"; //$NON-NLS-1$
	public static final String MUSTEXIST_PARAMETER_NAME = "mustExist"; //$NON-NLS-1$

	public static final String OPEN_FILE_AFTER_EXEC_PARAMETER_NAME = "openAfterExecCheckbox"; //$NON-NLS-1$
	public static final String OPEN_FILE_AFTER_EXEC_LABEL_PARAMETER_NAME = "openAfterExecCheckboxLabel"; //$NON-NLS-1$

	public static final String USE_CURRENT_WORKFLOW_AS_DEFAULT_PARAMETER_NAME = "useCurrentWorkflowAsDefault"; //$NON-NLS-1$
	public static final String USE_SELECTED_FILE_AS_DEFAULT_PARAMETER_NAME = "useSelectedFileAsDefault"; //$NON-NLS-1$
	public static final String USE_DEFAULT_FILE_LABEL_PARAMETER_NAME = "useDefaultFileCheckboxLabel"; //$NON-NLS-1$

	public final IOHandle createIOControl(Composite parent,
			ContentChangeListener pageListener,
			IStructuredSelection currentSelection) {
		boolean useWorkflow = isTrue(getParameter(USE_CURRENT_WORKFLOW_AS_DEFAULT_PARAMETER_NAME));
		boolean useSelected = isTrue(getParameter(USE_SELECTED_FILE_AS_DEFAULT_PARAMETER_NAME));
		final AbstractFilePickerController<FileType> controller = createController(
				pageListener, getFileFilter(),
				getDefaultFile(currentSelection, useWorkflow, useSelected));
		final CustomizedFilePickerPathView view = new CustomizedFilePickerPathView(
				parent, controller);

		return new IOHandle() {
			public boolean hasAllRequiredInformation() {
				return controller.getSelection() != null;
			}

			public FileType getValue() {
				return controller.getSelection();
			}

			public FilePickerPathView getControl() {
				return view;
			}

			public void afterTransformation() throws CoreException {
				if (view.openCreatedFile) {
					controller.openFile();
				}
			}
		};
	}

	protected abstract AbstractFilePickerController<FileType> createController(
			ContentChangeListener pageListener, FileFilter fileFilter,
			FileType defaultFile);

	protected abstract FileType getDefaultFile(
			IStructuredSelection currentSelection, boolean useWorkflow,
			boolean useSelected);

	private class CustomizedFilePickerPathView extends FilePickerPathView {
		private boolean openCreatedFile = false;

		public CustomizedFilePickerPathView(Composite parent,
				AbstractFilePickerController<?> controller) {
			super(parent, controller);
		}

		private void setUseCurrentModel(boolean value) {
			if (value) {
				controller.resetSelection();
			}
			fileText.setEnabled(!value);
			fileBrowse.setEnabled(!value);
		}

		@Override
		protected void init() {
			boolean useDefaultCheckbox = isTrue(getParameter(USE_CURRENT_WORKFLOW_AS_DEFAULT_PARAMETER_NAME))
					| isTrue(getParameter(USE_SELECTED_FILE_AS_DEFAULT_PARAMETER_NAME));
			String defaultCheckboxLabel = getParameter(USE_DEFAULT_FILE_LABEL_PARAMETER_NAME);
			if (defaultCheckboxLabel == null) {
				defaultCheckboxLabel = TransformationsMessages.useDefaultFile;
			}

			if (useDefaultCheckbox) {
				final Button useCurrentModelCheckbox = new Button(this,
						SWT.CHECK);
				useCurrentModelCheckbox.setText(defaultCheckboxLabel);
				useCurrentModelCheckbox.setLayoutData(new GridData(SWT.NULL,
						SWT.NULL, false, false, 2, 1));

				super.init();

				useCurrentModelCheckbox.addListener(SWT.Selection,
						new Listener() {
							public void handleEvent(Event e) {
								setUseCurrentModel(useCurrentModelCheckbox
										.getSelection());
							}
						});

				useCurrentModelCheckbox.setSelection(true);
				setUseCurrentModel(true);
			} else {
				super.init();
			}

			boolean openFileCheckbox = isTrue(getParameter(OPEN_FILE_AFTER_EXEC_PARAMETER_NAME));
			String openFileCheckboxLabel = getParameter(OPEN_FILE_AFTER_EXEC_LABEL_PARAMETER_NAME);
			if (openFileCheckboxLabel == null) {
				openFileCheckboxLabel = TransformationsMessages.openCreatedFile;
			}

			if (openFileCheckbox) {
				final Button checkbox = new Button(this, SWT.CHECK);
				checkbox.setSelection(false);
				checkbox.setText(openFileCheckboxLabel);
				checkbox.setLayoutData(new GridData(SWT.NULL, SWT.NULL, false,
						false, 2, 1));
				checkbox.setEnabled(true);
				openCreatedFile = true;

				checkbox.addListener(SWT.Selection, new Listener() {
					public void handleEvent(Event e) {
						openCreatedFile = checkbox.getSelection();
					}
				});
			}
		}

	}

	private FileFilter getFileFilter() {
		String extensionsParam = getParameter(EXTENSIONS_PARAMETER_NAME);

		String[] extensions = extensionsParam == null ? new String[] {}
				: extensionsParam.split(","); //$NON-NLS-1$
		boolean mustExist = isTrue(getParameter(MUSTEXIST_PARAMETER_NAME));

		return new FileFilter(extensions, mustExist);
	}

	private static boolean isTrue(String parameter) {
		return "true" //$NON-NLS-1$
		.equals(parameter);
	}
}
