package org.eclipse.jwt.transformations.io;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.transformations.api.io.ContentChangeListener;
import org.eclipse.jwt.transformations.api.io.IODefinition;
import org.eclipse.jwt.transformations.widgets.file.AbstractFilePickerController;
import org.eclipse.jwt.transformations.widgets.file.LocalFilePickerController;
import org.eclipse.jwt.transformations.widgets.file.AbstractFilePickerController.FileFilter;
import org.eclipse.jwt.we.misc.util.GeneralHelper;

/**
 * An {@link IODefinition} whose {@link IOHandle} provides a {@link File}
 * instance to transformations.
 * <p>
 * This definition accepts optional parameters defined in
 * {@link FileIODefinition}.
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.3
 * 
 */
public class LocalFileIODefinition extends FileIODefinition<File> {
	@Override
	protected AbstractFilePickerController<File> createController(
			ContentChangeListener pageListener, FileFilter fileFilter,
			File defaultFile) {
		return new LocalFilePickerController(pageListener, fileFilter,
				defaultFile);
	}

	@Override
	protected File getDefaultFile(IStructuredSelection currentSelection,
			boolean useWorkflow, boolean useSelected) {
		Object firstItem = currentSelection.getFirstElement();

		if (useWorkflow && GeneralHelper.getActiveInstance() != null) {
			URI uri = GeneralHelper.getActiveInstance()
					.getCurrentActivitySheet().getActivityModel().eResource()
					.getURI();
			if (uri != null) {
				return new File(CommonPlugin.resolve(uri).toFileString());
			}
		}

		if (useSelected && firstItem instanceof IFile) {
			return ((IFile) firstItem).getLocation().toFile();
		}

		return null;
	}
}
