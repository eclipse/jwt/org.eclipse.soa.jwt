/**
 * File:    JavaApplicationParameterUpdater.java
 * Created: 26.03.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009   Open Wide SA
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *    Mickael Istria, Open Wide, France
 *      - Creation and Implementation
 *      - Use of JDT to introspect classes
 *******************************************************************************/
package org.eclipse.jwt.we.helpers.app.changeListener.applicationMethod;

import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.jwt.meta.model.data.DataFactory;
import org.eclipse.jwt.meta.model.data.InputParameter;
import org.eclipse.jwt.meta.model.data.OutputParameter;
import org.eclipse.jwt.we.helpers.app.jdt.JDTTools;

public class JavaApplicationParameterUpdater {

    /**
    *
    * @param notifier
    * @return true if found a matching method
    */
   public static boolean updateInputOutput(Application app, String clazz, String methodName) {
       try {
		   IMethod method = JDTTools.getMethod(methodName, clazz, JDTTools.getJavaProject(app));
	       if (method == null)
	           return false;
	       else {
	           DataFactory dataFactory = DataFactory.eINSTANCE;
	           InputParameter current;
	           app.getInput().clear();
	           String[] parameterNames = method.getParameterNames();
	           for (int i = 0; i < method.getParameterTypes().length; i++) {
	        	   current = dataFactory.createInputParameter();
	        	   current.setName(parameterNames[i]); //$NON-NLS-1$
	               app.getInput().add(current);
	           }
	           app.getOutput().clear();
	           if (! (method.getReturnType() == null
	                   || Signature.toString(method.getReturnType()).equals("void"))) { //$NON-NLS-1$
	               OutputParameter output = dataFactory.createOutputParameter();
	               output.setName("res"); //$NON-NLS-1$
	               app.getOutput().add(output);
	           }
	           return true;
	       }
       } catch (JavaModelException ex) {
    	   return false;
       }
   }
}