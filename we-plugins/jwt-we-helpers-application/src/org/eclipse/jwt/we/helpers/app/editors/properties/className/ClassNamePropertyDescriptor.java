/**
 * File:    ClassNamePropertyDescriptor.java
 * Created: 26.03.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2008   Open Wide SA
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *    Mickael Istria, Open Wide, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.we.helpers.app.editors.properties.className;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.swt.widgets.Composite;

public class ClassNamePropertyDescriptor extends PropertyDescriptor {

	public ClassNamePropertyDescriptor(Object object, IItemPropertyDescriptor itemPropertyDescriptor) {
		super(object, itemPropertyDescriptor);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public CellEditor createPropertyEditor(Composite parent) {
		return new ClassNameCellEditor(parent, new LabelProvider(), (Application)object);
	}

}
