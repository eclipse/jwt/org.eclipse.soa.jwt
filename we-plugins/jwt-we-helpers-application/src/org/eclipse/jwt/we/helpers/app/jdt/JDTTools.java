/**
 * File:    JDTTools
 * Created: 07.04.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.helpers.app.jdt;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jwt.meta.model.application.Application;

/**
 * This util class contains a set of static method intended to facilitate the use of JDT
 * in JWT Editor
 * @author mistria
 * @since 0.6
 */
public class JDTTools {
	
	/**
	 * 
	 * @param application
	 * @return The Java project for this application (computed from the path on the model), or null
	 * if not in a Java project
	 */
	public static IJavaProject getJavaProject(Application application) {
		// Get Java project
		String path = application.eResource().getURI().toPlatformString( false );
		path = path.replaceFirst( "platform:/resource", "" ); //$NON-NLS-1$ //$NON-NLS-2$
		try {
			IResource r = ResourcesPlugin.getWorkspace().getRoot().findMember( path );
			if( r == null || !( r instanceof IFile )) {
				return null;
			}

			IProject p = r.getProject();
			if( !p.hasNature( JavaCore.NATURE_ID )) {
				return null;
			}
				
			return JavaCore.create( p );
			
		} catch( Exception e ) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<IMethod> getMethodList(String className, IJavaProject project) throws JavaModelException {
		List<IMethod> res = new ArrayList<IMethod>();
		if (className == null)
			return res;
		
		try {
			IType appType = project.findType(className);
			if (appType == null)
				return res;
			
			IMethod[] methods = appType.getMethods();
			for (int i = 0; i < methods.length; i++) {
				IMethod method = methods[i];
				res.add(method);
			}
			return res;
		} catch (JavaModelException ex) {
			return res;
		}
	}
	
	public static IMethod getMethod(String method, String className, IJavaProject project) throws JavaModelException {
		try {
			IMethod[] methods = project.findType(className).getMethods();
			for (int i = 0; i < methods.length; i++) {
				if (methods[i].getElementName().equals(method))
					return methods[i];
			}
			return null;
		} catch (Exception ex) {
			return null;
		}
	}
}
