/**
 * File:    ClassNameCellEditor.java
 * Created: 26.03.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2009   Open Wide SA
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *    Mickael Istria, Open Wide, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.we.helpers.app.editors.properties.className;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.ui.IJavaElementSearchConstants;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.jwt.we.helpers.app.jdt.JDTTools;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.SelectionDialog;

public class ClassNameCellEditor extends DialogCellEditor {

	private Application application;
	private Text text;

	public ClassNameCellEditor(Composite composite, ILabelProvider labelProvider, Application application) {
		super(composite);
		this.application = application;
	}
	
	
	@Override
	protected Control createContents(Composite parent) {
        text = new Text(parent, SWT.LEFT);
        text.setFont(parent.getFont());
        text.setBackground(parent.getBackground());
        text.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				// Nothing particular happens
			}

			public void focusLost(FocusEvent e) {
				doSetValue(text.getText());
			}
        });
        text.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {
				if (e.detail == SWT.TRAVERSE_RETURN) {
					doSetValue(text.getText());
					focusLost();
				} else if (e.detail == SWT.TRAVERSE_ESCAPE) {
					fireCancelEditor();
				}
			}
        });
        text.setFocus();
        return text;
	}

	@Override
	protected void updateContents(Object object) {
		super.updateContents(object);
		if (object != null) {
			text.setText(object.toString());
		}
	}
	
	@Override
	protected void doSetFocus() {
		text.setFocus();
	}
	
	
	
	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		
		IJavaElement[] javaElements = new IJavaElement[] { JDTTools.getJavaProject(application) };
		
		IJavaSearchScope scope = SearchEngine.createJavaSearchScope( 
				javaElements,
				IJavaSearchScope.SOURCES 
				| IJavaSearchScope.APPLICATION_LIBRARIES 
				| IJavaSearchScope.REFERENCED_PROJECTS );
		
		String filter = application.getJavaClass();
		filter = filter == null || filter.length() == 0 ? "?" : filter; //$NON-NLS-1$
		try {
			SelectionDialog dlg = JavaUI.createTypeDialog( 
					shell, 
					new ProgressMonitorDialog( shell ), 
					scope, 
					IJavaElementSearchConstants.CONSIDER_ALL_TYPES, 
					true, 
					filter );
			
			if( dlg.open() == Window.OK ) {
				IType type = (IType) dlg.getResult()[ 0 ];
				String selection = type.getFullyQualifiedName();
				return selection;
			} else {
				return application.getJavaClass();
			}
			
		} catch( Exception e ) {
			e.printStackTrace();
			return application.getJavaClass();
		}

	}
	
	
}
