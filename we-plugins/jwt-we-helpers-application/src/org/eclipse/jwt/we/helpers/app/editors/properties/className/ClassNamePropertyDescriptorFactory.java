/**
 * File:    ClassNamePropertyDescriptorFactory.java
 * Created: 26.03.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2008   Open Wide SA
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *    Mickael Istria, Open Wide, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.we.helpers.app.editors.properties.className;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.jwt.we.editors.properties.extension.PropertyDescriptorFactory;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

public class ClassNamePropertyDescriptorFactory implements PropertyDescriptorFactory {

	public IPropertyDescriptor getPropertyDescriptor(Object arg0, IItemPropertyDescriptor arg1) {
		return new ClassNamePropertyDescriptor(arg0, arg1);
	}

}
