/**
 * File:    GuessParameterFromApplicationMethod.java
 * Created: 22.09.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008   Open Wide SA
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *    Mickael Istria, Open Wide, France
 *      - Creation and Implementation
 *******************************************************************************/


package org.eclipse.jwt.we.helpers.app.changeListener.applicationMethod;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.jwt.meta.model.application.Application;

public class ApplicationParametersChangedListener implements INotifyChangedListener {

	public void notifyChanged(Notification notification) {
		Application app = (Application)notification.getNotifier();
		JavaApplicationParameterUpdater.updateInputOutput(app, app.getJavaClass(), app.getMethod());
	}

}
