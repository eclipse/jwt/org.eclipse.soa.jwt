/**
 * File:    JarArchiveChangedListener.java
 * Created: 07.04.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.helpers.app.changeListener.jarArchive;

import java.io.File;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.jwt.we.helpers.app.Messages;
import org.eclipse.jwt.we.helpers.app.jdt.JDTTools;
import org.eclipse.swt.SWT;
import org.eclipse.ui.PlatformUI;

/**
 * This class in an extension of extension point "org.eclipse.jwt.we.changedListener".
 * It allows to automatically add a jar used in process into the classpath in the process
 * is part of a Java project.
 * @author mistria
 *
 */
public class JarArchiveChangedListener implements INotifyChangedListener {

	public void notifyChanged(Notification notification) {
		String jarFile = notification.getNewStringValue();
		Application app = (Application)notification.getNotifier();
		updateClasspath(jarFile, JDTTools.getJavaProject(app));
	}

	private void updateClasspath(String jarFile, IJavaProject javaProject) {
		if (javaProject == null)
			return;
		try {
			boolean alreadyInClasspath = false;
			for (IClasspathEntry entry : javaProject.getResolvedClasspath(true)) {
				if (entry.getEntryKind() == IClasspathEntry.CPE_LIBRARY
					&& entry.getPath().toString().equals(jarFile))
					alreadyInClasspath = true;
			}
			if (!alreadyInClasspath && new File(jarFile).exists()) {
				MessageDialog messageBox = new MessageDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						Messages.addToClassPath_title, null,
						Messages.addToClassPath_message, MessageDialog.QUESTION,
						new String[] { Messages.no, Messages.yes }, 1);
				int res = messageBox.open();
				if (res == SWT.OK) {
					IClasspathEntry[] cp = javaProject.getRawClasspath();
					IClasspathEntry[] newCp = new IClasspathEntry[cp.length + 1];
					System.arraycopy(cp, 0, newCp, 0, cp.length);
					IPath jarFileIPath = new Path(new File(jarFile).getAbsolutePath());
					newCp[cp.length] = JavaCore.newLibraryEntry(jarFileIPath, null, null);
					javaProject.setRawClasspath(newCp, new NullProgressMonitor());
				}
			}
		} catch (JavaModelException ex) {
			// DO NOTHING
		}
	}
	
	

}
