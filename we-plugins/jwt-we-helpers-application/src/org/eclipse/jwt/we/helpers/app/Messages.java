package org.eclipse.jwt.we.helpers.app;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	private static final String BUNDLE_NAME = "org.eclipse.jwt.we.helpers.app.messages"; //$NON-NLS-1$
	
	static {
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}
	
	public static String addToClassPath_message;
	public static String addToClassPath_title;
	public static String yes;
	public static String no;
	
}
