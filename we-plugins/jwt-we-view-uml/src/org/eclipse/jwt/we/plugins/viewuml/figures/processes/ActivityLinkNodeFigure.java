package org.eclipse.jwt.we.plugins.viewuml.figures.processes;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jwt.we.figures.processes.RoundedRectangleBorder;
import org.eclipse.jwt.we.plugins.viewuml.Activator;
import org.eclipse.swt.graphics.Image;


/**
 * @author ElSaad
 * 
 */
public class ActivityLinkNodeFigure
		extends org.eclipse.jwt.we.figures.processes.ActivityLinkNodeFigure
{

	/**
	 * The constructor.
	 */
	public ActivityLinkNodeFigure()
	{
		super();

		// a standard border
		setBorder(new RoundedRectangleBorder());

		// label left, icon right
		getLabel().setTextPlacement(PositionConstants.WEST);
		getLabel().setIconAlignment(PositionConstants.RIGHT);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.figures.processes.ActivityLinkNodeFigure#getPreferredSize(int,
	 * int)
	 */
	@Override
	public Dimension getPreferredSize(int hint, int hint2)
	{
		Dimension pref = super.getPreferredSize(hint, hint2);

		// make the figure a bit bigger
		pref.width += 5;
		return pref;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.NamedIconFigure#getCellEditorArea()
	 */
	@Override
	public Rectangle getCellEditorArea()
	{
		Rectangle result = getClientArea().getCopy();

		return result;// .crop(new Insets(iconBounds.height, 0, 0, 0));
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.figures.core.NamedIconFigure#setIcon(org.eclipse.swt.graphics
	 * .Image)
	 */
	@Override
	public void setIcon(Image icon)
	{
		// show fork icon
		super.setIcon(Activator.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
				"icons/callbehaviouraction.gif").createImage());
	}

}