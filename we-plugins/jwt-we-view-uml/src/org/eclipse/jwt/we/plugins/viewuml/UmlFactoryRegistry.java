/**
 * File:    UmlFactoryRegistry.java
 * Created: 10.03.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewuml;

import org.eclipse.gef.EditPartFactory;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jwt.we.IFactoryRegistry;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.editors.palette.IPaletteFactory;
import org.eclipse.jwt.we.figures.IFigureFactory;
import org.eclipse.jwt.we.figures.IImageFactory;
import org.eclipse.jwt.we.figures.StandardFigureFactory;
import org.eclipse.jwt.we.figures.internal.CompositeFigureFactory;
import org.eclipse.jwt.we.figures.internal.ImageFactory;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.parts.JWTEditPartFactory;
import org.eclipse.jwt.we.plugins.viewuml.figures.UmlFigureFactory;
import org.eclipse.jwt.we.plugins.viewuml.palette.UMLPaletteFactory;

public class UmlFactoryRegistry implements IFactoryRegistry
{

	/**
	 * A Logger.
	 */
	private static final Logger logger = Logger.getLogger(UmlFactoryRegistry.class);

	/**
	 * Path where the icons are located
	 */
	private static final String ICONS_BASE_PATH = "icons/"; //$NON-NLS-1$

	/**
	 * imageFactory
	 */
	protected IImageFactory imageFactory;

	/**
	 * figureFactory
	 */
	protected IFigureFactory figureFactory;

	/**
	 * editPartFactory
	 */
	protected EditPartFactory editPartFactory;

	/**
	 * paletteFactory
	 */
	protected IPaletteFactory paletteFactory;


	public UmlFactoryRegistry()
	{
		super();
	}


	public IImageFactory getImageFactory(final Object... objects)
	{
		if (imageFactory == null)
		{
			final ImageRegistry imageRegistry = Plugin.getInstance().getImageRegistry();
			// add imageFactory for jwt-we plug-in
			imageFactory = new ImageFactory(imageRegistry, Plugin.getDefault(),
					UmlFactoryRegistry.ICONS_BASE_PATH);
		}
		return imageFactory;
	}


	public IPaletteFactory getPaletteFactory()
	{
		if (paletteFactory == null)
		{
			paletteFactory = new UMLPaletteFactory();
		}

		return paletteFactory;
	}


	public IFigureFactory getFigureFactory()
	{
		if (figureFactory == null)
		{
			logger.info("**** getFigureFactory in UML called !!!!!!");
			final CompositeFigureFactory compositeFigureFactory = new CompositeFigureFactory();
			IFigureFactory figureFactory = new UmlFigureFactory();
			compositeFigureFactory.addFigureFactory(figureFactory);
			// add default figureFactory from jwt
			figureFactory = new StandardFigureFactory();
			compositeFigureFactory.addFigureFactory(figureFactory);
			this.figureFactory = compositeFigureFactory;
		}

		return figureFactory;
	}


	public EditPartFactory getEditPartFactory()
	{
		if (editPartFactory == null)
		{
			editPartFactory = new JWTEditPartFactory();
		}

		return editPartFactory;
	}

}
