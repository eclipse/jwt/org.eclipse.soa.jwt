/**
 * File:    UmlFigureFactory.java
 * Created: 27.09.2008
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany, http://www.ds-lab.org/
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewuml.figures;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jwt.meta.Plugin;
import org.eclipse.jwt.we.figures.IFigureFactory;
import org.eclipse.jwt.we.misc.views.Views;
import org.eclipse.jwt.we.plugins.viewuml.UmlViewPluginProperties;

/**
 * A custom figure factory that exchanges some Draw2D figures of JWT.
 */
public class UmlFigureFactory implements IFigureFactory
{

	/**
	 * Package that contains the model elements (all model elements are defined
	 * within jwt) resulting string is: org.eclipse.jwt.we.model
	 */
	private static final String MODEL_ROOT_PACKAGE = Plugin.class.getPackage().getName()
			+ ".model";

	/**
	 * Package from this plug-in that contains the figures.
	 * (org.ow2.bonita.figures)
	 */
	private static final String FIGURE_ROOT_PACKAGE = UmlFigureFactory.class.getPackage()
			.getName();

	/**
	 * Postfix for figure class names.
	 */
	private static final String FIGURE_POSTFIX = "Figure";


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.figures.IFigureFactory#createFigure(java.lang.Class)
	 */
	public IFigure createFigure(Class modelType)
	{
		IFigure figure = null;

		// the class name of the figure
		final String figureClass = FIGURE_ROOT_PACKAGE
				+ modelType.getName().substring(MODEL_ROOT_PACKAGE.length())
				+ FIGURE_POSTFIX;
		// instantiate the figure class
		try
		{
			figure = (IFigure) Class.forName(figureClass).newInstance();
		}
		catch (final InstantiationException e)
		{
			throw new RuntimeException("Error creating figure for modelType: "
					+ modelType.getName(), e);
		}
		catch (final IllegalAccessException e)
		{
			throw new RuntimeException("Error creating figure for modelType: "
					+ modelType.getName(), e);
		}
		catch (final ClassNotFoundException e)
		{
			return null;
		}
		return figure;
	}
}
