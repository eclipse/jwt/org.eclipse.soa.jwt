/**
 * File:    UmlViewPluginProperties.java
 * Created: 27.09.2008
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany, http://www.ds-lab.org/
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Messages refactoring to use NLS
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewuml;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.osgi.util.NLS;


public class UmlViewPluginProperties extends NLS
{
	private static final String BUNDLE_NAME = "org.eclipse.jwt.we.plugins.viewuml.plugin"; //$NON-NLS-1$


	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);
	
	private static final Logger logger = Logger.getLogger(UmlViewPluginProperties.class);

	
	
	public static String view_UML_icon;
	public static String view_UML_name;
	
	static {
      NLS.initializeMessages(BUNDLE_NAME, UmlViewPluginProperties.class);
	}
	
	/**
	 * Returns the value from the <code>plugin.properties</code> identified by a key.
	 * 
	 * <p>
	 * It the resource is not available, a warning is logged and the empty String is
	 * returned.
	 * </p>
	 * 
	 * @param key The key of the property.
	 * @return The value of the key.
	 */
	public static String getString(String key)
	{
		try
		{
//			return getResourceLocator().getString(key);
			return RESOURCE_BUNDLE.getString(key);
		}
		catch (MissingResourceException mre)
		{
			logger.warning("The resource " + key + " is missing.", mre);
		}
		return "!"+key+"!";
	}
}
