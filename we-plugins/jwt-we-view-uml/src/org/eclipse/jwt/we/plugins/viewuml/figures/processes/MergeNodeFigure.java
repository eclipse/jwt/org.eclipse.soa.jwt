/**
 * File:    UmlMergeNodeFigure.java
 * Created: 27.09.2008
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany, http://www.ds-lab.org/
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewuml.figures.processes;

import org.eclipse.jwt.we.figures.processes.DiamondFigure;
import org.eclipse.swt.graphics.Image;


public class MergeNodeFigure
		extends DiamondFigure
{
	/**
	 * sets no icon
	 */
	public void setIcon(Image icon) {	
	}
}
