/**
 * File:   DecisionNodeEditPart.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	  Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewbpmn;


import org.eclipse.gef.EditPartFactory;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jwt.we.IFactoryRegistry;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.editors.palette.IPaletteFactory;
import org.eclipse.jwt.we.figures.IFigureFactory;
import org.eclipse.jwt.we.figures.IImageFactory;
import org.eclipse.jwt.we.figures.StandardFigureFactory;
import org.eclipse.jwt.we.figures.internal.CompositeFigureFactory;
import org.eclipse.jwt.we.figures.internal.CompositeImageFactory;
import org.eclipse.jwt.we.figures.internal.ImageFactory;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.parts.CompositeEditPartFactory;
import org.eclipse.jwt.we.parts.JWTEditPartFactory;
import org.eclipse.jwt.we.plugins.viewbpmn.figures.BPMNFigureFactory;
import org.eclipse.jwt.we.plugins.viewbpmn.palette.BPMNPaletteFactory;
import org.eclipse.jwt.we.plugins.viewbpmn.parts.BPMNEditPartFactory;

public class BPMNFactoryRegistry implements IFactoryRegistry
{

	/**
	 * A Logger.
	 */
	private static final Logger logger = Logger.getLogger(BPMNFactoryRegistry.class);

	/**
	 * Path where the icons are located
	 */
	private static final String ICONS_BASE_PATH = "icons/";

	/**
	 * imageFactory
	 */
	protected IImageFactory imageFactory;

	/**
	 * figureFactory
	 */
	protected IFigureFactory figureFactory;

	/**
	 * editPartFactory
	 */
	protected EditPartFactory editPartFactory;

	/**
	 * paletteFactory
	 */
	protected IPaletteFactory paletteFactory;


	public BPMNFactoryRegistry()
	{
		super();
	}


	public IImageFactory getImageFactory(final Object... objects)
	{
		if (imageFactory == null)
		{
			final ImageRegistry imageRegistry = Plugin.getInstance().getImageRegistry();
			final CompositeImageFactory compositeImageFactory = new CompositeImageFactory();
			// add imageFactory for bonita plug-in
			imageFactory = new ImageFactory(imageRegistry, Activator.getDefault(),
					BPMNFactoryRegistry.ICONS_BASE_PATH);
			compositeImageFactory.addImageFactory(imageFactory);
			
			// add imageFactory for jwt-we plug-in
			imageFactory = new ImageFactory(imageRegistry, Plugin.getDefault(),
					org.eclipse.jwt.we.Plugin.ICONS_BASE_PATH);
			
			compositeImageFactory.addImageFactory(imageFactory);
			
			imageFactory = compositeImageFactory;
			return compositeImageFactory;
		}
		return imageFactory;
	}


	public IPaletteFactory getPaletteFactory()
	{
		if (paletteFactory == null)
		{
			paletteFactory = new BPMNPaletteFactory();
		}

		return paletteFactory;
	}


	public IFigureFactory getFigureFactory()
	{
		if (figureFactory == null)
		{
			final CompositeFigureFactory compositeFigureFactory = new CompositeFigureFactory();
			// add figureFactory for bonita plug-in
			IFigureFactory figureFactory = new BPMNFigureFactory();
			compositeFigureFactory.addFigureFactory(figureFactory);
			// add default figureFactory from jwt
			figureFactory = new StandardFigureFactory();
			compositeFigureFactory.addFigureFactory(figureFactory);
			this.figureFactory = compositeFigureFactory;
		}

		return figureFactory;
	}


	public EditPartFactory getEditPartFactory()
	{
		if (editPartFactory == null)
		{
			final CompositeEditPartFactory compositeEditPartFactory = new CompositeEditPartFactory();
			EditPartFactory editPartFactory = new BPMNEditPartFactory();
			compositeEditPartFactory.addEditPartFactory(editPartFactory);
			editPartFactory = new JWTEditPartFactory();
			compositeEditPartFactory.addEditPartFactory(editPartFactory);
			this.editPartFactory = compositeEditPartFactory;
		}

		return editPartFactory;
	}
}
