/**
 * File:    BPMNPaletteFactory.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg
 *     - initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewbpmn.palette;

import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.jwt.we.editors.WEEditor;

public class BPMNPaletteFactory implements
org.eclipse.jwt.we.editors.palette.IPaletteFactory 
{
	public PaletteRoot getPaletteRoot(final WEEditor editor) {
		return new Palette(editor);
	}
}
