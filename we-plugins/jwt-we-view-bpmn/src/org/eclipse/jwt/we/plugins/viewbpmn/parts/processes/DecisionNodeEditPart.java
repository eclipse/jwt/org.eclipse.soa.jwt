/**
 * File:   DecisionNodeEditPart.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	  Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/



package org.eclipse.jwt.we.plugins.viewbpmn.parts.processes;



public class DecisionNodeEditPart
		extends org.eclipse.jwt.we.parts.processes.DecisionNodeEditPart
{

	/**
	 * Maximum number of ingoing edges.
	 */
	private static final int MAXIMUM_IN_EDGES = 99;

	/**
	 * Maximum number of outgoing edges.
	 */
	private static final int MAXIMUM_OUT_EDGES = 99;


	/**
	 * Default constructor.
	 */
	public DecisionNodeEditPart()
	{
		
		setMaximumInActivityEdges(MAXIMUM_IN_EDGES);
		setMaximumOutActivityEdges(MAXIMUM_OUT_EDGES);
	}

}
