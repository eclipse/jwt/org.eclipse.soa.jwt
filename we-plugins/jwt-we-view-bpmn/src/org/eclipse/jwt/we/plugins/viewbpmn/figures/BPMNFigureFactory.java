/**
 * File:    BPMNFigureFactory.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg
 *      - initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewbpmn.figures;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jwt.meta.Plugin;
import org.eclipse.jwt.we.figures.IFigureFactory;

public class BPMNFigureFactory implements IFigureFactory
{

	private static final String MODEL_ROOT_PACKAGE_JWTMETA = Plugin.class.getPackage()
			.getName() + ".model"; //$NON-NLS-1$

	/**
	 * Package from this plug-in that contains the figures.
	 * (org.ow2.bonita.figures)
	 */
	private static final String FIGURE_ROOT_PACKAGE_BPMN = BPMNFigureFactory.class
			.getPackage().getName();

	/**
	 * Postfix for figure class names.
	 */
	private static final String FIGURE_POSTFIX = "Figure"; //$NON-NLS-1$


	/**
	 * creates the figures that are available in this plugin
	 */
	public IFigure createFigure(final Class modelType)
	{
		IFigure figure = null;
		
		String figureClassString = null;
		
		if (modelType.getName().startsWith(MODEL_ROOT_PACKAGE_JWTMETA))
		{
			// model element is in jwt meta plugin
			// -> check if bpmn plugin contains replacement figure for this
			// element
			figureClassString = FIGURE_ROOT_PACKAGE_BPMN
					+ modelType.getName().substring(MODEL_ROOT_PACKAGE_JWTMETA.length())
					+ FIGURE_POSTFIX;
		}
		else
		{
			// other (e.g. view meta model -> return null)
			return null;
		}

		// instantiate the figure class
		try
		{
			figure = (IFigure) Class.forName(figureClassString).newInstance();
		}
		catch (final InstantiationException e)
		{
			throw new RuntimeException(
					"Error creating figure for modelType: " + modelType.getName(), e); //$NON-NLS-1$
		}
		catch (final IllegalAccessException e)
		{
			throw new RuntimeException(
					"Error creating figure for modelType: " + modelType.getName(), e); //$NON-NLS-1$
		}
		catch (final ClassNotFoundException e)
		{
			return null;
		}
		
		return figure;
	}

}
