/**
 * File:    RoundedRectangleBorder.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg
 *      - adapted to BPMN view
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewbpmn.figures.processes;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.swt.graphics.Color;


public class RoundedRectangleBorder
		extends LineBorder
{

	/**
	 * Creates a new RoundedRectangleBorder.
	 */
	public RoundedRectangleBorder()
	{
		super(ColorConstants.black, 3);
	}


	/**
	 * Calculates the corner dimensions relative to a figure.
	 * 
	 * @param figure
	 *            The figure.
	 * @return The current corner dimensions.
	 */
	public Dimension getCornerDimensions(IFigure figure)
	{
		int size = PreferenceReader.appearanceCornerSize.get() * 2;
		return new Dimension(size, size);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Border#getInsets(org.eclipse.draw2d.IFigure)
	 */
	@Override
	public Insets getInsets(IFigure figure)
	{
		/*
		 * 
		 * corner: ,,======= } l //=='' | ,,=' | ,,'\ | ,,' | `\ c-l | // | `\ | // | `\ | // |
		 * `\| ||------|---------| || <---a---> <l><-x--> <--------c------>
		 * 
		 * c = cornersize / 2 l: line width
		 * 
		 * roudedborder is a circle with diameter c rectengular triangle: hypotenuse: c -
		 * l sides: a
		 * 
		 * find inset x
		 * 
		 * 2a² = (c - l)² <=> sqrt(2) * a = c - l <=> a = 1/sqrt(2) * (c - l)
		 * 
		 * x = c - a => x = c - 1/sqrt(2) * (c - l)
		 */

		Dimension corner = getCornerDimensions(figure);

		int inset = (int) Math.ceil(corner.width * 0.5
				- (corner.width * 0.5 - getWidth()) / Math.sqrt(2));

		return new Insets(inset);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Border#paint(org.eclipse.draw2d.IFigure,
	 *      org.eclipse.draw2d.Graphics, org.eclipse.draw2d.geometry.Insets)
	 */
	@Override
	public void paint(IFigure figure, Graphics graphics, Insets insets)
	{
		tempRect.setBounds(getPaintRectangle(figure, insets));
		if (getWidth() % 2 == 1)
		{
			tempRect.width--;
			tempRect.height--;
		}
		tempRect.shrink(getWidth() / 2, getWidth() / 2);

		Dimension corner = getCornerDimensions(figure);

		graphics.setLineWidth(getWidth());

		// draw shadow?
		if (PreferenceReader.appearanceShadowVisible.get())
		{
			// make rectangle smaller and center
			tempRect.height -= 4;
			tempRect.width -= 4;
			tempRect.x += 2;
			tempRect.y += 2;

			// calculate and draw shadow rectangle
			Rectangle shadowRectangle = new Rectangle(tempRect);
			shadowRectangle.x += 3;
			shadowRectangle.y += 3;
			graphics.setAlpha(50);
			graphics.setBackgroundColor(PreferenceReader.appearanceShadowColor.get());
			graphics.fillRoundRectangle(shadowRectangle, corner.width, corner.height);

			// calculate and draw filled original rectangle
			graphics.setAlpha(255);
			graphics.setBackgroundColor(figure.getBackgroundColor());
			tempRect.width += 1;
			tempRect.height += 1;
			graphics.fillRoundRectangle(tempRect, corner.width, corner.height);
			tempRect.width -= 1;
			tempRect.height -= 1;
		}
		//for BPMNs specific
		
		//graphics.setAlpha(200);
		graphics.setBackgroundColor(new Color(null, 70, 130, 180));
		//graphics.setBackgroundColor(new Color(null, 176, 196, 222));
		graphics.setForegroundColor(new Color(null, 0, 0, 0));
		
		
		//graphics.setForegroundColor(new Color(null, 255, 255, 255));

		graphics.fillRoundRectangle(tempRect, corner.width, corner.height);
		graphics.setBackgroundColor(PreferenceReader.appearanceBorderColor.get());
		graphics.drawRoundRectangle(tempRect, corner.width, corner.height);
	}
}
