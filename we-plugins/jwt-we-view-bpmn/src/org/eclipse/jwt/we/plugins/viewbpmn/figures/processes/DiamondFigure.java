/**
 * File:    DiamondFigure.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- moved border stuff in separate class, added shadow
 *    Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg
 *      - adapted to BPMN view
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewbpmn.figures.processes;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.figures.core.IconFigure;


/**
 * A figure that looks like a diamond.
 */
public class DiamondFigure
		extends IconFigure
{

	/**
	 * Creates the figure.
	 */
	public DiamondFigure()
	{
		super();
		setBorder(new DiamondBorder());
		
	}


	/**
	 * @return Returns the bottomPoint.
	 */
	public Point getBottomPoint()
	{
		return ((DiamondBorder) getBorder()).getBottomPoint();
	}


	/**
	 * @return Returns the leftPoint.
	 */
	public Point getLeftPoint()
	{
		return ((DiamondBorder) getBorder()).getLeftPoint();
	}


	/**
	 * @return Returns the rightPoint.
	 */
	public Point getRightPoint()
	{
		return ((DiamondBorder) getBorder()).getRightPoint();
	}


	/**
	 * @return Returns the topPoint.
	 */
	public Point getTopPoint()
	{
		return ((DiamondBorder) getBorder()).getTopPoint();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.IconFigure#getIconClientArea()
	 */
	@Override
	protected Rectangle getIconClientArea()
	{
		Rectangle iconClientArea = super.getIconClientArea();

		// if shadow is activated, resize client area for painting the icon
		if (PreferenceReader.appearanceShadowVisible.get())
		{
			iconClientArea.height -= 10;
			iconClientArea.width -= 10;
			
			iconClientArea.x += 4;
			iconClientArea.y += 4;
		}

		return iconClientArea;
	}

}
