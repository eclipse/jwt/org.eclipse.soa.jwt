/**
 * File:   BPMNEditPartFactory.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	  Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewbpmn.parts;


import org.eclipse.gef.EditPart;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.parts.EditPartAdapter;
import org.eclipse.jwt.we.parts.EditPartAdapterFactory;
import org.eclipse.jwt.we.plugins.viewbpmn.Activator;

public class BPMNEditPartFactory implements org.eclipse.gef.EditPartFactory
{

	/**
	 * The Logger.
	 */
	private static final Logger logger = Logger.getLogger(BPMNEditPartFactory.class);

	/**
	 * Factory for EditParts.
	 */
	EditPartAdapterFactory adapterFactory = new EditPartAdapterFactory(
											BPMNEditPartFactory.class.getPackage().getName(),
											EditPartAdapterFactory.MODEL_VIEW_ROOT_PACKAGE, Activator.getDefault());


	/**
	 * Constructor.
	 */
	public BPMNEditPartFactory()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(EditPart context, Object model)
	{
		
				if (model == null)
					return null;
				
				
				else if (model.getClass().toString().contains("DecisionNode")
						|| model.getClass().toString().contains("ForkNode"))
				{
					
					EditPartAdapter adapter = (EditPartAdapter) adapterFactory.adapt(model,
							EditPartAdapter.class);
					
					if (adapter == null)
						return null;

					return adapter.getEditPart();	
				}
				

				return null;
	}

}
