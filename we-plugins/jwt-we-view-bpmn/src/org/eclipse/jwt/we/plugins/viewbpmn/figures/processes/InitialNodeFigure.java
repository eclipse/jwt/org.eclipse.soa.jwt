/**
 * File:    InitialNodeFigure.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewbpmn.figures.processes;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jwt.we.figures.core.ModelElementFigure;


public class InitialNodeFigure
		extends ModelElementFigure
{

	/**
	 * Creates the figure.
	 */
	public InitialNodeFigure()
	{
		super();
		setBorder(new EllipseBorder(1));
	}
	
	@Override
	public Dimension getPreferredSize(int wHint, int hHint)
	{
		Dimension dimension = new Dimension(30, 30);
		return dimension;
	}
}
