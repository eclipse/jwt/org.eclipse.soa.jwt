/**
 * File:    EllipseBorder.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg
 *      - adapted to BPMN view
 *******************************************************************************/


package org.eclipse.jwt.we.plugins.viewbpmn.figures.processes;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;


/**
 * An ellipse border.
 * 
 * @version $Id: EllipseBorder.java,v 1.10 2009-11-26 12:41:07 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class EllipseBorder
		extends LineBorder
{

	/**
	 * Creates a new EllipseBorder.
	 */
	public EllipseBorder(int width)
	{
		super(ColorConstants.black, width);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.LineBorder#getInsets(org.eclipse.draw2d.IFigure)
	 */
	@Override
	public Insets getInsets(IFigure figure)
	{
		/*
		 * ellipse: width w, height h line width: l inner ellipse: width a = w - l, height
		 * b = h - l => max rectangle: x = a/2 * sqrt(2)
		 * 
		 * insets: i = (w - x) / 2 = w/2 - (w - l)/4 * sqrt(2)
		 */

		Dimension bounds = figure.getSize();
		int i = (int) Math.ceil(bounds.width / 2d - (bounds.width - getWidth()) / 4d
				* Math.sqrt(2));

		return new Insets(i);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.LineBorder#paint(org.eclipse.draw2d.IFigure,
	 *      org.eclipse.draw2d.Graphics, org.eclipse.draw2d.geometry.Insets)
	 */
	@Override
	public void paint(IFigure figure, Graphics graphics, Insets insets)
	{
		tempRect.setBounds(getPaintRectangle(figure, insets));
		if (getWidth() % 2 == 1)
		{
			tempRect.width--;
			tempRect.height--;
		}
		tempRect.shrink(getWidth() / 2, getWidth() / 2);

		graphics.setLineWidth(getWidth());
		if (getColor() != null)
		{
			graphics.setForegroundColor(getColor());
		}

		// draw shadow?
		if (PreferenceReader.appearanceShadowVisible.get())
		{
			// make rectangle smaller and center
			tempRect.height -= 4;
			tempRect.width -= 4;
			tempRect.x += 2;
			tempRect.y += 2;

			// calculate and draw shadow oval
			Rectangle shadowRectangle = new Rectangle(tempRect); 
			shadowRectangle.x += 3;
			shadowRectangle.y += 3;
			graphics.setAlpha(50);
			graphics.setBackgroundColor(PreferenceReader.appearanceShadowColor.get());
			graphics.fillOval(shadowRectangle);

			// calculate and draw filled original oval
			graphics.setAlpha(255);
			graphics.setBackgroundColor(figure.getBackgroundColor());
			tempRect.width += 1;
			tempRect.height += 1;
			graphics.fillOval(tempRect);
			tempRect.width -= 1;
			tempRect.height -= 1;
		}

		graphics.drawOval(tempRect);
	}

}
