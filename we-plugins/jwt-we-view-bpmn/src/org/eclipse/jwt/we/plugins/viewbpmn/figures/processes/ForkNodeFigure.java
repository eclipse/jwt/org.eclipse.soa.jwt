/**
 * File:    ForkNodeFigure.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg
 *      - adapted to BPMN view
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewbpmn.figures.processes;

import java.util.MissingResourceException;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.meta.model.processes.impl.ProcessesFactoryImpl;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.views.Views;
import org.eclipse.swt.graphics.Image;

/**
 * A figure which displays an icon.
 * 
 */
public class ForkNodeFigure extends DiamondFigure
{

	/**
	 * Creates the figure
	 */
	public ForkNodeFigure()
	{
		super();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.figures.core.IconFigure#setIcon(org.eclipse.swt.graphics
	 * .Image)
	 */
	@Override
	public void setIcon(Image icon)
	{
		// use the large icon (without visible bounds) instead of the small one
		String path;
		try
		{
			path = PluginProperties.model_largeIcon(ProcessesFactoryImpl.eINSTANCE
					.createForkNode());
		}
		catch (MissingResourceException e)
		{
			super.setIcon(icon);
			return;
		}
		ImageDescriptor imageDescriptor = Plugin.getInstance().getFactoryRegistry()
				.getImageFactory(Views.getInstance().getSelectedView())
				.createImageDescriptor(path);

		// test if the image is available
		if (imageDescriptor == null || imageDescriptor.getImageData() == null)
		{
			super.setIcon(icon);
			return;
		}
		else
		{
			super.setIcon(Plugin.getDefault().getFactoryRegistry().getImageFactory()
					.getImage(imageDescriptor));
		}
	}
	
	@Override
	public Dimension getPreferredSize(int wHint, int hHint)
	{
		Dimension dimension = new Dimension(40, 40);
		return dimension;
	}

}
