/**
 * File:   Palette.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	  Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg
 *     - adapted to BPMN view
 *******************************************************************************/


package org.eclipse.jwt.we.plugins.viewbpmn.palette;


import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteSeparator;
import org.eclipse.gef.palette.PanningSelectionToolEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.meta.model.events.EventsPackage;
import org.eclipse.jwt.meta.model.processes.ProcessesPackage;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.figures.internal.ImageFactory;
import org.eclipse.jwt.we.figures.internal.ScalingImageDescriptor;
import org.eclipse.jwt.we.misc.factories.EcoreFactory;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.misc.views.Views;
import org.eclipse.jwt.we.plugins.viewbpmn.BPMNViewPluginProperties;
import org.eclipse.swt.graphics.Image;

public class Palette extends PaletteRoot
{
	/**
	 * Size for small icons in the palette.
	 */
	public static final Dimension ICON_SMALL_SIZE = ImageFactory.MODEL_TYPE_IMAGE_SIZE;

	/**
	 * Size for large icons in the palette.
	 */
	public static final Dimension ICON_LARGE_SIZE = new Dimension(24, 24);

	/**
	 * A Logger.
	 */
	private static final Logger logger = Logger.getLogger(Palette.class);


	/**
	 * Constructor
	 */
	public Palette(final WEEditor editor)
	{
		PaletteDrawer activityGroup = null;
		final PaletteGroup palette = new PaletteGroup("BPMN Editor"); //$NON-NLS-1$
		palette.add(new PanningSelectionToolEntry());
		if (activityGroup == null)
		{
			activityGroup = new PaletteDrawer(BPMNViewPluginProperties.palette_ActivityElementsGroups_name);
		}
		// Build palette according on the selected view.
		logger
				.info("Palette building for selected View = " + Views.getInstance().getSelectedView().getInternalName()); //$NON-NLS-1$
		logger
				.info("in properties selected view = " + BPMNViewPluginProperties.view_BPMN_name); //$NON-NLS-1$

		activityGroup.add(createCreationToolEntry(ProcessesPackage.Literals.ACTION));
		activityGroup.add(createCreationToolEntry(ProcessesPackage.Literals.INITIAL_NODE));
		activityGroup.add(createCreationToolEntry(ProcessesPackage.Literals.FINAL_NODE));
		activityGroup.add(createCreationToolEntry(ProcessesPackage.Literals.FORK_NODE));
		activityGroup.add(createCreationToolEntry(ProcessesPackage.Literals.DECISION_NODE));
		
	
		activityGroup.add(new PaletteSeparator());
		
		activityGroup.add(createCreationToolEntry(EventsPackage.Literals.EVENT));
		
		activityGroup.add(new PaletteSeparator());
		
		activityGroup.add(createConnectionCreationToolEntry(ProcessesPackage.Literals.ACTIVITY_EDGE));
		
		palette.add(activityGroup);
		add(palette);
	}
	
	/**
	 * Create a tool to create a new model element.
	 * 
	 * @param modelType
	 *            The type of the model.
	 * @return A CreationToolEntry.
	 */
	public ToolEntry createCreationToolEntry(final EClass modelType)
	{
		final EcoreFactory factory = new EcoreFactory(modelType);

		ImageDescriptor smallIcon = Plugin
				.getInstance()
				.getFactoryRegistry()
				.getImageFactory(Views.getInstance().getSelectedView())
				.createImageDescriptor(BPMNViewPluginProperties.model_smallIcon(modelType));
		
		smallIcon = new ScalingImageDescriptor(smallIcon, ICON_SMALL_SIZE);

		ImageDescriptor largeIcon = Plugin
				.getInstance()
				.getFactoryRegistry()
				.getImageFactory(Views.getInstance().getSelectedView())
				.createImageDescriptor(BPMNViewPluginProperties.model_smallIcon(modelType));
		largeIcon = new ScalingImageDescriptor(largeIcon, ICON_LARGE_SIZE);

		if (!createsValidImage(smallIcon))
		{
			smallIcon = null;
		}
		if (!createsValidImage(largeIcon))
		{
			largeIcon = null;
		}

		return new CombinedTemplateCreationEntry(BPMNViewPluginProperties
				.palette_CreationEntry_name(modelType), BPMNViewPluginProperties
				.palette_CreationEntry_description(modelType), factory, factory,
				smallIcon, largeIcon);
	}
	
	/**
	 * Checks if an ImageDescriptor creates and image or null.
	 * 
	 * @param imageDescriptor
	 *            The ImageDescriptor to check.
	 * @return <code>true</code>, if an image is return, <code>false</code>
	 *         otherwise.
	 */
	public boolean createsValidImage(final ImageDescriptor imageDescriptor)
	{
		boolean result = false;

		final Image image = imageDescriptor.createImage(false);
		if (image != null)
		{
			image.dispose();
			result = true;
		}

		return result;
	}
	
	/**
	 * Create a tool to create a new connection.
	 * 
	 * @param modelType
	 *            The type of the model.
	 * @return A ToolEntry.
	 */
	public ToolEntry createConnectionCreationToolEntry(final EClass modelType)
	{
		final EcoreFactory factory = new EcoreFactory(modelType);

		return new ConnectionCreationToolEntry(BPMNViewPluginProperties
				.palette_CreationEntry_name(modelType), BPMNViewPluginProperties
				.palette_CreationEntry_description(modelType), factory, Plugin
				.getInstance().getFactoryRegistry().getImageFactory(
						Views.getInstance().getSelectedView()).createImageDescriptor(
								BPMNViewPluginProperties.model_smallIcon(modelType)), Plugin
				.getInstance().getFactoryRegistry().getImageFactory(
						Views.getInstance().getSelectedView()).createImageDescriptor(
								BPMNViewPluginProperties.model_largeIcon(modelType)));
	}

}
