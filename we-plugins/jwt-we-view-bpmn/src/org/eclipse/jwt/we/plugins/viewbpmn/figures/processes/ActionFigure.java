/**
 * File:    ActionFigure.java
 * Created: 28.08.2012
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Ahmed Samir Wafa, Programming distributed Systems Lab, University of Augsburg
 *      - adapted to BPMN view
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewbpmn.figures.processes;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jwt.we.figures.core.NamedElementFigure;
import org.eclipse.swt.graphics.Color;


public class ActionFigure
		extends NamedElementFigure
{

	/**
	 * Creates the figure.
	 */
	public ActionFigure()
	{
		
		setBorder(new RoundedRectangleBorder());
		super.getLabel().setForegroundColor(new Color(null, 255, 255, 255));
		
	}


	/**
	 * This function makes sure that the label is shown completely.
	 * 
	 * @see org.eclipse.jwt.we.figures.core.ModelElementFigure#getPreferredSize(int, int)
	 */
	@Override
	public Dimension getPreferredSize(int wHint, int hHint)
	{	
		return getPreferredChildrenSize(wHint, hHint).union(new Dimension(80, 50));
	}
	
}