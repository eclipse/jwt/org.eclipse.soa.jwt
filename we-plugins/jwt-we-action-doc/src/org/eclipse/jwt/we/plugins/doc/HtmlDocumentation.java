package org.eclipse.jwt.we.plugins.doc;

import org.eclipse.jwt.meta.model.core.Model;
import org.eclipse.jwt.meta.model.organisations.Role;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.jwt.meta.model.data.Data;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.jwt.meta.model.organisations.OrganisationUnit;
import org.eclipse.jwt.meta.model.core.Comment;
import org.eclipse.jwt.meta.model.data.InputParameter;
import org.eclipse.jwt.meta.model.data.OutputParameter;
import org.eclipse.jwt.meta.model.data.Parameter;
import org.eclipse.jwt.meta.PluginProperties;
import java.util.*;

public class HtmlDocumentation
{
  protected static String nl;
  public static synchronized HtmlDocumentation create(String lineSeparator)
  {
    nl = lineSeparator;
    HtmlDocumentation result = new HtmlDocumentation();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + " " + NL + "<html>" + NL + "" + NL + "<head>" + NL + "<title>";
  protected final String TEXT_3 = "</title>" + NL + "<link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\">" + NL + "</head>" + NL + "" + NL + "<body link=\"#000000\" vlink=\"#000000\" alink=\"#000000\" >" + NL + "<p>&nbsp;</p>" + NL + "" + NL + "<center>" + NL + "" + NL + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse\" bordercolor=\"#111111\" width=\"95%\" height=\"100%\">" + NL + "" + NL + "\t<tr> <!-- heading line -->" + NL + "    \t<td width=\"1\" height=\"1\" bgcolor=\"#000000\"></td>" + NL + "    \t<td height=\"1\"  bgcolor=\"#000000\"></td>" + NL + "    \t<td width=\"1\" height=\"1\" bgcolor=\"#000000\"></td>" + NL + " \t</tr>" + NL + "" + NL + "" + NL + "<tr>" + NL + "    <td width=\"1\"  bgcolor=\"#000000\"></td> <!--  left line -->" + NL + "    <td id=\"greatcontenttable\" align=\"center\" valign=\"top\">  <!-- beginning content table -->" + NL + "\t" + NL + "\t<br><br><br><br>" + NL + "\t<h1><u>";
  protected final String TEXT_4 = " ";
  protected final String TEXT_5 = "</u></h1>" + NL + "\t<br><br><br><br>" + NL + "\t" + NL + "\t";
  protected final String TEXT_6 = NL + "\t<table id=\"infotable\">" + NL + "\t\t";
  protected final String TEXT_7 = NL + "\t\t<tr>" + NL + "\t\t\t<td>";
  protected final String TEXT_8 = ": </td>" + NL + "\t\t\t<td>";
  protected final String TEXT_9 = "</td>" + NL + "\t\t</tr>" + NL + "\t\t";
  protected final String TEXT_10 = NL + "\t\t";
  protected final String TEXT_11 = NL + "\t\t<tr>" + NL + "\t\t\t<td>";
  protected final String TEXT_12 = ": </td>" + NL + "\t\t\t<td>";
  protected final String TEXT_13 = "</td>" + NL + "\t\t</tr>" + NL + "\t\t";
  protected final String TEXT_14 = NL + "\t\t";
  protected final String TEXT_15 = NL + "\t\t<tr>" + NL + "\t\t\t<td>";
  protected final String TEXT_16 = ": </td>" + NL + "\t\t\t<td>";
  protected final String TEXT_17 = "</td>" + NL + "\t\t</tr>" + NL + "\t\t";
  protected final String TEXT_18 = NL + "\t</table>" + NL + "\t<br><br><br>" + NL + "\t";
  protected final String TEXT_19 = " " + NL + "\t" + NL + "\t<table id=\"navigationtable\" width=\"75%\">" + NL + "      <tr>" + NL + "      \t<td  align=\"center\"><b>" + NL + "        \t<a href=\"#Roles\">";
  protected final String TEXT_20 = "</a></b></td>" + NL + "        <td  align=\"center\"><b>" + NL + "        \t<a href=\"#Applications\">";
  protected final String TEXT_21 = "</a></b></td>" + NL + "        <td  align=\"center\"><b>" + NL + "        \t<a href=\"#Data\">";
  protected final String TEXT_22 = "</a></b></td>" + NL + "        <td  align=\"center\"><b>" + NL + "        \t<a href=\"#Processes\">";
  protected final String TEXT_23 = "</a></b></td>" + NL + "      </tr>" + NL + "\t</table> " + NL + "" + NL + "\t<br><br>" + NL + "" + NL + "\t<!-- Headline of this section -->" + NL + "    <table id=\"heading\" width=\"95%\" >" + NL + "      <tr>" + NL + "        <td width=\"7%\"><hr color=\"#000000\" align=\"right\" size=\"1\"></td>" + NL + "        <td width=\"10%\" align=\"center\">" + NL + "        <b><a name=\"Roles\">";
  protected final String TEXT_24 = "</a></b>" + NL + "        </td>" + NL + "        <td width=\"83%\"><hr color=\"#000000\" size=\"1\"></td>" + NL + "      </tr>" + NL + "    </table>" + NL + "\t<br><br>" + NL + "\t" + NL + "" + NL + "" + NL + "\t";
  protected final String TEXT_25 = NL + "\t<table id=\"scopetable\" width=\"90%\">\t" + NL + "\t" + NL + "\t<tr>" + NL + "\t\t<td>" + NL + "\t\t\t<table id=\"contentheadingtable\" width=\"100%\">" + NL + "\t\t\t\t<tr>" + NL + "\t\t\t\t\t" + NL + "            \t\t<td><p align=\"left\"><b>";
  protected final String TEXT_26 = "</b></p></td>" + NL + "          \t\t</tr>" + NL + "        \t</table>" + NL + "        </td>" + NL + "    </tr>" + NL + "\t" + NL + "\t<tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_27 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_28 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>" + NL + "\t" + NL + "\t";
  protected final String TEXT_29 = NL + "\t<tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_30 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_31 = NL + "\t\t\t\t\t\t";
  protected final String TEXT_32 = NL + "\t\t\t\t\t\t";
  protected final String TEXT_33 = ", ";
  protected final String TEXT_34 = NL + "\t\t\t\t\t</td>\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_35 = NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>";
  protected final String TEXT_36 = NL + "    ";
  protected final String TEXT_37 = NL + "\t<tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_38 = ": </b></td>" + NL + "            \t\t<td>" + NL + "            \t\t";
  protected final String TEXT_39 = NL + "\t\t\t\t\t";
  protected final String TEXT_40 = NL + "\t\t\t\t\t";
  protected final String TEXT_41 = NL + "            \t\t</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>\t\t" + NL + "\t";
  protected final String TEXT_42 = NL + "\t" + NL + "\t</table> <!-- end of this role -->" + NL + "\t<br><br>" + NL + "\t";
  protected final String TEXT_43 = NL + NL + NL + NL + NL + "\t<!-- Headline of this section -->" + NL + "\t<br><br>" + NL + "    <table id=\"heading\" width=\"95%\" >" + NL + "      <tr>" + NL + "        <td width=\"7%\"><hr color=\"#000000\" align=\"right\" size=\"1\"></td>" + NL + "        <td width=\"10%\" align=\"center\">" + NL + "        <b><a name=\"Applications\">";
  protected final String TEXT_44 = "</a></b>" + NL + "        </td>" + NL + "        <td width=\"83%\"><hr color=\"#000000\" size=\"1\"></td>" + NL + "      </tr>" + NL + "" + NL + "    </table>" + NL + "\t<br><br>" + NL;
  protected final String TEXT_45 = NL + "\t<table id=\"scopetable\" width=\"90%\">\t" + NL + "\t" + NL + "\t<tr>" + NL + "\t\t<td>" + NL + "\t\t\t<table id=\"contentheadingtable\" width=\"100%\">" + NL + "\t\t\t\t<tr>" + NL + "\t\t\t\t\t" + NL + "            \t\t<td><p align=\"left\"><b>";
  protected final String TEXT_46 = "</b></p></td>" + NL + "          \t\t</tr>" + NL + "        \t</table>" + NL + "        </td>" + NL + "    </tr>" + NL + "    " + NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_47 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_48 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>" + NL + "    ";
  protected final String TEXT_49 = NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_50 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_51 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>";
  protected final String TEXT_52 = NL + "    ";
  protected final String TEXT_53 = NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_54 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_55 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>";
  protected final String TEXT_56 = NL + "    " + NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_57 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_58 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>" + NL + "    ";
  protected final String TEXT_59 = NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_60 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_61 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>";
  protected final String TEXT_62 = NL + "    ";
  protected final String TEXT_63 = NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_64 = ": </b></td>" + NL + "            \t\t<td>" + NL + "\t\t\t\t\t";
  protected final String TEXT_65 = NL + "\t\t\t\t\t";
  protected final String TEXT_66 = ": ";
  protected final String TEXT_67 = ", ";
  protected final String TEXT_68 = "</td>";
  protected final String TEXT_69 = NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>";
  protected final String TEXT_70 = NL + "    ";
  protected final String TEXT_71 = NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_72 = ": </b></td>" + NL + "            \t\t<td>" + NL + "            \t\t";
  protected final String TEXT_73 = NL + "\t\t\t\t\t";
  protected final String TEXT_74 = ": ";
  protected final String TEXT_75 = ", ";
  protected final String TEXT_76 = "</td>";
  protected final String TEXT_77 = NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>";
  protected final String TEXT_78 = NL + "    ";
  protected final String TEXT_79 = NL + "\t<tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_80 = ": </b></td>" + NL + "            \t\t<td>" + NL + "            \t\t";
  protected final String TEXT_81 = NL + "\t\t\t\t\t";
  protected final String TEXT_82 = NL + "\t\t\t\t\t";
  protected final String TEXT_83 = NL + "            \t\t</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>\t\t" + NL + "\t";
  protected final String TEXT_84 = NL + "    " + NL + "\t</table> <!-- end of this application -->" + NL + "\t<br><br>";
  protected final String TEXT_85 = NL + NL + NL + NL + "\t<!-- Headline of this section -->" + NL + "\t<br><br>" + NL + "    <table id=\"heading\" width=\"95%\" >" + NL + "      <tr>" + NL + "        <td width=\"7%\"><hr color=\"#000000\" align=\"right\" size=\"1\"></td>" + NL + "        <td width=\"10%\" align=\"center\">" + NL + "        <b><a name=\"Data\">";
  protected final String TEXT_86 = "</a></b>" + NL + "        </td>" + NL + "        <td width=\"83%\"><hr color=\"#000000\" size=\"1\"></td>" + NL + "      </tr>" + NL + "" + NL + "    </table>" + NL + "   \t<!-- ENDE ?berschrift der Sektion --> " + NL + "\t<br><br>" + NL;
  protected final String TEXT_87 = NL + "\t<table id=\"scopetable\" width=\"90%\">" + NL + "" + NL + "\t<tr>" + NL + "\t\t<td>" + NL + "\t\t\t<table id=\"contentheadingtable\" width=\"100%\">" + NL + "\t\t\t\t<tr>" + NL + "\t\t\t\t\t" + NL + "            \t\t<td><p align=\"left\"><b>";
  protected final String TEXT_88 = "</b></p></td>" + NL + "          \t\t</tr>" + NL + "        \t</table>" + NL + "        </td>" + NL + "    </tr>" + NL + "    " + NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_89 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_90 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>" + NL + "    ";
  protected final String TEXT_91 = NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_92 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_93 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>";
  protected final String TEXT_94 = NL + "    " + NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_95 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_96 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>" + NL + "    ";
  protected final String TEXT_97 = NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_98 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_99 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>";
  protected final String TEXT_100 = NL + "    ";
  protected final String TEXT_101 = NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_102 = ": </b></td>" + NL + "            \t\t<td>" + NL + "\t\t\t\t\t";
  protected final String TEXT_103 = NL + "\t\t\t\t\t";
  protected final String TEXT_104 = ": ";
  protected final String TEXT_105 = ", ";
  protected final String TEXT_106 = "</td>";
  protected final String TEXT_107 = NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>";
  protected final String TEXT_108 = NL + "    ";
  protected final String TEXT_109 = NL + "\t<tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_110 = ": </b></td>" + NL + "            \t\t<td>" + NL + "            \t\t";
  protected final String TEXT_111 = NL + "\t\t\t\t\t";
  protected final String TEXT_112 = NL + "\t\t\t\t\t";
  protected final String TEXT_113 = NL + "            \t\t</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>\t\t" + NL + "\t";
  protected final String TEXT_114 = NL + NL + "\t</table> <!-- end of this data -->" + NL + "\t<br><br>";
  protected final String TEXT_115 = NL + NL + NL + NL + NL + NL + NL + NL + NL + "\t<!-- Headline of this section -->" + NL + "\t<br><br>" + NL + "    <table id=\"heading\" width=\"95%\" >" + NL + "      <tr>" + NL + "        <td width=\"7%\"><hr color=\"#000000\" align=\"right\" size=\"1\"></td>" + NL + "        <td width=\"10%\" align=\"center\">" + NL + "        <b><a name=\"Processes\">";
  protected final String TEXT_116 = "</a></b>" + NL + "        </td>" + NL + "        <td width=\"83%\"><hr color=\"#000000\" size=\"1\"></td>" + NL + "      </tr>" + NL + "" + NL + "    </table>" + NL + "\t<br><br>" + NL;
  protected final String TEXT_117 = NL + "\t<table id=\"scopetable\" width=\"90%\">" + NL + "" + NL + "\t<tr>" + NL + "\t\t<td>" + NL + "\t\t\t<table id=\"contentheadingtable\" width=\"100%\">" + NL + "\t\t\t\t<tr>" + NL + "\t\t\t\t\t" + NL + "            \t\t<td><p align=\"left\"><b>";
  protected final String TEXT_118 = "</b></p></td>" + NL + "          \t\t</tr>" + NL + "        \t</table>" + NL + "        </td>" + NL + "    </tr>" + NL + "    " + NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_119 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_120 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>" + NL + "    " + NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_121 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_122 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>" + NL + "    ";
  protected final String TEXT_123 = NL + "    <tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_124 = ": </b></td>" + NL + "            \t\t<td>";
  protected final String TEXT_125 = "</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>\t";
  protected final String TEXT_126 = NL + "    ";
  protected final String TEXT_127 = NL + "\t<tr>" + NL + "        <td>" + NL + "        \t<table id=\"contenttable\"  width=\"100%\"> " + NL + "          \t\t<tr>" + NL + "           \t\t\t<td width=\"15%\"><b>";
  protected final String TEXT_128 = ": </b></td>" + NL + "            \t\t<td>" + NL + "            \t\t";
  protected final String TEXT_129 = NL + "\t\t\t\t\t";
  protected final String TEXT_130 = NL + "\t\t\t\t\t";
  protected final String TEXT_131 = NL + "            \t\t</td>" + NL + "          \t\t</tr>" + NL + "       \t\t</table> " + NL + "        </td>" + NL + "    </tr>\t\t" + NL + "\t";
  protected final String TEXT_132 = NL + "\t" + NL + "\t<tr>" + NL + "\t\t<td>" + NL + "\t\t\t<table id=\"contenttable\" width=\"100%\">" + NL + "\t\t\t\t<tr>" + NL + "\t\t\t\t\t<td><img src=process";
  protected final String TEXT_133 = ".jpg></img>" + NL + "\t\t\t\t\t</td>" + NL + "\t\t\t\t</tr>" + NL + "\t\t\t</table>" + NL + "\t\t</td>" + NL + "\t</tr>" + NL + "" + NL + "" + NL + "\t</table> <!-- end of this process -->" + NL + "\t<br><br>";
  protected final String TEXT_134 = NL + NL + NL + NL + NL + NL + NL + NL + NL + NL + "\t" + NL + "\t" + NL + "\t</td> <!-- end of great content table -->" + NL + "\t" + NL + "\t<td width=\"1\"  bgcolor=\"#000000\"></td> <!--  right line -->" + NL + "    </tr> " + NL + "    " + NL + "    <tr> <!-- bottom line -->" + NL + "    \t<td width=\"1\" height=\"1\" bgcolor=\"#000000\"></td>" + NL + "    \t<td height=\"1\"  bgcolor=\"#000000\"></td>" + NL + "\t    <td width=\"1\" height=\"1\" bgcolor=\"#000000\"></td>" + NL + "\t  </tr>" + NL + "\t" + NL + "" + NL + "\t</table> <!-- END outer table->" + NL + "" + NL + "   " + NL + "    " + NL + "   </center>" + NL + "" + NL + "<br>" + NL + "" + NL + "</body>" + NL + "" + NL + "</html>" + NL + "\t\t\t" + NL + "\t\t\t";
  protected final String TEXT_135 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     Object[] arguments = (Object[])argument;
   Model model = (Model)arguments[0];
   List roles = (List)arguments[1];
   List applications = (List)arguments[2];
   List datas = (List)arguments[3];
   List processes = (List)arguments[4];
   int counter = 0;
 
    stringBuffer.append(TEXT_2);
    stringBuffer.append( model.getName() );
    stringBuffer.append(TEXT_3);
    stringBuffer.append( Messages.doc_title);
    stringBuffer.append(TEXT_4);
    stringBuffer.append( model.getName() );
    stringBuffer.append(TEXT_5);
     if (model.getAuthor() != "" || model.getVersion() != "" || model.getDescription() != "") { 
    stringBuffer.append(TEXT_6);
     if (model.getAuthor() != "") { 
    stringBuffer.append(TEXT_7);
    stringBuffer.append( PluginProperties.getString("model_Model_author_name") );
    stringBuffer.append(TEXT_8);
    stringBuffer.append( model.getAuthor() );
    stringBuffer.append(TEXT_9);
     } 
    stringBuffer.append(TEXT_10);
     if (model.getVersion() != "") { 
    stringBuffer.append(TEXT_11);
    stringBuffer.append( PluginProperties.getString("model_Model_version_name") );
    stringBuffer.append(TEXT_12);
    stringBuffer.append( model.getVersion() );
    stringBuffer.append(TEXT_13);
     } 
    stringBuffer.append(TEXT_14);
     if (model.getDescription() != "") { 
    stringBuffer.append(TEXT_15);
    stringBuffer.append( PluginProperties.getString("model_Model_description_name") );
    stringBuffer.append(TEXT_16);
    stringBuffer.append( model.getDescription() );
    stringBuffer.append(TEXT_17);
     } 
    stringBuffer.append(TEXT_18);
     } 
    stringBuffer.append(TEXT_19);
    stringBuffer.append( Messages.doc_heading_roles );
    stringBuffer.append(TEXT_20);
    stringBuffer.append( Messages.doc_heading_applications );
    stringBuffer.append(TEXT_21);
    stringBuffer.append( Messages.doc_heading_data );
    stringBuffer.append(TEXT_22);
    stringBuffer.append( Messages.doc_heading_processes );
    stringBuffer.append(TEXT_23);
    stringBuffer.append( Messages.doc_heading_roles );
    stringBuffer.append(TEXT_24);
     for (Iterator i = roles.iterator(); i.hasNext();) {
	Role role = (Role)i.next(); 
    stringBuffer.append(TEXT_25);
    stringBuffer.append(role.getName() );
    stringBuffer.append(TEXT_26);
    stringBuffer.append( PluginProperties.getString("model_PackageableElement_package_name") );
    stringBuffer.append(TEXT_27);
    stringBuffer.append( role.getPackage().getName() );
    stringBuffer.append(TEXT_28);
     List pB = role.getPerformedBy();
		if (!pB.isEmpty()) { 
    stringBuffer.append(TEXT_29);
    stringBuffer.append( PluginProperties.getString("model_Role_performedBy_name") );
    stringBuffer.append(TEXT_30);
      for (int j = 0; j < pB.size(); j++) {
						OrganisationUnit unit = (OrganisationUnit)pB.get(j); 
    stringBuffer.append(TEXT_31);
    stringBuffer.append( unit.getName() );
    stringBuffer.append(TEXT_32);
     if (j < pB.size()-1) { 
    stringBuffer.append(TEXT_33);
     } 
    stringBuffer.append(TEXT_34);
     } 
    stringBuffer.append(TEXT_35);
     } 
    stringBuffer.append(TEXT_36);
     List comments = role.getOwnedComment();
		if (!comments.isEmpty()) { 
    stringBuffer.append(TEXT_37);
    stringBuffer.append( PluginProperties.getString("model_ModelElement_ownedComment_name") );
    stringBuffer.append(TEXT_38);
     for (Iterator k = comments.iterator(); k.hasNext();) {
					Comment com = (Comment)k.next(); 
    stringBuffer.append(TEXT_39);
    stringBuffer.append( com.getText() );
    stringBuffer.append(TEXT_40);
     } 
    stringBuffer.append(TEXT_41);
     } 
    stringBuffer.append(TEXT_42);
     } 
    stringBuffer.append(TEXT_43);
    stringBuffer.append( Messages.doc_heading_applications );
    stringBuffer.append(TEXT_44);
     for (Iterator i = applications.iterator(); i.hasNext();) {
	Application app = (Application)i.next(); 
    stringBuffer.append(TEXT_45);
    stringBuffer.append(app.getName() );
    stringBuffer.append(TEXT_46);
    stringBuffer.append( PluginProperties.getString("model_PackageableElement_package_name") );
    stringBuffer.append(TEXT_47);
    stringBuffer.append( app.getPackage().getName() );
    stringBuffer.append(TEXT_48);
     if (app.getType() != null) { 
    stringBuffer.append(TEXT_49);
    stringBuffer.append( PluginProperties.getString("model_Application_type_name") );
    stringBuffer.append(TEXT_50);
    stringBuffer.append( app.getType().getName() );
    stringBuffer.append(TEXT_51);
     } 
    stringBuffer.append(TEXT_52);
     if (app.getJarArchive() != "" && app.getJarArchive() != null) { 
    stringBuffer.append(TEXT_53);
    stringBuffer.append( PluginProperties.getString("model_Application_jarArchive_name") );
    stringBuffer.append(TEXT_54);
    stringBuffer.append( app.getJarArchive() );
    stringBuffer.append(TEXT_55);
     } 
    stringBuffer.append(TEXT_56);
    stringBuffer.append( PluginProperties.getString("model_Application_javaClass_name") );
    stringBuffer.append(TEXT_57);
    stringBuffer.append( app.getJavaClass() );
    stringBuffer.append(TEXT_58);
     if (app.getMethod() != "" && app.getMethod() != null) { 
    stringBuffer.append(TEXT_59);
    stringBuffer.append( PluginProperties.getString("model_Application_method_name") );
    stringBuffer.append(TEXT_60);
    stringBuffer.append( app.getMethod() );
    stringBuffer.append(TEXT_61);
     } 
    stringBuffer.append(TEXT_62);
     List input = app.getInput(); 
		if (!input.isEmpty()) { 
    stringBuffer.append(TEXT_63);
    stringBuffer.append( PluginProperties.getString("model_Application_input_name") );
    stringBuffer.append(TEXT_64);
     for (int j = 0; j < input.size(); j++) {
					InputParameter iparam = (InputParameter)input.get(j);
    stringBuffer.append(TEXT_65);
    stringBuffer.append( iparam.getName() );
    stringBuffer.append(TEXT_66);
    stringBuffer.append( iparam.getValue() );
     if (j < input.size()-1) { 
    stringBuffer.append(TEXT_67);
     } 
    stringBuffer.append(TEXT_68);
     } 
    stringBuffer.append(TEXT_69);
     } 
    stringBuffer.append(TEXT_70);
     List output = app.getOutput();
		if (!output.isEmpty()) { 
    stringBuffer.append(TEXT_71);
    stringBuffer.append( PluginProperties.getString("model_Application_output_name") );
    stringBuffer.append(TEXT_72);
      for (int k = 0; k < output.size(); k++) {
					OutputParameter oparam = (OutputParameter)output.get(k);
    stringBuffer.append(TEXT_73);
    stringBuffer.append( oparam.getName() );
    stringBuffer.append(TEXT_74);
    stringBuffer.append( oparam.getValue() );
     if (k < output.size()-1) { 
    stringBuffer.append(TEXT_75);
     } 
    stringBuffer.append(TEXT_76);
     } 
    stringBuffer.append(TEXT_77);
     } 
    stringBuffer.append(TEXT_78);
     List comments = app.getOwnedComment();
		if (!comments.isEmpty()) { 
    stringBuffer.append(TEXT_79);
    stringBuffer.append( PluginProperties.getString("model_ModelElement_ownedComment_name") );
    stringBuffer.append(TEXT_80);
     for (Iterator k = comments.iterator(); k.hasNext();) {
					Comment com = (Comment)k.next(); 
    stringBuffer.append(TEXT_81);
    stringBuffer.append( com.getText() );
    stringBuffer.append(TEXT_82);
     } 
    stringBuffer.append(TEXT_83);
     } 
    stringBuffer.append(TEXT_84);
     } 
    stringBuffer.append(TEXT_85);
    stringBuffer.append( Messages.doc_heading_data );
    stringBuffer.append(TEXT_86);
     for (Iterator i = datas.iterator(); i.hasNext();) {
	Data data = (Data)i.next(); 
    stringBuffer.append(TEXT_87);
    stringBuffer.append(data.getName() );
    stringBuffer.append(TEXT_88);
    stringBuffer.append( PluginProperties.getString("model_PackageableElement_package_name") );
    stringBuffer.append(TEXT_89);
    stringBuffer.append( data.getPackage().getName() );
    stringBuffer.append(TEXT_90);
     if (data.getValue() != "" && data.getValue() != null) { 
    stringBuffer.append(TEXT_91);
    stringBuffer.append( PluginProperties.getString("model_Data_value_name") );
    stringBuffer.append(TEXT_92);
    stringBuffer.append( data.getValue() );
    stringBuffer.append(TEXT_93);
     } 
    stringBuffer.append(TEXT_94);
    stringBuffer.append( PluginProperties.getString("model_Data_dataType_name") );
    stringBuffer.append(TEXT_95);
     if (data.getDataType() != null) { 
    stringBuffer.append( data.getDataType().getName() );
     } 
    stringBuffer.append(TEXT_96);
     if (data.getInformationType() != null) { 
    stringBuffer.append(TEXT_97);
    stringBuffer.append( PluginProperties.getString("model_Data_informationType_name") );
    stringBuffer.append(TEXT_98);
    stringBuffer.append( data.getInformationType().getName() );
    stringBuffer.append(TEXT_99);
     } 
    stringBuffer.append(TEXT_100);
      List parameter = data.getParameters(); if (!parameter.isEmpty()) { 
    stringBuffer.append(TEXT_101);
    stringBuffer.append( PluginProperties.getString("model_Data_parameters_name") );
    stringBuffer.append(TEXT_102);
     for (int j = 0; j < parameter.size(); j++) {
					Parameter param = (Parameter)parameter.get(j); 
    stringBuffer.append(TEXT_103);
    stringBuffer.append( param.getName() );
    stringBuffer.append(TEXT_104);
    stringBuffer.append( param.getValue() );
     if (j < parameter.size()-1) { 
    stringBuffer.append(TEXT_105);
     } 
    stringBuffer.append(TEXT_106);
     } 
    stringBuffer.append(TEXT_107);
     } 
    stringBuffer.append(TEXT_108);
     List comments = data.getOwnedComment();
		if (!comments.isEmpty()) { 
    stringBuffer.append(TEXT_109);
    stringBuffer.append( PluginProperties.getString("model_ModelElement_ownedComment_name") );
    stringBuffer.append(TEXT_110);
     for (Iterator k = comments.iterator(); k.hasNext();) {
					Comment com = (Comment)k.next(); 
    stringBuffer.append(TEXT_111);
    stringBuffer.append( com.getText() );
    stringBuffer.append(TEXT_112);
     } 
    stringBuffer.append(TEXT_113);
     } 
    stringBuffer.append(TEXT_114);
     } 
    stringBuffer.append(TEXT_115);
    stringBuffer.append( Messages.doc_heading_processes );
    stringBuffer.append(TEXT_116);
     for (Iterator i = processes.iterator(); i.hasNext();) {
	Activity process = (Activity)i.next(); 
    stringBuffer.append(TEXT_117);
    stringBuffer.append(process.getName() );
    stringBuffer.append(TEXT_118);
    stringBuffer.append( PluginProperties.getString("model_PackageableElement_package_name") );
    stringBuffer.append(TEXT_119);
    stringBuffer.append( process.getPackage().getName() );
    stringBuffer.append(TEXT_120);
    stringBuffer.append( PluginProperties.getString("model_Activity_totalexecutiontime_name") );
    stringBuffer.append(TEXT_121);
    stringBuffer.append( process.getTotalexecutiontime() );
    stringBuffer.append(TEXT_122);
     if (process.getEventHandler() != null) {
    stringBuffer.append(TEXT_123);
    stringBuffer.append( PluginProperties.getString("model_Activity_eventHandler_name") );
    stringBuffer.append(TEXT_124);
    stringBuffer.append( process.getEventHandler().getName() );
    stringBuffer.append(TEXT_125);
     } 
    stringBuffer.append(TEXT_126);
     List comments = process.getOwnedComment();
		if (!comments.isEmpty()) { 
    stringBuffer.append(TEXT_127);
    stringBuffer.append( PluginProperties.getString("model_ModelElement_ownedComment_name") );
    stringBuffer.append(TEXT_128);
     for (Iterator k = comments.iterator(); k.hasNext();) {
					Comment com = (Comment)k.next(); 
    stringBuffer.append(TEXT_129);
    stringBuffer.append( com.getText() );
    stringBuffer.append(TEXT_130);
     } 
    stringBuffer.append(TEXT_131);
     } 
    stringBuffer.append(TEXT_132);
    stringBuffer.append( ++counter );
    stringBuffer.append(TEXT_133);
     } 
    stringBuffer.append(TEXT_134);
    stringBuffer.append(TEXT_135);
    return stringBuffer.toString();
  }
}
