package org.eclipse.jwt.we.plugins.doc;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	public static String doc_dialog_title;
	public static String doc_dialog_message;
	public static String doc_preferences_description;
	public static String doc_preferences_path;
	public static String doc_title;
	public static String doc_heading_roles;
	public static String doc_heading_applications;
	public static String doc_heading_data;
	public static String doc_heading_processes;
	
	private static final String BUNDLE_NAME = "org.eclipse.jwt.we.plugins.doc.docplugin"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
		.getBundle(BUNDLE_NAME);
	
	private static final Logger logger = Logger.getLogger(Messages.class);


	static {
		initializeMessages(BUNDLE_NAME, Messages.class);
	}
	
	/**
	 * Returns the value from the <code>plugin.properties</code> identified by a key.
	 * 
	 * <p>
	 * It the resource is not available, a warning is logged and the empty String is
	 * returned.
	 * </p>
	 * 
	 * @param key The key of the property.
	 * @return The value of the key.
	 */
	public static String getString(String key)
	{
		try
		{
			return RESOURCE_BUNDLE.getString(key);
		}
		catch (MissingResourceException mre)
		{
			logger.warning("The resource " + key + " is missing.", mre);  //$NON-NLS-1$ //$NON-NLS-2$
		}
		return "!"+key+"!"; //$NON-NLS-1$ //$NON-NLS-2$
	}
}
