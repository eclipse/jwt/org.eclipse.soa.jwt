/**
 * File:    PreferenceConstants.java
 * Created: 24.09.2007
 *
 *
 *******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Seitz, Programming distributed Systems Lab, University of Augsburg - initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg - smaller changes
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.doc;

/**
 * Constant definitions for plug-in preferences
 * 
 * @author Christian Seitz (chr.seitz@gmx.de), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org.
 */
public class PreferenceConstants
{

	public static final String P_CSSPATH = "csspathPreference"; //$NON-NLS-1$

}
