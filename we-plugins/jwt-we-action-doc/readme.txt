*******************************************************
**********READMEFILE FOR JWT-WE-DOC 1.0****************
*******************************************************

 *******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Seitz, Programming distributed Systems Lab, University of Augsburg
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *******************************************************************************


This readme file contains installation instructions to use this plugin 
with either JWT-WE or the RCP application AgilPro LiMo and a guide how 
to create and use user-designed style sheets with JWT-WE-DOC.

1. Installation instructions

JWT-WE-DOC 1.0 is available as a zip file containing the following files:
- org.eclipse.jwt.we.plugins.doc_1.0.0.jar: the plugin for automatic 
  documentation generation
- style.css: the cascading style sheet used by jwt-we-doc
- readme.txt: this readme file

Installing jwt-we-doc is quite simple. The org.eclipse.jwt.we.plugins.doc_1.0.0.jar 
just has to be copied to the right place:
- JWT-WE as Eclipse plugin: copy the jar into the plugins folder of your 
  Eclipse application.
- AgilPro LiMo: copy the jar into the plugin folder of your AgilPro 
  installation.

Now jwt-we-doc is integrated into WE / AgilPro LiMo (additional toolbar 
entry + menu entry in the LiMo).

If for some reasons the plugin doesn't find the standard stylesheet, you 
will be asked to set the location of the css-file manually. In that case 
please use the style.css file you find in this zip, store it somewhere on 
your disk and set the path in the preferences (Window->Preferences->Workflow 
Editor->Documentation).

*******************************************************
*******************************************************
*******************************************************

2. Create and use customized Cascading Style Sheets

If the standard appearance of the generated documentation doesn't suit your 
liking, feel free to create and use your own style sheet. We recommend to 
use the style.css file in this zip as a starting point for the changes you 
want to make.

There are several custom sections that are responsible for certain parts 
of the documentation. Here is a short description of these sections for 
easier identification of the places responsible for the changes you want 
to make:

#greatcontenttable: This is the surrounding table that contains all elements 
 of the documentation.
#infotable: Displays the general information of each model file such as Author, 
 Description etc.
#navigationtable: This are the navigation links at the beginning of the documentation
#heading: The headings of each part
#scopetable: This table contains all the elements of each section (Roles, 
 Applications...)
#contentheadingtable: The heading of each entry in a section (say e.g. the 
 name of each Role)
#contenttable: The content of each entry in a section (e.g. Package, Comment...)

To use your own stylesheet for the documentation generation just set the path 
to this file in the preferences (Window->Preferences->Workflow Editor->Documentation).
