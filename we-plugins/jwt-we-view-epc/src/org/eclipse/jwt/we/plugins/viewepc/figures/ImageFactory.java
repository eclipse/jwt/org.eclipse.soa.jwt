/**
 * File:    ImageFactory.java
 * Created: 25.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Florian Lautenbacher, adapted it to EPC view
 *******************************************************************************/


package org.eclipse.jwt.we.plugins.viewepc.figures;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.plugins.viewepc.Activator;
import org.eclipse.ui.plugin.AbstractUIPlugin;


/**
 * Creates images. It uses an {@link ImageRegistry} for disposing the images.
 * 
 * @version $Id: ImageFactory.java,v 1.3 2009-11-26 12:38:17 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class ImageFactory
{

	/**
	 * Path where the icons are located.
	 */
	public static final String ICONS_BASE_PATH = "icons/"; //$NON-NLS-1$

	/**
	 * The size for images representing a model type.
	 */
	public static final Dimension MODEL_TYPE_IMAGE_SIZE = new Dimension(16, 16);


	/**
	 * @param imageRegistry
	 *            The registry for disposing the images.
	 */
	public ImageFactory(ImageRegistry imageRegistry)
	{
		assert imageRegistry != null;
	}


	/**
	 * Returns an image descriptor for the image file at the given plug-in relative path.
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor createImageDescriptor(String path)
	{
		ImageDescriptor result = AbstractUIPlugin.imageDescriptorFromPlugin(Activator
				.getId(), path);
		/**
		 * If the icon could not be found (for example because the user put it in a
		 * different folder than icons, load it a snd time but this time from the original plugin
		 */
		if (result == null)
			result = AbstractUIPlugin.imageDescriptorFromPlugin(Plugin.getId(), path);
		return result;
	}
}
