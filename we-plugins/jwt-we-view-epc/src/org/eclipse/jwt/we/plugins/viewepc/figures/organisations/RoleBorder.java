/**
 * File:    RoleBorder.java
 * Created: 12.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Florian Lautenbacher, adapted to EPC organizations
 *******************************************************************************/


package org.eclipse.jwt.we.plugins.viewepc.figures.organisations;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.swt.graphics.Color;


/**
 * An role border: an EllipseBorder with an additional line
 * 
 */
public class RoleBorder extends LineBorder
{

	/**
	 * Creates a new EllipseBorder.
	 */
	public RoleBorder()
	{
		super(PreferenceReader.appearanceBorderColor.get(), PreferenceReader.appearanceLineWidth.get());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.LineBorder#getInsets(org.eclipse.draw2d.IFigure)
	 */
	@Override
	public Insets getInsets(IFigure figure)
	{
		/*
		 * ellipse: width w, height h line width: l inner ellipse: width a = w - l, height
		 * b = h - l => max rectangle: x = a/2 * sqrt(2)
		 * 
		 * insets: i = (w - x) / 2 = w/2 - (w - l)/4 * sqrt(2)
		 */

		Dimension bounds = figure.getSize();
		int i = (int) Math.ceil(bounds.width / 2d - (bounds.width - getWidth()) / 4d
				* Math.sqrt(2));

		return new Insets(i);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.LineBorder#paint(org.eclipse.draw2d.IFigure,
	 *      org.eclipse.draw2d.Graphics, org.eclipse.draw2d.geometry.Insets)
	 */
	@Override
	public void paint(IFigure figure, Graphics graphics, Insets insets)
	{
		tempRect.setBounds(getPaintRectangle(figure, insets));
		if (getWidth() % 2 == 1)
		{
			tempRect.width--;
			tempRect.height--;
		}
		tempRect.shrink(getWidth() / 2, getWidth() / 2);

		graphics.setLineWidth(getWidth());
		if (getColor() != null)
		{
			graphics.setForegroundColor(getColor());
		}

		// draw shadow?
		if (PreferenceReader.appearanceShadowVisible.get())
		{
			// make rectangle smaller and center
			tempRect.height -= 4;
			tempRect.width -= 4;
			tempRect.x += 2;
			tempRect.y += 2;

			// calculate and draw shadow oval
			Rectangle shadowRectangle = new Rectangle(tempRect); 
			shadowRectangle.x += 3;
			shadowRectangle.y += 3;
			graphics.setAlpha(50);
			graphics.setBackgroundColor(PreferenceReader.appearanceShadowColor.get());
			graphics.fillOval(shadowRectangle);

			// calculate and draw filled original oval
			graphics.setAlpha(255);
			graphics.setBackgroundColor(figure.getBackgroundColor());
			tempRect.width += 1;
			tempRect.height += 1;
			graphics.fillOval(tempRect);
			tempRect.width -= 1;
			tempRect.height -= 1;
		}

		graphics.setAlpha(50);
		graphics.setBackgroundColor(new Color(null, 255, 215, 10));
		//graphics.setForegroundColor(new Color(null, 255, 215, 10));

		graphics.fillOval(tempRect);
		
		graphics.setBackgroundColor(PreferenceReader.appearanceBorderColor.get());
		graphics.drawOval(tempRect);
		
		int xb = tempRect.x+(tempRect.width / 7);
		int dy = tempRect.height / 7;
		int yb1 = tempRect.y+dy;
		int yb2 = tempRect.y+tempRect.height-dy; //((1-(4*xb*tempRect.x)/(tempRect.width*tempRect.width))*tempRect.height*tempRect.height/4)/tempRect.y;		
		
		Point lineTop = new Point(xb, yb1);
		Point lineBottom = new Point(xb, yb2);
		graphics.drawLine(lineTop, lineBottom);
	}

}

