/**
 * File:    EpcViewPluginProperties.java
 * Created: 30.01.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany, http://www.ds-lab.org/
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewepc;

import java.text.MessageFormat;
import java.util.MissingResourceException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osgi.util.NLS;

public class EpcViewPluginProperties extends NLS
{
	// "epcplugin" references the epplugin[_locale].properties file path
	// at the root of the jwt-we-view-epc bundle.
	private static final String BUNDLE_NAME = "epcplugin"; //$NON-NLS-1$

	public static String view_EPC_icon;
	public static String view_EPC_name;
	
	public static String model_Default_smallIcon;
	public static String model_Default_largeIcon;

	public static String model_Xor_connector_icon ; 
	public static String model_Xor_connector_largeIcon ;
	public static String model_And_connector_icon ; 
	public static String model_And_connector_largeIcon ;
	
	public static String model_DecisionNode_icon;
	public static String model_DecisionNode_largeIcon;
	public static String model_MergeNode_icon;
	public static String model_MergeNode_largeIcon;
	public static String model_ForkNode_icon;
	public static String model_ForkNode_largeIcon;
	public static String model_JoinNode_icon;
	public static String model_JoinNode_largeIcon;
	
	public static String model_InitialNode_icon;
	public static String model_InitialNode_largeIcon;
	public static String model_FinalNode_icon;
	public static String model_FinalNode_largeIcon;
	public static String model_Action_icon;
	public static String model_Action_largeIcon;
	public static String model_Event_icon;
	public static String model_Event_largeIcon;
	
	public static String model_ActivityEdge_icon;
	public static String model_ActivityEdge_largeIcon;
	
	public static String model_EPCEvent_icon;
	public static String model_EPCEvent_largeIcon;

	public static String model_ExecutableNode_icon;
	public static String model_ExecutableNode_largeIcon;

	public static String model_ForkNode_type;
	public static String model_DecisionNode_type;
	public static String model_InitialNode_type;
	public static String model_FinalNode_type;
	public static String model_Action_type;
	public static String model_ActivityEdge_type;
	public static String model_EPCEvent_type;
	public static String model_ExecutableNode_type;
	/**
	 * Name of group in the palette that contains the activity elements.
	 */
	public static String palette_ActivityElementsGroups_name;
	public static String palette_CreationEntry_description;

	
	static {
      NLS.initializeMessages(BUNDLE_NAME, EpcViewPluginProperties.class);
	}
	
	/**
	 * Returns a path for an icon for a model element.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @param largeIcon
	 *            If <code>true</code>, a large icon will be retured, otherwise a small
	 *            icon.
	 * @return A path to an icon.
	 */
	public static String model_icon(Object model, boolean largeIcon)
	{
		String modelName = getModelTypeName(model);
		String result = null;
		String imageType = largeIcon ? "_largeIcon" : "_smallIcon";  //$NON-NLS-1$ //$NON-NLS-2$

		// try to get image name from resource bundle
		try
		{
			result = getStringExpectMissing("model_" + modelName + imageType);  //$NON-NLS-1$
		}
		catch (MissingResourceException e)
		{
		}

		if (result == null)
		{
			// no small image found, try normal image
			try
			{
				result = model_icon(model);
			}
			catch (MissingResourceException e)
			{
			}

			// if image not found, try loading default image
			if (result == null)
			{
				result = getStringExpectMissing("model_Default" + imageType);  //$NON-NLS-1$
			}
		}

		return result;
	}
	
	/**
	 * Returns a path for an icon for a model element.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return A path to an icon.
	 */
	public static String model_icon(Object model)
	{
		String modelName = getModelTypeName(model);
		String result = null;

		// try to get image name from resource bundle
		try
		{
			result = getStringExpectMissing("model_" + modelName + "_icon");  //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (MissingResourceException e)
		{
		}

		// if image not found, try loading default image
		if (result == null)
		{
			try
			{
				result = getStringExpectMissing("model_Default_icon");  //$NON-NLS-1$
			}
			catch (MissingResourceException e)
			{
			}
		}

		return result;
	}
	
	/**
	 * Returns a path for a small icon for a model element.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return A path to a small icon.
	 */
	public static String model_smallIcon(Object model)
	{
		return model_icon(model, false);
	}

	/**
	 * Returns a path for a large icon for a model element.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return A path to a large icon.
	 */
	public static String model_largeIcon(Object model)
	{
		return model_icon(model, true);
	}

	
	/**
	 * Creates a description for a creation entry of a model element.
	 * 
	 * @param model
	 *            The class of the model that is created.
	 * @return The content of the description.
	 */
	public static String palette_CreationEntry_description(Object model)
	{
		String entryName = palette_CreationEntry_name(model);
		String typeName = model_type(model);
		return MessageFormat.format(palette_CreationEntry_description,
				new Object[]
				{ entryName, typeName });
	}
	
	/**
	 * Creates a name for a creation entry of a model element.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return The name for the entry.
	 */
	public static String palette_CreationEntry_name(Object model)
	{
		return model_type(model);
	}


	
	/**
	 * Returns a simple representing name of the type of an object.
	 * 
	 * @param object
	 *            The object, may be <code>null</code>.
	 * @return The name of the type.
	 */
	public static String getModelTypeName(Object object)
	{
		if (object == null)
		{
			return "null";  //$NON-NLS-1$
		}
		if (object instanceof EClass)
		{
			return ((EClass) object).getName();
		}
		if (object instanceof Class<?>)
		{
			return ((Class<?>) object).getSimpleName();
		}
		if (object instanceof EObject)
		{
			return ((EObject) object).eClass().getName();
		}
		return object.getClass().getSimpleName();
	}
	
	/**
	 * The name of the type of a model.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return A name of the type.
	 */
	public static String model_type(Object model)
	{
		if (model instanceof EAttribute)
		{
			return model_datatype((EAttribute) model);
		}

		String modelType = getModelTypeName(model);
		try
		{
			return getStringExpectMissing("model_" + modelType + "_type"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (MissingResourceException e)
		{
		}

		return modelType;
	}

	/**
	 * The name of an attribute.
	 * 
	 * @param attribute
	 *            The attribute.
	 * @return A name of the type.
	 */
	public static String model_datatype(EAttribute attribute)
	{
		String name = attribute.getName();
		try
		{
			return getStringExpectMissing("model_" + name + "_datatype"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (MissingResourceException e)
		{
		}

		return name;
	}


	/**
	 * Returns the value from the <code>plugin.properties</code> identified by a key.
	 * 
	 * <p>
	 * It the resource is not available a {@link MissingResourceException} is thrown.
	 * </p>
	 * 
	 * @param key
	 *            The key of the property.
	 * @return The value of the key.
	 * @throws MissingResourceException
	 *             If the resource can not be found.
	 */
	public static String getStringExpectMissing(String key)
			throws MissingResourceException
	{
		try {
			String res = (String)EpcViewPluginProperties.class.getField(key).get(null); 
			if (res != null)
				return res;
			else
				throw new NullPointerException();
		} catch (Exception ex) {
			throw new MissingResourceException("Cannot find value associated to key [" + key + "]", EpcViewPluginProperties.class.getName(), key); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}
}
