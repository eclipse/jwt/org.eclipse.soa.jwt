/**
 * File:    EpcPaletteFactory.java
 * Created: 10.03.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewepc.editor.palette;

import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.jwt.we.editors.WEEditor;

/**
 * Palette factory that provides the root palette of the Epc View Editor
 * @author Florian Lautenbacher
 */
public class EpcPaletteFactory implements
		org.eclipse.jwt.we.editors.palette.IPaletteFactory {

	
	public PaletteRoot getPaletteRoot(final WEEditor editor) {
		return new Palette(editor);
	}

}
