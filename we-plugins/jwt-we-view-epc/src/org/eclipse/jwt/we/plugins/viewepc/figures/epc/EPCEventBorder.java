/**
 * File:    EventBorder.java
 * Created: 30.01.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewepc.figures.epc;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.swt.graphics.Color;


/**
 * A border with six corners: a hexagon.
 */
public class EPCEventBorder
		extends LineBorder
{

	/**
	 * The top left point of the hexagon.
	 */
	private Point topLeftPoint = new Point();

	/**
	 * The top right point of the hexagon.
	 */
	private Point topRightPoint = new Point();

	/**
	 * The bottom left point of the hexagon.
	 */
	private Point bottomLeftPoint = new Point();

	/**
	 * The bottom right point of the hexagon.
	 */
	private Point bottomRightPoint = new Point();

	/**
	 * The middle left point of the hexagon.
	 */
	private Point middleLeftPoint = new Point();

	/**
	 * The middle right point of the hexagon.
	 */
	private Point middleRightPoint = new Point();


	/**
	 * Creates a new border for EPC events: a hexagon.
	 */
	public EPCEventBorder()
	{
		super(PreferenceReader.appearanceBorderColor.get(),
				PreferenceReader.appearanceLineWidth.get());
	}


	/**
	 * @return Returns the topLeftPoint.
	 */
	public Point getTopLeftPoint()
	{
		return topLeftPoint;
	}


	/**
	 * @return Returns the topRightPoint.
	 */
	public Point getTopRightPoint()
	{
		return topRightPoint;
	}


	/**
	 * @return Returns the bottomLeftPoint.
	 */
	public Point getBottomLeftPoint()
	{
		return bottomLeftPoint;
	}


	/**
	 * @return Returns the bottomRightPoint.
	 */
	public Point getBottomRightPoint()
	{
		return bottomRightPoint;
	}

	/**
	 * @return Returns the middleLeftPoint.
	 */
	public Point getMiddleLeftPoint()
	{
		return middleLeftPoint;
	}

	/**
	 * @return Returns the middleRightPoint.
	 */
	public Point getMiddleRightPoint()
	{
		return middleRightPoint;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Border#paint(org.eclipse.draw2d.IFigure,
	 *      org.eclipse.draw2d.Graphics, org.eclipse.draw2d.geometry.Insets)
	 */
	@Override
	public void paint(IFigure figure, Graphics graphics, Insets insets)
	{
		tempRect.setBounds(getPaintRectangle(figure, insets));

		// set line width and color
		graphics.setLineWidth(getWidth());
		if (getColor() != null)
		{
			graphics.setForegroundColor(getColor());
		}

		boolean drawShadow = PreferenceReader.appearanceShadowVisible.get();

		// if a shadow is drawn, make the original figure smaller
		if (drawShadow)
		{
			tempRect.width -= 2;
			tempRect.height -= 2;
		}

		// calculate the hexagon corner points
		int dx = tempRect.width / 6;
		int dy = tempRect.height / 2;

		PointList pointList = new PointList();
//		topPoint = new Point(tempRect.x + dx, tempRect.y);
//		rightPoint = new Point(tempRect.x + tempRect.width, tempRect.y + dy);
//		bottomPoint = new Point(tempRect.x + dx, tempRect.y + tempRect.height);
//		leftPoint = new Point(tempRect.x, tempRect.y + dy);
		
		topLeftPoint = new Point(tempRect.x+dx, tempRect.y);
		topRightPoint = new Point(tempRect.x+tempRect.width-dx, tempRect.y);
		middleRightPoint = new Point(tempRect.x+tempRect.width-1, tempRect.y+dy);
		bottomRightPoint = new Point(tempRect.x+tempRect.width-dx, tempRect.y+tempRect.height-1);
		bottomLeftPoint = new Point(tempRect.x+dx, tempRect.y+tempRect.height-1);
		middleLeftPoint = new Point(tempRect.x, tempRect.y+dy);

		// draw shadow?
		if (PreferenceReader.appearanceShadowVisible.get())
		{
			// draw shadow hexagon
			pointList.removeAllPoints();
			pointList.addPoint(topLeftPoint.x + 2, topLeftPoint.y + 2);
			pointList.addPoint(topRightPoint.x + 2, topRightPoint.y + 2);
			pointList.addPoint(middleRightPoint.x + 2, middleRightPoint.y + 2);
			pointList.addPoint(bottomRightPoint.x + 2, bottomRightPoint.y + 2);			
			pointList.addPoint(bottomLeftPoint.x + 2, bottomLeftPoint.y + 2);
			pointList.addPoint(middleLeftPoint.x + 2, middleLeftPoint.y + 2);

			graphics.setAlpha(50);
			graphics.setBackgroundColor(PreferenceReader.appearanceShadowColor.get());
			graphics.fillPolygon(pointList);

			// draw original filled hexagon area
			pointList.removeAllPoints();
			pointList.addPoint(topLeftPoint.x, topLeftPoint.y);
			pointList.addPoint(topRightPoint.x, topRightPoint.y);
			pointList.addPoint(middleRightPoint.x, middleRightPoint.y);
			pointList.addPoint(bottomRightPoint.x, bottomRightPoint.y);
			pointList.addPoint(bottomLeftPoint.x, bottomLeftPoint.y);
			pointList.addPoint(middleLeftPoint.x, middleLeftPoint.y);

			graphics.setAlpha(255);
			graphics.setBackgroundColor(figure.getBackgroundColor());
			graphics.fillPolygon(pointList);
		}

		// draw border
		pointList.removeAllPoints();
		pointList.addPoint(topLeftPoint.x, topLeftPoint.y);
		pointList.addPoint(topRightPoint.x, topRightPoint.y);
		pointList.addPoint(middleRightPoint.x, middleRightPoint.y);
		pointList.addPoint(bottomRightPoint.x, bottomRightPoint.y);
		pointList.addPoint(bottomLeftPoint.x, bottomLeftPoint.y);
		pointList.addPoint(middleLeftPoint.x, middleLeftPoint.y);

		graphics.setAlpha(50);
		graphics.setBackgroundColor(new Color(null, 250, 40, 100));
		//graphics.setForegroundColor(new Color(null, 250, 40, 100));

		graphics.fillPolygon(pointList);
		graphics.setBackgroundColor(PreferenceReader.appearanceBorderColor.get());
		graphics.drawPolygon(pointList);
	}

}
