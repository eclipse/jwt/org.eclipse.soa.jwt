/**
 * File:    MergeNodeFigure.java
 * Created: 30.01.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany, http://www.ds-lab.org/
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewepc.figures.processes;

import java.util.MissingResourceException;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.meta.model.processes.impl.ProcessesFactoryImpl;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.figures.processes.DiamondFigure;
import org.eclipse.jwt.we.figures.processes.EllipseBorder;
import org.eclipse.jwt.we.plugins.viewepc.EpcViewPluginProperties;
import org.eclipse.jwt.we.plugins.viewepc.figures.ImageFactory;
import org.eclipse.swt.graphics.Image;


public class MergeNodeFigure
		extends DiamondFigure
{
	
	
	public MergeNodeFigure() {
		super();
		setBorder(new EllipseBorder());		
	}

	/**
	 * sets the EPC XOR Icon (a "xor")
	 */
	public void setIcon(Image icon) {		
		// use the large icon (without visible bounds) instead of the small one
		String path;
		try
		{
			path = EpcViewPluginProperties.model_icon(ProcessesFactoryImpl.eINSTANCE
					.createDecisionNode(), true);
		}
		catch (MissingResourceException e)
		{
			super.setIcon(icon);
			return;
		}

		ImageDescriptor imageDescriptor = ImageFactory.createImageDescriptor("icons/"+path); //$NON-NLS-1$

		// test if the image is available
		if (imageDescriptor == null || imageDescriptor.getImageData() == null)
		{
			super.setIcon(icon);
			return;
		}
		else
		{
			super.setIcon(imageDescriptor.createImage());
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.IconFigure#getIconClientArea()
	 */
	protected Rectangle getIconClientArea()
	{
		Rectangle iconClientArea = getClientArea();

		// if shadow is activated, resize client area for painting the icon
		if (PreferenceReader.appearanceShadowVisible.get())
		{
			iconClientArea.height -= 2;
			iconClientArea.width -= 2;
		}

		return iconClientArea;
	}
}
