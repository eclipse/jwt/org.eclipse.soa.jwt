/**
 * File:    DataBorder.java
 * Created: 12.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.plugins.viewepc.figures.data;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.swt.graphics.Color;

/**
 * A border as a rectangle but with curved lines at the bottom
 */
public class DataBorder extends LineBorder
{

		/**
		 * Creates a new RoundedRectangleBorder.
		 */
		public DataBorder()
		{
			super(PreferenceReader.appearanceBorderColor.get(),
					PreferenceReader.appearanceLineWidth.get());
		}


		/**
		 * Calculates the corner dimensions relative to a figure.
		 * 
		 * @param figure
		 *            The figure.
		 * @return The current corner dimensions.
		 */
		public Dimension getCornerDimensions(IFigure figure)
		{
			int size = PreferenceReader.appearanceCornerSize.get() * 2;
			return new Dimension(size, size);
		}


		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.draw2d.Border#paint(org.eclipse.draw2d.IFigure,
		 *      org.eclipse.draw2d.Graphics, org.eclipse.draw2d.geometry.Insets)
		 */
		@Override
		public void paint(IFigure figure, Graphics graphics, Insets insets)
		{
			tempRect.setBounds(getPaintRectangle(figure, insets));
			tempRect.width -= 1;
			tempRect.height -= 1;

			// set line width and color
			graphics.setLineWidth(getWidth());
			if (getColor() != null)
			{
				graphics.setForegroundColor(getColor());
			}

			boolean drawShadow = PreferenceReader.appearanceShadowVisible.get();

			// if a shadow is drawn, make the original figure smaller
			if (drawShadow)
			{
				tempRect.width -= 2;
				tempRect.height -= 2;
			}
	
			int dx = tempRect.width / 4;
			int dy = tempRect.height / 8;

			PointList pointList = new PointList();

			pointList.addPoint(tempRect.x, tempRect.y);
			pointList.addPoint(tempRect.x+tempRect.width, tempRect.y);
			pointList.addPoint(tempRect.x+tempRect.width, tempRect.y+tempRect.height-dy);

			pointList.addPoint(tempRect.x+tempRect.width-dx, tempRect.y+tempRect.height);
			pointList.addPoint(tempRect.x+(tempRect.width / 2), tempRect.y+tempRect.height-dy);
			pointList.addPoint(tempRect.x+dx, tempRect.y+tempRect.height-(2*dy));

			pointList.addPoint(tempRect.x, tempRect.y+tempRect.height-dy);
			graphics.setAlpha(50);
			graphics.setBackgroundColor(new Color(null, 255, 255, 0));
			//graphics.setForegroundColor(new Color(null, 255, 255, 0));

			graphics.fillPolygon(pointList);
			graphics.setBackgroundColor(PreferenceReader.appearanceBorderColor.get());

			graphics.drawPolygon(pointList);

		}
	}

