/**
 * File:    ActionFigure.java
 * Created: 19.01.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg
 *      - adapted to EPC view
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewepc.figures.processes;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jwt.we.figures.core.NamedElementFigure;


/**
 * Figure for an {@link Action}.
 * 
 * @version $Id: ActionFigure.java,v 1.3 2009-11-26 12:38:04 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class ActionFigure
		extends NamedElementFigure
{

	/**
	 * Creates the figure.
	 */
	public ActionFigure()
	{
		setBorder(new RoundedRectangleBorder());
	}


	/**
	 * This function makes sure that the label is shown completely.
	 * 
	 * @see org.eclipse.jwt.we.figures.core.ModelElementFigure#getPreferredSize(int, int)
	 */
	@Override
	public Dimension getPreferredSize(int wHint, int hHint)
	{
		return getPreferredChildrenSize(wHint, hHint);
	}
}