/**
 * File:    DataFigure.java
 * Created: 30.01.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany, http://www.ds-lab.org/
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewepc.figures.data;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jwt.we.figures.core.NamedElementFigure;

public class DataFigure extends NamedElementFigure
{

	/**
	 * Creates the figure.
	 */
	public DataFigure()
	{
		setBorder(new DataBorder());
		
	}
	
	/**
	 * This function makes sure that the label is shown completely.
	 * 
	 * @see org.eclipse.jwt.we.figures.core.ModelElementFigure#getPreferredSize(int, int)
	 */
	@Override
	public Dimension getPreferredSize(int wHint, int hHint)
	{
		return getPreferredChildrenSize(wHint, hHint);
	}
}
