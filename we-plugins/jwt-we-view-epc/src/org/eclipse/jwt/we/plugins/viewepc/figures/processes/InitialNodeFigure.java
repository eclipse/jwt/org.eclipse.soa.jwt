/**
 * File:    InitialNodeFigure.java
 * Created: 30.01.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany, http://www.ds-lab.org/
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewepc.figures.processes;

import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.plugins.viewepc.figures.epc.EPCEventFigure;

public class InitialNodeFigure
			extends EPCEventFigure
{

	/**
	 * Creates the figure
	 */
	public InitialNodeFigure()
	{
		this.setBackgroundColor(PreferenceReader.appearanceBorderColor.get());
	}
}
