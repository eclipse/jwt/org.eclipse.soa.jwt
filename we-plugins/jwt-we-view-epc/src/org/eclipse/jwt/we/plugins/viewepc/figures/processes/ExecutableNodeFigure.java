/**
 * File:    ExecutableNodeFigure.java
 * Created: 30.01.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany, http://www.ds-lab.org/
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.plugins.viewepc.figures.processes;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.jwt.we.figures.core.NamedElementFigure;
import org.eclipse.jwt.we.plugins.viewepc.figures.epc.EPCEventBorder;

public class ExecutableNodeFigure extends NamedElementFigure
{

	/**
	 * Creates the figure.
	 */
	public ExecutableNodeFigure()
	{
		super();
		setBorder(new EPCEventBorder());
	}


	/**
	 * @return Returns the bottomLeftPoint.
	 */
	public Point getBottomLeftPoint()
	{
		return ((EPCEventBorder) getBorder()).getBottomLeftPoint();
	}


	/**
	 * @return Returns the bottomRightPoint.
	 */
	public Point getBottomRightPoint()
	{
		return ((EPCEventBorder) getBorder()).getBottomRightPoint();
	}


	/**
	 * @return Returns the middleLeftPoint.
	 */
	public Point getMiddleLeftPoint()
	{
		return ((EPCEventBorder) getBorder()).getMiddleLeftPoint();
	}

	/**
	 * @return Returns the middleRightPoint.
	 */
	public Point getMiddleRightPoint()
	{
		return ((EPCEventBorder) getBorder()).getMiddleRightPoint();
	}

	/**
	 * @return Returns the topLeftPoint.
	 */
	public Point getTopLeftPoint()
	{
		return ((EPCEventBorder) getBorder()).getTopLeftPoint();
	}

	/**
	 * @return Returns the topRightPoint.
	 */
	public Point getTopRightPoint()
	{
		return ((EPCEventBorder) getBorder()).getTopRightPoint();
	}

}
