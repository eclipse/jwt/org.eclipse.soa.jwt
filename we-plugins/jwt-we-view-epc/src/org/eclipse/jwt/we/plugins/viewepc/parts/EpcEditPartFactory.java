/**
 * File:    EPCEditPartFactory.java
 * Created: 10.03.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.plugins.viewepc.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.parts.EditPartAdapter;
import org.eclipse.jwt.we.parts.EditPartAdapterFactory;
import org.eclipse.jwt.we.parts.JWTEditPartFactory;
import org.eclipse.jwt.we.plugins.viewepc.Activator;
import org.eclipse.jwt.we.plugins.viewepc.model.epc.EPCEvent;

/**
 * Factory that maps model elements to edit parts within Bonita plug-in.
 * 
 * The EditPartFactory uses an {@link EditPartAdapterFactory} to create the
 * EditParts. That means that the model elements are actually adapted to
 * {@link EditPartAdapter}s.
 * 
 * @author Blachon Marc
 */
public class EpcEditPartFactory implements EditPartFactory
{

	/**
	 * The Logger.
	 */
	private static final Logger logger = Logger.getLogger(EpcEditPartFactory.class);

	/**
	 * Factory for EditParts.
	 */
	private EditPartAdapterFactory adapterFactory = new EditPartAdapterFactory(
			"org.eclipse.jwt.we.plugins.viewepc.parts",
			"org.eclipse.jwt.we.plugins.viewepc.model", Activator.getDefault());


	/**
	 * Constructor.
	 */
	public EpcEditPartFactory()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(final EditPart context, final Object model)
	{
		// replace eventeditpart with epceventeditpart
		if (model == null)
			return null;
		else if (model instanceof EPCEvent)
		{
			EditPartAdapter adapter = (EditPartAdapter) adapterFactory.adapt(model,
					EditPartAdapter.class);

			if (adapter == null)
				return null;

			return adapter.getEditPart();
		}

		return null;
	}

}
