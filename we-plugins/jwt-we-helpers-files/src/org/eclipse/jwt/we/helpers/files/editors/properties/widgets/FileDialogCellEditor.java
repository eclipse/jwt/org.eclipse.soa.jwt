/**
 * File:    FileDialogCellEditor.java
 * Created: 11.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008   Open Wide SA
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *	 
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg, Germany
 *      - Creation and implementation
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Move into a plugin
 *******************************************************************************/


package org.eclipse.jwt.we.helpers.files.editors.properties.widgets;



import java.io.File;

import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.ui.celleditor.ExtendedDialogCellEditor;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;



/**
 * A file dialog cell editor. It opens a file dialog with the given file extensions preset
 * and checks if the selected file lies within the plugin path. If so, it returns the path
 * relatively to the plugin root, if not it returns the absolute path.
 * 
 * @version $Id: FileDialogCellEditor.java,v 1.1 2010-05-10 08:36:56 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class FileDialogCellEditor
		extends ExtendedDialogCellEditor
{

	/**
	 * The standard file extensions of the file dialog.
	 */
	private String extensions;


	/**
	 * The constructor. Calls the super constructor and sets the file extensions
	 * 
	 * @param composite
	 *            The composite.
	 * @param labelProvider
	 *            The labelProvider.
	 * @param extensions
	 *            The file extensions.
	 */
	public FileDialogCellEditor(Composite composite, ILabelProvider labelProvider,
			String extensions)
	{
		super(composite, labelProvider);
		this.extensions = extensions;
	}


	/**
	 * Opens the file-dialog box and returns the relative or absolute path of the selected
	 * file (relative if it lies in the plugin)
	 * 
	 * @param cellEditorWindow
	 *            The cellEditorWindow.
	 * @return The path.
	 */
	@Override
	protected Object openDialogBox(Control cellEditorWindow)
	{
		String oldLabelText = getDefaultLabel().getText();

		// create file dialog
		FileDialog fd = new FileDialog(cellEditorWindow.getShell(), SWT.OPEN);

		// set extensions
		fd.setFilterExtensions(new String[]
		{ this.extensions });

		// if old path was absolute, set it as default path
		if (new Path(oldLabelText).isAbsolute())
		{
			fd.setFilterPath(oldLabelText);
		}
		
		// if old path was empty, use default icon path (<base>\icons)
		if (oldLabelText.equals("")) //$NON-NLS-1$
		{
			File iconPath = new File("icons"); //$NON-NLS-1$
			if (iconPath.exists())
			{
				fd.setFilterPath(iconPath.getAbsolutePath());
			}
		}
		
		// get the absolute icon-path
		String dialogpath = fd.open();
		if (dialogpath == null || !new File(dialogpath).exists()) {
			return oldLabelText;
		} else {
			return new File(dialogpath).getAbsolutePath();
		}
	}

}
