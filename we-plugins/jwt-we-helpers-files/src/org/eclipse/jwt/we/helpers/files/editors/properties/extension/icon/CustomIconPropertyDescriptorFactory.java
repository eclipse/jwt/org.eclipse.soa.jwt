/**
 * File:    CustomIconPropertyDescriptorFactory.java
 * Created: 22.09.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008   Open Wide SA
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *    Mickael Istria, Open Wide, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.we.helpers.files.editors.properties.extension.icon;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.jwt.we.editors.properties.extension.PropertyDescriptorFactory;
import org.eclipse.jwt.we.helpers.files.editors.properties.widgets.BrowserPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

/**
 * Custom property editor for Icon
 * @author mistria
 *
 */
public class CustomIconPropertyDescriptorFactory implements
		PropertyDescriptorFactory {

	public IPropertyDescriptor getPropertyDescriptor(Object object,	IItemPropertyDescriptor itemPropertyDescriptor) {
		return new BrowserPropertyDescriptor(object, itemPropertyDescriptor, "*.ico;*.gif;*.png"); //$NON-NLS-1$
	}

}
