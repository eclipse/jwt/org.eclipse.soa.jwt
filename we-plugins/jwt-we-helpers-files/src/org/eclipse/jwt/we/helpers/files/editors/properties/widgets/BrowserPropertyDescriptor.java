/**
 * File:    BrowserPropertyDescriptor.java
 * Created: 11.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 *    Programming distributed Systems Lab, University of Augsburg
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *	 
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg, Germany
 *      - Creation and implementation
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Move into a plugin
 *******************************************************************************/

package org.eclipse.jwt.we.helpers.files.editors.properties.widgets;



import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;


/**
 * A custom property descriptor which overrides createPropertyEditor in order to make
 * custom cell editors possible which differ from the automated cell editor choice
 * 
 * @version $Id: BrowserPropertyDescriptor.java,v 1.1 2010-05-10 08:36:56 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class BrowserPropertyDescriptor
		extends PropertyDescriptor
{
	
	private String extensions;

	/**
	 * The constructor.
	 * 
	 * @param object
	 *            The object.
	 * @param itemPropertyDescriptor
	 *            The itemPropertyDescriptor.
	 */
	public BrowserPropertyDescriptor(Object object,	IItemPropertyDescriptor itemPropertyDescriptor, String extensions)
	{
		super(object, itemPropertyDescriptor);
		this.extensions = extensions;
	}


	/**
	 * If an icon property is selected a filedialog cell-editor is opened, if not, the
	 * decision about the right cell-editor is up to EMF
	 * 
	 * @param composite
	 *            The composite.
	 * @return the cell editor
	 */
	@Override
	public CellEditor createPropertyEditor(Composite composite)
	{
		return new FileDialogCellEditor(composite, getEditLabelProvider(),
				extensions);
	}

}
