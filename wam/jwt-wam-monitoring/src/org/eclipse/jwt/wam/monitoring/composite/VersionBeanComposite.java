/**
 * File:    VersionBeanComposites.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.composite;

public class VersionBeanComposite extends WorkflowBeanComposite {

	private String version;
	
	public VersionBeanComposite(String version, WorkflowBeanComposite parent) {
		this.version = version;
		this.setParent(parent);
	}	
	
	/**
	 * @deprecated When using a sorted tree
	 */
	public VersionBeanComposite(String version) {
		this.version = version;
	}
	
	public Object[] getChildren() {
		return null;
	}

	public String getName() {
		return "version";
	}

	public String getValue() {
		return version;
	}
	
	public int compareTo(WorkflowBeanComposite o) {
		// Only one version by element
		return 0;
	}
}
