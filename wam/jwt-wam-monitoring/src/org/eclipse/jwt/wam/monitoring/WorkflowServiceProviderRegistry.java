/**
 * File:    WorkflowServiceProviderRegistry.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jwt.wam.monitoring.serviceAPI.WorkflowServiceProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.WorkbenchException;

/**
 * A "library" of WorkflowService providers
 * @author mistria
 *
 */
public class WorkflowServiceProviderRegistry {

	private static WorkflowServiceProviderRegistry instance = null;
	private Map<String, WorkflowServiceProvider> workflowServiceRetrievers;
	
	private WorkflowServiceProviderRegistry() {
		workflowServiceRetrievers = new HashMap<String, WorkflowServiceProvider>();
	}
	
	public void addWorkflowServiceProvider(String functionName,
			WorkflowServiceProvider proxy) {
		workflowServiceRetrievers.put(functionName, proxy);	
	}
	
	public Set<String> getWorkflowServiceProvidersNames() {
		return workflowServiceRetrievers.keySet();
	}

	public static WorkflowServiceProviderRegistry getInstance() {
		if (instance == null) {
			instance = new WorkflowServiceProviderRegistry();
			try {
				ProcessExtensions.process(
						"org.eclipse.jwt.wam.serviceProvider",
						WorkflowServiceProviderRegistry.getInstance());	
			} catch (WorkbenchException ex) {
				Display display = Display.getCurrent();
				Shell shell = new Shell(display);
				MessageDialog.openError(shell, "Error", "An error occured when truing to get extensions");
			}
		}
		return instance;
	}

	public WorkflowServiceProvider getWorkflowServiceProvider(String string) {
		return workflowServiceRetrievers.get(string);
	}

}
