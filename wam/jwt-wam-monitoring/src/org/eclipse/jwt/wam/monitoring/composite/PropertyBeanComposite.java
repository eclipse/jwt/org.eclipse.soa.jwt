/**
 * File:    PropertyBeanComposite.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.composite;

import org.eclipse.jwt.wam.api.model.Property;


public class PropertyBeanComposite extends WorkflowBeanComposite {

	private Property property;
	
	public PropertyBeanComposite(Property property, WorkflowBeanComposite parent) {
		this.property = property;
		this.setParent(parent);
	}
	
	public Object[] getChildren() {
		return null;
	}

	public String getName() {
		return property.getKey();
	}

	public String getValue() {
		return property.getValue().toString();
	}

	public int compareTo(WorkflowBeanComposite o) {
		if (!(o instanceof PropertyBeanComposite))
			return -1;
		else
			return getName().compareTo(((PropertyBeanComposite)o).getName());
	}
}
