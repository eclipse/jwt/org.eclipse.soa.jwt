/**
 * File:    ProjectBeanComposite.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.composite;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jwt.wam.api.model.Project;
import org.eclipse.jwt.wam.api.model.Property;

public class ProjectBeanComposite extends WorkflowBeanComposite {

	private Project project;
	
	public ProjectBeanComposite(Project _project, WorkflowBeanComposite parent) {
		project = _project;
		this.setParent(parent);
	}
	
	public Object[] getChildren() {
		/*if (project.getName() == null)
			project = workflowService.getProjectProperties(this.project, user);*/
		List<WorkflowBeanComposite> children = new ArrayList<WorkflowBeanComposite>();
		children.add(new NameBeanComposite(project.getName(), this));
		children.add(new VersionBeanComposite(project.getVersion(), this));
		for (Property property : project.getProperties())
			children.add(new PropertyBeanComposite(property, this));
		return children.toArray();
	}

	public String getName() {
		return "Project";
	}

	public String getValue() {
		return null;
	}

	public int compareTo(WorkflowBeanComposite o) {
		return 0;
	}
}
