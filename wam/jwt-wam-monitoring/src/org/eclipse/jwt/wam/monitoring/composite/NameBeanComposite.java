/**
 * File:    NameBeanComposite.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.composite;

public class NameBeanComposite extends WorkflowBeanComposite {

	private String name;
	
	/*public NameBeanComposite(String name) {
		this.name = name;
		this.category  = category;
	}*/
	
	public NameBeanComposite(String name, WorkflowBeanComposite parent) {
		this.name = name;
		this.setParent(parent);
	}
	
	public Object[] getChildren() {
		return null;
	}

	public String getName() {
		return "name";
	}

	public String getValue() {
		return name;
	}
	
	public String toString() {
		return "name = " + name;
	}

	public int compareTo(WorkflowBeanComposite o) {
		// Only one name per element
		return 0;
	}
}
