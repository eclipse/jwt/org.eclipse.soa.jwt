/**
 * File:    ActivityBeanComposite.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.composite;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jwt.wam.api.model.Activity;
import org.eclipse.jwt.wam.api.model.Property;

public class ActivityBeanComposite extends WorkflowBeanComposite {

	private Activity activity;
	
	public ActivityBeanComposite(Activity activity, WorkflowBeanComposite parent) {
		this.activity = activity;
		this.setParent(parent);
	}
	
	public Object[] getChildren() {
		List<WorkflowBeanComposite> children = new ArrayList<WorkflowBeanComposite>();
		children.add(new NameBeanComposite(activity.getName(), this));
		children.add(new StateBeanComposite(activity.getState(), this));
		for (Property property : activity.getProperties())
			children.add(new PropertyBeanComposite(property, this));
		return children.toArray();
	}

	public String getName() {
		return "Activity";
	}

	public String getValue() {
		return null;
	}

	public int compareTo(WorkflowBeanComposite o) {
		return 0;
	}
}
