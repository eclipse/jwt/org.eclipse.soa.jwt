/**
 * File:    StateBeanComposite.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.composite;

public class StateBeanComposite extends WorkflowBeanComposite {

	private String state;
	
	public StateBeanComposite(String state, WorkflowBeanComposite parent) {
		this.state = state;
		this.setParent(parent);
	}
	
	public Object[] getChildren() {
		return null;
	}

	public String getName() {
		return "state";
	}

	public String getValue() {
		return state;
	}

	public int compareTo(WorkflowBeanComposite o) {
		// only one state per element
		return 0;
	}
}
