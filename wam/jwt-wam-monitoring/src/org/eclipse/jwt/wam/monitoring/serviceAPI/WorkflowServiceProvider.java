/**
 * File:    WorkflowServiceProvider.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.serviceAPI;

import org.eclipse.jwt.wam.api.WorkflowService;

public abstract class WorkflowServiceProvider {
	
	private String description;

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}

	/**
	 * Method called when user client wants to add a WorkflowService
	 * with this provider
	 * @param viewer
	 * @TODO check whether a user parameter could be useful
	 */
	//public abstract void processServiceAdd(TreeViewer viewer);

	public abstract WorkflowService getWorkflowService();
}
