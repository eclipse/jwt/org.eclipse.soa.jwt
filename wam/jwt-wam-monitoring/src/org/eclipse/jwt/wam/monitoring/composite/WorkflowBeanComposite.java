/**
 * File:    WorkflowBeanComposite.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.composite;

import java.util.HashMap;
import java.util.Map;


public abstract class WorkflowBeanComposite implements Comparable<WorkflowBeanComposite> {
	private WorkflowBeanComposite parent;

	
	private static Map<Class<?>, Class<? extends WorkflowBeanComposite>> elementToComposite = new HashMap<Class<?>, Class<? extends WorkflowBeanComposite>>();	
	
	/**
	 * 
	 * @return name of the bean (that will be used for the left column of the tree
	 */
	public abstract String getName();
	
	/**
	 * 
	 * @return the value of the element (used by right column of the tree
	 */
	public abstract String getValue();
	
	/**
	 * 
	 * @return An array of {@link WorkflowBeanComposite} that contains the chlidren of current composite
	 */
	public abstract Object[] getChildren();
	
	//public abstract String toString();
	
	/**
	 * Used by sorter
	 * @return the category (depth) of the element
	 */
	public int getCategory() {
		if (parent == null)
			return 0;
		else
			return parent.getCategory() + 1;
	}
	
	public void setParent(WorkflowBeanComposite parent) {
		this.parent = parent;
	}
	
	public static WorkflowBeanComposite createComposite(Object element, WorkflowBeanComposite parent) {
		elementToComposite.put(String.class, NameBeanComposite.class);
		try {
			WorkflowBeanComposite res = elementToComposite.get(element.getClass())
				.getConstructor(String.class, element.getClass(), WorkflowServiceComposite.class, String.class).newInstance(element);
			res.setParent(parent);
			return res;
		} catch (Exception e) {
			System.err.println("Unable to create composite for class " + element.getClass());
			return null;
		}
	}
}
