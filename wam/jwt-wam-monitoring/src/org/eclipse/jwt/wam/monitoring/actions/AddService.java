/**
 * File:    AddService.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.actions;

import java.util.Set;

import org.eclipse.jwt.wam.monitoring.WorkflowServiceProviderRegistry;
import org.eclipse.jwt.wam.monitoring.serviceAPI.WorkflowServiceProvider;
import org.eclipse.jwt.wam.monitoring.views.WorkflowServicesView;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Class that contains a GUI to add a WorkflowService
 * @author mistria
 *
 */
public class AddService {
	
	private static AddService instance = null;
	
	private AddService() {
	}
	
	public static AddService getInstance() {
		if (instance == null)
			instance = new AddService();
		return instance;
	}

	private void centerOnScreen(Display display, Shell shell) {
		Rectangle rect = display.getBounds();
		Point size = shell.getSize();
		int x = (rect.width - size.x) / 2;
		int y = (rect.height - size.y) / 2;
		shell.setLocation(new Point(x, y));
	}
	
	public void addService(final WorkflowServicesView view) {
		final Display display = Display.getCurrent();
		final Shell shell = new Shell(display);
		
		shell.setText("Select the Workflow Service");
		
		Button ok = null;
		Button abort = null;
		Label providerLabel = null;
		Label loginLabel = null;
		
		GridData gridData13 = new GridData();
		gridData13.horizontalAlignment = GridData.END;
		gridData13.grabExcessHorizontalSpace = true;
		gridData13.verticalAlignment = GridData.CENTER;
		GridData gridData12 = new GridData();
		gridData12.verticalAlignment = GridData.CENTER;
		gridData12.horizontalAlignment = GridData.END;
		GridData gridData1 = new GridData();
		gridData1.horizontalSpan = 7;
		gridData1.verticalAlignment = GridData.CENTER;
		gridData1.grabExcessHorizontalSpace = true;
		gridData1.horizontalAlignment = GridData.FILL;
		GridData gridData = new GridData();
		gridData.horizontalSpan = 7;
		gridData.verticalAlignment = GridData.CENTER;
		gridData.horizontalAlignment = GridData.FILL;
		GridData descGridData = new GridData();
		descGridData.horizontalSpan = 8;
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 8;
		providerLabel = new Label(shell, SWT.NONE);
		providerLabel.setText("Workflow Service Provider");
		final Combo provider = new Combo(shell, SWT.READ_ONLY);
		populateWithProviders(provider);
		provider.setLayoutData(gridData1);
		final Label description = new Label(shell, SWT.NONE);
		description.setText("");
		description.setLayoutData(descGridData);
		loginLabel = new Label(shell, SWT.NONE);
		loginLabel.setText("Login");
		final Text user = new Text(shell, SWT.BORDER);
		user.setLayoutData(gridData);
		new Label(shell, SWT.NONE);
		ok = new Button(shell, SWT.NONE);
		ok.setText("Ok");
		ok.setLayoutData(gridData13);
		abort = new Button(shell, SWT.NONE);
		abort.setText("Cancel");
		abort.setLayoutData(gridData12);
		shell.setLayout(gridLayout);
		shell.setSize(new Point(400, 105));
		
		abort.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				shell.dispose();
			}
		});
		provider.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				description.setText(WorkflowServiceProviderRegistry.getInstance().getWorkflowServiceProvider(provider.getText()).getDescription());
				description.update();
				shell.layout();
			}
			
		});
		ok.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				WorkflowServiceProvider wfProvider = WorkflowServiceProviderRegistry.getInstance().getWorkflowServiceProvider(provider.getText());
				// TODO UI?
				view.addService(wfProvider);
				shell.dispose();
			}
		});
		
		shell.pack();
		centerOnScreen(display, shell);
		shell.open();
	}

	private void populateWithProviders(Combo provider) {
		Set<String> names = WorkflowServiceProviderRegistry.getInstance().getWorkflowServiceProvidersNames();
		for (String name : names)
			provider.add(name);
	}
}
