/**
 * File:    TransformationRegistry.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.composite;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jwt.wam.api.WorkflowService;
import org.eclipse.jwt.wam.api.model.Process;

public class ProcessBeanComposite extends WorkflowBeanComposite {

	private Process process;
	
	public ProcessBeanComposite(Process _process, WorkflowService workflowService, WorkflowBeanComposite parent) {
		process = _process;
		this.setParent(parent);
	}

	public String getName() {
		return "Process";
	}

	public String getValue() {
		return process.getInstanceName();
	}

	public Object[] getChildren() {
		List<WorkflowBeanComposite> children = new ArrayList<WorkflowBeanComposite>();
		children.add(new NameBeanComposite(process.getInstanceName(), this));
		children.add(new ProjectBeanComposite(process.getProject(), this));
		children.add(new ActivityBeanComposite(process.getActivity(),this));
		return children.toArray();
	}
	
	public int compareTo(WorkflowBeanComposite o) {
		if (!(o instanceof ProcessBeanComposite))
			return 0;
		else {
			String s1 = this.process.getInstanceName();
			String s2 = ((ProcessBeanComposite)o).process.getInstanceName();
			
			int i;
			for (i = 0; i < s1.length() && i < s2.length(); i++)
				/* digit and letter => digit < letter */
				if (Character.isDigit(s1.charAt(i)) && !Character.isDigit(s2.charAt(i)))
					return -1;
				/* letter and digit => letter > digit */
				else if (!Character.isDigit(s1.charAt(i)) && Character.isDigit(s2.charAt(i)))
					return 1;
				/* 2 letters => alphabetic */
				else if (!Character.isDigit(s1.charAt(i)) && !Character.isDigit(s2.charAt(i)))
					if (s1.charAt(i) != s1.charAt(i)) return s1.charAt(i) - s2.charAt(i);
					else continue;
				/* 2 digits => compare numbers, not characters */
				else {
					int n1 = Integer.parseInt(s1.substring(i, s1.length()));
					int n2 = Integer.parseInt(s2.substring(i, s2.length()));
					return n1 - n2;
				}
			return (s1.length() < s2.length() ? -1 : 1);
		}
	}
}
