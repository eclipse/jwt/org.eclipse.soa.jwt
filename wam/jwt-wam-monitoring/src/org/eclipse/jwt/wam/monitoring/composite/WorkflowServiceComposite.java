/**
 * File:    WorkflowServiceComposite.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.composite;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jwt.wam.api.WorkflowService;
import org.eclipse.jwt.wam.api.model.Process;
import org.eclipse.jwt.wam.api.model.Processes;

public class WorkflowServiceComposite extends WorkflowBeanComposite {

	private List<ProcessBeanComposite> processes;
	private String name;

	
	public WorkflowServiceComposite(String name, WorkflowService workflowService, String user) {
		this.processes = new ArrayList<ProcessBeanComposite>();
		this.name = name;
		// TODO replace this ugly listUserActivities by something better
		Processes listUserActivities = workflowService.listUserActivities(true, true, user);
		if (listUserActivities == null)
			return;
		for (Process process : listUserActivities.getProcesses())
			this.processes.add(new ProcessBeanComposite(process, workflowService, this));
	}
	
	public Object[] getChildren() {
		//System.err.println(processes.getProcesses()[0]);
		return processes.toArray();
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return null;
	}

	public int compareTo(WorkflowBeanComposite arg0) {
		return getName().compareTo(arg0.getName());
	}

	public int getCategory() {
		return 1; // Son of the root
	}
}
