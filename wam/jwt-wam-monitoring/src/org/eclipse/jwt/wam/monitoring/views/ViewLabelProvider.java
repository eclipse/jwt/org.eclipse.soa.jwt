package org.eclipse.jwt.wam.monitoring.views;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jwt.wam.monitoring.composite.WorkflowBeanComposite;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
/**
 * File:    ViewLabelProvider.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
import org.eclipse.ui.PlatformUI;

/**
 * Class in charge of returning label and icons for the elements in the TreeViewer
 * @author mistria
 *
 */
class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object obj, int index) {
		if (index == 0)
			return ((WorkflowBeanComposite)obj).getName();
		else
			return ((WorkflowBeanComposite)obj).getValue();
	}
	
	public String getText(Object obj) {
		return ((WorkflowBeanComposite)obj).getName();
	}
	
	public Image getColumnImage(Object obj, int index) {
		if (index == 0)
			return getImage(obj);
		else
			return null;
	}
	
	public Image getImage(Object obj) {
		return PlatformUI.getWorkbench().
				getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
	}
}