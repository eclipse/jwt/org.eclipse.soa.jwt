/**
 * File:    ViewContentProvider.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.views;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jwt.wam.monitoring.composite.WorkflowBeanComposite;

/**
 * Class used by TreeViewer that is in charge of populating the tree
 * @author mistria
 *
 */
class ViewContentProvider implements ITreeContentProvider {
	static String hello = "hello";

	WorkflowBeanComposite projectComposite;

	public ViewContentProvider() {

	}

	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
	}

	public void dispose() {
	}

	public Object[] getElements(Object parent) {
		return getChildren(parent);
	}

	public Object[] getChildren(Object parentElement) {
		return ((WorkflowBeanComposite) parentElement).getChildren();
	}

	public Object getParent(Object element) {
		return null;
	}

	public boolean hasChildren(Object element) {
		return (element != null
				&& ((WorkflowBeanComposite) element).getChildren() != null
				&& ((WorkflowBeanComposite) element).getChildren().length != 0);
	}
}