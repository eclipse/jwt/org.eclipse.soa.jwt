/**
 * File:    RootComposite.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.monitoring.composite;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jwt.wam.api.WorkflowService;

public class RootComposite extends WorkflowBeanComposite {

	List<WorkflowServiceComposite> services;
	
	public RootComposite() {
		services = new ArrayList<WorkflowServiceComposite>();
	}
	
	public void addService(String name, WorkflowService workflowService, String user) {
		services.add(new WorkflowServiceComposite(name, workflowService, user));
	}
	
	public Object[] getChildren() {
		return services.toArray();
	}

	public String getName() {
		return null;
	}

	public String getValue() {
		return null;
	}

	public int compareTo(WorkflowBeanComposite o) {
		// Only one root
		return 0;
	}

	public int getCategory() {
		return 0; //ROOT
	}

	public void remove(WorkflowServiceComposite wfService) {
		services.remove(wfService);
	}

}
