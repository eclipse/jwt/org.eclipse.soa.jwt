/**
 * File:    WorkflowServerException.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Guillaume Décarnin, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.api;

/**
 * @author gdecarnin
 *
 * NB. Must be a RuntimeException, else tuscany for axis2 / axiom
 * can't translate it (databinding TransformationException). This
 * way it produces an UndeclaredThrowableException on client side.
 */
public class WorkflowServerException extends RuntimeException {
    private static final long serialVersionUID = -813838837001597219L;

    public WorkflowServerException() {
        super();
    }

    public WorkflowServerException(String message) {
        super(message);
    }

    public WorkflowServerException(Throwable cause) {
        this(cause.getMessage(), cause);
        // TODO better look in causes for a message not empty if any
        setStackTrace(cause.getStackTrace());
    }

    public WorkflowServerException(String message, Throwable cause) {
        super(message);
        setStackTrace(cause.getStackTrace());
    }
}
