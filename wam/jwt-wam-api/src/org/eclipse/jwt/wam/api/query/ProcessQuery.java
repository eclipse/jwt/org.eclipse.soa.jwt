/**
 * File:    WorkflowService.java
 * Created: 30.08.2010
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012    Open Wide (www.openwide.fr) 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/

package org.eclipse.jwt.wam.api.query;

import org.eclipse.jwt.wam.api.model.Process;


/**
 * A simple parameterized (with filters, pagination) query model
 * for workflow processes & tasks
 * 
 * @author mdutoo
 *
 */
public class ProcessQuery {

	/** if false, Process & task properties are not inlined in the return message */
	private boolean returnProperties;
	
	/** pagination start */
	private int start;
	/** pagination end */
	private int end;
	
	/** Returned Processes will have the same task name, state and properties
	 * if any are provided */
	private Process[] filterProcesses;

	public boolean isReturnProperties() {
		return returnProperties;
	}

	public void setReturnProperties(boolean returnProperties) {
		this.returnProperties = returnProperties;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public Process[] getFilterProcesses() {
		return filterProcesses;
	}

	public void setFilterProcesses(Process[] filterProcesses) {
		this.filterProcesses = filterProcesses;
	}
	
}
