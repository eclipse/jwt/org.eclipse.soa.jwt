/**
 * File:    Activity.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Guillaume Décarnin, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.api.model;

import java.io.Serializable;

public class Activity implements Serializable {
    private static final long serialVersionUID = 4906106836029609262L;
    
    private String name;
    private String state;
    private Property[] properties;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Property[] getProperties() {
        if (properties == null) {
            properties = new Property[0];
        }
        return properties;
    }

    public Property getProperty(String key) {
        for (Property property : getProperties()) {
            if (property != null && property.getKey().equals(key)) {
                return property;
            }
        }
        return null;
    }

    public Object getPropertyValue(String key) {
        Property property = getProperty(key);
        if (property != null) {
            return property.getValue();
        }
        return null;
    }

    public void setProperties(Property... properties) {
        this.properties = properties;
    }
    
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(name);
        
        result.append("[");
        result.append(state);
        
        if (properties != null) {
            result.append(", ");
            
            result.append("[");
            for (int i = 0; i < properties.length; i++) {
                result.append(properties[i].toString());
                if (i != properties.length - 1) {
                    result.append(", ");
                }
            }
            result.append("]");
        }

        result.append("]");
        
        return result.toString();
    }
}
