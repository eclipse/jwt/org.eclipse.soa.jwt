/**
 * File:    Process.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Guillaume Décarnin, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.api.model;

import java.io.Serializable;

public class Process implements Serializable {
    private static final long serialVersionUID = 4568804714175834205L;
    
    private String instanceName;
    private Project project;
    private Activity activity;
    private String name;
    private String state;
    
    public String getInstanceName() {
        return instanceName;
    }
    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
    
    public String getProcessName() {
        return name;
    }
    public void setProcessName(String name) {
        this.name = name;
    }
    
    public String getProcessState() {
        return state;
    }
    public void setProcessState(String state) {
        this.state = state;
    }
    
    public Project getProject() {
        if (project == null) {
            project = new Project();
        }
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Activity getActivity() {
        if (activity == null) {
            activity = new Activity();
        }
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return instanceName + "[" + project + ", " + activity + "]";
    }
}
