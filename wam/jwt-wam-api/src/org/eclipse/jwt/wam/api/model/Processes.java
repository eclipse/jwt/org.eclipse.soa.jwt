/**
 * File:    Processes.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Guillaume Décarnin, Open Wide, Lyon, France
 *      - Creation and implementation
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - Added pagination
 *******************************************************************************/
package org.eclipse.jwt.wam.api.model;

import java.io.Serializable;

public class Processes implements Serializable {
    private static final long serialVersionUID = -6520237282558092928L;
	
	private Process[] processes;
	
	private int start;
	private int end;
	private int size;


	public Process[] getProcesses() {
		return processes;
	}

	public void setProcesses(Process[] processes) {
		this.processes = processes;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}
