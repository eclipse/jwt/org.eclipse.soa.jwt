/**
 * File:    Projects.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Guillaume Décarnin, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.api.model;

import java.io.Serializable;

public class Projects implements Serializable {
    private static final long serialVersionUID = -6520237282558092927L;
	
	private Project[] projects;

	public Project[] getProjects() {
		return projects;
	}

	public void setProjects(Project[] projects) {
		this.projects = projects;
	}

}
