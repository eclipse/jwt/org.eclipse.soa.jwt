/**
 * File:    WorkflowService.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012    Open Wide (www.openwide.fr) 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Guillaume Décarnin, Open Wide, Lyon, France
 *      - Creation and implementation
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Comments and refactoring
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - Added delete, query
 *******************************************************************************/
package org.eclipse.jwt.wam.api;

import org.eclipse.jwt.wam.api.model.Activity;
import org.eclipse.jwt.wam.api.model.Process;
import org.eclipse.jwt.wam.api.model.Processes;
import org.eclipse.jwt.wam.api.model.Project;
import org.eclipse.jwt.wam.api.model.Projects;
import org.eclipse.jwt.wam.api.query.ProcessQuery;


/**
 * @author gdecarnin
 * 
 * NB. These methods do not "officially" throw BonitaServerException because
 * it will rather result in an UndeclaredThrowableException in a ws client.
 */
public interface WorkflowService {

	/**
	 * List all projects (aka Package in XPDL)
	 * @param returnProperties
	 * @param startableOnly
	 * @param login
	 * @return
	 */
    public Projects listProjects(boolean returnProperties, boolean startableOnly, String login); // throws BonitaServerException

	/**
	 * get a specific project (aka Package in XPDL) properties
	 * @param returnProperties
	 * @param startableOnly
	 * @param login
	 * @return
	 */
    public Project getProjectProperties(Project project, String login); // throws BonitaServerException

    /**
     * Starts a new Activity (aka Process in XPDL)
     * @param process
     * @param login
     * @return The Process bean with processInstance and processState updated
     */
    public Process instanciateActivity(Process process, String login); // throws BonitaServerException

    /**
     * List all running activities (ie Processes in XPDL) in which user in implied
     * @param returnProperties if yes, result beans will contain updated properties
     * @param returnTerminated if yes, returns also processes where the used WAS implied
     * @param login
     * @return The processes beans, with at least instanceName and state updated
     */
    public Processes listUserActivities(boolean returnProperties, boolean returnTerminated, String login); // throws BonitaServerException

    /**
     * Allows pagination and server-side filtering
     * @param processQuery
     * @param login
     * @return
     */
    public Processes listUserActivities(ProcessQuery processQuery, String login); // throws WorkflowServerException
    
    /**
     * Starts an action (ie an XPDL activity)
     * @param process a details process bean, with instanceName and properties set
     * @param login
     * @return
     */
    public Process startAction(Process process, String login); // throws BonitaServerException

    /**
     * Returns an action (ie a XPDL Activity) properties
     * @param process
     * @param login
     * @return
     */
    public Activity getActionProperties(Process process, String login); // throws BonitaServerException

    /**
     * Sets an action (ie a XPDL Activity) properties
     * @param process
     * @param login
     * @return
     */
    public Activity setActionProperties(Process process, String login); // throws BonitaServerException

    /**
     * Ends an action (ie a XPDL Activity) properties
     * @param process
     * @param login
     * @return
     */
    public Process terminateAction(Process process, String login); // throws BonitaServerException
    
    /**
     * Cancels an activity (ie a XPDL Process)
     * @param process
     * @param deleteHistory also deletes it from history (logs). By default, is false.
     * @param login
     * @return
     */
    public Process deleteActivity(Process process, boolean deleteFromHistory, String login); // throws WorkflowServerException
    
}
