/**
 * File:    Property.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Guillaume Decarnin, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.wam.api.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Property implements Serializable {
    private static final long serialVersionUID = 7407375935328204599L;
    
    private String key;
    private Object value;

    public Property() {}

    public Property(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public static Property[] toArray(HashMap<String, Object> map) {
        Property[] properties = new Property[map.size()];
        int i = 0;
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            properties[i] = new Property(entry.getKey(), entry.getValue());
            i++;
        }
        return properties;
    }

    @Override
    public String toString() {
        return key + "=" + value;
    }
}
