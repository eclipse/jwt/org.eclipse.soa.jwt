Java Workflow Tooling 1.2.0
---------------------------

JWT 1.2 contains bugfixes and stability improvements and will be
adapted to run on the Eclipse 4.2 platform


Java Workflow Tooling 1.1.0
---------------------------

JWT 1.1 contains several bugfixes


Java Workflow Tooling 1.0.0
---------------------------

JWT 1.0 contains several bugfixes


Java Workflow Tooling 0.7.0
---------------------------

JWT 0.7.0 features a separated meta model plugin, view data stored in a different file,
multiple view data per element, layout algorithms and many bug fixes: 

206999  WONT   	generic event dispatcher
238259 	FIXE 	Add extension point for views
215238 	INVA 	Adapt simulator to changed namespaces
232953 	INVA 	Problems with rights in Vista
248567 	FIXE 	source out metamodel?
249916 	FIXE 	extend/rewrite the converter
296235 	FIXE 	converter from 0.6.0 to 0.7.0
297843 	FIXE 	Ecore file not accessible for transformations
262786 	DUPL 	Transformations from BPMN to JWT
277084 	FIXE 	Export wizard should propose to save input model if necessary
297844 	FIXE 	Adapt transformations to changes in the metamodel
298046 	FIXE 	BPMN transformation generates files in version 0.6.0
205835 	FIXE 	translate 'official' eclipse commands
215187 	WONT 	problem with toolbar when converter is running
216458 	INVA 	Mac installer does not work
248565 	FIXE 	separate view-specific model content and support different view data
258401 	WORK 	unable to add eddges (reference and/or activity) to workflow after it has run in AgilPro Simulator
258622 	FIXE 	activity is not removed if it's deleted indirectly
258626 	FIXE 	import workflow templates into existing workflows
258816 	FIXE 	include layout algorithms
262357 	WORK 	Extension point for marking activity nodes
263485 	FIXE 	Reference activated instead of element
275874 	WONT 	display datatypes in the data field on the overview page
279605 	FIXE 	Open wizard when clicking on the JWT entry from the "sample" section of welcome page
285165 	FIXE 	Apply layout algorithm does not work
294791 	FIXE 	The screen was not correctly displayed, it was necessary to restart Eclipse
295572 	FIXE 	New workflow model gets opened twice in workflow editor
295887 	DUPL 	References are not built automatically within the workflow editor
296224 	FIXE 	view data consistency check
296231 	FIXE 	dynamic properties tab is not shown
296232 	FIXE 	view dropdown text changes
296238 	FIXE 	transition of plugins to 0.7.0
296241 	FIXE 	regenerate view files for 0.7.0 metamodel
297482 	FIXE 	exception in outline view
297945 	FIXE 	wrong behavior after loading error
298035 	FIXE 	Creating a new file does not open the newly created file
298036 	FIXE 	Event has no icon in the palette
298039 	FIXE 	Some more icons missing or not set correctly
298040 	FIXE 	Several warnings from NLS and handler conflicts
298043 	FIXE 	Aspects from 0.6.0 can not be loaded anymore
298044 	FIXE 	JWT Examples need to be converted
298045 	FIXE 	Views have strange behavior
298048 	FIXE 	EPC View: Conf file could not be found
298053 	FIXE 	check exported folders
296319 	FIXE 	Store default values in conf-file
262021 	WONT 	Make XPDL exported files compatible with Enhydra Shark
202259 	WONT 	Data handling
249361 	DUPL 	Migrate EMF templates to Ganymede
286920 	FIXE 	HTML docu does not work when data without name exists
293986 	FIXE 	Allow to open a workflow model that uses unknown Aspect extensions
296332 	FIXE 	Views do not work anymore for outsourced metamodel
298038 	DUPL 	UML Activity diagram view does not have own figures
276694 	FIXE 	Only one external view in workflow editor possible
289035 	FIXE 	"Save as" to new file does not save attached aspects
293939 	FIXE 	Provide Logging Aspect sample


JWT Workflow Editor 0.6.0
-------------------------

205163 	Pasting an object inside a SAN --> Object gets "strange" position
215634 	flow templates don't include types
225704  Include generic properties
238883 	show additional information in the status bar
241567 	Typed metamodel extensions (Aspects)
244825 	Transformations from JWT to STP-IM
247917 	ExternalActions don't listen to model selection changes
248698 	bug in uml view when process contains subprocesscall
249270  Integration of JWT to BPMN transformation
249274 	improve the flow editor perspective
249333 	activate multipropertytab only if additional tabs were registered to jwt-we
249335 	bug when selecting a view in plugin mode
249437 	Develop a new view editor
249911 	Error in view selection toolbar element
250050 	error when selected view was not found
251240 	nullpointerexception cutting edges
251376 	graphical outline viewer
251697 	keep open flow in toolbar?
252932 	Set of extensions for property Descriptor
253144 	Add import and export transformations into the import and export Eclipse menu
254571 	Replace jdom.jar dependency inclusion by a dependency to org.jdom provided by Orbit
256395 	performance issues
256560 	alileo issues
256563 	NPE in modelcontentoutlinepage
256612 	problem with view dropdown in toolbar label
256617 	error loading external view files
256694 	Add a graphical overview outline page
257195 	Disable overview page
257224 	Associate a custom palette factory to a view
257400 	Allow to specify custom PropertySheet through dedicated extension point
257403 	Views.displayObject() should return true by default
257404 	Improve model load error handling for metamodel extensions
257698 	npex in ExtensionPointNotifyChangedListener when closing flow
257725 	It is not able to extend the editPart actions
257736 	Dependancy to Sun JWM because of platform line separator
257942 	Overlap in Shortcuts
258414 	New Wizard: Change category
259390 	NPE in export as XPDL
259476  JWT-Converter mentions build version
259578 	Eclipse build base on build.eclipse.org is x gtk x86 whereas build.eclipse.org is a PPC
260233 	Profile and Aspect development not possible in the new outline
260521 	The "New..." wizard contains 2 categories "Java flow Tooling"
260704 	Internationalize transformation plugins
261010 	add examples to eclipse category
262781 	Refactoring of XSL based transformations
262783 	New view for EPCs
263241 	FactoryRegistry as an extension point.
263959 	Aspects cause many warnings
264149 	Lanes in JWT to BPMN transformation
264172 	New flow not opened when created
264849 	Add support for drag'n'drop from other plugins into WE
265493 	Add support for double click through an extension point
265805 	Cannot install Xalan from p2 (JWT integration update site error)
266134 	Conf Editor : Improve ecore urce loading
266277 	In ConfEditor, unable to load org.eclipse.jwt.we.conf.property EMF model
266279 	Worflow editor does not open with Galileo M5
266465 	Extraction of properties from Model (including aspect) to a .properties file
266821 	Bad namespace in jwt2xpdl transformation
266875 	"Switch view" widget is sometimes not displayed in toolbar
267029 	input output data not correct
267600 	BPMN to JWT Transformation
268172 	Improve error handling in XPDL transfo
268301 	[XPDl] ForkNode lved to Split XOR instead of Split AND
269412 	UML AD-View with wrong figures
270135 	"Open Type" widget for javaClass Application atbute
272132 	Include line numbers in built classes
273077 	NPE when trying to set method on Application
274812 	Change license text for JWT Feature
274817 	NLS missing for documentation plugin
274995 	Move to ATL v3
275428 	ClassNotFoundException in the ConfEditor
275639 	Getting java.lang.NullPointerException in JWT View File
275878 	created data doesn't show up in the palette
276320 	ManageActivatedProfile UI : enable and disable buttons  badly
276504 	Fix loading of installed conf model from bundles
276680 	Change name of conf-model editor
276689 	Own icon for JWT Conf-model.editor
276703 	Latest build shows new error
276704 	Clean CVS before the official release
276888 	error on activating perspective
277085 	Error in JWT to BPMN transfo when no role/lane is associated to task
277086 	Errors during transformations are silent
277436 	Key bindings conflicts on @ï¿½$ with Danish keyboard layout
277670 	set logging level to warn
277673 	warning on start
278052 	NLS messaging not clean in EPC view plugin
279275 	Feature name should include (Incubation)
280239 	Toolbar entries
280261 	Reorganize plugin names 


JWT Workflow Editor 0.5.0
-------------------------

Features:

[238879] add feedback figure when inserting objects 
[238880] add shadow to figures 
[214897] Colors in the graphical editor 
[239408] Allow several routers 
[225706] Extend views with different figures 
[238259] Add extension point for views 
[240499] Add of an extension point to customise PropertyEditor 
[240502] Add an extension point for changeNotification 
[221479] Language pack: French 
[238405] drag and drop in the outline view 
[238883] show additional information in the status bar
[248528] add extension point for additional property tabs
[248531] filter options for outline view
[248395] provide extension mechanism for custom editor sheets

Changed :

[201188] rewrite ReferenceEdge code 
[207000] add offset to pasted objects 
[238882] improve quality of several figures/figure icons 
[238885] add external functions to context menu 
[248529] put views and external actions in the toolbar into dropdowns
[248450] Remove info from toolbar and from WE completely

Bugfixes:

[201185] bug when manipulating reference-edges when the corresponding scope is not displayed 
[201186] Problems handling connections 
[205155] Edges are still displayed if resizing a StructuredActivityNode 
[215151] bug when copying, cutting, pasting connections 
[216521] cuttoclipboard acts as delete command 
[221646] Load from URL does not work 
[221647] �Save as� does not work 
[223607] Ganymede: Unable to open files 
[238402] Zoom problems: nullpointerexception and enablement error 
[201193] add tutorial to plugin
[242203] Silent NullPointerException when cancelling New Workflow from Wizard 
[241936] (catched) npe when setting an edge�s target 
[241934] NullPointerException when switching between JWT and development Eclipse 
[238884] exception when renaming elements which are not shown in the graphical editor 
[238404] Aborting a drag of a scope from outline to editor affects the commandstack 
[248532] create refelements on subprocess bug
[248534] bug when deleting referenced objects and their references at the same time
[248535] bug in dragndrop from outline to editor
+ many more small bugfixes


JWT Workflow Editor 0.4.0
-------------------------

- Feature: Integrated HTML documentation plugin 
- Feature: Integrated BPEL code generation plugin 
- Feature: A grid can be displayed (configurable via menu and preferences)
- Feature: Functions to graphically align model elements
- Feature: Guard is now directly selectable and editable via diect edit
- Feature: Guard trim/wrap/color settable in preferences  
- Feature: Preference pages for appearance (font/color/other) added 
- Feature: Improved overview sheet layout 
- Feature: The current selection in the overview sheet is synchronized with the outline view 
- Feature: The context menu is accessible in the overview sheet 
- Feature: Packages that contain information are expanded automatically in the overview sheet on loading
- Feature: Wizards opened from overview sheet preselect the package which was selected in the overview sheet 
- Feature: Added author and version to "create workflow wizard" 
- Feature: Added "create package" button to wizards 
- Feature: First activity tab is now closable 
- Feature: Added option to create standard packages for applications/roles/data/datatypes to creation wizard 
- Feature: Standard datatypes added to data wizard 
- Feature: When the user wants to delete a still referenced element, the references can be automatically deleted as well 

- Changed: File information now always displays full file path in overview sheet
- Changed: Icon dialog (wizards, properties) shows icon directory by default 
- Changed: Removed open workflow entries from file menu in plugin mode 
- Changed: Overview page always shows standard activity icon 
- Changed: If an icon cannot be found, the standard icon of the corresponding type is shown
- Changed: The icon location can now be given relative to the eclipse root directory
- Changed: Changed some icons to free epl licensed icons
- Changed: Redesigned installer

- Bugfix: Toolbar is displayed correctly when creating a workflow using the wizard
- Bugfix: If a workflow file cannot be created or opened, a proper error message is displayed
- Bugfix: Overview sheet is updated when view is changed 
- Bugfix: Error message if plugin.properties was not found is hard coded to prevent infinite loop 
- Bugfix: The file path in the overview sheet is updated if it changes (e.g. 'save as') 
- Bugfix: Fixed possible exception in delete buttons in overview sheet if selection is invalid 
- Bugfix: Removed nullpointerexception in palette when deleting subpackages with entries  
- Bugfix: Context menu entries cut/copy/paste/delete were never enabled 
- Bugfix: "create child"/"create sibling" actions now work two times in a row without changing selection 
- Bugfix: Some small fixes 

- Metamodel: Removed version from EMF model namespaces

- Internal: Changed versioning scheme (1.4.0 -> 0.4.0)
- Internal: Updated converter to fit old and new versioning scheme
- Internal: Improved preference interface
- Internal: New package for miscellaneous files/packages 
- Internal: Some structural refactoring 
- Internal: Removed unnecessary dependencies 

Known Issues:
- Cut/Copy to clipboard and Paste from clipboard may not always work as expected
- When Undo/Redo affects ReferenceEdges, the corresponding Activity must be opened 


JWT Workflow Editor 0.3.0
-------------------------

- Metamodel: New fileversion attribute to model concept 
- Metamodel: Model namespaces changed to "org.eclipse.jwt/1.3.0" 
- Metamodel: GuardSpecifications are now read only

- Feature: Both plugin and RCP application now have a toolbar 
- Feature: It is now possible to open multiple processes in a workflow 
- Feature: New SubProcessCall element which links to another process 
- Feature: Extension point 'menu' added for including plugins in menu/toolbar 
- Feature: JWT adapted for Eclipse plugin mode 
- Feature: Improved error handling when writing guard restrictions
- Feature: New plugin mode wizard which adds the new workflow file to a project
- Feature: New datatype wizard
- Feature: New package wizard
- Feature: Now it is possible to save a process as an image
- Feature: Zoom for processes
- Feature: Keybindings can now be defined using the preference page

- Changed: [agil-mod] project became [jwt-we] (plugin) and [agil-mod] (RCP)
- Changed: JWT uses the licence EPL
- Changed: JWT and AgilPro are adapted to Eclipse 3.3 
- Changed: File extension changed from '.agilpro' to '.workflow'
- Changed: Added Eclipse/JWT to copyright information  
- Changed: Internal actions adapted to Eclipse command/handler interface 
- Changed: Menu/Toolbar uses the new org.eclipse.ui.menus extension point
- Changed: Renamed StructuredActivityNode to EmbeddedSubProcess 
- Changed: Icons for most menu entries added
- Changed: GuardSpecifications are hidden in Business View
- Changed: Sample processes improved
- Changed: New version of IzPack installer (3.10.2)
- Changed: New main preference page under which plugins may add their own pages 

- Bugfix: Some deprecated parts were adapted to fit Eclipse 3.3 
- Bugfix: Eclipse 'Open file' menu entry in RCP mode removed
- Bugfix: If an editor is opened, the activity is selected, otherwise the model element
- Bugfix: Fixed shortcuts for menu entries
- Bugfix: Umlaut bug in guard creation
- Bugfix: Rebuilt context menu
- Bugfix: Wrong image size of some icons
- Bugfix: Wizard bug on Mac
- Bugfix: "Location" and "Last modified" at the overview page now work in jwt-we
- Bugfix: Location path is displayed in the OS specific version
- Bugfix: Fixed 'open file' problem with Eclipse 3.3 if path is lies not in workspace
- Bugfix: Removed Override tags that caused problems with some Eclipse versions
- Bugfix: Adapted manifest to fit Eclipse standards
- Bugfix: Many small bugs