This project builds the JWT update site

Build only update site
----------------------
Goals: clean verify


Build update site, pack200, sign, promote (only on Hudson)
----------------------------------------------------------
Goals: clean install
Profiles: eclipse-sign, publish


Local build
-----------
* Adapt "relativePath" attributes in all project pom.xml
* Build only update site