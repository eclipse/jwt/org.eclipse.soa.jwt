<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
    http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <modelVersion>4.0.0</modelVersion>
  <groupId>org.eclipse.jwt</groupId>
  <artifactId>parent</artifactId>
  <version>1.6.0.qualifier</version>
  <packaging>pom</packaging>
  
 <!-- jwt modules -->
  <modules>
    <module>../../metamodel/jwt-converter</module>
    <module>../../metamodel/jwt-metamodel</module>
    <module>../../we/jwt-we</module>
    <module>../../we/jwt-we-conf-model</module>
    <module>../../we/jwt-we-conf-model.edit</module>
    <module>../../we/jwt-we-conf-model.editor</module>
    <module>../../we/jwt-we-conf-model.we</module>
    <module>../../we/jwt-we-conf-property-model</module>
    <module>../../we/jwt-we-conf-property-model.edit</module>
    <module>../../we/jwt-we-view-editor</module>
    <module>../../we-plugins/jwt-we-helpers-application</module>
    <module>../../we-plugins/jwt-we-helpers-files</module>
    <module>../../we-plugins/jwt-we-action-doc</module>
    <module>../../we-plugins/jwt-we-view-epc</module>
    <module>../../we-plugins/jwt-we-view-uml</module>
    <module>../../we-plugins/jwt-we-view-bpmn</module>
    <module>../../releng/jwt-feature-branding</module>
    <module>../../releng/jwt-we-example</module>
    <module>../../releng/jwt-tests-plugin</module>
    <module>../../transformations/jwt-transformation-stpim</module>
    <module>../../transformations/jwt-transformation-base</module>
    <module>../../transformations/jwt-transformation-bpmn</module>
    <module>../../transformations/jwt-transformation-properties</module>
    <module>../../transformations/jwt-transformation-xpdl</module>
    <module>../../transformations/jwt-transformation-xslt-tools</module>
    <module>../../releng/org.eclipse.jwt.feature</module>
    <module>../../releng/org.eclipse.jwt.test.feature</module>
    <module>../../releng/org.eclipse.jwt.p2updatesite</module>
  </modules>

  <properties>
    <tycho-version>0.24.0</tycho-version>
    <maven.build.timestamp.format>yyyyMMdd-HHmm</maven.build.timestamp.format>
    <download-publish-path>/home/data/httpd/download.eclipse.org/jwt/</download-publish-path>
  </properties>

 <!-- eclipse build platform -->
  <repositories>
    <repository>
      <id>eclipse-neon</id>
      <layout>p2</layout>
      <url>http://download.eclipse.org/releases/neon/</url>
    </repository>
  </repositories>

 <!-- repositories for maven plugins -->
 <pluginRepositories>
  
    <pluginRepository>
      <id>maven.eclipse.org</id>
      <url>https://repo.eclipse.org/content/groups/tycho/</url>
    </pluginRepository>
    
	<pluginRepository>
      <id>cbi</id>
      <url>https://repo.eclipse.org/content/repositories/cbi-releases/</url>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </pluginRepository>
   
  </pluginRepositories>

  <build>
    <plugins>

      <plugin>
        <groupId>org.eclipse.tycho</groupId>
        <artifactId>tycho-maven-plugin</artifactId>
        <version>${tycho-version}</version>
        <extensions>true</extensions>
      </plugin>
    
	  <!-- tycho versions plugin -->
	  <plugin>
	    <groupId>org.eclipse.tycho</groupId>
	    <artifactId>tycho-versions-plugin</artifactId>
	    <version>${tycho-version}</version>
  	  </plugin>
    
      <plugin>
        <groupId>org.eclipse.tycho</groupId>
        <artifactId>tycho-surefire-plugin</artifactId>
        <version>${tycho-version}</version>
 
        <configuration>
         
          <useUIHarness>true</useUIHarness>
         
          <testFailureIgnore>true</testFailureIgnore>
        
          <includes>
            <include>**/AllTests.java</include>
          </includes>
          
        </configuration>
      </plugin>

    </plugins>
  </build>
  

<!-- build profiles -->
<profiles>

	<!-- sign jars on eclipse server -->
    <profile>
    
      <id>eclipse-sign</id>
      
      <build>
        <plugins>
        
          <plugin>
            <groupId>org.eclipse.tycho</groupId>
            <artifactId>target-platform-configuration</artifactId>
            <version>${tycho-version}</version>
            
            <configuration>
              <includePackedArtifacts>false</includePackedArtifacts>
            </configuration>
          
          </plugin>
          
          <plugin>
            <groupId>org.eclipse.tycho.extras</groupId>
            <artifactId>tycho-pack200a-plugin</artifactId>
            <version>${tycho-version}</version>
            <executions>
              <execution>
                <id>pack200-normalize</id>
                <goals>
                  <goal>normalize</goal>
                </goals>
                <phase>verify</phase>
              </execution>
            </executions>
          </plugin>
           
          <plugin>
            <groupId>org.eclipse.cbi.maven.plugins</groupId>
            <artifactId>eclipse-jarsigner-plugin</artifactId>
            <version>1.0.4</version>
            <executions>
              <execution>
                <id>sign</id>
                <goals>
                  <goal>sign</goal>
                </goals>
                <phase>verify</phase>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.eclipse.tycho.extras</groupId>
            <artifactId>tycho-pack200b-plugin</artifactId>
            <version>${tycho-version}</version>
            <executions>
              <execution>
                <id>pack200-pack</id>
                <goals>
                  <goal>pack</goal>
                </goals>
                <phase>verify</phase>
              </execution>
            </executions>
          </plugin>
          
          <plugin>
            <groupId>org.eclipse.tycho</groupId>
            <artifactId>tycho-p2-plugin</artifactId>
            <version>${tycho-version}</version>
            <executions>
              <execution>
                <id>p2-metadata</id>
                <goals>
                  <goal>p2-metadata</goal>
                </goals>
                <phase>verify</phase>
              </execution>
            </executions>
            <configuration>
              <defaultP2Metadata>false</defaultP2Metadata>
            </configuration>
          </plugin>
          
        </plugins>
      </build>
    </profile>

	<!-- init hudson variables for local build -->
	<profile>
	    <id>Hudson Surrogate Params</id>
	    <properties>
	        <env.BUILD_NUMBER>UNKNOWN</env.BUILD_NUMBER>
	        <env.BUILD_ID>${maven.build.timestamp}</env.BUILD_ID>
	        <env.JOB_NAME>${project.name}</env.JOB_NAME>
	        <env.BUILD_TAG>${user.name}-${env.JOB_NAME}-${project.version}_${env.BUILD_NUMBER}-${env.BUILD_ID}</env.BUILD_TAG>
	        <env.EXECUTOR_NUMBER>0</env.EXECUTOR_NUMBER>
	        <env.WORKSPACE>${basedir}</env.WORKSPACE>
	        <env.HUDSON_URL>UNKNOWN</env.HUDSON_URL>
	        <env.SVN_REVISION>rUNKNOWN</env.SVN_REVISION>
	    </properties>
	    <activation>
	        <property>
	            <!-- Activated if Hudson hasn't already set the BUILD_NUMBER -->
	            <name>!env.BUILD_NUMBER</name>
	        </property>
	    </activation>
	</profile>
	
  </profiles>

</project> 