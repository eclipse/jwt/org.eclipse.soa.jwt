/*
 * Copyright (c) 2007-2009 Obeo
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *    University of Augsburg - adapted from SCA to the JWT project
 */
package org.eclipse.jwt.examples.wizards;

import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;


/**
 * 
 * @author Florian Lautenbacher, thanks to Stephane Drapeau
 * 
 */
public class JWTExample
{

	/**
	 * 
	 */
	private URL zipURL;

	/**
	 * 
	 */
	private String title;

	/**
	 * 
	 */
	private String description;

	/**
	 * 
	 */
	private ImageDescriptor imageDesc;

	/**
	 * 
	 */
	private int difficulty;


	/**
	 * @param zipURLC
	 * @param titleC
	 * @param descC
	 * @param difficulty
	 *            the lowest value means the easiest level
	 * @param imageDescC
	 */
	public JWTExample(URL zipURLC, String titleC, String descC, int difficulty,
			ImageDescriptor imageDescC)
	{
		this.zipURL = zipURLC;
		this.title = titleC;
		this.description = descC;
		this.difficulty = difficulty;
		this.imageDesc = imageDescC;
	}


	/**
	 * @return
	 */
	public URL getZipURL()
	{
		return zipURL;
	}


	/**
	 * @return
	 */
	public String getTitle()
	{
		return title;
	}


	/**
	 * @return
	 */
	public String getDescription()
	{
		return description;
	}


	/**
	 * @return the difficulty
	 */
	public int getDifficulty()
	{
		return difficulty;
	}


	/**
	 * @return
	 */
	public ImageDescriptor getImageDesc()
	{
		return imageDesc;
	}

}
