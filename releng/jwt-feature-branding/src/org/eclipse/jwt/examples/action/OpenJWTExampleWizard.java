/**
 * File:    OpenJWTExampleWizard
 * Created: 18.12.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany, http://www.ds-lab.org/
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.examples.action;

import java.util.Properties;

import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jwt.examples.wizards.NewExampleWizard;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.config.IIntroAction;

/**
 * opens the wizard page from the Eclipse Intro page, section "Samples", see also
 * intro/css/overviewExtensionContent.xml where a specific command defined in the
 * plugin.xml is called which finally calls this class
 * @author F. Lautenbacher
 *
 */
public class OpenJWTExampleWizard implements IIntroAction{

	/**
	 * @see org.eclipse.ui.intro.config.IIntroAction.run()
	 */
	public void run(IIntroSite site, Properties params) {
		NewExampleWizard newiz = new NewExampleWizard();		
		newiz.init(site.getWorkbenchWindow().getWorkbench(), null);
		WizardDialog dialog = new WizardDialog(site.getWorkbenchWindow().getWorkbench().getActiveWorkbenchWindow().getShell(), newiz);
		dialog.create();
		dialog.open();
	}
}
