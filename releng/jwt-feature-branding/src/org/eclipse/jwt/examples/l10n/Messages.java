/*
 * Copyright (c) 2007-2009 Obeo
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *    University of Augsburg - adapted from the SCA to the JWT project
 */
package org.eclipse.jwt.examples.l10n;

import org.eclipse.osgi.util.NLS;


/**
 * 
 * @author Florian Lautenbacher, thanks to Stephane Drapeau
 * 
 */
public final class Messages
		extends NLS
{

	private static final String BUNDLE_NAME = "org.eclipse.jwt.examples.l10n.Messages";//$NON-NLS-1$


	private Messages()
	{
		// Do not instantiate
	}

	public static String monitor_creatingProject;
	public static String monitor_unzippingProject;
	public static String JWTExampleWizardPage_pageName;
	public static String JWTExampleWizardPage_title;
	public static String JWTExampleWizardPage_desc;
	public static String JWTExampleWizardPage_icon;
	public static String WizardNewProjectCreationPage_pageName;
	public static String WizardNewProjectCreationPage_title;
	public static String WizardNewProjectCreationPage_desc;

	static
	{
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

}