/**
 * File:    TestXPDLTransformation.java
 * Created: 14.01.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009    Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.tests.transformation.xpdl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.TransformerFactory;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.eclipse.jwt.transformations.api.TransformationException;
import org.eclipse.jwt.transformations.xpdl.impl.Jwt2XpdlService;

public class TestXPDLTransformation extends TestCase {
	/**
	 * Constructor for TestVersion.
	 * @param name
	 */
	public TestXPDLTransformation(String name) {
		super(name);
	}

	/**
	 * Returns the JUnit test suite that implements the <b>TestVersion</b>
	 * definition.
	 */
	public static Test suite() {
		TestSuite testVersion = new TestSuite(TestXPDLTransformation.class);
		return testVersion;
	}
	
	public void testHasXSLTransformer() throws Exception {
		TransformerFactory.newInstance();
	}
	
	public void testBug259390() throws Exception {
		FileInputStream fis = new FileInputStream(getXPDLFile());
		assertTrue("XPDL file seems too small to be good", fis.available() > 100);
	}
	
	public void testBug266821() throws Exception {
		File file = getXPDLFile();
		BufferedReader bufferedInputStream = new BufferedReader(new FileReader(file));
		String line = null;
		while ((line = bufferedInputStream.readLine()) != null) {
			if (line.contains("xmlns=\"\""))
				fail("Output XPDL file must not contain xmlns=\"\"\n" + line);
		}
	}

	private File getXPDLFile() throws IOException, Exception, TransformationException, FileNotFoundException {
		Jwt2XpdlService jwt2xpdlTransfo = new Jwt2XpdlService();
		InputStream input = getClass().getResourceAsStream("ExternalApplications.workflow");
		assertNotNull("Cannot find resource file for test", input);
		File inputFile = File.createTempFile("testBug259290_", ".workflow");
		assertTrue("inputFile " + inputFile.getAbsolutePath() + " does not exist", inputFile.exists());
		copyFile(input, inputFile);
		File outputFile = File.createTempFile("testBug259290_", ".xpdl");
		assertTrue("outputFile " + inputFile.getAbsolutePath() + " does not exist", outputFile.exists());
		jwt2xpdlTransfo.transform(inputFile.getAbsolutePath(), outputFile.getAbsolutePath());
		return outputFile;
	}
	
	public static void copyFile(InputStream in, File out) throws Exception {
	    FileOutputStream fos = new FileOutputStream(out);
	    try {
	        byte[] buf = new byte[1024];
	        int i = 0;
	        while ((i = in.read(buf)) != -1) {
	            fos.write(buf, 0, i);
	        }
	    } 
	    catch (Exception e) {
	        throw e;
	    }
	    finally {
	        if (in != null) in.close();
	        if (fos != null) fos.close();
	    }
	  }
}
