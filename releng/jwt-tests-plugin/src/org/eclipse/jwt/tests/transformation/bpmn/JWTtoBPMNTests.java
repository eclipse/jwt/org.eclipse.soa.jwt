/**
 * File:    JWTtoBPMNTests.java
 * Created: 19.01.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  Open Wide (www.openwide.fr)
 * 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - creation and implementation (Bug 249270) 
 *******************************************************************************/
package org.eclipse.jwt.tests.transformation.bpmn;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.eclipse.jwt.transformations.api.TransformationService;
import org.eclipse.jwt.transformations.bpmn.Jwt2BpmnService;

public class JWTtoBPMNTests extends TestCase {
	
	public static TestSuite suite() {
		return new TestSuite(JWTtoBPMNTests.class);
	}
	

	public void testBug249270Sequence() throws Exception {
		TransformationService transfo = new Jwt2BpmnService();
		InputStream input = getClass().getResourceAsStream("ExternalApplications.workflow");
		assertNotNull("Cannot find resource file for test", input);
		File inputFile = File.createTempFile("testBug249270_", ".workflow");
		assertTrue("inputFile " + inputFile.getAbsolutePath() + " does not exist", inputFile.exists());
		copyFile(input, inputFile);
		File outputFile = File.createTempFile("testBug249270_", ".bpmn");
		assertTrue("outputFile " + inputFile.getAbsolutePath() + " does not exist", outputFile.exists());
		transfo.transform(inputFile.getAbsolutePath(), outputFile.getAbsolutePath());
		FileInputStream fis = new FileInputStream(outputFile);
		assertTrue("Export result BPMN File seems too small to be good", fis.available() > 300);
	}
	
	public void testBug277085NoRole() throws Exception {
		TransformationService transfo = new Jwt2BpmnService();
		InputStream input = getClass().getResourceAsStream("testNoRole.workflow");
		assertNotNull("Cannot find resource file for test", input);
		File inputFile = File.createTempFile("testBug277085_", ".workflow");
		assertTrue("inputFile " + inputFile.getAbsolutePath() + " does not exist", inputFile.exists());
		copyFile(input, inputFile);
		File outputFile = File.createTempFile("testBug277085_", ".bpmn");
		assertTrue("outputFile " + inputFile.getAbsolutePath() + " does not exist", outputFile.exists());
		transfo.transform(inputFile.getAbsolutePath(), outputFile.getAbsolutePath());
		FileInputStream fis = new FileInputStream(outputFile);
		assertTrue("Export result BPMN File seems too small to be good", fis.available() > 300);
	}
	
	/*
	 * somehow a merge node causes problems, but I don't find the cause of this problem
	public void testMergeNode() throws Exception {
		TransformationService transfo = new Jwt2BpmnService();
		InputStream input = getClass().getResourceAsStream("testMergeNode.workflow");
		assertNotNull("Cannot find resource file for test", input);
		File inputFile = File.createTempFile("testMergeNode_", ".workflow");
		assertTrue("inputFile " + inputFile.getAbsolutePath() + " does not exist", inputFile.exists());
		copyFile(input, inputFile);
		File outputFile = File.createTempFile("testMergeNode_", ".bpmn");
		assertTrue("outputFile " + inputFile.getAbsolutePath() + " does not exist", outputFile.exists());
		transfo.transform(inputFile.getAbsolutePath(), outputFile.getAbsolutePath());
		FileInputStream fis = new FileInputStream(outputFile);
		assertTrue("Export result BPMN File seems too small to be good", fis.available() > 300);
	}
	*/
	
	public static void copyFile(InputStream in, File out) throws Exception {
	    FileOutputStream fos = new FileOutputStream(out);
	    try {
	        byte[] buf = new byte[1024];
	        int i = 0;
	        while ((i = in.read(buf)) != -1) {
	            fos.write(buf, 0, i);
	        }
	    } 
	    catch (Exception e) {
	        throw e;
	    }
	    finally {
	        if (in != null) in.close();
	        if (fos != null) fos.close();
	    }
	    fos.close();
	  }
}
