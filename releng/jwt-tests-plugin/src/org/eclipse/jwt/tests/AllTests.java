/**
 * File:    AllTests.java
 * Created: 14.01.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009    Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and Implementation
 *    Florian Lautenbacher, University of Augsburg, Germany
 *      - Adding some more tests, adapted code
 *******************************************************************************/
package org.eclipse.jwt.tests;

import org.eclipse.jwt.tests.integration.TestIntegrationInPlatform;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests extends TestSuite {

	public static Test suite() {
		
		return new AllTests();
	}
	
	public AllTests()
	{
		addTest(org.eclipse.jwt.tests.we.AllTests.suite());
		addTest(org.eclipse.jwt.tests.transformation.xpdl.TestXPDLTransformation.suite());
		addTest(org.eclipse.jwt.tests.transformation.bpmn.JWTtoBPMNTests.suite());
		addTest(org.eclipse.jwt.tests.transformation.bpmn.BPMNtoJWTTests.suite());
		addTest(org.eclipse.jwt.tests.transformation.stpim.JWTtoSTPIMTests.suite());
		//suite.addTest(org.eclipse.jwt.transformation.jpdl.test.TestJPDLTransformation.suite());
		addTest(org.eclipse.jwt.tests.converter.Convert060ToCurrentTests.suite());
		addTest(org.eclipse.jwt.tests.conf.editor.TestConfEditor.suite());
		addTest(TestIntegrationInPlatform.suite());
	}

}
