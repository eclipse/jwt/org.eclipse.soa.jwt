package org.eclipse.jwt.tests.conf.editor;

import java.net.URI;
import java.net.URL;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.jwt.we.conf.presentation.ConfEditor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

/**
 * @since 0.7
 */
public class TestConfEditor extends TestCase {

	/**
	 * Returns the JUnit test suite that implements the
	 * <b>TestIntegrationInPlatform</b> definition.
	 */
	public static Test suite() {
		TestSuite suite = new TestSuite(TestConfEditor.class);
		return suite;
	}

	public void testOpenEditor() throws Exception {
		URL resource = getClass().getResource("My.conf");
		URI resourceUri = new URI(FileLocator.toFileURL(resource)
				.toExternalForm().replaceAll(" ", "%20"));

		IEditorPart editor = null;
		try {
			IWorkbenchPage page = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage();
			editor = IDE.openEditor(page, resourceUri,
					"org.eclipse.jwt.we.conf.presentation.ConfEditorID", true);
			assertTrue("Workflow model is not opened with JWT Editor",
					editor instanceof ConfEditor);
		} catch (Exception e) {
			String editorInfo = "<null>";
			if (editor != null)
				editorInfo = editor.getClass().getCanonicalName();
			editorInfo += " instead of " + ConfEditor.class.getCanonicalName();

			fail("could not open editor: " + editorInfo);
			e.printStackTrace();
		}

	}

}
