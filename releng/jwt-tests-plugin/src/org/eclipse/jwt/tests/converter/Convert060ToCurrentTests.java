/**
 * File:    Convert06to07Tests.java
 * Created: 15.12.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  University of Augsburg
 * 
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, University of Augsburg, Germany
 *      - creation and implementation (Bug 296235) 
 *******************************************************************************/
package org.eclipse.jwt.tests.converter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.eclipse.jwt.converter.Converter;

import junit.framework.TestCase;
import junit.framework.TestSuite;

public class Convert060ToCurrentTests extends TestCase {
	
	public static TestSuite suite() {
		return new TestSuite(Convert060ToCurrentTests.class);
	}
	

	public void testConverterNormal() throws Exception {
		InputStream input = getClass().getResourceAsStream("ExternalApplications06.workflow");
		assertNotNull("Cannot find resource file for test", input);
		File inputFile = File.createTempFile("testConverterNormal_", ".workflow");
		assertTrue("inputFile " + inputFile.getAbsolutePath() + " does not exist", inputFile.exists());
		copyFile(input, inputFile);
		
		Converter.activateJUnitMode();
		Converter.updateModel(org.eclipse.emf.common.util.URI.createFileURI(inputFile.getAbsolutePath()), "1.6.0", false);
		FileInputStream fis = new FileInputStream(inputFile);
		assertTrue("Export result workflow file seems too small to be good", fis.available() > 300);
	}
	
	public void testBug296235() throws Exception {
		InputStream input = getClass().getResourceAsStream("Itil06.workflow");
		assertNotNull("Cannot find resource file for test", input);
		File inputFile = File.createTempFile("testBug296235_", ".workflow");
		assertTrue("inputFile " + inputFile.getAbsolutePath() + " does not exist", inputFile.exists());
		copyFile(input, inputFile);
		
		Converter.activateJUnitMode();
		Converter.updateModel(org.eclipse.emf.common.util.URI.createFileURI(inputFile.getAbsolutePath()), "1.2.0", false);
		FileInputStream fis = new FileInputStream(inputFile);
		assertTrue("Export result workflow file seems too small to be good", fis.available() > 300);
	}

	
	
	public static void copyFile(InputStream in, File out) throws Exception {
	    FileOutputStream fos = new FileOutputStream(out);
	    try {
	        byte[] buf = new byte[1024];
	        int i = 0;
	        while ((i = in.read(buf)) != -1) {
	            fos.write(buf, 0, i);
	        }
	    } 
	    catch (Exception e) {
	        throw e;
	    }
	    finally {
	        if (in != null) in.close();
	        if (fos != null) fos.close();
	    }
	    fos.close();
	  }
}
