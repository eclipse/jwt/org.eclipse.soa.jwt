<?xml version="1.0" encoding="UTF-8"?>
<core:Model xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:application="org.eclipse.jwt/application" xmlns:core="org.eclipse.jwt/core" xmlns:data="org.eclipse.jwt/data" xmlns:organisations="org.eclipse.jwt/organisations" xmlns:processes="org.eclipse.jwt/processes" name="ITIL" author="F. Lautenbacher, University of Augsburg" version="1.6.0" description="Two short processes that show the usage of ITIL processes in AgilPro LiMo.&#xD;&#xA;These processes cannot be simulated, but show only the usage of AgilPro to model ITIL processes." fileversion="0.6.0">
  <subpackages name="Rollen">
    <elements xsi:type="organisations:Role" name="Anwender" icon="pawn_glass_red.png"/>
    <elements xsi:type="organisations:Role" name="2ndL Support" icon="pawn_glass_yellow.png"/>
    <elements xsi:type="organisations:Role" name="Service-Desk" icon="pawn_glass_blue.png"/>
    <elements xsi:type="organisations:Role" name="Problem Manager" icon="pawn_glass_blue.png"/>
    <elements xsi:type="organisations:Role" name="Change Manager" icon="pawn_glass_blue.png"/>
    <elements xsi:type="organisations:Role" name="Release Manager" icon="pawn_glass_blue.png"/>
    <elements xsi:type="organisations:Role" name="Configuration Manager" icon="pawn_glass_blue.png"/>
  </subpackages>
  <subpackages name="Anwendungen">
    <subpackages name="Utils">
      <elements xsi:type="application:Application" name="Microsoft Word" icon="notebook.png" javaClass="eu.emundo.agilpro.fw.fe.intf.WordUi"/>
      <elements xsi:type="application:Application" name="Microsoft Excel" icon="chart.png" javaClass="eu.emundo.agilpro.fw.fe.intf.ExcelUi"/>
      <elements xsi:type="application:Application" name="OpenOffice Writer" icon="notebook.png" javaClass="eu.emundo.agilpro.fw.fe.intf.WriterUi"/>
      <elements xsi:type="application:Application" name="OpenOffice Calc" icon="line-chart.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.CalcUi"/>
      <elements xsi:type="application:Application" name="Adobe Acrobat" icon="contract.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.AcrobatUi"/>
      <elements xsi:type="application:Application" name="Web Browser" icon="window_earth.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.BrowserUi"/>
      <elements xsi:type="application:Application" name="Google Suche" icon="earth_find.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.be.intf.GoogleWs"/>
      <elements xsi:type="application:Application" name="Meta Suche" icon="earth_view.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.MetaSearchUi"/>
      <elements xsi:type="application:Application" name="Generische Gui" icon="window_gear.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.GenericUi"/>
    </subpackages>
    <elements xsi:type="application:Application" name="Incident System" icon="window_gear.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.BrowserUi"/>
    <elements xsi:type="application:Application" name="Problem Reporting" icon="window_gear.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.BrowserUi"/>
    <elements xsi:type="application:Application" name="Change Scheduler" icon="window_gear.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.BrowserUi"/>
    <elements xsi:type="application:Application" name="Version Control System" icon="window_gear.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.BrowserUi"/>
  </subpackages>
  <subpackages name="Daten">
    <subpackages name="Datentypen">
      <elements xsi:type="data:DataType" name="eu.emundo.agilpro.fw.fe.dto.FileDTO"/>
      <elements xsi:type="data:DataType" name="eu.emundo.agilpro.fw.be.dto.GoogleDTO"/>
      <elements xsi:type="data:DataType" name="eu.emundo.agilpro.fw.be.dto.MetaSearchDTO"/>
      <elements xsi:type="data:DataType" name="eu.emundo.agilpro.fw.fe.dto.BrowserDTO"/>
      <elements xsi:type="data:DataType" name="eu.emundo.agilpro.fw.fe.dio.GenericDIO"/>
    </subpackages>
    <subpackages name="WWW Services">
      <elements xsi:type="data:DataType" name="searchquery"/>
      <elements xsi:type="data:DataType" name="qualifier"/>
      <elements xsi:type="data:DataType" name="URL"/>
      <elements xsi:type="data:Data" name="Siemens Portal" icon="help_earth.png" value="http://www.click2procure.de" dataType="//@subpackages.2/@subpackages.1/@elements.2"/>
      <elements xsi:type="data:Data" name="Zugverbindung" icon="help_earth.png" value="http://reiseauskunft.bahn.de/bin/query.exe/d?S=$start&amp;Z=$ziel&amp;REQ0JourneyDate=$datum&amp;start=1" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Google Parameter" icon="help_earth.png" value="System München" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Wikipedia" icon="help_earth.png" value="http://de.wikipedia.org/wiki/$" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Wörterbuch" icon="help_earth.png" value="http://dict.leo.org/ende?lp=ende&amp;search=$" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Wetter" icon="help_earth.png" value="http://www.viamichelin.com/viamichelin/deu/dyn/controller/localeWeather?strLocation=$ziel&amp;strCountry=eur&amp;google=1" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Flugverbindung" icon="help_earth.png" value="http://www.expedia.de/pub/agent.dll?qscr=fexp&amp;flag=q&amp;city1=$start&amp;citd1=$ziel&amp;date1=$datum&amp;date2=$datum" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Landkarte" icon="help_earth.png" value="http://www.viamichelin.com/viamichelin/deu/dyn/controller/mapPerformPage?strLocation=$ziel&amp;strCountry=eur" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Route" icon="help_earth.png" value="http://www.viamichelin.com/viamichelin/deu/dyn/controller/ItiWGPerformPage?strStartCity=$start&amp;strDestCity=$ziel" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Hotels" icon="help_earth.png" value="http://www.viamichelin.com/viamichelin/deu/dyn/controller/hbListPerformPage?strLocation=$ziel&amp;strCountry=eur" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Restaurants" icon="help_earth.png" value="http://www.viamichelin.com/viamichelin/deu/dyn/controller/poiListPerformPage?productId=41102&amp;strLocation=$ziel&amp;strCountry=eur" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Google News" icon="help_earth.png" value="http://news.google.de/news?hl=de&amp;ned=de&amp;q=$firma&amp;btnG=News-Suche" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Firmen Homepage" icon="help_earth.png" value="http://www.google.de/search?q=$firma&amp;btnI=1" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Sehenswürdigkeiten" icon="help_earth.png" value="http://www.viamichelin.com/viamichelin/deu/dyn/controller/poiListPerformPage?productId=41104&amp;strLocation=$ziel" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="GBI Firmenkontor" icon="help_earth.png" value="http://www.gbi.de/r_profisuche/firmenkontor.ein?ST0_CO=$firma&amp;START=A00&amp;DBN=CRXHRBA" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="GBI Pressekontor" icon="help_earth.png" value="http://www.gbi.de/r_profisuche/pressekontor.ein?ST0_=$firma&amp;DBN=CRX076&amp;STARTA00=Suche" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Fonds-1" icon="help_earth.png" value="" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Fonds-2" icon="help_earth.png" value="" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Fonds-3" icon="help_earth.png" value="" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
      <elements xsi:type="data:Data" name="Google Map" icon="help_earth.png" value="http://maps.google.de/maps?f=l&amp;hl=de&amp;q=$firma&amp;near=$ort&amp;ie=UTF8&amp;z=12&amp;om=1&amp;iwloc=A" dataType="//@subpackages.2/@subpackages.1/@elements.1"/>
    </subpackages>
    <subpackages name="Eingabeparameter">
      <elements xsi:type="data:DataType" name="dioParameter"/>
      <elements xsi:type="data:Data" name="Ziel" icon="component.png" dataType="//@subpackages.2/@subpackages.2/@elements.0"/>
      <elements xsi:type="data:Data" name="Start" icon="component.png" dataType="//@subpackages.2/@subpackages.2/@elements.0"/>
      <elements xsi:type="data:Data" name="Strasse" icon="component.png" value="strasse" dataType="//@subpackages.2/@subpackages.2/@elements.0"/>
      <elements xsi:type="data:Data" name="Firma" icon="component.png" value="" dataType="//@subpackages.2/@subpackages.2/@elements.0"/>
      <elements xsi:type="data:Data" name="Branche" icon="component.png" value="'Handel' | 'Industrie' | 'Dienstleistung'" dataType="//@subpackages.2/@subpackages.2/@elements.0"/>
      <elements xsi:type="data:Data" name="Datum" icon="component.png" value="" dataType="//@subpackages.2/@subpackages.2/@elements.0"/>
      <elements xsi:type="data:Data" name="Reisemittel" icon="component.png" value="'Bahn' | 'Flugzeug'" dataType="//@subpackages.2/@subpackages.2/@elements.0"/>
      <elements xsi:type="data:Data" name="Ort" icon="component.png" value="" dataType="//@subpackages.2/@subpackages.2/@elements.0"/>
    </subpackages>
    <subpackages name="Utils">
      <elements xsi:type="data:Data" name="Browser Daten" icon="window_environment.png" dataType="//@subpackages.2/@subpackages.0/@elements.3"/>
      <elements xsi:type="data:Data" name="Google Daten" icon="environment.png" dataType="//@subpackages.2/@subpackages.0/@elements.1"/>
      <elements xsi:type="data:Data" name="Meta Suche Daten" icon="environment_view.png" value="" dataType="//@subpackages.2/@subpackages.0/@elements.2"/>
      <elements xsi:type="data:Data" name="Generische Daten" icon="gear.png" value="" dataType="//@subpackages.2/@subpackages.0/@elements.4"/>
      <elements xsi:type="data:Data" name="Word/Writer Datei" icon="document_notebook.png" dataType="//@subpackages.2/@subpackages.0/@elements.0"/>
      <elements xsi:type="data:Data" name="Excel/Calc Datei" icon="document_chart.png" dataType="//@subpackages.2/@subpackages.0/@elements.0"/>
      <elements xsi:type="data:Data" name="PDF Datei" icon="document_text.png" dataType="//@subpackages.2/@subpackages.0/@elements.0"/>
    </subpackages>
    <elements xsi:type="data:Data" name="CMDB" icon="gear.png"/>
    <elements xsi:type="data:Data" name="I-Ticket" icon="document_text.png" value="http://demo.otrs.org/otrs/index.pl" dataType="//@subpackages.2/@subpackages.1/@elements.2"/>
  </subpackages>
  <subpackages name="Prozesse">
    <elements xsi:type="processes:Activity" name="Incident Management">
      <nodes xsi:type="processes:InitialNode" out="//@subpackages.3/@elements.0/@edges.0">
        <Location x="-6" y="188"/>
      </nodes>
      <nodes xsi:type="processes:FinalNode" in="//@subpackages.3/@elements.0/@edges.5">
        <Location x="835" y="282"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Störung melden" in="//@subpackages.3/@elements.0/@edges.0" out="//@subpackages.3/@elements.0/@edges.1" performedBy="//@subpackages.0/@elements.0" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.0">
        <Location x="28" y="185"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Störung erfassen" in="//@subpackages.3/@elements.0/@edges.1" out="//@subpackages.3/@elements.0/@edges.2" performedBy="//@subpackages.0/@elements.2" executedBy="//@subpackages.1/@elements.0" outputs="//@subpackages.2/@elements.1" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.1 //@subpackages.3/@elements.0/@referenceEdges.2 //@subpackages.3/@elements.0/@referenceEdges.3">
        <Location x="104" y="262"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Erstbewerten und &#xD;&#xA;klassifizieren" in="//@subpackages.3/@elements.0/@edges.2" out="//@subpackages.3/@elements.0/@edges.3" performedBy="//@subpackages.0/@elements.2" executedBy="//@subpackages.1/@elements.0" inputs="//@subpackages.2/@elements.1" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.4 //@subpackages.3/@elements.0/@referenceEdges.5 //@subpackages.3/@elements.0/@referenceEdges.6">
        <Location x="187" y="327"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Beheben und Service &#xD;&#xA;wiederherstellen" in="//@subpackages.3/@elements.0/@edges.7" out="//@subpackages.3/@elements.0/@edges.4" performedBy="//@subpackages.0/@elements.2" executedBy="//@subpackages.1/@elements.0" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.7 //@subpackages.3/@elements.0/@referenceEdges.8">
        <Location x="534" y="268"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Störung schliessen" in="//@subpackages.3/@elements.0/@edges.4" out="//@subpackages.3/@elements.0/@edges.5" performedBy="//@subpackages.0/@elements.2" executedBy="//@subpackages.1/@elements.0" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.9 //@subpackages.3/@elements.0/@referenceEdges.10">
        <Location x="687" y="279"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Umgehen mit 2nd &#xD;&#xA;Level Support" in="//@subpackages.3/@elements.0/@edges.6" out="//@subpackages.3/@elements.0/@edges.10" performedBy="//@subpackages.0/@elements.1" executedBy="//@subpackages.1/@elements.1" inputs="//@subpackages.2/@elements.1" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.11 //@subpackages.3/@elements.0/@referenceEdges.12 //@subpackages.3/@elements.0/@referenceEdges.13">
        <Location x="431" y="378"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Meldung an Probem &#xD;&#xA;Management" in="//@subpackages.3/@elements.0/@edges.8" out="//@subpackages.3/@elements.0/@edges.9" performedBy="//@subpackages.0/@elements.1" executedBy="//@subpackages.1/@elements.1" inputs="//@subpackages.2/@elements.1" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.14 //@subpackages.3/@elements.0/@referenceEdges.15 //@subpackages.3/@elements.0/@referenceEdges.16">
        <Location x="416" y="511"/>
      </nodes>
      <nodes xsi:type="processes:DecisionNode" in="//@subpackages.3/@elements.0/@edges.3" out="//@subpackages.3/@elements.0/@edges.6 //@subpackages.3/@elements.0/@edges.8 //@subpackages.3/@elements.0/@edges.11">
        <Location x="326" y="338"/>
      </nodes>
      <nodes xsi:type="processes:MergeNode" in="//@subpackages.3/@elements.0/@edges.9 //@subpackages.3/@elements.0/@edges.10 //@subpackages.3/@elements.0/@edges.11" out="//@subpackages.3/@elements.0/@edges.7">
        <Location x="598" y="342"/>
      </nodes>
      <edges source="//@subpackages.3/@elements.0/@nodes.0" target="//@subpackages.3/@elements.0/@nodes.2"/>
      <edges source="//@subpackages.3/@elements.0/@nodes.2" target="//@subpackages.3/@elements.0/@nodes.3"/>
      <edges source="//@subpackages.3/@elements.0/@nodes.3" target="//@subpackages.3/@elements.0/@nodes.4"/>
      <edges source="//@subpackages.3/@elements.0/@nodes.4" target="//@subpackages.3/@elements.0/@nodes.9"/>
      <edges source="//@subpackages.3/@elements.0/@nodes.5" target="//@subpackages.3/@elements.0/@nodes.6"/>
      <edges source="//@subpackages.3/@elements.0/@nodes.6" target="//@subpackages.3/@elements.0/@nodes.1"/>
      <edges source="//@subpackages.3/@elements.0/@nodes.9" target="//@subpackages.3/@elements.0/@nodes.7">
        <guard name="need_help" textualdescription="" shortdescription="I-Ticket.help == false">
          <detailedSpecification data="//@subpackages.2/@elements.1" attribute="help" value="false"/>
        </guard>
      </edges>
      <edges source="//@subpackages.3/@elements.0/@nodes.10" target="//@subpackages.3/@elements.0/@nodes.5"/>
      <edges source="//@subpackages.3/@elements.0/@nodes.9" target="//@subpackages.3/@elements.0/@nodes.8">
        <guard name="uno_problem" textualdescription="" shortdescription="I-Ticket.unknown == false">
          <detailedSpecification data="//@subpackages.2/@elements.1" attribute="unknown" value="false"/>
        </guard>
      </edges>
      <edges source="//@subpackages.3/@elements.0/@nodes.8" target="//@subpackages.3/@elements.0/@nodes.10"/>
      <edges source="//@subpackages.3/@elements.0/@nodes.7" target="//@subpackages.3/@elements.0/@nodes.10"/>
      <edges source="//@subpackages.3/@elements.0/@nodes.9" target="//@subpackages.3/@elements.0/@nodes.10"/>
      <references reference="//@subpackages.0/@elements.2" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.2 //@subpackages.3/@elements.0/@referenceEdges.5 //@subpackages.3/@elements.0/@referenceEdges.8 //@subpackages.3/@elements.0/@referenceEdges.10">
        <Location x="341" y="80"/>
      </references>
      <references reference="//@subpackages.0/@elements.0" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.0">
        <Location x="61" y="97"/>
      </references>
      <references reference="//@subpackages.1/@elements.0" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.1 //@subpackages.3/@elements.0/@referenceEdges.4 //@subpackages.3/@elements.0/@referenceEdges.7 //@subpackages.3/@elements.0/@referenceEdges.9">
        <Location x="323" y="207"/>
      </references>
      <references reference="//@subpackages.0/@elements.1" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.12 //@subpackages.3/@elements.0/@referenceEdges.15">
        <Location x="326" y="579"/>
      </references>
      <references reference="//@subpackages.1/@elements.1" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.11 //@subpackages.3/@elements.0/@referenceEdges.14">
        <Location x="542" y="581"/>
      </references>
      <references reference="//@subpackages.2/@elements.1" referenceEdges="//@subpackages.3/@elements.0/@referenceEdges.3 //@subpackages.3/@elements.0/@referenceEdges.6 //@subpackages.3/@elements.0/@referenceEdges.13 //@subpackages.3/@elements.0/@referenceEdges.16">
        <Location x="119" y="374"/>
      </references>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.1" action="//@subpackages.3/@elements.0/@nodes.2"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.2" action="//@subpackages.3/@elements.0/@nodes.3"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.0" action="//@subpackages.3/@elements.0/@nodes.3"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.5" action="//@subpackages.3/@elements.0/@nodes.3" direction="out"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.2" action="//@subpackages.3/@elements.0/@nodes.4"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.0" action="//@subpackages.3/@elements.0/@nodes.4"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.5" action="//@subpackages.3/@elements.0/@nodes.4" direction="in"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.2" action="//@subpackages.3/@elements.0/@nodes.5"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.0" action="//@subpackages.3/@elements.0/@nodes.5"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.2" action="//@subpackages.3/@elements.0/@nodes.6"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.0" action="//@subpackages.3/@elements.0/@nodes.6"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.4" action="//@subpackages.3/@elements.0/@nodes.7"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.3" action="//@subpackages.3/@elements.0/@nodes.7"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.5" action="//@subpackages.3/@elements.0/@nodes.7" direction="in"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.4" action="//@subpackages.3/@elements.0/@nodes.8"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.3" action="//@subpackages.3/@elements.0/@nodes.8"/>
      <referenceEdges reference="//@subpackages.3/@elements.0/@references.5" action="//@subpackages.3/@elements.0/@nodes.8" direction="in"/>
    </elements>
    <elements xsi:type="processes:Activity" name="Service Support Overview" icon="organisation.gif">
      <nodes xsi:type="processes:Action" name="Incident &#xD;&#xA;Management" in="//@subpackages.3/@elements.1/@edges.6" out="//@subpackages.3/@elements.1/@edges.7" performedBy="//@subpackages.0/@elements.2" executedBy="//@subpackages.1/@elements.0" inputs="//@subpackages.2/@elements.0" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.0 //@subpackages.3/@elements.1/@referenceEdges.1 //@subpackages.3/@elements.1/@referenceEdges.2">
        <Location x="102" y="203"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Problem&#xD;&#xA;Management" in="//@subpackages.3/@elements.1/@edges.7" out="//@subpackages.3/@elements.1/@edges.8" performedBy="//@subpackages.0/@elements.3" executedBy="//@subpackages.1/@elements.1" inputs="//@subpackages.2/@elements.0" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.3 //@subpackages.3/@elements.1/@referenceEdges.4 //@subpackages.3/@elements.1/@referenceEdges.5">
        <Location x="244" y="237"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Change&#xD;&#xA;Management" in="//@subpackages.3/@elements.1/@edges.8" out="//@subpackages.3/@elements.1/@edges.9" performedBy="//@subpackages.0/@elements.4" executedBy="//@subpackages.1/@elements.2" inputs="//@subpackages.2/@elements.0" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.6 //@subpackages.3/@elements.1/@referenceEdges.7 //@subpackages.3/@elements.1/@referenceEdges.8">
        <Location x="388" y="310"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Release&#xD;&#xA;Management" in="//@subpackages.3/@elements.1/@edges.9" out="//@subpackages.3/@elements.1/@edges.10" performedBy="//@subpackages.0/@elements.5" executedBy="//@subpackages.1/@elements.3" inputs="//@subpackages.2/@elements.0" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.9 //@subpackages.3/@elements.1/@referenceEdges.10 //@subpackages.3/@elements.1/@referenceEdges.11">
        <Location x="514" y="348"/>
      </nodes>
      <nodes xsi:type="processes:Action" name="Configuration&#xD;&#xA;Management" in="//@subpackages.3/@elements.1/@edges.10" out="//@subpackages.3/@elements.1/@edges.11" performedBy="//@subpackages.0/@elements.6" executedBy="//@subpackages.1/@elements.3" inputs="//@subpackages.2/@elements.0" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.12 //@subpackages.3/@elements.1/@referenceEdges.13 //@subpackages.3/@elements.1/@referenceEdges.14">
        <Location x="630" y="384"/>
      </nodes>
      <nodes xsi:type="processes:InitialNode" out="//@subpackages.3/@elements.1/@edges.6">
        <Location x="38" y="219"/>
      </nodes>
      <nodes xsi:type="processes:FinalNode" in="//@subpackages.3/@elements.1/@edges.11">
        <Location x="816" y="396"/>
      </nodes>
      <edges/>
      <edges/>
      <edges/>
      <edges/>
      <edges/>
      <edges/>
      <edges source="//@subpackages.3/@elements.1/@nodes.5" target="//@subpackages.3/@elements.1/@nodes.0"/>
      <edges source="//@subpackages.3/@elements.1/@nodes.0" target="//@subpackages.3/@elements.1/@nodes.1"/>
      <edges source="//@subpackages.3/@elements.1/@nodes.1" target="//@subpackages.3/@elements.1/@nodes.2"/>
      <edges source="//@subpackages.3/@elements.1/@nodes.2" target="//@subpackages.3/@elements.1/@nodes.3"/>
      <edges source="//@subpackages.3/@elements.1/@nodes.3" target="//@subpackages.3/@elements.1/@nodes.4"/>
      <edges source="//@subpackages.3/@elements.1/@nodes.4" target="//@subpackages.3/@elements.1/@nodes.6"/>
      <references reference="//@subpackages.0/@elements.2" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.1">
        <Location x="118" y="94"/>
      </references>
      <references reference="//@subpackages.0/@elements.3" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.4">
        <Location x="268" y="154"/>
      </references>
      <references reference="//@subpackages.0/@elements.4" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.7">
        <Location x="401" y="221"/>
      </references>
      <references reference="//@subpackages.0/@elements.5" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.10">
        <Location x="529" y="269"/>
      </references>
      <references reference="//@subpackages.0/@elements.6" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.13">
        <Location x="644" y="296"/>
      </references>
      <references reference="//@subpackages.2/@elements.0" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.2 //@subpackages.3/@elements.1/@referenceEdges.5 //@subpackages.3/@elements.1/@referenceEdges.8 //@subpackages.3/@elements.1/@referenceEdges.11 //@subpackages.3/@elements.1/@referenceEdges.14">
        <Location x="325" y="538"/>
      </references>
      <references reference="//@subpackages.1/@elements.3" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.9 //@subpackages.3/@elements.1/@referenceEdges.12">
        <Location x="558" y="487"/>
      </references>
      <references reference="//@subpackages.1/@elements.2" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.6">
        <Location x="376" y="403"/>
      </references>
      <references reference="//@subpackages.1/@elements.1" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.3">
        <Location x="221" y="356"/>
      </references>
      <references reference="//@subpackages.1/@elements.0" referenceEdges="//@subpackages.3/@elements.1/@referenceEdges.0">
        <Location x="53" y="334"/>
      </references>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.9" action="//@subpackages.3/@elements.1/@nodes.0"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.0" action="//@subpackages.3/@elements.1/@nodes.0"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.5" action="//@subpackages.3/@elements.1/@nodes.0" direction="in"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.8" action="//@subpackages.3/@elements.1/@nodes.1"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.1" action="//@subpackages.3/@elements.1/@nodes.1"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.5" action="//@subpackages.3/@elements.1/@nodes.1" direction="in"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.7" action="//@subpackages.3/@elements.1/@nodes.2"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.2" action="//@subpackages.3/@elements.1/@nodes.2"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.5" action="//@subpackages.3/@elements.1/@nodes.2" direction="in"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.6" action="//@subpackages.3/@elements.1/@nodes.3"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.3" action="//@subpackages.3/@elements.1/@nodes.3"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.5" action="//@subpackages.3/@elements.1/@nodes.3" direction="in"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.6" action="//@subpackages.3/@elements.1/@nodes.4"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.4" action="//@subpackages.3/@elements.1/@nodes.4"/>
      <referenceEdges reference="//@subpackages.3/@elements.1/@references.5" action="//@subpackages.3/@elements.1/@nodes.4" direction="in"/>
    </elements>
  </subpackages>
</core:Model>
