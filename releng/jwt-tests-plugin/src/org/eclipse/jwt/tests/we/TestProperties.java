package org.eclipse.jwt.tests.we;

import java.util.MissingResourceException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.eclipse.jwt.we.PluginProperties;

/**
 * @since 0.7
 */
public class TestProperties extends TestCase {
	
	/**
	 * Returns the JUnit test suite that implements the <b>TestProperties</b>
	 * definition.
	 */
	public static Test suite() {
		TestSuite testVersion = new TestSuite(TestProperties.class);
		return testVersion;
	}
	
	public void testSimpleProperty() {
		assertEquals("PluginProperties does not resolve properties", "org.eclipse.jwt.we.menu", PluginProperties.extension_point_menu);
	}
	
	public void testDereferenceProperty() {
		String res = PluginProperties.getString("convert_rootpackage_ok");
		assertNotNull("PluginProperties.getString returns null", res);
		assertTrue("PluginProperties.getString return empty strings", res.length() > 0);
		assertFalse("PluginProperties does not resolve properties", res.contains("missing"));
	}
	
	public void testMissingProperty() {
		try {
			PluginProperties.getStringExpectMissing("kikoolol");
			fail("Expected MissingResourceException");
		} catch (MissingResourceException ex) {
		}
	}
}
