package org.eclipse.jwt.tests.we;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @since 0.7
 */
public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for org.eclipse.jwt.we.tests");
		//$JUnit-BEGIN$
		suite.addTest(TestVersion.suite());
		suite.addTest(TestProperties.suite());
		//$JUnit-END$
		return suite;
	}

}
