package org.eclipse.jwt.tests.we;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.eclipse.jwt.we.misc.util.GeneralHelper;

/**
 * Generated code for the test suite <b>TestVersion</b> located at
 * <i>/jwt-we-test/src/TestVersion.testsuite</i>.
 * @since 0.7
 */
public class TestVersion extends TestCase {
	/**
	 * Constructor for TestVersion.
	 * @param name
	 */
	public TestVersion(String name) {
		super(name);
	}

	/**
	 * Returns the JUnit test suite that implements the <b>TestVersion</b>
	 * definition.
	 */
	public static Test suite() {
		TestSuite testVersion = new TestSuite(TestVersion.class);
		return testVersion;
	}

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
	}

	public void testBug259476() {
		String[] version = GeneralHelper.getWEVersion().split("\\.");
		assertEquals(3, version.length);
		assertFalse(version[2].contains("qualifier") ||
				version[2].contains("I") ||
				version[2].contains("S") ||
				version[2].contains("R"));
	}
}
