/**
 * File:    TestXPDLTransformation.java
 * Created: 14.01.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009    Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.tests.transformation.jpdl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import javax.xml.transform.TransformerFactory;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.eclipse.jwt.transformation.jpdl.JwtToJpdlTransformation;

public class TestJPDLTransformation extends TestCase {
	/**
	 * Constructor for TestVersion.
	 * @param name
	 */
	public TestJPDLTransformation(String name) {
		super(name);
	}

	/**
	 * Returns the JUnit test suite that implements the <b>TestVersion</b>
	 * definition.
	 */
	public static Test suite() {
		TestSuite testVersion = new TestSuite(TestJPDLTransformation.class);
		return testVersion;
	}
	
	public void testHasXSLTransformer() throws Exception {
		TransformerFactory.newInstance();
	}
	
	public void testBug259390() throws Exception {
		JwtToJpdlTransformation jwt2xpdlTransfo = new JwtToJpdlTransformation();
		InputStream input = getClass().getResourceAsStream("ExternalApplications.workflow");
		assertNotNull("Cannot find resource file for test", input);
		File inputFile = File.createTempFile("testBug259290_", ".workflow");
		assertTrue("inputFile " + inputFile.getAbsolutePath() + " does not exist", inputFile.exists());
		copyFile(input, inputFile);
		File outputFile = File.createTempFile("testBug259290_", ".jpdl");
		assertTrue("outputFile " + inputFile.getAbsolutePath() + " does not exist", outputFile.exists());
		jwt2xpdlTransfo.transform(inputFile.getAbsolutePath(), outputFile.getAbsolutePath());
		FileInputStream fis = new FileInputStream(outputFile);
		assertTrue("jPDL file seems too small to be good", fis.available() > 1000);
	}
	
	public static void copyFile(InputStream in, File out) throws Exception {
	    FileOutputStream fos = new FileOutputStream(out);
	    try {
	        byte[] buf = new byte[1024];
	        int i = 0;
	        while ((i = in.read(buf)) != -1) {
	            fos.write(buf, 0, i);
	        }
	    } 
	    catch (Exception e) {
	        throw e;
	    }
	    finally {
	        if (in != null) in.close();
	        if (fos != null) fos.close();
	    }
	  }
}
