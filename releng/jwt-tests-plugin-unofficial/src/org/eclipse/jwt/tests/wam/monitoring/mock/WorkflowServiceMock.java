/**
 * File:    WorkflowMonitoringMock.java
 * Created: 07.05.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.tests.wam.monitoring.mock;

import org.eclipse.jwt.wam.api.WorkflowService;
import org.eclipse.jwt.wam.api.model.Activity;
import org.eclipse.jwt.wam.api.model.Process;
import org.eclipse.jwt.wam.api.model.Processes;
import org.eclipse.jwt.wam.api.model.Project;
import org.eclipse.jwt.wam.api.model.Projects;
import org.eclipse.jwt.wam.api.query.ProcessQuery;

public class WorkflowServiceMock implements WorkflowService {

	public Activity getActivityProperties(Process process, String login) {
		// TODO Auto-generated method stub
		return null;
	}

	public Project getProjectProperties(Project project, String login) {
		// TODO Auto-generated method stub
		return null;
	}

	public Process instanciateProject(Process process, String login) {
		// TODO Auto-generated method stub
		return null;
	}

	public Projects listProjects(boolean returnProperties,
			boolean startableOnly, String login) {
		// TODO Auto-generated method stub
		return null;
	}

	public Processes listUserActivities(boolean returnProperties,
			boolean returnTerminated, String login) {
		// TODO Auto-generated method stub
		return null;
	}

	public Activity setActivityProperties(Process process, String login) {
		// TODO Auto-generated method stub
		return null;
	}

	public Process startActivity(Process process, String login) {
		// TODO Auto-generated method stub
		return null;
	}

	public Process terminateActivity(Process process, String login) {
		// TODO Auto-generated method stub
		return null;
	}

	public Activity getActionProperties(Process process, String login)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Process instanciateActivity(Process process, String login)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Activity setActionProperties(Process process, String login)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Process startAction(Process process, String login)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Process terminateAction(Process process, String login)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Processes listUserActivities(ProcessQuery processQuery, String login) {
		// TODO Auto-generated method stub
		return null;
	}

	public Process deleteActivity(Process process, boolean deleteFromHistory,
			String login) {
		// TODO Auto-generated method stub
		return null;
	}

}
