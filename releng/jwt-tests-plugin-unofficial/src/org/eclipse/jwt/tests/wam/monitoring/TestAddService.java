/**
 * File:    TestAddService.java
 * Created: 07.05.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickaël Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.tests.wam.monitoring;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.eclipse.jwt.tests.wam.monitoring.mock.WorkflowServiceMock;
import org.eclipse.jwt.wam.monitoring.views.WorkflowServicesView;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

/**
 * Generated code for the test suite <b>TestAddService</b> located at
 * <i>/jwt-monitoring-test/src/org/eclipse/jwt/wam/monitoring/test/TestAddService.testsuite</i>.
 */
public class TestAddService extends TestCase {
	/**
	 * Constructor for TestAddService.
	 * @param name
	 */
	public TestAddService(String name) {
		super(name);
	}

	/**
	 * Returns the JUnit test suite that implements the <b>TestAddService</b>
	 * definition.
	 */
	public static Test suite() {
		TestSuite testAddService = new TestSuite(TestAddService.class);
		return testAddService;
	}

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
	}

	private IWorkbenchPage getPage() {
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		return window.getActivePage();
	}
	
	public void testShowHideView() throws PartInitException {
		IViewPart viewer = getPage().showView("org.eclipse.jwt.wam.monitoring.view");
		getPage().hideView(viewer);
	}
	
	public void testAddService() throws PartInitException {
		WorkflowServicesView viewer = (WorkflowServicesView)getPage().showView("org.eclipse.jwt.wam.monitoring.view");
		viewer.addService("test", new WorkflowServiceMock(), null);
		getPage().hideView(viewer);
	}
	
	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
	}

}
