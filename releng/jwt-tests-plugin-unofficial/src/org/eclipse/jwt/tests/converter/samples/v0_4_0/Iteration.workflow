<?xml version="1.0" encoding="UTF-8"?>
<core:Model xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:core="org.eclipse.jwt/core" xmlns:data="org.eclipse.jwt/data" xmlns:organisations="org.eclipse.jwt/organisations" xmlns:processes="org.eclipse.jwt/processes" name="Iteration" author="Guillaume Decarnin" version="1.0.0" fileversion="0.4.0">
  <elements xsi:type="processes:Activity" name="IterationDiagram">
    <nodes xsi:type="processes:InitialNode" out="//@elements.0/@edges.0">
      <Location x="256" y="28"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="A1" in="//@elements.0/@edges.4" out="//@elements.0/@edges.1" performedBy="//@elements.3" outputs="//@elements.2" referenceEdges="//@elements.0/@referenceEdges.1 //@elements.0/@referenceEdges.2">
      <Location x="163" y="240"/>
      <Size width="52" height="31"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="A2" in="//@elements.0/@edges.3" out="//@elements.0/@edges.5" performedBy="//@elements.3" referenceEdges="//@elements.0/@referenceEdges.0">
      <Location x="336" y="240"/>
      <Size width="56" height="17"/>
    </nodes>
    <nodes xsi:type="processes:FinalNode" in="//@elements.0/@edges.2">
      <Location x="256" y="448"/>
    </nodes>
    <nodes xsi:type="processes:DecisionNode" in="//@elements.0/@edges.1" out="//@elements.0/@edges.2 //@elements.0/@edges.3">
      <Location x="256" y="322"/>
    </nodes>
    <nodes xsi:type="processes:MergeNode" in="//@elements.0/@edges.0 //@elements.0/@edges.5" out="//@elements.0/@edges.4">
      <Location x="256" y="107"/>
    </nodes>
    <edges source="//@elements.0/@nodes.0" target="//@elements.0/@nodes.5"/>
    <edges source="//@elements.0/@nodes.1" target="//@elements.0/@nodes.4"/>
    <edges source="//@elements.0/@nodes.4" target="//@elements.0/@nodes.3">
      <guard shortdescription="choice != 1">
        <detailedSpecification attribute="choice" operation="!=" value="1"/>
      </guard>
    </edges>
    <edges source="//@elements.0/@nodes.4" target="//@elements.0/@nodes.2">
      <guard shortdescription="choice == 1">
        <detailedSpecification attribute="choice" value="1"/>
      </guard>
    </edges>
    <edges source="//@elements.0/@nodes.5" target="//@elements.0/@nodes.1"/>
    <edges source="//@elements.0/@nodes.2" target="//@elements.0/@nodes.5"/>
    <references reference="//@elements.2" referenceEdges="//@elements.0/@referenceEdges.2">
      <Location x="56" y="235"/>
    </references>
    <references reference="//@elements.3" referenceEdges="//@elements.0/@referenceEdges.0 //@elements.0/@referenceEdges.1">
      <Location x="173" y="98"/>
    </references>
    <referenceEdges reference="//@elements.0/@references.1" action="//@elements.0/@nodes.2"/>
    <referenceEdges reference="//@elements.0/@references.1" action="//@elements.0/@nodes.1"/>
    <referenceEdges reference="//@elements.0/@references.0" action="//@elements.0/@nodes.1" direction="out"/>
  </elements>
  <elements xsi:type="data:DataType" name="string" icon=""/>
  <elements xsi:type="data:Data" name="choice" icon="" value="" dataType="//@elements.1"/>
  <elements xsi:type="organisations:Role" name="bsoa" icon=""/>
</core:Model>
