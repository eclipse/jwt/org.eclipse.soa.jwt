<?xml version="1.0" encoding="UTF-8"?>
<core:Model xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:application="org.eclipse.jwt/application" xmlns:core="org.eclipse.jwt/core" xmlns:data="org.eclipse.jwt/data" xmlns:organisations="org.eclipse.jwt/organisations" xmlns:processes="org.eclipse.jwt/processes" name="ExternalApplicationTest" author="F. Lautenbacher, University of Augsburg" version="1.6.0" description="This simple process shows the usage of adapters in the simulator. &#xD;&#xA;This example requires that you have several files predefined in order for the AgilPro Simulator to simulate the process: there needs to be a folder /Testdocuments in your workspace and there you should put the files Test.doc, Test.xls and Test.pdf.&#xD;&#xA;The Simulator can only simulate the process if additional software is installed: either OpenOffice or MS Office for the .doc and .xls files and Adobe Acrobat Reader for the .pdf file." fileversion="0.6.0">
  <subpackages name="External applications">
    <elements xsi:type="application:Application" name="Microsoft Word" icon="notebook.png" javaClass="eu.emundo.agilpro.fw.fe.intf.WordUi"/>
    <elements xsi:type="application:Application" name="Microsoft Excel" icon="chart.png" javaClass="eu.emundo.agilpro.fw.fe.intf.ExcelUi"/>
    <elements xsi:type="application:Application" name="OpenOffice Writer" icon="notebook.png" javaClass="eu.emundo.agilpro.fw.fe.intf.WriterUi"/>
    <elements xsi:type="application:Application" name="OpenOffice Calc" icon="line-chart.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.CalcUi"/>
    <elements xsi:type="application:Application" name="PDF Viewer" icon="contract.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.AcrobatUi"/>
    <elements xsi:type="application:Application" name="Browser" icon="window_earth.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.BrowserUi"/>
    <elements xsi:type="application:Application" name="Google Search" icon="earth_find.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.be.intf.GoogleWs"/>
    <elements xsi:type="application:Application" name="Meta search" icon="earth_view.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.MetaSearchUi"/>
    <elements xsi:type="application:Application" name="Generic GUI" icon="window_gear.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.GenericUi"/>
    <elements xsi:type="application:Application" name="Mozilla Firefox" icon="window_earth.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.MozillaUi"/>
    <elements xsi:type="application:Application" name="Internet Explorer" icon="window_earth.png" jarArchive="" javaClass="eu.emundo.agilpro.fw.fe.intf.IExplorerUi" method=""/>
  </subpackages>
  <subpackages name="External data">
    <subpackages name="Data types">
      <elements xsi:type="data:DataType" name="URL"/>
      <elements xsi:type="data:DataType" name="qualifier"/>
      <elements xsi:type="data:DataType" name="filename"/>
    </subpackages>
    <elements xsi:type="data:Data" name="File" icon="document_plain.png"/>
    <elements xsi:type="data:Data" name="Browser data" icon="window_environment.png" value="http://www.yahoo.com" dataType="//@subpackages.1/@subpackages.0/@elements.0"/>
    <elements xsi:type="data:Data" name="Google data" icon="environment.png" value="http://www.google.de"/>
    <elements xsi:type="data:Data" name="Meta search data" icon="environment_view.png" value="http://www.google.com" dataType="//@subpackages.1/@subpackages.0/@elements.1"/>
    <elements xsi:type="data:Data" name="Generic data" icon="gear.png" value=""/>
    <elements xsi:type="data:Data" name="Word/Writer File" icon="document_notebook.png" value="/Testdocuments/Test.doc" dataType="//@subpackages.1/@subpackages.0/@elements.2"/>
    <elements xsi:type="data:Data" name="Excel/Calc File" icon="document_chart.png" value="/Testdocuments/Test.xls" dataType="//@subpackages.1/@subpackages.0/@elements.2"/>
    <elements xsi:type="data:Data" name="PDF File" icon="document_text.png" value="/Testdocuments/Test.pdf" dataType="//@subpackages.1/@subpackages.0/@elements.2"/>
  </subpackages>
  <subpackages name="Roles">
    <elements xsi:type="organisations:Role" name="StandardUser" icon=""/>
  </subpackages>
  <elements xsi:type="processes:Activity" name="ExternalApplicationTest">
    <nodes xsi:type="processes:Action" name="start Word" in="//@elements.0/@edges.3" out="//@elements.0/@edges.0" performedBy="//@subpackages.2/@elements.0" executedBy="//@subpackages.0/@elements.0" inputs="//@subpackages.1/@elements.5" referenceEdges="//@elements.0/@referenceEdges.0 //@elements.0/@referenceEdges.6 //@elements.0/@referenceEdges.12">
      <Location x="62" y="155"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="start Excel" in="//@elements.0/@edges.0" out="//@elements.0/@edges.1" performedBy="//@subpackages.2/@elements.0" executedBy="//@subpackages.0/@elements.1" inputs="//@subpackages.1/@elements.6" referenceEdges="//@elements.0/@referenceEdges.1 //@elements.0/@referenceEdges.7 //@elements.0/@referenceEdges.13">
      <Location x="182" y="156"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="start Acrobat" in="//@elements.0/@edges.1" out="//@elements.0/@edges.2" performedBy="//@subpackages.2/@elements.0" executedBy="//@subpackages.0/@elements.4" inputs="//@subpackages.1/@elements.7" referenceEdges="//@elements.0/@referenceEdges.2 //@elements.0/@referenceEdges.8 //@elements.0/@referenceEdges.14">
      <Location x="332" y="156"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="start Browser" in="//@elements.0/@edges.2" out="//@elements.0/@edges.5" performedBy="//@subpackages.2/@elements.0" executedBy="//@subpackages.0/@elements.5" inputs="//@subpackages.1/@elements.1" referenceEdges="//@elements.0/@referenceEdges.3 //@elements.0/@referenceEdges.9 //@elements.0/@referenceEdges.15">
      <Location x="484" y="156"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="search Meta" in="//@elements.0/@edges.5" out="//@elements.0/@edges.4" performedBy="//@subpackages.2/@elements.0" executedBy="//@subpackages.0/@elements.7" inputs="//@subpackages.1/@elements.3" referenceEdges="//@elements.0/@referenceEdges.5 //@elements.0/@referenceEdges.11 //@elements.0/@referenceEdges.17">
      <Location x="636" y="158"/>
    </nodes>
    <nodes xsi:type="processes:InitialNode" out="//@elements.0/@edges.3">
      <Location x="16" y="159"/>
    </nodes>
    <nodes xsi:type="processes:FinalNode" in="//@elements.0/@edges.4">
      <Location x="758" y="163"/>
    </nodes>
    <edges source="//@elements.0/@nodes.0" target="//@elements.0/@nodes.1"/>
    <edges source="//@elements.0/@nodes.1" target="//@elements.0/@nodes.2"/>
    <edges source="//@elements.0/@nodes.2" target="//@elements.0/@nodes.3"/>
    <edges source="//@elements.0/@nodes.5" target="//@elements.0/@nodes.0"/>
    <edges source="//@elements.0/@nodes.4" target="//@elements.0/@nodes.6"/>
    <edges source="//@elements.0/@nodes.3" target="//@elements.0/@nodes.4"/>
    <references reference="//@subpackages.1/@elements.3" referenceEdges="//@elements.0/@referenceEdges.17">
      <Location x="580" y="217"/>
    </references>
    <references reference="//@subpackages.1/@elements.1" referenceEdges="//@elements.0/@referenceEdges.15">
      <Location x="441" y="216"/>
    </references>
    <references reference="//@subpackages.1/@elements.7" referenceEdges="//@elements.0/@referenceEdges.14">
      <Location x="303" y="223"/>
    </references>
    <references reference="//@subpackages.1/@elements.6" referenceEdges="//@elements.0/@referenceEdges.13">
      <Location x="129" y="225"/>
    </references>
    <references reference="//@subpackages.1/@elements.5" referenceEdges="//@elements.0/@referenceEdges.12">
      <Location x="4" y="223"/>
    </references>
    <references reference="//@subpackages.2/@elements.0" referenceEdges="//@elements.0/@referenceEdges.6 //@elements.0/@referenceEdges.7 //@elements.0/@referenceEdges.8 //@elements.0/@referenceEdges.9 //@elements.0/@referenceEdges.11">
      <Location x="431" y="33"/>
    </references>
    <references reference="//@subpackages.0/@elements.7" referenceEdges="//@elements.0/@referenceEdges.5">
      <Location x="649" y="298"/>
    </references>
    <references reference="//@subpackages.0/@elements.5" referenceEdges="//@elements.0/@referenceEdges.3">
      <Location x="507" y="298"/>
    </references>
    <references reference="//@subpackages.0/@elements.4" referenceEdges="//@elements.0/@referenceEdges.2">
      <Location x="343" y="297"/>
    </references>
    <references reference="//@subpackages.0/@elements.1" referenceEdges="//@elements.0/@referenceEdges.1">
      <Location x="182" y="299"/>
    </references>
    <references reference="//@subpackages.0/@elements.0" referenceEdges="//@elements.0/@referenceEdges.0">
      <Location x="60" y="298"/>
    </references>
    <referenceEdges reference="//@elements.0/@references.10" action="//@elements.0/@nodes.0"/>
    <referenceEdges reference="//@elements.0/@references.9" action="//@elements.0/@nodes.1"/>
    <referenceEdges reference="//@elements.0/@references.8" action="//@elements.0/@nodes.2"/>
    <referenceEdges reference="//@elements.0/@references.7" action="//@elements.0/@nodes.3"/>
    <referenceEdges/>
    <referenceEdges reference="//@elements.0/@references.6" action="//@elements.0/@nodes.4"/>
    <referenceEdges reference="//@elements.0/@references.5" action="//@elements.0/@nodes.0"/>
    <referenceEdges reference="//@elements.0/@references.5" action="//@elements.0/@nodes.1"/>
    <referenceEdges reference="//@elements.0/@references.5" action="//@elements.0/@nodes.2"/>
    <referenceEdges reference="//@elements.0/@references.5" action="//@elements.0/@nodes.3"/>
    <referenceEdges/>
    <referenceEdges reference="//@elements.0/@references.5" action="//@elements.0/@nodes.4"/>
    <referenceEdges reference="//@elements.0/@references.4" action="//@elements.0/@nodes.0" direction="in"/>
    <referenceEdges reference="//@elements.0/@references.3" action="//@elements.0/@nodes.1" direction="in"/>
    <referenceEdges reference="//@elements.0/@references.2" action="//@elements.0/@nodes.2" direction="in"/>
    <referenceEdges reference="//@elements.0/@references.1" action="//@elements.0/@nodes.3" direction="in"/>
    <referenceEdges/>
    <referenceEdges reference="//@elements.0/@references.0" action="//@elements.0/@nodes.4" direction="in"/>
  </elements>
</core:Model>
