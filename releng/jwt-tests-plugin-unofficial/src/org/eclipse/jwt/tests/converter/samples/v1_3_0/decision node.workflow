<?xml version="1.0" encoding="UTF-8"?>
<core:Model xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:core="org.eclipse.jwt/1.3.0/core" xmlns:organisations="org.eclipse.jwt/1.3.0/organisations" xmlns:processes="org.eclipse.jwt/1.3.0/processes" name="MainPackage" fileversion="1.3.0">
  <elements xsi:type="processes:Activity" name="Diagram1">
    <nodes xsi:type="processes:DecisionNode" name="isOK?" in="//@elements.0/@edges.0" out="//@elements.0/@edges.1 //@elements.0/@edges.2">
      <Location x="266" y="130"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="Decision start" out="//@elements.0/@edges.0">
      <Location x="84" y="126"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="Ok" in="//@elements.0/@edges.1">
      <Location x="392" y="153"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="Not Ok" in="//@elements.0/@edges.2">
      <Location x="378" y="56"/>
    </nodes>
    <edges source="//@elements.0/@nodes.1" target="//@elements.0/@nodes.0"/>
    <edges source="//@elements.0/@nodes.0" target="//@elements.0/@nodes.2">
      <guard name="OK" textualdescription="" shortdescription="ok==true">
        <detailedSpecification attribute="ok" value="true"/>
      </guard>
    </edges>
    <edges source="//@elements.0/@nodes.0" target="//@elements.0/@nodes.3">
      <guard name="Not OK" shortdescription="ok==false">
        <detailedSpecification attribute="ok" value="false"/>
      </guard>
    </edges>
  </elements>
  <elements xsi:type="organisations:Role" name="role" icon=""/>
</core:Model>
