<?xml version="1.0" encoding="UTF-8"?>
<core:Model xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:core="org.eclipse.jwt/core" xmlns:processes="org.eclipse.jwt/processes" name="ForkJoin" author="Guillaume Decarnin" version="1.0.0" description="" fileversion="0.4.0">
  <elements xsi:type="processes:Activity" name="ForkJoinDiagram">
    <nodes xsi:type="processes:ForkNode" in="//@elements.0/@edges.0" out="//@elements.0/@edges.1 //@elements.0/@edges.2">
      <Location x="210" y="100"/>
      <Size width="175" height="150"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="A" in="//@elements.0/@edges.6" out="//@elements.0/@edges.0">
      <Location x="23" y="159"/>
      <Size width="124" height="31"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="B1" in="//@elements.0/@edges.1" out="//@elements.0/@edges.4">
      <Location x="283" y="52"/>
      <Size width="156" height="29"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="B2" in="//@elements.0/@edges.2" out="//@elements.0/@edges.3">
      <Location x="283" y="280"/>
      <Size width="156" height="34"/>
    </nodes>
    <nodes xsi:type="processes:JoinNode" in="//@elements.0/@edges.3 //@elements.0/@edges.4" out="//@elements.0/@edges.5">
      <Location x="490" y="100"/>
      <Size width="89" height="150"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="C" in="//@elements.0/@edges.5" out="//@elements.0/@edges.7">
      <Location x="546" y="159"/>
      <Size width="106" height="32"/>
    </nodes>
    <nodes xsi:type="processes:InitialNode" out="//@elements.0/@edges.6">
      <Location x="73" y="56"/>
    </nodes>
    <nodes xsi:type="processes:FinalNode" in="//@elements.0/@edges.7">
      <Location x="587" y="56"/>
    </nodes>
    <edges source="//@elements.0/@nodes.1" target="//@elements.0/@nodes.0"/>
    <edges source="//@elements.0/@nodes.0" target="//@elements.0/@nodes.2"/>
    <edges source="//@elements.0/@nodes.0" target="//@elements.0/@nodes.3"/>
    <edges source="//@elements.0/@nodes.3" target="//@elements.0/@nodes.4"/>
    <edges source="//@elements.0/@nodes.2" target="//@elements.0/@nodes.4"/>
    <edges source="//@elements.0/@nodes.4" target="//@elements.0/@nodes.5"/>
    <edges source="//@elements.0/@nodes.6" target="//@elements.0/@nodes.1"/>
    <edges source="//@elements.0/@nodes.5" target="//@elements.0/@nodes.7"/>
  </elements>
</core:Model>
