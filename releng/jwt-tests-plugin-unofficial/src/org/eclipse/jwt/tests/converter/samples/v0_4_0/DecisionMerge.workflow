<?xml version="1.0" encoding="UTF-8"?>
<core:Model xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:core="org.eclipse.jwt/core" xmlns:data="org.eclipse.jwt/data" xmlns:organisations="org.eclipse.jwt/organisations" xmlns:processes="org.eclipse.jwt/processes" name="DecisionMerge" author="Guillaume Decarnin" version="1.0.0" description="" fileversion="0.4.0">
  <elements xsi:type="processes:Activity" name="DecisionMergeDiagram">
    <nodes xsi:type="processes:Action" name="A" in="//@elements.0/@edges.5" out="//@elements.0/@edges.0" performedBy="//@elements.3" outputs="//@elements.2" referenceEdges="//@elements.0/@referenceEdges.1 //@elements.0/@referenceEdges.2">
      <Location x="20" y="195"/>
      <Size width="124" height="31"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="B1" in="//@elements.0/@edges.7" out="//@elements.0/@edges.3" performedBy="//@elements.4" outputs="//@elements.6" referenceEdges="//@elements.0/@referenceEdges.4 //@elements.0/@referenceEdges.6">
      <Location x="294" y="108"/>
      <Size width="156" height="29"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="B2" in="//@elements.0/@edges.1" out="//@elements.0/@edges.2" performedBy="//@elements.4" outputs="//@elements.6" referenceEdges="//@elements.0/@referenceEdges.3 //@elements.0/@referenceEdges.5">
      <Location x="294" y="283"/>
      <Size width="156" height="34"/>
    </nodes>
    <nodes xsi:type="processes:Action" name="C" in="//@elements.0/@edges.4" out="//@elements.0/@edges.6" performedBy="//@elements.4" inputs="//@elements.6" referenceEdges="//@elements.0/@referenceEdges.7 //@elements.0/@referenceEdges.8">
      <Location x="616" y="195"/>
      <Size width="106" height="32"/>
    </nodes>
    <nodes xsi:type="processes:InitialNode" name="NameInit" out="//@elements.0/@edges.5">
      <Location x="70" y="76"/>
    </nodes>
    <nodes xsi:type="processes:FinalNode" name="NameFinal" in="//@elements.0/@edges.6">
      <Location x="658" y="76"/>
    </nodes>
    <nodes xsi:type="processes:DecisionNode" in="//@elements.0/@edges.0" out="//@elements.0/@edges.1 //@elements.0/@edges.7">
      <Location x="205" y="199"/>
    </nodes>
    <nodes xsi:type="processes:MergeNode" in="//@elements.0/@edges.3 //@elements.0/@edges.2" out="//@elements.0/@edges.4">
      <Location x="449" y="199"/>
    </nodes>
    <edges source="//@elements.0/@nodes.0" target="//@elements.0/@nodes.6"/>
    <edges source="//@elements.0/@nodes.6" target="//@elements.0/@nodes.2">
      <guard shortdescription="choice != 1">
        <detailedSpecification attribute="choice" operation="!=" value="1"/>
      </guard>
    </edges>
    <edges source="//@elements.0/@nodes.2" target="//@elements.0/@nodes.7"/>
    <edges source="//@elements.0/@nodes.1" target="//@elements.0/@nodes.7"/>
    <edges source="//@elements.0/@nodes.7" target="//@elements.0/@nodes.3"/>
    <edges source="//@elements.0/@nodes.4" target="//@elements.0/@nodes.0"/>
    <edges source="//@elements.0/@nodes.3" target="//@elements.0/@nodes.5"/>
    <edges source="//@elements.0/@nodes.6" target="//@elements.0/@nodes.1">
      <guard shortdescription="choice == 1">
        <detailedSpecification attribute="choice" value="1"/>
      </guard>
    </edges>
    <references reference="//@elements.4" referenceEdges="//@elements.0/@referenceEdges.3 //@elements.0/@referenceEdges.4 //@elements.0/@referenceEdges.8">
      <Location x="488" y="406"/>
    </references>
    <references reference="//@elements.6" referenceEdges="//@elements.0/@referenceEdges.5 //@elements.0/@referenceEdges.6 //@elements.0/@referenceEdges.7">
      <Location x="504" y="67"/>
    </references>
    <references reference="//@elements.3" referenceEdges="//@elements.0/@referenceEdges.2">
      <Location x="54" y="406"/>
    </references>
    <references reference="//@elements.2" referenceEdges="//@elements.0/@referenceEdges.1">
      <Location x="196" y="67"/>
    </references>
    <referenceEdges/>
    <referenceEdges reference="//@elements.0/@references.3" action="//@elements.0/@nodes.0" direction="out"/>
    <referenceEdges reference="//@elements.0/@references.2" action="//@elements.0/@nodes.0"/>
    <referenceEdges reference="//@elements.0/@references.0" action="//@elements.0/@nodes.2"/>
    <referenceEdges reference="//@elements.0/@references.0" action="//@elements.0/@nodes.1"/>
    <referenceEdges reference="//@elements.0/@references.1" action="//@elements.0/@nodes.2" direction="out"/>
    <referenceEdges reference="//@elements.0/@references.1" action="//@elements.0/@nodes.1" direction="out"/>
    <referenceEdges reference="//@elements.0/@references.1" action="//@elements.0/@nodes.3" direction="in"/>
    <referenceEdges reference="//@elements.0/@references.0" action="//@elements.0/@nodes.3"/>
  </elements>
  <elements xsi:type="data:DataType" name="number" icon=""/>
  <elements xsi:type="data:Data" name="choice" icon="" value="" dataType="//@elements.1"/>
  <elements xsi:type="organisations:Role" name="manager" icon=""/>
  <elements xsi:type="organisations:Role" name="operator" icon=""/>
  <elements xsi:type="data:DataType" name="string" icon=""/>
  <elements xsi:type="data:Data" name="info" icon="" value="" dataType="//@elements.5"/>
</core:Model>
