/**
 * File:    TestConverter.java
 * Created: 30.05.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012    Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere, Open Wide, Lyon, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.tests.converter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jwt.converter.Converter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;

/**
 * Tests the converter against a bunch of old files, just to see if anything
 * fails (it should not).
 * 
 * @author Yoann Rodiere, Open Wide, Lyon, France
 */
@RunWith(value = Parameterized.class)
public class TestConverter {
	private static final String CURRENT_VERSION = "1.2.0";
	private static final String SAMPLE_PACKAGE = "samples";

	@Parameters
	public static Collection<Object[]> data() {
		Object[][] data = {
				// {"v0_4_0/ComplexWorkflow.workflow"},
				/*
				 * Trying to register several rules as default for element
				 * IN!WebService1: 'Application2Application' and
				 * 'WebServiceApplication2WebServiceApplication'
				 */
				{ "v0_4_0/DecisionMerge.workflow" },
				{ "v0_4_0/ForkJoin.workflow" },
				{ "v0_4_0/Iteration.workflow" },
				{ "v0_4_0/OneAction.workflow" },
				{ "v0_4_0/RoleDataApplication.workflow" },
				// { "v0_4_0/SubprocessesWithoutNames.workflow" },
				/*
				 * org.eclipse.m2m.atl.engine.vm.VMException: Could not find
				 * operation including on Module having supertypes: [OclAny] at
				 * A.__applyModel2Model(1 : NTransientLink;) :
				 * ??#114(JWT06toJWT07.atl[150:17-151:44]) local variables =
				 * {model=OUT!SubprocessesWithoutNames,
				 * input=IN!SubprocessesWithoutNames, link=TransientLink {rule =
				 * 'Model2Model', sourceElements = {input =
				 * IN!SubprocessesWithoutNames}, targetElements = {output2 =
				 * VIEW!<unnamed>, model = OUT!SubprocessesWithoutNames},
				 * variables = {}}, self=JWT06toJWT07 : ASMModule, e=IN!,
				 * output2=VIEW!<unnamed>} local stack = [VIEW!<unnamed>,
				 * VIEW!<unnamed>]
				 */
				// { "v0_4_0/Subprocesses.workflow" },
				/*
				 * org.eclipse.m2m.atl.engine.vm.VMException: Could not find
				 * operation including on Module having supertypes: [OclAny] at
				 * A.__applyModel2Model(1 : NTransientLink;) :
				 * ??#114(JWT06toJWT07.atl[150:17-151:44]) local variables =
				 * {model=OUT!Subprocesses, input=IN!Subprocesses,
				 * link=TransientLink {rule = 'Model2Model', sourceElements =
				 * {input = IN!Subprocesses}, targetElements = {output2 =
				 * VIEW!<unnamed>, model = OUT!Subprocesses}, variables = {}},
				 * self=JWT06toJWT07 : ASMModule, e=IN!Subprocess1,
				 * output2=VIEW!<unnamed>} local stack = [VIEW!<unnamed>,
				 * VIEW!<unnamed>]
				 */
				{ "v0_5_0/activity_property_edited.workflow" },
				{ "v0_5_0/activity_property.workflow" },
				{ "v0_5_0/activity_registereddynamicaspect_edited.workflow" },
				{ "v0_5_0/activity_registereddynamicaspect.workflow" },
				{ "v0_5_0/activity_staticaspect_edited.workflow" },
				{ "v0_5_0/activity_staticaspect.workflow" },
				{ "v0_6_0/activity_logging_edited.workflow" },
				{ "v0_6_0/activity_logging.workflow" },
				{ "v0_6_0/activity_property_edited.workflow" },
				{ "v0_6_0/activity_property.workflow" },
				{ "v0_6_0/activity_registereddynamicaspect_edited.workflow" },
				{ "v0_6_0/activity_registereddynamicaspect.workflow" },
				{ "v0_6_0/activity_standalonedynamicaspect_edited.workflow" },
				{ "v0_6_0/activity_standalonedynamicaspect.workflow" },
				{ "v0_6_0/activity_staticaspect_edited.workflow" },
				{ "v0_6_0/activity_staticaspect.workflow" },
				{ "v0_6_0/ExternalApplications06.workflow" },
				{ "v0_6_0/Itil06.workflow" }, { "v1_1_0/testNoRole.workflow" },
				{ "v1_2_0/ExternalApplications.workflow" },
				{ "v1_2_0/testMergeNode.workflow" }
		// ,{ "v1_3_0/activity.workflow" },
		// { "v1_3_0/automatic activity.workflow" },
		// { "v1_3_0/decision node.workflow" },
		// { "v1_3_0/empty activity.workflow" },
		// { "v1_3_0/role.workflow" }, { "v1_3_0/subprocess.workflow" }
		};
		return Arrays.asList(data);
	}

	private final String resourcePath;
	private URI sampleURI;

	/**
	 * Constructor for TestConverter.
	 * 
	 * @param name
	 */
	public TestConverter(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	@Before
	public void setUp() throws Exception {
		assertNotNull("Resource file path is null", resourcePath);
		InputStream input = getClass().getResourceAsStream(
				SAMPLE_PACKAGE + '/' + resourcePath);
		assertNotNull("Cannot find resource file '" + resourcePath
				+ "' for test", input);
		File inputFile = File.createTempFile(getClass().getSimpleName(),
				".workflow");
		assertTrue("inputFile " + inputFile.getAbsolutePath()
				+ " does not exist", inputFile.exists());
		copyFile(input, inputFile);
		this.sampleURI = URI.createFileURI(inputFile.getPath());

		Converter.activateJUnitMode();
	}

	@Test
	public void testConverter() throws Exception {
		Converter.updateModel(sampleURI, CURRENT_VERSION, false);
		FileInputStream fis = new FileInputStream(sampleURI.toFileString());
		assertTrue("Export result workflow file seems too small to be good",
				fis.available() > 300);
	}

	private static void copyFile(InputStream in, File out) throws Exception {
		FileOutputStream fos = new FileOutputStream(out);
		try {
			byte[] buf = new byte[1024];
			int i = 0;
			while ((i = in.read(buf)) != -1) {
				fos.write(buf, 0, i);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (in != null)
				in.close();
			if (fos != null)
				fos.close();
		}
	}
}
