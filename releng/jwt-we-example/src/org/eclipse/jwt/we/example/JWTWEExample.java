/*
 * Copyright (c) 2009 University of Augsburg
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, University of Augsburg - initial API and implementation
 */
package org.eclipse.jwt.we.example;

import java.net.MalformedURLException;
import java.net.URL;

public class JWTWEExample extends org.eclipse.jwt.examples.wizards.JWTExample {
	
	public JWTWEExample() throws MalformedURLException
	{
		super(
				new URL(Activator.getDefault().getZipURL()
						+ Messages.JWTExample1_zip), 
				Messages.JWTExample1_title,
				Messages.JWTExample1_desc,
				0,
				Activator
						.getDefault()
						.getBundledImageDescriptor(
								Messages.JWTExample1_image));
	}

}


