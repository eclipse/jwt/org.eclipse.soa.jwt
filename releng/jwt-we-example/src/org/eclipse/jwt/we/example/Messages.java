/*
 * Copyright (c) 2009 University of Augsburg
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, University of Augsburg - initial API and implementation
 */
package org.eclipse.jwt.we.example;

import org.eclipse.osgi.util.NLS;

/**
 * 
 * @author Florian Lautenbacher, thanks to Stephane Drapeau
 * 
 */
public final class Messages extends NLS {

	private static final String BUNDLE_NAME = "org.eclipse.jwt.we.example.Messages";//$NON-NLS-1$

	private Messages() {
		// Do not instantiate
	}

	public static String JWTExample1_title;
	public static String JWTExample1_desc;
	public static String JWTExample1_image;
	public static String JWTExample1_zip;
	
	static {
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

}
