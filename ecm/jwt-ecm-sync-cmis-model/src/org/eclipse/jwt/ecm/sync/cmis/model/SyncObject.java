/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sync Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getOutgoingRelationships <em>Outgoing Relationships</em>}</li>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getIngoingRelationships <em>Ingoing Relationships</em>}</li>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getProperties <em>Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncObject()
 * @model abstract="true"
 * @generated
 */
public interface SyncObject extends SyncItem {
	/**
	 * Returns the value of the '<em><b>Outgoing Relationships</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Relationships</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Relationships</em>' reference list.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncObject_OutgoingRelationships()
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getSource
	 * @model opposite="source" ordered="false"
	 * @generated
	 */
	EList<SyncRelationship> getOutgoingRelationships();

	/**
	 * Returns the value of the '<em><b>Ingoing Relationships</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ingoing Relationships</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ingoing Relationships</em>' reference list.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncObject_IngoingRelationships()
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getTarget
	 * @model opposite="target" ordered="false"
	 * @generated
	 */
	EList<SyncRelationship> getIngoingRelationships();

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.Object},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' map.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncObject_Properties()
	 * @model mapType="org.eclipse.jwt.ecm.sync.cmis.model.StringToObjectMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EJavaObject>"
	 * @generated
	 */
	EMap<String, Object> getProperties();

} // SyncObject
