/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sync Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncRelationship()
 * @model
 * @generated
 */
public interface SyncRelationship extends SyncObject {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getOutgoingRelationships <em>Outgoing Relationships</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(SyncObject)
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncRelationship_Source()
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getOutgoingRelationships
	 * @model opposite="outgoingRelationships" required="true"
	 * @generated
	 */
	SyncObject getSource();

	/**
	 * Sets the value of the '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(SyncObject value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getIngoingRelationships <em>Ingoing Relationships</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(SyncObject)
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncRelationship_Target()
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getIngoingRelationships
	 * @model opposite="ingoingRelationships"
	 * @generated
	 */
	SyncObject getTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(SyncObject value);

} // SyncRelationship
