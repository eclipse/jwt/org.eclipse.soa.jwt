/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sync Document</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncDocumentImpl#getContentStream <em>Content Stream</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SyncDocumentImpl extends SyncFileableObjectImpl implements SyncDocument {
	/**
	 * The cached value of the '{@link #getContentStream() <em>Content Stream</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContentStream()
	 * @generated
	 * @ordered
	 */
	protected SyncContentStream contentStream;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SyncDocumentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SyncModelPackage.Literals.SYNC_DOCUMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncContentStream getContentStream() {
		return contentStream;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContentStream(SyncContentStream newContentStream, NotificationChain msgs) {
		SyncContentStream oldContentStream = contentStream;
		contentStream = newContentStream;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SyncModelPackage.SYNC_DOCUMENT__CONTENT_STREAM, oldContentStream, newContentStream);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContentStream(SyncContentStream newContentStream) {
		if (newContentStream != contentStream) {
			NotificationChain msgs = null;
			if (contentStream != null)
				msgs = ((InternalEObject)contentStream).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SyncModelPackage.SYNC_DOCUMENT__CONTENT_STREAM, null, msgs);
			if (newContentStream != null)
				msgs = ((InternalEObject)newContentStream).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SyncModelPackage.SYNC_DOCUMENT__CONTENT_STREAM, null, msgs);
			msgs = basicSetContentStream(newContentStream, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SyncModelPackage.SYNC_DOCUMENT__CONTENT_STREAM, newContentStream, newContentStream));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SyncModelPackage.SYNC_DOCUMENT__CONTENT_STREAM:
				return basicSetContentStream(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SyncModelPackage.SYNC_DOCUMENT__CONTENT_STREAM:
				return getContentStream();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SyncModelPackage.SYNC_DOCUMENT__CONTENT_STREAM:
				setContentStream((SyncContentStream)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SyncModelPackage.SYNC_DOCUMENT__CONTENT_STREAM:
				setContentStream((SyncContentStream)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SyncModelPackage.SYNC_DOCUMENT__CONTENT_STREAM:
				return contentStream != null;
		}
		return super.eIsSet(featureID);
	}

} //SyncDocumentImpl
