/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sync Fileable Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject#getParents <em>Parents</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncFileableObject()
 * @model abstract="true"
 * @generated
 */
public interface SyncFileableObject extends SyncObject {
	/**
	 * Returns the value of the '<em><b>Parents</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parents</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parents</em>' reference list.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncFileableObject_Parents()
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder#getChildren
	 * @model opposite="children"
	 * @generated
	 */
	EList<SyncFolder> getParents();

} // SyncFileableObject
