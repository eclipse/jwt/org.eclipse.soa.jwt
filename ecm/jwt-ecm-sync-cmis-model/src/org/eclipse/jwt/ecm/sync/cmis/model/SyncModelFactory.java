/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage
 * @generated
 */
public interface SyncModelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SyncModelFactory eINSTANCE = org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Sync Document</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Document</em>'.
	 * @generated
	 */
	SyncDocument createSyncDocument();

	/**
	 * Returns a new object of class '<em>Sync Folder</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Folder</em>'.
	 * @generated
	 */
	SyncFolder createSyncFolder();

	/**
	 * Returns a new object of class '<em>Sync Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Relationship</em>'.
	 * @generated
	 */
	SyncRelationship createSyncRelationship();

	/**
	 * Returns a new object of class '<em>Sync Content Stream</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Content Stream</em>'.
	 * @generated
	 */
	SyncContentStream createSyncContentStream();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SyncModelPackage getSyncModelPackage();

} //SyncModelFactory
