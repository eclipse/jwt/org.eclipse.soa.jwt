/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sync Folder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder#getChildren <em>Children</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncFolder()
 * @model
 * @generated
 */
public interface SyncFolder extends SyncFileableObject {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject#getParents <em>Parents</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' reference list.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncFolder_Children()
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject#getParents
	 * @model opposite="parents" ordered="false"
	 * @generated
	 */
	EList<SyncFileableObject> getChildren();

} // SyncFolder
