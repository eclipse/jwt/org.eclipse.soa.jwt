/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sync Document</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument#getContentStream <em>Content Stream</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncDocument()
 * @model
 * @generated
 */
public interface SyncDocument extends SyncFileableObject {
	/**
	 * Returns the value of the '<em><b>Content Stream</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content Stream</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content Stream</em>' containment reference.
	 * @see #setContentStream(SyncContentStream)
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#getSyncDocument_ContentStream()
	 * @model containment="true"
	 * @generated
	 */
	SyncContentStream getContentStream();

	/**
	 * Sets the value of the '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument#getContentStream <em>Content Stream</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content Stream</em>' containment reference.
	 * @see #getContentStream()
	 * @generated
	 */
	void setContentStream(SyncContentStream value);

} // SyncDocument
