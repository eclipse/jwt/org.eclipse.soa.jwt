/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model.impl;

import java.util.Map.Entry;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.jwt.ecm.sync.cmis.model.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SyncModelFactoryImpl extends EFactoryImpl implements SyncModelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SyncModelFactory init() {
		try {
			SyncModelFactory theSyncModelFactory = (SyncModelFactory)EPackage.Registry.INSTANCE.getEFactory("org.eclipse.jwt.ecm.sync"); 
			if (theSyncModelFactory != null) {
				return theSyncModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SyncModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SyncModelPackage.SYNC_DOCUMENT: return createSyncDocument();
			case SyncModelPackage.SYNC_FOLDER: return createSyncFolder();
			case SyncModelPackage.SYNC_RELATIONSHIP: return createSyncRelationship();
			case SyncModelPackage.SYNC_CONTENT_STREAM: return createSyncContentStream();
			case SyncModelPackage.STRING_TO_OBJECT_MAP_ENTRY: return (EObject)createStringToObjectMapEntry();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncDocument createSyncDocument() {
		SyncDocumentImpl syncDocument = new SyncDocumentImpl();
		return syncDocument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncFolder createSyncFolder() {
		SyncFolderImpl syncFolder = new SyncFolderImpl();
		return syncFolder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncRelationship createSyncRelationship() {
		SyncRelationshipImpl syncRelationship = new SyncRelationshipImpl();
		return syncRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncContentStream createSyncContentStream() {
		SyncContentStreamImpl syncContentStream = new SyncContentStreamImpl();
		return syncContentStream;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Entry<String, Object> createStringToObjectMapEntry() {
		StringToObjectMapEntryImpl stringToObjectMapEntry = new StringToObjectMapEntryImpl();
		return stringToObjectMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncModelPackage getSyncModelPackage() {
		return (SyncModelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SyncModelPackage getPackage() {
		return SyncModelPackage.eINSTANCE;
	}

} //SyncModelFactoryImpl
