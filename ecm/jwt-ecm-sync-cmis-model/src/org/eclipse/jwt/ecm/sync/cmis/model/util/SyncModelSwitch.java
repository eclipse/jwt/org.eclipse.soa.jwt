/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model.util;

import java.util.Map.Entry;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.jwt.ecm.sync.cmis.model.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage
 * @generated
 */
public class SyncModelSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SyncModelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncModelSwitch() {
		if (modelPackage == null) {
			modelPackage = SyncModelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case SyncModelPackage.SYNC_OBJECT: {
				SyncObject syncObject = (SyncObject)theEObject;
				T result = caseSyncObject(syncObject);
				if (result == null) result = caseSyncItem(syncObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SyncModelPackage.SYNC_DOCUMENT: {
				SyncDocument syncDocument = (SyncDocument)theEObject;
				T result = caseSyncDocument(syncDocument);
				if (result == null) result = caseSyncFileableObject(syncDocument);
				if (result == null) result = caseSyncObject(syncDocument);
				if (result == null) result = caseSyncItem(syncDocument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SyncModelPackage.SYNC_FOLDER: {
				SyncFolder syncFolder = (SyncFolder)theEObject;
				T result = caseSyncFolder(syncFolder);
				if (result == null) result = caseSyncFileableObject(syncFolder);
				if (result == null) result = caseSyncObject(syncFolder);
				if (result == null) result = caseSyncItem(syncFolder);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SyncModelPackage.SYNC_RELATIONSHIP: {
				SyncRelationship syncRelationship = (SyncRelationship)theEObject;
				T result = caseSyncRelationship(syncRelationship);
				if (result == null) result = caseSyncObject(syncRelationship);
				if (result == null) result = caseSyncItem(syncRelationship);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SyncModelPackage.SYNC_FILEABLE_OBJECT: {
				SyncFileableObject syncFileableObject = (SyncFileableObject)theEObject;
				T result = caseSyncFileableObject(syncFileableObject);
				if (result == null) result = caseSyncObject(syncFileableObject);
				if (result == null) result = caseSyncItem(syncFileableObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SyncModelPackage.SYNC_ITEM: {
				SyncItem syncItem = (SyncItem)theEObject;
				T result = caseSyncItem(syncItem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SyncModelPackage.SYNC_CONTENT_STREAM: {
				SyncContentStream syncContentStream = (SyncContentStream)theEObject;
				T result = caseSyncContentStream(syncContentStream);
				if (result == null) result = caseSyncItem(syncContentStream);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SyncModelPackage.STRING_TO_OBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Entry<String, Object> stringToObjectMapEntry = (Entry<String, Object>)theEObject;
				T result = caseStringToObjectMapEntry(stringToObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncObject(SyncObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Document</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Document</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncDocument(SyncDocument object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Folder</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Folder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncFolder(SyncFolder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Relationship</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncRelationship(SyncRelationship object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Fileable Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Fileable Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncFileableObject(SyncFileableObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncItem(SyncItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Content Stream</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Content Stream</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncContentStream(SyncContentStream object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String To Object Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String To Object Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringToObjectMapEntry(Entry<String, Object> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SyncModelSwitch
