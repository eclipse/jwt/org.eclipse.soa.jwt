/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelFactory
 * @model kind="package"
 * @generated
 */
public interface SyncModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "model";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "org.eclipse.jwt.ecm.sync";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sync";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SyncModelPackage eINSTANCE = org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncItemImpl <em>Sync Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncItemImpl
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncItem()
	 * @generated
	 */
	int SYNC_ITEM = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_ITEM__ID = 0;

	/**
	 * The feature id for the '<em><b>Synchronized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_ITEM__SYNCHRONIZED = 1;

	/**
	 * The number of structural features of the '<em>Sync Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_ITEM_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncObjectImpl <em>Sync Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncObjectImpl
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncObject()
	 * @generated
	 */
	int SYNC_OBJECT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_OBJECT__ID = SYNC_ITEM__ID;

	/**
	 * The feature id for the '<em><b>Synchronized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_OBJECT__SYNCHRONIZED = SYNC_ITEM__SYNCHRONIZED;

	/**
	 * The feature id for the '<em><b>Outgoing Relationships</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_OBJECT__OUTGOING_RELATIONSHIPS = SYNC_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ingoing Relationships</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_OBJECT__INGOING_RELATIONSHIPS = SYNC_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_OBJECT__PROPERTIES = SYNC_ITEM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Sync Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_OBJECT_FEATURE_COUNT = SYNC_ITEM_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncFileableObjectImpl <em>Sync Fileable Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncFileableObjectImpl
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncFileableObject()
	 * @generated
	 */
	int SYNC_FILEABLE_OBJECT = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FILEABLE_OBJECT__ID = SYNC_OBJECT__ID;

	/**
	 * The feature id for the '<em><b>Synchronized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FILEABLE_OBJECT__SYNCHRONIZED = SYNC_OBJECT__SYNCHRONIZED;

	/**
	 * The feature id for the '<em><b>Outgoing Relationships</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FILEABLE_OBJECT__OUTGOING_RELATIONSHIPS = SYNC_OBJECT__OUTGOING_RELATIONSHIPS;

	/**
	 * The feature id for the '<em><b>Ingoing Relationships</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FILEABLE_OBJECT__INGOING_RELATIONSHIPS = SYNC_OBJECT__INGOING_RELATIONSHIPS;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FILEABLE_OBJECT__PROPERTIES = SYNC_OBJECT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Parents</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FILEABLE_OBJECT__PARENTS = SYNC_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sync Fileable Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FILEABLE_OBJECT_FEATURE_COUNT = SYNC_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncDocumentImpl <em>Sync Document</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncDocumentImpl
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncDocument()
	 * @generated
	 */
	int SYNC_DOCUMENT = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_DOCUMENT__ID = SYNC_FILEABLE_OBJECT__ID;

	/**
	 * The feature id for the '<em><b>Synchronized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_DOCUMENT__SYNCHRONIZED = SYNC_FILEABLE_OBJECT__SYNCHRONIZED;

	/**
	 * The feature id for the '<em><b>Outgoing Relationships</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_DOCUMENT__OUTGOING_RELATIONSHIPS = SYNC_FILEABLE_OBJECT__OUTGOING_RELATIONSHIPS;

	/**
	 * The feature id for the '<em><b>Ingoing Relationships</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_DOCUMENT__INGOING_RELATIONSHIPS = SYNC_FILEABLE_OBJECT__INGOING_RELATIONSHIPS;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_DOCUMENT__PROPERTIES = SYNC_FILEABLE_OBJECT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Parents</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_DOCUMENT__PARENTS = SYNC_FILEABLE_OBJECT__PARENTS;

	/**
	 * The feature id for the '<em><b>Content Stream</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_DOCUMENT__CONTENT_STREAM = SYNC_FILEABLE_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sync Document</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_DOCUMENT_FEATURE_COUNT = SYNC_FILEABLE_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncFolderImpl <em>Sync Folder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncFolderImpl
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncFolder()
	 * @generated
	 */
	int SYNC_FOLDER = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FOLDER__ID = SYNC_FILEABLE_OBJECT__ID;

	/**
	 * The feature id for the '<em><b>Synchronized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FOLDER__SYNCHRONIZED = SYNC_FILEABLE_OBJECT__SYNCHRONIZED;

	/**
	 * The feature id for the '<em><b>Outgoing Relationships</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FOLDER__OUTGOING_RELATIONSHIPS = SYNC_FILEABLE_OBJECT__OUTGOING_RELATIONSHIPS;

	/**
	 * The feature id for the '<em><b>Ingoing Relationships</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FOLDER__INGOING_RELATIONSHIPS = SYNC_FILEABLE_OBJECT__INGOING_RELATIONSHIPS;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FOLDER__PROPERTIES = SYNC_FILEABLE_OBJECT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Parents</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FOLDER__PARENTS = SYNC_FILEABLE_OBJECT__PARENTS;

	/**
	 * The feature id for the '<em><b>Children</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FOLDER__CHILDREN = SYNC_FILEABLE_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sync Folder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_FOLDER_FEATURE_COUNT = SYNC_FILEABLE_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncRelationshipImpl <em>Sync Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncRelationshipImpl
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncRelationship()
	 * @generated
	 */
	int SYNC_RELATIONSHIP = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_RELATIONSHIP__ID = SYNC_OBJECT__ID;

	/**
	 * The feature id for the '<em><b>Synchronized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_RELATIONSHIP__SYNCHRONIZED = SYNC_OBJECT__SYNCHRONIZED;

	/**
	 * The feature id for the '<em><b>Outgoing Relationships</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_RELATIONSHIP__OUTGOING_RELATIONSHIPS = SYNC_OBJECT__OUTGOING_RELATIONSHIPS;

	/**
	 * The feature id for the '<em><b>Ingoing Relationships</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_RELATIONSHIP__INGOING_RELATIONSHIPS = SYNC_OBJECT__INGOING_RELATIONSHIPS;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_RELATIONSHIP__PROPERTIES = SYNC_OBJECT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_RELATIONSHIP__SOURCE = SYNC_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_RELATIONSHIP__TARGET = SYNC_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sync Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_RELATIONSHIP_FEATURE_COUNT = SYNC_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncContentStreamImpl <em>Sync Content Stream</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncContentStreamImpl
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncContentStream()
	 * @generated
	 */
	int SYNC_CONTENT_STREAM = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_CONTENT_STREAM__ID = SYNC_ITEM__ID;

	/**
	 * The feature id for the '<em><b>Synchronized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_CONTENT_STREAM__SYNCHRONIZED = SYNC_ITEM__SYNCHRONIZED;

	/**
	 * The feature id for the '<em><b>File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_CONTENT_STREAM__FILE_NAME = SYNC_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Mime Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_CONTENT_STREAM__MIME_TYPE = SYNC_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_CONTENT_STREAM__LENGTH = SYNC_ITEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_CONTENT_STREAM__CONTENT = SYNC_ITEM_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Sync Content Stream</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_CONTENT_STREAM_FEATURE_COUNT = SYNC_ITEM_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.StringToObjectMapEntryImpl <em>String To Object Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.StringToObjectMapEntryImpl
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getStringToObjectMapEntry()
	 * @generated
	 */
	int STRING_TO_OBJECT_MAP_ENTRY = 7;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_OBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_OBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To Object Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_OBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * Returns the meta object for class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncObject <em>Sync Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sync Object</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncObject
	 * @generated
	 */
	EClass getSyncObject();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getOutgoingRelationships <em>Outgoing Relationships</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing Relationships</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getOutgoingRelationships()
	 * @see #getSyncObject()
	 * @generated
	 */
	EReference getSyncObject_OutgoingRelationships();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getIngoingRelationships <em>Ingoing Relationships</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ingoing Relationships</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getIngoingRelationships()
	 * @see #getSyncObject()
	 * @generated
	 */
	EReference getSyncObject_IngoingRelationships();

	/**
	 * Returns the meta object for the map '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Properties</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncObject#getProperties()
	 * @see #getSyncObject()
	 * @generated
	 */
	EReference getSyncObject_Properties();

	/**
	 * Returns the meta object for class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument <em>Sync Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sync Document</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument
	 * @generated
	 */
	EClass getSyncDocument();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument#getContentStream <em>Content Stream</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Content Stream</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument#getContentStream()
	 * @see #getSyncDocument()
	 * @generated
	 */
	EReference getSyncDocument_ContentStream();

	/**
	 * Returns the meta object for class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder <em>Sync Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sync Folder</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder
	 * @generated
	 */
	EClass getSyncFolder();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Children</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder#getChildren()
	 * @see #getSyncFolder()
	 * @generated
	 */
	EReference getSyncFolder_Children();

	/**
	 * Returns the meta object for class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship <em>Sync Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sync Relationship</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship
	 * @generated
	 */
	EClass getSyncRelationship();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getSource()
	 * @see #getSyncRelationship()
	 * @generated
	 */
	EReference getSyncRelationship_Source();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship#getTarget()
	 * @see #getSyncRelationship()
	 * @generated
	 */
	EReference getSyncRelationship_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject <em>Sync Fileable Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sync Fileable Object</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject
	 * @generated
	 */
	EClass getSyncFileableObject();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject#getParents <em>Parents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Parents</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject#getParents()
	 * @see #getSyncFileableObject()
	 * @generated
	 */
	EReference getSyncFileableObject_Parents();

	/**
	 * Returns the meta object for class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncItem <em>Sync Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sync Item</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncItem
	 * @generated
	 */
	EClass getSyncItem();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncItem#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncItem#getId()
	 * @see #getSyncItem()
	 * @generated
	 */
	EAttribute getSyncItem_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncItem#isSynchronized <em>Synchronized</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Synchronized</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncItem#isSynchronized()
	 * @see #getSyncItem()
	 * @generated
	 */
	EAttribute getSyncItem_Synchronized();

	/**
	 * Returns the meta object for class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream <em>Sync Content Stream</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sync Content Stream</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream
	 * @generated
	 */
	EClass getSyncContentStream();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream#getFileName <em>File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>File Name</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream#getFileName()
	 * @see #getSyncContentStream()
	 * @generated
	 */
	EAttribute getSyncContentStream_FileName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream#getMimeType <em>Mime Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mime Type</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream#getMimeType()
	 * @see #getSyncContentStream()
	 * @generated
	 */
	EAttribute getSyncContentStream_MimeType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream#getLength <em>Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Length</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream#getLength()
	 * @see #getSyncContentStream()
	 * @generated
	 */
	EAttribute getSyncContentStream_Length();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream#getContent()
	 * @see #getSyncContentStream()
	 * @generated
	 */
	EAttribute getSyncContentStream_Content();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To Object Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To Object Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString"
	 *        valueDataType="org.eclipse.emf.ecore.EJavaObject"
	 * @generated
	 */
	EClass getStringToObjectMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToObjectMapEntry()
	 * @generated
	 */
	EAttribute getStringToObjectMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringToObjectMapEntry()
	 * @generated
	 */
	EAttribute getStringToObjectMapEntry_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SyncModelFactory getSyncModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncObjectImpl <em>Sync Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncObjectImpl
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncObject()
		 * @generated
		 */
		EClass SYNC_OBJECT = eINSTANCE.getSyncObject();

		/**
		 * The meta object literal for the '<em><b>Outgoing Relationships</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNC_OBJECT__OUTGOING_RELATIONSHIPS = eINSTANCE.getSyncObject_OutgoingRelationships();

		/**
		 * The meta object literal for the '<em><b>Ingoing Relationships</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNC_OBJECT__INGOING_RELATIONSHIPS = eINSTANCE.getSyncObject_IngoingRelationships();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNC_OBJECT__PROPERTIES = eINSTANCE.getSyncObject_Properties();

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncDocumentImpl <em>Sync Document</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncDocumentImpl
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncDocument()
		 * @generated
		 */
		EClass SYNC_DOCUMENT = eINSTANCE.getSyncDocument();

		/**
		 * The meta object literal for the '<em><b>Content Stream</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNC_DOCUMENT__CONTENT_STREAM = eINSTANCE.getSyncDocument_ContentStream();

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncFolderImpl <em>Sync Folder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncFolderImpl
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncFolder()
		 * @generated
		 */
		EClass SYNC_FOLDER = eINSTANCE.getSyncFolder();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNC_FOLDER__CHILDREN = eINSTANCE.getSyncFolder_Children();

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncRelationshipImpl <em>Sync Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncRelationshipImpl
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncRelationship()
		 * @generated
		 */
		EClass SYNC_RELATIONSHIP = eINSTANCE.getSyncRelationship();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNC_RELATIONSHIP__SOURCE = eINSTANCE.getSyncRelationship_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNC_RELATIONSHIP__TARGET = eINSTANCE.getSyncRelationship_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncFileableObjectImpl <em>Sync Fileable Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncFileableObjectImpl
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncFileableObject()
		 * @generated
		 */
		EClass SYNC_FILEABLE_OBJECT = eINSTANCE.getSyncFileableObject();

		/**
		 * The meta object literal for the '<em><b>Parents</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNC_FILEABLE_OBJECT__PARENTS = eINSTANCE.getSyncFileableObject_Parents();

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncItemImpl <em>Sync Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncItemImpl
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncItem()
		 * @generated
		 */
		EClass SYNC_ITEM = eINSTANCE.getSyncItem();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYNC_ITEM__ID = eINSTANCE.getSyncItem_Id();

		/**
		 * The meta object literal for the '<em><b>Synchronized</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYNC_ITEM__SYNCHRONIZED = eINSTANCE.getSyncItem_Synchronized();

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncContentStreamImpl <em>Sync Content Stream</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncContentStreamImpl
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getSyncContentStream()
		 * @generated
		 */
		EClass SYNC_CONTENT_STREAM = eINSTANCE.getSyncContentStream();

		/**
		 * The meta object literal for the '<em><b>File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYNC_CONTENT_STREAM__FILE_NAME = eINSTANCE.getSyncContentStream_FileName();

		/**
		 * The meta object literal for the '<em><b>Mime Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYNC_CONTENT_STREAM__MIME_TYPE = eINSTANCE.getSyncContentStream_MimeType();

		/**
		 * The meta object literal for the '<em><b>Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYNC_CONTENT_STREAM__LENGTH = eINSTANCE.getSyncContentStream_Length();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYNC_CONTENT_STREAM__CONTENT = eINSTANCE.getSyncContentStream_Content();

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.StringToObjectMapEntryImpl <em>String To Object Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.StringToObjectMapEntryImpl
		 * @see org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncModelPackageImpl#getStringToObjectMapEntry()
		 * @generated
		 */
		EClass STRING_TO_OBJECT_MAP_ENTRY = eINSTANCE.getStringToObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_OBJECT_MAP_ENTRY__KEY = eINSTANCE.getStringToObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_TO_OBJECT_MAP_ENTRY__VALUE = eINSTANCE.getStringToObjectMapEntry_Value();

	}

} //SyncModelPackage
