/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sync Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncObjectImpl#getOutgoingRelationships <em>Outgoing Relationships</em>}</li>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncObjectImpl#getIngoingRelationships <em>Ingoing Relationships</em>}</li>
 *   <li>{@link org.eclipse.jwt.ecm.sync.cmis.model.impl.SyncObjectImpl#getProperties <em>Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class SyncObjectImpl extends SyncItemImpl implements SyncObject {
	/**
	 * The cached value of the '{@link #getOutgoingRelationships() <em>Outgoing Relationships</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutgoingRelationships()
	 * @generated
	 * @ordered
	 */
	protected EList<SyncRelationship> outgoingRelationships;

	/**
	 * The cached value of the '{@link #getIngoingRelationships() <em>Ingoing Relationships</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIngoingRelationships()
	 * @generated
	 * @ordered
	 */
	protected EList<SyncRelationship> ingoingRelationships;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, Object> properties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SyncObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SyncModelPackage.Literals.SYNC_OBJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SyncRelationship> getOutgoingRelationships() {
		if (outgoingRelationships == null) {
			outgoingRelationships = new EObjectWithInverseResolvingEList<SyncRelationship>(SyncRelationship.class, this, SyncModelPackage.SYNC_OBJECT__OUTGOING_RELATIONSHIPS, SyncModelPackage.SYNC_RELATIONSHIP__SOURCE);
		}
		return outgoingRelationships;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SyncRelationship> getIngoingRelationships() {
		if (ingoingRelationships == null) {
			ingoingRelationships = new EObjectWithInverseResolvingEList<SyncRelationship>(SyncRelationship.class, this, SyncModelPackage.SYNC_OBJECT__INGOING_RELATIONSHIPS, SyncModelPackage.SYNC_RELATIONSHIP__TARGET);
		}
		return ingoingRelationships;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, Object> getProperties() {
		if (properties == null) {
			properties = new EcoreEMap<String,Object>(SyncModelPackage.Literals.STRING_TO_OBJECT_MAP_ENTRY, StringToObjectMapEntryImpl.class, this, SyncModelPackage.SYNC_OBJECT__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SyncModelPackage.SYNC_OBJECT__OUTGOING_RELATIONSHIPS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutgoingRelationships()).basicAdd(otherEnd, msgs);
			case SyncModelPackage.SYNC_OBJECT__INGOING_RELATIONSHIPS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIngoingRelationships()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SyncModelPackage.SYNC_OBJECT__OUTGOING_RELATIONSHIPS:
				return ((InternalEList<?>)getOutgoingRelationships()).basicRemove(otherEnd, msgs);
			case SyncModelPackage.SYNC_OBJECT__INGOING_RELATIONSHIPS:
				return ((InternalEList<?>)getIngoingRelationships()).basicRemove(otherEnd, msgs);
			case SyncModelPackage.SYNC_OBJECT__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SyncModelPackage.SYNC_OBJECT__OUTGOING_RELATIONSHIPS:
				return getOutgoingRelationships();
			case SyncModelPackage.SYNC_OBJECT__INGOING_RELATIONSHIPS:
				return getIngoingRelationships();
			case SyncModelPackage.SYNC_OBJECT__PROPERTIES:
				if (coreType) return getProperties();
				else return getProperties().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SyncModelPackage.SYNC_OBJECT__OUTGOING_RELATIONSHIPS:
				getOutgoingRelationships().clear();
				getOutgoingRelationships().addAll((Collection<? extends SyncRelationship>)newValue);
				return;
			case SyncModelPackage.SYNC_OBJECT__INGOING_RELATIONSHIPS:
				getIngoingRelationships().clear();
				getIngoingRelationships().addAll((Collection<? extends SyncRelationship>)newValue);
				return;
			case SyncModelPackage.SYNC_OBJECT__PROPERTIES:
				((EStructuralFeature.Setting)getProperties()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SyncModelPackage.SYNC_OBJECT__OUTGOING_RELATIONSHIPS:
				getOutgoingRelationships().clear();
				return;
			case SyncModelPackage.SYNC_OBJECT__INGOING_RELATIONSHIPS:
				getIngoingRelationships().clear();
				return;
			case SyncModelPackage.SYNC_OBJECT__PROPERTIES:
				getProperties().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SyncModelPackage.SYNC_OBJECT__OUTGOING_RELATIONSHIPS:
				return outgoingRelationships != null && !outgoingRelationships.isEmpty();
			case SyncModelPackage.SYNC_OBJECT__INGOING_RELATIONSHIPS:
				return ingoingRelationships != null && !ingoingRelationships.isEmpty();
			case SyncModelPackage.SYNC_OBJECT__PROPERTIES:
				return properties != null && !properties.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SyncObjectImpl
