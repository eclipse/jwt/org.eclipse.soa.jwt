/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model.util;

import java.util.Map.Entry;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.jwt.ecm.sync.cmis.model.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage
 * @generated
 */
public class SyncModelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SyncModelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncModelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SyncModelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SyncModelSwitch<Adapter> modelSwitch =
		new SyncModelSwitch<Adapter>() {
			@Override
			public Adapter caseSyncObject(SyncObject object) {
				return createSyncObjectAdapter();
			}
			@Override
			public Adapter caseSyncDocument(SyncDocument object) {
				return createSyncDocumentAdapter();
			}
			@Override
			public Adapter caseSyncFolder(SyncFolder object) {
				return createSyncFolderAdapter();
			}
			@Override
			public Adapter caseSyncRelationship(SyncRelationship object) {
				return createSyncRelationshipAdapter();
			}
			@Override
			public Adapter caseSyncFileableObject(SyncFileableObject object) {
				return createSyncFileableObjectAdapter();
			}
			@Override
			public Adapter caseSyncItem(SyncItem object) {
				return createSyncItemAdapter();
			}
			@Override
			public Adapter caseSyncContentStream(SyncContentStream object) {
				return createSyncContentStreamAdapter();
			}
			@Override
			public Adapter caseStringToObjectMapEntry(Entry<String, Object> object) {
				return createStringToObjectMapEntryAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncObject <em>Sync Object</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncObject
	 * @generated
	 */
	public Adapter createSyncObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument <em>Sync Document</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument
	 * @generated
	 */
	public Adapter createSyncDocumentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder <em>Sync Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder
	 * @generated
	 */
	public Adapter createSyncFolderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship <em>Sync Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship
	 * @generated
	 */
	public Adapter createSyncRelationshipAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject <em>Sync Fileable Object</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject
	 * @generated
	 */
	public Adapter createSyncFileableObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncItem <em>Sync Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncItem
	 * @generated
	 */
	public Adapter createSyncItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream <em>Sync Content Stream</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream
	 * @generated
	 */
	public Adapter createSyncContentStreamAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>String To Object Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createStringToObjectMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //SyncModelAdapterFactory
