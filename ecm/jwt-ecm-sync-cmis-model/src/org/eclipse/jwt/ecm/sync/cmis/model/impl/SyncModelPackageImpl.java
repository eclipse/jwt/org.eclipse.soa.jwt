/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.eclipse.jwt.ecm.sync.cmis.model.impl;

import java.util.Map.Entry;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncItem;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncModelFactory;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SyncModelPackageImpl extends EPackageImpl implements SyncModelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncDocumentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncFolderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncFileableObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncContentStreamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToObjectMapEntryEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SyncModelPackageImpl() {
		super(eNS_URI, SyncModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SyncModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SyncModelPackage init() {
		if (isInited) return (SyncModelPackage)EPackage.Registry.INSTANCE.getEPackage(SyncModelPackage.eNS_URI);

		// Obtain or create and register package
		SyncModelPackageImpl theSyncModelPackage = (SyncModelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SyncModelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SyncModelPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theSyncModelPackage.createPackageContents();

		// Initialize created meta-data
		theSyncModelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSyncModelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SyncModelPackage.eNS_URI, theSyncModelPackage);
		return theSyncModelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncObject() {
		return syncObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncObject_OutgoingRelationships() {
		return (EReference)syncObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncObject_IngoingRelationships() {
		return (EReference)syncObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncObject_Properties() {
		return (EReference)syncObjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncDocument() {
		return syncDocumentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncDocument_ContentStream() {
		return (EReference)syncDocumentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncFolder() {
		return syncFolderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncFolder_Children() {
		return (EReference)syncFolderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncRelationship() {
		return syncRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncRelationship_Source() {
		return (EReference)syncRelationshipEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncRelationship_Target() {
		return (EReference)syncRelationshipEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncFileableObject() {
		return syncFileableObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSyncFileableObject_Parents() {
		return (EReference)syncFileableObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncItem() {
		return syncItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncItem_Id() {
		return (EAttribute)syncItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncItem_Synchronized() {
		return (EAttribute)syncItemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncContentStream() {
		return syncContentStreamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncContentStream_FileName() {
		return (EAttribute)syncContentStreamEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncContentStream_MimeType() {
		return (EAttribute)syncContentStreamEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncContentStream_Length() {
		return (EAttribute)syncContentStreamEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSyncContentStream_Content() {
		return (EAttribute)syncContentStreamEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToObjectMapEntry() {
		return stringToObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToObjectMapEntry_Key() {
		return (EAttribute)stringToObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringToObjectMapEntry_Value() {
		return (EAttribute)stringToObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncModelFactory getSyncModelFactory() {
		return (SyncModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		syncObjectEClass = createEClass(SYNC_OBJECT);
		createEReference(syncObjectEClass, SYNC_OBJECT__OUTGOING_RELATIONSHIPS);
		createEReference(syncObjectEClass, SYNC_OBJECT__INGOING_RELATIONSHIPS);
		createEReference(syncObjectEClass, SYNC_OBJECT__PROPERTIES);

		syncDocumentEClass = createEClass(SYNC_DOCUMENT);
		createEReference(syncDocumentEClass, SYNC_DOCUMENT__CONTENT_STREAM);

		syncFolderEClass = createEClass(SYNC_FOLDER);
		createEReference(syncFolderEClass, SYNC_FOLDER__CHILDREN);

		syncRelationshipEClass = createEClass(SYNC_RELATIONSHIP);
		createEReference(syncRelationshipEClass, SYNC_RELATIONSHIP__SOURCE);
		createEReference(syncRelationshipEClass, SYNC_RELATIONSHIP__TARGET);

		syncFileableObjectEClass = createEClass(SYNC_FILEABLE_OBJECT);
		createEReference(syncFileableObjectEClass, SYNC_FILEABLE_OBJECT__PARENTS);

		syncItemEClass = createEClass(SYNC_ITEM);
		createEAttribute(syncItemEClass, SYNC_ITEM__ID);
		createEAttribute(syncItemEClass, SYNC_ITEM__SYNCHRONIZED);

		syncContentStreamEClass = createEClass(SYNC_CONTENT_STREAM);
		createEAttribute(syncContentStreamEClass, SYNC_CONTENT_STREAM__FILE_NAME);
		createEAttribute(syncContentStreamEClass, SYNC_CONTENT_STREAM__MIME_TYPE);
		createEAttribute(syncContentStreamEClass, SYNC_CONTENT_STREAM__LENGTH);
		createEAttribute(syncContentStreamEClass, SYNC_CONTENT_STREAM__CONTENT);

		stringToObjectMapEntryEClass = createEClass(STRING_TO_OBJECT_MAP_ENTRY);
		createEAttribute(stringToObjectMapEntryEClass, STRING_TO_OBJECT_MAP_ENTRY__KEY);
		createEAttribute(stringToObjectMapEntryEClass, STRING_TO_OBJECT_MAP_ENTRY__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		syncObjectEClass.getESuperTypes().add(this.getSyncItem());
		syncDocumentEClass.getESuperTypes().add(this.getSyncFileableObject());
		syncFolderEClass.getESuperTypes().add(this.getSyncFileableObject());
		syncRelationshipEClass.getESuperTypes().add(this.getSyncObject());
		syncFileableObjectEClass.getESuperTypes().add(this.getSyncObject());
		syncContentStreamEClass.getESuperTypes().add(this.getSyncItem());

		// Initialize classes and features; add operations and parameters
		initEClass(syncObjectEClass, SyncObject.class, "SyncObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSyncObject_OutgoingRelationships(), this.getSyncRelationship(), this.getSyncRelationship_Source(), "outgoingRelationships", null, 0, -1, SyncObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSyncObject_IngoingRelationships(), this.getSyncRelationship(), this.getSyncRelationship_Target(), "ingoingRelationships", null, 0, -1, SyncObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSyncObject_Properties(), this.getStringToObjectMapEntry(), null, "properties", null, 0, -1, SyncObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(syncDocumentEClass, SyncDocument.class, "SyncDocument", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSyncDocument_ContentStream(), this.getSyncContentStream(), null, "contentStream", null, 0, 1, SyncDocument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(syncFolderEClass, SyncFolder.class, "SyncFolder", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSyncFolder_Children(), this.getSyncFileableObject(), this.getSyncFileableObject_Parents(), "children", null, 0, -1, SyncFolder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(syncRelationshipEClass, SyncRelationship.class, "SyncRelationship", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSyncRelationship_Source(), this.getSyncObject(), this.getSyncObject_OutgoingRelationships(), "source", null, 1, 1, SyncRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSyncRelationship_Target(), this.getSyncObject(), this.getSyncObject_IngoingRelationships(), "target", null, 0, 1, SyncRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(syncFileableObjectEClass, SyncFileableObject.class, "SyncFileableObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSyncFileableObject_Parents(), this.getSyncFolder(), this.getSyncFolder_Children(), "parents", null, 0, -1, SyncFileableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(syncItemEClass, SyncItem.class, "SyncItem", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSyncItem_Id(), ecorePackage.getEString(), "id", null, 1, 1, SyncItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSyncItem_Synchronized(), ecorePackage.getEBoolean(), "synchronized", "false", 1, 1, SyncItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(syncContentStreamEClass, SyncContentStream.class, "SyncContentStream", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSyncContentStream_FileName(), ecorePackage.getEString(), "fileName", null, 1, 1, SyncContentStream.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSyncContentStream_MimeType(), ecorePackage.getEString(), "mimeType", null, 0, 1, SyncContentStream.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSyncContentStream_Length(), ecorePackage.getELong(), "length", null, 0, 1, SyncContentStream.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSyncContentStream_Content(), ecorePackage.getEByteArray(), "content", null, 0, 1, SyncContentStream.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringToObjectMapEntryEClass, Entry.class, "StringToObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringToObjectMapEntry_Key(), ecorePackage.getEString(), "key", null, 0, 1, Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringToObjectMapEntry_Value(), ecorePackage.getEJavaObject(), "value", null, 0, 1, Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //SyncModelPackageImpl
