package org.eclipse.jwt.ecm.sync.demo.internal;

import java.io.IOException;
import java.util.Map;

public interface CmisConfiguration {

	public Map<String, String> getSessionParameters() throws IOException;

}