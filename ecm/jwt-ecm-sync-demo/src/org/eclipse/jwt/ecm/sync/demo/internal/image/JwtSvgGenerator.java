package org.eclipse.jwt.ecm.sync.demo.internal.image;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.jwt.we.misc.util.SaveImage;

public class JwtSvgGenerator extends JwtImageGenerator {

	public JwtSvgGenerator(EObject activity) {
		super(activity);
	}

	public String getMimeType() {
		return "text/svg"; // TODO check
	}

	public byte[] getContent() {
//		try {
//			GraphicalViewer viewer = createGraphicalViewer();
//			File tmpFile;
//
//			tmpFile = File.createTempFile(
//					JwtSvgGenerator.class.getSimpleName(), ".svg");
//
//			SaveImage.saveSVG(viewer, tmpFile.getPath());
//
//			InputStream fileInput = new FileInputStream(tmpFile);
//
//			byte fileContent[] = new byte[(int) tmpFile.length()];
//
//			fileInput.read(fileContent);
//
//			return fileContent;
//		} catch (IOException e) {
//			throw new RuntimeException(e);
//		}
		return "This is SVG content".getBytes();
	}
}
