package org.eclipse.jwt.ecm.sync.demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jwt.ecm.sync.DmsSynchronizer;
import org.eclipse.jwt.ecm.sync.SynchronizationException;
import org.eclipse.jwt.ecm.sync.cmis.CmisDmsSynchronizer;
import org.eclipse.jwt.ecm.sync.demo.internal.CmisConfiguration;
import org.eclipse.jwt.ecm.sync.demo.internal.InputDialogCmisConfiguration;
import org.eclipse.jwt.ecm.sync.demo.internal.image.ExportToImageUtil;
import org.eclipse.jwt.ecm.sync.demo.internal.strategy.DemoMatchingStrategy;
import org.eclipse.jwt.ecm.sync.demo.internal.strategy.DemoSynchronizationStrategy;
import org.eclipse.jwt.transformations.api.TransformationException;
import org.eclipse.jwt.transformations.api.TransformationService;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModel;
import org.eclipse.m2m.atl.drivers.emf4atl.AtlEMFModelHandler;
import org.eclipse.m2m.atl.engine.vm.AtlLauncher;
import org.eclipse.m2m.atl.engine.vm.AtlModelHandler;
import org.eclipse.m2m.atl.engine.vm.ModelLoader;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModel;

public class DmsSynchronizationTransformation extends TransformationService {
	private final CmisConfiguration cmisConfiguration = new InputDialogCmisConfiguration();

	private final static String resourcePath = "/org/eclipse/jwt/ecm/sync/cmis/ecore/"; //$NON-NLS-1$

	private AtlEMFModelHandler modelHandler;
	private ModelLoader modelLoader;

	private InputStream JWT_ModelResource;
	private InputStream CMIS_ModelResource;
	private URL JWT2CMIS_TransfoResource;
	private ASMEMFModel jwtMetamodel;
	private ASMEMFModel cmisMetamodel;

	private void createResources() throws TransformationException {
		if (CMIS_ModelResource == null) {
			modelHandler = (AtlEMFModelHandler) AtlEMFModelHandler
					.getDefault(AtlModelHandler.AMH_EMF);
			modelLoader = modelHandler.createModelLoader();

			String mm_path = "/org/eclipse/jwt/meta/ecore/JWTMetaModel.ecore";

			JWT_ModelResource = getClass().getResourceAsStream(mm_path);//$NON-NLS-1$
			CMIS_ModelResource = getClass().getResourceAsStream(
					resourcePath + "CMISSyncModel.ecore");//$NON-NLS-1$

			JWT2CMIS_TransfoResource = DmsSynchronizationTransformation.class
					.getResource("internal/resources/jwt2cmis.asm");//$NON-NLS-1$
		}
	}

	private Map<String, Object> getMetamodels() throws TransformationException {
		Map<String, Object> models = new HashMap<String, Object>();

		if (jwtMetamodel == null) {
			try {
				createResources();
				jwtMetamodel = (ASMEMFModel) modelLoader.loadModel(
						"jwt", modelLoader.getMOF(), JWT_ModelResource);//$NON-NLS-1$
				cmisMetamodel = (ASMEMFModel) modelLoader.loadModel(
						"cmismodel", modelLoader.getMOF(), CMIS_ModelResource);//$NON-NLS-1$
			} catch (IOException ex) {
				throw new TransformationException(ex.getMessage(), ex);
			}
		}
		models.put("JWT", jwtMetamodel);//$NON-NLS-1$
		models.put("CMIS", cmisMetamodel);//$NON-NLS-1$
		return models;
	}

	@Override
	public void transform(String inFilePath, String outFilePath)
			throws IOException, TransformationException {
		final URI OUT_RESOURCE_URI = URI.createFileURI("thisisanURI");
		try {
			Map<String, Object> models = getMetamodels();

			// get/create models
			ASMEMFModel jwtInputModel = (ASMEMFModel) modelLoader.loadModel(
					"IN", jwtMetamodel, new FileInputStream(inFilePath));//$NON-NLS-1$
			ASMEMFModel cmisOutputModel = (ASMEMFModel) modelLoader.newModel(
					"OUT", OUT_RESOURCE_URI.toFileString(), cmisMetamodel);//$NON-NLS-1$
			// load models
			models.put("IN", jwtInputModel);//$NON-NLS-1$
			models.put("OUT", cmisOutputModel);//$NON-NLS-1$
			// launch
			ExportToImageUtil.setupVM(cmisMetamodel);
			AtlLauncher.getDefault().launch(this.JWT2CMIS_TransfoResource,
					Collections.EMPTY_MAP, models, Collections.EMPTY_MAP,
					Collections.EMPTY_LIST, Collections.EMPTY_MAP);


			Map<String, String> sessionParameters = cmisConfiguration
					.getSessionParameters();

			modelLoader.save(cmisOutputModel, outFilePath); // TODO remove (debug)
			performSync(cmisOutputModel, sessionParameters);

		} catch (SynchronizationException e) {
			throw new TransformationException(
					"Synchronization failed.\nCheck your remote repository: is there a\n'/default-domain/workspaces/demo' folder?");
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} catch (TransformationException e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void performSync(ASMModel cmisModel,
			Map<String, String> sessionParameters) throws IOException,
			SynchronizationException {
		DmsSynchronizer synchronizer = new CmisDmsSynchronizer(
				new DemoMatchingStrategy(), new DemoSynchronizationStrategy(),
				sessionParameters);

		// Load the transformation output in the synchronizer
		File tempFile = File.createTempFile("dmsSync", "");
		String tempFileHref = tempFile.toURI().toURL().toString();
		modelLoader.save(cmisModel, tempFileHref);
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.createResource(URI
				.createFileURI(tempFile.getAbsolutePath()));
		resource.load(null);

		synchronizer.synchronize(resource);
	}

}
