-- @atlcompiler atl2006
-- @path CMIS=/jwt-ecm-sync-cmis-model/src/org/eclipse/jwt/ecm/sync/cmis/ecore/CMISSyncModel.ecore
-- @path JWT=/jwt-metamodel/src/org/eclipse/jwt/meta/ecore/JWTMetaModel.ecore

module jwt2cmis;
create OUT : CMIS from IN : JWT;

helper context JWT!Package def: canonicalName(): String =
	if self.superpackage.oclIsUndefined() then
		self.name
	else
		self.superpackage.canonicalName() + '.' + self.name
	endif;

helper context JWT!PackageableElement def: canonicalName(): String =
	self.package.canonicalName() + '.' + self.name;

rule Model2SyncRoot {
	from
		s: JWT!Model
	to
		thisModelsRoot: CMIS!SyncFolder (
			id <- s.canonicalName(),
			properties <- thisModelsRoot.properties.append(typeProperty1),
			properties <- thisModelsRoot.properties.append(nameProperty),
			parents <- Sequence{demoRoot}
			),
		nameProperty: CMIS!StringToObjectMapEntry(key <- 'cmis:name', value <- s.name),
		typeProperty1: CMIS!StringToObjectMapEntry(key <- 'cmis:objectTypeId', value <- 'cmis:folder'),
		demoRoot: CMIS!SyncFolder (
			id <- 'demoRoot'
		)
}

rule Activity2Document {
	from
		s: JWT!Activity
	to
		t: CMIS!SyncDocument (
			id <- s.canonicalName(),
			parents <- JWT!Model.allInstances(),
			properties <- t.properties.append(typeProperty),
			properties <- t.properties.append(nameProperty),
			contentStream <- thisModule.exportToSvg(s)
			),
		typeProperty: CMIS!StringToObjectMapEntry(key <- 'cmis:objectTypeId', value <- 'cmis:document'),
		nameProperty: CMIS!StringToObjectMapEntry(key <- 'cmis:name', value <- s.name)
}
