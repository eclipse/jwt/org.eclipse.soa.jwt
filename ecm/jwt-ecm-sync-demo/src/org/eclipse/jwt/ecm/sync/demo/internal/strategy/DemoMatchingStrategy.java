package org.eclipse.jwt.ecm.sync.demo.internal.strategy;

import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.eclipse.jwt.ecm.sync.cmis.SyncUtils;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder;
import org.eclipse.jwt.ecm.sync.cmis.strategy.nuxeo.NuxeoCustomIdPropertyMatchingStrategy;
import org.eclipse.jwt.ecm.sync.demo.internal.Constants;

public class DemoMatchingStrategy extends NuxeoCustomIdPropertyMatchingStrategy {

	public DemoMatchingStrategy() {
		super(Constants.DEMO_CMIS_ID_PROPERTY_NAME);
	}

	@Override
	public Folder findSynchronizationRoot(SyncFolder localFolder,
			Session session) {
		return SyncUtils.safeCast(session.getObjectByPath("/default-domain/workspaces/demo"), Folder.class);
	}
}
