package org.eclipse.jwt.ecm.sync.demo.internal;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;

public class InputDialogCmisConfiguration implements CmisConfiguration {

	public Map<String, String> getSessionParameters() throws IOException {
		String strServerURL = getServerURL();
		Map<String, String> sessionParameters = new HashMap<String, String>();

		// Server URL parsing
		URL serverURL = new URL(strServerURL);
		String userInfo[] = serverURL.getUserInfo().split(":");
		if (userInfo.length != 2 || userInfo[0] == null || userInfo[1] == null) {
			throw new MalformedURLException(
					"Bad user info. This should be something like: 'http://user:password@host:port/path/to/file");
		}
		URL atompubURL = new URL(serverURL.getProtocol(), serverURL.getHost(),
				serverURL.getPort(), serverURL.getFile());

		// user credentials
		sessionParameters.put(SessionParameter.USER, userInfo[0]);
		sessionParameters.put(SessionParameter.PASSWORD, userInfo[1]);
		// connection settings
		sessionParameters.put(SessionParameter.BINDING_TYPE,
				BindingType.ATOMPUB.value());
		sessionParameters.put(SessionParameter.ATOMPUB_URL,
				atompubURL.toString());
		sessionParameters.put(SessionParameter.REPOSITORY_ID, "default");

		return sessionParameters;
	}

	private String getServerURL() throws IOException {
		InputDialog dialog = new InputDialog(
				null,
				"Server information",
				"Please enter the AtomPub endpoint URL. It must have the same format as this example:\n  'http://Administrator:Administrator@localhost:8080/nuxeo/atom/cmis'",
				"http://Administrator:Administrator@localhost:8080/nuxeo/atom/cmis",
				null);
		dialog.setBlockOnOpen(true);
		dialog.open();

		if (dialog.getReturnCode() == Window.OK)
			return dialog.getValue();

		throw new IOException("No server URL provided" + dialog.getReturnCode());
	}
}
