package org.eclipse.jwt.ecm.sync.demo.internal.strategy;

import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncObject;
import org.eclipse.jwt.ecm.sync.cmis.strategy.ExactReplicaSynchronizationStrategy;
import org.eclipse.jwt.ecm.sync.demo.internal.Constants;

public class DemoSynchronizationStrategy extends
		ExactReplicaSynchronizationStrategy {
	
	@Override
	public void alterCreatedProperties(SyncObject localObject,
			Map<String, Object> newProperties) {
		super.alterCreatedProperties(localObject, newProperties);
		newProperties.put(Constants.DEMO_CMIS_ID_PROPERTY_NAME,
				localObject.getId());
	}

	@Override
	public void alterUpdatedProperties(SyncObject localObject,
			CmisObject remoteObject, Map<String, Object> updatedProperties) {
		super.alterUpdatedProperties(localObject, remoteObject,
				updatedProperties);
		updatedProperties.put(Constants.DEMO_CMIS_ID_PROPERTY_NAME,
				localObject.getId());
	}
}
