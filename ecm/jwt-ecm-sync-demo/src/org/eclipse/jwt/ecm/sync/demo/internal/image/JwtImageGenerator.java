package org.eclipse.jwt.ecm.sync.demo.internal.image;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerImpl;
import org.eclipse.jwt.we.commands.editdomain.GefEmfEditingDomain;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.parts.JWTEditPartFactory;

public abstract class JwtImageGenerator {
	private EObject activity;
	private final EReference ACTIVITY_PACKAGE;
	private final EReference PACKAGE_SUPERPACKAGE;

	public JwtImageGenerator(EObject activity) {
		Assert.isNotNull(activity);
		this.activity = activity;

		// Init structural features
		EClass activityClass = activity.eClass();
		this.ACTIVITY_PACKAGE = (EReference) activityClass
				.getEStructuralFeature("package");
		EClass packageClass = ACTIVITY_PACKAGE.getEReferenceType();
		this.PACKAGE_SUPERPACKAGE = (EReference) packageClass
				.getEStructuralFeature("superpackage");
	}

	public abstract String getMimeType();

	public abstract byte[] getContent();

	protected EObject getModel() {
		EObject packageObject = (EObject) activity.eGet(ACTIVITY_PACKAGE);
		return getRoot(packageObject);
	}

	private EObject getRoot(EObject packageObject) {
		EObject superPackage = (EObject) packageObject
				.eGet(PACKAGE_SUPERPACKAGE);
		if (superPackage == null) {
			return packageObject;
		} else {
			return getRoot(superPackage);
		}
	}

	/*
	 * (non-javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.misc.wizards.view.pages.PreviewViewConfWizardPage#
	 * loadPreview()
	 */
	protected GraphicalViewer createGraphicalViewer() {
		// TODO reuse an existing GraphicalViewer thanks to an instance of WEEditor?
		
		GraphicalViewerImpl gv = new GraphicalViewerImpl();
		gv.setRootEditPart(new ScalableFreeformRootEditPart());
		gv.setEditPartFactory(new JWTEditPartFactory());
		gv.setEditDomain(new GefEmfEditingDomain(null));
		gv.setContents(activity);
		ScalableFreeformRootEditPart rootEditPart = (ScalableFreeformRootEditPart) gv
				.getRootEditPart();
		rootEditPart.getFigure().setBackgroundColor(
				PreferenceReader.appearanceEditorColor.get());

		return gv;
	}
}
