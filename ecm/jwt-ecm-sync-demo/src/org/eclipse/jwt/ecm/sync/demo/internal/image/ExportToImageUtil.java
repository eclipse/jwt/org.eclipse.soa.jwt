package org.eclipse.jwt.ecm.sync.demo.internal.image;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModel;
import org.eclipse.m2m.atl.drivers.emf4atl.ASMEMFModelElement;
import org.eclipse.m2m.atl.engine.vm.ATLVMTools;
import org.eclipse.m2m.atl.engine.vm.NativeStackFrame;
import org.eclipse.m2m.atl.engine.vm.Operation;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModelElement;
import org.eclipse.m2m.atl.engine.vm.nativelib.ASMModule;

public class ExportToImageUtil {
	private static ASMEMFModelElement contentStreamClass = null;

	/**
	 * Adds export methods to "Module" objects.
	 * <p>
	 * This registers appropriate methods in the ATL VM in order to make exports
	 * methods available in ATL file. After a call to this method, the following
	 * calls are valid in launched ATL modules:
	 * <ul>
	 * <li>thisModule.exportToSvg(activity) -- 'activity' being a JWT Activity
	 * EMF object
	 * </ul>
	 * They return a fully configured SyncContentStream EMF object.
	 * 
	 * @param cmisMetamodel
	 *            The CMIS metamodel used to instantiate the SyncContentStream
	 *            objects returned by the calls.
	 */
	public static void setupVM(ASMEMFModel cmisMetamodel) {
		contentStreamClass = (ASMEMFModelElement) cmisMetamodel
				.findModelElement("SyncContentStream");
		Assert.isLegal(contentStreamClass != null,
				"cmisMetamodel must define a 'SyncContentStream' class.");

		Operation op = ATLVMTools.toVMOperation(ExportToImageUtil.class,
				"exportToSvg");
		ATLVMTools.addVMOperation(ASMModule.myType, op);
	}

	public static ASMModelElement exportToSvg(NativeStackFrame frame,
			ASMModule self, ASMEMFModelElement jwtActivity) {
		return generateContentStream(frame, self, new JwtSvgGenerator(
				jwtActivity.getObject()));
	}

	private static ASMModelElement generateContentStream(
			NativeStackFrame frame, ASMModule self,
			JwtImageGenerator jwtImageGenerator) {

		ASMEMFModelElement modelElement = (ASMEMFModelElement) ASMEMFModelElement
				.newInstance(frame, contentStreamClass);

		EObject object = modelElement.getObject();
		final EClass streamClass = object.eClass();
		final EStructuralFeature mimeFeature = streamClass
				.getEStructuralFeature("mimeType");
		final EStructuralFeature contentFeature = streamClass
				.getEStructuralFeature("content");

		object.eSet(mimeFeature, jwtImageGenerator.getMimeType());
		object.eSet(contentFeature, jwtImageGenerator.getContent());

		return modelElement;
	}
}
