package org.eclipse.jwt.ecm.sync.cmis.strategy.nuxeo;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder;
import org.eclipse.jwt.ecm.sync.cmis.strategy.IdBasedMatchingStrategy;

/**
 * A MatchingStrategy assuming that local IDs are defined in the editor and
 * refer to a certain property in a Nuxeo instance.
 * 
 * @author yrodiere
 * 
 */
public class NuxeoCustomIdPropertyMatchingStrategy extends IdBasedMatchingStrategy {

	private final static EStructuralFeature syncIdFeature = SyncModelPackage.eINSTANCE
			.getSyncItem_Id();
	final private String idPropertyName;

	public NuxeoCustomIdPropertyMatchingStrategy(String idPropertyName) {
		super();
		Assert.isLegal(idPropertyName != null && !idPropertyName.isEmpty());
		// TODO check for SQL injection issues?
		this.idPropertyName = idPropertyName;
	}

	@Override
	protected String extractId(CmisObject remoteObject) {
		return remoteObject.getPropertyValue(idPropertyName);
	}

	@Override
	protected String buildDocumentWhereClause(SyncDocument localObject) {
		// TODO handle null value
		String wantedId = (String) localObject.eGet(syncIdFeature);

		/*
		 * Nuxeo-specific code: we need to get the object representing the
		 * "version series", and not the latest version. Only the "version
		 * series" seems to be changeable.
		 */
		String whereClause = String.format("%s = '%s' AND %s = false",
				idPropertyName, wantedId, "nuxeo:isVersion");

		return whereClause;
	}

	@Override
	protected String buildFolderWhereClause(SyncFolder localObject) {
		// TODO handle null value
		String wantedId = (String) localObject.eGet(syncIdFeature);

		/*
		 * Nuxeo-specific code: on contrary to files, here we need to get the
		 * object representing the latest version, and not the "version series".
		 * Only the "version series" seems to be changeable.
		 */
		String whereClause = String.format(
				"%s = '%s' AND cmis:parentId IS NOT NULL", idPropertyName,
				wantedId);

		return whereClause;
	}

	@Override
	protected String buildSynchronizationRootWhereClause(SyncFolder localObject) {
		return buildFolderWhereClause(localObject);
	}
}
