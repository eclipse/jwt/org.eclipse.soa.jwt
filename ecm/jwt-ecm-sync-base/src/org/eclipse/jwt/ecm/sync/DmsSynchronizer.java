package org.eclipse.jwt.ecm.sync;

import org.eclipse.emf.ecore.resource.Resource;

/**
 * A simple interface for objects synchronizing EMF Resources with Document
 * Management Systems (DMS)
 * 
 */
public interface DmsSynchronizer {
	public void synchronize(Resource resource) throws SynchronizationException;
}
