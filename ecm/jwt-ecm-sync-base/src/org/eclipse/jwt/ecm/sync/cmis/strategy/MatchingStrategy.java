package org.eclipse.jwt.ecm.sync.cmis.strategy;

import org.apache.chemistry.opencmis.client.api.FileableCmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Relationship;
import org.apache.chemistry.opencmis.client.api.Session;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jwt.ecm.sync.SynchronizationException;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship;

public interface MatchingStrategy {
	/**
	 * Finds a "matching" remote object for the given {@link SyncFileableObject}
	 * .
	 * <p>
	 * The meaning of "matching" depends on the implementation, but the
	 * <code>cmis:objectTypeId</code> property of the returned object must be
	 * the same as <code>localObject</code>'s.
	 * 
	 * @param localObject
	 *            A non-null local object whose matching remote copy has to be
	 *            found. This object MAY be the model root (i.e.
	 *            <code>localObject.getParent()</code> MAY be null), and this
	 *            special case is to be handled by the MatchingStrategy.
	 * @param session
	 * @return The remote document which should be synchronized with
	 *         <code>localObject</code>, or null if not found.
	 */
	public FileableCmisObject findMatchingRemote(
			SyncFileableObject localObject, Session session);

	/**
	 * Finds a "matching" {@link SyncFileableObject} for the given remote
	 * object.
	 * <p>
	 * The meaning of "matching" depends on the implementation, but the
	 * <code>cmis:objectTypeId</code> property of the returned object must be
	 * the same as <code>remoteObject</code>'s.
	 * 
	 * @param remoteObject
	 * @param localResource
	 * @return The local object which should be synchronized with
	 *         <code>remoteObject</code>, or null if not found.
	 */
	public SyncFileableObject findMatchingLocal(
			FileableCmisObject remoteObject, Resource localResource);

	/**
	 * Finds a matching remote folder for the given synchronization root.
	 * 
	 * @return A non-null remote Folder
	 * @throws SynchronizationException When no matching remote folder could be found.
	 */
	public Folder findSynchronizationRoot(SyncFolder localFolder, Session session);
}
