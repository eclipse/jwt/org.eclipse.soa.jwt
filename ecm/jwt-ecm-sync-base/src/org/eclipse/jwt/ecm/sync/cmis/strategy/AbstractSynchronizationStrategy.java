package org.eclipse.jwt.ecm.sync.cmis.strategy;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.FileableCmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.eclipse.jwt.ecm.sync.SynchronizationException;
import org.eclipse.jwt.ecm.sync.cmis.CmisDmsSynchronizer;
import org.eclipse.jwt.ecm.sync.cmis.CmisDmsSynchronizer.SynchronizationPerformer;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder;

public abstract class AbstractSynchronizationStrategy implements
		SynchronizationStrategy {
	private SynchronizationPerformer performer = null;

	@Override
	public void setPerformer(SynchronizationPerformer performer) {
		this.performer = performer;
	}

	protected SynchronizationPerformer getPerformer()
			throws SynchronizationException {
		if (this.performer == null)
			throw new SynchronizationException(
					"Attempt to perform synchronization with a null SynchronizationPerformer");

		return this.performer;
	}

	@Override
	public FileableCmisObject newObject(Folder remoteFolder,
			SyncFolder localFolder, SyncFileableObject localObject)
			throws SynchronizationException {
		if (localObject instanceof SyncDocument) {
			return newObject(remoteFolder, localFolder,
					(SyncDocument) localObject);
		} else if (localObject instanceof SyncFolder) {
			return newObject(remoteFolder, localFolder,
					(SyncFolder) localObject);
		} else {
			throw new IllegalArgumentException(
					"Cannot handle 'newObject' event for type "
							+ localObject.getClass());
		}
	}

	protected abstract Document newObject(Folder remoteFolder,
			SyncFolder localFolder, SyncDocument localObject)
			throws SynchronizationException;

	protected abstract Folder newObject(Folder remoteFolder,
			SyncFolder localFolder, SyncFolder localObject)
			throws SynchronizationException;

	@Override
	public void deletedObject(Folder remoteFolder, SyncFolder localFolder,
			FileableCmisObject remoteObject) throws SynchronizationException {
		if (remoteObject instanceof Document) {
			deletedObject(remoteFolder, localFolder, (Document) remoteObject);
		} else if (remoteObject instanceof Folder) {
			deletedObject(remoteFolder, localFolder, (Folder) remoteObject);
		} else {
			throw new IllegalArgumentException(
					"Cannot handle 'removedObject' event for type "
							+ remoteObject.getClass());
		}
	}

	protected abstract void deletedObject(Folder remoteFolder,
			SyncFolder localFolder, Document remoteObject)
			throws SynchronizationException;

	protected abstract void deletedObject(Folder remoteFolder,
			SyncFolder localFolder, Folder remoteObject)
			throws SynchronizationException;

	@Override
	public FileableCmisObject match(Folder remoteFolder,
			SyncFolder localFolder, FileableCmisObject remoteObject,
			SyncFileableObject localObject) throws SynchronizationException {
		try {
			return match(remoteFolder, localFolder, (Document) remoteObject,
					(SyncDocument) localObject);
		} catch (ClassCastException e1) {
			try {
				return match(remoteFolder, localFolder, (Folder) remoteObject,
						(SyncFolder) localObject);
			} catch (ClassCastException e2) {
				throw new IllegalArgumentException(
						"Cannot handle 'match' event for remote object type "
								+ remoteObject.getClass()
								+ " and local object type "
								+ localObject.getClass());
			}
		}
	}

	protected abstract FileableCmisObject match(Folder remoteFolder,
			SyncFolder localFolder, Document remoteObject,
			SyncDocument localObject) throws SynchronizationException;

	protected abstract FileableCmisObject match(Folder remoteFolder,
			SyncFolder localFolder, Folder remoteObject, SyncFolder localObject)
			throws SynchronizationException;
}
