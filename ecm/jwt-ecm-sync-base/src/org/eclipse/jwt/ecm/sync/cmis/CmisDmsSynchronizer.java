package org.eclipse.jwt.ecm.sync.cmis;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.FileableCmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.Relationship;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.UnfileObject;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisBaseException;
import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jwt.ecm.sync.DmsSynchronizer;
import org.eclipse.jwt.ecm.sync.SynchronizationException;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncContentStream;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncModelPackage;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship;
import org.eclipse.jwt.ecm.sync.cmis.strategy.MatchingStrategy;
import org.eclipse.jwt.ecm.sync.cmis.strategy.SynchronizationStrategy;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

import static org.eclipse.jwt.ecm.sync.cmis.SyncUtils.*;

/* TODO don't synchronize relationships immediately, since they introduce
 * cycles in the object graph (which induces circular dependencies)
 */
public class CmisDmsSynchronizer implements DmsSynchronizer {

	private final Map<String, String> sessionParameters;
	private final SynchronizationStrategy syncStrategy;
	private final MatchingStrategy matchingStrategy;
	private final SessionFactory sessionFactory;

	private Resource resource;
	private Session session;
	private SynchronizationAnalyzer syncAnalyzer;
	private SynchronizationPerformerImpl syncPerformer;

	public CmisDmsSynchronizer(MatchingStrategy matchingStrategy,
			SynchronizationStrategy syncStrategy,
			Map<String, String> cmisSessionParameters) {
		super();
		Assert.isLegal(syncStrategy != null);
		Assert.isLegal(matchingStrategy != null);
		Assert.isLegal(cmisSessionParameters != null);

		this.syncStrategy = syncStrategy;
		this.matchingStrategy = matchingStrategy;
		this.sessionParameters = cmisSessionParameters;

		BundleContext context = FrameworkUtil.getBundle(this.getClass())
				.getBundleContext();
		ServiceReference<?> serviceReference = context
				.getServiceReference(SessionFactory.class.getName());
		this.sessionFactory = (SessionFactory) context
				.getService(serviceReference);
		Assert.isNotNull(this.sessionFactory,
				"Could not retrieve an OpenCMIS session factory");
	}

	@Override
	public final void synchronize(Resource resource)
			throws SynchronizationException {
		Assert.isNotNull(resource);

		this.resource = resource;
		// TODO set all "synchronized" attributes to false beforehand?

		try {
			setup();
			for (EObject localObject : resource.getContents()) {
				syncAnalyzer.synchronizeTreeForRoot(localObject);
			}
			for (EObject localObject : resource.getContents()) {
				syncAnalyzer.synchronizeRelationshipsForRoot(localObject);
			}
			teardown();
		} catch (CmisBaseException e) {
			throw new SynchronizationException(e);
		} finally {
			this.resource = null;
		}
	}

	private void setup() {
		session = sessionFactory.createSession(sessionParameters);
		syncAnalyzer = new SynchronizationAnalyzer();
		syncPerformer = new SynchronizationPerformerImpl();
		syncStrategy.setPerformer(syncPerformer);
	}

	private void teardown() {
		syncStrategy.setPerformer(null);
		syncPerformer = null;
		syncAnalyzer = null;
		session = null;
	}

	public interface SynchronizationPerformer {

		public Document create(Folder remoteParent, SyncDocument localObject)
				throws SynchronizationException;

		public Folder create(Folder remoteParent, SyncFolder localObject)
				throws SynchronizationException;

		public Relationship addRelationship(SyncRelationship localObject,
				CmisObject remoteSource, CmisObject remoteTarget)
				throws SynchronizationException;

		public void addToFolder(FileableCmisObject object, Folder newParent)
				throws SynchronizationException;

		public void removeFromFolder(FileableCmisObject object, Folder newParent)
				throws SynchronizationException;

		public Document update(Document remoteObject, SyncDocument localObject)
				throws SynchronizationException;

		public Folder update(Folder remoteObject, SyncFolder localObject)
				throws SynchronizationException;

		public Relationship update(Relationship remoteObject,
				SyncRelationship localObject) throws SynchronizationException;

		public void delete(Document remoteObject)
				throws SynchronizationException;

		public void delete(Folder remoteObject) throws SynchronizationException;

		public void removeRelationship(Relationship remoteObject)
				throws SynchronizationException;
	}

	private class SynchronizationAnalyzer {
		private Folder getSynchronizationRoot(EObject localObject)
				throws SynchronizationException {
			if (!(localObject instanceof SyncFolder)) {
				// Not a folder, thus not a root
				return null;
			}

			SyncFolder localFolder = (SyncFolder) localObject;

			if (!localFolder.getParents().isEmpty()) {
				// Has a parent, thus not a root
				return null;
			}

			Folder remoteFolder = matchingStrategy.findSynchronizationRoot(
					localFolder, session);

			if (remoteFolder == null) {
				throw new SynchronizationException(
						"Could not find a matching remote folder for synchronization root "
								+ localFolder.getId());
			}

			return remoteFolder;
		}

		public void synchronizeTreeForRoot(EObject localObject)
				throws SynchronizationException {
			Folder remoteSyncRoot = getSynchronizationRoot(localObject);

			if (remoteSyncRoot != null) {
				synchronizeChildLinks(remoteSyncRoot, (SyncFolder) localObject);
			}
		}

		/*
		 * TODO: split into several methods. This is UGLY.
		 */
		public void synchronizeChildLinks(Folder remoteFolder,
				SyncFolder localFolder) throws SynchronizationException {
			List<SyncFileableObject> newChildren = new ArrayList<SyncFileableObject>();
			Map<String, SyncFileableObject> matchingLocalChildren = new HashMap<String, SyncFileableObject>();
			Map<String, FileableCmisObject> matchingRemoteChildren = new HashMap<String, FileableCmisObject>();

			for (SyncFileableObject localChild : localFolder.getChildren()) {
				FileableCmisObject remoteChild = matchingStrategy
						.findMatchingRemote(localChild, session);
				localChild.setSynchronized(true);

				if (remoteChild == null) {
					/* This object does not exist remotely */
					newChildren.add(localChild);
				} else {
					matchingLocalChildren.put(remoteChild.getId(), localChild);
					matchingRemoteChildren
							.put(remoteChild.getId(), remoteChild);
				}
			}

			remoteFolder.refresh();
			Iterator<CmisObject> iterator = remoteFolder.getChildren()
					.iterator();
			while (iterator.hasNext()) {
				FileableCmisObject remoteChild = (FileableCmisObject) iterator
						.next();
				SyncFileableObject matchingLocal = matchingLocalChildren
						.remove(remoteChild.getId());

				if (matchingLocal != null) {
					/*
					 * The object is a child of the current folder both locally
					 * and remotely.
					 */
					matchingLocal.setSynchronized(true);
					syncStrategy.match(remoteFolder, localFolder, remoteChild,
							matchingLocal);
				} else {
					/* The object is only a child of this folder remotely */
					matchingLocal = matchingStrategy.findMatchingLocal(
							remoteChild, resource);

					if (matchingLocal != null) {
						/*
						 * A matching local object exists, but it is not a child
						 * of this folder
						 */
						syncStrategy.removedChildLink(remoteFolder,
								localFolder, remoteChild, matchingLocal);
					} else {
						/* This object does not exist locally */
						syncStrategy.deletedObject(remoteFolder, localFolder,
								remoteChild);
					}
				}
			}

			for (Map.Entry<String, SyncFileableObject> localChildEntry : matchingLocalChildren
					.entrySet()) {
				/*
				 * This local object has a matching remote copy, but the remote
				 * copy is not a child of this folder
				 */
				syncStrategy.newChildLink(remoteFolder, localFolder,
						matchingRemoteChildren.get(localChildEntry.getKey()),
						localChildEntry.getValue());
			}

			for (SyncFileableObject localChild : newChildren) {
				/* This object does not exist remotely */
				syncStrategy.newObject(remoteFolder, localFolder, localChild);
			}
		}

		public void synchronizeRelationshipsForRoot(EObject localObject)
				throws SynchronizationException {
			Folder remoteSyncRoot = getSynchronizationRoot(localObject);

			if (remoteSyncRoot != null) {
				synchronizeRelationships(remoteSyncRoot,
						(SyncFolder) localObject);
			}
		}

		private void synchronizeRelationships(FileableCmisObject remoteObject,
				SyncFileableObject localObject) {

			// TODO sync relationships for object

			if (SyncModelPackage.eINSTANCE.getSyncFolder().isSuperTypeOf(
					localObject.eClass())) {
				// TODO sync relationships for children
			}
		}

		public Relationship synchronize(SyncRelationship localObject)
				throws SynchronizationException {
			throw new UnsupportedOperationException("Not implemented yet");

			// Assert.isNotNull(localObject);
			//
			// Relationship remoteObject = matchingStrategy.findMatchingRemote(
			// localObject, session);
			//
			// if (localObject.isSynchronized()) {
			// // This returns null if the matching object was
			// // deleted during synchronization
			// return remoteObject;
			// }
			//
			// localObject.setSynchronized(true);
			//
			// if (remoteObject == null) {
			// remoteObject = syncStrategy.newObject(localObject);
			// } else {
			// remoteObject = syncStrategy.match(remoteObject, localObject);
			// }
			//
			// return remoteObject;
		}

		public void synchronize(Relationship remoteObject)
				throws SynchronizationException {
			throw new UnsupportedOperationException("Not implemented yet");
			// Assert.isNotNull(remoteObject);
			//
			// SyncRelationship localObject = matchingStrategy
			// .findMatchingLocal(remoteObject, resource);
			//
			// if (localObject == null) {
			// syncStrategy.localMissing(remoteObject);
			// } else if (!localObject.isSynchronized()) {
			// localObject.setSynchronized(true);
			// syncStrategy.match(remoteObject, localObject);
			// }
		}
	}

	private class SynchronizationPerformerImpl implements
			SynchronizationPerformer {

		private <ReturnType> ReturnType retrieveCreatedObject(ObjectId id,
				Class<ReturnType> expectedType) throws SynchronizationException {
			CmisObject createdObject = session.getObject(id);
			if (createdObject != null)
				return safeCast(createdObject, expectedType);

			// TODO handle creation failure
			throw new SynchronizationException("CMIS object creation FAILED");
		}

		private Map<String, Object> createPropagatedProperties(
				SyncObject localObject) {
			EMap<String, Object> localObjectProps = localObject.getProperties();
			Assert.isNotNull(localObjectProps);

			return new HashMap<String, Object>(localObjectProps.map());
		}

		private ContentStream createPropagatedContentStream(
				SyncDocument localObject) {
			ContentStream propagatedStream = null;
			SyncContentStream localStream = localObject.getContentStream();

			if (localStream != null) {
				propagatedStream = session.getObjectFactory()
						.createContentStream(localStream.getFileName(),
								localStream.getLength(),
								localStream.getMimeType(),
								new ByteArrayInputStream(localStream.getContent()));
			}

			return propagatedStream;
		}

		@Override
		public final Document create(Folder remoteParent,
				SyncDocument localObject) throws SynchronizationException {
			final Map<String, Object> newProperties = createPropagatedProperties(localObject);
			syncStrategy.alterCreatedProperties(localObject, newProperties);

			ObjectId createdObjectId = null;
			try {
				ContentStream newStream = createPropagatedContentStream(localObject);

				// TODO choose versioning state
				createdObjectId = session.createDocument(newProperties,
						remoteParent, newStream, VersioningState.MINOR);
			} catch (CmisBaseException e) {
				throw new SynchronizationException(e);
			}

			// TODO relationships (AFTER synchronizing the tree)

			return retrieveCreatedObject(createdObjectId, Document.class);
		}

		@Override
		public final Folder create(Folder remoteParent, SyncFolder localObject)
				throws SynchronizationException {
			if (remoteParent == null) {
				throw new SynchronizationException(
						"Cannot create a folder without parent; a folder MUST be filed");
			}

			final Map<String, Object> newProperties = createPropagatedProperties(localObject);
			syncStrategy.alterCreatedProperties(localObject, newProperties);

			ObjectId createdObjectId = null;
			try {
				createdObjectId = session.createFolder(newProperties,
						remoteParent);
			} catch (CmisBaseException e) {
				throw new SynchronizationException(e);
			}

			Folder createdFolder = retrieveCreatedObject(createdObjectId,
					Folder.class);

			syncAnalyzer.synchronizeChildLinks(createdFolder, localObject);

			// TODO relationships (AFTER synchronizing the tree)

			return createdFolder;
		}

		@Override
		public final Relationship addRelationship(SyncRelationship localObject,
				CmisObject remoteSource, CmisObject remoteTarget) {
			// TODO Auto-generated method stub
			throw new UnsupportedOperationException("Not implemented yet");

			// final Map<String, Object> newProperties =
			// getPropagatedProperties(localObject);
			// CmisObject remoteTarget = synchronize(localObject.getTarget());
			// CmisObject remoteSource = synchronize(localObject.getSource());
			//
			// if (remoteTarget == null || remoteSource == null) {
			// // Cannot create a relationship with a non-existent object
			// return null;
			// }
			//
			// newProperties.put(PropertyIds.SOURCE_ID, remoteSource.getId());
			// newProperties.put(PropertyIds.TARGET_ID, remoteTarget.getId());
			//
			// alterCreatedProperties(localObject, newProperties);
			//
			// ObjectId id = session.createRelationship(newProperties);
			//
			// return returnCreatedObject(id, Relationship.class);
		}

		private <ReturnType> ReturnType update(CmisObject remoteObject,
				SyncObject localObject, Class<ReturnType> expectedReturnType)
				throws SynchronizationException {
			// Synchronize properties
			Map<String, Object> newProperties = createPropagatedProperties(localObject);
			syncStrategy.alterUpdatedProperties(localObject, remoteObject,
					newProperties);
			if (!newProperties.isEmpty()) {
				// TODO handle refresh. After this line, "remoteObject"'s
				// properties are not updated, but the real remote
				// properties are.
				try {
					remoteObject = remoteObject.updateProperties(newProperties);
				} catch (CmisBaseException e) {
					throw new SynchronizationException(e);
				}
			}

			// TODO Sync relationships, and more?

			/*
			 * We must ensure the returned object still has the same concrete
			 * type, since an ECM can change the underlying object when updating
			 * its properties
			 */
			return safeCast(remoteObject, expectedReturnType);
		}

		@Override
		public final Document update(Document remoteObject,
				SyncDocument localObject) throws SynchronizationException {
			// Update everything common to any Object sub-types
			remoteObject = update(remoteObject, localObject, Document.class);

			ContentStream propagatedStream = createPropagatedContentStream(localObject);

			// TODO refresh?
			if (propagatedStream != null) {
				// TODO allow to parameterize: overwrite
				remoteObject.setContentStream(propagatedStream, true);
			} else {
				remoteObject.deleteContentStream();
			}

			// TODO Sync more?

			return remoteObject;
		}

		@Override
		public final Folder update(Folder remoteObject, SyncFolder localObject)
				throws SynchronizationException {
			// Update everything common to any Object sub-types
			remoteObject = update(remoteObject, localObject, Folder.class);

			syncAnalyzer.synchronizeChildLinks(remoteObject, localObject);

			/*
			 * TODO refresh the CmisObject? It may have been modified, since
			 * children may have been added.
			 */

			// TODO Sync more?

			return remoteObject;
		}

		@Override
		public final Relationship update(Relationship remoteObject,
				SyncRelationship localObject) throws SynchronizationException {
			// TODO Auto-generated method stub
			throw new UnsupportedOperationException("Not implemented yet");

			// Update everything common to any Object sub-types
			// remoteObject = updateRemote(remoteObject, localObject,
			// Relationship.class);
		}

		@Override
		public final void delete(Document remoteObject)
				throws SynchronizationException {
			// TODO allow to parameterize: all versions
			try {
				remoteObject.delete(true /* all versions */);
			} catch (CmisBaseException e) {
				throw new SynchronizationException(e);
			}
		}

		@Override
		public final void delete(Folder remoteObject)
				throws SynchronizationException {
			// TODO allow to parameterize: "delete" or "deleteTree"
			// TODO allow to parameterize: all versions
			// TODO allow to parameterize: DELETE or UNFILE or DELETESINGLEFILED
			// TODO allow to parameterize: continue on failures
			try {
				remoteObject.deleteTree(true /* all versions */,
						UnfileObject.DELETE, false /* continue on failures */);
			} catch (CmisBaseException e) {
				throw new SynchronizationException(e);
			}
		}

		@Override
		public final void removeRelationship(Relationship remoteObject)
				throws SynchronizationException {
			// TODO Auto-generated method stub
			throw new UnsupportedOperationException("Not implemented yet");
		}

		@Override
		public void addToFolder(FileableCmisObject object, Folder newParent)
				throws SynchronizationException {
			// TODO Auto-generated method stub
			// CAUTION if the repository doesn't support multifiling, call
			// moveObject()
			throw new UnsupportedOperationException("Not implemented yet");
		}

		@Override
		public void removeFromFolder(FileableCmisObject object, Folder newParent)
				throws SynchronizationException {
			throw new UnsupportedOperationException("Not implemented yet");
			// CAUTION if the repository doesn't support unfiling and this
			// unfiles the object, throw an exception?
		}
	}
}
