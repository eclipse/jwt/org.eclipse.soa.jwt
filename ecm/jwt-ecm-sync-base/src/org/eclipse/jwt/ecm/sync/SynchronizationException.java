package org.eclipse.jwt.ecm.sync;

public class SynchronizationException extends Exception {
	private static final long serialVersionUID = -3227812680178334287L;

	public SynchronizationException() {
		super();
	}

	public SynchronizationException(String message, Throwable cause) {
		super(message, cause);
	}

	public SynchronizationException(String message) {
		super(message);
	}

	public SynchronizationException(Throwable cause) {
		super(cause);
	}
}
