package org.eclipse.jwt.ecm.sync.cmis;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncObject;

public class SyncUtils {

	public static <ExpectedType> ExpectedType safeCast(CmisObject object,
			Class<ExpectedType> expectedType) {
		try {
			return expectedType.cast(object);
		} catch (ClassCastException e) {
			throw new RuntimeException(
					"A CmisObject instance was found to have unexpected concrete type.",
					e);
		}
	}

	public static <ExpectedType> ExpectedType safeCast(EObject object,
			Class<ExpectedType> expectedType) {
		try {
			return expectedType.cast(object);
		} catch (ClassCastException e) {
			throw new RuntimeException(
					"An EObject instance was found to have unexpected concrete type.",
					e);
		}
	}
}
