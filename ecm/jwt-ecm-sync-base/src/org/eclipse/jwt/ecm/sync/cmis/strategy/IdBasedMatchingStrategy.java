package org.eclipse.jwt.ecm.sync.cmis.strategy;

import static org.eclipse.jwt.ecm.sync.cmis.SyncUtils.safeCast;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncObject;

public abstract class IdBasedMatchingStrategy extends AbstractMatchingStrategy {

	public IdBasedMatchingStrategy() {
		super();
	}

	protected abstract String extractId(CmisObject remoteObject);

	protected abstract String buildSynchronizationRootWhereClause(
			SyncFolder localObject);

	protected abstract String buildFolderWhereClause(SyncFolder localObject);

	protected abstract String buildDocumentWhereClause(SyncDocument localObject);

	public static CmisObject findMatchingRemoteImpl(Session session,
			SyncObject localObject, String whereClause) {
		// TODO handle null parameters
		// TODO handle null map and cast failure
		String wantedType = (String) localObject.getProperties().get(
				PropertyIds.OBJECT_TYPE_ID);

		OperationContext opContext = session.createOperationContext();

		ItemIterable<CmisObject> result = session.queryObjects(wantedType,
				whereClause, true, opContext);
		long numItems = result.getTotalNumItems();

		if (numItems > 1) {
			// Should not happen if the property is correctly generated
			throw new RuntimeException("Found multiple matching remote objects");
		} else if (numItems == 1) {
			return result.iterator().next();
		} else {
			return null;
		}
	}

	private EObject findMatchingLocalImpl(CmisObject remoteObject,
			Resource localResource) {
		// TODO handle null parameters
		String id = extractId(remoteObject);

		if (id == null || id.isEmpty())
			return null;

		EObject result = localResource.getEObject(id);

		return result;
	}

	@Override
	public Document findMatchingRemote(SyncDocument localObject, Session session) {
		return safeCast(
				findMatchingRemoteImpl(session, localObject,
						buildDocumentWhereClause(localObject)), Document.class);
	}

	@Override
	public Folder findMatchingRemote(SyncFolder localObject, Session session) {
		return safeCast(
				findMatchingRemoteImpl(session, localObject,
						buildFolderWhereClause(localObject)), Folder.class);
	}

	@Override
	protected SyncDocument findMatchingLocal(Document remoteObject,
			Resource localResource) {
		return safeCast(findMatchingLocalImpl(remoteObject, localResource),
				SyncDocument.class);
	}

	@Override
	protected SyncFolder findMatchingLocal(Folder remoteObject,
			Resource localResource) {
		return safeCast(findMatchingLocalImpl(remoteObject, localResource),
				SyncFolder.class);
	}

	public Folder findSynchronizationRoot(SyncFolder localFolder,
			Session session) {
		return safeCast(
				findMatchingRemoteImpl(session, localFolder,
						buildSynchronizationRootWhereClause(localFolder)), Folder.class);
	}

}