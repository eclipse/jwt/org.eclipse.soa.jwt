package org.eclipse.jwt.ecm.sync.cmis.strategy;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.FileableCmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder;

public abstract class AbstractMatchingStrategy implements MatchingStrategy {

	@Override
	public FileableCmisObject findMatchingRemote(
			SyncFileableObject localObject, Session session) {
		if (localObject instanceof SyncDocument) {
			return findMatchingRemote((SyncDocument) localObject, session);
		} else if (localObject instanceof SyncFolder) {
			return findMatchingRemote((SyncFolder) localObject, session);
		} else {
			throw new IllegalArgumentException(
					"Cannot find a remote object for type "
							+ localObject.getClass());
		}
	}

	protected abstract Document findMatchingRemote(SyncDocument localObject,
			Session session);

	protected abstract Folder findMatchingRemote(SyncFolder localObject,
			Session session);

	@Override
	public SyncFileableObject findMatchingLocal(
			FileableCmisObject remoteObject, Resource localResource) {
		if (remoteObject instanceof Document) {
			return findMatchingLocal((Document) remoteObject, localResource);
		} else if (remoteObject instanceof Folder) {
			return findMatchingLocal((Folder) remoteObject, localResource);
		} else {
			throw new IllegalArgumentException(
					"Cannot find a local object for type "
							+ remoteObject.getClass());
		}
	}

	protected abstract SyncDocument findMatchingLocal(Document remoteObject,
			Resource localResource);

	protected abstract SyncFolder findMatchingLocal(Folder remoteObject,
			Resource localResource);
}
