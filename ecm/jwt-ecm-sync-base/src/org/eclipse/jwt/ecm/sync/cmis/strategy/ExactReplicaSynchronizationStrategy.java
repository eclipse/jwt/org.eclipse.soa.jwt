package org.eclipse.jwt.ecm.sync.cmis.strategy;

import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.FileableCmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Relationship;
import org.eclipse.jwt.ecm.sync.SynchronizationException;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship;

public class ExactReplicaSynchronizationStrategy extends
		AbstractSynchronizationStrategy {

	@Override
	public void newChildLink(Folder remoteFolder, SyncFolder localFolder,
			FileableCmisObject remoteObject, SyncFileableObject localObject)
			throws SynchronizationException {
		match(remoteFolder, localFolder, remoteObject, localObject);
		getPerformer().addToFolder(remoteObject, remoteFolder);
	}

	@Override
	public void removedChildLink(Folder remoteFolder, SyncFolder localFolder,
			FileableCmisObject remoteObject, SyncFileableObject localObject)
			throws SynchronizationException {
		match(remoteFolder, localFolder, remoteObject, localObject);
		getPerformer().removeFromFolder(remoteObject, remoteFolder);
	}

	@Override
	public void alterCreatedProperties(SyncObject localObject,
			Map<String, Object> newProperties) {
		// Propagate properties as is.
	}

	@Override
	public void alterUpdatedProperties(SyncObject localObject,
			CmisObject remoteObject, Map<String, Object> updatedProperties) {
		// Propagate properties as is.
	}

	@Override
	protected Document newObject(Folder remoteFolder, SyncFolder localFolder,
			SyncDocument localObject) throws SynchronizationException {
		return getPerformer().create(remoteFolder, localObject);
	}

	@Override
	protected Folder newObject(Folder remoteFolder, SyncFolder localFolder,
			SyncFolder localObject) throws SynchronizationException {
		return getPerformer().create(remoteFolder, localObject);
	}

	@Override
	protected void deletedObject(Folder remoteFolder, SyncFolder localFolder,
			Document remoteObject) throws SynchronizationException {
		getPerformer().delete(remoteObject);
	}

	@Override
	protected void deletedObject(Folder remoteFolder, SyncFolder localFolder,
			Folder remoteObject) throws SynchronizationException {
		getPerformer().delete(remoteObject);
	}

	@Override
	protected FileableCmisObject match(Folder remoteFolder,
			SyncFolder localFolder, Document remoteObject,
			SyncDocument localObject) throws SynchronizationException {
		return getPerformer().update(remoteObject, localObject);
	}

	@Override
	protected FileableCmisObject match(Folder remoteFolder,
			SyncFolder localFolder, Folder remoteObject, SyncFolder localObject)
			throws SynchronizationException {
		return getPerformer().update(remoteObject, localObject);
	}

	@Override
	public Relationship newRelationship(SyncRelationship localRelationship,
			CmisObject remoteSource, CmisObject remoteTarget)
			throws SynchronizationException {
		return getPerformer().addRelationship(localRelationship, remoteSource,
				remoteTarget);
	}

	@Override
	public Relationship match(Relationship remoteRelationship,
			SyncRelationship localRelationship) throws SynchronizationException {
		return getPerformer().update(remoteRelationship, localRelationship);
	}

	@Override
	public void removedRelationship(Relationship remoteRelationship,
			SyncObject localSource, SyncObject localTarget)
			throws SynchronizationException {
		getPerformer().removeRelationship(remoteRelationship);
	}

}
