package org.eclipse.jwt.ecm.sync.cmis.strategy;

import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.FileableCmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Relationship;
import org.eclipse.jwt.ecm.sync.SynchronizationException;
import org.eclipse.jwt.ecm.sync.cmis.CmisDmsSynchronizer;
import org.eclipse.jwt.ecm.sync.cmis.CmisDmsSynchronizer.SynchronizationPerformer;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncDocument;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFileableObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncFolder;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncObject;
import org.eclipse.jwt.ecm.sync.cmis.model.SyncRelationship;

public interface SynchronizationStrategy {

	/**
	 * Defines the object to be used to perform desired actions (create, update,
	 * delete) on remote objects when one of the other methods is called.
	 * 
	 * @param performer
	 */
	public void setPerformer(SynchronizationPerformer performer);

	/**
	 * Defines the Synchronizer's behavior when a new object is found in the
	 * local model, i.e. when a local object has no remote equivalent.
	 * 
	 * @param remoteFolder
	 *            The remote version of the containing folder (non-null)
	 * @param localFolder
	 *            The local version of the containing folder (non-null)
	 * @param localObject
	 *            The local object (non-null) which does not match any remote
	 *            object.
	 * @return A new remote object matching the local one, or null if none was
	 *         created.
	 */
	public FileableCmisObject newObject(Folder remoteFolder,
			SyncFolder localFolder, SyncFileableObject localObject)
			throws SynchronizationException;

	/**
	 * Defines the Synchronizer's behavior when a removed object is found in the
	 * remote model, i.e. when a remote object has no local equivalent.
	 * 
	 * @param remoteFolder
	 *            The remote version of the containing folder (non-null)
	 * @param localFolder
	 *            The local version of the containing folder (non-null)
	 * @param remoteObject
	 *            The remote object (non-null) which does not match any local
	 *            object.
	 */
	public void deletedObject(Folder remoteFolder, SyncFolder localFolder,
			FileableCmisObject remoteObject) throws SynchronizationException;

	/**
	 * 
	 * Defines the Synchronizer's behavior when a child link was added, i.e.
	 * when a local object has a remote equivalent, but the remote equivalent is
	 * not in the same containing folder than the local object.
	 * 
	 * @param remoteFolder
	 *            The remote version of the containing folder (non-null)
	 * @param localFolder
	 *            The local version of the containing folder (non-null)
	 * @param remoteObject
	 *            The remote version of the object (non-null)
	 * @param localObject
	 *            The local version of the object (non-null)
	 */
	public void newChildLink(Folder remoteFolder, SyncFolder localFolder,
			FileableCmisObject remoteObject, SyncFileableObject localObject)
			throws SynchronizationException;

	/**
	 * 
	 * Defines the Synchronizer's behavior when a child link was removed, i.e.
	 * when a local object has a remote equivalent, but the remote equivalent is
	 * in a containing folder in which the local object is not.
	 * 
	 * @param remoteFolder
	 *            The remote version of the containing folder (non-null)
	 * @param localFolder
	 *            The local version of the containing folder (non-null)
	 * @param remoteObject
	 *            The remote version of the object (non-null)
	 * @param localObject
	 *            The local version of the object (non-null)
	 */
	public void removedChildLink(Folder remoteFolder, SyncFolder localFolder,
			FileableCmisObject remoteObject, SyncFileableObject localObject)
			throws SynchronizationException;

	/**
	 * Defines the Synchronizer's behavior when a local object has a remote
	 * equivalent in the same containing folder.
	 * 
	 * @param remoteFolder
	 *            The remote version of the containing folder (non-null)
	 * @param localFolder
	 *            The local version of the containing folder (non-null)
	 * @param remoteObject
	 *            The remote version of the object (non-null)
	 * @param localObject
	 *            The local version of the object (non-null)
	 */
	public FileableCmisObject match(Folder remoteFolder,
			SyncFolder localFolder, FileableCmisObject remoteObject,
			SyncFileableObject localObject) throws SynchronizationException;

	/**
	 * Defines the Synchronizer's behavior when a new relationship is found in
	 * the local model, i.e. when a local relationship has no remote equivalent.
	 * 
	 * @param localRelationship
	 *            The local relationship (non-null) which does not match any
	 *            remote relationship.
	 * @param remoteSource
	 *            The remote version of the relationship's source
	 * @param remoteTarget
	 *            The remote version of the relationship's target
	 * @return A new remote relationship matching the local one, or null if none
	 *         was created.
	 */
	public Relationship newRelationship(SyncRelationship localRelationship,
			CmisObject remoteSource, CmisObject remoteTarget)
			throws SynchronizationException;

	/**
	 * Defines the Synchronizer's behavior when a local relationship has a
	 * remote equivalent.
	 * 
	 * @param remoteRelationship
	 *            The remote version of the relationship (non-null)
	 * @param localRelationship
	 *            The local version of the relationship (non-null)
	 */
	public Relationship match(Relationship remoteRelationship,
			SyncRelationship localRelationship) throws SynchronizationException;

	/**
	 * Defines the Synchronizer's behavior when a removed relationship is found
	 * in the remote model, i.e. when a remote relationship has no local
	 * equivalent.
	 * 
	 * @param remoteRelationship
	 *            The remote relationship (non-null) which does not match any
	 *            local relationship.
	 * @param localSource
	 *            The local version of the relationship's source. (May be null)
	 * @param localTarget
	 *            The local version of the relationship's target. (May be null)
	 * @return A new remote relationship matching the local one, or null if none
	 *         was created.
	 */
	public void removedRelationship(Relationship remoteRelationship,
			SyncObject localSource, SyncObject localTarget)
			throws SynchronizationException;

	/**
	 * Alters the properties to be propagated to the remote copy when a remote
	 * object is being {@link #create(SyncDocument) created}.
	 * 
	 * @param localObject
	 *            The local object being created (non-null)
	 * @param properties
	 *            The properties that will be assigned to the created remote
	 *            object. <em>This</em> is the object to be modified, and not
	 *            <code>localObject.getProperties()</code>.
	 */
	public void alterCreatedProperties(SyncObject localObject,
			Map<String, Object> newProperties);

	/**
	 * Alters the properties to be propagated to the remote copy when a remote
	 * object is being {@link #update(Document, SyncDocument) updated}.
	 * 
	 * @param localObject
	 *            The local version of the object being updated (non-null)
	 * @param remoteObject
	 *            The remote version of the object being updated (non-null)
	 * @param properties
	 *            The properties that will be updated on the remote object.
	 *            <em>This</em> is the object to be modified, and not
	 *            <code>localObject.getProperties()</code>.
	 */
	public void alterUpdatedProperties(SyncObject localObject,
			CmisObject remoteObject, Map<String, Object> updatedProperties);

}