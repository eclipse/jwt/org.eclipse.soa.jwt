/**
 * File:    PluginProperties.java
 * Created: 13.03.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Melanie Hesselbart, Programming distributed Systems Lab, University of Augsburg 
 *    	- API and implementation, based on the PluginProperties of jwt-we
 *******************************************************************************/

package org.eclipse.jwt.we_view_editor;

import org.eclipse.osgi.util.NLS;


/**
 * Provides access to the <code>viewplugin.properties</code> file.
 * 
 * <p>
 * All keys in the <code>viewplugin.properties</code> are access with static members. Some
 * properties need parameters and they are represented by member functions. They may also
 * encapsulate a simple logic to find the right property based on the parameters.
 * </p>
 */
public class PluginProperties extends NLS
{
    
	/**
	 * The name of the plugin.
	 */
	public static String BUNDLE_NAME = "viewplugin"; //$NON-NLS-1$
	
	/**
	 * The ID of the perspective for the RCP application.
	 */
	public static String perspective_id;
	
	/**
	 * Extension for the view files.
	 */
	public static String plugin_view_extension;
	
	/**
	 * Pagetext for the part of the editor where view-name and properties are set.
	 */
	public static String editor_pagetext_properties;
	
	/**
	 * Text of the group where the view-name is set.
	 */
	public static String editor_group_viewname;
	
	/**
	 * Text of the label for the view-name.
	 */
	public static String editor_fieldname_viewname;
	
	/**
	 * Text of the group for the table where the view-properties are set.
	 */
	public static String editor_group_properties;
	
	/**
	 * Text of the table column for the properties.
	 */
	public static String editor_properties_tableColumn;
	
	/**
	 * Error-message for the "new wizard".
	 */
	public static String editor_ErrorMessage_title;
	
	/**
	 * Error-message for the editor if an saving doesn't work.
	 */
	public static String editor_ErrorSavingFile_message;
	
	/**
	 * Error-message for the editor if no view name is set.
	 */
	public static String editor_ErrorMissingViewName_message;
	
	/**
	 * Error-message for the editor if destination file for save as is currently open.
	 */
	public static String editor_ErrorCurrentlyOpen_message;
	
	/**
	 * Question-title at save as, if destination file already exists.
	 */
	public static String editor_QuestionOverwriteFile_title;
	
	/**
	 * Question at save as, if destination file already exists.
	 */
	public static String editor_QuestionOverwriteFile_message;
	
	/**
	 * Error-message for the editor if no view name is set.
	 */
	public static String newWizard_error_noViewNameSet;
	
	/**
	 * Error-message for the "new wizard".
	 */
	public static String newWizard_error;
	
	/**
	 * Status-message while creating a new view file in the "new wizard".
	 */
	public static String newWizard_creating;
	
	/**
	 * First part of status-message in the "new wizard" that appears if the selected project doesn't exist.
	 */
	public static String newWizard_projectDoesntExist_part1;

	/**
	 * Second part of status-message in the "new wizard" that appears if the selected project doesn't exist.
	 */
	public static String newWizard_projectDoesntExist_part2;
	
	/**
	 * Status-message for the "new wizard" while opening a new file for editing.
	 */
	public static String newWizard_openFileForEditing;
	
	/**
	 * Description of the "new wizard".
	 */
	public static String newWizard_wizardDescription;
	
	/**
	 * Page title for creating a new file in the "new wizard".
	 */
	public static String newWizard_pageTitle;
	
	/**
	 * Initial message for the new file created in the "new wizard".
	 */
	public static String newWizard_initalFileContentMessage;
	
	/**
	 * Text of the label for the project that the new view file should belong to.
	 */
	public static String newWizard_project;
	
	/**
	 * Text of the browse-button in the "new wizard".
	 */
	public static String newWizard_browse;
	
	/**
	 * Text of the label for the new view file in the "new wizard".
	 */
	public static String newWizard_fileName;
	
	/**
	 * Text of the label for the view name in the "new wizard".
	 */
	public static String newWizard_viewName;
	
	/**
	 * Initial file name for the new view file.
	 */
	public static String newWizard_newFile_fileName;
	
	/**
	 * Text of the label for the selected project that the new view file should belong to.
	 */
	public static String newWizard_selectProject;
	
	/**
	 * Error-message in the "new wizard" if the project wasn't specified.
	 */
	public static String newWizard_error_unspecifiedProject;
	
	/**
	 * Error-message in the "new wizard" if the selected project doesn't exist.
	 */
	public static String newWizard_error_notExistingProject;
	
	/**
	 * Error-message in the "new wizard" if the selected project isn't writable.
	 */
	public static String newWizard_error_notWritableProject;
	
	/**
	 * Error-message in the "new wizard" if the file name isn't specified.
	 */
	public static String newWizard_error_unspecifiedFileName;
	
	/**
	 * Error message in the "new wizard" if the file name isn't valid.
	 */
	public static String newWizard_error_notValidFileName;
	
	/**
	 * Error-message in the "new wizard" if the file extension isn't ".view".
	 */
	public static String newWizard_error_wrongFileExtension;
	
	/**
	 * Error-message in the "new wizard" if the file already exists.
	 */
	public static String newWizard_error_existingFile;
	
	
	static {
	      NLS.initializeMessages(BUNDLE_NAME, PluginProperties.class);
		}


	/**
	 * No instances allowed, only static access.
	 */
	private PluginProperties()
	{
	}

}
