/**
 * File:    ViewDetails.java
 * Created: 13.03.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany, http://www.ds-lab.org/
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Melanie Hesselbart, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.we_view_editor;

import java.net.URL;
import java.util.List;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jwt.we.misc.views.ViewItemWrapper;
import org.eclipse.jwt.we.misc.views.ViewEcoreManager;
import org.eclipse.jwt.we_view_editor.editors.ViewEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.jwt.we_view_editor.PluginProperties;

public class ViewDetails extends ViewPart implements ISelectionListener
{
	/**
	 * The composite, where all SWT-Elements belong to.
	 */
	private Composite parent;
	
	/**
	 * A flag that says, if the table with the detailled properties is currently initialized
	 * or not (important to know before filling the table for setting the right layout).
	 */
	boolean tableOn = false;
	
	/**
	 * The table for the detailles properties for the E-Class selected in the Editor.
	 */
	private Table table;
	
	/**
	 * The reference to the active part of the View Editor.
	 */
	private ViewEditor part1;
	
	/**
	 * The path to the ecore-file.
	 */
	private URL ecorePath;
	
	/**
	 * A flag that says weather this view is loading the first time (just for the right layout).
	 */
	boolean firstLoaded = true;

	
	/**
	 * The constructor which does nothing
	 */
	public ViewDetails()
	{
	}

	/**
	 * Inits the view.
	 */
	public void createPartControl(Composite c_parent) 
	{
	    parent = c_parent;
	    table = new Table(parent, SWT.SINGLE | SWT.V_SCROLL | 
			SWT.BORDER | SWT.FULL_SELECTION);
	    //table is not null; necessary for enabling an disabling of the table
	    tableOn = true;
		
	    //add view to selectionListener of the view-editor
	    getSite().getPage().addSelectionListener(this);

	    openView();
	}

	
	/**
	 * Reloads the table with contains all the EAttribute.
	 * 
	 * @param attrtabledata
	 * @param itemValue	value of father-item
	 */
	private void reloadAttrTables(List<ViewItemWrapper> attrtabledata, Object itemValue)
	{
	    //instance of table is not null
	    if (tableOn)
	    {
	    	//disposing table necessary for right layout after reloading table (numbers of checkboxes doesn't change otherwise)
		table.dispose();
	    	tableOn = false;
	    }
		
	    //create new instance of table
	    createTable();
		
	    Object[][] data = null;
		
	    if (attrtabledata != null)
	    {
		data = new Object[attrtabledata.size()][2];

		for (int i = 0; i < attrtabledata.size(); i++)
		{
		    data[i][0] = attrtabledata.get(i);
		    data[i][1] = attrtabledata.get(i).getValue();
		    
		    final int p = i;
		    final Object[][] dataCopy = data;
		    final Button buttonCheck = new Button(table, SWT.CHECK);

			buttonCheck.addSelectionListener(new SelectionListener()
		            {
		            	public void widgetDefaultSelected(SelectionEvent e) 
		            	{
		            	}
		            
		            	public void widgetSelected(SelectionEvent e) 
		            	{
		            	    part1.setDirty(true);
		            	    ViewItemWrapper viw = (ViewItemWrapper) dataCopy[p][0];
		            	    boolean value = buttonCheck.getSelection();
		            	    ViewEcoreManager.getInstance().setDisplay(viw, value);
					
		            	    part1.setChangesFromView(dataCopy[p][0], value);
		            	}
		            }
		            );

		    
		    TableItem item = new TableItem(table, SWT.NULL);
		    TableEditor editor = new TableEditor(table);
		    editor.grabHorizontal=true;
		    editor.minimumHeight=buttonCheck.getSize().x;
		    editor.minimumWidth=buttonCheck.getSize().y;
		    editor.setEditor(buttonCheck, item, 1);
		    item.setText(0, (data[i][0]).toString());
		    item.setData(new Button(table, SWT.CHECK));
				
		    if ((data[i][1]).toString() == "true") //$NON-NLS-1$
		    {
			buttonCheck.setSelection(true);
		    }
		    else
		    {
			buttonCheck.setSelection(false);
		    }
		}
	    }
	    //no attributes for the selected E-Class
	    else
	    {
		table.setEnabled(false);
		data = new Object[0][2];
	    }
	    
	    //table is enabled for editing, if E-Class has attributes 
	    if (itemValue.toString().equals("true")) //$NON-NLS-1$
	    {
		table.setEnabled(true);
	    }
	    else
	    {
		table.setEnabled(false);
	    }
		
	    //reloading the view
	    parent.layout();
	}	

	/**
	 * Creates the layout for the table with the detail properties.
	 */
	private void createTable()
	{
	    table = new Table(parent, SWT.SINGLE | SWT.V_SCROLL | 
			SWT.BORDER | SWT.FULL_SELECTION);
	    table.setHeaderVisible(true);
	    table.setLinesVisible(true);
	    TableColumn columnProp = new TableColumn(table, SWT.NONE,0);
	    columnProp.setText(PluginProperties.editor_properties_tableColumn);
	    columnProp.setWidth(200);
	    TableColumn columnAct = new TableColumn(table, SWT.NONE, 1);
	    columnAct.setWidth(15);
	    table.setVisible(true);
		
	    tableOn = true;
	}
	
	/**
	 * Opening the view.
	 */
	private void openView()
	{
	    //inits an empty disabled table, just for the first time
	    reloadAttrTables(null, false);
	}

	/**
	 * To realize what changes in the View Editor have happened.
	 * for activity as selectionListener
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) 
	{
	   if (part.getTitle().endsWith("."+PluginProperties.plugin_view_extension)) //$NON-NLS-1$
	   {
	       part1 = (ViewEditor) part;
	       reloadAttrTables(part1.getListOfAttributes(), part1.getItemValue());
	       ecorePath = part1.getEcorePath();
	       
	       if (firstLoaded)
	       {
		   firstLoading();
		   firstLoaded = false;
	       }
	   }
	}
	
	/**
	 * Gets path to ecore-file at the first time when an E-Class has been selected.
	 */
	private void firstLoading()
	{
	    ViewEcoreManager.getInstance().setPath(ecorePath.getFile());
	}

	@Override
	public void setFocus() 
	{    
	}
}
