/**
 * File:    PropertyItemProviderAdapterFactory.java
 * Created: 17.12.2006
 *
/*******************************************************************************
 * Copyright (c) 2007-2009 Open Wide SA, France <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.property.edit.providerdecorator;

import org.eclipse.emf.common.notify.Adapter;

/**
 *
 * @obsolete not used, does nothing
 * @version $Id: PropertyItemProviderAdapterFactory.java,v 1.1 2010-05-10 10:04:30 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class PropertyItemProviderAdapterFactory
		extends
		org.eclipse.jwt.we.conf.property.edit.provider.PropertyItemProviderAdapterFactory {

	@Override
	public Adapter createPropertyAdapter() {
		if (propertyItemProvider == null) {
			propertyItemProvider = new PropertyItemProvider(this);
		}

		return propertyItemProvider;
	}
	
}
