/**
 * File:    PropertyItemProvider.java
 * Created: 17.12.2006
 *
/*******************************************************************************
 * Copyright (c) 2007-2009 Open Wide SA, France <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.property.edit.providerdecorator;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.jwt.we.conf.property.model.property.PropertyPackage;

/**
 * 
 * @obsolete not used, does nothing
 * @version $Id: PropertyItemProvider.java,v 1.1 2010-05-10 10:04:30 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class PropertyItemProvider extends
		org.eclipse.jwt.we.conf.property.edit.provider.PropertyItemProvider {

	public PropertyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	@Override
	protected void addValuePropertyDescriptor(Object object) {
		// case of "value" attribute of dynamic Properties : type depends on Aspect
		// MDU TODO better, pluggable ?
		ItemPropertyDescriptor origItemPropertyDescriptor = createItemPropertyDescriptor
		(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Property_value_feature"), //$NON-NLS-1$
				 getString("_UI_PropertyDescriptor_description", "_UI_Property_value_feature", "_UI_Property_type"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				 PropertyPackage.Literals.PROPERTY__VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null);
		//PropertyPropertyDescriptor propertyItemPropertyDescriptor = new PropertyPropertyDescriptor(object, origItemPropertyDescriptor);
		//itemPropertyDescriptors.add(propertyItemPropertyDescriptor);
		itemPropertyDescriptors.add(origItemPropertyDescriptor);
	}
	
}
