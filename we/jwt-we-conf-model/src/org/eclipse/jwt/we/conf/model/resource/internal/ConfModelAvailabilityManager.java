/**
 * File:    ConfModelResourceManagerImpl.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.resource.internal;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;


/**
 * Manages unavailibility of EMF model resources' related _conf or standalone .conf files
 * 
 * @version $Id: ConfModelAvailabilityManager.java,v 1.2 2010-05-27 15:29:51 mdutoo Exp $
 * @author Marc Dutoo
 *
 */
public class ConfModelAvailabilityManager {
	
	private static final String DEFAULT_MESSAGE = "unknown"; //$NON-NLS-1$
	
	/** Used only to flag standalone .conf files */
	private final HashMap<URI,String> erroredUris = new HashMap<URI,String>();
	
	/**
	 * Creates a new ConfModelAvailabilityManager
	 */
	protected ConfModelAvailabilityManager() {
		
	}

	/**
	 * Tells whether the given model's _conf or standalone .conf file as unavailable.
	 * @param modelRoot null in the case of a standalone .conf file
	 * @param confModelUri required for standalone .conf files
	 * @return if null, not flagged
	 */
	public String flaggedUnavailable(EObject modelRoot, URI confModelUri) {
		if (modelRoot != null) {
			for (Adapter resourceAdapter : modelRoot.eResource().eAdapters()) {
				if (resourceAdapter.isAdapterForType(ConfModelAvailabilityManager.class)) {
					return ((ConfModelAvailabilityResourceAdapter) resourceAdapter).getMessage();
				}
			}
			return null;
			
		} else {
			return erroredUris.get(confModelUri);
		}
	}

	/**
	 * Flags the given model's _conf or standalone .conf file as unavailable.
	 * @param modelRoot null in the case of a standalone .conf file
	 * @param confModelUri required for standalone .conf files, otherwise as info
	 * @param message about unavailability cause
	 */
	public void flagUnavailable(EObject modelRoot, URI confModelUri, String message) {
		// checking msg is not null, else doesn't work
		message = (message == null) ? DEFAULT_MESSAGE : message;
		
		if (modelRoot != null) {
			// cleaning up possible obsolete ConfModelUnavailableResourceAdapter
			Resource modelResource = modelRoot.eResource();
			for (Adapter resourceAdapter : new ArrayList<Adapter>(modelResource.eAdapters())) {
				if (resourceAdapter.isAdapterForType(ConfModelAvailabilityManager.class)) {
					// TODO resourceAdapter
					modelResource.eAdapters().remove(resourceAdapter);
				}
			}
			// flagging using a new adapter
			modelResource.eAdapters().add(new ConfModelAvailabilityResourceAdapter(confModelUri, message));
			
		} else {
			erroredUris.put(confModelUri, message);
		}
	}

	/**
	 * Unflags unavailability of the given model's _conf or standalone .conf.
	 * @param modelRoot null in the case of a standalone .conf file
	 * @param confModelUri required for standalone .conf files, otherwise as info
	 */
	public void unflagUnavailability(EObject modelRoot, URI confModelUri) {
		if (modelRoot != null) {
			// cleaning up possible obsolete ConfModelUnavailableResourceAdapter
			Resource modelResource = modelRoot.eResource();
			for (Adapter resourceAdapter : new ArrayList<Adapter>(modelResource.eAdapters())) {
				if (resourceAdapter.isAdapterForType(ConfModelAvailabilityManager.class)) {
					// TODO resourceAdapter
					modelResource.eAdapters().remove(resourceAdapter);
				}
			}
			
		} else {
			erroredUris.remove(confModelUri);
		}
	}

}
