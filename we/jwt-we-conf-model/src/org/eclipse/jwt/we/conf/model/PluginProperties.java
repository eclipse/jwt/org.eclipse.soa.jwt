/**
 * File:    PluginProperties.java
 * Created: 11.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Use NLS and static fields
 *    Marc Blachon, Bull SAS
 *      - adding extension point for the factoryRegistry
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model;

import java.util.MissingResourceException;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osgi.util.NLS;


/**
 * Provied access the <code>plugin.properties</code> file.
 * 
 * <p>
 * All keys in the <code>plugin.properties</code> are access with static members. Some
 * properties need parameters and they are represented by member functions. They may also
 * encapsulate a simple logic to find the right property based on the parameters.
 * </p>
 * 
 * @version $Id: PluginProperties.java,v 1.1 2010-05-10 08:27:22 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6
 */
public class PluginProperties extends NLS
{

	// "plugin" references the plugin[_locale].properties file path
	// at the root of the jwt-we bundle.
	private static final String BUNDLE_NAME = "plugin"; //$NON-NLS-1$

	static {
		NLS.initializeMessages(BUNDLE_NAME, PluginProperties.class);
	}
	
	/**
	 * Returns the value associated to a property.
	 * Prefer using directly the field for this property when possible
	 * @param string
	 * @return
	 */
	public static String getString(String key) {
		try {
			return (String)PluginProperties.class.getField(key).get(null);
		} catch (Exception ex) {
			return "!" + key + "!";  //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * Returns the value from the <code>plugin.properties</code> identified by a key.
	 * 
	 * <p>
	 * It the resource is not available a {@link MissingResourceException} is thrown.
	 * </p>
	 * 
	 * @param key
	 *            The key of the property.
	 * @return The value of the key.
	 * @throws MissingResourceException
	 *             If the resource can not be found.
	 */
	public static String getStringExpectMissing(String key)
			throws MissingResourceException
	{
		try {
			return (String)PluginProperties.class.getField(key).get(null);
		} catch (Exception ex) {
			throw new MissingResourceException("Cannot find value associated to key [" + key + "]", PluginProperties.class.getName(), key); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}


	/**
	 * Returns the value from the <code>plugin.properties</code> identified by a key and
	 * filled with a parameter.
	 * 
	 * <p>
	 * It the resource is not available a {@link MissingResourceException} is thrown.
	 * </p>
	 * 
	 * @param key
	 *            The key of the property.
	 * @param data
	 *            The parameter.
	 * @return The value of the key.
	 * @throws MissingResourceException
	 *             If the resource can not be found.
	 */
	public static String getStringExpectMissing(String key, Object[] data)
			throws MissingResourceException
	{
		return bind(getStringExpectMissing(key), data);
	}


	/**
	 * Returns a simple representing name of the type of an object.
	 * 
	 * @param object
	 *            The object, may be <code>null</code>.
	 * @return The name of the type.
	 */
	public static String getModelTypeName(Object object)
	{
		if (object == null)
		{
			return "null";  //$NON-NLS-1$
		}
		if (object instanceof EClass)
		{
			return ((EClass) object).getName();
		}
		if (object instanceof Class)
		{
			return ((Class<?>) object).getSimpleName();
		}
		if (object instanceof EObject)
		{
			return ((EObject) object).eClass().getName();
		}
		return object.getClass().getSimpleName();
	}

	
	//
	//
	//
	//
	//
	
	

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Plugin data
	//

	public static String pluginName ; // JWT
	public static String providerName ; // http://www.eclipse.org/jwt

	public static String logging_level ; // 
	
	//
	//
	//
	//
	//
	//

}
