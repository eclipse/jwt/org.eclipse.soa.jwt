/**
 * File:    ConfModelAvailabilityResourceAdapter.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.resource.internal;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.util.URI;


/**
 * Used to flag model resources that their related ConfModel is unavailable.
 * 
 * @version $Id: ConfModelAvailabilityResourceAdapter.java,v 1.1 2010-05-10 08:27:22 chsaad Exp $
 * @author Marc Dutoo
 *
 */
public class ConfModelAvailabilityResourceAdapter implements Adapter {
	
	private URI confModelUri;
	private String message;

	/**
	 * Creates a new unavailability flagging adapter
	 * @param confModelUri
	 * @param message
	 */
	public ConfModelAvailabilityResourceAdapter(URI confModelUri, String message) {
		this.confModelUri = confModelUri;
		this.message = message;
		
	}

	public Notifier getTarget() {
		return null;
	}

	/**
	 * NB. Adapter on EMF model Resource for ConfModelAvailabilityManager
	 */
	public boolean isAdapterForType(Object type) {
		return type == ConfModelAvailabilityManager.class;
	}

	public void notifyChanged(Notification notification) {
		// nothing
	}

	public void setTarget(Notifier newTarget) {
		// nothing
	}


	/**
	 * @return the flagged conf model uri
	 */
	public URI getConfModelUri() {
		return confModelUri;
	}

	/**
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
}
