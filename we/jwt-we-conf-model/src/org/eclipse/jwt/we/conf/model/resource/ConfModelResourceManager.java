/**
 * File:    ConfModelResourceManager.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.resource;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jwt.we.conf.model.ConfModel;
import org.eclipse.jwt.we.conf.model.Profile;
import org.eclipse.jwt.we.conf.model.resource.internal.ConfModelResourceManagerImpl;


/**
 * 
 * @version $Id: ConfModelResourceManager.java,v 1.1 2010-05-10 08:27:22 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 * @since 0.6
 */
public interface ConfModelResourceManager {
	
	ConfModelResourceManager INSTANCE = ConfModelResourceManagerImpl.createConfModelResourceManager();
			
	/**
	 * 
	 * @param profiles
	 * @param f
	 * @return exported profiles as confModel
	 * @throws ConfResourceException if error saving
	 */
	public abstract ConfModel exportProfiles(
			Collection<Profile> profiles, File f) throws ConfResourceException, IOException;

	/**
	 * 
	 * @param profiles
	 * @param f
	 * @return exported profiles as confModel
	 * @throws ConfResourceException if error saving
	 * @obsolete use exportProfiles instead
	 */
	public abstract ConfModel exportProfilesToFile(
			Collection<Profile> profiles, File f) throws Exception;
	
	/**
	 * Try to find, load and link model.
	 * @param modelRoot
	 * @param createIfDoesntExist if true, if conf model doesn't exist
	 * (including unavailability), tries to create a new one.
	 * @return
	 * @throws ConfResourceException if error while loading resource from fs
	 */
	public abstract ConfModel loadConfModelOfModel(EObject modelRoot,
			boolean createIfDoesntExist) throws ConfResourceException;
	/**
	 * Like loadConfModelOfModel() above, except loading configuration is taken
	 * from given status parameter rather than explicit parameters,
	 * and adds child statuses with errors to the given status parameter
	 * rather than throwing an exception.
	 * Useful to know whether a model will have to be patched and ask
	 * the user about it before trying to patch it in a second call.
	 * @param modelRoot
	 * @param cmStatus
	 * @return
	 * @author Marc Dutoo, Open Wide SA
	 * @since 0.7
	 */
	public abstract ConfModel loadConfModelOfModel(EObject modelRoot,
			ConfModelStatus cmStatus);

	/**
	 * Creates and saves a new ConfModel
	 * @param confModelUri to create
	 * @param modelResourceSet where to create it (if null, uses modelRoot's if not null, else a new one)
	 * @param modelRoot if not null, links the confModel with it and starts listening to its changes
	 * @return the newly created ConfModel 
	 * @throws ConfResourceException if error while loading resource from fs
	 */
	public abstract ConfModel createConfModel(URI confModelUri,
			ResourceSet modelResourceSet, EObject modelRoot)
			throws ConfResourceException;

	/**
	 * Rather use loadConfModel(ConfModelStatus).
	 * Loads an existing confModel.
	 * @param confModelUri
	 * @param modelResourceSet if null, tries to use modelRoot's if not null, else a new one
	 * @param modelRoot if provided and its the first time load, adds a
	 * ConfModelSavingModelResourceAdapter that will save the confModel when the model
	 * is saved, and removes the previously existing ones
	 * @param tryGettingExistingOneFirst true save if it can never have been already loaded
	 * @return
	 * @throws ConfResourceException if error while loading resource from fs
	 */
	public abstract ConfModel loadConfModel(URI confModelUri,
			ResourceSet modelResourceSet, EObject modelRoot,
			boolean tryGettingExistingOneFirst) throws ConfResourceException;
	/**
	 * Like loadConfModel() above, except loading configuration is taken
	 * from given status parameter rather than explicit parameters,
	 * and adds child statuses with errors to the given status parameter
	 * rather than throwing an exception.
	 * Useful to know whether a model will have to be patched and ask
	 * the user about it before trying to patch it in a second call.
	 * @param confModelUri
	 * @param modelResourceSet
	 * @param modelRoot
	 * @param cmStatus
	 * @return
	 * @author Marc Dutoo, Open Wide SA
	 * @since 0.7
	 */
	public abstract ConfModel loadConfModel(URI confModelUri,
			ResourceSet modelResourceSet, EObject modelRoot,
			ConfModelStatus cmStatus);
	
	/**
	 * Updates a badly linked confModel to target its model
	 * @param confModel
	 * @param oldModelURI
	 * @param newModelURI
	 * @param modelRoot
	 * @return 
	 * @throws ConfResourceException if error while saving or reloading resource from fs
	 */
	public abstract ConfModel migrateConfModelToModel(ConfModel confModel,
			URI oldModelURI, URI newModelURI, EObject modelRoot)
			throws ConfResourceException;

	/**
	 * Deletes the confModel i.e. disables conf
	 * @param modelElementOrConfModel
	 */
	public abstract boolean deleteConfModel(ConfModel confModel);

}