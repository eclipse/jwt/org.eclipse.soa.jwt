/**
 * File:    AspectFactoryImpl.java
 * Created: 08.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.aspects.factory.internal;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jwt.we.conf.model.Aspect;
import org.eclipse.jwt.we.conf.model.AspectInstance;
import org.eclipse.jwt.we.conf.model.aspects.factory.AspectFactory;
import org.eclipse.jwt.we.conf.model.plugin.internal.Logger;


/**
 * Provides aspect instantiation features.
 * 
 * TODO :
 * extract itf, indent
 * 
 * @version $Id: AspectFactoryImpl.java,v 1.1 2010-05-10 08:27:23 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 */
public class AspectFactoryImpl implements AspectFactory {

	private static Logger logger = Logger.getLogger(AspectFactoryImpl.class);
	
	/**
	 * Should be instanciated by Aspects.java
	 */
	protected AspectFactoryImpl() {
		
	}


	public static AspectFactory createAspectFactory() {
		return new AspectFactoryImpl();
	}
	

	/**
	 * Creates the corresponding aspect instance, and sets its id and target model element
	 * @param aspect aspect definition
	 * @return a new aspect instance that follows the given definition
	 */
	public AspectInstance createAspectInstance(Aspect aspect, EObject modelElement) {
		EClassifier aspectInstanceETypeEClassifier = aspect.getAspectInstanceEType();

		if (aspectInstanceETypeEClassifier == null) {
			// should not happen because 1-1 relation
			logger.severe("Creating aspect : no aspectInstanceEType " //$NON-NLS-1$
					+ aspectInstanceETypeEClassifier + " of aspect " + aspect.getId()); //$NON-NLS-1$
			// NB. no error dialog, because happens when building new child menu
			return null;
		}
		
		if (aspectInstanceETypeEClassifier.eIsProxy()) {
			// EMF definition of aspect instance type has not been found
			logger.severe("Creating aspect : can't find EMF definition of aspectInstanceEType " //$NON-NLS-1$
					+ aspectInstanceETypeEClassifier + " of aspect " + aspect.getId()); //$NON-NLS-1$
			// NB. no error dialog, because happens when building new child menu TODO sure ??
			return null;
		}
  	    
  	    // now using ext point factory
  	    AspectFactory aspectFactory = AspectFactory.Registry.INSTANCE.getFactory(aspect);
  	    AspectInstance newAspectInstance = aspectFactory.createAspectInstance(aspect, modelElement);
          
      	// setting aspect id
        newAspectInstance.setId(aspect.getId());
        // setting aspect enriched model element
		newAspectInstance.setTargetModelElement(modelElement);
  			
  		return newAspectInstance;
	}
	
}
