/**
 * File:    EclassAspectFactory.java
 * Created: 08.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.aspects.factory.internal;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jwt.we.conf.model.Aspect;
import org.eclipse.jwt.we.conf.model.AspectInstance;
import org.eclipse.jwt.we.conf.model.ConfPackage;
import org.eclipse.jwt.we.conf.model.aspects.factory.RegistryAspectFactory;

/**
 * Simple EClass aspect factory, meant to be default.
 * 
 * @version $Id: EclassAspectFactory.java,v 1.1 2010-05-10 08:27:23 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 */
public class EclassAspectFactory implements RegistryAspectFactory {
	
	public EclassAspectFactory() {
		
	}
	

	public boolean isFactoryFor(Aspect aspect) {
		// not null nor proxy, already checked
		EClassifier aspectInstanceETypeEClassifier = aspect.getAspectInstanceEType();
		return (aspectInstanceETypeEClassifier instanceof EClass)
				&& ConfPackage.eINSTANCE.getAspectInstance().isSuperTypeOf((EClass) aspectInstanceETypeEClassifier);
	}

	
	public AspectInstance createAspectInstance(Aspect aspect, EObject modelElement) {
		// not null nor proxy, already checked
		EClassifier aspectInstanceETypeEClassifier = aspect.getAspectInstanceEType();
		// only one EClass Aspect cases
		// only if isFactoryFor aspect :
		return createAspectCustom((EClass) aspectInstanceETypeEClassifier);
	}

	
	/**
	 * @param aspectInstanceEClass
	 * @return a new instance of the given custom (i.e. that extends AspectInstance) aspect.
	 */
	public AspectInstance createAspectCustom(
			EClass aspectInstanceEClass) {
		EFactory aspectEFactory = aspectInstanceEClass.getEPackage().getEFactoryInstance();
		AspectInstance newAspectInstance = (AspectInstance) aspectEFactory.create(aspectInstanceEClass); // TODO check
		return newAspectInstance;
	}
	
}
