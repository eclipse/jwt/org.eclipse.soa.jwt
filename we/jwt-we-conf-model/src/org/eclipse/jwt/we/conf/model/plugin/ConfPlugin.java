/**
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * $Id: ConfPlugin.java,v 1.1 2010-05-10 08:27:23 chsaad Exp $
 */
package org.eclipse.jwt.we.conf.model.plugin;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.jwt.we.conf.model.aspects.AspectManager;
import org.eclipse.jwt.we.conf.model.aspects.event.AspectEventManager;
import org.eclipse.jwt.we.conf.model.plugin.internal.Logger;
import org.osgi.framework.BundleContext;

/**
 * This is the central singleton for the ConfPlugin plugin.
 * @since 0.6
 */
public final class ConfPlugin extends EMFPlugin {
	
	public static final String CONF_PLUGIN_ID = "org.eclipse.jwt.we.conf.model"; //$NON-NLS-1$
	
	/**
	 * Keep track of the singleton.
	 */
	public static final ConfPlugin INSTANCE = new ConfPlugin();

	/**
	 * Keep track of the singleton.
	 */
	private static Implementation plugin;

	/**
	 * The workspace root.
	 * @see #getWorkspaceRoot
	 */
	private static IWorkspaceRoot workspaceRoot;

	/**
	 * Create the instance.
	 */
	public ConfPlugin() {
		super
		  (new ResourceLocator [] {});
	}

	/**
	 * Returns the singleton instance of the Eclipse plugin.
	 * @return the singleton instance.
	 */
	@Override
	public ResourceLocator getPluginResourceLocator() {
		return plugin;
	}

	/**
	 * Returns the singleton instance of the Eclipse plugin.
	 * @return the singleton instance.
	 */
	public static Implementation getPlugin() {
		return plugin;
	}

	/**
	 * The actual implementation of the Eclipse <b>Plugin</b>.
	 */
	public static class Implementation extends EclipsePlugin {

		/**
		 * Creates an instance.
		 */
		public Implementation() {
			super();

			// Remember the static instance.
			//
			plugin = this;
		}
		
		/* (non-Javadoc)
		 * @see org.eclipse.core.runtime.Plugin#start(org.osgi.framework.BundleContext)
		 */
		@Override
		public void start(BundleContext context) throws Exception {
			super.start(context);

			// initing workspaceRoot (helper). NB. Same as in EcorePlugin.
			if (IS_RESOURCES_BUNDLE_AVAILABLE && System.getProperty(
					"org.eclipse.emf.ecore.plugin.EcorePlugin.doNotLoadResourcesPlugin") == null) { //$NON-NLS-1$
				workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			}
			
			// TODO init registries, managers and caches, like in EcorePlugin
		}
	}


	/**
	 * The Logger.
	 */
	public static final Logger logger = Logger.getLogger(ConfPlugin.class);
	
	public static final AspectManager getAspectManager() {
		return AspectManager.INSTANCE;
	}
	
	public static final AspectEventManager getDefaultAspectEventManager() {
		return AspectEventManager.DEFAULT_INSTANCE;
	}
	
	/**
	 * Returns the workspace root, or <code>null</code>, if the runtime environment is stand-alone.
	 * @return the workspace root, or <code>null</code>.
	 */
	public static IWorkspaceRoot getWorkspaceRoot() {
	  return workspaceRoot;
	}

}
