/**
 * File:    AspectEventManager.java
 * Created: 25.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2010 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.aspects.event;

import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jwt.we.conf.model.Aspect;
import org.eclipse.jwt.we.conf.model.aspects.AspectManager;


/**
 * 
 * Hooks up and implements generic aspects behaviour on model events by listening
 * to the decorated EMF model(s).
 * 
 * To enable it, register it as a listener on the adapterFactory
 *    * of the decorated model(s), which is done at loading time in
 * ConfModelResourceManagerImpl.loadConfModel(URI, rset, modelRoot, cmStatus)
 *    * or on the main ComposedAdapterFactory
 *    * or possibly in a model element
 * 
 * NB. NOT USED anymore for now.
 * 
 * @version $Id: AspectEventManager.java,v 1.3 2010-05-27 15:29:51 mdutoo Exp $
 * @author Marc Dutoo, Open Wide SA, France, <www.openwide.fr>
 * @since 0.6
 */
public class AspectEventManager implements Adapter {

	public static final AspectEventManager DEFAULT_INSTANCE = new AspectEventManager();

	/**
	 * Creates a new AspectEventManager
	 */
	public AspectEventManager() {
		
	}
	

	/**
	 * Adds aspect-oriented behaviour on changes in the decorated model.
	 * Those behaviours are : on ADD, add autocreated aspectInstances.
	 * 
	 * NB. NOT USED anymore for now :
	 * aspects are now auto created in CreateChildCommand so AddCommand
	 * (and copy / paste) won't trigger it.
	 */
	public void notifyChanged(Notification notification) {
	
	}

	public Notifier getTarget() {
		return null;
	}

	public boolean isAdapterForType(Object type) {
		return type == AspectEventManager.class;
	}

	public void setTarget(Notifier newTarget) {
		
	}
	

	/**
	 * Should be called when a model element has been created.
	 * Adds to it new instances of all relevant Aspects.
	 * @param modelElement
	 */
	public void onCreated(EObject modelElement) {
		List<Aspect> aspects = AspectManager.INSTANCE.getAspects(modelElement);
		this.onCreatedInternal(modelElement, aspects);
	}
	
	public void onCreatedInternal(EObject modelElement, List<Aspect> aspects) {
		for (Aspect aspect : aspects) {
			// NB. no need to check for existing aspect instances since this model element has just been created
			if (aspect.isAutocreated()) {
				// create and add aspect
				AspectManager.INSTANCE.createAndAddAspectInstance(aspect, modelElement);
			}
		}
	}

}
