/**
 * File:    AspectFactory.java
 * Created: 08.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.aspects.factory;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jwt.we.conf.model.Aspect;
import org.eclipse.jwt.we.conf.model.AspectInstance;
import org.eclipse.jwt.we.conf.model.aspects.factory.internal.AspectFactoryImpl;
import org.eclipse.jwt.we.conf.model.aspects.factory.internal.AspectFactoryRegistryImpl;
import org.eclipse.jwt.we.conf.model.plugin.ConfPlugin;


/**
 * Aspect factory
 * 
 * @version $Id: AspectFactory.java,v 1.2 2010-05-25 18:32:10 mdutoo Exp $
 * @author Marc Dutoo, Open Wide SA
 * @since 0.6
 */
public interface AspectFactory {
	
	public interface Registry {
		
		String ASPECT_FACTORY_EXTENSION_POINT_ID = ConfPlugin.CONF_PLUGIN_ID + ".registryAspectFactory"; //$NON-NLS-1$

		AspectFactory getFactory(Aspect aspect);
		
		Registry INSTANCE = AspectFactoryRegistryImpl.createRegistry();
		
	}
	
	/**
	 * Creates the corresponding aspect instance, and sets its id and target model element
	 * @param aspect aspect definition
	 * @param modelElement can be null (ex. if creating a not yet set copy)
	 * @return a new aspect instance that follows the given definition
	 */
	AspectInstance createAspectInstance(Aspect aspect, EObject modelElement);

	AspectFactory INSTANCE = AspectFactoryImpl.createAspectFactory();
	
}
