/**
 * File:    ConfRegistry.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.aspects;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jwt.we.conf.model.Aspect;
import org.eclipse.jwt.we.conf.model.ConfModel;
import org.eclipse.jwt.we.conf.model.Profile;
import org.eclipse.jwt.we.conf.model.aspects.internal.ConfRegistryImpl;
import org.eclipse.jwt.we.conf.model.plugin.ConfPlugin;


/**
 * 
 * @version $Id: ConfRegistry.java,v 1.1 2010-05-10 08:27:23 chsaad Exp $
 * @author Marc Dutoo
 * @since 0.6
 */
public interface ConfRegistry {

	String CONF_EXTENSION_POINT_ID = ConfPlugin.CONF_PLUGIN_ID + ".conf"; //$NON-NLS-1$
	/** Path to the directory where the confs should be within the bundle or fs */
	String CONFDIRECTORY = "conf/"; //$NON-NLS-1$
	String CONFEXTENSION = "conf"; //$NON-NLS-1$

	ConfRegistry INSTANCE = ConfRegistryImpl.createConfRegistry();
	
	/**
	 * Returns the given installed aspect
	 * @param aspectId null if none
	 * @return
	 */
	Aspect getInstalledAspect(String aspectId);
	/**
	 * Finds and returns the corresponding installed Profile
	 * @param profileId
	 * @return the Profile that has the given id, null if none
	 */
	Profile getInstalledProfile(String profileId);
	/**
	 * (copy not required since should be loaded at all times)
	 * @return all installed profiles
	 */
	ArrayList<Profile> getInstalledProfiles();

	/**
	 * Adds a discovered conf.
	 * If done explicitly after init, conf caches are rebuilt.
	 * @param confModel
	 */
	void addDiscoveredConf(ConfModel confModel);
	/**
	 * Adds a declared conf.
	 * If done explicitly after init, conf caches are rebuilt.
	 * TODO listener to plugin registry
	 * @param confModel
	 */
	void addDeclaredConf(ConfModel confModel);List<ConfModel> getDeclaredConfs();
	
	/**
	 * Returns all discovered confs
	 * @return
	 */
	List<ConfModel> getDiscoveredConfs();
	/**
	 * Returns all installed confs
	 * @return
	 */
	List<ConfModel> getInstalledConfs();
	
}
