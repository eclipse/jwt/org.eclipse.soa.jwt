/**
 * File:    ConfResourceException.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.resource;

/**
 * 
 * @version $Id: ConfResourceException.java,v 1.1 2010-05-10 08:27:22 chsaad Exp $
 * @author Marc Dutoo
 * @since 0.6
 */
public class ConfResourceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2936645513687546443L;

	public ConfResourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConfResourceException(String message) {
		super(message);
	}
	
	

}
