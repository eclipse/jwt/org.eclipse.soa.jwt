/**
 * File:    RegistryAspectFactory.java
 * Created: 08.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.aspects.factory;

import org.eclipse.jwt.we.conf.model.Aspect;


/**
 * Aspect factory that can be chosen by the AspectFactoryRegistry to instantiate a
 * given aspect
 * 
 * @version $Id: RegistryAspectFactory.java,v 1.1 2010-05-10 08:27:22 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 * @since 0.6
 */
public interface RegistryAspectFactory extends AspectFactory {

	/**
	 * @param aspect
	 * @return true if this factory is the one within the registry that can
	 * and should instanciate this aspect
	 */
	boolean isFactoryFor(Aspect aspect);
	
}
