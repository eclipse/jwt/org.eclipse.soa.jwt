/**
 * File:    AspectManager.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2009-2010 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.resource;

import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.jwt.we.conf.model.plugin.ConfPlugin;


/**
 * Look in child statuses for errors
 * 
 * @version $Id: ConfModelStatus.java,v 1.1 2010-05-10 08:27:22 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 * @since 0.7
 *
 */
public class ConfModelStatus extends MultiStatus {
	
	// error codes
	public static final int RESOURCE_NOT_ALREADY_LOADED = 101;
	public static final int RESOURCE_SUCCESSFULLY_PATCHED = 201;
	public static final int RESOURCE_DOES_NOT_EXIST = 401;
	public static final int RESOURCE_PATCHABLE_MISSING_EMF = 402;
	public static final int CONF_MODEL_PATCHABLE_NO_ENRICHED_MODEL_REFERENCE = 403;
	public static final int CONF_MODEL_PATCHABLE_REFERENCES_ANOTHER_ENRICHED_MODEL = 404;
	public static final int RESOURCE_CANT_BE_LOADED = 501;
	public static final int RESOURCE_LOADING_ERROR = 502;
	public static final int RESOURCE_PATCHING_ERROR = 503;
	public static final int CONF_MODEL_CREATION_ERROR = 504;
	public static final int CONF_MODEL_ANOTHER_ENRICHED_MODEL_MIGRATION_ERROR = 505;
	
	// load features
	private boolean createIfDoesntExist = false;
	private boolean tryGettingExistingOneFirst = false;
	private boolean bypassFlaggedUnavailable = false;
	private boolean tryPatchingMissingEmf = false;
	private boolean dontTryPatchingNoEnrichedModelReference = false;
	private boolean dontTryPatchingReferencesAnotherEnrichedModel = false;


	public ConfModelStatus() {
		super(ConfPlugin.CONF_PLUGIN_ID, 0, "See children statuses", null);
	}

	
	public ConfModelStatus(ConfModelStatus cmStatus) {
		this();
		this.createIfDoesntExist = cmStatus.createIfDoesntExist;
		this.tryGettingExistingOneFirst = cmStatus.tryGettingExistingOneFirst;
		this.bypassFlaggedUnavailable = cmStatus.bypassFlaggedUnavailable;
		this.tryPatchingMissingEmf = cmStatus.tryPatchingMissingEmf;
		this.dontTryPatchingNoEnrichedModelReference = cmStatus.dontTryPatchingNoEnrichedModelReference;
		this.dontTryPatchingReferencesAnotherEnrichedModel = cmStatus.dontTryPatchingReferencesAnotherEnrichedModel;
		this.createIfDoesntExist = cmStatus.createIfDoesntExist;
	}


	public boolean isCreateIfDoesntExist() {
		return createIfDoesntExist;
	}
	public void setCreateIfDoesntExist(boolean createIfDoesntExist) {
		this.createIfDoesntExist = createIfDoesntExist;
	}
	
	public boolean isTryGettingExistingOneFirst() {
		return tryGettingExistingOneFirst;
	}
	public void setTryGettingExistingOneFirst(boolean tryGettingExistingOneFirst) {
		this.tryGettingExistingOneFirst = tryGettingExistingOneFirst;
	}
	
	public boolean isBypassFlaggedUnavailable() {
		return bypassFlaggedUnavailable;
	}
	public void setBypassFlaggedUnavailable(boolean bypassFlaggedUnavailable) {
		this.bypassFlaggedUnavailable = bypassFlaggedUnavailable;
	}
	
	public boolean isTryPatchingMissingEmf() {
		return tryPatchingMissingEmf;
	}
	public void setTryPatchingMissingEmf(boolean tryPatchingMissingEmf) {
		this.tryPatchingMissingEmf = tryPatchingMissingEmf;
	}

	public boolean isDontTryPatchingNoEnrichedModelReference() {
		return dontTryPatchingNoEnrichedModelReference;
	}
	public void setDontTryPatchingNoEnrichedModelReference(
			boolean dontTryPatchingNoEnrichedModelReference) {
		this.dontTryPatchingNoEnrichedModelReference = dontTryPatchingNoEnrichedModelReference;
	}

	public boolean isDontTryPatchingReferencesAnotherEnrichedModel() {
		return dontTryPatchingReferencesAnotherEnrichedModel;
	}
	public void setDontTryPatchingReferencesAnotherEnrichedModel(
			boolean dontTryPatchingReferencesAnotherEnrichedModel) {
		this.dontTryPatchingReferencesAnotherEnrichedModel = dontTryPatchingReferencesAnotherEnrichedModel;
	}

}
