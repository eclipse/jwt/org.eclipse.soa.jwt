/**
 * File:    ConfModelSavingModelResourceAdapter.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.model.resource.internal;

import java.util.HashMap;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jwt.we.conf.model.plugin.ConfPlugin;
import org.eclipse.jwt.we.conf.model.resource.ConfModelResourceManager;


/**
 * Add it to a model's resource to save its confModel when the model is saved
 * @version $Id: ConfModelSavingModelResourceAdapter.java,v 1.1 2010-05-10 08:27:22 chsaad Exp $
 * @author mdutoo
 *
 */
public class ConfModelSavingModelResourceAdapter implements Adapter {

	private Resource confModelResource;
	private URI modelUri;
	
	private boolean disabled = false;
	
	/**
	 * 
	 * @param confModelResource to save
	 * @param modelUri2 for trace purpose
	 */
	public ConfModelSavingModelResourceAdapter(Resource confModelResource, URI modelUri) {
		this.confModelResource = confModelResource;
		this.modelUri = modelUri;
	}

	public Notifier getTarget() {
		return null;
	}
	
	public boolean isAdapterForType(Object type) {
		return type == ConfModelResourceManager.class;
	}
	
	public void notifyChanged(Notification notification) {
		if (disabled) {
			return;
		}

		// Notifications from a model resource are either their own save, or the save
		// of their confModel (which references them), so let's save these :
		if (notification.getFeatureID(Resource.class) == Resource.RESOURCE__IS_MODIFIED
				&& notification.getEventType() == Notification.SET
				&& notification.getNewBooleanValue() == false
				&& (notification.getNotifier() instanceof Resource)) {
			
			if (!modelUri.equals(((Resource) notification.getNotifier()).getURI())) {
				// [bugzilla #289035] model has been "saved as" a new model : let's move the
				// conf model resource at the right path before saving it
				// NB. its reference to the model will be updated when saved
				URI newModelUri = ((Resource) notification.getNotifier()).getURI();
				URI newConfModelUri = ((ConfModelResourceManagerImpl)
						ConfModelResourceManager.INSTANCE).buildConfModelUriForModel(newModelUri);
				confModelResource.setURI(newConfModelUri);
			} // else model has been saved through a simple "save", nothing special to do

			// now let's save the conf model
			try {
				// TODO put(XMLResource.OPTION_ENCODING, "UTF-8") ? dep on ecore.xmi
				confModelResource.save(new HashMap<String,String>());
			} catch (Exception ex) {
				ConfPlugin.logger.warning("Error saving ConfModel " + confModelResource.getURI() //$NON-NLS-1$
						+ " on change notification (meaning save) from model " + modelUri, ex); //$NON-NLS-1$
			}
		}
	}
	
	public void setTarget(Notifier newTarget) {
		
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public Resource getConfModelResource() {
		return confModelResource;
	}

	public URI getModelUri() {
		return modelUri;
	}

}
