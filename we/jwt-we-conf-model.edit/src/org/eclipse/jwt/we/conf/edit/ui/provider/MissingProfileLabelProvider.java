/**
 * File:    MissingProfileLabelProvider.java
 * Created: 08.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.conf.edit.ui.provider;

import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.jwt.we.conf.model.Profile;
import org.eclipse.jwt.we.conf.model.aspects.ConfRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;


/**
 * Specific label provider for displaying missing profiles. Displays them in red.
 * @version $Id: MissingProfileLabelProvider.java,v 1.1 2010-05-10 08:27:16 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class MissingProfileLabelProvider extends AdapterFactoryColumnLabelProvider.ColorProvider {

	public MissingProfileLabelProvider(AdapterFactory adapterFactory,
			Map<Integer, String> columnToAttributeNameMap) {
		super(adapterFactory, columnToAttributeNameMap);
	}

	@Override
	public Color getBackground(Object object, int columnIndex) {
		return this.getBackground(object);
	}

	@Override
	public Color getForeground(Object object, int columnIndex) {
		return this.getForeground(object);
	}

	@Override
	public Color getBackground(Object object) {
		if (object instanceof Profile) {
			String profileId = ((Profile) object).getName();
			if (ConfRegistry.INSTANCE.getInstalledProfile(profileId) == null) {
    			return Display.getCurrent().getSystemColor(SWT.COLOR_RED);	
			}
		}
		return super.getBackground(object);
	}

	@Override
	public Color getForeground(Object object) {
		if (object instanceof Profile) {
			String profileId = ((Profile) object).getName();
			if (ConfRegistry.INSTANCE.getInstalledProfile(profileId) == null) {
				return Display.getCurrent().getSystemColor(SWT.COLOR_WHITE); 
			}
		}
		return super.getBackground(object);
	}
	
}
