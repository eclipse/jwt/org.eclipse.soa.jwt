/**
@ File:    messages.properties
# Created: 30.01.2009
#
#
#/*******************************************************************************
# * Copyright (c) 2009  Open Wide <www.openwide.fr>
# *
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Mickael Istria, Open Wide, Lyon, France
# *      - creation and impl
# *******************************************************************************/


package org.eclipse.jwt.we.conf.edit.ui.internal;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	private static final String BUNDLE_NAME="org.eclipse.jwt.we.conf.edit.ui.messages"; //$NON-NLS-1$

	public static String Success;
	public static String noMissingProfilesTitle;
	public static String advertUseEmbeddedProfiles;
	public static String missingProfilesWarningTitle;
	public static String missingProfilesEmbeddedConfText;
	public static String missingProfilesNotEmbeddedConfText;
	public static String noWorkflowModelSelected;
	public static String enableProfile;
	public static String profileDisabled;
	public static String enable;
	public static String warningAndAdvice;
	public static String exportMissingProfiles;
	public static String choose;
	public static String exportToConfFile;
	public static String developmentTools;
	public static String usingEmbeddedProfiles;
	public static String detailAvailableProfiles;
	public static String missingProfiles;
	public static String installedProfiles;
	public static String embeddedProfiles;
	public static String description;
	public static String version;
	public static String disableProfiles;
	public static String importSuccess;
	public static String importError;
	public static String enablementError;
	public static String confirmConfRemoval_Title;
	public static String confirmConfRemoval_Body;
	public static String confModelNotDeleted;
	public static String errorDisablingProfiles;
	public static String activate;
	public static String disableProfiles_Title;
	public static String disable;
	public static String disableProfiles_Body;
	public static String errorCreatingOrReloadingConfModel;

	public static String confirmTryPatching_title;
	public static String confirmTryPatching_body;
	public static String errorPatching_title;
	public static String errorPatching_body;
	public static String noPatchApplied_title;
	public static String noPatchApplied_body;
	public static String errorLoadingConfModel_title;
	public static String errorLoadingConfModel_body;
	public static String successfullyPatched_title;
	public static String successfullyPatched_body;
	public static String showStatusDetails_button;
	public static String statusDetails_title;
	public static String statusDetails_body;
	public static String statusDetails_body_nodetails;
	public static String statusDetails_body_details;
	
	
	static {
	    NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}
}
