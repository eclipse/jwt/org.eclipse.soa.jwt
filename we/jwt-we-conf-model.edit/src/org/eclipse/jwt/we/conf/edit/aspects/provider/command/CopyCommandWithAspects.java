/**
 * File:    CopyCommandWithAspects.java
 * Created: 10.05.2010
 *
 *
/*******************************************************************************
 * Copyright (c) 2010 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.edit.aspects.provider.command;

import java.util.List;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.CopyCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.we.conf.model.AspectInstance;
import org.eclipse.jwt.we.conf.model.aspects.AspectManager;


/**
 * To an element's copy, adds copy of its aspects
 * 
 * @version $Id: CopyCommandWithAspects.java,v 1.1 2010-05-25 19:20:53 mdutoo Exp $
 * @author Marc Dutoo, Open Wide SA
 */
public class CopyCommandWithAspects extends CopyCommand {


	public CopyCommandWithAspects(EditingDomain domain, EObject owner,
			Helper copyHelper) {
		super(domain, owner, copyHelper);
	}


	protected void addCreateCopyCommands(CompoundCommand compoundCommand, EObject eObject) {
		// adding createCopyCommands for aspect instances
		List<AspectInstance> aspectInstances = AspectManager.INSTANCE.getAspectInstances(eObject);
		if (aspectInstances != null && !aspectInstances.isEmpty()) {
			// ensuring there is a mergeable compound command in which to add aspects
			CompoundCommand multipleCompoundCommand = compoundCommand;
			if (compoundCommand.getResultIndex() != MERGE_COMMAND_ALL) {
				multipleCompoundCommand = new CompoundCommand(MERGE_COMMAND_ALL);
				compoundCommand.append(multipleCompoundCommand);
				CompoundCommand originalCompoundCommand  = new CompoundCommand(compoundCommand.getResultIndex());
				multipleCompoundCommand.append(originalCompoundCommand);
				compoundCommand = originalCompoundCommand;
			}

			for (AspectInstance aspectInstance : aspectInstances) {
				super.addCreateCopyCommands(multipleCompoundCommand, aspectInstance);
			}

		}
		// else TODO eObject already a copy !!?? i.e. its copied aspects are in its copy command result OR clipboard

		super.addCreateCopyCommands(compoundCommand, eObject);
	}
	  
}
