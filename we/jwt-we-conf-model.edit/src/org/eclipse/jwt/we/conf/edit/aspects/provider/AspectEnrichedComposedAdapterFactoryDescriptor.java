/**
 * File:    AspectEnrichedComposedAdapterFactoryDescriptor.java
 * Created: 11.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *    Marc Dutoo, Open Wide, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.edit.aspects.provider;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;


/**
 * Able to create a new composedadapterfactory based on the
 * EMF registry with a reflectiveitemprovideradapterfactory.
 * 
 * @deprecated not used
 * 
 * @version $Id: AspectEnrichedComposedAdapterFactoryDescriptor.java,v 1.1 2010-05-10 08:27:16 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA, France <www.openwide.fr>
 * @since 0.6
 */
public class AspectEnrichedComposedAdapterFactoryDescriptor
		implements ComposedAdapterFactory.Descriptor {

	/**
	 * Creates a new DefaultComposedAdapterFactory 
	 */
	public AdapterFactory createAdapterFactory() {
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
		return adapterFactory;
	}

}
