/**
 * File:    ClassViewFilter.java
 * Created: 25.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.edit.ui.viewer;

import java.util.ArrayList;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

/**
 * A filter that hides classes of the specified types on the viewer
 * that it is added on.
 * 
 * TODO possible perf improvement : cache isAssignableFrom results
 * 
 * @version $Id: ClassViewFilter.java,v 1.1 2010-05-10 08:27:16 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA, France, <www.openwide.fr>
 *
 */
public class ClassViewFilter extends ViewerFilter {

	/**
	 * The list of element types which are filtered out.
	 */
	private ArrayList<Class<?>> hiddenTypes;
	
	/**
	 * Creates a new ClassViewFilter
	 */
	public ClassViewFilter(ArrayList<Class<?>> hiddenTypes) {
		this.hiddenTypes = hiddenTypes;
	}
	
	/**
	 * Creates a new ClassViewFilter
	 */
	public ClassViewFilter(Class<?> ... hiddenTypes) {
		this.hiddenTypes = new ArrayList<Class<?>>(hiddenTypes.length);
		for (Class<?> hiddenType : hiddenTypes) {
			this.hiddenTypes.add(hiddenType);
		}
	}
	
	
	/**
	 * Returns whether the given element makes it through this filter.
	 * 
	 * @param viewer the viewer
	 * @param parentElement the parent element
	 * @param element the element
	 * @return <code>true</code> if element class is assignable from a class
	 * included in the filtered set, and <code>false</code> if excluded
	 */
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		// hide element if its type is in hiddenTypes
		for (Class<?> classType : hiddenTypes) {
			if (classType.isAssignableFrom(element.getClass())) {
				return false;
			}
		}

		return true;
	}
}
