/**
 * File:    CreateChildCommandWithAspects.java
 * Created: 10.05.2010
 *
 *
/*******************************************************************************
 * Copyright (c) 2010 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.edit.aspects.provider.command;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.we.conf.model.Aspect;
import org.eclipse.jwt.we.conf.model.ConfModel;
import org.eclipse.jwt.we.conf.model.aspects.AspectManager;
import org.eclipse.jwt.we.conf.model.plugin.ConfPlugin;

/**
 * Adds auto created aspects to CreateChild.
 * 
 * @version $Id: CreateChildCommandWithAspects.java,v 1.1 2010-05-25 19:20:53 mdutoo Exp $
 * @author Marc Dutoo, Open Wide SA
 */
public class CreateChildCommandWithAspects extends CreateChildCommand {

	  public CreateChildCommandWithAspects(EditingDomain domain,
	                            EObject owner,
	                            EStructuralFeature feature,
	                            Object child,
	                            int index,
	                            Collection<?> selection,
	                            CreateChildCommand.Helper helper) {
		super(domain, owner, feature, child, index, selection, helper);
	}

	  
		@Override
		public void execute() {
			// creating the model element as usually
			super.execute();
			
			// adding automatically created aspects
			doCreateChildAspects();
		}

		@Override
		public void redo() {
			// creating the model element as usually
			super.redo();

			// adding automatically created aspects
			doCreateChildAspects();
		}
		
		protected void doCreateChildAspects() {
		  if (this.child instanceof EObject) {
			  // an element of the aspect'd model has been created, 
			  // let's handle aspect behaviour :
			  // TODO better instance injection
			  ConfPlugin.getDefaultAspectEventManager().onCreated((EObject) this.child);
		  } else {
			  // TODO log
		  }
		}

		@Override
		public void undo() {
			if (this.child instanceof EObject) {
				// removing aspects of the model element
				List<Aspect> aspects = AspectManager.INSTANCE.getAspects((EObject) this.child);
				if (aspects != null && !aspects.isEmpty()) {
					ConfModel confModel = (ConfModel) aspects.iterator().next().eContainer();
					if (confModel != null) {
						confModel.getAspectInstances().removeAll(aspects);
					} // else TODO log
				}
			} // else TODO log
			
			// removing the model element as usually as usually
			super.undo();
		}
		
}
