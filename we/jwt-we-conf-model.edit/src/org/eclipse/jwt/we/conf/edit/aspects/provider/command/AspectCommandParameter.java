/**
 * File:    AspectCommandParameter.java
 * Created: 08.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.edit.aspects.provider.command;

import java.util.ArrayList;

import org.eclipse.emf.edit.command.CommandParameter;


/**
 * CommandParamter with the id of the aspect it applies to
 * @version $Id: AspectCommandParameter.java,v 1.1 2010-05-10 08:27:16 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class AspectCommandParameter extends CommandParameter {

	private String aspectId;

	public String getAspectId() {
		return aspectId;
	}

	public AspectCommandParameter(Object owner, Object feature, Object value, String aspectId) {
		super(owner, feature, value);
		this.aspectId = aspectId;
	}

	public AspectCommandParameter(Object owner, Object feature,
			Object newChildDescriptor, ArrayList<Object> selection,
			String aspectId) {
		super(owner, feature, newChildDescriptor, selection);
		this.aspectId = aspectId;
	}

}
