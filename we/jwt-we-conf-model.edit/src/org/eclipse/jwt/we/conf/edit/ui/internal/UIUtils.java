/**
 * File:    UIUtils.java
 * Created: 08.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.conf.edit.ui.internal;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;


/**
 * Helper giving access to current parts of Eclipse 
 * @version $Id: UIUtils.java,v 1.1 2010-05-10 08:27:15 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class UIUtils {

	
	/**
	 * Helper
	 * @return
	 */
	public static IWorkbenchWindow getActiveWorkbenchWindow() {
		IWorkbench iwb = PlatformUI.getWorkbench();
		if (iwb == null)
			return null;
		return iwb.getActiveWorkbenchWindow();
	}
	
	/**
	 * Helper
	 * @return
	 */
	public static Shell getActiveShell() {
		IWorkbenchWindow iwbw = getActiveWorkbenchWindow();
		if (iwbw == null)
			return null;
		return iwbw.getShell();
	}
	
	/**
	 * Helper
	 * @return
	 */
	public static IEditorPart getActiveEditor() {
		IWorkbenchWindow iwbw = getActiveWorkbenchWindow();
		if (iwbw == null)
			return null;
		IWorkbenchPage page = iwbw.getActivePage();
		if (page == null)
			return null;
		return page.getActiveEditor();
	}
	
}
