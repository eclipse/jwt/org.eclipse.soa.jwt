/**
 * File:    ConfItemProviderAdapterFactory.java
 * Created: 10.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.conf.edit.providerdecorator;

import org.eclipse.emf.common.notify.Adapter;


/**
 * Enriches the generated ConfItemProviderAdapterFactory with conf (profile
 * / aspect) features.
 * 
 * @version $Id: ConfItemProviderAdapterFactory.java,v 1.1 2010-05-10 08:27:16 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class ConfItemProviderAdapterFactory extends
		org.eclipse.jwt.we.conf.edit.provider.ConfItemProviderAdapterFactory {

	/**
	 * Creates a new ConfItemProviderAdapterFactory.
	 */
	public ConfItemProviderAdapterFactory() {
		super();
	}


	/**
	 * Decorates the ConfModel with the ability to notify the model rootAdapterFactory
	 * of changes that impacts it (concretely, AspectInstance additions and deletions).
	 */
	@Override
	public Adapter createConfModelAdapter() {
		if (confModelItemProvider == null) {
			confModelItemProvider = new ConfModelItemProvider(this);
		}

		return confModelItemProvider;
	}

	/**
	 * Decorates the ConfModel with helping for profile and aspect names
	 */
	@Override
	public Adapter createProfileAdapter() {
		if (profileItemProvider == null) {
			profileItemProvider = new ProfileItemProvider(this);
		}

		return profileItemProvider;
	}
	
}
