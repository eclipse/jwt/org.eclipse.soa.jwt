/**
 * File:    CreateChildAspectCommand.java
 * Created: 08.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.edit.aspects.provider.command;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.EMFEditPlugin;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.we.conf.edit.aspects.provider.AspectEnrichedItemProviderAdapter;


/**
 * Command that creates a new aspect instance on the target element
 * @version $Id: CreateChildAspectCommand.java,v 1.1 2010-05-10 08:27:16 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class CreateChildAspectCommand extends CreateChildCommand implements
		Command {

	private String aspectId;

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return super.getDescription();
	}

	@Override
	public Object getImage() {
		// TODO Auto-generated method stub
		return super.getImage();
	}

	@Override
	public Collection<?> getResult() {
		// TODO Auto-generated method stub
		return super.getResult();
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		//return super.getText();
		return aspectId;
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return super.getToolTipText();
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		//return super.getLabel();
		return aspectId;
	}

	public CreateChildAspectCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Object value, int index,
			Collection<?> collection,
			AspectEnrichedItemProviderAdapter aspectEnrichedItemProviderAdapter,
			String aspectId) {
		super(domain, owner, feature, value, index, collection, aspectEnrichedItemProviderAdapter);
		this.aspectId = aspectId;

	    //String text = this.helper.getCreateChildText(owner, feature, child, selection);
		String text = aspectId;
	    setLabel(EMFEditPlugin.INSTANCE.getString(
	      "_UI_CreateChildCommand_label", new Object[] { text })); //$NON-NLS-1$
	    // TODO override createChildText etc.
	}
	
	  /**
	   * This returns a command created by the editing domain to add the
	   * child described by <code>newChildDescriptor</code> to the given
	   * <code>object</code>.
	   */
	  public static Command create(EditingDomain domain, Object owner,
	                               Object newChildDescriptor,
	                               Collection<?> selection)
	  {
		  if (newChildDescriptor instanceof AspectCommandParameter) {
			  String aspectId = ((AspectCommandParameter) newChildDescriptor).getAspectId();
			    return 
			      domain.createCommand
			        (CreateChildCommand.class, 
			         new AspectCommandParameter(owner, null, newChildDescriptor, new ArrayList<Object>(selection), aspectId));
			  }
		  // TODO better
	    System.err.println(Thread.currentThread().getStackTrace()[0].toString());//
		  return null;
	  }

	public String getAspectId() {
		return aspectId;
	}

}
