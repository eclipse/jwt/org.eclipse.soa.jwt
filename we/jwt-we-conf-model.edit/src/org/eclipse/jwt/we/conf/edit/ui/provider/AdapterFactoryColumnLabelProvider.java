/**
 * File:    AdapterFactoryColumnLabelProvider.java
 * Created: 08.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.conf.edit.ui.provider;

import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.IFontProvider;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableFontProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;


/**
 * UI Helper that provides table columns from mapped EMF features
 * @version $Id: AdapterFactoryColumnLabelProvider.java,v 1.1 2010-05-10 08:27:16 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class AdapterFactoryColumnLabelProvider extends AdapterFactoryLabelProvider
		implements ITableLabelProvider {
	
	/**
     * An extended version of the adapter factory label provider that also provides for colors.
     */
	public static class ColorProvider extends AdapterFactoryColumnLabelProvider implements IColorProvider, IFontProvider, ITableColorProvider, ITableFontProvider {

		public ColorProvider(AdapterFactory adapterFactory,
				Map<Integer, String> columnToAttributeNameMap) {
			super(adapterFactory, columnToAttributeNameMap);
		}
		
	}
	
	
	private Map<Integer, String> columnToAttributeNameMap;

	public AdapterFactoryColumnLabelProvider(AdapterFactory adapterFactory,
			Map<Integer, String> columnToAttributeNameMap) {
		super(adapterFactory);
		this.columnToAttributeNameMap = columnToAttributeNameMap;
	}

	@Override
	public Image getColumnImage(Object object, int columnIndex) {
		String columnAttributeName = columnToAttributeNameMap.get(columnIndex);
		if (columnAttributeName != null && object instanceof EObject) {
	    	return null;	
	    }
	    return super.getColumnImage(object, columnIndex); 
	}

	@Override
	public String getColumnText(Object object, int columnIndex) {
		String columnAttributeName = columnToAttributeNameMap.get(columnIndex);
		if (columnAttributeName != null && object instanceof EObject) {
	    	EObject eObject = (EObject) object;
	    	EStructuralFeature columnAttribute = eObject.eClass().getEStructuralFeature(columnAttributeName);
	    	if (columnAttribute == null) {
	    		return null;
	    	}
	    	Object columnAttributeValue = ((EObject) object).eGet(columnAttribute);
	    	return (columnAttributeValue == null) ? "" : String.valueOf(columnAttributeValue); //$NON-NLS-1$
	    }
	    return super.getColumnText(object, columnIndex);
	}

}
