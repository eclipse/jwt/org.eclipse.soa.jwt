/**
 * File:    ConfModelItemProvider.java
 * Created: 10.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.conf.edit.providerdecorator;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.we.conf.model.AspectInstance;
import org.eclipse.jwt.we.conf.model.ConfModel;
import org.eclipse.jwt.we.conf.model.ConfPackage;


/**
 * Decorates the ConfModelItemProvider with additional features. Notably :
 *    * to notify the model rootAdapterFactory of changes that impacts it.
 * Those changes are AspectInstance additions and deletions, which therefore
 * allow an AspectInstance that has been deleted to be removed from under
 * its decorated EObject (resp. created, added).
 * 
 * @version $Id: ConfModelItemProvider.java,v 1.1 2010-05-10 08:27:16 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 */
public class ConfModelItemProvider extends
		org.eclipse.jwt.we.conf.edit.provider.ConfModelItemProvider {

	private ComposeableAdapterFactory composeableAdapterFactory;

	public ConfModelItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
		this.composeableAdapterFactory = (ComposeableAdapterFactory) adapterFactory;
	}

	
	/**
	 * Forwards notifications of changes that impact the decorated model
	 * to the decorated model's appropriate ItemProvider.
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ConfModel.class)) {
			case ConfPackage.CONF_MODEL__ASPECT_INSTANCES:
				EObject targetElement = null; 
				if (notification.getEventType() == Notification.ADD
							&& notification.wasSet() && !notification.isTouch()) {
					targetElement = ((AspectInstance) notification.getNewValue()).getTargetModelElement();
				} else if (notification.getEventType() == Notification.REMOVE
						&& notification.wasSet() && !notification.isTouch()) {
					targetElement = ((AspectInstance) notification.getOldValue()).getTargetModelElement();
				}
				if (targetElement != null) {
					ItemProviderAdapter targetElementItemProviderAdapter = (ItemProviderAdapter)
							this.composeableAdapterFactory.getRootAdapterFactory().adapt(targetElement, targetElement); // NB. IChangeNotifier would be enough
					targetElementItemProviderAdapter.fireNotifyChanged(new ViewerNotification(notification, targetElement, true, false));
				}
				return;
		}
		
		// letting super do its work
		super.notifyChanged(notification);
	}
}
