/**
 * File:    ManageActivatedProfileDialog.java
 * Created: 10.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.conf.edit.ui;

import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jwt.we.conf.edit.ui.internal.ManageSelectionActivatedProfilesUI;
import org.eclipse.jwt.we.conf.edit.ui.internal.UIUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;


/**
 * Dialog for ManageActivatedProfileUI.
 * 
 * @version $Id: ManageActivatedProfileDialog.java,v 1.1 2010-05-10 08:27:15 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 * @since 0.6
 */
public class ManageActivatedProfileDialog extends Dialog {

	private AdapterFactory adapterFactory;
	private EObject modelRoot;
	private boolean onSelection;
	private CommandStack commandStack;
	
	private ManageActivatedProfilesUI profileUi;

	/**
	 * 
	 * @param adapterFactory 
	 * @param weEditor if null, non-modal and selection listening mode ;
	 * else, modal and fixed model mode
	 */
	public ManageActivatedProfileDialog(AdapterFactory adapterFactory, EObject modelRoot, CommandStack commandStack, boolean onSelection) {
		super((UIUtils.getActiveShell() != null)
				? UIUtils.getActiveShell() : new Shell(Display.getCurrent())); // SWT.APPLICATION_MODAL
		this.adapterFactory = adapterFactory;
		this.modelRoot = modelRoot;
		this.onSelection = onSelection;
		this.commandStack = commandStack;
		
		if (onSelection) {
			// non modal in listening mode : reset shellStyle without model bit
			// also set the resize bit so it is resizable
			setShellStyle(SWT.DIALOG_TRIM | SWT.MAX | SWT.RESIZE
					| getDefaultOrientation());
		}
	}

	
	protected Control createDialogArea(Composite parent) {
		if (onSelection) {
			profileUi = new ManageSelectionActivatedProfilesUI(adapterFactory) {
				public void refresh() {
					super.refresh();
					getButton(IDialogConstants.OK_ID).setEnabled(getMissingProfiles().isEmpty() || useEmbeddedConf());
					//getShell().setSize(getShell().computeSize(SWT.DEFAULT, SWT.DEFAULT)); // resize TODO doesn't work
				}
			};
		} else {
			profileUi = new ManageActivatedProfilesUI(adapterFactory);
			profileUi.setSelectedModel(modelRoot);
		}
		if (modelRoot != null) {
			profileUi.setSelectedModel(modelRoot);
		}
		Composite composite = profileUi.createControl(parent, commandStack);
		// no refresh yet, else OK button not ready
		
		applyDialogFont(composite);
		return composite;
	}


	@Override
	protected Control createContents(Composite parent) {
		Control control = super.createContents(parent);
		profileUi.refresh(); // Now OK button ready
		this.getButton(IDialogConstants.OK_ID).setFocus();
		return control;
	}

	
	public boolean close() {
		if (profileUi instanceof ManageSelectionActivatedProfilesUI) {
			((ManageSelectionActivatedProfilesUI) profileUi).dispose();
		}
		return super.close();
	}
	
}
