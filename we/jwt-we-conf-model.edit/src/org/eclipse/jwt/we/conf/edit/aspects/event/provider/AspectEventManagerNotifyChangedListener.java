/**
 * File:    AspectEventManagerNotifyChangedListener.java
 * Created: 08.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2010 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.edit.aspects.event.provider;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.jwt.we.conf.model.aspects.event.AspectEventManager;


/**
 * [#311818] 2010/05/06 Moved from org.eclipse.jwt.we.conf.aspects.event to avoid package conflict at build time
 * @deprecated not used
 * @version $Id: AspectEventManagerNotifyChangedListener.java,v 1.1 2010-05-10 08:27:16 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class AspectEventManagerNotifyChangedListener implements INotifyChangedListener {
	
	public static final AspectEventManagerNotifyChangedListener DEFAULT_INSTANCE = new AspectEventManagerNotifyChangedListener();
	
	private final AspectEventManager aspectEventAdapter = new AspectEventManager();

	public void notifyChanged(Notification notification) {
		this.aspectEventAdapter.notifyChanged(notification);
	}

}
