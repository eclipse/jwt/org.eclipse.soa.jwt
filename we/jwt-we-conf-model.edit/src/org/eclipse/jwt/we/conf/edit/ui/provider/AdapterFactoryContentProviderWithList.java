/**
 * File:    ManageActivatedProfilesAction.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.conf.edit.ui.provider;

import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;


/**
 * Adds List handling to AdapterFactoryContentProvider's handling of EObject.
 * Typically used to provide content for a list of EMF objects.
 * 
 * @version $Id: AdapterFactoryContentProviderWithList.java,v 1.2 2010-05-27 15:42:49 mdutoo Exp $
 * @author Marc Dutoo, Open Wide SA
 */
public class AdapterFactoryContentProviderWithList extends AdapterFactoryContentProvider
		implements ITreeContentProvider {

	public AdapterFactoryContentProviderWithList(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	@SuppressWarnings("unchecked")
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof List) {
			return ((List) parentElement).toArray();
		}
		return super.getChildren(parentElement);
	}

	@SuppressWarnings("unchecked")
	public Object getParent(Object element) {
		if (element instanceof List) {
			return null;
		}
		return super.getParent(element);
	}

	@SuppressWarnings("unchecked")
	public boolean hasChildren(Object element) {
		if (element instanceof List) {
			return !((List) element).isEmpty();
		}
		return super.hasChildren(element);
	}

	@SuppressWarnings("unchecked")
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List) {
			return ((List) inputElement).toArray();
		}
		return super.getChildren(inputElement);
	}

}
