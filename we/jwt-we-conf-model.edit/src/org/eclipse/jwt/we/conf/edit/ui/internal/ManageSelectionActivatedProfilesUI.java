/**
 * File:    ManageSelectionActivatedProfilesUI.java
 * Created: 26.02.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.conf.edit.ui.internal;

import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.we.conf.edit.ui.ManageActivatedProfilesUI;
import org.eclipse.jwt.we.conf.model.aspects.AspectManager;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;


/**
 * Manages activated profiles for the selected model..
 * 
 * Makes it work for different weEditors by overriding getWeEditor(), and
 * listening to selection changes.
 * 
 * @version $Id: ManageSelectionActivatedProfilesUI.java,v 1.1 2010-05-10 08:27:15 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 */
public class ManageSelectionActivatedProfilesUI extends ManageActivatedProfilesUI
		implements ISelectionListener {

	/**
	 * @param adapterFactory 
	 * @param weEditor if null, standard super ManageActivatedProfilesUI
	 * behaviour. Else manages profiles of selected model and editor.
	 */
	public ManageSelectionActivatedProfilesUI(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}
	
	public Composite createControl(Composite parent, CommandStack commandStack) {
		Composite composite = super.createControl(parent, commandStack);

		// works
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().addSelectionListener(this);
		
		// doesn't work : only on start editor
		/*this.getWEEditor().getSelectionProvider().addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				ManageActivatedProfilesAction.this.selectionChanged(null, event.getSelection());
				//ManageActivatedProfilesAction.this.selectionChanged(getWEEditor(), event.getSelection());
			}
			
		});*/
		
		return composite;
	}

	
	/**
	 * Refreshes the UI when the selected model changes
	 */
    public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		Object selectedObject = null;
    	EObject model = null;
    	
    	if (selection instanceof IStructuredSelection) {
    		// getting selected model
    		selectedObject = ((IStructuredSelection) selection).getFirstElement();
    		model = AspectManager.INSTANCE.getModelRoot(selectedObject); // TODO no cast !
    		if (selectedObject != null && model == null) {
        		return; // selection is not within a workflow model, nothing to do
    		}
    		
    	} else if (!selection.isEmpty()) {
    		return; // unknown selection, unrelated to workflow model, nothing to do
    	}

		if (model != this.getSelectedModel()) {
			this.setSelectedModel(model);
			this.refresh();
		}
    }
   
    
    /**
     * Removes the selection listener. To be called by the wrapping UI.
     */
    @Override
    public void dispose() {
    	PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().removeSelectionListener(this);
    }

}
