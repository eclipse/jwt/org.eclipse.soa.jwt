/**
 * File:    ScopeFigure.java
 * Created: 19.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;


/**
 * Figure of a {@link Scope}.
 * 
 * @version $Id: ScopeFigure.java,v 1.7 2009-11-26 12:41:06 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ScopeFigure
		extends FreeformLayer
{

	/**
	 * Creates a new figure.
	 */
	public ScopeFigure()
	{
		setLayoutManager(new FreeformLayout());
	}
}
