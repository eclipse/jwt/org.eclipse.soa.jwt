/**
 * File:    IFigureFactory.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.figures;

import org.eclipse.draw2d.IFigure;


/**
 * An interface for a figure factory.
 * 
 * @version $Id: IFigureFactory.java,v 1.5 2009-11-26 12:41:49 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public interface IFigureFactory
{

	/**
	 * This method is invoked with a java class and must return the IFigure for this class
	 * type or null.
	 * 
	 * @param modelType
	 * @return The figure
	 */
	public IFigure createFigure(Class modelType);
}
