/**
 * File:    FinalNodeFigure.java
 * Created: 23.01.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.jwt.meta.model.processes.FinalNode;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;


/**
 * Figure for a {@link FinalNode}.
 * 
 * It displays a filled circle surrounded by another circle.
 * 
 * @version $Id: FinalNodeFigure.java,v 1.10 2009-11-26 12:41:12 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class FinalNodeFigure
		extends InitialNodeFigure
{

	/**
	 * Inner Ellipse.
	 */
	private Ellipse innerEllipse = new Ellipse();


	/**
	 * Creates the figure.
	 */
	public FinalNodeFigure()
	{
		ToolbarLayout layout = new ToolbarLayout();
		layout.setMinorAlignment(ToolbarLayout.ALIGN_CENTER);
		setLayoutManager(new StackLayout());

		innerEllipse.setBackgroundColor(PreferenceReader.appearanceBorderColor.get());
		innerEllipse.setOutline(false);
		innerEllipse.setOpaque(false);

		add(innerEllipse);
	}
}
