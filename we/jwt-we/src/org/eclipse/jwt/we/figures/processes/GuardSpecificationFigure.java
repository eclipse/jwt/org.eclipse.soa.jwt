/**
 * File:    GuardSpecificationFigure.java
 * Created: 29.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.Figure;
import org.eclipse.jwt.meta.model.processes.GuardSpecification;


/**
 * Figure for a {@link GuardSpecification}.
 * 
 * It just displays the name of a model element.
 * 
 * @version $Id: GuardSpecificationFigure.java,v 1.7 2009-11-26 12:41:12 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class GuardSpecificationFigure
		extends Figure
{

	// /**
	// * Creates the figure.
	// */
	// public GuardSpecificationFigure()
	// {
	// super();
	//
	// super.setMinimumSize(new Dimension(0, 0));
	// }
}
