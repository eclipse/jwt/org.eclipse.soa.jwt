/**
 * File:    ApplicationFigure.java
 * Created: 12.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Florian Lautenbacher: own figure for Application, Data and Role
 *******************************************************************************/


package org.eclipse.jwt.we.figures.application;

import org.eclipse.draw2d.Graphics;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.figures.core.NamedIconFigure;

/**
 * Figure for an {@link Application}.
 * @since 0.6.0
 */
public class ApplicationFigure
		extends NamedIconFigure
{

	/**
	 * Creates the figure.
	 */
	public ApplicationFigure()
	{
		super();
		setFont(PreferenceReader.appearanceReferenceableElementFont.get());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.ModelElementFigure#paint(org.eclipse.draw2d.Graphics)
	 */
	@Override
	public void paint(Graphics graphics)
	{
		super.paint(graphics);
	}
}
