/**
 * File:    CombinedImageDescriptor.java
 * Created: 22.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.figures.internal;

import java.util.Arrays;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;

/**
 * ImageDescriptor that uses several other ImageDescriptors to create the image.
 * 
 * <p>
 * All ImageDescriptors are asked in sequence. The first image that is not
 * <code>null</code> is returned.
 * </p>
 * 
 * @version $Id: CombinedImageDescriptor.java,v 1.3 2009/11/26 12:41:39 chsaad
 *          Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 */
public class CombinedImageDescriptor extends ImageDescriptor
{

	/**
	 * The ImageDescriptors.
	 * 
	 * <p>
	 * Never <code>null</code>.
	 * </p>
	 */
	private ImageDescriptor[] imageDescriptors;


	/**
	 * @param imageDescriptors
	 *            The ImageDescriptors.
	 */
	public CombinedImageDescriptor(ImageDescriptor[] imageDescriptors)
	{
		assert imageDescriptors != null;
		this.imageDescriptors = imageDescriptors;
	}


	/**
	 * @return Returns the imageDescriptors.
	 */
	public ImageDescriptor[] getImageDescriptors()
	{
		return imageDescriptors;
	}


	/**
	 * Asks all {@link #imageDescriptors} to create an image.
	 * 
	 * @see org.eclipse.jface.resource.ImageDescriptor#createImage(boolean,
	 *      org.eclipse.swt.graphics.Device)
	 */
	@Override
	public Image createImage(boolean returnMissingImageOnError, Device device)
	{
		for (ImageDescriptor imageDescriptor : imageDescriptors)
		{
			if (imageDescriptor != null)
			{
				Image image;
				try
				{
					image = Plugin.getDefault().getFactoryRegistry().getImageFactory()
							.getImage(imageDescriptor);
					if (image != null)
					{
						return image;
					}
				}
				catch (RuntimeException e)
				{
					// create failed, try next descriptor
				}
			}
		}

		if (returnMissingImageOnError)
		{
			return Plugin.getDefault().getFactoryRegistry().getImageFactory().getImage(
					getMissingImageDescriptor());
		}

		return null;
	}


	/**
	 * Asks all {@link #imageDescriptors} to create the image data.
	 * 
	 * @see org.eclipse.jface.resource.ImageDescriptor#getImageData()
	 */
	@Override
	public ImageData getImageData()
	{
		for (ImageDescriptor imageDescriptor : imageDescriptors)
		{
			if (imageDescriptor != null)
			{
				ImageData imageData;

				try
				{
					imageData = imageDescriptor.getImageData();
					if (imageData != null)
					{
						return imageData;
					}
				}
				catch (Exception e)
				{
					// failed, try next descriptor
				}
			}
		}

		return null;
	}


	/* (non-Javadoc)
	 * @see org.eclipse.jface.resource.ImageDescriptor#createImage()
	 */
	@Override
	public Image createImage()
	{
		return createImage(true, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return Arrays.toString(imageDescriptors);
	}
}
