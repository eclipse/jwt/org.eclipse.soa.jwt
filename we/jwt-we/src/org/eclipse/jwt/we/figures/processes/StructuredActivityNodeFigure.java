/**
 * File:    StructuredActivityNodeFigure.java
 * Created: 19.05.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.FreeformViewport;
import org.eclipse.draw2d.GroupBoxBorder;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LabeledBorder;
import org.eclipse.draw2d.ScrollPane;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.jwt.meta.model.processes.StructuredActivityNode;
import org.eclipse.jwt.we.figures.core.NamedElementFigure;


/**
 * Figure for a {@link StructuredActivityNode}.
 * 
 * @version $Id: StructuredActivityNodeFigure.java,v 1.7 2009-11-26 12:41:07 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class StructuredActivityNodeFigure
		extends NamedElementFigure
{

	/**
	 * The content pane for the child figures.
	 */
	private IFigure contentPane;

	/**
	 * Displays the name of the node.
	 */
	private LabeledBorder title = new GroupBoxBorder();


	/**
	 * Creates the figure.
	 */
	public StructuredActivityNodeFigure()
	{
		// dont create a label
		super(null);

		setBorder(title);

		ScrollPane scrollpane = new ScrollPane();
		contentPane = new FreeformLayer();
		contentPane.setLayoutManager(new FreeformLayout());
		setLayoutManager(new StackLayout());
		add(scrollpane);
		scrollpane.setViewport(new FreeformViewport());
		scrollpane.setContents(contentPane);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.ModelElementFigure#getContentPane()
	 */
	@Override
	public IFigure getContentPane()
	{
		return contentPane;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.NamedElementFigure#setName(java.lang.String)
	 */
	@Override
	public void setName(String name)
	{
		title.setLabel(name);
	}

}
