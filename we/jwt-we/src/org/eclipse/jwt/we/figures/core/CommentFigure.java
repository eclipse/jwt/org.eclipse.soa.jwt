/**
 * File:    CommentFigure.java
 * Created: 22.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.core;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.geometry.Dimension;


/**
 * Figure for a {@link Comment}.
 * 
 * @version $Id: CommentFigure.java,v 1.7 2009-11-26 12:41:02 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class CommentFigure
		extends NamedElementFigure
{

	/**
	 * Creates the figure.
	 */
	public CommentFigure()
	{
		setBackgroundColor(ColorConstants.tooltipBackground);
		setForegroundColor(ColorConstants.tooltipForeground);
		setBorder(new CommentBorder());
	}


	/**
	 * This function makes sure that the label is shown completely.
	 * 
	 * @see org.eclipse.jwt.we.figures.core.ModelElementFigure#getPreferredSize(int, int)
	 */
	@Override
	public Dimension getPreferredSize(int wHint, int hHint)
	{
		return getPreferredChildrenSize(wHint, hHint);
	}
}
