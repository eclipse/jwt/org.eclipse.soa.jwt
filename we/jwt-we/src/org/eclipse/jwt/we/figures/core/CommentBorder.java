/**
 * File:    CommentBorder.java
 * Created: 22.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.core;

import org.eclipse.draw2d.AbstractBorder;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;


/**
 * Border for a {@link CommentFigure}.
 * 
 * @version $Id: CommentBorder.java,v 1.8 2009-11-26 12:41:01 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class CommentBorder
		extends AbstractBorder
{

	/**
	 * @return The width of the border lines.
	 */
	public int getLineWidth()
	{
		return PreferenceReader.appearanceLineWidth.get();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Border#getInsets(org.eclipse.draw2d.IFigure)
	 */
	public Insets getInsets(IFigure figure)
	{
		return new Insets(getLineWidth(), getLineWidth(), getLineWidth(),
				PreferenceReader.appearanceCornerSize.get());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Border#paint(org.eclipse.draw2d.IFigure,
	 *      org.eclipse.draw2d.Graphics, org.eclipse.draw2d.geometry.Insets)
	 */
	public void paint(IFigure figure, Graphics graphics, Insets insets)
	{
		Rectangle rect = getPaintRectangle(figure, insets);
		int cornerSize = PreferenceReader.appearanceCornerSize.get();

		if (getLineWidth() % 2 == 1)
		{
			tempRect.width--;
			tempRect.height--;
		}
		rect.shrink(getLineWidth() / 2, getLineWidth() / 2);

		// fill the note
		PointList outline = new PointList();

		outline.addPoint(0, 0);
		outline.addPoint(rect.width - cornerSize, 0);
		outline.addPoint(rect.width, cornerSize);
		outline.addPoint(rect.width, rect.height);
		outline.addPoint(0, rect.height);

		graphics.fillPolygon(outline);

		// draw the inner outline
		PointList innerLine = new PointList();

		innerLine.addPoint(rect.width - cornerSize, 0);
		innerLine.addPoint(rect.width - cornerSize, cornerSize);
		innerLine.addPoint(rect.width, cornerSize);
		innerLine.addPoint(rect.width - cornerSize, 0);
		innerLine.addPoint(0, 0);
		innerLine.addPoint(0, rect.height);
		innerLine.addPoint(rect.width - 1, rect.height);
		innerLine.addPoint(rect.width, cornerSize);

		graphics.drawPolygon(innerLine);
	}
}
