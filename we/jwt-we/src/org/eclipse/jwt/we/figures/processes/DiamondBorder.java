/**
 * File:    DiamondBorder.java
 * Created: 22.06.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;


/**
 * A diamond border.
 * 
 * @version $Id: DiamondBorder.java,v 1.7 2009-11-26 12:41:13 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class DiamondBorder
		extends LineBorder
{

	/**
	 * The top point of the diamond.
	 */
	private Point topPoint = new Point();

	/**
	 * The right point of the diamond.
	 */
	private Point rightPoint = new Point();

	/**
	 * The bottom point of the diamond.
	 */
	private Point bottomPoint = new Point();

	/**
	 * The left point of the diamond.
	 */
	private Point leftPoint = new Point();


	/**
	 * Creates a new RoundedRectangleBorder.
	 */
	public DiamondBorder()
	{
		super(PreferenceReader.appearanceBorderColor.get(),
				PreferenceReader.appearanceLineWidth.get());
	}


	/**
	 * @return Returns the topPoint.
	 */
	public Point getTopPoint()
	{
		return topPoint;
	}


	/**
	 * @return Returns the rightPoint.
	 */
	public Point getRightPoint()
	{
		return rightPoint;
	}


	/**
	 * @return Returns the bottomPoint.
	 */
	public Point getBottomPoint()
	{
		return bottomPoint;
	}


	/**
	 * @return Returns the leftPoint.
	 */
	public Point getLeftPoint()
	{
		return leftPoint;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Border#paint(org.eclipse.draw2d.IFigure,
	 *      org.eclipse.draw2d.Graphics, org.eclipse.draw2d.geometry.Insets)
	 */
	@Override
	public void paint(IFigure figure, Graphics graphics, Insets insets)
	{
		tempRect.setBounds(getPaintRectangle(figure, insets));

		// set line width and color
		graphics.setLineWidth(getWidth());
		if (getColor() != null)
		{
			graphics.setForegroundColor(getColor());
		}

		boolean drawShadow = PreferenceReader.appearanceShadowVisible.get();

		// if a shadow is drawn, make the original figure smaller
		if (drawShadow)
		{
			tempRect.width -= 2;
			tempRect.height -= 2;
		}

		// calculate the diamond corner points
		int dx = tempRect.width / 2;
		int dy = tempRect.height / 2;

		PointList pointList = new PointList();
		topPoint = new Point(tempRect.x + dx, tempRect.y);
		rightPoint = new Point(tempRect.x + tempRect.width, tempRect.y + dy);
		bottomPoint = new Point(tempRect.x + dx, tempRect.y + tempRect.height);
		leftPoint = new Point(tempRect.x, tempRect.y + dy);

		// draw shadow?
		if (PreferenceReader.appearanceShadowVisible.get())
		{
			// draw shadow diamond
			pointList.removeAllPoints();
			pointList.addPoint(topPoint.x + 2, topPoint.y + 2);
			pointList.addPoint(rightPoint.x + 2, rightPoint.y + 2);
			pointList.addPoint(bottomPoint.x + 2, bottomPoint.y + 2);
			pointList.addPoint(leftPoint.x + 2, leftPoint.y + 2);

			graphics.setAlpha(50);
			graphics.setBackgroundColor(PreferenceReader.appearanceShadowColor.get());
			graphics.fillPolygon(pointList);

			// draw original filled diamond area
			pointList.removeAllPoints();
			pointList.addPoint(topPoint.x, topPoint.y);
			pointList.addPoint(rightPoint.x, rightPoint.y);
			pointList.addPoint(bottomPoint.x, bottomPoint.y);
			pointList.addPoint(leftPoint.x, leftPoint.y);

			graphics.setAlpha(255);
			graphics.setBackgroundColor(figure.getBackgroundColor());
			graphics.fillPolygon(pointList);
		}

		// draw border
		pointList.removeAllPoints();
		pointList.addPoint(topPoint.x, topPoint.y);
		pointList.addPoint(rightPoint.x, rightPoint.y);
		pointList.addPoint(bottomPoint.x, bottomPoint.y);
		pointList.addPoint(leftPoint.x, leftPoint.y);

		graphics.drawPolygon(pointList);
	}

}
