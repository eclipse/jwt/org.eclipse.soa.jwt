/**
 * File:    ScalingImageDescriptor.java
 * Created: 22.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.internal;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;


/**
 * This ImageDescriptor scales the image returned by another ImageDescriptor.
 * 
 * @version $Id: ScalingImageDescriptor.java,v 1.4 2009-12-02 12:18:03 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class ScalingImageDescriptor
		extends ImageDescriptor
{

	/**
	 * The ImageDescriptor that creates the original image.
	 */
	private ImageDescriptor imageDescriptor;

	/**
	 * The size of the returned image.
	 * 
	 * <p>
	 * If <code>null</code> the size of the image is not changed.
	 * </p>
	 */
	private Dimension size;


	/**
	 * Creates a new image descriptor.
	 * 
	 * @param imageDescriptor
	 *            The ImageDescriptor that creates the original image.
	 * @param size
	 *            The size of the returned image.
	 */
	public ScalingImageDescriptor(ImageDescriptor imageDescriptor, Dimension size)
	{
		setSize(size);
		setImageDescriptor(imageDescriptor);
	}


	/**
	 * @return Returns the imageDescriptor.
	 */
	public ImageDescriptor getImageDescriptor()
	{
		return imageDescriptor;
	}


	/**
	 * @param imageDescriptor
	 *            The imageDescriptor to set.
	 */
	public void setImageDescriptor(ImageDescriptor imageDescriptor)
	{
		this.imageDescriptor = imageDescriptor;

		// optimisation: dont scale image twice
		if (imageDescriptor != null)
		{
			if (imageDescriptor instanceof ScalingImageDescriptor)
			{
				this.imageDescriptor = ((ScalingImageDescriptor) imageDescriptor)
						.getImageDescriptor();
			}

			// disable scaling of nested ScalingImageDescriptors
			if (imageDescriptor instanceof CombinedImageDescriptor)
			{
				CombinedImageDescriptor combinedImageDescriptor = (CombinedImageDescriptor) imageDescriptor;

				for (ImageDescriptor descriptor : combinedImageDescriptor
						.getImageDescriptors())
				{
					if (descriptor != null
							&& descriptor instanceof ScalingImageDescriptor)
					{
						((ScalingImageDescriptor) descriptor).setSize(null);
					}
				}
			}
		}
	}


	/**
	 * @return Returns the size.
	 */
	public Dimension getSize()
	{
		return size;
	}


	/**
	 * @param size
	 *            The size to set.
	 */
	public void setSize(Dimension size)
	{
		this.size = size;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.resource.ImageDescriptor#getImageData()
	 */
	@Override
	public ImageData getImageData()
	{
		if (imageDescriptor == null)
		{
			return null;
		}

		ImageData imageData = imageDescriptor.getImageData();

		if (imageData == null)
		{
			return null;
		}

		if (size != null
				&& (imageData.width != size.width || imageData.height != size.height))
		{
			imageData = imageData.scaledTo(size.width, size.height);
		}

		return imageData;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		if (size == null)
		{
			return imageDescriptor + ""; //$NON-NLS-1$
		}

		return imageDescriptor + "@" + size.width + "x" + size.height; //$NON-NLS-1$ //$NON-NLS-2$
	}


	/* (non-Javadoc)
	 * @see org.eclipse.jface.resource.ImageDescriptor#createImage()
	 */
	@Override
	public Image createImage()
	{
		return Plugin.getInstance().getFactoryRegistry().getImageFactory().getImage(this);
	}

}
