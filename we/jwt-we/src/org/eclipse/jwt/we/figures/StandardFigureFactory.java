/**
 * File:    StandardFigureFactory.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Yoann Rodiere, Open Wide, Lyon, France
 *    	- changed implementation to hard-code figure mappings
 *******************************************************************************/

package org.eclipse.jwt.we.figures;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.jwt.meta.model.core.Comment;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.meta.model.data.Data;
import org.eclipse.jwt.meta.model.events.Event;
import org.eclipse.jwt.meta.model.organisations.Role;
import org.eclipse.jwt.meta.model.processes.Action;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;
import org.eclipse.jwt.meta.model.processes.ActivityLinkNode;
import org.eclipse.jwt.meta.model.processes.DecisionNode;
import org.eclipse.jwt.meta.model.processes.FinalNode;
import org.eclipse.jwt.meta.model.processes.ForkNode;
import org.eclipse.jwt.meta.model.processes.Guard;
import org.eclipse.jwt.meta.model.processes.GuardSpecification;
import org.eclipse.jwt.meta.model.processes.InitialNode;
import org.eclipse.jwt.meta.model.processes.JoinNode;
import org.eclipse.jwt.meta.model.processes.MergeNode;
import org.eclipse.jwt.meta.model.processes.StructuredActivityNode;
import org.eclipse.jwt.meta.model.processes.impl.ActivityImpl;
import org.eclipse.jwt.we.figures.application.ApplicationFigure;
import org.eclipse.jwt.we.figures.core.CommentFigure;
import org.eclipse.jwt.we.figures.core.ReferenceableElementFigure;
import org.eclipse.jwt.we.figures.data.DataFigure;
import org.eclipse.jwt.we.figures.events.EventFigure;
import org.eclipse.jwt.we.figures.internal.FigureMappingRegistry;
import org.eclipse.jwt.we.figures.organisations.RoleFigure;
import org.eclipse.jwt.we.figures.processes.ActionFigure;
import org.eclipse.jwt.we.figures.processes.ActivityEdgeFigure;
import org.eclipse.jwt.we.figures.processes.ActivityFigure;
import org.eclipse.jwt.we.figures.processes.ActivityLinkNodeFigure;
import org.eclipse.jwt.we.figures.processes.DecisionNodeFigure;
import org.eclipse.jwt.we.figures.processes.FinalNodeFigure;
import org.eclipse.jwt.we.figures.processes.ForkNodeFigure;
import org.eclipse.jwt.we.figures.processes.GuardFigure;
import org.eclipse.jwt.we.figures.processes.GuardSpecificationFigure;
import org.eclipse.jwt.we.figures.processes.InitialNodeFigure;
import org.eclipse.jwt.we.figures.processes.JoinNodeFigure;
import org.eclipse.jwt.we.figures.processes.MergeNodeFigure;
import org.eclipse.jwt.we.figures.processes.StructuredActivityNodeFigure;
import org.eclipse.jwt.we.figures.view.ReferenceEdgeFigure;
import org.eclipse.jwt.we.figures.view.ReferenceFigure;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.model.view.ReferenceEdge;

/**
 * This is the standard implementation of IFigureFactory that returns the
 * typical JWT Draw2D figures (from the figures package) for model elements.
 * 
 * @version $Id: StandardFigureFactory.java,v 1.7 2009-11-26 12:41:49 chsaad Exp
 *          $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class StandardFigureFactory implements IFigureFactory {

	/**
	 * A Logger.
	 */
	private static final Logger logger = Logger
			.getLogger(StandardFigureFactory.class);

	/**
	 * The registry carrying information about which figure must be instantiated
	 * for which model type.
	 */
	private static final FigureMappingRegistry registry = new FigureMappingRegistry(
			25);
	static {
		/*
		 * Class<ActivityImpl> is used when dragging pre-existing elements on
		 * the diagram.
		 * 
		 * A reference to another activity can result into three different
		 * "objects" after dropping it on the diagram. At this stage we don't
		 * know which one it will be, so we display a DefaultFigure.
		 */
		registry.map(ActivityImpl.class, DefaultFigure.class);

		registry.map(Action.class, ActionFigure.class);
		registry.map(Activity.class, ActivityFigure.class);
		registry.map(ActivityEdge.class, ActivityEdgeFigure.class);
		registry.map(ActivityLinkNode.class, ActivityLinkNodeFigure.class);
		registry.map(Application.class, ApplicationFigure.class);
		registry.map(Comment.class, CommentFigure.class);
		registry.map(Data.class, DataFigure.class);
		registry.map(DecisionNode.class, DecisionNodeFigure.class);
		registry.map(Event.class, EventFigure.class);
		registry.map(FinalNode.class, FinalNodeFigure.class);
		registry.map(ForkNode.class, ForkNodeFigure.class);
		registry.map(Guard.class, GuardFigure.class);
		registry.map(GuardSpecification.class, GuardSpecificationFigure.class);
		registry.map(InitialNode.class, InitialNodeFigure.class);
		registry.map(JoinNode.class, JoinNodeFigure.class);
		registry.map(MergeNode.class, MergeNodeFigure.class);
		registry.map(Reference.class, ReferenceFigure.class);
		registry.map(ReferenceableElement.class,
				ReferenceableElementFigure.class);
		registry.map(ReferenceEdge.class, ReferenceEdgeFigure.class);
		registry.map(Role.class, RoleFigure.class);
		registry.map(StructuredActivityNode.class,
				StructuredActivityNodeFigure.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.figures.factory.IFigureFactory#createFigure(java.lang
	 * .Class)
	 */
	public IFigure createFigure(Class modelType) {
		Class<? extends IFigure> figureClass = registry
				.getFirstMatch(modelType);

		if (figureClass != null) {
			try {
				return figureClass.newInstance();
			} catch (InstantiationException e) {
			} catch (IllegalAccessException e) {
			}
		}

		logger.debug("Can't find figure class for " + modelType); //$NON-NLS-1$
		return null;
	}

}
