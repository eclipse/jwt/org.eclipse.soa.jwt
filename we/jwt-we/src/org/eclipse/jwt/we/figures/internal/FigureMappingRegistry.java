/**
 * File:    FigureMappingRegistry.java
 * Created: 27.04.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.figures.internal;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.eclipse.draw2d.IFigure;

/**
 * This class maintains a mapping registry to retrieve, for a given model
 * object, which figure class should represent it based on its type.
 * <p>
 * The registry is populated using {@link #map(Class, Class)}, and can then be
 * queried using {@link #getFirstMatch(Class)}.
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.2
 */
public class FigureMappingRegistry {
	/**
	 * The association registry data. The {@link LinkedHashMap} implementation
	 * is needed to take the insertion order into account when iterating.
	 */
	private final LinkedHashMap<Class<?>, Class<? extends IFigure>> associations;

	public FigureMappingRegistry(int initialCapacity) {
		this.associations = new LinkedHashMap<Class<?>, Class<? extends IFigure>>(
				initialCapacity);
	}

	public FigureMappingRegistry() {
		this.associations = new LinkedHashMap<Class<?>, Class<? extends IFigure>>();
	}

	/**
	 * Maps a given model type to a specific figure type.
	 * 
	 * @param modelType
	 *            The type to be compared to the argument of
	 *            {@link #getFirstMatch(Class)}.
	 * @param figureType
	 *            The type to be returned by {@link #getFirstMatch(Class)}
	 *            whenever an argument matches {@link modelType}.
	 */
	public void map(Class<?> modelType, Class<? extends IFigure> figureType) {
		this.associations.put(modelType, figureType);
	}

	/**
	 * Returns a matching figure class for the given model class.
	 * <p>
	 * The returned type will be the figure type mapped to the first model type
	 * that can be assigned from {@code modelType}. The order of comparison is
	 * based on the order of the previous {@link #map(Class, Class)} calls.
	 * 
	 * @see Class#isAssignableFrom(Class)
	 * @param modelType
	 *            The class of the model object to be represented by the figure.
	 * @return A matching figure class for the given model class, or null if
	 *         there was none.
	 */
	public Class<? extends IFigure> getFirstMatch(Class<?> modelType) {
		for (Entry<Class<?>, Class<? extends IFigure>> entry : associations
				.entrySet()) {
			if (entry.getKey().isAssignableFrom(modelType)) {
				return entry.getValue();
			}
		}
		return null;
	}
}