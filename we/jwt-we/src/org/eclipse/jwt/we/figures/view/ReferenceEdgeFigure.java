/**
 * File:    ReferenceEdgeFigure.java
 * Created: 26.12.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.view;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.jwt.we.model.view.ReferenceEdge;


/**
 * Figure for a {@link ReferenceEdge}. Is displayed, if the display type is Normal
 * Display Mode
 * 
 * @version $Id: ReferenceEdgeFigure.java,v 1.3 2009-11-26 12:41:29 chsaad Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ReferenceEdgeFigure
		extends PolylineConnection
{

	public boolean display = true;


	public ReferenceEdgeFigure()
	{
		super();
		setLineStyle(Graphics.LINE_DASH);
	}

}
