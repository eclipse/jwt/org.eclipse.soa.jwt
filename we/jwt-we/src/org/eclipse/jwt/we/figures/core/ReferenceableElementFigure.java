/**
 * File:    ReferenceableElementFigure.java
 * Created: 21.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.core;

import org.eclipse.draw2d.Graphics;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;


/**
 * Figure for a {@link ReferenceableElement}.
 * 
 * @version $Id: ReferenceableElementFigure.java,v 1.8 2009-11-26 12:41:02 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ReferenceableElementFigure
		extends NamedIconFigure
{

	/**
	 * Creates the figure.
	 */
	public ReferenceableElementFigure()
	{
		super();
		setFont(PreferenceReader.appearanceReferenceableElementFont.get());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.ModelElementFigure#paint(org.eclipse.draw2d.Graphics)
	 */
	@Override
	public void paint(Graphics graphics)
	{
		super.paint(graphics);
	}
}
