/**
 * File:    NamedIconFigure.java
 * Created: 20.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.core;

import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.swt.graphics.Image;


/**
 * This figure displays an icon with text.
 * 
 * Note: Images are not disposed by this class. This must be done by the creating class.
 * 
 * @version $Id: NamedIconFigure.java,v 1.9 2009-11-26 12:41:02 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class NamedIconFigure
		extends NamedElementFigure
{

	/**
	 * Creates the figure.
	 */
	public NamedIconFigure()
	{
		ToolbarLayout layout = new ToolbarLayout(false);
		layout.setStretchMinorAxis(true);
		setLayoutManager(layout);

		getLabel().setIconAlignment(PositionConstants.CENTER);
		getLabel().setTextPlacement(PositionConstants.SOUTH);

		checkBorder();
	}


	/**
	 * Sets bounds if no text and no icons is visible so that this figure does not get
	 * invisible.
	 */
	protected void checkBorder()
	{
		if (getLabel().getIcon() == null && getLabel().getText().length() == 0)
		{
			getLabel().setBorder(
					new LineBorder(PreferenceReader.appearanceBorderColor.get(),
							PreferenceReader.appearanceLineWidth.get()));
		}
		else
		{
			getLabel().setBorder(null);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.NamedElementFigure#setName(java.lang.String)
	 */
	@Override
	public void setName(String name)
	{
		super.setName(name);
		checkBorder();
	}


	/**
	 * Sets a new icon.
	 * 
	 * @param newIcon
	 *            The icon to set.
	 */
	@Override
	public void setIcon(Image newIcon)
	{
		Image oldIcon = getLabel().getIcon();
		if (oldIcon == newIcon)
		{
			return;
		}

		getLabel().setIcon(newIcon);
		checkBorder();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.ModelElementFigure#getPreferredSize(int, int)
	 */
	@Override
	public Dimension getPreferredSize(int wHint, int hHint)
	{
		return getPreferredChildrenSize(wHint, hHint);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.NamedElementFigure#getCellEditorArea()
	 */
	@Override
	public Rectangle getCellEditorArea()
	{
		Rectangle result = getClientArea().getCopy();
		Rectangle iconBounds = getLabel().getIconBounds();

		return result.crop(new Insets(iconBounds.height, 0, 0, 0));
	}
}
