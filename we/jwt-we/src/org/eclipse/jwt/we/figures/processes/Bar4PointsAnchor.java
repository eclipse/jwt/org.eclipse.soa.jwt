/**
 * File:    Bar4PointsAnchor.java
 * Created: 29.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;


/**
 * This anchor returns 4 anchor points on the border.
 * 
 * If horizontal it creates 2 points on the left and and 2 points on right border. If
 * vertical it creates 2 points on the top and and 2 points on the bottom border.
 * 
 * @version $Id: Bar4PointsAnchor.java,v 1.7 2009-11-26 12:41:12 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class Bar4PointsAnchor
		extends BorderAnchor
{

	/**
	 * The direction of the points.
	 */
	private boolean horizontal = false;


	/**
	 * @param figure
	 *            The figure to calculate the anchor for.
	 */
	public Bar4PointsAnchor(IFigure figure)
	{
		super(figure);
	}


	/**
	 * @param figure
	 *            The figure to calculate the anchor for.
	 * @param horizontal
	 *            The direction of the points.
	 */
	public Bar4PointsAnchor(IFigure figure, boolean horizontal)
	{
		super(figure);
		this.horizontal = horizontal;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.processes.BorderAnchor#getPoints(org.eclipse.draw2d.geometry.Rectangle)
	 */
	@Override
	public Point[] getPoints(Rectangle bounds)
	{
		if (horizontal)
		{
			int deltaTop = bounds.width / 4;
			int deltaBottom = bounds.width - deltaTop;
			return new Point[]
			{ getTopPoint(bounds, deltaTop), getTopPoint(bounds, deltaBottom),
					getBottomPoint(bounds, deltaTop), getBottomPoint(bounds, deltaBottom) };
		}

		int deltaTop = bounds.height / 4;
		int deltaBottom = bounds.height - deltaTop;
		return new Point[]
		{ getLeftPoint(bounds, deltaTop), getLeftPoint(bounds, deltaBottom),
				getRightPoint(bounds, deltaTop), getRightPoint(bounds, deltaBottom) };
	}

}
