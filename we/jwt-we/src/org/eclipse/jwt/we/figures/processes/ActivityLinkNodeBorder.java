/**
 * File:    ActivityLinkNodeBorder.java
 * Created: 03.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;


/**
 * A rounded Border with small + in Rectangle on the ground Line.
 * 
 * @version
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ActivityLinkNodeBorder
		extends RoundedRectangleBorder
{

	/**
	 * Creates a new RoundedRectangleBorder with added +.
	 */
	public ActivityLinkNodeBorder()
	{
		super();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Border#paint(org.eclipse.draw2d.IFigure,
	 *      org.eclipse.draw2d.Graphics, org.eclipse.draw2d.geometry.Insets)
	 */
	@Override
	public void paint(IFigure figure, Graphics graphics, Insets insets)
	{
		super.paint(figure, graphics, insets);

		Point tpoint1 = new Point(tempRect.x + (tempRect.width / 2) - 6, tempRect.y + 12);
		Point tpoint2 = new Point(tpoint1.x + 11, tpoint1.y - 12);
		graphics.drawRectangle(new Rectangle(tpoint1, tpoint2));
		graphics.drawLine(tpoint1.x + 3, tpoint1.y - 6, tpoint1.x + 9, tpoint1.y - 6);
		graphics.drawLine(tpoint1.x + 6, tpoint1.y - 2, tpoint1.x + 6, tpoint1.y - 9);
	}

}
