/**
 * File:    ContainerFigure.java
 * Created: 25.02.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.figures.core.NamedElementFigure;


/**
 * A figure that contains other figures.
 * 
 * The figure is invisible and provides an container for another figure. The contained
 * figure can take the complete contentPane of this figure.
 * 
 * @version $Id: ContainerFigure.java,v 1.9 2009-11-26 12:41:05 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ContainerFigure
		extends NamedElementFigure
{

	/**
	 * Creates the figure.
	 */
	public ContainerFigure()
	{
		super(null);

		// initially draw a border so that the figure is not invisible
		updateBorder();
	}


	/**
	 * Update the border so that the figure is not invisible.
	 * 
	 * If the figure has children no border is displayed. If it is empty are border is set
	 * to make the figure visible.
	 */
	private void updateBorder()
	{
		if (getChildren().isEmpty())
		{
			if (getBorder() == null)
			{
				setBorder(new LineBorder(PreferenceReader.appearanceBorderColor.get(),
						PreferenceReader.appearanceLineWidth.get()));
			}
		}
		else
		{
			if (getBorder() != null)
			{
				setBorder(null);
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.ModelElementFigure#getPreferredSize(int, int)
	 */
	@Override
	public Dimension getPreferredSize(int wHint, int hHint)
	{
		return getPreferredChildrenSize(wHint, hHint);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Figure#add(org.eclipse.draw2d.IFigure, java.lang.Object,
	 *      int)
	 */
	@Override
	public void add(IFigure figure, Object constraint, int index)
	{
		super.add(figure, constraint, index);
		updateBorder();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Figure#remove(org.eclipse.draw2d.IFigure)
	 */
	@Override
	public void remove(IFigure figure)
	{
		super.remove(figure);
		updateBorder();
	}
}
