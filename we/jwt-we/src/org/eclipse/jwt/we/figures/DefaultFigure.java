/**
 * File:    DefaultFigure.java
 * Created: 27.04.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere, Open Wide, Lyon, France
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.figures;

import org.eclipse.jwt.we.figures.core.NamedIconFigure;

/**
 * Figure for an element for which no corresponding figure was found.
 * 
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.2
 */
public class DefaultFigure extends NamedIconFigure {

}
