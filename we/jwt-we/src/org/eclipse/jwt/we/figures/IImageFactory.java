/**
 * File:    IImageFactory.java
 * Created: 02.02.2009
 *
/*******************************************************************************
 * Copyright (C) 2007  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - initial API.
 *******************************************************************************/

package org.eclipse.jwt.we.figures;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

/**
 * This is an interface for the images factory.
 * 
 * @version $Id: IImageFactory.java,v 1.2 2009-11-04 17:18:23 chsaad Exp $
 * @author Marc Blachon, Bull SAS, France, www.bull.com
 */

public interface IImageFactory {

	/**
	 * Returns an image.
	 * 
	 * <p>
	 * The {@link ImageRegistry} handles dispose of the image.
	 * </p>
	 * 
	 * @param imageDescriptor
	 *            The {@link ImageDescriptor} that creates the image. Its String
	 *            representations is the key in the {@link ImageRegistry}.
	 * @return The image, or <code>null</code> if it could not be found.
	 */
	public Image getImage(ImageDescriptor imageDescriptor);
	
	/**
	 * Returns an image from the {@link ImageRegistry}.
	 * 
	 * <p>
	 * The image will be scaled to the given size.
	 * </p>
	 * 
	 * @param imageDescriptor
	 *            The {@link ImageDescriptor} that creates the image. Its String
	 *            representations is the key in the {@link ImageRegistry}.
	 * @param size
	 *            The size of the returned image. If <code>null</code> the image is
	 *            returned unscaled.
	 * @return The image, or <code>null</code> if it could not be found.
	 */
	public Image getImage(ImageDescriptor imageDescriptor, Dimension size);
	
	/**
	 * Returns an image descriptor for the image file at the given plug-in relative path.
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public ImageDescriptor createImageDescriptor(String path);
	
	/**
	 * Creates an ImageDescriptor that represents the type of a model object.
	 * 
	 * @param model
	 *            The model object.
	 * @return The ImageDescriptor.
	 */
	public ImageDescriptor createModelTypeImageDescriptor(Object model);		
	
}
