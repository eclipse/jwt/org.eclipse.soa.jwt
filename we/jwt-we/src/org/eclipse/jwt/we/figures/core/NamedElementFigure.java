/**
 * File:    NamedElementFigure.java
 * Created: 04.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - use of IImageFactory instead of ImageFactory.
 *******************************************************************************/


package org.eclipse.jwt.we.figures.core;

import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.meta.model.core.NamedElement;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.figures.IImageFactory;
import org.eclipse.jwt.we.figures.internal.ImageFactory;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.swt.graphics.Image;


/**
 * Figure for a {@link NamedElement}.
 * 
 * This figure has the ability to display an icon with text. But the icon is not set to
 * the label in order to allow subclasses to reuse this class also if they do not have an
 * icon. Subclasses that need to display an icon must overwrite {@link #setIcon(Image)}.
 * An ImageFactory must be set with {@link #setImageFactory(ImageFactory)} before icons
 * can be painted.
 * 
 * @version $Id: NamedElementFigure.java,v 1.12 2009-11-26 12:41:01 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class NamedElementFigure
		extends ModelElementFigure
{

	/**
	 * A Logger.
	 */
	private static final Logger logger = Logger.getLogger(NamedElementFigure.class);

	/**
	 * Label to show the name of the element.
	 */
	private Label label;

	/**
	 * ImageFactory for creating the images.
	 */
	private IImageFactory imageFactory;


	/**
	 * Creates the figure with a label from {@link #createLabel()}.
	 */
	public NamedElementFigure()
	{
		setLayoutManager(new StackLayout());
		setLabel(createLabel());
	}


	/**
	 * Creates the figure.
	 * 
	 * @param label
	 *            The initial label, may be <code>null</code> if no label is needed.
	 */
	public NamedElementFigure(Label label)
	{
		setLayoutManager(new StackLayout());
		setLabel(label);
	}


	/**
	 * Sets the name of the figure.
	 * 
	 * @param name
	 *            The new name to be displayed by the figure.
	 */
	public void setName(String name)
	{
		if (label != null)
		{
			label.setText(name);
		}
	}


	/**
	 * @return Returns the imageFactory.
	 */
	public IImageFactory getImageFactory()
	{
		return imageFactory;
	}


	/**
	 * Sets the {@link ImageFactory} that is used to create the icons.
	 * 
	 * @param imageFactory
	 *            The imageFactory to set.
	 */
	public void setImageFactory(IImageFactory imageFactory)
	{
		this.imageFactory = imageFactory;
	}


	/**
	 * @return The size the icons should have.
	 */
	public Dimension getPreferredIconSize()
	{
		return PreferenceReader.appearanceFigureIconSize.get();
	}


	/**
	 * Sets an icon for the figure.
	 * 
	 * The icon will be created from a {@link ImageFactory}.
	 * 
	 * @param icon
	 *            An {@link ImageDescriptor} that can create the image.
	 */
	public void setIcon(ImageDescriptor icon)
	{
		if (icon == null)
		{
			setIcon((Image) null);
			return;
		}

		if (imageFactory != null)
		{
			setIcon(imageFactory.getImage(icon, getPreferredIconSize()));
		}
		else
		{
			logger.debugWarning("imageFactory is null, no image is created."); //$NON-NLS-1$
		}
	}


	/**
	 * Sets an icon for the figure.
	 * 
	 * The icon should have the size of {@link #getPreferredIconSize()}.
	 * 
	 * This implementation is empty. Subclasses that need to display an icon must
	 * overwrite this method.
	 * 
	 * @param icon
	 *            The new icon.
	 */
	public void setIcon(Image icon)
	{
	}


	/**
	 * Creates a label.
	 * 
	 * Subclasses may overwrite and return <code>null</code>, if no label is needed.
	 * 
	 * @return A new label.
	 */
	protected Label createLabel()
	{
		Label newLabel = new Label();
		newLabel.setLabelAlignment(PositionConstants.CENTER);
		newLabel.setForegroundColor(PreferenceReader.appearanceTextColor.get());
		return newLabel;
	}


	/**
	 * @return Returns the label.
	 */
	public Label getLabel()
	{
		return label;
	}


	/**
	 * Sets the label and adds it to the figure.
	 * 
	 * @param newLabel
	 *            The label to set.
	 */
	public void setLabel(Label newLabel)
	{
		if (label != null)
		{
			remove(label);
		}

		label = newLabel;

		if (label != null)
		{
			add(label);
		}
	}


	/**
	 * @return The area in which the CellEditor for directEdit should be located.
	 */
	public Rectangle getCellEditorArea()
	{
		return getClientArea();
	}
}
