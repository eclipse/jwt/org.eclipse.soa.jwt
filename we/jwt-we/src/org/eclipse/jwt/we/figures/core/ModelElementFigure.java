/**
 * File:    ModelElementFigure.java
 * Created: 04.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - use of IImageFactory instead of ImageFactory.
 *******************************************************************************/

package org.eclipse.jwt.we.figures.core;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.figures.IImageFactory;
import org.eclipse.jwt.we.figures.internal.IWEFigure;
import org.eclipse.swt.SWT;


/**
 * Figure for a {@link ModelElement}. This figure is empty and should be used as a base
 * class for other figures.
 * 
 * This figure does the essential setting that are common for all figures.
 * 
 * @version $Id: ModelElementFigure.java,v 1.15 2009-11-26 12:41:01 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ModelElementFigure
		extends Figure
		implements IWEFigure
{

	/**
	 * Indicates whether the alpha filtering should be enabled.
	 */
	private boolean alpha = false;


	/**
	 * Creates a new figure and does the essential setting that are common for all
	 * figures.
	 */
	public ModelElementFigure()
	{
		setOpaque(false);
		setForegroundColor(PreferenceReader.appearanceTextColor.get());
		setBackgroundColor(PreferenceReader.appearanceFillColor.get());
		setFont(PreferenceReader.appearanceDefaultFont.get());
		setMinimumSize(PreferenceReader.appearanceMinimumSize.get());
	}


	/**
	 * Enables/Disables the alpha filtering (e.g. if for drawing a feedback figure).
	 * 
	 * @param alpha
	 *            The alpha to set.
	 */
	public void setAlpha(boolean alpha)
	{
		this.alpha = alpha;
	}


	/**
	 * Makes sure that the figure does not disappear by setting the minimum size at least
	 * {@link PreferenceReader#appearanceMinimumSize}.
	 * 
	 * @see org.eclipse.draw2d.Figure#setMinimumSize(org.eclipse.draw2d.geometry.Dimension)
	 */
	@Override
	public void setMinimumSize(Dimension d)
	{
		Dimension size = PreferenceReader.appearanceMinimumSize.get();

		if (d.width < size.width)
		{
			d.width = size.width;
		}
		if (d.height < size.height)
		{
			d.height = size.height;
		}

		super.setMinimumSize(d);
	}


	/**
	 * Returns the minimum size which is the size stored in the model.
	 * 
	 * This is the preferred size. Figures that need greater size should overwrite this
	 * function but they must make sure that they do not return a smaller size. Layout
	 * managers do not honor the minimum size and so the actual size could then be smaller
	 * than the minimum size.
	 * 
	 * @see org.eclipse.draw2d.Figure#getPreferredSize(int, int)
	 */
	@Override
	public Dimension getPreferredSize(int wHint, int hHint)
	{
		Dimension minimum = getMinimumSize(wHint, hHint);
		return minimum;
	}


	/**
	 * Returns the preferred size of this figure, which includes the preferred size of its
	 * children.
	 * 
	 * Subclasses may overwrite the {@link #getPreferredSize(int, int)} method and
	 * delegete to this method if they want to have their size calculated from the
	 * children.
	 * 
	 * @param wHint
	 *            a width hint
	 * @param hHint
	 *            a height hint
	 * @return The preferred size
	 */
	public Dimension getPreferredChildrenSize(int wHint, int hHint)
	{
		Dimension pref = getMinimumSize(wHint, hHint).getCopy();
		if (getLayoutManager() != null)
		{
			pref.union(getLayoutManager().getPreferredSize(this, wHint, hHint));
		}

		return pref;
	}


	/**
	 * Returns the content pane in which child figures should be placed.
	 * 
	 * <p>
	 * The default is the figure itself.
	 * </p>
	 * 
	 * @return The content pane.
	 */
	public IFigure getContentPane()
	{
		return this;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Figure#paint(org.eclipse.draw2d.Graphics)
	 */
	@Override
	public void paint(Graphics graphics)
	{
		// enable antialiasing
		if (PreferenceReader.appearanceAntialiasing.get())
		{
			graphics.setAntialias(SWT.ON);
			graphics.setTextAntialias(SWT.ON);
		}

		// set alpha value (e.g. if feedback figure)
		if (alpha)
		{
			graphics.setAlpha(130);
		}

		super.paint(graphics);

		// if a shadow should be drawn, the client area must be redrawn
		if (PreferenceReader.appearanceShadowVisible.get())
		{
			paintClientArea(graphics);
			paintFigure(graphics);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.jwt.we.figures.IJWTFigure#setIcon(org.eclipse.jface.resource.
	 * ImageDescriptor)
	 */
	public void setIcon(ImageDescriptor icon)
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.IJWTFigure#setName(java.lang.String)
	 */
	public void setName(String name)
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.figures.IWEFigure#setImageFactory(org.eclipse.jwt.we.figures
	 * .ImageFactory)
	 */
	public void setImageFactory(IImageFactory imageFactory)
	{
	}

}
