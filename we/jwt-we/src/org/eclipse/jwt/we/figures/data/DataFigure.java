/**
 * File:    DataFigure.java
 * Created: 12.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Florian Lautenbacher: own figure for data, application and role
 *******************************************************************************/


package org.eclipse.jwt.we.figures.data;

import org.eclipse.draw2d.Graphics;
import org.eclipse.jwt.meta.model.data.Data;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.figures.core.NamedIconFigure;

/**
 * Figure for a {@link Data}.
 * @since 0.6.0
 */
public class DataFigure
		extends NamedIconFigure
{

	/**
	 * Creates the figure.
	 */
	public DataFigure()
	{
		super();
		setFont(PreferenceReader.appearanceReferenceableElementFont.get());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.ModelElementFigure#paint(org.eclipse.draw2d.Graphics)
	 */
	@Override
	public void paint(Graphics graphics)
	{
		super.paint(graphics);
	}
}
