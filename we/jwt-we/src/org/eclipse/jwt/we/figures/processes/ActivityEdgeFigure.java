/**
 * File:    ActivityEdgeFigure.java
 * Created: 22.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MidpointLocator;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;


/**
 * Figure for an {@link ActivityEdge}.
 * 
 * @version $Id: ActivityEdgeFigure.java,v 1.7 2009-11-26 12:41:06 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ActivityEdgeFigure
		extends PolylineConnection
{

	/**
	 * Container that contains the children.
	 * 
	 * The children are display in the middle of the connection.
	 */
	private Figure container = new Figure();


	/**
	 * Creates the figure.
	 */
	public ActivityEdgeFigure()
	{
		setTargetDecoration(new PolygonDecoration());

		container.setOpaque(false);

		ToolbarLayout containerLayout = new ToolbarLayout(false);
		containerLayout.setStretchMinorAxis(true);
		container.setLayoutManager(containerLayout);

		MidpointLocator locator = new MidpointLocator(this, 0);
		add(container, locator);

	}


	/**
	 * @return The pane for the children.
	 */
	public IFigure getContentPane()
	{
		return container;
	}
}
