/**
 * File:    ForkNodeFigure.java
 * Created: 12.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.figures.processes;

/**
 * A fork node figure.
 * 
 * @version $Id: ForkNodeFigure.java,v 1.5 2009-11-26 12:41:13 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ForkNodeFigure
		extends BarFigure
{

}
