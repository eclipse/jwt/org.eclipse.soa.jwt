/**
 * File:    CompositeFigureFactory.java
 * Created: 02.02.2009
 *
/*******************************************************************************
 * Copyright (C) 2007  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - initial API and implementation.
 *******************************************************************************/

package org.eclipse.jwt.we.figures.internal;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jwt.we.figures.IFigureFactory;

/**
 * This factory for figures contains a collection of IFigureFactory and implements also the IFigureFactory interface.
 * It allows to create the figure with one of the IfigureFactory in the collection.
 * @author Marc Blachon, Bull SAS, France, www.bull.com
 */

public class CompositeFigureFactory implements IFigureFactory {

	/**
	 * Set of FigureFactory
	 */
	Collection<IFigureFactory> collection;
	
	/**
	 * Constructor
	 */
	public CompositeFigureFactory() {
		collection = new ArrayList<IFigureFactory>();
	}
	
	public IFigure createFigure(final Class modelType) {
		for (final IFigureFactory figureFactory:collection) {
			final IFigure result = figureFactory.createFigure(modelType);
			if (result != null) return result; 
		}
		return null;
	}	
	
	/**
	 * Adds a IFigureFactory
	 * @param figureFactory
	 */
	public void addFigureFactory(final IFigureFactory figureFactory) {
		if (figureFactory != null) {
			collection.add(figureFactory);			
		}
		else {
			throw new IllegalArgumentException("Error: the argument is null!"); //$NON-NLS-1$
		}
	}

}
