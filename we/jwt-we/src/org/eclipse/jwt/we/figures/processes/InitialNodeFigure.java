/**
 * File:    InitialNodeFigure.java
 * Created: 23.01.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.jwt.meta.model.processes.InitialNode;
import org.eclipse.jwt.we.figures.core.ModelElementFigure;


/**
 * Figure for an {@link InitialNode}.
 * 
 * It displays an empty circle.
 * 
 * @version $Id: InitialNodeFigure.java,v 1.9 2009-11-26 12:41:07 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class InitialNodeFigure
		extends ModelElementFigure
{

	/**
	 * Creates the figure.
	 */
	public InitialNodeFigure()
	{
		super();
		setBorder(new EllipseBorder());
	}
}
