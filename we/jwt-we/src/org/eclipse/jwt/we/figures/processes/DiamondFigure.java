/**
 * File:    DiamondFigure.java
 * Created: 25.01.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- moved border stuff in separate class, added shadow
 *******************************************************************************/

package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.figures.core.IconFigure;


/**
 * A figure that looks like a diamond.
 * 
 * @version $Id: DiamondFigure.java,v 1.11 2009-11-26 12:41:13 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class DiamondFigure
		extends IconFigure
{

	/**
	 * Creates the figure.
	 */
	public DiamondFigure()
	{
		super();
		setBorder(new DiamondBorder());
	}


	/**
	 * @return Returns the bottomPoint.
	 */
	public Point getBottomPoint()
	{
		return ((DiamondBorder) getBorder()).getBottomPoint();
	}


	/**
	 * @return Returns the leftPoint.
	 */
	public Point getLeftPoint()
	{
		return ((DiamondBorder) getBorder()).getLeftPoint();
	}


	/**
	 * @return Returns the rightPoint.
	 */
	public Point getRightPoint()
	{
		return ((DiamondBorder) getBorder()).getRightPoint();
	}


	/**
	 * @return Returns the topPoint.
	 */
	public Point getTopPoint()
	{
		return ((DiamondBorder) getBorder()).getTopPoint();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.IconFigure#getIconClientArea()
	 */
	@Override
	protected Rectangle getIconClientArea()
	{
		Rectangle iconClientArea = super.getIconClientArea();

		// if shadow is activated, resize client area for painting the icon
		if (PreferenceReader.appearanceShadowVisible.get())
		{
			iconClientArea.height -= 2;
			iconClientArea.width -= 2;
		}

		return iconClientArea;
	}

}
