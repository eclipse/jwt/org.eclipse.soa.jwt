/**
 * File:    GuardFigure.java
 * Created: 17.09.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.figures.core.NamedIconFigure;


/**
 * Figure for a {@link Guard}
 * 
 * @version $Id: GuardFigure.java,v 1.8 2009-11-26 12:41:07 chsaad Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class GuardFigure
		extends NamedIconFigure
{

	/**
	 * Creates the figure.
	 */
	public GuardFigure()
	{
		super();

		setFont(PreferenceReader.appearanceGuardFont.get());
	}
}
