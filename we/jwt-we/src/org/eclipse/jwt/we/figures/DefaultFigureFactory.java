/**
 * File:    DefaultFigureFactory.java
 * Created: 27.04.2012
 *
 *
/*******************************************************************************
 * Copyright (c) 2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Yoann Rodiere, Open Wide, Lyon, France
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.figures;

import org.eclipse.draw2d.IFigure;

/**
 * This is a default implementation of IFigureFactory that always return
 * a non-null object of type DefaultFigure.
 *
 * @author Yoann Rodiere (yoann.rodiere@openwide.fr), Open Wide, Lyon, France
 * @since 1.2
 */
public class DefaultFigureFactory implements IFigureFactory {
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.figures.factory.IFigureFactory#createFigure(java.lang
	 * .Class)
	 */
	public IFigure createFigure(Class modelType) {
		return new DefaultFigure();
	}

}
