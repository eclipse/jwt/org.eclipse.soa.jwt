/**
 * File:    DiamondAnchor.java
 * Created: 25.01.2006
 *
 /*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;


/**
 * An anchor for a {@link DiamondFigure}.
 * 
 * The anchor has four anchor point: Top middle, right middle, bottom middle, left middle.
 * The nearest point to the reference point is chosen as anchor.
 * 
 * @version $Id: DiamondAnchor.java,v 1.7 2009-11-26 12:41:13 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class DiamondAnchor
		extends BorderAnchor
{

	/**
	 * @param figure
	 *            The figure to calculate the anchor for.
	 */
	public DiamondAnchor(IFigure figure)
	{
		super(figure);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.processes.BorderAnchor#getPoints(org.eclipse.draw2d.geometry.Rectangle)
	 */
	@Override
	public Point[] getPoints(Rectangle bounds)
	{
		int dx = bounds.width / 2;
		int dy = bounds.height / 2;

		return new Point[]
		{ getTopPoint(bounds, dx), getRightPoint(bounds, dy), getBottomPoint(bounds, dx),
				getLeftPoint(bounds, dy) };
	}
}
