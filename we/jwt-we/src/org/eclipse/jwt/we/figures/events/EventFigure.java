/**
 * File:    EventFigure.java
 * Created: 04.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.events;

import org.eclipse.jwt.we.figures.core.IconFigure;


/**
 * Figure for an {@link Event}.
 * 
 * It displays an icon inside a circle.
 * 
 * @version $Id: EventFigure.java,v 1.8 2009-11-26 12:41:53 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class EventFigure
		extends IconFigure
{

	/**
	 * Constructs the figure.
	 */
	public EventFigure()
	{
		// show only the icon with no border
		//setBorder(new EllipseBorder());
	}
}
