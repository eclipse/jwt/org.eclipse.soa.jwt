/**
 * File:    CompositeImageFactory.java
 * Created: 02.02.2009
 *
/*******************************************************************************
 * Copyright (C) 2007  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - initial API and implementation.
 *******************************************************************************/

package org.eclipse.jwt.we.figures.internal;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jwt.we.figures.IImageFactory;
import org.eclipse.swt.graphics.Image;

/**
 * This factory for images contains a collection of IImageFactory and implements also the IImageFactory interface.
 * It allows to create/get images with one of the IImageFactory in the collection.
 * @author Marc Blachon, Bull SAS, France, www.bull.com
 */

public class CompositeImageFactory implements IImageFactory {

	/**
	 * Collection of ImageFactory
	 */
	Collection<IImageFactory> collection;
	
	/**
	 * Constructor
	 */
	public CompositeImageFactory() {
		collection = new ArrayList<IImageFactory>();
	}
	
	/**
	 * Returns an image descriptor for the image file at the given plug-in relative path.
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public ImageDescriptor createImageDescriptor(String path) {
		for (IImageFactory imageFactory:collection) {
			final ImageDescriptor result = imageFactory.createImageDescriptor(path);
			if (result != null) return result; 
		}
		return null;
	}

	/**
	 * Creates an ImageDescriptor that represents the type of a model object.
	 * 
	 * @param model
	 *            The model object.
	 * @return The ImageDescriptor.
	 */
	public ImageDescriptor createModelTypeImageDescriptor(Object model) {
		for (IImageFactory imageFactory:collection) {
			final ImageDescriptor result = imageFactory.createModelTypeImageDescriptor(model);
			if (result != null) return result; 
		}
		return null;
	}

	/**
	 * Returns an image.
	 * 
	 * <p>
	 * The {@link ImageRegistry} handles dispose of the image.
	 * </p>
	 * 
	 * @param imageDescriptor
	 *            The {@link ImageDescriptor} that creates the image. Its String
	 *            representations is the key in the {@link ImageRegistry}.
	 * @return The image, or <code>null</code> if it could not be found.
	 */
	public Image getImage(ImageDescriptor imageDescriptor) {
		for (IImageFactory imageFactory:collection) {
			final Image result = imageFactory.getImage(imageDescriptor);
			if (result != null) return result; 
		}
		return null;
	}

	/**
	 * Returns an image from the {@link ImageRegistry}.
	 * 
	 * <p>
	 * The image will be scaled to the given size.
	 * </p>
	 * 
	 * @param imageDescriptor
	 *            The {@link ImageDescriptor} that creates the image. Its String
	 *            representations is the key in the {@link ImageRegistry}.
	 * @param size
	 *            The size of the returned image. If <code>null</code> the image is
	 *            returned unscaled.
	 * @return The image, or <code>null</code> if it could not be found.
	 */
	public Image getImage(ImageDescriptor imageDescriptor, Dimension size) {
		for (IImageFactory imageFactory:collection) {
			final Image result = imageFactory.getImage(imageDescriptor, size);
			if (result != null) return result; 
		}
		return null;
	}
	
	/**
	 * Adds imageFactory to collection.
	 * @param imageFactory
	 */
	public void  addImageFactory (IImageFactory imageFactory) {
		if (imageFactory != null) {			
			collection.add(imageFactory);
		} else {
			throw new IllegalArgumentException("Error: the argument is null!"); //$NON-NLS-1$
		} 
		
	}
	
	/**
	 * Removes the imageFactory fromthe collection.
	 * @param imageFactory
	 */
	public void removeImageFactory (IImageFactory imageFactory) {
		if (imageFactory != null) {
			if (collection.contains(imageFactory)) {
				collection.remove(imageFactory);
			}				
		} else {
			throw new IllegalArgumentException("Error: the argument is null!"); //$NON-NLS-1$
		} 
	}
	
	/**
	 * 
	 * @param imageFactory
	 */
	public void setImageFactory (Collection<IImageFactory> colImageFactory) {
		if (colImageFactory != null) {
			collection = colImageFactory;
		} else {
			throw new IllegalArgumentException("Error: the argument is null!"); //$NON-NLS-1$
		}
	}
}
