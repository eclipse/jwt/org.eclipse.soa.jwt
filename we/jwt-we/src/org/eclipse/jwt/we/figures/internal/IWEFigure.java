/**
 * File:    IWEFigure.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - use of IImageFactory instead of ImageFactory. 
 *******************************************************************************/

package org.eclipse.jwt.we.figures.internal;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.we.figures.IImageFactory;


/**
 * This is an interface for a typical workflow editor figure. All figures used should
 * extend this class in order to ensure compatibility with all parts of the graphical
 * editor handling code.
 * 
 * @version $Id: IWEFigure.java,v 1.3 2009-11-26 12:41:39 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
/**
 * TODO Description of this type.
 * 
 * @version $Id: IWEFigure.java,v 1.3 2009-11-26 12:41:39 chsaad Exp $
 * @author Programming distributed Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
/**
 * TODO Description of this type.
 * 
 * @version $Id: IWEFigure.java,v 1.3 2009-11-26 12:41:39 chsaad Exp $
 * @author Programming distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 */
public interface IWEFigure
{

	/**
	 * Activate alpha (e.g. when displayed as feedback figure).
	 * 
	 * @param alpha
	 */
	public void setAlpha(boolean alpha);


	/**
	 * Set the displayed name of the figure.
	 * 
	 * @param name
	 */
	public void setName(String name);


	/**
	 * Set the icon of the figure.
	 * 
	 * @param icon
	 */
	public void setIcon(ImageDescriptor icon);


	/**
	 * Set the image factory of the figure.
	 * 
	 * @param imageFactory
	 */
	public void setImageFactory(IImageFactory imageFactory);
}
