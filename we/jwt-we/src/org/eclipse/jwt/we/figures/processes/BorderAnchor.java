/**
 * File:    BorderAnchor.java
 * Created: 29.03.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;


/**
 * Anchor that calculates the nearest point of a set of points on the border of a
 * rectengular figure.
 * 
 * @version $Id: BorderAnchor.java,v 1.7 2009-11-26 12:41:12 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public abstract class BorderAnchor
		extends AbstractConnectionAnchor
{

	/**
	 * @param figure
	 *            The figure to calculate the anchor for.
	 */
	public BorderAnchor(IFigure figure)
	{
		super(figure);
	}


	/**
	 * Returs the nearest point to a reference point.
	 * 
	 * @param reference
	 *            The reference point.
	 * @param points
	 *            The points to test.
	 * @return The nearest point.
	 */
	public Point getNearestPoint(Point reference, Point[] points)
	{
		Point resultPoint = null;
		double nearestDistance = Double.POSITIVE_INFINITY;

		for (Point point : points)
		{
			double distance = reference.getDistance(point);
			if (distance < nearestDistance)
			{
				nearestDistance = distance;
				resultPoint = point;
			}
		}

		return resultPoint;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.ConnectionAnchor#getLocation(org.eclipse.draw2d.geometry.Point)
	 */
	public Point getLocation(Point reference)
	{
		Rectangle bounds = Rectangle.SINGLETON.setBounds(getOwner().getBounds());
		getOwner().translateToAbsolute(bounds);

		return getNearestPoint(reference, getPoints(bounds));
	}


	/**
	 * Calculates all possible points for the given bounds.
	 * 
	 * @param bounds
	 *            The bounds of figure.
	 * @return An array of points.
	 */
	public abstract Point[] getPoints(Rectangle bounds);


	/**
	 * Returns a point on the top border shifted horizontal by delta.
	 * 
	 * @param bounds
	 *            The bounds of figure.
	 * @param delta
	 *            The amount to shift the point.
	 * @return The point.
	 */
	public Point getTopPoint(Rectangle bounds, int delta)
	{
		return new Point(bounds.x + delta, bounds.y);
	}


	/**
	 * Returns a point on the bottom border shifted horizontal by delta.
	 * 
	 * @param bounds
	 *            The bounds of figure.
	 * @param delta
	 *            The amount to shift the point.
	 * @return The point.
	 */
	public Point getBottomPoint(Rectangle bounds, int delta)
	{
		return new Point(bounds.x + delta, bounds.y + bounds.height);
	}


	/**
	 * Returns a point on the left border shifted vertical by delta.
	 * 
	 * @param bounds
	 *            The bounds of figure.
	 * @param delta
	 *            The amount to shift the point.
	 * @return The point.
	 */
	public Point getLeftPoint(Rectangle bounds, int delta)
	{
		return new Point(bounds.x, bounds.y + delta);
	}


	/**
	 * Returns a point on the right border shifted vertical by delta.
	 * 
	 * @param bounds
	 *            The bounds of figure.
	 * @param delta
	 *            The amount to shift the point.
	 * @return The point.
	 */
	public Point getRightPoint(Rectangle bounds, int delta)
	{
		return new Point(bounds.x + bounds.width, bounds.y + delta);
	}
}
