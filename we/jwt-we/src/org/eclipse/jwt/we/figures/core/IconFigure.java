/**
 * File:    IconFigure.java
 * Created: 05.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.figures.core;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Image;


/**
 * Figure that displays an icon without text.
 * 
 * @version $Id: IconFigure.java,v 1.8 2009-11-26 12:41:02 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class IconFigure
		extends NamedElementFigure
{

	/**
	 * The icon to display.
	 */
	private Image iconImage;


	/**
	 * Constructs the figure.
	 */
	public IconFigure()
	{
		super(null);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.NamedElementFigure#getPreferredIconSize()
	 */
	@Override
	public Dimension getPreferredIconSize()
	{
		// scaling is done in paintFigure
		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.core.NamedElementFigure#setIcon(org.eclipse.swt.graphics.Image)
	 */
	@Override
	public void setIcon(Image icon)
	{
		iconImage = icon;
	}


	/**
	 * Returns the area where the icon is drawn. The standard area is the client area.
	 * 
	 * @return The area for the icon.
	 */
	protected Rectangle getIconClientArea()
	{
		return getClientArea();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
	 */
	@Override
	protected void paintFigure(Graphics graphics)
	{
		super.paintFigure(graphics);

		if (iconImage == null)
		{
			return;
		}

		Rectangle area = getIconClientArea();
		org.eclipse.swt.graphics.Rectangle imageBounds = iconImage.getBounds();

		graphics.drawImage(iconImage, imageBounds.x, imageBounds.y, imageBounds.width,
				imageBounds.height, area.x, area.y, area.width, area.height);
	}

}
