/**
 * File:    ActivityLinkNodeFigure.java
 * Created: 14.12.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- adapted for jwt
 *******************************************************************************/

package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jwt.meta.model.processes.ActivityLinkNode;
import org.eclipse.jwt.we.figures.core.NamedIconFigure;


/**
 * Figure for an {@link ActivityLinkNode}.
 * 
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ActivityLinkNodeFigure
		extends NamedIconFigure

{

	/**
	 * Creates the figure.
	 */
	public ActivityLinkNodeFigure()
	{
		super();
		setBorder(new ActivityLinkNodeBorder());
	}


	/**
	 * This function makes sure that the label is shown completely.
	 * 
	 * @see org.eclipse.jwt.we.figures.core.ModelElementFigure#getPreferredSize(int, int)
	 */
	@Override
	public Dimension getPreferredSize(int wHint, int hHint)
	{
		return getPreferredChildrenSize(wHint, hHint);
	}


	/**
	 * @return The size the icons should have.
	 */
	public Dimension getPreferredIconSize()
	{
		return new Dimension(16, 16);
	}

}