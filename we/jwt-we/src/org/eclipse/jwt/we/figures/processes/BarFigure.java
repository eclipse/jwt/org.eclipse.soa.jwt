/**
 * File:    BarFigure.java
 * Created: 29.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.figures.core.ModelElementFigure;


/**
 * Figure displaying a horizontal or vertikal bar.
 * 
 * @version $Id: BarFigure.java,v 1.9 2009-11-26 12:41:12 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class BarFigure
		extends ModelElementFigure
{

	/**
	 * The direction of the bar.
	 */
	private boolean horizontal = false;


	/**
	 * Creates the figure.
	 */
	public BarFigure()
	{
		this(false);
	}


	/**
	 * Creates the figure.
	 * 
	 * @param horizontal
	 *            The direction of the bar.
	 */
	public BarFigure(boolean horizontal)
	{
		super();

		this.horizontal = horizontal;
		setOpaque(true);
		setBackgroundColor(PreferenceReader.appearanceBorderColor.get());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Figure#setBounds(org.eclipse.draw2d.geometry.Rectangle)
	 */
	@Override
	public void setBounds(Rectangle rect)
	{
		// fix the width
		if (horizontal)
		{
			rect.height = PreferenceReader.appearanceBarFigureWidth.get();
		}
		else
		{
			rect.width = PreferenceReader.appearanceBarFigureWidth.get();
		}

		super.setBounds(rect);
	}
}
