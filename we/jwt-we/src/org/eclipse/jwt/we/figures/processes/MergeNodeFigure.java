/**
 * File:    MergeNodeFigure.java
 * Created: 15.12.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - getting ImageFactory from factory registry.
 *******************************************************************************/

package org.eclipse.jwt.we.figures.processes;

import java.util.MissingResourceException;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.meta.model.processes.impl.ProcessesFactoryImpl;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.views.Views;
import org.eclipse.swt.graphics.Image;

/**
 * A figure which displays an icon.
 * 
 * @version $Id: MergeNodeFigure.java,v 1.11 2009-12-02 12:18:03 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class MergeNodeFigure extends DiamondFigure
{

	/**
	 * Creates the figure
	 */
	public MergeNodeFigure()
	{
		super();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.figures.core.IconFigure#setIcon(org.eclipse.swt.graphics
	 * .Image)
	 */
	@Override
	public void setIcon(Image icon)
	{
		// use the large icon (without visible bounds) instead of the small one
		String path;
		try
		{
			path = PluginProperties.model_largeIcon(ProcessesFactoryImpl.eINSTANCE
					.createMergeNode());
		}
		catch (MissingResourceException e)
		{
			super.setIcon(icon);
			return;
		}

		ImageDescriptor imageDescriptor = Plugin.getInstance().getFactoryRegistry()
				.getImageFactory(Views.getInstance().getSelectedView())
				.createImageDescriptor(path);

		// test if the image is available
		if (imageDescriptor == null || imageDescriptor.getImageData() == null)
		{
			super.setIcon(icon);
			return;
		}
		else
		{
			super.setIcon(Plugin.getDefault().getFactoryRegistry().getImageFactory()
					.getImage(imageDescriptor));
		}
	}

}
