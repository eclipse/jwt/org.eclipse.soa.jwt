/**
 * File:    BarMiddleAnchor.java
 * Created: 29.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.figures.processes;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;


/**
 * This anchor returns anchor points in the middle of a border.
 * 
 * If horizontal it creates point in the middle of the left and right border. If vertical
 * it creates point in the middle of the top and bottom border.
 * 
 * @version $Id: BarMiddleAnchor.java,v 1.7 2009-11-26 12:41:06 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class BarMiddleAnchor
		extends BorderAnchor
{

	/**
	 * The direction of the points.
	 */
	private boolean horizontal = false;


	/**
	 * @param figure
	 *            The figure to calculate the anchor for.
	 */
	public BarMiddleAnchor(IFigure figure)
	{
		super(figure);
	}


	/**
	 * @param figure
	 *            The figure to calculate the anchor for.
	 * @param horizontal
	 *            The direction of the points.
	 */
	public BarMiddleAnchor(IFigure figure, boolean horizontal)
	{
		super(figure);
		this.horizontal = horizontal;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.figures.processes.BorderAnchor#getPoints(org.eclipse.draw2d.geometry.Rectangle)
	 */
	@Override
	public Point[] getPoints(Rectangle bounds)
	{
		if (horizontal)
		{
			int delta = bounds.width / 2;
			return new Point[]
			{ getTopPoint(bounds, delta), getBottomPoint(bounds, delta) };
		}

		int delta = bounds.height / 2;
		return new Point[]
		{ getLeftPoint(bounds, delta), getRightPoint(bounds, delta) };
	}

}
