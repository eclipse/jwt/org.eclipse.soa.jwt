/**
 * File:    CreateDatatypeDialog.java
 * Created: 04.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Seitz, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.wizards.model;

import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;


/**
 * A dialog to specify the new of a new package.
 * 
 * @version $Id: CreatePackageDialog.java,v 1.7 2009-11-26 12:41:02 chsaad Exp $
 * @author Christian Seitz (chr.seitz@gmx.de), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org.
 */
public class CreatePackageDialog
		extends Dialog
{

	/**
	 * @param parent
	 */
	public CreatePackageDialog(Shell parent)
	{
		super(parent);

	}


	/**
	 * @param parent
	 * @param style
	 */
	public CreatePackageDialog(Shell parent, int style)
	{
		super(parent, style);

	}


	/**
	 * Opens the dialog
	 * 
	 * @return The entered name for the new datatype
	 */
	public void open(SelectNameWizardPage page)
	{
		final SelectNameWizardPage wizardpage = page;
		Shell parent = getParent();
		final Shell shell = new Shell(parent, SWT.TITLE | SWT.BORDER
				| SWT.APPLICATION_MODAL);
		shell.setText(PluginProperties.wizards_NewPackage);

		shell.setLayout(new GridLayout(2, true));
		Label label = new Label(shell, SWT.NULL);
		label.setText(PluginProperties.wizards_PackagDialog_name);

		final Text text = new Text(shell, SWT.SINGLE | SWT.BORDER);
		text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		final Button buttonOk = new Button(shell, SWT.PUSH);
		buttonOk.setText(PluginProperties.editor_Ok_message);
		buttonOk.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		buttonOk.setEnabled(false);
		Button buttonAbort = new Button(shell, SWT.PUSH);
		buttonAbort.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		buttonAbort.setText(PluginProperties.editor_Cancel_message);

		text.addListener(SWT.Modify, new Listener()
		{

			public void handleEvent(Event event)
			{
				if (text.getText() != null)
				{
					if (text.getText().length() != 0)
						buttonOk.setEnabled(true);
					else
						buttonOk.setEnabled(false);
				}
			}
		});

		buttonOk.addListener(SWT.Selection, new Listener()
		{

			public void handleEvent(Event event)
			{
				wizardpage.createSubpackage(text.getText());
				shell.dispose();

			}

		});

		buttonAbort.addListener(SWT.Selection, new Listener()
		{

			public void handleEvent(Event event)
			{
				shell.dispose();
			}
		});

		shell.pack();
		shell.open();

		Rectangle shellBounds = getParent().getBounds();
		Point dialogSize = shell.getSize();

		shell.setLocation(shellBounds.x + (shellBounds.width - dialogSize.x) / 2,
				shellBounds.y + (shellBounds.height - dialogSize.y) / 2);

	}

}
