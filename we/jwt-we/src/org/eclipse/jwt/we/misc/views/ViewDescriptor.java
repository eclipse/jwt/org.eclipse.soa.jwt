/**
 * File:    ViewDescriptor.java
 * Created: 25.09.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - getting ImageFactory from factory registry.
 *******************************************************************************/
package org.eclipse.jwt.we.misc.views;

import java.util.Hashtable;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.we.IFactoryRegistry;
import org.eclipse.jwt.we.editors.actions.managed.views.ViewsAction;
import org.eclipse.jwt.we.figures.IFigureFactory;

/**
 * This file describes an internal or external view.
 * 
 * @version $Id: ViewDescriptor.java,v 1.9 2010-05-10 09:31:27 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of
 *         Augsburg, Germany, www.ds-lab.org
 */
public class ViewDescriptor
{

	/**
	 * The internal name of the view.
	 */
	private String internalName;

	/**
	 * The official name of the view.
	 */
	private String officialName;

	/**
	 * The data of the view.
	 */
	private Hashtable<String, ViewItemWrapper> viewData;

	/**
	 * The action for this view.
	 */
	private ViewsAction viewAction;

	/**
	 * The action for this view.
	 */
	private IFactoryRegistry factoryRegistry;

	/**
	 * The image.
	 */
	private ImageDescriptor imageDescriptor;


	/**
	 * @return Returns the internalName.
	 */
	public String getInternalName()
	{
		return internalName;
	}


	/**
	 * @param internalName
	 *            The internalName to set.
	 */
	public void setInternalID(String internalName)
	{
		this.internalName = internalName;
	}


	/**
	 * @param officialName
	 *            The officialName to set.
	 */
	public void setOfficialName(String officialName)
	{
		this.officialName = officialName;
	}


	/**
	 * @return Returns the officialname.
	 */
	public String getOfficialName()
	{
		return officialName;
	}


	/**
	 * @return Returns the imageDescriptor.
	 */
	public ImageDescriptor getImageDescriptor()
	{
		return imageDescriptor;
	}


	/**
	 * @param imageDescriptor
	 *            The imageDescriptor to set.
	 */
	public void setImageDescriptor(ImageDescriptor imageDescriptor)
	{
		this.imageDescriptor = imageDescriptor;
	}


	/**
	 * @return Returns the viewData.
	 */
	public Hashtable<String, ViewItemWrapper> getViewData()
	{
		return viewData;
	}


	/**
	 * @param viewData
	 *            The viewData to set.
	 */
	public void setViewData(Hashtable<String, ViewItemWrapper> viewData)
	{
		this.viewData = viewData;
	}


	/**
	 * @return Returns the viewAction.
	 */
	public ViewsAction getViewAction()
	{
		return viewAction;
	}


	/**
	 * @param factoryRegistry
	 *            The factoryRegistry to set.
	 */
	public void setFactoryRegistry(IFactoryRegistry factoryRegistry)
	{
		this.factoryRegistry = factoryRegistry;
	}


	/**
	 * @param viewAction
	 *            The viewAction to set.
	 */
	public void setViewAction(ViewsAction viewAction)
	{
		this.viewAction = viewAction;
	}


	/**
	 * @return Returns the factoryRegistry.
	 */
	public IFactoryRegistry getFactoryRegistry()
	{
		return factoryRegistry;
	}

}
