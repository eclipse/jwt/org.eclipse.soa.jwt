/**
 * File:    TransferFactory.java
 * Created: 14.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.factories;

import org.eclipse.gef.requests.CreationFactory;


/**
 * A factory that does not create a new object, but simply transfers a given object.
 * 
 * <p>
 * This class can be used for drag & drop.
 * </p>
 * 
 * @version $Id: TransferFactory.java,v 1.6 2009-11-26 12:41:32 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class TransferFactory
		implements CreationFactory
{

	/**
	 * The object that is transfered.
	 */
	private Object object;


	/**
	 * @param object
	 *            The object that is transfered.
	 */
	public TransferFactory(Object object)
	{
		assert object != null;
		this.object = object;
	}


	/**
	 * @return The object that is transfered. Not <code>null</code>.
	 */
	public Object getTransferObject()
	{
		return object;
	}


	/**
	 * @return The object itself.
	 * 
	 * @see org.eclipse.gef.requests.CreationFactory#getNewObject()
	 */
	public Object getNewObject()
	{
		return getTransferObject();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.requests.CreationFactory#getObjectType()
	 */
	public Object getObjectType()
	{
		return object.getClass();
	}
}
