/**
 * File:    ActivityViewConfWizardPage.java
 * Created: 08.01.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.wizards.view.pages;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.misc.wizards.WizardHelper;
import org.eclipse.jwt.we.misc.wizards.view.ViewConfWizard;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IWorkbench;


/**
 * Select the activities on which the import/layouting should be carried out.
 * 
 * @version $Id: ActivityViewConfWizardPage.java,v 1.1.2.2 2009/03/05 17:00:30 chsaad Exp
 *          $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class ActivityViewConfWizardPage
		extends WizardPage
{

	/**
	 * A Logger.
	 */
	static final Logger logger = Logger.getLogger(ActivityViewConfWizardPage.class);

	/**
	 * Select all activities.
	 */
	private Button allActivities;

	/**
	 * Select specific activities.
	 */
	private Button selectedActivities;

	/**
	 * The list of available activities.
	 */
	private Table activityList;


	/**
	 * Constructor.
	 * 
	 * @param pageId
	 *            The name of the page.
	 * @param workbench
	 *            The workbench.
	 */
	public ActivityViewConfWizardPage(String pageId, IWorkbench workbench)
	{
		super(pageId);
	}


	/**
	 * @return The EMF EditingDomain.
	 */
	public EditingDomain getEditingDomain()
	{
		return (EditingDomain) GeneralHelper.getActiveInstance().getAdapter(
				EditingDomain.class);
	}


	/**
	 * @return The {@link AdapterFactory} from an {@link AdapterFactoryEditingDomain} if
	 *         available.
	 */
	public AdapterFactory getAdapterFactory()
	{
		return (AdapterFactory) GeneralHelper.getActiveInstance().getAdapter(
				AdapterFactory.class);
	}


	/**
	 * Returns a list of selected algorithms.
	 * 
	 * @return The selected algorithms.
	 */
	public ArrayList<Activity> getSelectedActivities()
	{
		ArrayList<Activity> result = new ArrayList<Activity>();

		if (allActivities.getSelection())
		{
			// return all activities
			for (TableItem tableItem : activityList.getItems())
			{
				result.add((Activity) tableItem.getData());
			}
		}
		else
		{
			// return only selected activities
			for (TableItem tableItem : activityList.getItems())
			{
				if (tableItem.getChecked())
				{
					result.add((Activity) tableItem.getData());
				}
			}
		}

		return result;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite
	 * )
	 */
	public void createControl(Composite parent)
	{
		// Composite
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));
		composite.setLayoutData(WizardHelper.createData(GridData.FILL, GridData.FILL,
				true, false, -1, -1));

		// select all activities
		allActivities = new Button(composite, SWT.RADIO);
		allActivities.setLayoutData(WizardHelper.createData(-1, -1, false, false, 1, 1));
		allActivities.setText(PluginProperties.wizards_ViewConfActivityAll_label);
		allActivities.addSelectionListener(new SelectionListener()
		{

			public void widgetDefaultSelected(SelectionEvent e)
			{
			}


			public void widgetSelected(SelectionEvent e)
			{
				activityList.setEnabled(false);
			}
		});

		// only selected activities
		selectedActivities = new Button(composite, SWT.RADIO);
		selectedActivities.setLayoutData(WizardHelper.createData(-1, -1, false, false, 1,
				1));
		selectedActivities
				.setText(PluginProperties.wizards_ViewConfActivitySelected_label);
		selectedActivities.addSelectionListener(new SelectionListener()
		{

			public void widgetDefaultSelected(SelectionEvent e)
			{
			}


			public void widgetSelected(SelectionEvent e)
			{
				activityList.setEnabled(true);
			}
		});

		// the list of all activities
		activityList = new Table(composite, SWT.CHECK);
		activityList.setLayoutData(WizardHelper.createData(-1, -1, false, false, 1, 1));
		activityList.setBackground(composite.getBackground());

		for (Iterator iter = EcoreUtil.getAllContents((EObject) GeneralHelper
				.getActiveInstance().getModel(), true); iter.hasNext();)
		{
			EObject eobject = (EObject) iter.next();

			if (eobject instanceof Activity)
			{
				TableItem tableItem = new TableItem(activityList, SWT.NONE);
				if (((Activity) eobject).getName() != null)
				{
					tableItem.setText(((Activity) eobject).getName());
				}
				tableItem.setData(eobject);
			}
		}

		// set to all activities as default
		allActivities.setSelection(true);
		activityList.setEnabled(false);

		// Validate
		setMessage(PluginProperties.wizards_ViewConfActivity_message);
		setControl(composite);

		// refresh packageviewer display
		getShell().pack();

		// make dialog bigger and center
		WizardHelper.resizeDialog(getShell());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage#getNextPage()
	 */
	@Override
	public IWizardPage getNextPage()
	{
		// remove preview layoutdata (if coming from preview page)
		((ViewConfWizard) getWizard()).disposePreview();

		return getWizard().getPage("preview"); //$NON-NLS-1$
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite
	 * )
	 */
	@Override
	public void dispose()
	{
		if (allActivities != null && !allActivities.isDisposed())
		{
			allActivities.dispose();
			allActivities = null;
		}

		if (selectedActivities != null && !selectedActivities.isDisposed())
		{
			selectedActivities.dispose();
			selectedActivities = null;
		}

		if (activityList != null && !activityList.isDisposed())
		{
			activityList.dispose();
			activityList = null;
		}

		super.dispose();
	}

}