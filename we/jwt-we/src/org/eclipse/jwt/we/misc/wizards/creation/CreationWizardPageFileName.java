/**
 * File:    CreationWizardPageFileName.java
 * Created: 13.03.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.wizards.creation;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.actions.handlers.OpenFileHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;


/**
 * This is the page where the filename of the new model can be entered along with the name
 * of the standard {@link Package} and {@link Activity} which are created automatically.
 * 
 * @version $Id: CreationWizardTemplatePageHelper.java
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class CreationWizardPageFileName
		extends WizardPage
{

	/**
	 * The textfield for the package name.
	 */
	protected Text packageField;

	/**
	 * The textfield for the activity name.
	 */
	protected Text activityField;

	/**
	 * The textfield for the file name.
	 */
	protected Text fileField;

	/**
	 * The textfield for the author.
	 */
	protected Text authorField;

	/**
	 * The textfield for the version.
	 */
	protected Text versionField;

	/**
	 * Show file name field?
	 */
	protected boolean showFileNameField;

	/**
	 * The Checkbox for standard packages
	 */
	protected Button standardPackagesCheckbox;


	/**
	 * Pass in the selection.
	 * 
	 * @param pageId
	 *            The name of the page.
	 */
	public CreationWizardPageFileName(String pageId, boolean showFileNameField)
	{
		super(pageId);
		this.showFileNameField = showFileNameField;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	public void createControl(Composite parent)
	{
		Composite composite = new Composite(parent, SWT.NONE);
		{
			GridLayout layout = new GridLayout();
			layout.numColumns = 1;
			layout.verticalSpacing = 12;
			composite.setLayout(layout);

			GridData data = new GridData();
			data.verticalAlignment = GridData.FILL;
			data.grabExcessVerticalSpace = true;
			data.horizontalAlignment = GridData.FILL;
			composite.setLayoutData(data);
		}

		// package name
		Label packageNameLabel = new Label(composite, SWT.LEFT);
		{
			packageNameLabel.setText(PluginProperties.wizards_ModelWizardPackage_label);

			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			packageNameLabel.setLayoutData(data);
		}

		packageField = new Text(composite, SWT.BORDER);
		{
			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			data.grabExcessHorizontalSpace = true;
			data.horizontalSpan = 1;
			packageField.setLayoutData(data);
			packageField.setText(PluginProperties.wizards_ModelWizardPackage_std);
		}

		// activity name
		Label activityNameLabel = new Label(composite, SWT.LEFT);
		{
			activityNameLabel.setText(PluginProperties.wizards_ModelWizardActivity_label);

			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			activityNameLabel.setLayoutData(data);
		}

		activityField = new Text(composite, SWT.BORDER);
		{
			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			data.grabExcessHorizontalSpace = true;
			data.horizontalSpan = 1;
			activityField.setLayoutData(data);
			activityField.setText(PluginProperties.wizards_ModelWizardActivity_std);
		}

		// author
		Label authorLabel = new Label(composite, SWT.LEFT);
		{
			authorLabel.setText(PluginProperties.wizards_ModelWizardAuthor_label);

			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			authorLabel.setLayoutData(data);
		}

		authorField = new Text(composite, SWT.BORDER);
		{
			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			data.grabExcessHorizontalSpace = true;
			data.horizontalSpan = 1;
			authorField.setLayoutData(data);
		}

		// version
		Label versionLabel = new Label(composite, SWT.LEFT);
		{
			versionLabel.setText(PluginProperties.wizards_ModelWizardVersion_label);

			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			versionLabel.setLayoutData(data);
		}

		versionField = new Text(composite, SWT.BORDER);
		{
			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			data.grabExcessHorizontalSpace = true;
			data.horizontalSpan = 1;
			versionField.setLayoutData(data);
		}

		// standard packages checkbox
		Label dummyLabel = new Label(composite, SWT.LEFT);
		dummyLabel.setLayoutData(new GridData());
		standardPackagesCheckbox = new Button(composite, SWT.CHECK);
		standardPackagesCheckbox.setText(PluginProperties.wizards_ModelWizardStandardPackages_label);
		standardPackagesCheckbox.setSelection(true);

		// file
		Label resourceURILabel = new Label(composite, SWT.LEFT);
		{
			resourceURILabel.setText(PluginProperties.wizards_ModelWizardFile_label);

			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			resourceURILabel.setLayoutData(data);
		}

		Composite fileComposite = new Composite(composite, SWT.NONE);
		{
			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			data.grabExcessHorizontalSpace = true;
			fileComposite.setLayoutData(data);

			GridLayout layout = new GridLayout();
			data.horizontalAlignment = GridData.FILL;
			layout.marginHeight = 0;
			layout.marginWidth = 0;
			layout.numColumns = 2;
			fileComposite.setLayout(layout);
		}

		fileField = new Text(fileComposite, SWT.BORDER);
		{
			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			data.grabExcessHorizontalSpace = true;
			data.horizontalSpan = 1;
			fileField.setLayoutData(data);
		}

		fileField.addModifyListener(validator);

		Button resourceURIBrowseFileSystemButton = new Button(fileComposite, SWT.PUSH);
		resourceURIBrowseFileSystemButton.setText(PluginProperties.browse_button);

		resourceURIBrowseFileSystemButton.addSelectionListener(new SelectionAdapter()
		{

			@Override
			public void widgetSelected(SelectionEvent event)
			{
				String fileExtension = WEEditor.getWorkflowExtension();
				String filePath = OpenFileHandler.openFilePathDialog(getShell(), null,
						SWT.OPEN);
				if (filePath != null)
				{
					if (!filePath.endsWith("." + fileExtension)) //$NON-NLS-1$
					{
						filePath = filePath + "." + fileExtension; //$NON-NLS-1$
					}
					fileField.setText(filePath);
				}
			}
		});

		// in project mode, disable file name (because it's entered on separate page)
		if (!showFileNameField)
		{
			resourceURILabel.setVisible(false);
			resourceURIBrowseFileSystemButton.setVisible(false);
			fileField.setVisible(false);
		}

		// finish creation
		setPageComplete(validatePage());
		setControl(composite);
	}

	/**
	 * Checks if filename was entered
	 */
	protected ModifyListener validator = new ModifyListener()
	{

		public void modifyText(ModifyEvent e)
		{
			setPageComplete(validatePage());
		}
	};


	/**
	 * Validates the page (file name must be entered).
	 */
	protected boolean validatePage()
	{
		URI fileURI = getFileURI();
		setErrorMessage(null);

		if (showFileNameField && (fileURI == null || fileURI.isEmpty()))
		{
			return false;
		}

		return true;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.DialogPage#setVisible(boolean)
	 */
	@Override
	public void setVisible(boolean visible)
	{
		super.setVisible(visible);
		if (visible && fileField.getVisible())
		{
			fileField.setFocus();
		}
	}


	/**
	 * @return The entered URI.
	 */
	public URI getFileURI()
	{
		try
		{
			return URI.createFileURI(fileField.getText());
		}
		catch (Exception exception)
		{
		}
		return null;
	}


	/**
	 * @return The entered package name.
	 */
	public String getPackageName()
	{
		return packageField.getText();
	}


	/**
	 * @return The entered activity name.
	 */
	public String getActivityName()
	{
		return activityField.getText();
	}


	/**
	 * @return The entered author.
	 */
	public String getAuthor()
	{
		return authorField.getText();
	}


	/**
	 * @return The entered version.
	 */
	public String getVersion()
	{
		return versionField.getText();
	}


	/**
	 * Select the filename textfield
	 */
	public void selectFileField()
	{
		fileField.selectAll();
		fileField.setFocus();
	}


	/**
	 * @return The standard packages state.
	 */
	public boolean getStandardPackages()
	{
		return standardPackagesCheckbox.getSelection();
	}

}