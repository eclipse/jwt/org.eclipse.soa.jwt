/**
 * File:    ModeViewConfWizardPage.java
 * Created: 15.12.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.wizards.view.pages;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.misc.wizards.WizardHelper;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;


/**
 * Select the mode for creating layout data.
 * 
 * @version $Id: ModeViewConfWizardPage.java,v 1.4 2009-12-14 13:59:17 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class ModeViewConfWizardPage
		extends WizardPage
{

	/**
	 * A Logger.
	 */
	static final Logger logger = Logger.getLogger(ModeViewConfWizardPage.class);

	/**
	 * The radio button for import.
	 */
	private Button importViewData;

	/**
	 * The radio button for create.
	 */
	private Button createViewData;


	/**
	 * Pass in the selection.
	 * 
	 * @param pageId
	 *            The name of the page.
	 * @param workbench
	 *            The workbench.
	 */
	public ModeViewConfWizardPage(String pageId, IWorkbench workbench)
	{
		super(pageId);
	}


	/**
	 * @return The EMF EditingDomain.
	 */
	public EditingDomain getEditingDomain()
	{
		return (EditingDomain) GeneralHelper.getActiveInstance().getAdapter(
				EditingDomain.class);
	}


	/**
	 * @return The {@link AdapterFactory} from an {@link AdapterFactoryEditingDomain} if
	 *         available.
	 */
	public AdapterFactory getAdapterFactory()
	{
		return (AdapterFactory) GeneralHelper.getActiveInstance().getAdapter(
				AdapterFactory.class);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite
	 * )
	 */
	public void createControl(Composite parent)
	{
		// Composite
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));
		composite.setLayoutData(WizardHelper.createData(GridData.FILL, GridData.FILL,
				true, false, -1, -1));

		// create
		createViewData = new Button(composite, SWT.RADIO);
		createViewData
				.setText(PluginProperties.wizards_ViewConfCreate_label);
		createViewData.setLayoutData(WizardHelper.createData(-1, GridData.BEGINNING,
				false, false, -1, 1));

		// import
		importViewData = new Button(composite, SWT.RADIO);
		importViewData
				.setText(PluginProperties.wizards_ViewConfImport_label);
		importViewData.setLayoutData(WizardHelper.createData(-1, GridData.BEGINNING,
				false, false, -1, 1));

		// initialize with create mode
		createViewData.setSelection(true);

		// Validate
		setMessage(null);
		setControl(composite);

		// refresh packageviewer display
		getShell().pack();

		// make dialog bigger and center
		WizardHelper.resizeDialog(getShell());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage#getNextPage()
	 */
	@Override
	public IWizardPage getNextPage()
	{
		if (importViewData.getSelection())
		{
			return getWizard().getPage("import"); //$NON-NLS-1$
		}
		else if (createViewData.getSelection())
		{
			return getWizard().getPage("create"); //$NON-NLS-1$
		}

		return null;
	}


	/**
	 * Import mode activated?
	 * 
	 * @return mode.
	 */
	public boolean isImportMode()
	{
		return importViewData.getSelection();
	}


	/**
	 * Import mode activated?
	 * 
	 * @return mode.
	 */
	public boolean isCreateMode()
	{
		return createViewData.getSelection();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite
	 * )
	 */
	@Override
	public void dispose()
	{
		if (!importViewData.isDisposed())
		{
			importViewData.dispose();
			importViewData = null;
		}

		if (!createViewData.isDisposed())
		{
			createViewData.dispose();
			createViewData = null;
		}

		super.dispose();
	}

}