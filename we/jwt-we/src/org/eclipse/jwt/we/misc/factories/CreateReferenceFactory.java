/**
 * File:    CreateReferenceFactory.java
 * Created: 27.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.factories;

import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.model.view.ViewFactory;


/**
 * A factory that creates a {@link Reference} to a {@link ReferenceableElement}.
 * 
 * @version $Id: CreateReferenceFactory.java,v 1.6 2009-11-26 12:41:32 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class CreateReferenceFactory
		implements CreationFactory
{

	/**
	 * The {@link ReferenceableElement} for that a {@link Reference} will is created.
	 */
	private ReferenceableElement element;


	/**
	 * @param element
	 *            The {@link ReferenceableElement} for that a {@link Reference} will is
	 *            created.
	 */
	public CreateReferenceFactory(ReferenceableElement element)
	{
		assert element != null;
		this.element = element;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.requests.CreationFactory#getNewObject()
	 */
	public Object getNewObject()
	{
		Reference reference = ViewFactory.eINSTANCE.createReference();
		reference.setReference(element);

		return reference;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.requests.CreationFactory#getObjectType()
	 */
	public Object getObjectType()
	{
		return Reference.class;
	}

}
