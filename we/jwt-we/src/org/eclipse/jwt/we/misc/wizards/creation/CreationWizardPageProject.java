/**
 * File:    CreationWizardPageProject.java
 * Created: 08.09.2007
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.wizards.creation;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;


/**
 * This WizardPage selects a project in which the file should be created.
 * 
 * @version $Id: CreationWizardPageProject.java
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class CreationWizardPageProject
		extends WizardNewFileCreationPage
{

	/**
	 * A Logger.
	 */
	static final Logger logger = Logger.getLogger(CreationWizardPageProject.class);


	/**
	 * Create a new wizard page instance.
	 * 
	 * @param pageId
	 *            The page id
	 * @param selection
	 *            The current object selection
	 */
	public CreationWizardPageProject(String pageId, IStructuredSelection selection)
	{
		super(pageId, selection);

		// set the appropriate file extension
		this.setFileExtension(WEEditor.getWorkflowExtension());
	}

}
