/**
 * File:    CreateApplicationWizard.java
 * Created: 20.12.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Seitz, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.wizards.model;

import java.util.Collections;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.jwt.meta.model.application.ApplicationFactory;
import org.eclipse.jwt.meta.model.application.WebServiceApplication;
import org.eclipse.jwt.meta.model.core.CorePackage;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.IWorkbench;


/**
 * Creates a new Application using a wizard.
 * 
 * @author Christian Seitz (chr.seitz@gmx.de), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org.
 */
public class CreateApplicationWizard
		extends Wizard
{

	/**
	 * The page for the selection of Name, Package and Icon.
	 */
	protected SelectNameWizardPage initialPage;

	/**
	 * The page for the selection of JAR file, Java class ans method.
	 */
	protected SelectJarWizardPage jarPage;

	/**
	 * The default package.
	 */
	protected org.eclipse.jwt.meta.model.core.Package defaultPackage = null;


	/**
	 * Creates a new wizard.
	 */
	public CreateApplicationWizard()
	{
		super();
		setWindowTitle(PluginProperties.wizards_AppWizard_title);
	}


	/**
	 * Creates a new wizard.
	 */
	public CreateApplicationWizard(org.eclipse.jwt.meta.model.core.Package defaultPackage)
	{
		this();
		this.defaultPackage = defaultPackage;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
	 * org.eclipse.jface.viewers.IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection)
	{
		initialPage = new SelectNameWizardPage(
				org.eclipse.jwt.meta.PluginProperties.model_Application_type,
				defaultPackage, workbench);
		jarPage = new SelectJarWizardPage(
				org.eclipse.jwt.meta.PluginProperties.model_Application_jarArchive_name);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages()
	{
		addPage(initialPage);
		initialPage.setDescription(PluginProperties.wizards_Page_name_package);
		addPage(jarPage);
		jarPage.setDescription(PluginProperties.wizards_Page_jar);

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish()
	{
		String name = initialPage.getName();
		org.eclipse.jwt.meta.model.core.Package selectedPackage = initialPage
				.getPackage();
		String icon = initialPage.getIconPath();
		String jar = jarPage.getJar();
		String selclass = jarPage.getSelClass();
		String method = jarPage.getMethod();
		String webserviceInterface = jarPage.getInterface();
		String webserviceOperation = jarPage.getOperation();

		ApplicationFactory factory = ApplicationFactory.eINSTANCE;
		Application application;
		if (jarPage.getIsWebservice())
		{
			application = factory.createWebServiceApplication();
		}
		else
		{
			application = factory.createApplication();
		}
		application.setName(name);
		application.setIcon(icon);
		application.setJarArchive(jar);
		application.setJavaClass(selclass);
		application.setMethod(method);
		if (jarPage.getIsWebservice())
		{
			((WebServiceApplication) application).setInterface(webserviceInterface);
			((WebServiceApplication) application).setOperation(webserviceOperation);
		}

		// This command creates the new Application
		CommandParameter childDescriptor = new CommandParameter(null,
				CorePackage.Literals.PACKAGE__ELEMENTS, application);
		Command cmd = CreateChildCommand.create(getEditingDomain(), selectedPackage,
				childDescriptor, Collections.singleton(selectedPackage));
		getEditingDomain().getCommandStack().execute(cmd);

		return true;
	}


	/**
	 * @return The EMF EditingDomain.
	 */
	public EditingDomain getEditingDomain()
	{

		return (EditingDomain) GeneralHelper.getActiveInstance().getAdapter(
				EditingDomain.class);
	}

}
