/**
 * File:    EclipseLogHandler.java
 * Created: 13.12.2005
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.logging.internal;

import java.util.logging.ErrorManager;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;


/**
 * This handler publishes log events to the integrated log system of eclipse.
 * 
 * @version $Id: EclipseLogHandler.java,v 1.3 2009-11-26 12:41:04 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class EclipseLogHandler
		extends Handler
{

	/**
	 * The logger for eclipse.
	 */
	private ILog eclipseLogger;

	/**
	 * The plugin to use the handler for.
	 */
	private Plugin plugin;


	/**
	 * Constructor.
	 * 
	 * @param plugin
	 *            The plugin to use the handler for.
	 */
	public EclipseLogHandler(Plugin plugin)
	{
		this.plugin = plugin;
		eclipseLogger = plugin.getLog();
		setFormatter(new EclipseLogFormatter());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.logging.Handler#publish(java.util.logging.LogRecord)
	 */
	@Override
	public void publish(LogRecord record)
	{
		if (!isLoggable(record))
		{
			return;
		}

		int level = record.getLevel().intValue();
		int severity = IStatus.OK;

		if (level >= Level.SEVERE.intValue())
		{
			severity = IStatus.ERROR;
		}
		else if (level >= Level.WARNING.intValue())
		{
			severity = IStatus.WARNING;
		}
		else if (level >= Level.INFO.intValue())
		{
			severity = IStatus.INFO;
		}

		String message = null;
		try
		{
			message = getFormatter().format(record);
		}
		catch (Exception ex)
		{
			// We don't want to throw an exception here, but we
			// report the exception to any registered ErrorManager.
			reportError(null, ex, ErrorManager.FORMAT_FAILURE);
		}

		if (message == null || message.length() == 0 && record.getThrown() != null)
		{
			message = record.getThrown().toString();
		}
		if (message == null)
		{
			message = ""; //$NON-NLS-1$
		}

		Status status = new Status(severity, plugin.getBundle().getSymbolicName(), plugin
				.getBundle().getState(), message, record.getThrown());

		eclipseLogger.log(status);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.logging.Handler#flush()
	 */
	@Override
	public void flush()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.logging.Handler#close()
	 */
	@Override
	public void close() throws SecurityException
	{
	}

}
