/**
 * File:    EcoreCopyFactory.java
 * Created: 24.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.factories;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.views.LayoutDataManager;
import org.eclipse.jwt.we.misc.views.Views;

/**
 * This factory creates new Object by copying existing Ecore {@link EObject}s.
 * 
 * <p>
 * This factory is also a {@link TransferFactory} and allows direct access to
 * the transfered object.
 * </p>
 * 
 * @version $Id: EcoreCopyFactory.java,v 1.7 2009-11-26 12:41:32 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 * @since 0.6.0
 */
public class EcoreCopyFactory extends TransferFactory
{

	/**
	 * The workflow editor.
	 */
	private WEEditor weeditor;


	/**
	 * @param sourceObject
	 *            The object to copy.
	 */
	public EcoreCopyFactory(WEEditor weeditor, EObject sourceObject)
	{
		super(sourceObject);
		this.weeditor = weeditor;
	}


	/*
	 * @param sourceObjects The objects to copy.
	 */
	public EcoreCopyFactory(WEEditor weeditor, Collection sourceObjects)
	{
		super(sourceObjects);
		this.weeditor = weeditor;
	}


	/**
	 * @return A copy of the transfered object.
	 * 
	 * @see org.eclipse.gef.requests.CreationFactory#getNewObject()
	 */
	@Override
	public Object getNewObject()
	{
		// bugfix: dragndrop outline.refelement -> scope.reference (<->
		// scopelayouteditpolicy)
		// 
		// bugfix: activity -> scope.activitylinknode (<->
		// insertscopecommand,insertscopecopycommand)
		// object == refelement -> return refelement
		// object == activity -> return activity
		// object != refelement -> return copy of element
		// 
		// also remove all layoutdata (except the one for the current view)

		if (getTransferObject() instanceof EObject)
		{
			Object returnobject;

			if (getTransferObject() instanceof ReferenceableElement)
				returnobject = getTransferObject();
			else if (getTransferObject() instanceof Activity)
				returnobject = getTransferObject();
			else
				returnobject = EcoreUtil.copy((EObject) getTransferObject());

			// remove layoutdata from copy
			LayoutDataManager.removeLayoutData(weeditor,
					(EObject) returnobject, Views.getInstance()
							.getSelectedView().getInternalName());

			return returnobject;
		}
		else if (getTransferObject() instanceof Collection)
		{
			Collection returnlist = new ArrayList();

			for (Iterator i = ((Collection) getTransferObject()).iterator(); i
					.hasNext();)
			{
				Object object = i.next();
				Object returnobject;

				// use original for refelems and activities
				if (object instanceof ReferenceableElement)
					returnobject = (EObject) object;
				else if (object instanceof Activity)
					returnobject = (EObject) object;
				else
				{
					// create copy
					returnobject = (EObject) EcoreUtil.copy((EObject) object);
					// remove layoutdata from copy
					LayoutDataManager.removeLayoutData(weeditor,
							(EObject) returnobject, Views.getInstance()
									.getSelectedView().getInternalName());
				}

				returnlist.add(returnobject);
			}
			return returnlist;
		}

		return null;
	}
}
