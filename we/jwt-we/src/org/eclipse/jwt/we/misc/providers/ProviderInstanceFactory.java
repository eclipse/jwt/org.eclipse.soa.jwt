package org.eclipse.jwt.we.misc.providers;

import org.eclipse.jwt.meta.providers.interfaces.ICommandProvider;
import org.eclipse.jwt.meta.providers.interfaces.IImageProvider;
import org.eclipse.jwt.meta.providers.interfaces.IVisibilityProvider;
import org.eclipse.jwt.we.editors.WEEditor;

public class ProviderInstanceFactory
{

	private static ProviderInstanceFactory myinstance;

	private IVisibilityProvider visibilityProvider;
	private IImageProvider imageProvider;


	/**
	 * Singleton accessor.
	 * 
	 * @return The only instance of this class.
	 */
	public static ProviderInstanceFactory getInstance()
	{
		if (myinstance == null)
			myinstance = new ProviderInstanceFactory();
		return myinstance;
	}


	/**
	 * @return Returns the defaultVisibilityProvider.
	 */
	public IVisibilityProvider getDefaultVisibilityProvider()
	{
		if (visibilityProvider == null)
		{
			visibilityProvider = new MetaModelVisibilityProvider();
		}
		return visibilityProvider;
	}


	/**
	 * @return Returns the defaultImageProvider.
	 */
	public IImageProvider getDefaultImageProvider()
	{
		if (imageProvider == null)
		{
			imageProvider = new MetaModelImageProvider();
		}
		return imageProvider;
	}


	/**
	 * @return Returns the defaultCommandProvider.
	 */
	public ICommandProvider getDefaultCommandProvider(WEEditor weeditor)
	{
		return new MetaModelCommandProvider(weeditor);
	}
}
