package org.eclipse.jwt.we.misc.providers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jwt.meta.providers.interfaces.IVisibilityProvider;
import org.eclipse.jwt.we.misc.views.Views;


public class MetaModelVisibilityProvider
		implements IVisibilityProvider
{

	public boolean displayObject(Object object)
	{
		return Views.getInstance().displayObject((EObject) object);
	}

	
	
}
