/**
 * File:    EcoreFactory.java
 * Created: 08.12.2005
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.factories;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.jwt.we.misc.logging.Logger;


/**
 * This factory creates new object from Ecore {@link EClass}es.
 * 
 * @version $Id: EcoreFactory.java,v 1.7 2009-11-26 12:41:32 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class EcoreFactory
		implements CreationFactory
{

	/**
	 * The Logger.
	 */
	private static final Logger logger = Logger.getLogger(EcoreFactory.class);

	/**
	 * The class to create.
	 */
	private EClass eClass;


	/**
	 * @param eClass
	 *            The class to create.
	 */
	public EcoreFactory(EClass eClass)
	{
		this.eClass = eClass;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.requests.CreationFactory#getNewObject()
	 */
	public Object getNewObject()
	{
		if (eClass == null)
		{
			return null;
		}

		Object newObject = null;

		try
		{
			newObject = EcoreUtil.create(eClass);
			logger.instanceCreation(eClass.getName());
		}
		catch (Throwable error)
		{
			logger.warning("Object \"" + eClass.getName() //$NON-NLS-1$
					+ "\" could not be instanciated.", error); //$NON-NLS-1$
		}

		return newObject;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.requests.CreationFactory#getObjectType()
	 */
	public Object getObjectType()
	{
		if (eClass == null)
		{
			return null;
		}

		return eClass.getInstanceClass();
	}
}
