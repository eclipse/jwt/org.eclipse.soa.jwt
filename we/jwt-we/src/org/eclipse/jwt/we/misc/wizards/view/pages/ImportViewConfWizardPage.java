/**
 * File:    ImportViewConfWizardPage.java
 * Created: 15.12.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.wizards.view.pages;

import java.util.HashMap;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.misc.util.FontUtil;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.misc.views.LayoutDataManager;
import org.eclipse.jwt.we.misc.views.ViewDescriptor;
import org.eclipse.jwt.we.misc.views.Views;
import org.eclipse.jwt.we.misc.wizards.WizardHelper;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;

/**
 * Import layout data from another view.
 * 
 * @version $Id: ImportViewConfWizardPage.java,v 1.2 2009/11/04 17:18:31 chsaad
 *          Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class ImportViewConfWizardPage extends WizardPage
{

	/**
	 * A Logger.
	 */
	static final Logger logger = Logger
			.getLogger(ImportViewConfWizardPage.class);

	/**
	 * The ID of the selected view
	 */
	private String selectedViewID;

	/**
	 * Import layout data only for new elements.
	 */
	private boolean importOnlyMissingLayoutData;


	/**
	 * Constructor.
	 * 
	 * @param pageId
	 *            The name of the page.
	 * @param workbench
	 *            The workbench.
	 */
	public ImportViewConfWizardPage(String pageId, IWorkbench workbench)
	{
		super(pageId);
	}


	/**
	 * @return The EMF EditingDomain.
	 */
	public EditingDomain getEditingDomain()
	{
		return (EditingDomain) GeneralHelper.getActiveInstance().getAdapter(
				EditingDomain.class);
	}


	/**
	 * @return The {@link AdapterFactory} from an
	 *         {@link AdapterFactoryEditingDomain} if available.
	 */
	public AdapterFactory getAdapterFactory()
	{
		return (AdapterFactory) GeneralHelper.getActiveInstance().getAdapter(
				AdapterFactory.class);
	}


	/**
	 * Returns the ID of the selected view.
	 * 
	 * @return The ID of the selected view.
	 */
	public String getSelectedViewID()
	{
		return selectedViewID;
	}


	/**
	 * Returns importOnlyMissingLayoutData.
	 * 
	 * @return importOnlyMissingLayoutData.
	 */
	public boolean isImportOnlyMissingLayoutData()
	{
		return importOnlyMissingLayoutData;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets
	 * .Composite )
	 */
	public void createControl(Composite parent)
	{
		// Composite
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));
		composite.setLayoutData(WizardHelper.createData(GridData.FILL,
				GridData.FILL, true, false, -1, -1));

		final HashMap scoreImport = LayoutDataManager
				.computeViewScore(GeneralHelper.getActiveInstance());

		// the head line
		Label viewLabel1 = new Label(composite, SWT.LEFT);
		viewLabel1.setText(PluginProperties.wizards_ViewConfCurrentView_label);
		viewLabel1.setLayoutData(WizardHelper.createData(-1,
				GridData.BEGINNING, false, false, -1, 2));

		Label viewLabel2 = new Label(composite, SWT.LEFT);
		viewLabel2
				.setText(Views.getInstance().getSelectedView()
						.getOfficialName()
						+ " ("
						+ Views.getInstance().getSelectedView()
								.getInternalName() + ")");
		viewLabel2.setLayoutData(WizardHelper.createData(-1,
				GridData.BEGINNING, false, false, -1, 1));
		viewLabel2.setFont(FontUtil.getSystemStyle(SWT.ITALIC | SWT.BOLD));

		Label dummyLabel = new Label(composite, SWT.LEFT);
		dummyLabel.setLayoutData(WizardHelper.createData(-1,
				GridData.BEGINNING, false, false, -1, 3));
		Label dummyLabel2 = new Label(composite, SWT.LEFT);
		dummyLabel2.setLayoutData(WizardHelper.createData(-1,
				GridData.BEGINNING, false, false, -1, 3));

		// the select view label
		Label selectLabel = new Label(composite, SWT.LEFT | SWT.WRAP);
		selectLabel
				.setText(PluginProperties.wizards_ViewConfImportSelect_label);
		selectLabel.setLayoutData(WizardHelper.createData(-1,
				GridData.BEGINNING, false, false, 1, 3));

		Label dummyLabel3 = new Label(composite, SWT.LEFT);
		dummyLabel3.setLayoutData(WizardHelper.createData(-1,
				GridData.BEGINNING, false, false, -1, 3));

		// add the view radio buttons
		boolean firstViewFound = false;
		for (ViewDescriptor viewD : Views.getInstance().getAvailableViews())
		{
			// add views (but not the current one)
			if (!viewD.getInternalName().equals(
					Views.getInstance().getSelectedView().getInternalName()))
			{
				Button viewButton = new Button(composite, SWT.RADIO);
				viewButton.setLayoutData(WizardHelper.createData(-1,
						GridData.BEGINNING, false, false, -1, 1));
				viewButton.setText(viewD.getOfficialName());
				viewButton.setData(viewD.getInternalName());
				viewButton.setFont(FontUtil.getSystemStyle(SWT.BOLD));

				viewButton.addSelectionListener(new SelectionListener()
				{

					public void widgetDefaultSelected(SelectionEvent e)
					{
					}


					public void widgetSelected(SelectionEvent e)
					{
						selectedViewID = ((Button) e.widget).getData()
								.toString();
					}
				});

				// the view label
				Label buttonLabel = new Label(composite, SWT.LEFT);
				buttonLabel.setLayoutData(WizardHelper.createData(-1,
						GridData.BEGINNING, false, false, -1, 2));
				buttonLabel.setFont(FontUtil.getSystemStyle(SWT.ITALIC));
				Object score = scoreImport.get(viewD.getInternalName()) != null ? scoreImport
						.get(viewD.getInternalName())
						: 0;

				buttonLabel.setText(PluginProperties.bind(
						PluginProperties.wizards_ViewConfImportRelevant_label,
						viewD.getInternalName(), score));

				// select the first view
				if (!firstViewFound)
				{
					viewButton.setSelection(true);
					firstViewFound = true;
					selectedViewID = viewD.getInternalName();
				}
			}
		}

		Label dummyLabel4 = new Label(composite, SWT.LEFT);
		dummyLabel4.setLayoutData(WizardHelper.createData(-1,
				GridData.BEGINNING, false, false, -1, 3));

		// add the import only new data checkbox
		final Button onlyNewButton = new Button(composite, SWT.CHECK);
		onlyNewButton.setLayoutData(WizardHelper.createData(-1,
				GridData.BEGINNING, false, false, -1, 3));
		onlyNewButton
				.setText(PluginProperties.wizards_ViewConfImportOnlyNew_label);
		onlyNewButton.setSelection(true);

		onlyNewButton.addSelectionListener(new SelectionListener()
		{

			public void widgetDefaultSelected(SelectionEvent e)
			{
			}


			public void widgetSelected(SelectionEvent e)
			{
				importOnlyMissingLayoutData = onlyNewButton.getSelection();
			}
		});

		// select the first view
		importOnlyMissingLayoutData = onlyNewButton.getSelection();

		// if no view was found, make
		if (!firstViewFound)
		{
			setPageComplete(false);
		}

		// Validate
		setMessage(PluginProperties.wizards_ViewConfImport_message);
		setControl(composite);

		// refresh packageviewer display
		getShell().pack();

		// make dialog bigger and center
		WizardHelper.resizeDialog(getShell());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage#getNextPage()
	 */
	@Override
	public IWizardPage getNextPage()
	{
		return getWizard().getPage("activity"); //$NON-NLS-1$
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets
	 * .Composite )
	 */
	@Override
	public void dispose()
	{
		super.dispose();
	}

}