/**
 * File:    CreateDataWizard.java
 * Created: 19.12.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Seitz, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.wizards.model;

import java.util.Collections;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jwt.meta.model.core.CorePackage;
import org.eclipse.jwt.meta.model.data.Data;
import org.eclipse.jwt.meta.model.data.DataFactory;
import org.eclipse.jwt.meta.model.data.DataType;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.IWorkbench;


/**
 * Creates a new Data element using a wizard
 * 
 * @author Christian Seitz (chr.seitz@gmx.de), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org.
 */
public class CreateDataWizard
		extends Wizard
{

	/**
	 * The first Page of the wizard.
	 */
	protected SelectNameWizardPage initialPage;

	/**
	 * The second Page of the wizard.
	 */
	protected SelectDatatypeWizardPage datatypePage;

	/**
	 * The default package.
	 */
	protected org.eclipse.jwt.meta.model.core.Package defaultPackage = null;


	/**
	 * Creates a new wizard.
	 */
	public CreateDataWizard()
	{
		super();
		setWindowTitle(PluginProperties.wizards_DataWizard_title);
	}


	/**
	 * Creates a new wizard.
	 */
	public CreateDataWizard(org.eclipse.jwt.meta.model.core.Package defaultPackage)
	{
		this();
		this.defaultPackage = defaultPackage;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
	 * org.eclipse.jface.viewers.IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection)
	{
		initialPage = new SelectNameWizardPage(
				org.eclipse.jwt.meta.PluginProperties.model_Data_type, defaultPackage,
				workbench);
		datatypePage = new SelectDatatypeWizardPage(
				org.eclipse.jwt.meta.PluginProperties.model_DataType_type, workbench);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages()
	{
		addPage(initialPage);
		initialPage.setDescription(PluginProperties.wizards_Page_name_package);
		addPage(datatypePage);
		datatypePage.setDescription(PluginProperties.wizards_Page_datatype);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish()
	{
		String name = initialPage.getName();
		org.eclipse.jwt.meta.model.core.Package selectedPackage = initialPage
				.getPackage();
		String icon = initialPage.getIconPath();
		DataType datatype = datatypePage.getDataType();
		String value = datatypePage.getValue();

		DataFactory factory = DataFactory.eINSTANCE;
		Data data = factory.createData();
		data.setName(name);
		data.setIcon(icon);
		data.setDataType(datatype);
		data.setValue(value);

		// This command creates the new Data
		CommandParameter childDescriptor = new CommandParameter(null,
				CorePackage.Literals.PACKAGE__ELEMENTS, data);
		Command cmd = CreateChildCommand.create(getEditingDomain(), selectedPackage,
				childDescriptor, Collections.singleton(selectedPackage));
		getEditingDomain().getCommandStack().execute(cmd);

		return true;
	}


	/**
	 * @return The EMF EditingDomain.
	 */
	public EditingDomain getEditingDomain()
	{
		return (EditingDomain) GeneralHelper.getActiveInstance().getAdapter(
				EditingDomain.class);
	}

}
