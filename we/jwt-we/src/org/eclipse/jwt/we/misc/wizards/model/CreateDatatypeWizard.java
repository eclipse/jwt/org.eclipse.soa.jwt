/**
 * File:    CreateDatatypeWizard.java
 * Created: 04.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Seitz, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.wizards.model;

import java.util.Collections;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jwt.meta.model.core.CorePackage;
import org.eclipse.jwt.meta.model.core.Package;
import org.eclipse.jwt.meta.model.data.DataFactory;
import org.eclipse.jwt.meta.model.data.DataType;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.IWorkbench;


/**
 * Wizard to create a new datatype
 * 
 * @version $Id: CreateDatatypeWizard.java,v 1.7 2009-11-26 12:41:02 chsaad Exp $
 * @author Christian Seitz (chr.seitz@gmx.de), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org.
 */
public class CreateDatatypeWizard
		extends Wizard
{

	/**
	 * The first Page of the wizard
	 */
	protected SelectNameWizardPage initialPage;


	/**
	 * Creates a new wizard
	 */
	public CreateDatatypeWizard()
	{
		super();
		setWindowTitle(PluginProperties.wizards_DatatypeWizard_title);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish()
	{
		String name = initialPage.getName();
		Package selectedPackage = initialPage.getPackage();
		String icon = initialPage.getIconPath();
		DataFactory factory = DataFactory.eINSTANCE;
		DataType datatype = factory.createDataType();
		datatype.setName(name);
		datatype.setPackage(selectedPackage);
		datatype.setIcon(icon);

		// This command creates the new Data
		CommandParameter childDescriptor = new CommandParameter(null,
				CorePackage.Literals.PACKAGE__ELEMENTS, datatype);
		Command cmd = CreateChildCommand.create(getEditingDomain(), selectedPackage,
				childDescriptor, Collections.singleton(selectedPackage));
		getEditingDomain().getCommandStack().execute(cmd);

		return true;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
	 * org.eclipse.jface.viewers.IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection)
	{
		initialPage = new SelectNameWizardPage(
				org.eclipse.jwt.meta.PluginProperties.model_DataType_type, workbench);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages()
	{
		addPage(initialPage);
		initialPage.setDescription(PluginProperties.wizards_Page_name_package);
	}


	/**
	 * @return The EMF EditingDomain.
	 */
	public EditingDomain getEditingDomain()
	{
		return (EditingDomain) GeneralHelper.getActiveInstance().getAdapter(
				EditingDomain.class);
	}

}
