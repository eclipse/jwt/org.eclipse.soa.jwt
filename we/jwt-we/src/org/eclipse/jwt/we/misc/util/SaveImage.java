/**
 * File:    SaveImage.java
 * Created: 25.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Seitz, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Yoann Rodiere, Open Wide, Lyon, France
 *    	- added SVG support + javadoc
 *******************************************************************************/

package org.eclipse.jwt.we.misc.util;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import org.apache.batik.svggen.SVGGraphics2DIOException;
import org.eclipse.draw2d.FreeformFigure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.SWTGraphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editparts.LayerManager;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.jwt.we.misc.util.internal.gmf.runtime.draw2d.ui.render.awt.internal.svg.export.GraphicsSVG;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Control;

/**
 * Provides static methods to save a picture of a model element to disc.
 * 
 * @version $Id: SaveImage.java,v 1.8 2009-11-26 12:41:18 chsaad Exp $
 * @author Christian Seitz (chr.seitz@gmx.de), Programming distributed Systems
 *         Lab, University of Augsburg, Germany, www.ds-lab.org.
 * @since 0.6.0
 * @noextend This class is not intended to be subclassed by clients.
 */
public class SaveImage {

	/**
	 * Saves a model view to a PNG, BMP or JPEG file using SWT capabilities.
	 * <p>
	 * Example:<br>
	 * <code>
	  {@link WEEditor} editor = ...;<br>
	  GraphicalViewer viewer = editor.getCurrentActivitySheet().getGraphicalViewer();<br>
	  SaveImage.save(viewer, "/my/path/my_file.jpeg", SWT.IMAGE_JPEG);<br>
	  SaveImage.save(viewer, "/my/path/my_file.png", SWT.IMAGE_PNG);<br>
	  SaveImage.save(viewer, "/my/path/my_file.bmp", SWT.IMAGE_BMP);
	 * </code>
	 * 
	 * @param viewer
	 *            The {@link GraphicalViewer} describing what will be exported.
	 * @param saveFilePath
	 *            The path of the file to write to.
	 * @param format
	 *            The format of the exported file. One of {@link SWT.IMAGE_BPM},
	 *            {@link SWT.IMAGE_PNG} or {@link SWT.IMAGE_JPEG}.
	 * @return True if the image was exported to {@code saveFilePath}, false
	 *         otherwise.
	 */
	public static boolean save(GraphicalViewer viewer, String saveFilePath,
			int format) {
		if (format != SWT.IMAGE_BMP && format != SWT.IMAGE_JPEG
				&& format != SWT.IMAGE_PNG)
			throw new IllegalArgumentException("Save format not supported"); //$NON-NLS-1$

		try {
			saveEditorContentsAsImage(viewer, saveFilePath, format);
		} catch (Exception ex) {
			return false;
		}

		return true;
	}

	/**
	 * Saves a model view to an SVG file.
	 * <p>
	 * Example:<br/>
	 * <code>
	  {@link WEEditor} editor = ...;<br>
	  GraphicalViewer viewer = editor.getCurrentActivitySheet().getGraphicalViewer();<br>
	  SaveImage.saveSVG(viewer, "/my/path/my_file.svg");
	 * </code>
	 * 
	 * @param viewer
	 *            The {@link GraphicalViewer} describing what will be exported.
	 * @param saveFilePath
	 *            The path of the file to write to.
	 * @return True if the image was exported to {@code saveFilePath}, false
	 *         otherwise.
	 * 
	 * @since 1.2
	 */
	public static boolean saveSVG(GraphicalViewer viewer, String saveFilePath) {
		try {
			saveEditorContentsAsSVG(viewer, saveFilePath);
		} catch (Exception ex) {
			return false;
		}

		return true;
	}

	private static void saveEditorContentsAsImage(GraphicalViewer viewer,
			String saveFilePath, int format) {
		FreeformFigure rootFigure = getRootFigure(viewer);
		Rectangle rootFigureBounds = rootFigure.getFreeformExtent();
		Control figureCanvas = viewer.getControl();
		GC figureCanvasGC = new GC(figureCanvas);

		// create Graphics onto which the figure can be painted
		Image img = new Image(null, rootFigureBounds.width,
				rootFigureBounds.height);
		GC imageGC = new GC(img);
		imageGC.setBackground(figureCanvasGC.getBackground());
		imageGC.setForeground(figureCanvasGC.getForeground());
		imageGC.setFont(figureCanvasGC.getFont());
		imageGC.setLineStyle(figureCanvasGC.getLineStyle());
		imageGC.setLineWidth(figureCanvasGC.getLineWidth());
		// deprecated: imageGC.setXORMode(figureCanvasGC.getXORMode());
		Graphics imgGraphics = new SWTGraphics(imageGC);
		// this line is important to for parts of the FreeformFigure that have
		// negative
		// coordinates
		imgGraphics.translate(rootFigureBounds.getLocation().negate());
		// Draw rootFigure onto image
		rootFigure.paint(imgGraphics);

		// Save image
		ImageData[] imgData = new ImageData[1];
		imgData[0] = img.getImageData();

		ImageLoader imgLoader = new ImageLoader();
		imgLoader.data = imgData;
		imgLoader.save(saveFilePath, format);

		// release resources */
		figureCanvasGC.dispose();
		imageGC.dispose();
		img.dispose();
	}

	private static void saveEditorContentsAsSVG(GraphicalViewer viewer,
			String saveFilePath) throws SVGGraphics2DIOException, FileNotFoundException {
		FreeformFigure rootFigure = getRootFigure(viewer);
		Rectangle rootFigureBounds = rootFigure.getFreeformExtent();

		GraphicsSVG graphics = GraphicsSVG.getInstance(rootFigureBounds
				.getTranslated(rootFigureBounds.getLocation().negate()));
		graphics.translate(rootFigureBounds.getLocation().negate());
		rootFigure.paint(graphics);

		graphics.getSVGGraphics2D().stream(
				new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
						saveFilePath))));
	}

	private static FreeformFigure getRootFigure(GraphicalViewer viewer) {
		ScalableFreeformRootEditPart rootEditPart = (ScalableFreeformRootEditPart) viewer
				.getRootEditPart();
		return (FreeformFigure) ((LayerManager) rootEditPart)
				.getLayer(LayerConstants.PRINTABLE_LAYERS);
	}
}
