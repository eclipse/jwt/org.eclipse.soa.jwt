/**
 * File:    Serialization.java
 * Created: 30.03.2005
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.util.internal;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.eclipse.jwt.we.misc.logging.Logger;
import org.jdom.Element;


/**
 * Class for De-/Serialization of objects.
 * 
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class Serialization
{

	/**
	 * Deserializes a bytestream and creates an object out of it
	 * 
	 * @param stream
	 *            Bytestream
	 * @param offset
	 *            Offset of the object
	 * @param length
	 *            Length of serialization object
	 * @return Object
	 */
	public static Object deserializebytestream(byte[] stream, int offset, int length)
	{
		ByteArrayInputStream myinput = null;
		ObjectInputStream myinputstream = null;
		Object result = null;
		try
		{
			myinput = new ByteArrayInputStream(stream, offset, length);
			myinputstream = new ObjectInputStream(myinput);
			result = myinputstream.readObject();
		}
		catch (IOException ex)
		{
			Logger.getLogger(Serialization.class).warning("Deserialize: " + ex); //$NON-NLS-1$
		}
		catch (ClassNotFoundException ex)
		{
			Logger.getLogger(Serialization.class).warning("Deserialize: " + ex); //$NON-NLS-1$
		}
		finally
		{
			try
			{
				if (myinput != null)
					myinput.close();
				if (myinputstream != null)
					myinputstream.close();
			}
			catch (IOException ex)
			{

			}
		}
		return result;
	}


	/**
	 * Creates a byte stream out of an object
	 * 
	 * @param obj
	 *            The serialisable object
	 * @return Bytearray
	 */
	public static byte[] serializeobject(Object obj) throws IOException
	{
		byte[] result = null;
		ByteArrayOutputStream mybytestream = null;
		ObjectOutputStream mystream = null;
		try
		{
			mybytestream = new ByteArrayOutputStream();
			mystream = new ObjectOutputStream(mybytestream);
			mystream.writeObject(obj);
			result = mybytestream.toByteArray();
		}
		catch (IOException ex)
		{
			throw ex;
		}
		finally
		{
			try
			{
				if (mystream != null)
					mystream.close();
				if (mybytestream != null)
					mybytestream.close();
			}
			catch (IOException ex)
			{

			}
		}

		return result;
	}


	/**
	 * Serializes an object, converts the bytes to a string and puts all of this in a jdom
	 * element, which is added to another element
	 * 
	 * @param obj
	 *            The object to be serialized
	 * @param elem
	 *            where the new element should be added to
	 * @param name
	 *            The name of the new element
	 * @throws Exception
	 */
	public static void writeObjecttoXML(Object obj, Element elem, String name)
			throws Exception
	{
		Element e = new Element(name);
		if (obj != null)
			e.addContent(ByteStringConv.createString(Serialization.serializeobject(obj)));
		elem.addContent(e);
	}


	/**
	 * Creates an object out of an element text.
	 * 
	 * @param elem
	 *            The element whose text should be converted
	 * @return An object
	 */
	public static Object createObject(Element elem)
	{
		if (elem.getText().equals("")) //$NON-NLS-1$
			return null;
		byte[] data = ByteStringConv.createByteArray(elem.getText());
		return Serialization.deserializebytestream(data, 0, data.length);
	}
}
