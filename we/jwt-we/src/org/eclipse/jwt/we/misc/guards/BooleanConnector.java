/**
 * File:    BooleanConnector.java
 * Created: 20.09.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.guards;

/**
 * 
 * Simple Class for holding an {@link BooleanConnector}
 * 
 * @version $Id: BooleanConnector.java,v 1.4 2009-11-26 12:41:01 chsaad Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class BooleanConnector
{

	private BooleanParserState operator = BooleanParserState.None;


	public BooleanConnector(BooleanParserState op)
	{
		operator = op;
	}


	public BooleanParserState getOperator()
	{
		return operator;
	}
}
