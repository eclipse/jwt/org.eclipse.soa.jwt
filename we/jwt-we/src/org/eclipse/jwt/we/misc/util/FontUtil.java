/**
 * File:    FontUtil.java
 * Created: 27.10.2007
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.util;

import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;


/**
 * Static methods for font handling.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org.
 * @since 0.6.0
 */
public class FontUtil
{

	/**
	 * Returns the system default font.
	 * 
	 * @return System font.
	 */
	public static Font getSystemDefault()
	{
		Display current = Display.getCurrent();
		if (current == null)
		{
			Shell shell = new Shell();
			FontData fontData = shell.getFont().getFontData()[0];
			shell.dispose();

			Font font = new Font(null, fontData);

			return font;
		}

		Font font = new Font(null, current.getSystemFont().getFontData()[0]);

		return font;
	}


	/**
	 * Returns the system default font with an additional style.
	 * 
	 * @return System font.
	 */
	public static Font getSystemStyle(int style)
	{
		Display current = Display.getCurrent();
		if (current == null)
		{
			Shell shell = new Shell();
			FontData fontData = shell.getFont().getFontData()[0];
			shell.dispose();

			fontData.setStyle(fontData.getStyle() | style);

			Font font = new Font(null, fontData);

			return font;
		}

		FontData fontData = current.getSystemFont().getFontData()[0];

		fontData.setStyle(fontData.getStyle() | style);

		Font font = new Font(null, fontData);

		return font;
	}
	
	
	/**
	 * Returns the font with a new size.
	 * 
	 * @param newSize
	 * @return The new font.
	 */
	public static Font getResizedFont(Font font, int newSize)
	{
		FontData fontdata = font.getFontData()[0];
		fontdata.setHeight(newSize);

		return new Font(font.getDevice(), fontdata);
	}


	/**
	 * Converts a font into a string. (Format: Name,Height,Style)
	 * 
	 * @param font
	 * @return Font string.
	 */
	public static String serializeFont(Font font)
	{
		FontData fontData;
		String fontDataString;

		fontData = font.getFontData()[0];
		fontDataString = fontData.getName() + "," + fontData.getHeight() + ","
				+ fontData.getStyle();

		return fontDataString;
	}


	/**
	 * Constructs a font from a string. (Format: Name,Height,Style)
	 * 
	 * @param fontDataString
	 * @return Font.
	 */
	public static Font deSerializeFont(String fontDataString)
	{
		String[] splittedFontDataString;
		FontData fontData;

		splittedFontDataString = fontDataString.split(",");
		fontData = new FontData(splittedFontDataString[0], new Integer(
				splittedFontDataString[1]), new Integer(splittedFontDataString[2]));

		return new Font(null, fontData);
	}

}