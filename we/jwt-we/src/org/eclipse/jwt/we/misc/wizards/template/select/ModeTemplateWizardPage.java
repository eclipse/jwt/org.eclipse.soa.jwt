/**
 * File:    ModeTemplateWizardPage.java
 * Created: 08.12.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.wizards.template.select;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.misc.wizards.WizardHelper;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;


/**
 * This is a wizard page where parts of a model can be selected for export.
 * 
 * @version $Id: ModeTemplateWizardPage.java,v 1.5 2009-11-26 12:41:49 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class ModeTemplateWizardPage
		extends WizardPage
{

	/**
	 * A Logger.
	 */
	static final Logger logger = Logger.getLogger(ModeTemplateWizardPage.class);

	/**
	 * The radio button for import.
	 */
	private Button importTemplates;

	/**
	 * The radio button for export.
	 */
	private Button exportTemplates;


	/**
	 * Pass in the selection.
	 * 
	 * @param pageId
	 *            The name of the page.
	 * @param workbench
	 *            The workbench.
	 */
	public ModeTemplateWizardPage(String pageId, IWorkbench workbench)
	{
		super(pageId);
	}


	/**
	 * @return The EMF EditingDomain.
	 */
	public EditingDomain getEditingDomain()
	{
		return (EditingDomain) GeneralHelper.getActiveInstance().getAdapter(
				EditingDomain.class);
	}


	/**
	 * @return The {@link AdapterFactory} from an {@link AdapterFactoryEditingDomain} if
	 *         available.
	 */
	public AdapterFactory getAdapterFactory()
	{
		return (AdapterFactory) GeneralHelper.getActiveInstance().getAdapter(
				AdapterFactory.class);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite
	 * )
	 */
	public void createControl(Composite parent)
	{
		// Composite
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));
		composite.setLayoutData(WizardHelper.createData(GridData.FILL, GridData.FILL,
				true, false, -1, -1));

		// import
		importTemplates = new Button(composite, SWT.RADIO);
		importTemplates.setText(PluginProperties.wizards_ImportTemplateWizard_title);
		importTemplates.setLayoutData(WizardHelper.createData(-1, GridData.BEGINNING,
				false, false, -1, 1));
		
		// export
		exportTemplates = new Button(composite, SWT.RADIO);
		exportTemplates.setText(PluginProperties.wizards_ExportTemplateWizard_title);
		exportTemplates.setLayoutData(WizardHelper.createData(-1, GridData.BEGINNING,
				false, false, -1, 1));
		
		// initialize with import mode
		importTemplates.setSelection(true);

		// Validate
		setMessage(null);
		setControl(composite);

		// refresh packageviewer display
		getShell().pack();

		// make dialog bigger and center
		WizardHelper.resizeDialog(getShell());
	}


	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.WizardPage#getNextPage()
	 */
	@Override
	public IWizardPage getNextPage()
	{
		if (importTemplates.getSelection())
		{
			return getWizard().getPage("import"); //$NON-NLS-1$
		}
		else
		{
			return getWizard().getPage("export"); //$NON-NLS-1$
		}
	}
	

	/**
	 * Import mode activated?
	 * 
	 * @return mode.
	 */
	public boolean isImportMode()
	{
		return importTemplates.getSelection();
	}

		
	/**
	 * Import mode activated?
	 * 
	 * @return mode.
	 */
	public boolean isExportMode()
	{
		return exportTemplates.getSelection();
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite
	 * )
	 */
	@Override
	public void dispose()
	{
		// dispose of everything
		importTemplates.dispose();
		exportTemplates.dispose();

		super.dispose();
	}


}