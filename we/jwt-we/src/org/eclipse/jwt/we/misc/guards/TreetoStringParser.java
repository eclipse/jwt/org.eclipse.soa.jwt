/**
 * File:    TreetoStringParser.java
 * Created: 20.09.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.guards;

import org.eclipse.jwt.meta.model.processes.GuardSpecification;


/**
 * This class is the inverse of the StringtoTreeParser. Out of a GuardSpecification it
 * creates a string.
 * 
 * @version $Id: TreetoStringParser.java,v 1.6 2009-11-26 12:41:01 chsaad Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class TreetoStringParser
{

	/**
	 * Creates a String out of a {@link GuardSpecification}
	 * 
	 * @param gs
	 * @return
	 */
	public static String getString(GuardSpecification gs)
	{
		StringBuilder strb = new StringBuilder();
		if (gs.getSubSpecification().size() == 0)
			return getStringfromGuardSpec(gs);
		for (int i = 0; i < gs.getSubSpecification().size(); i++)
		{
			GuardSpecification guardspec = (GuardSpecification) gs.getSubSpecification()
					.get(i);
			if (guardspec.getSubSpecification().size() > 0)
			{
				String text = getString(guardspec);
				text = optimiseString(text);
				if (i > 0)
					strb.append(" ").append(guardspec.getSubSpecificationConnector()) //$NON-NLS-1$
							.append(" "); //$NON-NLS-1$
				text = "(" + text + ")"; //$NON-NLS-1$ //$NON-NLS-2$
				text = optimiseString(text);
				strb.append(text);
			}
			else
			{
				if (i > 0)
					strb.append(" ").append(gs.getSubSpecificationConnector()) //$NON-NLS-1$
							.append(" "); //$NON-NLS-1$
				strb.append(getStringfromGuardSpec(guardspec));
			}
		}
		return strb.toString();
	}


	/**
	 * Removes "((" braces from the term. This is not "necessary" in the usual term (the
	 * term would also be correct with more braces)
	 * 
	 * @param term
	 * @return
	 */
	private static String optimiseString(String term)
	{
		if (term.indexOf("((") == 0) //$NON-NLS-1$
		{
			if (term.lastIndexOf("))") == term.length() - 2) //$NON-NLS-1$
			{
				term = term.substring(1);
				term = term.substring(0, term.length() - 1);
			}
		}
		return term;
	}


	/**
	 * Transforms a GuardSpecification into a string
	 * 
	 * @param gs
	 * @return
	 */
	private static String getStringfromGuardSpec(GuardSpecification gs)
	{
		StringBuilder strb = new StringBuilder();
		if (gs.getSubSpecification().size() == 0)
		{
			strb.append((gs.getData() != null) ? (gs.getData().getName() + ".") : ""); //$NON-NLS-1$ //$NON-NLS-2$
			strb.append((gs.getAttribute() != null) ? gs.getAttribute() : ""); //$NON-NLS-1$
			strb.append(" "); //$NON-NLS-1$
			strb.append(gs.getOperation().getLiteral());
			strb.append(" "); //$NON-NLS-1$
			strb.append(gs.getValue());
		}
		else
		{
			strb.append(gs.getSubSpecificationConnector().getLiteral());
		}
		return strb.toString();
	}
}
