/**
 * File:    SelectNameWizardPage.java
 * Created: 14.12.2006
 *
 *
 /*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Seitz, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.wizards;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;


/**
 * Filter to display only Packages in a TreeViewer
 * 
 * @author Christian Seitz (chr.seitz@gmx.de), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org.
 */
public class PackageFilter
		extends ViewerFilter
{

	/**
	 * Returns whether the given element makes it through this filter.
	 * 
	 * @param viewer
	 *            the viewer
	 * @param parentElement
	 *            the parent element
	 * @param element
	 *            the element
	 * @return <code>true</code> if element is included in the filtered set, and
	 *         <code>false</code> if excluded
	 */
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element)
	{
		if (element instanceof org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl)
			return true;
		if (element instanceof org.eclipse.jwt.meta.model.core.Package)
			return true;

		return false;

	}

}
