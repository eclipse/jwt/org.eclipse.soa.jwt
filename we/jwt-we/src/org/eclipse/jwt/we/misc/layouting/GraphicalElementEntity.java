/**
 * File:    GraphicalElementEntity.java
 * Created: 12.01.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.layouting;

import org.eclipse.jwt.meta.model.core.GraphicalElement;
import org.eclipse.jwt.meta.model.processes.FinalNode;
import org.eclipse.jwt.meta.model.processes.InitialNode;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.views.LayoutDataManager;
import org.eclipse.zest.layouts.LayoutEntity;
import org.eclipse.zest.layouts.constraints.BasicEntityConstraint;
import org.eclipse.zest.layouts.constraints.LayoutConstraint;

/**
 * A simple wrapper class for JWT nodes, used by the Zest project's layouter.
 * 
 * @version $Id: GraphicalElementEntity.java,v 1.2 2009/11/04 17:18:56 chsaad
 *          Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of
 *         Augsburg, Germany, www.ds-lab.org
 */
public class GraphicalElementEntity implements LayoutEntity, Comparable
{

	/**
	 * The workflow editor.
	 */
	private WEEditor weeditor;

	/**
	 * The graphical element this wrapper represents.
	 */
	private GraphicalElement graphicalElement;

	/**
	 * The internal node.
	 */
	private Object internalNode;


	/**
	 * The constructor.
	 * 
	 * @param weeditor
	 */
	public GraphicalElementEntity(WEEditor weeditor)
	{
		super();
		this.weeditor = weeditor;
	}


	/**
	 * @return Returns the graphicalElement.
	 */
	public GraphicalElement getGraphicalElement()
	{
		return graphicalElement;
	}


	/**
	 * @param graphicalElement
	 *            The graphicalElement to set.
	 */
	public void setGraphicalElement(GraphicalElement graphicalElement)
	{
		this.graphicalElement = graphicalElement;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.zest.layouts.LayoutEntity#getHeightInLayout()
	 */
	public double getHeightInLayout()
	{
		double height = LayoutDataManager.getHeight(weeditor, graphicalElement);
		height = height == 10 ? 30 : height;

		return height;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.zest.layouts.LayoutEntity#getWidthInLayout()
	 */
	public double getWidthInLayout()
	{
		double width = LayoutDataManager.getWidth(weeditor, graphicalElement);
		width = width == 10 ? 50 : width;

		return width;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.zest.layouts.LayoutEntity#getXInLayout()
	 */
	public double getXInLayout()
	{
		return LayoutDataManager.getX(weeditor, graphicalElement);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.zest.layouts.LayoutEntity#getYInLayout()
	 */
	public double getYInLayout()
	{
		return LayoutDataManager.getY(weeditor, graphicalElement);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.zest.layouts.LayoutEntity#populateLayoutConstraint(org.eclipse
	 * .zest .layouts.constraints.LayoutConstraint)
	 */
	public void populateLayoutConstraint(LayoutConstraint constraint)
	{
		((BasicEntityConstraint) constraint).hasPreferredSize = true;
		((BasicEntityConstraint) constraint).preferredWidth = getWidthInLayout();
		((BasicEntityConstraint) constraint).preferredHeight = getHeightInLayout();
		
		// initial node: top/left
		if (graphicalElement instanceof InitialNode)
		{
			((BasicEntityConstraint) constraint).hasPreferredLocation = true;
			((BasicEntityConstraint) constraint).preferredX = 10;
			((BasicEntityConstraint) constraint).preferredY = 10;
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.zest.layouts.LayoutEntity#setLocationInLayout(double,
	 * double)
	 */
	public void setLocationInLayout(double x, double y)
	{
		LayoutDataManager.setX(weeditor, graphicalElement, (int) Math.round(x));
		LayoutDataManager.setY(weeditor, graphicalElement, (int) Math.round(y));
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.zest.layouts.LayoutEntity#setSizeInLayout(double,
	 * double)
	 */
	public void setSizeInLayout(double width, double height)
	{
		/*
		 * LayoutDataManager.setWidth(weeditor, graphicalElement, (int) Math
		 * .round(width)); LayoutDataManager.setHeight(weeditor,
		 * graphicalElement, (int) Math .round(height));
		 */
	}


	public int compareTo(Object o)
	{
		if (o instanceof InitialNode)
		{
			return -1;
		}
		else if (o instanceof FinalNode)
		{
			return 1;
		}

		return 0;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uvic.cs.chisel.layouts.LayoutEntity#getInternalEntity()
	 */
	public Object getLayoutInformation()
	{
		return internalNode;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.uvic.cs.chisel.layouts.LayoutEntity#setInternalEntity(java.lang.Object
	 * )
	 */
	public void setLayoutInformation(Object internalEntity)
	{
		this.internalNode = internalEntity;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.zest.layouts.LayoutItem#getGraphData()
	 */
	public Object getGraphData()
	{
		return null;
	}


	public void setGraphData(Object o)
	{
	}

}
