/**
 * File:    LayoutDataManager.java
 * Created: 28.12.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.misc.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jwt.meta.model.core.Comment;
import org.eclipse.jwt.meta.model.core.GraphicalElement;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.commands.view.CreateLayoutDataCommand;
import org.eclipse.jwt.we.commands.view.ImportLayoutDataCommand;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.misc.util.EMFHelper;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.misc.wizards.view.ViewConfWizard;
import org.eclipse.jwt.we.model.view.LayoutData;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.model.view.impl.ViewFactoryImpl;
import org.eclipse.ui.PlatformUI;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;

/**
 * This class contains the routines for handling the multi-view layout data of
 * graphical elements. LayoutData should only be modified using methods from
 * this class!
 * 
 * @version $Id: LayoutDataManager.java,v 1.9 2010-06-29 10:52:30 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of
 *         Augsburg, Germany, www.ds-lab.org
 * @since 0.7.0
 */
public class LayoutDataManager
{

	/**
	 * LayoutData Handling: Show dialog.
	 */
	public static String VIEW_LAYOUTDATA_DIALOG = "view_layoutdata_dialog"; //$NON-NLS-1$

	/**
	 * LayoutData Handling: Show wizard.
	 */
	public static String VIEW_LAYOUTDATA_WIZARD = "view_layoutdata_wizard"; //$NON-NLS-1$

	/**
	 * LayoutData Handling: Automatic layout.
	 */
	public static String VIEW_LAYOUTDATA_LAYOUT = "view_layoutdata_layout"; //$NON-NLS-1$

	/**
	 * LayoutData Handling: Automatic import.
	 */
	public static String VIEW_LAYOUTDATA_IMPORT = "view_layoutdata_import"; //$NON-NLS-1$

	/**
	 * LayoutData Handling: Do not check.
	 */
	public static String VIEW_LAYOUTDATA_NOTHING = "view_layoutdata_nothing"; //$NON-NLS-1$


	/**
	 * Set the x value of the layoutdata for the given element.
	 * 
	 * @param graphicalelement
	 * @param x
	 */
	public static void setX(WEEditor weeditor, GraphicalElement graphicalelement, int x)
	{
		getLayoutData(weeditor, graphicalelement).setX(x);
		getLayoutData(weeditor, graphicalelement).setInitialized(true);
	}


	/**
	 * Set the y value of the layoutdata for the given element.
	 * 
	 * @param graphicalelement
	 * @param y
	 */
	public static void setY(WEEditor weeditor, GraphicalElement graphicalelement, int y)
	{
		getLayoutData(weeditor, graphicalelement).setY(y);
		getLayoutData(weeditor, graphicalelement).setInitialized(true);
	}


	/**
	 * Set the width value of the layoutdata for the given element.
	 * 
	 * @param graphicalelement
	 * @param width
	 */
	public static void setWidth(WEEditor weeditor, GraphicalElement graphicalelement,
			int width)
	{
		getLayoutData(weeditor, graphicalelement).setWidth(width);
		getLayoutData(weeditor, graphicalelement).setInitialized(true);
	}


	/**
	 * Set the height value of the layoutdata for the given element.
	 * 
	 * @param graphicalelement
	 * @param height
	 */
	public static void setHeight(WEEditor weeditor, GraphicalElement graphicalelement,
			int height)
	{
		getLayoutData(weeditor, graphicalelement).setHeight(height);
		getLayoutData(weeditor, graphicalelement).setInitialized(true);
	}


	/**
	 * Return the x value for the given element.
	 * 
	 * @param graphicalelement
	 * @return x
	 */
	public static int getX(WEEditor weeditor, GraphicalElement graphicalelement)
	{
		return getLayoutData(weeditor, graphicalelement).getX();
	}


	/**
	 * Return the y value for the given element.
	 * 
	 * @param graphicalelement
	 * @return y
	 */
	public static int getY(WEEditor weeditor, GraphicalElement graphicalelement)
	{
		return getLayoutData(weeditor, graphicalelement).getY();
	}


	/**
	 * Return the width value for the given element.
	 * 
	 * @param graphicalelement
	 * @return width
	 */
	public static int getWidth(WEEditor weeditor, GraphicalElement graphicalelement)
	{
		return getLayoutData(weeditor, graphicalelement).getWidth();
	}


	/**
	 * Return the height value for the given element.
	 * 
	 * @param graphicalelement
	 * @return height
	 */
	public static int getHeight(WEEditor weeditor, GraphicalElement graphicalelement)
	{
		return getLayoutData(weeditor, graphicalelement).getHeight();
	}


	/**
	 * Sets the layoutdata for the current view to (a copy) of the given values.
	 * 
	 * @param graphicalelement
	 * @param layoutData
	 */
	public static void setLayoutData(WEEditor weeditor,
			GraphicalElement graphicalelement, LayoutData layoutData)
	{
		// search layoutdata for this viewid
		LayoutData layoutDataPresent = findLayoutDataForView(weeditor, graphicalelement,
				layoutData.getViewid());

		transferLayoutDataValues(layoutData, layoutDataPresent);
	}


	/**
	 * Returns the layoutdata for the current view.
	 * 
	 * @param graphicalelement
	 * @return The layoutdata for the current view.
	 */
	public static LayoutData getLayoutData(WEEditor weeditor,
			GraphicalElement graphicalelement)
	{
		LayoutData layoutData = getLayoutDataForView(weeditor, graphicalelement, Views
				.getInstance().getSelectedView().getInternalName());

		// make sure the layoutdata has an active itemlabelprovider
		weeditor.getAdapterFactory().adapt(layoutData, IItemLabelProvider.class);

		return layoutData;
	}


	/**
	 * Returns all contained layoutdatas recursively.
	 * 
	 * @param collection
	 * @return The contained layoutdatas.
	 */
	public static Collection getAllContainedLayoutDatas(WEEditor weeditor,
			Collection collection)
	{
		Collection result = new ArrayList<LayoutData>();
		for (Object object : collection)
		{
			if (object instanceof GraphicalElement)
			{
				result.addAll(getLayoutDataForGraphicalElement(weeditor,
						(GraphicalElement) object));
			}
			if (object instanceof EObject && ((EObject) object).eContents() != null)
			{
				result.addAll(getAllContainedLayoutDatas(weeditor, ((EObject) object)
						.eContents()));
			}
		}

		return result;
	}


	/**
	 * Returns the layoutdata for the given view.
	 * 
	 * @param graphicalelement
	 * @param viewID
	 * @return The layoutdata for the given view.
	 */
	public static LayoutData getLayoutDataForView(WEEditor weeditor,
			GraphicalElement graphicalelement, String viewID)
	{
		// search if layoutdata for this viewid already exists
		LayoutData layoutDataPresent = findLayoutDataForView(weeditor, graphicalelement,
				viewID);

		// return layoutdata if found
		if (layoutDataPresent != null)
		{
			return layoutDataPresent;
		}

		// else create new layout data
		LayoutData newLayoutData = ViewFactoryImpl.eINSTANCE.createLayoutData();

		newLayoutData.setDescribesElement(graphicalelement);
		newLayoutData.setViewid(viewID);
		EMFHelper.getDiagram(weeditor).getLayoutData().add(newLayoutData);

		return newLayoutData;
	}


	/**
	 * Searches for the layoutdata for a given element and a given view. Returns
	 * null if not found
	 * 
	 * @param graphicalelement
	 * @param viewID
	 * @return The layoutdata or null.
	 */
	private static LayoutData findLayoutDataForView(WEEditor weeditor,
			GraphicalElement graphicalelement, String viewID)
	{
		if (graphicalelement == null)
		{
			return null;
		}

		// search if layoutdata for this viewid already exists
		viewID = viewID.toLowerCase();
		for (Object layoutData : getLayoutDataForGraphicalElement(weeditor,
				graphicalelement))
		{
			String viewIDld = ((LayoutData) layoutData).getViewid().toLowerCase();
			if (viewIDld.equals(viewID))
			{
				return (LayoutData) layoutData;
			}
		}

		return null;
	}


	/**
	 * The layoutData for a graphicalElement.
	 * 
	 * @param graphicalElement
	 * @return layoutData for a graphicalElement.
	 */
	public static EList<LayoutData> getLayoutDataForGraphicalElement(WEEditor weeditor,
			GraphicalElement graphicalElement)
	{
		EList layoutDatas = new BasicEList<LayoutData>();

		for (Object layoutData : EMFHelper.getDiagram(weeditor).getLayoutData())
		{
			if (((LayoutData) layoutData).getDescribesElement() != null
					&& ((LayoutData) layoutData).getDescribesElement().equals(
							graphicalElement))
			{
				layoutDatas.add(((LayoutData) layoutData));
			}
		}

		return layoutDatas;
	}


	/**
	 * Import layoutdata from a source view to a target view.
	 * 
	 * @param src_viewID
	 *            The source view ID.
	 * @param dest_viewID
	 *            The target view ID.
	 * @param onlyNew
	 *            Import only new layoutdata.
	 */
	public static void importLayoutData(WEEditor weeditor, Set<Scope> selectedScopes,
			String src_viewID, String dest_viewID, boolean onlyNew)
	{
		// do not import from the same view
		if (src_viewID.equals(dest_viewID))
		{
			return;
		}

		// execute the command on the command stack
		weeditor.getEditDomain().getCommandStack().execute(
				new ImportLayoutDataCommand(weeditor, selectedScopes, src_viewID,
						dest_viewID, onlyNew));
	}


	/**
	 * Import layoutdata from a source view to a target view.
	 * 
	 * @param src_viewID
	 *            The source view ID.
	 * @param dest_viewID
	 *            The target view ID.
	 * @param onlyNew
	 *            Import only new layoutdata.
	 */
	public static void createLayoutData(WEEditor weeditor, Set<Scope> selectedScopes,
			Class layoutClass)
	{
		// execute the command on the command stack
		weeditor.getEditDomain().getCommandStack().execute(
				new CreateLayoutDataCommand(weeditor, selectedScopes, layoutClass));
	}


	/**
	 * Sets the layoutdata of a target to a source layoutdata.
	 * 
	 * @param src_LayoutData
	 * @param dest_LayoutData
	 */
	public static void transferLayoutDataValues(LayoutData src_LayoutData,
			LayoutData dest_LayoutData)
	{
		dest_LayoutData.setX(src_LayoutData.getX());
		dest_LayoutData.setY(src_LayoutData.getY());
		dest_LayoutData.setWidth(src_LayoutData.getWidth());
		dest_LayoutData.setHeight(src_LayoutData.getHeight());

		dest_LayoutData.setInitialized(src_LayoutData.isInitialized());
	}


	/**
	 * This method should be called each time the view is changed. It searches
	 * for missing layoutdata and displays a message to the user (if activated
	 * in the preferences) which allows to start the view configuration wizard.
	 * It also calculates a best guess for importing view data (based on which
	 * view has the biggest amount of relevant objects with layoutdata)
	 */
	public static void uninitializedLayoutDataCheck(WEEditor weeditor)
	{
		// layoutdata preferences: return if deactivated in the preferences
		if (PreferenceReader.viewLayoutData.get().equals(VIEW_LAYOUTDATA_NOTHING))
		{
			return;
		}

		// search for missing layoutdata and collect statistic information
		Set<GraphicalElement> uninitializedElements = new HashSet<GraphicalElement>();
		Set<EObject> initializedElements = new HashSet<EObject>();

		for (Iterator iter = EcoreUtil
				.getAllContents((EObject) weeditor.getModel(), true); iter.hasNext();)
		{
			EObject eobject = (EObject) iter.next();

			// if the graphicalelement is visible in the current view
			if (eobject instanceof GraphicalElement && !(eobject instanceof Comment)
					&& Views.getInstance().displayObject(eobject))
			{
				if (!(getLayoutData(weeditor, (GraphicalElement) eobject))
						.isInitialized())
				{
					// if it is not initialized with layoutdata
					uninitializedElements.add((GraphicalElement) eobject);
				}
				else
				{
					// else initialized
					initializedElements.add(eobject);
				}
			}
		}
		for (Iterator iter = weeditor.getDiagramData().getReferences().iterator(); iter
				.hasNext();)
		{
			EObject eobject = (EObject) iter.next();

			// if the graphicalelement is visible in the current view and not
			// initialized...
			if (Views.getInstance().displayObject(eobject))
			{
				if (!(getLayoutData(weeditor, (GraphicalElement) eobject))
						.isInitialized())
				{
					// if it is not initialized with layoutdata
					uninitializedElements.add((GraphicalElement) eobject);
				}
				else
				{
					// else initialized
					initializedElements.add(eobject);
				}
			}
		}

		// return if no missing data
		if (uninitializedElements.size() == 0)
		{
			return;
		}

		// layoutdata preferences: show wizard
		if (PreferenceReader.viewLayoutData.get().equals(VIEW_LAYOUTDATA_WIZARD))
		{
			ViewConfWizard wizard = new ViewConfWizard();
			wizard.init(PlatformUI.getWorkbench(), null);
			WizardDialog wizardDialog = new WizardDialog(GeneralHelper.getActiveShell(),
					wizard);
			wizardDialog.open();
		}
		// layoutdata preferences: automatic import
		else if (PreferenceReader.viewLayoutData.get().equals(VIEW_LAYOUTDATA_IMPORT))
		{
			Set<Scope> activities = new HashSet<Scope>();

			for (Iterator iter = EcoreUtil.getAllContents((EObject) weeditor.getModel(),
					true); iter.hasNext();)
			{
				EObject eobject = (EObject) iter.next();

				if (eobject instanceof Scope)
				{
					activities.add((Scope) eobject);
				}
			}

			importLayoutData(weeditor, activities, computeBestView(weeditor), Views
					.getInstance().getSelectedView().getInternalName(), true);
		}
		// layoutdata preferences: automatic layout
		else if (PreferenceReader.viewLayoutData.get().equals(VIEW_LAYOUTDATA_LAYOUT))
		{
			Set<Scope> scopes = new HashSet<Scope>();

			// look for all activities which are completely uninitialized
			for (Iterator iter = EcoreUtil.getAllContents((EObject) weeditor.getModel(),
					true); iter.hasNext();)
			{
				EObject eobject = (EObject) iter.next();

				// check if scope is completely uninitialized
				if (eobject instanceof Scope)
				{
					boolean scopeUninitialized = true;

					// iterate over all contents of scope
					for (Iterator iter2 = EcoreUtil.getAllContents((Scope) eobject, true); iter2
							.hasNext();)
					{
						EObject containedObject = (EObject) iter2.next();
						if (initializedElements.contains(containedObject))
						{
							// an initialized element was found
							scopeUninitialized = false;
							break;
						}
					}
					for (Iterator iter2 = EMFHelper.getReferencesForScope(weeditor,
							(Scope) eobject).iterator(); iter2.hasNext();)
					{
						EObject containedObject = (EObject) iter2.next();
						if (initializedElements.contains(containedObject))
						{
							// an initialized element was found
							scopeUninitialized = false;
							break;
						}
					}

					if (scopeUninitialized)
					{
						scopes.add((Scope) eobject);
					}
				}
			}

			// execute the command on the command stack
			weeditor.getEditDomain().getCommandStack().execute(
					new CreateLayoutDataCommand(weeditor, scopes,
							SpringLayoutAlgorithm.class));
		}
		// layoutdata preferences: show dialog
		else if (PreferenceReader.viewLayoutData.get().equals(VIEW_LAYOUTDATA_DIALOG))
		{
			// find the view with the highest score (the most relevant) for the
			// suggestion
			String bestViewID = computeBestView(weeditor);

			// show dialog depending on whether a suitable view has been found
			if (!bestViewID.equals(Views.getInstance().getSelectedView()
					.getInternalName()))
			{
				// search for official view name
				String bestViewNameID = bestViewID;
				for (Object viewdescriptor : Views.getInstance().getAvailableViews())
				{
					if (bestViewID.equals(((ViewDescriptor) viewdescriptor)
							.getInternalName()))
					{
						bestViewNameID = ((ViewDescriptor) viewdescriptor)
								.getOfficialName();
					}
				}

				// ask the question
				int answer = new MessageDialog(GeneralHelper.getActiveShell(),
						PluginProperties.view_LayoutData_label, null, PluginProperties
								.bind(PluginProperties.view_NoLayoutData_label,
										bestViewNameID), MessageDialog.QUESTION,
						new String[]
						{
								PluginProperties.view_NoLayoutDataWizard_label,
								PluginProperties.bind(
										PluginProperties.view_NoLayoutDataImport_label,
										bestViewNameID),
								PluginProperties.view_NoLayoutDataNothing_label,
								PluginProperties.view_NoLayoutDataDeactivate_label }, 0)
						.open();
				if (answer == 0)
				{
					// open wizard
					ViewConfWizard wizard = new ViewConfWizard();
					wizard.init(PlatformUI.getWorkbench(), null);
					WizardDialog wizardDialog = new WizardDialog(GeneralHelper
							.getActiveShell(), wizard);
					wizardDialog.open();
				}
				else if (answer == 1)
				{
					Set<Scope> activities = new HashSet<Scope>();

					for (Iterator iter = EcoreUtil.getAllContents((EObject) weeditor
							.getModel(), true); iter.hasNext();)
					{
						EObject eobject = (EObject) iter.next();

						if (eobject instanceof Scope)
						{
							activities.add((Scope) eobject);
						}
					}

					// automatic import
					importLayoutData(weeditor, activities, bestViewID, Views
							.getInstance().getSelectedView().getInternalName(), true);
				}
				else if (answer == 3)
				{
					// deactivate automatic search
					PreferenceReader.viewLayoutData.set(VIEW_LAYOUTDATA_NOTHING);
				}
			}
			else
			{
				// no better viewdata was found

				// ask the question
				int answer = new MessageDialog(GeneralHelper.getActiveShell(),
						PluginProperties.view_LayoutData_label, null,
						PluginProperties.view_NoLayoutData2_label,
						MessageDialog.QUESTION, new String[]
						{ PluginProperties.view_NoLayoutDataWizard_label,
								PluginProperties.view_NoLayoutDataNothing_label,
								PluginProperties.view_NoLayoutDataDeactivate_label }, 0)
						.open();
				if (answer == 0)
				{
					// open wizard
					ViewConfWizard wizard = new ViewConfWizard();
					wizard.init(PlatformUI.getWorkbench(), null);
					WizardDialog wizardDialog = new WizardDialog(GeneralHelper
							.getActiveShell(), wizard);
					wizardDialog.open();
				}
				else if (answer == 2)
				{
					// deactivate automatic search
					PreferenceReader.viewLayoutData.set(VIEW_LAYOUTDATA_NOTHING);
				}
			}
		}
	}


	/**
	 * Calculates a best guess for importing view data (based on which view has
	 * the biggest amount of relevant objects with layoutdata).
	 * 
	 * @return A Map with the view ids and the scores.
	 */
	public static HashMap computeViewScore(WEEditor weeditor)
	{
		HashMap viewDefinitionIndex = new HashMap();
		ArrayList<GraphicalElement> graphicalElements = new ArrayList<GraphicalElement>();

		// iterate over all elements
		for (Iterator iter = EcoreUtil
				.getAllContents((EObject) weeditor.getModel(), true); iter.hasNext();)
		{
			EObject eobject = (EObject) iter.next();
			if (eobject instanceof GraphicalElement)
			{
				graphicalElements.add((GraphicalElement) eobject);
				// create the layout data if not already present
				getLayoutData(weeditor, (GraphicalElement) eobject);
			}
		}
		for (Iterator iter = weeditor.getDiagramData().getReferences().iterator(); iter
				.hasNext();)
		{
			Reference reference = (Reference) iter.next();
			graphicalElements.add(reference);
			// create the layout data if not already present
			getLayoutData(weeditor, reference);
		}

		for (GraphicalElement graphicalElement : graphicalElements)
		{
			// iterate over all layoutdata of graphicalelements
			for (Object layoutData : getLayoutDataForGraphicalElement(weeditor,
					graphicalElement))
			{
				String viewid = ((LayoutData) layoutData).getViewid().toLowerCase();

				// if the view has relevant information for the current view,
				// increase its score
				if (!(getLayoutData(weeditor, graphicalElement).isInitialized())
						&& ((LayoutData) layoutData).isInitialized()
						&& Views.getInstance().displayObject(graphicalElement))
				{
					// initialized in target view and displayed in current view:
					// increase relevant count for initialized elements for this
					// view
					if (viewDefinitionIndex.get(viewid) == null)
					{
						viewDefinitionIndex.put(viewid, new Integer(1));
					}
					else
					{
						viewDefinitionIndex.put(viewid, ((Integer) viewDefinitionIndex
								.get(viewid)) + 1);
					}
				}
			}
		}

		return viewDefinitionIndex;
	}


	/**
	 * Compute the view id of the view that has the biggest amount of relevant
	 * elements for the current view.
	 * 
	 * @return The view id.
	 */
	public static String computeBestView(WEEditor weeditor)
	{
		// compute the score
		HashMap viewDefinitionIndex = computeViewScore(weeditor);

		// return if no view definitions were found
		if (viewDefinitionIndex.isEmpty())
		{
			return Views.getInstance().getSelectedView().getInternalName();
		}

		// find the view with the highest score (the most relevant) for the
		// suggestion
		String bestViewID = viewDefinitionIndex.keySet().iterator().next().toString();
		for (Object viewID : viewDefinitionIndex.keySet())
		{
			if ((Integer) viewDefinitionIndex.get((String) viewID) > (Integer) viewDefinitionIndex
					.get(bestViewID))
			{
				bestViewID = (String) viewID;
			}
		}

		if (((Integer) viewDefinitionIndex.get(bestViewID)) == 0)
		{
			return Views.getInstance().getSelectedView().getInternalName();
		}

		return bestViewID;
	}


	/**
	 * Remove layoutdata from an object and (its contents recursively), except
	 * the layoutdata for the given view.
	 * 
	 * @param eobject
	 * @param viewID
	 */
	public static void removeLayoutData(WEEditor weeditor, EObject eobject, String viewID)
	{
		// create a list of all (contained) graphicalelements
		ArrayList<GraphicalElement> graphicalelements = new ArrayList<GraphicalElement>();
		if (eobject instanceof GraphicalElement)
		{
			graphicalelements.add((GraphicalElement) eobject);
		}
		if (eobject.eContents() != null && eobject.eContents().size() > 0)
		{
			for (Iterator iter = EcoreUtil.getAllContents(eobject, true); iter.hasNext();)
			{
				Object object = (EObject) iter.next();

				if (object instanceof GraphicalElement)
				{
					graphicalelements.add((GraphicalElement) object);
				}
			}
		}

		// remove all layoutdata except the one for the given viewid
		viewID = viewID.toLowerCase();
		for (GraphicalElement graphicalelement : graphicalelements)
		{
			if (graphicalelement instanceof GraphicalElement)
			{
				ArrayList<LayoutData> layoutdatas = new ArrayList<LayoutData>();

				for (Object lD : getLayoutDataForGraphicalElement(weeditor,
						graphicalelement))
				{
					layoutdatas.add((LayoutData) lD);
				}

				for (LayoutData lD : layoutdatas)
				{
					String viewIDld = lD.getViewid().toLowerCase();
					if (!viewIDld.equals(viewID))
					{
						lD.setDescribesElement(null);
						EMFHelper.getDiagram(weeditor).getLayoutData().remove(lD);
					}
				}
			}
		}
	}

}