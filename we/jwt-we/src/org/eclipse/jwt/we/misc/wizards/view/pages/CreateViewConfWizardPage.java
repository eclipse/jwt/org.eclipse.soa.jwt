/**
 * File:    CreateViewConfWizardPage.java
 * Created: 15.12.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.wizards.view.pages;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.misc.util.FontUtil;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.misc.views.Views;
import org.eclipse.jwt.we.misc.wizards.WizardHelper;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.zest.layouts.algorithms.HorizontalLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.HorizontalShift;
import org.eclipse.zest.layouts.algorithms.HorizontalTreeLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.VerticalLayoutAlgorithm;


/**
 * Create layout data using a layouting algorithm.
 * 
 * @version $Id: CreateViewConfWizardPage.java,v 1.4 2009-11-26 12:41:26 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class CreateViewConfWizardPage
		extends WizardPage
{

	/**
	 * A Logger.
	 */
	static final Logger logger = Logger.getLogger(CreateViewConfWizardPage.class);

	/**
	 * The available algorithms.
	 */
	private Combo algorithmCombo;

	/**
	 * The layout algorithm IDs.
	 */
	private String layoutIDs[] =
	{ "HorizontalTreeLayoutAlgorithm", "TreeLayoutAlgorithm", "RadialLayoutAlgorithm",
			"SpringLayoutAlgorithm", "HorizontalShift", "HorizontalLayoutAlgorithm",
			"VerticalLayoutAlgorithm" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$


	/**
	 * Constructor.
	 * 
	 * @param pageId
	 *            The name of the page.
	 * @param workbench
	 *            The workbench.
	 */
	public CreateViewConfWizardPage(String pageId, IWorkbench workbench)
	{
		super(pageId);
	}


	/**
	 * @return The EMF EditingDomain.
	 */
	public EditingDomain getEditingDomain()
	{
		return (EditingDomain) GeneralHelper.getActiveInstance().getAdapter(
				EditingDomain.class);
	}


	/**
	 * @return The {@link AdapterFactory} from an {@link AdapterFactoryEditingDomain} if
	 *         available.
	 */
	public AdapterFactory getAdapterFactory()
	{
		return (AdapterFactory) GeneralHelper.getActiveInstance().getAdapter(
				AdapterFactory.class);
	}


	/**
	 * Returns the algorithm class of the selected layouting algorithm.
	 * 
	 * @return The ID of the selected layout.
	 */
	public Class getSelectedLayoutClass()
	{
		String selectedLayoutID = algorithmCombo.getData(algorithmCombo.getText())
				.toString();

		if (selectedLayoutID.equals("HorizontalTreeLayoutAlgorithm")) //$NON-NLS-1$
			return HorizontalTreeLayoutAlgorithm.class;
		else if (selectedLayoutID.equals("TreeLayoutAlgorithm")) //$NON-NLS-1$
			return TreeLayoutAlgorithm.class;
		else if (selectedLayoutID.equals("RadialLayoutAlgorithm")) //$NON-NLS-1$
			return RadialLayoutAlgorithm.class;
		else if (selectedLayoutID.equals("SpringLayoutAlgorithm")) //$NON-NLS-1$
			return SpringLayoutAlgorithm.class;
		else if (selectedLayoutID.equals("HorizontalShift")) //$NON-NLS-1$
			return HorizontalShift.class;
		else if (selectedLayoutID.equals("HorizontalLayoutAlgorithm")) //$NON-NLS-1$
			return HorizontalLayoutAlgorithm.class;
		else if (selectedLayoutID.equals("VerticalLayoutAlgorithm")) //$NON-NLS-1$
			return VerticalLayoutAlgorithm.class;
		else
			return null;
	}


	/**
	 * Returns the display name of the given layouting algorithm.
	 * 
	 * @param layoutID
	 * @return The layout name.
	 */
	private String getLayoutName(String layoutID)
	{
		String layoutName = PluginProperties.getString("wizards_ViewConfAlgorithm_" //$NON-NLS-1$
				+ layoutID.replace("LayoutAlgorithm", "") + "_name"); //$NON-NLS-1$ //$NON-NLS-2$

		if (layoutName == null || layoutName.equals("")) //$NON-NLS-1$
		{
			return layoutID;
		}
		else
		{
			return layoutName;
		}
	}


	/**
	 * Returns the display description of the given layouting algorithm.
	 * 
	 * @param layoutID
	 * @return The layout description.
	 */
	private String getLayoutDescription(String layoutID)
	{
		String layoutDesc = PluginProperties.getString("wizards_ViewConfAlgorithm_" //$NON-NLS-1$
				+ layoutID.replace("LayoutAlgorithm", "") + "_description"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		if (layoutDesc == null || layoutDesc.equals("")) //$NON-NLS-1$
		{
			return layoutID;
		}
		else
		{
			return layoutDesc;
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite
	 * )
	 */
	public void createControl(Composite parent)
	{
		// Composite
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));
		composite.setLayoutData(WizardHelper.createData(GridData.FILL, GridData.FILL,
				true, false, -1, -1));

		// the head line
		Label viewLabel1 = new Label(composite, SWT.LEFT);
		viewLabel1.setText(PluginProperties
				.wizards_ViewConfCurrentView_label);
		viewLabel1.setLayoutData(WizardHelper.createData(-1, GridData.BEGINNING, false,
				false, -1, 1));

		Label viewLabel2 = new Label(composite, SWT.LEFT);
		viewLabel2.setText(Views.getInstance().getSelectedView().getOfficialName() + " ("
				+ Views.getInstance().getSelectedView().getInternalName() + ")");
		viewLabel2.setLayoutData(WizardHelper.createData(-1, GridData.BEGINNING, false,
				false, -1, 2));
		viewLabel2.setFont(FontUtil.getSystemStyle(SWT.ITALIC | SWT.BOLD));

		Label dummyLabel = new Label(composite, SWT.LEFT);
		dummyLabel.setLayoutData(WizardHelper.createData(-1, GridData.BEGINNING, false,
				false, -1, 3));
		Label dummyLabel2 = new Label(composite, SWT.LEFT);
		dummyLabel2.setLayoutData(WizardHelper.createData(-1, GridData.BEGINNING, false,
				false, -1, 3));

		// the select view label
		Label selectLabel = new Label(composite, SWT.LEFT | SWT.WRAP);
		selectLabel.setText(PluginProperties
				.wizards_ViewConfCreateSelect_label);
		selectLabel.setLayoutData(WizardHelper.createData(-1, GridData.BEGINNING, false,
				false, 2, 1));

		// layout algorithm Combo Box
		algorithmCombo = new Combo(composite, SWT.DROP_DOWN | SWT.READ_ONLY);
		algorithmCombo.setLayoutData(WizardHelper.createData(-1, -1, false, false, 1, 1));
		final Label descLabel = new Label(composite, SWT.LEFT);
		descLabel.setLayoutData(WizardHelper.createData(-1, -1, false, false, 1, 3));

		for (String layoutID : layoutIDs)
		{
			String layoutName = getLayoutName(layoutID);
			algorithmCombo.add(layoutName);
			algorithmCombo.setData(layoutName, layoutID);
		}

		// combo listener
		algorithmCombo.addSelectionListener(new SelectionListener()
		{

			public void widgetDefaultSelected(SelectionEvent e)
			{
			}


			public void widgetSelected(SelectionEvent e)
			{
				descLabel.setText(getLayoutDescription(algorithmCombo.getData(
						algorithmCombo.getText()).toString()));
			}
		});

		// activate first algorithm
		if (algorithmCombo.getItemCount() > 0)
		{
			algorithmCombo.select(3);
			descLabel.setText(getLayoutDescription(algorithmCombo.getData(
					algorithmCombo.getText()).toString()));
		}

		// Validate
		setMessage(PluginProperties.wizards_ViewConfCreate_message);
		setControl(composite);

		// refresh packageviewer display
		getShell().pack();

		// make dialog bigger and center
		WizardHelper.resizeDialog(getShell());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage#getNextPage()
	 */
	@Override
	public IWizardPage getNextPage()
	{
		return getWizard().getPage("activity"); //$NON-NLS-1$
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite
	 * )
	 */
	@Override
	public void dispose()
	{
		if (algorithmCombo != null && !algorithmCombo.isDisposed())
		{
			algorithmCombo.dispose();
			algorithmCombo = null;
		}

		super.dispose();
	}

}