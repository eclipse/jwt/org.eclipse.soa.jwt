/**
 * File:    EdgeRelationship.java
 * Created: 30.03.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.layouting;

import org.eclipse.zest.layouts.LayoutBendPoint;
import org.eclipse.zest.layouts.LayoutEntity;
import org.eclipse.zest.layouts.LayoutRelationship;
import org.eclipse.zest.layouts.constraints.LayoutConstraint;


/**
 * A simple wrapper class for JWT edges, used by the Zest project's layouter.
 * 
 * @version $Id: EdgeRelationship.java,v 1.4 2009-11-26 12:41:51 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class EdgeRelationship
		implements LayoutRelationship
{

	/**
	 * The sourceEntity of this SimpleRelation.
	 */
	protected LayoutEntity sourceEntity;

	/**
	 * The object of this SimpleRelation.
	 */
	protected LayoutEntity destinationEntity;

	/**
	 * The internal relationship.
	 */
	private Object internalRelationship;


	/**
	 * The constructor.
	 * 
	 * @param sourceEntity
	 * @param destinationEntity
	 */
	public EdgeRelationship(LayoutEntity sourceEntity, LayoutEntity destinationEntity)
	{
		super();
		this.sourceEntity = sourceEntity;
		this.destinationEntity = destinationEntity;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.zest.layouts.LayoutRelationship#clearBendPoints()
	 */
	public void clearBendPoints()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.uvic.cs.chisel.layouts.LayoutRelationship#getInternalRelationship()
	 */
	public Object getLayoutInformation()
	{
		return internalRelationship;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.uvic.cs.chisel.layouts.LayoutRelationship#setInternalRelationship(java.lang.
	 * Object)
	 */
	public void setLayoutInformation(Object layoutInformation)
	{
		this.internalRelationship = layoutInformation;
	}


	/**
	 * Gets the sourceEntity of this SimpleRelation whether the relation is exchangeable
	 * or not.
	 * 
	 * @return The sourceEntity.
	 */
	public LayoutEntity getSourceInLayout()
	{
		return sourceEntity;
	}


	/**
	 * Gets the destinationEntity of this SimpleRelation whether the relation is
	 * exchangeable or not.
	 * 
	 * @return The destinationEntity of this SimpleRelation.
	 */
	public LayoutEntity getDestinationInLayout()
	{
		return destinationEntity;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.zest.layouts.LayoutRelationship#populateLayoutConstraint(org.eclipse
	 * .zest.layouts.constraints.LayoutConstraint)
	 */
	public void populateLayoutConstraint(LayoutConstraint constraint)
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.zest.layouts.LayoutRelationship#setBendPoints(org.eclipse.zest.layouts
	 * .LayoutBendPoint[])
	 */
	public void setBendPoints(LayoutBendPoint[] bendPoints)
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.zest.layouts.LayoutItem#getGraphData()
	 */
	public Object getGraphData()
	{
		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.zest.layouts.LayoutItem#setGraphData(java.lang.Object)
	 */
	public void setGraphData(Object o)
	{

	}

}
