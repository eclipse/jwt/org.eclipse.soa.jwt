/**
 * File:    ParseProperties.java
 * Created: 30.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.util.internal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;


/**
 * Class for parsing propertie files.
 * 
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class ParseProperties
{

	/**
	 * Reads a propertie file and puts its keys and values into a hashtable
	 * 
	 * @param filename
	 *            The filename to be read
	 * @return {@link Hashtable}
	 * @throws FileNotFoundException
	 * @throws URISyntaxException
	 * @throws URISyntaxException
	 */
	public static Hashtable<String, String> getProperties(String filename)
			throws FileNotFoundException, URISyntaxException
	{
		Hashtable<String, String> returnhash = new Hashtable<String, String>();
		// TODO: really bad, optimize, but now you can access to files in classpath easily
		// System.out.println(ClassLoader.getSystemResource(filename).getFile());
		File file = new File(ClassLoader.getSystemResource(filename).getFile());
		System.out.println(file);
		BufferedReader myreader = new BufferedReader(new FileReader(file));
		try
		{

			String myline = ""; //$NON-NLS-1$
			while ((myline = myreader.readLine()) != null)
			{
				String[] parts = myline.split("="); //$NON-NLS-1$
				if (parts.length == 2)
				{
					returnhash.put(parts[0], parts[1]);
				}
			}

		}
		catch (IOException ex)
		{

		}
		finally
		{
			try
			{
				if (myreader != null)
					myreader.close();
			}
			catch (Exception ex)
			{

			}
		}

		return returnhash;
	}
}
