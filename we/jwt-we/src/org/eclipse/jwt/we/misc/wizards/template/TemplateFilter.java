/**
 * File:    ExportFilter.java
 * Created: 10.03.2007
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.wizards.template;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jwt.meta.model.application.ApplicationType;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.meta.model.data.DataType;
import org.eclipse.jwt.meta.model.processes.Activity;


/**
 * Filter to display only Packages, ReferenceableElements, Types and Activities in a TreeViewer.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class TemplateFilter
		extends ViewerFilter
{

	/**
	 * Returns whether the given element makes it through this filter.
	 * 
	 * @param viewer
	 *            the viewer
	 * @param parentElement
	 *            the parent element
	 * @param element
	 *            the element
	 * 
	 * @return <code>true</code> if element is included in the filtered set, and
	 *         <code>false</code> if excluded
	 */
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element)
	{
		if (element instanceof org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl
				|| element instanceof org.eclipse.jwt.meta.model.core.Package
				|| element instanceof ReferenceableElement
				|| element instanceof DataType || element instanceof ApplicationType
				|| element instanceof Activity)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
