/**
 * File:    TextFormatter.java
 * Created: 12.12.2005
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.logging.internal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import org.eclipse.swt.widgets.Text;


/**
 * Extended formatter to format log records for console output.
 * 
 * @version $Id: TextFormatter.java,v 1.3 2009-11-26 12:41:04 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
@SuppressWarnings("cast")
public class TextFormatter
		extends Formatter
{

	/**
	 * Pattern for the date. Use standard ISO-format.
	 */
	private static final String datePattern = "yyyy-MM-dd HH:mm:ss.SSS"; //$NON-NLS-1$

	/**
	 * Formatter to format the date.
	 */
	private static final SimpleDateFormat dateFormatter = new SimpleDateFormat(
			datePattern);

	/**
	 * Date.
	 */
	private Date date = new Date();

	/**
	 * Line separator string. This is the value of the line.separator property at the
	 * moment that the SimpleFormatter was created.
	 */
	private String lineSeparator = Text.DELIMITER;


	/**
	 * Constructor.
	 */
	public TextFormatter()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
	 */
	@Override
	public String format(LogRecord record)
	{
		StringBuffer text = new StringBuffer();

		// Date + time
		date.setTime(record.getMillis());
		text.append(dateFormatter.format(date));
		text.append(" "); //$NON-NLS-1$

		// class
		if (record.getSourceClassName() != null)
		{
			text.append(record.getSourceClassName());
		}
		else
		{
			text.append(record.getLoggerName());
		}

		// method
		if (record.getSourceMethodName() != null)
		{
			text.append(" "); //$NON-NLS-1$
			text.append(record.getSourceMethodName());
		}

		text.append(lineSeparator);

		// Level
		text.append("  "); //$NON-NLS-1$
		text.append(record.getLevel().getName());

		// Message
		if (record.getMessage() != null)
		{
			text.append(": "); //$NON-NLS-1$
			String message = formatMessage(record);
			text.append(message);
		}

		// Exception
		if (record.getThrown() != null)
		{
			text.append(" ("); //$NON-NLS-1$
			text.append(record.getThrown().getClass().getName() + ": " //$NON-NLS-1$
					+ record.getThrown().getMessage());
			text.append(")"); //$NON-NLS-1$
			// sb.append(lineSeparator);
			// try
			// {
			// StringWriter sw = new StringWriter();
			// PrintWriter pw = new PrintWriter(sw);
			// record.getThrown().printStackTrace(pw);
			// pw.close();
			// sb.append(sw.toString());
			// }
			// catch (Exception ex)
			// {
			// }
		}
		text.append(lineSeparator);

		return text.toString();
	}

}
