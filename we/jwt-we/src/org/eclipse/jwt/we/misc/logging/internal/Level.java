/**
 * File:    Level.java
 * Created: 11.12.2005
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.logging.internal;

/**
 * Some more levels for logging.
 * 
 * @version $Id: Level.java,v 1.3 2009-11-26 12:41:03 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
@SuppressWarnings("nls")
public class Level
		extends java.util.logging.Level
{

	/**
	 * The serial version UID
	 */
	private static final long serialVersionUID = -218428627180049351L;

	/**
	 * An warning for debug purpose. Something went wrong, but the normal user does not
	 * need to care about this.
	 */
	public static final Level DEBUG_WARNING = new Level("DEBUG WARNING", 750);

	/**
	 * A value/property has changed.
	 */
	public static final Level VALUE_CHANGED = new Level("VALUE CHANGED", 600);

	/**
	 * An instanz was created.
	 */
	public static final Level INSTANCE_CREATION = new Level("INSTANCE CREATION", 550);

	/**
	 * An exception is being throwed.
	 */
	public static final Level THROWING = new Level("THROWING", 450);

	/**
	 * Method entry.
	 */
	public static final Level ENTER = new Level("ENTER", 411);

	/**
	 * Method exit.
	 */
	public static final Level EXIT = new Level("EXIT", 410);

	/**
	 * Debug informations.
	 */
	public static final Level DEBUG = new Level("DEBUG", 100);


	/**
	 * @param name
	 *            The name of the Level.
	 * @param value
	 *            An integer value for the level.
	 */
	protected Level(String name, int value)
	{
		super(name, value);
	}


	/**
	 * @param name
	 *            The name of the Level.
	 * @param value
	 *            An integer value for the level.
	 * @param resourceBundleName
	 *            Name name of a resource bundle.
	 */
	protected Level(String name, int value, String resourceBundleName)
	{
		super(name, value, resourceBundleName);
	}

}
