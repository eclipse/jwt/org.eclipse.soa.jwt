/**
 * File:    StringSplitter.java
 * Created: 26.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.util.internal;

import java.util.ArrayList;


/**
 * Static methods for splitting a string into several substrings.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org.
 */
public class StringParser
{

	/**
	 * Splits a string into a list of strings according to a set of splitChars. Unlike the
	 * split method of the string class, the split characters are preserved in the result
	 * list. Additionally a minimal length for substrings can be provided. If a string
	 * part is shorter than the minimal length, it is appended to the previous part.
	 * 
	 * @param input
	 *            The input string
	 * @param splitChars
	 *            The list of split characters
	 * @param minLength
	 *            Characters shorter than this are appended to the predecessor
	 * @return A list of separate strings.
	 */
	public static ArrayList splitChar(String input, String[] splitChars, int minLength)
	{
		ArrayList stringParts = new ArrayList();
		String buffer = ""; //$NON-NLS-1$

		// do until input guard string is empty
		while (!input.equals(""))//$NON-NLS-1$
		{
			// move first character to buffer
			buffer += input.charAt(0);
			input = input.substring(1);

			int i = 0;
			boolean found = false;

			// look if one of the control characters is present
			while (i < splitChars.length && found == false)
			{
				if (buffer.contains(splitChars[i]))
				{
					// if the buffer is too short, add it to the previous
					// element, if it's long enough add it as separate element
					if (stringParts.size() > 0 &&
							((String)stringParts.get(stringParts.size() - 1)).length() < minLength)
					{
						stringParts.set(stringParts.size() - 1, stringParts
								.get(stringParts.size() - 1)
								+ buffer);
					}
					else
					{
						stringParts.add(buffer);
					}

					// clear buffer
					buffer = "";//$NON-NLS-1$
					found = true;
				}
				else
				{
					// if no element was found, look at next character
					i++;
				}
			}
		}

		// add the remaining part of the buffer to the list
		stringParts.add(buffer);

		return stringParts;
	}


	public static ArrayList splitLength(String input, int length)
	{
		ArrayList stringParts = new ArrayList();
		String buffer = ""; //$NON-NLS-1$
		
		for (int i = 0; i < input.length(); i++)
		{
			if ((i + 1) % length == 0)
			{
				buffer += input.charAt(i);
				stringParts.add(buffer);
				buffer = ""; //$NON-NLS-1$
			}
			else
			{
				buffer += input.charAt(i);
			}
		}
		
		if (!buffer.equals("")) //$NON-NLS-1$
		{
			stringParts.add(buffer);
		}
		
		return stringParts;
	}
		
	/**
	 * Concatenates a list of strings into a single string with line wraps after the
	 * maximal line length has been reached. If the length of several strings exceeds the
	 * maximal line length, a line wrap is inserted after the next complete part.
	 * 
	 * @param inputParts
	 *            The input string parts
	 * @param maxLength
	 *            The maximal line length
	 * @return A single string with line wraps.
	 */
	public static String concatWithLineWraps(ArrayList inputParts, int maxLength)
	{
		String buffer = ""; //$NON-NLS-1$
		String result = ""; //$NON-NLS-1$

		// concatenate the parts to a complete string
		for (int i = 0; i < inputParts.size(); i++)
		{
			String currentPart = (String) inputParts.get(i);

			// if the maximal length was not exceeded, add
			// the string part to the result, if it was exceeded
			// add a new line
			if (buffer.length() < maxLength)
			{
				buffer += currentPart;
			}
			else
			{
				result += buffer + "\n"; //$NON-NLS-1$
				buffer = currentPart;
			}
		}

		// add the rest of the buffer to the result
		result += buffer;

		// remove new line at the end of the result
		if (result.length() != 0 && result.charAt(result.length() - 1) == '\n')
		{
			result = result.substring(0, result.length() - 2);
		}

		return result;
	}

}
