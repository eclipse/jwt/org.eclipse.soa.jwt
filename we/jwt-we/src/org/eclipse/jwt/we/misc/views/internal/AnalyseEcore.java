/**
 * File:    AnalyseEcore.java
 * Created: 16.12.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.views.internal;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.misc.logging.internal.Level;


/**
 * This file analyses the ecore-metamodel and searches for every package
 * 
 * @version $Id: AnalyseEcore.java,v 1.4 2009-11-26 12:41:16 chsaad Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class AnalyseEcore
{

	Logger logger = Logger.getLogger(AnalyseEcore.class);
	List packageslist = new ArrayList();


	/**
	 * Standardconstructor. Automatically parses an ecore file and extracts the root
	 * packages. This root packages will be saved in a list for later analysation.
	 * 
	 * @param path
	 *            URI to the ecore file (bundleresource, file, platform, http...)
	 */
	public AnalyseEcore(String uri)
	{
		ResourceSetImpl resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*",  //$NON-NLS-1$
				new EcoreResourceFactoryImpl());

		URI fileURI = URI.createURI(uri);
		logger.log(Level.INFO, fileURI.devicePath());

		Resource resource = resourceSet.getResource(fileURI, true);
		if (resource != null)
		{
			EList modelpackage = resource.getContents();
			if (modelpackage.size() == 1)
			{
				EList allsubpackages = ((EObject) modelpackage.get(0)).eContents();
				for (int i = 0; i < allsubpackages.size(); i++)
				{
					EObject eobj = (EObject) allsubpackages.get(i);
					packageslist.add(eobj);
				}
			}
		}
	}


	/**
	 * Returns a list with all packages, that are directly contained underneath the
	 * "Model" package
	 * 
	 * @return
	 */
	public List getPackagesList()
	{
		return packageslist;
	}
}
