/**
 * File:    ExtensionPointNotifier.java
 * Created: 02.10.2008
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.extensions;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.Platform;


/**
 * This helper class offers some methods to deal with extension points.
 * 
 * @version $Id: ExtensionsHelper.java,v 1.4 2009-11-26 12:41:04 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class ExtensionsHelper
{

	/**
	 * Locate and return an extension point by its ID.
	 * 
	 * @param pointId
	 * @return The extension point.
	 */
	public static IExtensionPoint getExtensionPointByID(String pointId)
	{
		return Platform.getExtensionRegistry().getExtensionPoint(pointId);
	}


	/**
	 * Return all configuration elements in an extension point.
	 * 
	 * @param pointId
	 * @return The configuration elements.
	 */
	public static IConfigurationElement[] findConfigurationElements(String pointId)
	{
		IExtensionPoint extensionPoint = getExtensionPointByID(pointId);

		if (extensionPoint != null)
		{
			return extensionPoint.getConfigurationElements();
		}
		else
		{
			return null;
		}
	}


	/**
	 * Locate a specific configuration element with the given Id.
	 * 
	 * @param pointId
	 * @param extensionId
	 * @return The configuration element.
	 */
	public static IConfigurationElement findConfigurationElement(String pointId,
			String extensionId)
	{
		IExtensionPoint extensionPoint = getExtensionPointByID(pointId);

		if (extensionPoint != null)
		{
			for (IConfigurationElement element : extensionPoint
					.getConfigurationElements())
			{
				if (element.getAttribute("id").equals(extensionId)) //$NON-NLS-1$
				{
					return element;
				}
			}
		}

		return null;
	}

}
