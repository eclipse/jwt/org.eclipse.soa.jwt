/**
 * File:    EclipseLogFormatter.java
 * Created: 13.12.2005
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.misc.logging.internal;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;


/**
 * Formats log reports for the eclipse logger.
 * 
 * @version $Id: EclipseLogFormatter.java,v 1.3 2009-11-26 12:41:04 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class EclipseLogFormatter
		extends Formatter
{

	/**
	 * Constructor.
	 */
	public EclipseLogFormatter()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
	 */
	@Override
	public String format(LogRecord record)
	{
		return formatMessage(record);
	}

}
