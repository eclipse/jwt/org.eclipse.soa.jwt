/**
 * File:    NotaFormulaException.java
 * Created: 20.09.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.misc.guards;

import org.eclipse.jwt.we.PluginProperties;


/**
 * This exception is thrown if a formula is not a correct formula
 * 
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class NotaFormulaException
		extends Exception
{

	/**
	 * The serial version UID
	 */
	private static final long serialVersionUID = 4455640732366205533L;

	public static final int MISSING_CLOSING_BRACKET = 0;
	public static final int UNKNOWN_SIGN = 1;
	public static final int UNEVEN_NUMBER_OF_BRACKET = 2;
	public static final int UNKNOWN_OPERATOR = 3;
	public static final int NOT_A_CORRECT_TERM = 4;
	public static final int UNKNOWN_TERM_COMBINATION = 5;
	public static final int UNKNOWN_ATTRIBUTE = 6;

	int indexoferror = -1;
	int errornumber = -1;


	/**
	 * Constructor
	 * 
	 * @param message
	 * @param errorindex
	 * @param errornumber
	 */
	public NotaFormulaException(int errorindex, int errornumber)
	{
		super(getMessageInternal(errornumber));
		indexoferror = errorindex;
		this.errornumber = errornumber;
	}


	/**
	 * Error index.
	 * 
	 * @return
	 */
	public int getIndexofError()
	{
		return indexoferror;
	}


	/**
	 * Error number.
	 * 
	 * @return
	 */
	public int getErrorNumber()
	{
		return errornumber;
	}


	/**
	 * Get Message.
	 * 
	 * @param errornumber
	 * @return
	 */
	private static String getMessageInternal(int errornumber)
	{
		switch (errornumber)
		{
			case MISSING_CLOSING_BRACKET:
				return PluginProperties.guards_ExceptionClosingBracket_message;
			case UNKNOWN_SIGN:
				return PluginProperties.guards_ExceptionUnknownSign_message;
			case UNEVEN_NUMBER_OF_BRACKET:
				return PluginProperties.guards_ExceptionUnevenBracket_message;
			case UNKNOWN_OPERATOR:
				return PluginProperties.guards_ExceptionUnknownOperator_message;
			case NOT_A_CORRECT_TERM:
				return PluginProperties.guards_ExceptionNotCorrectTerm_message;
			case UNKNOWN_TERM_COMBINATION:
				return PluginProperties.guards_ExceptionUnknownCombination_message;
			case UNKNOWN_ATTRIBUTE:
				return PluginProperties.guards_ExceptionUnknownAttribute_message;
		}
		return ""; //$NON-NLS-1$
	}

}
