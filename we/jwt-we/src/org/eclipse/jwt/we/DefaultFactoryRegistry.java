/**
 * File:    FactoryRegistry.java
 * Created: 02.02.2009
 *
/*******************************************************************************
 * Copyright (C) 2007  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - initial API and implementation of the FactoryRegistry concept.
 *    Yoann Rodiere, Open Wide, Lyon, France
 *    	- changed figureFactory to always return at least a default figure
 *******************************************************************************/

package org.eclipse.jwt.we;

import org.eclipse.gef.EditPartFactory;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jwt.we.editors.palette.IPaletteFactory;
import org.eclipse.jwt.we.editors.palette.internal.PaletteFactory;
import org.eclipse.jwt.we.figures.DefaultFigureFactory;
import org.eclipse.jwt.we.figures.IFigureFactory;
import org.eclipse.jwt.we.figures.IImageFactory;
import org.eclipse.jwt.we.figures.StandardFigureFactory;
import org.eclipse.jwt.we.figures.internal.CompositeFigureFactory;
import org.eclipse.jwt.we.figures.internal.ImageFactory;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.parts.JWTEditPartFactory;

/**
 * Default implementation of the FactoryRegistry. This implementation can be
 * overwritten into the plug-in that extends the extension point:
 * factoryRegistry.
 * 
 * @version $Id: DefaultFactoryRegistry.java,v 1.1 2010-05-10 08:23:24 chsaad Exp $
 * @author Marc Blachon, Bull SAS, France, www.bull.com
 * @since 0.6.0
 */

public class DefaultFactoryRegistry implements IFactoryRegistry
{

	/**
	 * imageFactory
	 */
	protected IImageFactory imageFactory;

	/**
	 * figureFactory
	 */
	protected IFigureFactory figureFactory;

	/**
	 * editPartFactory
	 */
	protected EditPartFactory editPartFactory;

	/**
	 * paletteFactory
	 */
	protected PaletteFactory paletteFactory;

	/**
	 * A Logger.
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(DefaultFactoryRegistry.class);


	/**
	 * The constructor.
	 */
	public DefaultFactoryRegistry()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.IFactoryRegistry#getImageFactory(java.lang.Object[])
	 */
	public IImageFactory getImageFactory(final Object... objects)
	{
		/*
		 * ?? if (objects.length != 1 && !(objects[0] instanceof
		 * ViewDescriptor)) { throw new IllegalArgumentException(
		 * "Expected one parameter instance of ImageRegistry"); //$NON-NLS-1$ }
		 */
		IImageFactory imageFactory = getImageFactory();
		if (imageFactory == null)
		{
			final ImageRegistry imageRegistry = Plugin.getInstance().getImageRegistry();
			imageFactory = new ImageFactory(imageRegistry, Plugin.getDefault(),
					Plugin.ICONS_BASE_PATH);
			this.imageFactory = imageFactory;
		}
		return imageFactory;
	}


	public IImageFactory getImageFactory()
	{
		return imageFactory;
	}


	public IPaletteFactory getPaletteFactory()
	{
		if (paletteFactory == null)
		{
			paletteFactory = new PaletteFactory();
		}
		return paletteFactory;
	}


	public IFigureFactory getFigureFactory()
	{
		if (figureFactory == null)
		{
			CompositeFigureFactory newFactory = new CompositeFigureFactory();
			newFactory.addFigureFactory(new StandardFigureFactory());
			newFactory.addFigureFactory(new DefaultFigureFactory());
			
			figureFactory = newFactory;
		}
		return figureFactory;
	}


	public EditPartFactory getEditPartFactory()
	{
		if (editPartFactory == null)
		{
			editPartFactory = new JWTEditPartFactory();
		}
		return editPartFactory;
	}

}
