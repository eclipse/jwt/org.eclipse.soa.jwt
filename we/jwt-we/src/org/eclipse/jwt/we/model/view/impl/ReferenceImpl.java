/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.meta.model.core.impl.ModelElementImpl;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.model.view.ReferenceEdge;
import org.eclipse.jwt.we.model.view.ViewPackage;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.ReferenceImpl#getContainedIn <em>Contained In</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.ReferenceImpl#getReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.ReferenceImpl#getReferenceEdges <em>Reference Edges</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReferenceImpl
		extends ModelElementImpl
		implements Reference
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";

	/**
	 * The cached value of the '{@link #getContainedIn() <em>Contained In</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedIn()
	 * @generated
	 * @ordered
	 */
	protected Scope containedIn;

	/**
	 * The cached value of the '{@link #getReference() <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReference()
	 * @generated
	 * @ordered
	 */
	protected ReferenceableElement reference;

	/**
	 * The cached value of the '{@link #getReferenceEdges() <em>Reference Edges</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<ReferenceEdge> referenceEdges;


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferenceImpl()
	{
		super();
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ViewPackage.Literals.REFERENCE;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getContainedIn()
	{
		if (containedIn != null && containedIn.eIsProxy())
		{
			InternalEObject oldContainedIn = (InternalEObject) containedIn;
			containedIn = (Scope) eResolveProxy(oldContainedIn);
			if (containedIn != oldContainedIn)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ViewPackage.REFERENCE__CONTAINED_IN, oldContainedIn,
							containedIn));
			}
		}
		return containedIn;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scope basicGetContainedIn()
	{
		return containedIn;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainedIn(Scope newContainedIn)
	{
		Scope oldContainedIn = containedIn;
		containedIn = newContainedIn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.REFERENCE__CONTAINED_IN, oldContainedIn, containedIn));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceableElement getReference()
	{
		if (reference != null && reference.eIsProxy())
		{
			InternalEObject oldReference = (InternalEObject) reference;
			reference = (ReferenceableElement) eResolveProxy(oldReference);
			if (reference != oldReference)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ViewPackage.REFERENCE__REFERENCE, oldReference, reference));
			}
		}
		return reference;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceableElement basicGetReference()
	{
		return reference;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReference(ReferenceableElement newReference)
	{
		ReferenceableElement oldReference = reference;
		reference = newReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.REFERENCE__REFERENCE, oldReference, reference));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceEdge> getReferenceEdges()
	{
		if (referenceEdges == null)
		{
			referenceEdges = new EObjectWithInverseResolvingEList<ReferenceEdge>(
					ReferenceEdge.class, this, ViewPackage.REFERENCE__REFERENCE_EDGES,
					ViewPackage.REFERENCE_EDGE__REFERENCE);
		}
		return referenceEdges;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID,
			NotificationChain msgs)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE__REFERENCE_EDGES:
				return ((InternalEList<InternalEObject>) (InternalEList<?>) getReferenceEdges())
						.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID,
			NotificationChain msgs)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE__REFERENCE_EDGES:
				return ((InternalEList<?>) getReferenceEdges()).basicRemove(otherEnd,
						msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE__CONTAINED_IN:
				if (resolve)
					return getContainedIn();
				return basicGetContainedIn();
			case ViewPackage.REFERENCE__REFERENCE:
				if (resolve)
					return getReference();
				return basicGetReference();
			case ViewPackage.REFERENCE__REFERENCE_EDGES:
				return getReferenceEdges();
		}
		return super.eGet(featureID, resolve, coreType);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE__CONTAINED_IN:
				setContainedIn((Scope) newValue);
				return;
			case ViewPackage.REFERENCE__REFERENCE:
				setReference((ReferenceableElement) newValue);
				return;
			case ViewPackage.REFERENCE__REFERENCE_EDGES:
				getReferenceEdges().clear();
				getReferenceEdges()
						.addAll((Collection<? extends ReferenceEdge>) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE__CONTAINED_IN:
				setContainedIn((Scope) null);
				return;
			case ViewPackage.REFERENCE__REFERENCE:
				setReference((ReferenceableElement) null);
				return;
			case ViewPackage.REFERENCE__REFERENCE_EDGES:
				getReferenceEdges().clear();
				return;
		}
		super.eUnset(featureID);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE__CONTAINED_IN:
				return containedIn != null;
			case ViewPackage.REFERENCE__REFERENCE:
				return reference != null;
			case ViewPackage.REFERENCE__REFERENCE_EDGES:
				return referenceEdges != null && !referenceEdges.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ReferenceImpl