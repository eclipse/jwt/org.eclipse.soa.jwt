/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jwt.meta.model.core.GraphicalElement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Layout Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.we.model.view.LayoutData#getDescribesElement <em>Describes Element</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.LayoutData#getViewid <em>Viewid</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.LayoutData#getWidth <em>Width</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.LayoutData#getHeight <em>Height</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.LayoutData#getX <em>X</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.LayoutData#getY <em>Y</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.LayoutData#isInitialized <em>Initialized</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.we.model.view.ViewPackage#getLayoutData()
 * @model
 * @generated
 */
public interface LayoutData
		extends EObject
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * Returns the value of the '<em><b>Describes Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Describes Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Describes Element</em>' reference.
	 * @see #setDescribesElement(GraphicalElement)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getLayoutData_DescribesElement()
	 * @model required="true"
	 * @generated
	 */
	GraphicalElement getDescribesElement();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.LayoutData#getDescribesElement <em>Describes Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Describes Element</em>' reference.
	 * @see #getDescribesElement()
	 * @generated
	 */
	void setDescribesElement(GraphicalElement value);


	/**
	 * Returns the value of the '<em><b>Viewid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewid</em>' attribute.
	 * @see #setViewid(String)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getLayoutData_Viewid()
	 * @model
	 * @generated
	 */
	String getViewid();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.LayoutData#getViewid <em>Viewid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Viewid</em>' attribute.
	 * @see #getViewid()
	 * @generated
	 */
	void setViewid(String value);


	/**
	 * Returns the value of the '<em><b>Width</b></em>' attribute.
	 * The default value is <code>"10"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width</em>' attribute.
	 * @see #setWidth(int)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getLayoutData_Width()
	 * @model default="10"
	 * @generated
	 */
	int getWidth();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.LayoutData#getWidth <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width</em>' attribute.
	 * @see #getWidth()
	 * @generated
	 */
	void setWidth(int value);


	/**
	 * Returns the value of the '<em><b>Height</b></em>' attribute.
	 * The default value is <code>"10"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Height</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Height</em>' attribute.
	 * @see #setHeight(int)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getLayoutData_Height()
	 * @model default="10"
	 * @generated
	 */
	int getHeight();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.LayoutData#getHeight <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Height</em>' attribute.
	 * @see #getHeight()
	 * @generated
	 */
	void setHeight(int value);


	/**
	 * Returns the value of the '<em><b>X</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>X</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' attribute.
	 * @see #setX(int)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getLayoutData_X()
	 * @model default="0"
	 * @generated
	 */
	int getX();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.LayoutData#getX <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' attribute.
	 * @see #getX()
	 * @generated
	 */
	void setX(int value);


	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Y</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see #setY(int)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getLayoutData_Y()
	 * @model default="0"
	 * @generated
	 */
	int getY();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.LayoutData#getY <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' attribute.
	 * @see #getY()
	 * @generated
	 */
	void setY(int value);


	/**
	 * Returns the value of the '<em><b>Initialized</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initialized</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initialized</em>' attribute.
	 * @see #setInitialized(boolean)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getLayoutData_Initialized()
	 * @model default="false"
	 * @generated
	 */
	boolean isInitialized();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.LayoutData#isInitialized <em>Initialized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initialized</em>' attribute.
	 * @see #isInitialized()
	 * @generated
	 */
	void setInitialized(boolean value);

} // LayoutData