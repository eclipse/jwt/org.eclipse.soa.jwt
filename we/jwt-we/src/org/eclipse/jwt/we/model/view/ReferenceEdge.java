/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view;

import org.eclipse.jwt.meta.model.core.ModelElement;
import org.eclipse.jwt.meta.model.processes.Action;
import org.eclipse.jwt.meta.model.processes.Scope;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getContainedIn <em>Contained In</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getAction <em>Action</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getDirection <em>Direction</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.we.model.view.ViewPackage#getReferenceEdge()
 * @model
 * @generated
 */
public interface ReferenceEdge
		extends ModelElement
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * Returns the value of the '<em><b>Contained In</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained In</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained In</em>' reference.
	 * @see #setContainedIn(Scope)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getReferenceEdge_ContainedIn()
	 * @model required="true"
	 * @generated
	 */
	Scope getContainedIn();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getContainedIn <em>Contained In</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contained In</em>' reference.
	 * @see #getContainedIn()
	 * @generated
	 */
	void setContainedIn(Scope value);


	/**
	 * Returns the value of the '<em><b>Reference</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.jwt.we.model.view.Reference#getReferenceEdges <em>Reference Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' reference.
	 * @see #setReference(Reference)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getReferenceEdge_Reference()
	 * @see org.eclipse.jwt.we.model.view.Reference#getReferenceEdges
	 * @model opposite="referenceEdges" required="true"
	 * @generated
	 */
	Reference getReference();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getReference <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' reference.
	 * @see #getReference()
	 * @generated
	 */
	void setReference(Reference value);


	/**
	 * Returns the value of the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' reference.
	 * @see #setAction(Action)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getReferenceEdge_Action()
	 * @model required="true"
	 * @generated
	 */
	Action getAction();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getAction <em>Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(Action value);


	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * The literals are from the enumeration {@link org.eclipse.jwt.we.model.view.EdgeDirection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see org.eclipse.jwt.we.model.view.EdgeDirection
	 * @see #setDirection(EdgeDirection)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getReferenceEdge_Direction()
	 * @model default="0"
	 * @generated
	 */
	EdgeDirection getDirection();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see org.eclipse.jwt.we.model.view.EdgeDirection
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(EdgeDirection value);

} // ReferenceEdge