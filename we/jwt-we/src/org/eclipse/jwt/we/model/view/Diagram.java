/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jwt.meta.model.core.Model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Diagram</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.we.model.view.Diagram#getDescribesModel <em>Describes Model</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.Diagram#getLayoutData <em>Layout Data</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.Diagram#getReferences <em>References</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.Diagram#getReferenceEdges <em>Reference Edges</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.we.model.view.ViewPackage#getDiagram()
 * @model
 * @generated
 */
public interface Diagram
		extends EObject
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * Returns the value of the '<em><b>Describes Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Describes Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Describes Model</em>' reference.
	 * @see #setDescribesModel(Model)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getDiagram_DescribesModel()
	 * @model required="true"
	 * @generated
	 */
	Model getDescribesModel();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.Diagram#getDescribesModel <em>Describes Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Describes Model</em>' reference.
	 * @see #getDescribesModel()
	 * @generated
	 */
	void setDescribesModel(Model value);


	/**
	 * Returns the value of the '<em><b>Layout Data</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.jwt.we.model.view.LayoutData}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layout Data</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layout Data</em>' containment reference list.
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getDiagram_LayoutData()
	 * @model containment="true"
	 * @generated
	 */
	EList<LayoutData> getLayoutData();


	/**
	 * Returns the value of the '<em><b>References</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.jwt.we.model.view.Reference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>References</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References</em>' containment reference list.
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getDiagram_References()
	 * @model containment="true"
	 * @generated
	 */
	EList<Reference> getReferences();


	/**
	 * Returns the value of the '<em><b>Reference Edges</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.jwt.we.model.view.ReferenceEdge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Edges</em>' containment reference list.
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getDiagram_ReferenceEdges()
	 * @model containment="true"
	 * @generated
	 */
	EList<ReferenceEdge> getReferenceEdges();

} // Diagram