/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.jwt.meta.model.core.impl.ModelElementImpl;
import org.eclipse.jwt.meta.model.processes.Action;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.model.view.EdgeDirection;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.model.view.ReferenceEdge;
import org.eclipse.jwt.we.model.view.ViewPackage;


/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Reference Edge</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.ReferenceEdgeImpl#getContainedIn <em>Contained In</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.ReferenceEdgeImpl#getReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.ReferenceEdgeImpl#getAction <em>Action</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.ReferenceEdgeImpl#getDirection <em>Direction</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReferenceEdgeImpl
		extends ModelElementImpl
		implements ReferenceEdge
{

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";

	/**
	 * The cached value of the '{@link #getContainedIn() <em>Contained In</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getContainedIn()
	 * @generated
	 * @ordered
	 */
	protected Scope containedIn;

	/**
	 * The cached value of the '{@link #getReference() <em>Reference</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getReference()
	 * @generated
	 * @ordered
	 */
	protected Reference reference;

	/**
	 * The cached value of the '{@link #getAction() <em>Action</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected Action action;

	/**
	 * The default value of the '{@link #getDirection() <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected static final EdgeDirection DIRECTION_EDEFAULT = EdgeDirection.DEFAULT;

	/**
	 * The cached value of the '{@link #getDirection() <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected EdgeDirection direction = DIRECTION_EDEFAULT;


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferenceEdgeImpl()
	{
		super();
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ViewPackage.Literals.REFERENCE_EDGE;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Scope getContainedIn()
	{
		if (containedIn != null && containedIn.eIsProxy())
		{
			InternalEObject oldContainedIn = (InternalEObject) containedIn;
			containedIn = (Scope) eResolveProxy(oldContainedIn);
			if (containedIn != oldContainedIn)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ViewPackage.REFERENCE_EDGE__CONTAINED_IN, oldContainedIn,
							containedIn));
			}
		}
		return containedIn;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Scope basicGetContainedIn()
	{
		return containedIn;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainedIn(Scope newContainedIn)
	{
		Scope oldContainedIn = containedIn;
		containedIn = newContainedIn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.REFERENCE_EDGE__CONTAINED_IN, oldContainedIn, containedIn));
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Reference getReference()
	{
		if (reference != null && reference.eIsProxy())
		{
			InternalEObject oldReference = (InternalEObject) reference;
			reference = (Reference) eResolveProxy(oldReference);
			if (reference != oldReference)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ViewPackage.REFERENCE_EDGE__REFERENCE, oldReference,
							reference));
			}
		}
		return reference;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Reference basicGetReference()
	{
		return reference;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReference(Reference newReference,
			NotificationChain msgs)
	{
		Reference oldReference = reference;
		reference = newReference;
		if (eNotificationRequired())
		{
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, ViewPackage.REFERENCE_EDGE__REFERENCE,
					oldReference, newReference);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setReference(Reference newReference)
	{
		if (newReference != reference)
		{
			NotificationChain msgs = null;
			if (reference != null)
				msgs = ((InternalEObject) reference).eInverseRemove(this,
						ViewPackage.REFERENCE__REFERENCE_EDGES, Reference.class, msgs);
			if (newReference != null)
				msgs = ((InternalEObject) newReference).eInverseAdd(this,
						ViewPackage.REFERENCE__REFERENCE_EDGES, Reference.class, msgs);
			msgs = basicSetReference(newReference, msgs);
			if (msgs != null)
				msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.REFERENCE_EDGE__REFERENCE, newReference, newReference));
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Action getAction()
	{
		if (action != null && action.eIsProxy())
		{
			InternalEObject oldAction = (InternalEObject) action;
			action = (Action) eResolveProxy(oldAction);
			if (action != oldAction)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ViewPackage.REFERENCE_EDGE__ACTION, oldAction, action));
			}
		}
		return action;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Action basicGetAction()
	{
		return action;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setAction(Action newAction)
	{
		Action oldAction = action;
		action = newAction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.REFERENCE_EDGE__ACTION, oldAction, action));
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EdgeDirection getDirection()
	{
		return direction;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirection(EdgeDirection newDirection)
	{
		EdgeDirection oldDirection = direction;
		direction = newDirection == null ? DIRECTION_EDEFAULT : newDirection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.REFERENCE_EDGE__DIRECTION, oldDirection, direction));
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID,
			NotificationChain msgs)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE_EDGE__REFERENCE:
				if (reference != null)
					msgs = ((InternalEObject) reference)
							.eInverseRemove(this, ViewPackage.REFERENCE__REFERENCE_EDGES,
									Reference.class, msgs);
				return basicSetReference((Reference) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID,
			NotificationChain msgs)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE_EDGE__REFERENCE:
				return basicSetReference(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE_EDGE__CONTAINED_IN:
				if (resolve)
					return getContainedIn();
				return basicGetContainedIn();
			case ViewPackage.REFERENCE_EDGE__REFERENCE:
				if (resolve)
					return getReference();
				return basicGetReference();
			case ViewPackage.REFERENCE_EDGE__ACTION:
				if (resolve)
					return getAction();
				return basicGetAction();
			case ViewPackage.REFERENCE_EDGE__DIRECTION:
				return getDirection();
		}
		return super.eGet(featureID, resolve, coreType);
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE_EDGE__CONTAINED_IN:
				setContainedIn((Scope) newValue);
				return;
			case ViewPackage.REFERENCE_EDGE__REFERENCE:
				setReference((Reference) newValue);
				return;
			case ViewPackage.REFERENCE_EDGE__ACTION:
				setAction((Action) newValue);
				return;
			case ViewPackage.REFERENCE_EDGE__DIRECTION:
				setDirection((EdgeDirection) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE_EDGE__CONTAINED_IN:
				setContainedIn((Scope) null);
				return;
			case ViewPackage.REFERENCE_EDGE__REFERENCE:
				setReference((Reference) null);
				return;
			case ViewPackage.REFERENCE_EDGE__ACTION:
				setAction((Action) null);
				return;
			case ViewPackage.REFERENCE_EDGE__DIRECTION:
				setDirection(DIRECTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case ViewPackage.REFERENCE_EDGE__CONTAINED_IN:
				return containedIn != null;
			case ViewPackage.REFERENCE_EDGE__REFERENCE:
				return reference != null;
			case ViewPackage.REFERENCE_EDGE__ACTION:
				return action != null;
			case ViewPackage.REFERENCE_EDGE__DIRECTION:
				return direction != DIRECTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (direction: "); //$NON-NLS-1$
		result.append(direction);
		result.append(')');
		return result.toString();
	}

} // ReferenceEdgeImpl