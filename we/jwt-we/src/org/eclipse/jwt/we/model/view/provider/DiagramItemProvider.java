/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.commands.view.AddReferenceCommand;
import org.eclipse.jwt.we.commands.view.RemoveLayoutDataCommand;
import org.eclipse.jwt.we.commands.view.RemoveReferenceCommand;
import org.eclipse.jwt.we.commands.view.RemoveReferenceEdgeCommand;
import org.eclipse.jwt.we.model.view.Diagram;
import org.eclipse.jwt.we.model.view.LayoutData;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.model.view.ReferenceEdge;
import org.eclipse.jwt.we.model.view.ViewPackage;

/**
 * This is the item provider adapter for a
 * {@link org.eclipse.jwt.we.model.view.Diagram} object. <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class DiagramItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public DiagramItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

			addDescribesModelPropertyDescriptor(object);
			addLayoutDataPropertyDescriptor(object);
			addReferencesPropertyDescriptor(object);
			addReferenceEdgesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}


	/**
	 * This adds a property descriptor for the Describes Model feature. [JWT]
	 * Edit (by Wolf Fischer): The implementation has been changed to check if
	 * this feature should be shown or not (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors
	 * list on every call and only if it should be shown, added to the list
	 * again! <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDescribesModelPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.DIAGRAM__DESCRIBES_MODEL)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.DIAGRAM__DESCRIBES_MODEL))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.DIAGRAM__DESCRIBES_MODEL),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.DIAGRAM__DESCRIBES_MODEL),

							ViewPackage.Literals.DIAGRAM__DESCRIBES_MODEL, false, false,
							true, null, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Layout Data feature. [JWT] Edit
	 * (by Wolf Fischer): The implementation has been changed to check if this
	 * feature should be shown or not (depending on the selected view). This
	 * means, that the feature is deleted from the itemPropertyDescriptors list
	 * on every call and only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addLayoutDataPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.DIAGRAM__LAYOUT_DATA)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.DIAGRAM__LAYOUT_DATA))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.DIAGRAM__LAYOUT_DATA),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.DIAGRAM__LAYOUT_DATA),

							ViewPackage.Literals.DIAGRAM__LAYOUT_DATA, false, false,
							false, null, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the References feature. [JWT] Edit
	 * (by Wolf Fischer): The implementation has been changed to check if this
	 * feature should be shown or not (depending on the selected view). This
	 * means, that the feature is deleted from the itemPropertyDescriptors list
	 * on every call and only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addReferencesPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.DIAGRAM__REFERENCES)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.DIAGRAM__REFERENCES))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.DIAGRAM__REFERENCES),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.DIAGRAM__REFERENCES),

							ViewPackage.Literals.DIAGRAM__REFERENCES, false, false,
							false, null, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Reference Edges feature. [JWT]
	 * Edit (by Wolf Fischer): The implementation has been changed to check if
	 * this feature should be shown or not (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors
	 * list on every call and only if it should be shown, added to the list
	 * again! <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addReferenceEdgesPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.DIAGRAM__REFERENCE_EDGES)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.DIAGRAM__REFERENCE_EDGES))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.DIAGRAM__REFERENCE_EDGES),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.DIAGRAM__REFERENCE_EDGES),

							ViewPackage.Literals.DIAGRAM__REFERENCE_EDGES, false, false,
							false, null, null, null));
		}
	}


	/**
	 * This specifies how to implement {@link #getChildren(Object)} and is used
	 * to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand},
	 * {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in createCommand(). <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param object
	 * @return Collection
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(ViewPackage.Literals.DIAGRAM__LAYOUT_DATA);
			childrenFeatures.add(ViewPackage.Literals.DIAGRAM__REFERENCES);
			childrenFeatures.add(ViewPackage.Literals.DIAGRAM__REFERENCE_EDGES);
		}
		return childrenFeatures;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper
		// feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}


	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public String getText(Object object)
	{
		String type = PluginProperties.model_type(object);
		return PluginProperties.model_text(ViewPackage.Literals.DIAGRAM, new Object[]
		{ type });
	}


	/**
	 * This handles model notifications by calling
	 * {@link #updateChildren(Notification)} to update any cached children and
	 * by creating a viewer notification, which it passes to
	 * {@link #fireNotifyChanged(Notification)}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param notification
	 * @generated NOT
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(Diagram.class))
		{
			case ViewPackage.DIAGRAM__LAYOUT_DATA:
			case ViewPackage.DIAGRAM__REFERENCES:
			case ViewPackage.DIAGRAM__REFERENCE_EDGES:
				fireNotifyChanged(new ViewerNotification(notification,
						notification.getNotifier(), true, false));

				// collect all elements that are affected by this notification
				Collection affectedElements = new ArrayList();

				if (notification.getNewValue() instanceof Collection)
				{
					affectedElements.addAll(((Collection) notification.getNewValue()));
				}
				else if (notification.getNewValue() != null)
				{
					affectedElements.add(notification.getNewValue());
				}

				if (notification.getOldValue() instanceof Collection)
				{
					affectedElements.addAll(((Collection) notification.getOldValue()));
				}
				else if (notification.getOldValue() != null)
				{
					affectedElements.add(notification.getOldValue());
				}

				// notify corresponding elements so they can update themselves
				for (Object object : affectedElements)
				{
					// in the case of referenceedge
					if (object instanceof ReferenceEdge
							&& ((ReferenceEdge) object).getContainedIn() != null)
					{
						((ReferenceEdge) object).getContainedIn().eNotify(notification);
						((ReferenceEdge) object).getAction().eNotify(notification);
					}
					// in the case of reference
					if (object instanceof Reference
							&& ((Reference) object).getContainedIn() != null)
					{
						// notify reference container
						((Reference) object).getContainedIn().eNotify(notification);
					}
					// in the case of layoutdata
					if (object instanceof LayoutData
							&& ((LayoutData) object).getDescribesElement() != null)
					{
						// notify layoutdata container editpart (the
						// corresponding node editpart is notified in
						// layoutdataitemprovider)
						((LayoutData) object).getDescribesElement().eNotify(notification);
					}

				}

				return;
		}
		super.notifyChanged(notification);
	}


	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		/*
		 * super.collectNewChildDescriptors(newChildDescriptors, object);
		 * 
		 * newChildDescriptors.add(createChildParameter(
		 * ViewPackage.Literals.DIAGRAM__LAYOUT_DATA, ViewFactory.eINSTANCE
		 * .createLayoutData()));
		 * 
		 * newChildDescriptors.add(createChildParameter(
		 * ViewPackage.Literals.DIAGRAM__REFERENCES, ViewFactory.eINSTANCE
		 * .createReference()));
		 * 
		 * newChildDescriptors.add(createChildParameter(
		 * ViewPackage.Literals.DIAGRAM__REFERENCE_EDGES, ViewFactory.eINSTANCE
		 * .createReferenceEdge()));
		 */
	}


	/**
	 * Return an ImageDescriptor that describes the model. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @param object
	 * @return The ImageDescriptor.
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object)
	{
		return null;
	}


	/**
	 * Return the type image for creation of childs. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return The type image descriptor.
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getCreateChildImage(java.lang.Object,
	 *      java.lang.Object, java.lang.Object, java.util.Collection)
	 * @generated NOT
	 */
	@Override
	public Object getCreateChildImage(Object owner, Object feature, Object child,
			Collection selection)
	{
		return null;
	}


	/**
	 * Return the resource locator for this item provider's resources. Edit (by
	 * Marc Dutoo) : uses jwt plugin instance as resource locator and not <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator()
	{
		return Plugin.getInstance(); // [JWT]
	}


	/**
	 * This looks up the name of the type of the specified object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param object
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getTypeText(Object object)
	{
		return PluginProperties.model_type(object);
	}


	/**
	 * This looks up the name of the type of the specified attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param attribute
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getTypeText(EAttribute attribute)
	{
		return PluginProperties.model_datatype(attribute);
	}


	/**
	 * This looks up the name of the specified feature. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param feature
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getFeatureText(Object feature)
	{
		return PluginProperties.model_feature_name(feature);
	}


	/**
	 * This returns the description for CreateChildCommand. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return The description.
	 * @generated
	 */
	@Override
	public String getCreateChildDescription(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		Object selectionObject = selection == null || selection.isEmpty() ? null
				: selection.iterator().next();

		if (selectionObject == owner)
		{
			return PluginProperties
					.command_CreateChild_description(child, feature, owner);
		}

		Object sibling = selectionObject;
		Object siblingFeature = getChildFeature(owner, sibling);

		if (siblingFeature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) siblingFeature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) sibling;
			feature = entry.getEStructuralFeature();
			sibling = entry.getValue();
		}

		return PluginProperties
				.command_CreateSibling_description(child, feature, sibling);
	}


	/**
	 * This returns the tool tip text for CreateChildCommand. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return The description.
	 * @generated
	 */
	@Override
	public String getCreateChildToolTipText(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		return PluginProperties.command_CreateChild_tooltip(child, feature, owner);
	}


	/**
	 * This method overrides the ITreeItemContentProvider.getChildren to select
	 * which Objects may be shown in the outline depending on the view!
	 * 
	 * @author Wolf Fischer
	 * @generated NOT
	 */
	@Override
	public Collection getChildren(Object object)
	{
		return new ArrayList();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.edit.provider.ItemProviderAdapter#createRemoveCommand
	 * (org.eclipse .emf.edit.domain.EditingDomain,
	 * org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature,
	 * java.util.Collection)
	 */
	@Override
	protected Command createRemoveCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection collection)
	{
		// custom remove reference command
		if (feature == ViewPackage.Literals.DIAGRAM__REFERENCES)
		{
			return new RemoveReferenceCommand(domain, owner, feature, collection);
		}

		// custom remove referenceedge command
		if (feature == ViewPackage.Literals.DIAGRAM__REFERENCE_EDGES)
		{
			return new RemoveReferenceEdgeCommand(domain, owner, feature, collection);
		}

		// custom remove layout data command
		if (feature == ViewPackage.Literals.DIAGRAM__LAYOUT_DATA)
		{
			return new RemoveLayoutDataCommand(domain, owner, feature, collection);
		}

		return super.createRemoveCommand(domain, owner, feature, collection);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.edit.provider.ItemProviderAdapter#createAddCommand(org
	 * .eclipse. emf.edit.domain.EditingDomain, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection, int)
	 */
	@Override
	protected Command createAddCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection collection, int index)
	{
		// custom add reference command
		if (feature == ViewPackage.Literals.DIAGRAM__REFERENCES)
		{
			return new AddReferenceCommand(domain, owner, feature, collection, index);
		}
		return super.createAddCommand(domain, owner, feature, collection, index);
	}
}
