/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.meta.model.core.provider.ModelElementItemProvider;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.model.view.ViewPackage;

/**
 * This is the item provider adapter for a
 * {@link org.eclipse.jwt.we.model.view.Reference} object. <!-- begin-user-doc
 * --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class ReferenceItemProvider extends ModelElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ReferenceItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

			addContainedInPropertyDescriptor(object);
			addReferencePropertyDescriptor(object);
			addReferenceEdgesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}


	/**
	 * This adds a property descriptor for the Contained In feature. [JWT] Edit
	 * (by Wolf Fischer): The implementation has been changed to check if this
	 * feature should be shown or not (depending on the selected view). This
	 * means, that the feature is deleted from the itemPropertyDescriptors list
	 * on every call and only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addContainedInPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.REFERENCE__CONTAINED_IN)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.REFERENCE__CONTAINED_IN))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.REFERENCE__CONTAINED_IN),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.REFERENCE__CONTAINED_IN),

							ViewPackage.Literals.REFERENCE__CONTAINED_IN, false, false,
							true, null, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Reference feature. [JWT] Edit (by
	 * Wolf Fischer): The implementation has been changed to check if this
	 * feature should be shown or not (depending on the selected view). This
	 * means, that the feature is deleted from the itemPropertyDescriptors list
	 * on every call and only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addReferencePropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.REFERENCE__REFERENCE)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.REFERENCE__REFERENCE))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.REFERENCE__REFERENCE),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.REFERENCE__REFERENCE),

							ViewPackage.Literals.REFERENCE__REFERENCE, false, false,
							true, null, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Reference Edges feature. [JWT]
	 * Edit (by Wolf Fischer): The implementation has been changed to check if
	 * this feature should be shown or not (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors
	 * list on every call and only if it should be shown, added to the list
	 * again! <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addReferenceEdgesPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.REFERENCE__REFERENCE_EDGES)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.REFERENCE__REFERENCE_EDGES))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.REFERENCE__REFERENCE_EDGES),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.REFERENCE__REFERENCE_EDGES),

							ViewPackage.Literals.REFERENCE__REFERENCE_EDGES, false,
							false, true, null, null, null));
		}
	}


	/**
	 * This handles model notifications by calling
	 * {@link #updateChildren(Notification)} to update any cached children and
	 * by creating a viewer notification, which it passes to
	 * {@link #fireNotifyChanged(Notification)}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param notification
	 * @generated NOT
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		super.notifyChanged(notification);
	}


	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		// super.collectNewChildDescriptors(newChildDescriptors, object);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.model.core.provider.ModelElementItemProvider#getText
	 * (java.lang .Object)
	 * 
	 * generated NOT
	 */
	@Override
	public String getText(Object object)
	{
		String type = PluginProperties.model_type(object);
		ReferenceableElement element = ((Reference) object).getReference();

		String referenceText = null;
		if (element != null)
		{
			AdapterFactoryEditingDomain adapterFactoryEditingDomain = (AdapterFactoryEditingDomain) AdapterFactoryEditingDomain
					.getEditingDomainFor(element);
			AdapterFactory adapterFactory = adapterFactoryEditingDomain
					.getAdapterFactory();

			IItemLabelProvider itemLabelProvider = (IItemLabelProvider) adapterFactory
					.adapt(element, IItemLabelProvider.class);
			if (itemLabelProvider != null)
			{
				referenceText = itemLabelProvider.getText(element);
			}
		}

		return PluginProperties.model_text(Reference.class, new Object[]
		{ type, referenceText });
	}


	/**
	 * Returns the image of the referenced element.
	 * 
	 * @see org.eclipse.jwt.we.model_ext.core.provider.ModelElementItemProvider#getImage(java.lang.Object)
	 *      generated NOT
	 */
	@Override
	public Object getImage(Object object)
	{
		Object image = null;
		ReferenceableElement element = ((Reference) object).getReference();

		if (element != null)
		{
			AdapterFactoryEditingDomain adapterFactoryEditingDomain = (AdapterFactoryEditingDomain) AdapterFactoryEditingDomain
					.getEditingDomainFor(element);
			AdapterFactory adapterFactory = adapterFactoryEditingDomain
					.getAdapterFactory();

			IItemLabelProvider itemLabelProvider = (IItemLabelProvider) adapterFactory
					.adapt(element, IItemLabelProvider.class);
			if (itemLabelProvider != null)
			{
				image = itemLabelProvider.getImage(element);
			}
		}

		if (image == null)
		{
			image = super.getImage(object);
		}

		// TODO add a link decoration to the image
		return image;
	}


	/**
	 * Return the type image for creation of childs. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return The type image descriptor.
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getCreateChildImage(java.lang.Object,
	 *      java.lang.Object, java.lang.Object, java.util.Collection)
	 * @generated
	 */
	@Override
	public Object getCreateChildImage(Object owner, Object feature, Object child,
			Collection selection)
	{
		return ResourceProviderRegistry.getInstance().createImageDescriptor(child);
	}


	/**
	 * Return the resource locator for this item provider's resources. Edit (by
	 * Marc Dutoo) : uses jwt plugin instance as resource locator and not <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator()
	{
		return Plugin.getInstance(); // [JWT]
	}


	/**
	 * This looks up the name of the type of the specified object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param object
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getTypeText(Object object)
	{
		return PluginProperties.model_type(object);
	}


	/**
	 * This looks up the name of the type of the specified attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param attribute
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getTypeText(EAttribute attribute)
	{
		return PluginProperties.model_datatype(attribute);
	}


	/**
	 * This looks up the name of the specified feature. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param feature
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getFeatureText(Object feature)
	{
		return PluginProperties.model_feature_name(feature);
	}


	/**
	 * This returns the description for CreateChildCommand. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return The description.
	 * @generated
	 */
	@Override
	public String getCreateChildDescription(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		Object selectionObject = selection == null || selection.isEmpty() ? null
				: selection.iterator().next();

		if (selectionObject == owner)
		{
			return PluginProperties
					.command_CreateChild_description(child, feature, owner);
		}

		Object sibling = selectionObject;
		Object siblingFeature = getChildFeature(owner, sibling);

		if (siblingFeature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) siblingFeature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) sibling;
			feature = entry.getEStructuralFeature();
			sibling = entry.getValue();
		}

		return PluginProperties
				.command_CreateSibling_description(child, feature, sibling);
	}


	/**
	 * This returns the tool tip text for CreateChildCommand. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return The description.
	 * @generated
	 */
	@Override
	public String getCreateChildToolTipText(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		return PluginProperties.command_CreateChild_tooltip(child, feature, owner);
	}


	/**
	 * This method overrides the ITreeItemContentProvider.getChildren to select
	 * which Objects may be shown in the outline depending on the view!
	 * 
	 * @author Wolf Fischer
	 * @generated NOT
	 */
	@Override
	public Collection getChildren(Object object)
	{
		return new ArrayList();
	}

}
