/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.jwt.meta.model.core.CorePackage;


/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.jwt.we.model.view.ViewFactory
 * @model kind="package"
 * @generated
 */
public interface ViewPackage
		extends EPackage
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>"; //$NON-NLS-1$

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "view"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "org.eclipse.jwt/view"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "view"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ViewPackage eINSTANCE = org.eclipse.jwt.we.model.view.impl.ViewPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.we.model.view.impl.LayoutDataImpl <em>Layout Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.we.model.view.impl.LayoutDataImpl
	 * @see org.eclipse.jwt.we.model.view.impl.ViewPackageImpl#getLayoutData()
	 * @generated
	 */
	int LAYOUT_DATA = 0;

	/**
	 * The feature id for the '<em><b>Describes Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_DATA__DESCRIBES_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Viewid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_DATA__VIEWID = 1;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_DATA__WIDTH = 2;

	/**
	 * The feature id for the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_DATA__HEIGHT = 3;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_DATA__X = 4;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_DATA__Y = 5;

	/**
	 * The feature id for the '<em><b>Initialized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_DATA__INITIALIZED = 6;

	/**
	 * The number of structural features of the '<em>Layout Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_DATA_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.we.model.view.impl.ReferenceImpl <em>Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.we.model.view.impl.ReferenceImpl
	 * @see org.eclipse.jwt.we.model.view.impl.ViewPackageImpl#getReference()
	 * @generated
	 */
	int REFERENCE = 1;

	/**
	 * The feature id for the '<em><b>Owned Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE__OWNED_COMMENT = CorePackage.MODEL_ELEMENT__OWNED_COMMENT;

	/**
	 * The feature id for the '<em><b>Contained In</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE__CONTAINED_IN = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE__REFERENCE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Reference Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE__REFERENCE_EDGES = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.we.model.view.impl.ReferenceEdgeImpl <em>Reference Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.we.model.view.impl.ReferenceEdgeImpl
	 * @see org.eclipse.jwt.we.model.view.impl.ViewPackageImpl#getReferenceEdge()
	 * @generated
	 */
	int REFERENCE_EDGE = 2;

	/**
	 * The feature id for the '<em><b>Owned Comment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_EDGE__OWNED_COMMENT = CorePackage.MODEL_ELEMENT__OWNED_COMMENT;

	/**
	 * The feature id for the '<em><b>Contained In</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_EDGE__CONTAINED_IN = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_EDGE__REFERENCE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_EDGE__ACTION = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_EDGE__DIRECTION = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Reference Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_EDGE_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.we.model.view.impl.DiagramImpl <em>Diagram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.we.model.view.impl.DiagramImpl
	 * @see org.eclipse.jwt.we.model.view.impl.ViewPackageImpl#getDiagram()
	 * @generated
	 */
	int DIAGRAM = 3;

	/**
	 * The feature id for the '<em><b>Describes Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__DESCRIBES_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Layout Data</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__LAYOUT_DATA = 1;

	/**
	 * The feature id for the '<em><b>References</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__REFERENCES = 2;

	/**
	 * The feature id for the '<em><b>Reference Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__REFERENCE_EDGES = 3;

	/**
	 * The number of structural features of the '<em>Diagram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.eclipse.jwt.we.model.view.EdgeDirection <em>Edge Direction</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.jwt.we.model.view.EdgeDirection
	 * @see org.eclipse.jwt.we.model.view.impl.ViewPackageImpl#getEdgeDirection()
	 * @generated
	 */
	int EDGE_DIRECTION = 4;


	/**
	 * Returns the meta object for class '{@link org.eclipse.jwt.we.model.view.LayoutData <em>Layout Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Layout Data</em>'.
	 * @see org.eclipse.jwt.we.model.view.LayoutData
	 * @generated
	 */
	EClass getLayoutData();


	/**
	 * Returns the meta object for the reference '{@link org.eclipse.jwt.we.model.view.LayoutData#getDescribesElement <em>Describes Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Describes Element</em>'.
	 * @see org.eclipse.jwt.we.model.view.LayoutData#getDescribesElement()
	 * @see #getLayoutData()
	 * @generated
	 */
	EReference getLayoutData_DescribesElement();


	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.we.model.view.LayoutData#getViewid <em>Viewid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Viewid</em>'.
	 * @see org.eclipse.jwt.we.model.view.LayoutData#getViewid()
	 * @see #getLayoutData()
	 * @generated
	 */
	EAttribute getLayoutData_Viewid();


	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.we.model.view.LayoutData#getWidth <em>Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Width</em>'.
	 * @see org.eclipse.jwt.we.model.view.LayoutData#getWidth()
	 * @see #getLayoutData()
	 * @generated
	 */
	EAttribute getLayoutData_Width();


	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.we.model.view.LayoutData#getHeight <em>Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Height</em>'.
	 * @see org.eclipse.jwt.we.model.view.LayoutData#getHeight()
	 * @see #getLayoutData()
	 * @generated
	 */
	EAttribute getLayoutData_Height();


	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.we.model.view.LayoutData#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see org.eclipse.jwt.we.model.view.LayoutData#getX()
	 * @see #getLayoutData()
	 * @generated
	 */
	EAttribute getLayoutData_X();


	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.we.model.view.LayoutData#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see org.eclipse.jwt.we.model.view.LayoutData#getY()
	 * @see #getLayoutData()
	 * @generated
	 */
	EAttribute getLayoutData_Y();


	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.we.model.view.LayoutData#isInitialized <em>Initialized</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initialized</em>'.
	 * @see org.eclipse.jwt.we.model.view.LayoutData#isInitialized()
	 * @see #getLayoutData()
	 * @generated
	 */
	EAttribute getLayoutData_Initialized();


	/**
	 * Returns the meta object for class '{@link org.eclipse.jwt.we.model.view.Reference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference</em>'.
	 * @see org.eclipse.jwt.we.model.view.Reference
	 * @generated
	 */
	EClass getReference();


	/**
	 * Returns the meta object for the reference '{@link org.eclipse.jwt.we.model.view.Reference#getContainedIn <em>Contained In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Contained In</em>'.
	 * @see org.eclipse.jwt.we.model.view.Reference#getContainedIn()
	 * @see #getReference()
	 * @generated
	 */
	EReference getReference_ContainedIn();


	/**
	 * Returns the meta object for the reference '{@link org.eclipse.jwt.we.model.view.Reference#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference</em>'.
	 * @see org.eclipse.jwt.we.model.view.Reference#getReference()
	 * @see #getReference()
	 * @generated
	 */
	EReference getReference_Reference();


	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.jwt.we.model.view.Reference#getReferenceEdges <em>Reference Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Reference Edges</em>'.
	 * @see org.eclipse.jwt.we.model.view.Reference#getReferenceEdges()
	 * @see #getReference()
	 * @generated
	 */
	EReference getReference_ReferenceEdges();


	/**
	 * Returns the meta object for class '{@link org.eclipse.jwt.we.model.view.ReferenceEdge <em>Reference Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Edge</em>'.
	 * @see org.eclipse.jwt.we.model.view.ReferenceEdge
	 * @generated
	 */
	EClass getReferenceEdge();


	/**
	 * Returns the meta object for the reference '{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getContainedIn <em>Contained In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Contained In</em>'.
	 * @see org.eclipse.jwt.we.model.view.ReferenceEdge#getContainedIn()
	 * @see #getReferenceEdge()
	 * @generated
	 */
	EReference getReferenceEdge_ContainedIn();


	/**
	 * Returns the meta object for the reference '{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference</em>'.
	 * @see org.eclipse.jwt.we.model.view.ReferenceEdge#getReference()
	 * @see #getReferenceEdge()
	 * @generated
	 */
	EReference getReferenceEdge_Reference();


	/**
	 * Returns the meta object for the reference '{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Action</em>'.
	 * @see org.eclipse.jwt.we.model.view.ReferenceEdge#getAction()
	 * @see #getReferenceEdge()
	 * @generated
	 */
	EReference getReferenceEdge_Action();


	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see org.eclipse.jwt.we.model.view.ReferenceEdge#getDirection()
	 * @see #getReferenceEdge()
	 * @generated
	 */
	EAttribute getReferenceEdge_Direction();


	/**
	 * Returns the meta object for class '{@link org.eclipse.jwt.we.model.view.Diagram <em>Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Diagram</em>'.
	 * @see org.eclipse.jwt.we.model.view.Diagram
	 * @generated
	 */
	EClass getDiagram();


	/**
	 * Returns the meta object for the reference '{@link org.eclipse.jwt.we.model.view.Diagram#getDescribesModel <em>Describes Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Describes Model</em>'.
	 * @see org.eclipse.jwt.we.model.view.Diagram#getDescribesModel()
	 * @see #getDiagram()
	 * @generated
	 */
	EReference getDiagram_DescribesModel();


	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.jwt.we.model.view.Diagram#getLayoutData <em>Layout Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Layout Data</em>'.
	 * @see org.eclipse.jwt.we.model.view.Diagram#getLayoutData()
	 * @see #getDiagram()
	 * @generated
	 */
	EReference getDiagram_LayoutData();


	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.jwt.we.model.view.Diagram#getReferences <em>References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>References</em>'.
	 * @see org.eclipse.jwt.we.model.view.Diagram#getReferences()
	 * @see #getDiagram()
	 * @generated
	 */
	EReference getDiagram_References();


	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.jwt.we.model.view.Diagram#getReferenceEdges <em>Reference Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reference Edges</em>'.
	 * @see org.eclipse.jwt.we.model.view.Diagram#getReferenceEdges()
	 * @see #getDiagram()
	 * @generated
	 */
	EReference getDiagram_ReferenceEdges();


	/**
	 * Returns the meta object for enum '{@link org.eclipse.jwt.we.model.view.EdgeDirection <em>Edge Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Edge Direction</em>'.
	 * @see org.eclipse.jwt.we.model.view.EdgeDirection
	 * @generated
	 */
	EEnum getEdgeDirection();


	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ViewFactory getViewFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals
	{

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.we.model.view.impl.LayoutDataImpl <em>Layout Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.we.model.view.impl.LayoutDataImpl
		 * @see org.eclipse.jwt.we.model.view.impl.ViewPackageImpl#getLayoutData()
		 * @generated
		 */

		EClass LAYOUT_DATA = eINSTANCE.getLayoutData();

		/**
		 * The meta object literal for the '<em><b>Describes Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EReference LAYOUT_DATA__DESCRIBES_ELEMENT = eINSTANCE
				.getLayoutData_DescribesElement();

		/**
		 * The meta object literal for the '<em><b>Viewid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EAttribute LAYOUT_DATA__VIEWID = eINSTANCE.getLayoutData_Viewid();

		/**
		 * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EAttribute LAYOUT_DATA__WIDTH = eINSTANCE.getLayoutData_Width();

		/**
		 * The meta object literal for the '<em><b>Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EAttribute LAYOUT_DATA__HEIGHT = eINSTANCE.getLayoutData_Height();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EAttribute LAYOUT_DATA__X = eINSTANCE.getLayoutData_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EAttribute LAYOUT_DATA__Y = eINSTANCE.getLayoutData_Y();

		/**
		 * The meta object literal for the '<em><b>Initialized</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EAttribute LAYOUT_DATA__INITIALIZED = eINSTANCE.getLayoutData_Initialized();

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.we.model.view.impl.ReferenceImpl <em>Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.we.model.view.impl.ReferenceImpl
		 * @see org.eclipse.jwt.we.model.view.impl.ViewPackageImpl#getReference()
		 * @generated
		 */

		EClass REFERENCE = eINSTANCE.getReference();

		/**
		 * The meta object literal for the '<em><b>Contained In</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EReference REFERENCE__CONTAINED_IN = eINSTANCE.getReference_ContainedIn();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EReference REFERENCE__REFERENCE = eINSTANCE.getReference_Reference();

		/**
		 * The meta object literal for the '<em><b>Reference Edges</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EReference REFERENCE__REFERENCE_EDGES = eINSTANCE.getReference_ReferenceEdges();

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.we.model.view.impl.ReferenceEdgeImpl <em>Reference Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.we.model.view.impl.ReferenceEdgeImpl
		 * @see org.eclipse.jwt.we.model.view.impl.ViewPackageImpl#getReferenceEdge()
		 * @generated
		 */

		EClass REFERENCE_EDGE = eINSTANCE.getReferenceEdge();

		/**
		 * The meta object literal for the '<em><b>Contained In</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EReference REFERENCE_EDGE__CONTAINED_IN = eINSTANCE
				.getReferenceEdge_ContainedIn();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EReference REFERENCE_EDGE__REFERENCE = eINSTANCE.getReferenceEdge_Reference();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EReference REFERENCE_EDGE__ACTION = eINSTANCE.getReferenceEdge_Action();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EAttribute REFERENCE_EDGE__DIRECTION = eINSTANCE.getReferenceEdge_Direction();

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.we.model.view.impl.DiagramImpl <em>Diagram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.we.model.view.impl.DiagramImpl
		 * @see org.eclipse.jwt.we.model.view.impl.ViewPackageImpl#getDiagram()
		 * @generated
		 */

		EClass DIAGRAM = eINSTANCE.getDiagram();

		/**
		 * The meta object literal for the '<em><b>Describes Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EReference DIAGRAM__DESCRIBES_MODEL = eINSTANCE.getDiagram_DescribesModel();

		/**
		 * The meta object literal for the '<em><b>Layout Data</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EReference DIAGRAM__LAYOUT_DATA = eINSTANCE.getDiagram_LayoutData();

		/**
		 * The meta object literal for the '<em><b>References</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EReference DIAGRAM__REFERENCES = eINSTANCE.getDiagram_References();

		/**
		 * The meta object literal for the '<em><b>Reference Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */

		EReference DIAGRAM__REFERENCE_EDGES = eINSTANCE.getDiagram_ReferenceEdges();

		/**
		 * The meta object literal for the '{@link org.eclipse.jwt.we.model.view.EdgeDirection <em>Edge Direction</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.jwt.we.model.view.EdgeDirection
		 * @see org.eclipse.jwt.we.model.view.impl.ViewPackageImpl#getEdgeDirection()
		 * @generated
		 */

		EEnum EDGE_DIRECTION = eINSTANCE.getEdgeDirection();

	}

} //ViewPackage

