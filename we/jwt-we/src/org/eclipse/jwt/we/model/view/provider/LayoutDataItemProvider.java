/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.model.view.LayoutData;
import org.eclipse.jwt.we.model.view.ViewPackage;

/**
 * This is the item provider adapter for a
 * {@link org.eclipse.jwt.we.model.view.LayoutData} object. <!-- begin-user-doc
 * --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class LayoutDataItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public LayoutDataItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);

			addDescribesElementPropertyDescriptor(object);
			addViewidPropertyDescriptor(object);
			addWidthPropertyDescriptor(object);
			addHeightPropertyDescriptor(object);
			addXPropertyDescriptor(object);
			addYPropertyDescriptor(object);
			addInitializedPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}


	/**
	 * This adds a property descriptor for the Describes Element feature. [JWT]
	 * Edit (by Wolf Fischer): The implementation has been changed to check if
	 * this feature should be shown or not (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors
	 * list on every call and only if it should be shown, added to the list
	 * again! <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDescribesElementPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.LAYOUT_DATA__DESCRIBES_ELEMENT)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.LAYOUT_DATA__DESCRIBES_ELEMENT))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.LAYOUT_DATA__DESCRIBES_ELEMENT),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.LAYOUT_DATA__DESCRIBES_ELEMENT),

							ViewPackage.Literals.LAYOUT_DATA__DESCRIBES_ELEMENT, false,
							false, true, null, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Viewid feature. [JWT] Edit (by
	 * Wolf Fischer): The implementation has been changed to check if this
	 * feature should be shown or not (depending on the selected view). This
	 * means, that the feature is deleted from the itemPropertyDescriptors list
	 * on every call and only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addViewidPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.LAYOUT_DATA__VIEWID)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.LAYOUT_DATA__VIEWID))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.LAYOUT_DATA__VIEWID),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.LAYOUT_DATA__VIEWID),

							ViewPackage.Literals.LAYOUT_DATA__VIEWID, false, false,
							false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Width feature. [JWT] Edit (by
	 * Wolf Fischer): The implementation has been changed to check if this
	 * feature should be shown or not (depending on the selected view). This
	 * means, that the feature is deleted from the itemPropertyDescriptors list
	 * on every call and only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addWidthPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.LAYOUT_DATA__WIDTH)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.LAYOUT_DATA__WIDTH))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.LAYOUT_DATA__WIDTH),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.LAYOUT_DATA__WIDTH),

							ViewPackage.Literals.LAYOUT_DATA__WIDTH, true, false, false,
							ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Height feature. [JWT] Edit (by
	 * Wolf Fischer): The implementation has been changed to check if this
	 * feature should be shown or not (depending on the selected view). This
	 * means, that the feature is deleted from the itemPropertyDescriptors list
	 * on every call and only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addHeightPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.LAYOUT_DATA__HEIGHT)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.LAYOUT_DATA__HEIGHT))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.LAYOUT_DATA__HEIGHT),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.LAYOUT_DATA__HEIGHT),

							ViewPackage.Literals.LAYOUT_DATA__HEIGHT, true, false, false,
							ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the X feature. [JWT] Edit (by Wolf
	 * Fischer): The implementation has been changed to check if this feature
	 * should be shown or not (depending on the selected view). This means, that
	 * the feature is deleted from the itemPropertyDescriptors list on every
	 * call and only if it should be shown, added to the list again! <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addXPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.LAYOUT_DATA__X)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.LAYOUT_DATA__X))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.LAYOUT_DATA__X),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.LAYOUT_DATA__X),

							ViewPackage.Literals.LAYOUT_DATA__X, true, false, false,
							ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Y feature. [JWT] Edit (by Wolf
	 * Fischer): The implementation has been changed to check if this feature
	 * should be shown or not (depending on the selected view). This means, that
	 * the feature is deleted from the itemPropertyDescriptors list on every
	 * call and only if it should be shown, added to the list again! <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addYPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.LAYOUT_DATA__Y)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.LAYOUT_DATA__Y))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.LAYOUT_DATA__Y),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.LAYOUT_DATA__Y),

							ViewPackage.Literals.LAYOUT_DATA__Y, true, false, false,
							ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Initialized feature. [JWT] Edit
	 * (by Wolf Fischer): The implementation has been changed to check if this
	 * feature should be shown or not (depending on the selected view). This
	 * means, that the feature is deleted from the itemPropertyDescriptors list
	 * on every call and only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addInitializedPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ViewPackage.Literals.LAYOUT_DATA__INITIALIZED)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ViewPackage.Literals.LAYOUT_DATA__INITIALIZED))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ViewPackage.Literals.LAYOUT_DATA__INITIALIZED),

							PluginProperties
									.model_feature_description(ViewPackage.Literals.LAYOUT_DATA__INITIALIZED),

							ViewPackage.Literals.LAYOUT_DATA__INITIALIZED, false, false,
							false, ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @param object
	 * @return The text.
	 * @generated NOT
	 */
	@Override
	public String getText(Object object)
	{
		String type = PluginProperties.model_type(object);
		String text = PluginProperties.model_text(ViewPackage.Literals.LAYOUT_DATA,
				new Object[]
				{ type });

		// add view id to layoutdata text
		if (((LayoutData) object).getViewid() != null)
		{
			text += ": " + ((LayoutData) object).getViewid();
		}

		return text;
	}


	/**
	 * This handles model notifications by calling
	 * {@link #updateChildren(Notification)} to update any cached children and
	 * by creating a viewer notification, which it passes to
	 * {@link #fireNotifyChanged(Notification)}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param notification
	 * @generated NOT
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(LayoutData.class))
		{
			case ViewPackage.LAYOUT_DATA__VIEWID:
			case ViewPackage.LAYOUT_DATA__WIDTH:
			case ViewPackage.LAYOUT_DATA__HEIGHT:
			case ViewPackage.LAYOUT_DATA__X:
			case ViewPackage.LAYOUT_DATA__Y:
			case ViewPackage.LAYOUT_DATA__INITIALIZED:
				fireNotifyChanged(new ViewerNotification(notification,
						notification.getNotifier(), false, true));

				// notify corresponding element so it can update its figure
				((LayoutData) notification.getNotifier()).getDescribesElement().eNotify(
						notification);

				return;
		}
		super.notifyChanged(notification);
	}


	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		// super.collectNewChildDescriptors(newChildDescriptors, object);
	}


	/**
	 * Return an ImageDescriptor that describes the model. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @param object
	 * @return The ImageDescriptor.
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object)
	{
		return null;
	}


	/**
	 * Return the type image for creation of childs. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return The type image descriptor.
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getCreateChildImage(java.lang.Object,
	 *      java.lang.Object, java.lang.Object, java.util.Collection)
	 * @generated NOT
	 */
	@Override
	public Object getCreateChildImage(Object owner, Object feature, Object child,
			Collection selection)
	{
		return null;
	}


	/**
	 * Return the resource locator for this item provider's resources. Edit (by
	 * Marc Dutoo) : uses jwt plugin instance as resource locator and not <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator()
	{
		return Plugin.getInstance(); // [JWT]
	}


	/**
	 * This looks up the name of the type of the specified object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param object
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getTypeText(Object object)
	{
		return PluginProperties.model_type(object);
	}


	/**
	 * This looks up the name of the type of the specified attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param attribute
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getTypeText(EAttribute attribute)
	{
		return PluginProperties.model_datatype(attribute);
	}


	/**
	 * This looks up the name of the specified feature. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param feature
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getFeatureText(Object feature)
	{
		return PluginProperties.model_feature_name(feature);
	}


	/**
	 * This returns the description for CreateChildCommand. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return The description.
	 * @generated
	 */
	@Override
	public String getCreateChildDescription(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		Object selectionObject = selection == null || selection.isEmpty() ? null
				: selection.iterator().next();

		if (selectionObject == owner)
		{
			return PluginProperties
					.command_CreateChild_description(child, feature, owner);
		}

		Object sibling = selectionObject;
		Object siblingFeature = getChildFeature(owner, sibling);

		if (siblingFeature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) siblingFeature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) sibling;
			feature = entry.getEStructuralFeature();
			sibling = entry.getValue();
		}

		return PluginProperties
				.command_CreateSibling_description(child, feature, sibling);
	}


	/**
	 * This returns the tool tip text for CreateChildCommand. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return The description.
	 * @generated
	 */
	@Override
	public String getCreateChildToolTipText(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		return PluginProperties.command_CreateChild_tooltip(child, feature, owner);
	}


	/**
	 * This method overrides the ITreeItemContentProvider.getChildren to select
	 * which Objects may be shown in the outline depending on the view!
	 * 
	 * @author Wolf Fischer
	 * @generated NOT
	 */
	@Override
	public Collection getChildren(Object object)
	{
		return new ArrayList();
	}

}
