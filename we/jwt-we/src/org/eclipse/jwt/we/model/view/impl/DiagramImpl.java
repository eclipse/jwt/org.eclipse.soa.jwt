/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.jwt.meta.model.core.Model;
import org.eclipse.jwt.we.model.view.Diagram;
import org.eclipse.jwt.we.model.view.LayoutData;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.model.view.ReferenceEdge;
import org.eclipse.jwt.we.model.view.ViewPackage;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Diagram</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.DiagramImpl#getDescribesModel <em>Describes Model</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.DiagramImpl#getLayoutData <em>Layout Data</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.DiagramImpl#getReferences <em>References</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.DiagramImpl#getReferenceEdges <em>Reference Edges</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DiagramImpl
		extends EObjectImpl
		implements Diagram
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";

	/**
	 * The cached value of the '{@link #getDescribesModel() <em>Describes Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescribesModel()
	 * @generated
	 * @ordered
	 */
	protected Model describesModel;

	/**
	 * The cached value of the '{@link #getLayoutData() <em>Layout Data</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayoutData()
	 * @generated
	 * @ordered
	 */
	protected EList<LayoutData> layoutData;

	/**
	 * The cached value of the '{@link #getReferences() <em>References</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<Reference> references;

	/**
	 * The cached value of the '{@link #getReferenceEdges() <em>Reference Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<ReferenceEdge> referenceEdges;


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DiagramImpl()
	{
		super();
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ViewPackage.Literals.DIAGRAM;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model getDescribesModel()
	{
		if (describesModel != null && describesModel.eIsProxy())
		{
			InternalEObject oldDescribesModel = (InternalEObject) describesModel;
			describesModel = (Model) eResolveProxy(oldDescribesModel);
			if (describesModel != oldDescribesModel)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ViewPackage.DIAGRAM__DESCRIBES_MODEL, oldDescribesModel,
							describesModel));
			}
		}
		return describesModel;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model basicGetDescribesModel()
	{
		return describesModel;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescribesModel(Model newDescribesModel)
	{
		Model oldDescribesModel = describesModel;
		describesModel = newDescribesModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.DIAGRAM__DESCRIBES_MODEL, oldDescribesModel,
					describesModel));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LayoutData> getLayoutData()
	{
		if (layoutData == null)
		{
			layoutData = new EObjectContainmentEList<LayoutData>(LayoutData.class, this,
					ViewPackage.DIAGRAM__LAYOUT_DATA);
		}
		return layoutData;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Reference> getReferences()
	{
		if (references == null)
		{
			references = new EObjectContainmentEList<Reference>(Reference.class, this,
					ViewPackage.DIAGRAM__REFERENCES);
		}
		return references;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceEdge> getReferenceEdges()
	{
		if (referenceEdges == null)
		{
			referenceEdges = new EObjectContainmentEList<ReferenceEdge>(
					ReferenceEdge.class, this, ViewPackage.DIAGRAM__REFERENCE_EDGES);
		}
		return referenceEdges;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID,
			NotificationChain msgs)
	{
		switch (featureID)
		{
			case ViewPackage.DIAGRAM__LAYOUT_DATA:
				return ((InternalEList<?>) getLayoutData()).basicRemove(otherEnd, msgs);
			case ViewPackage.DIAGRAM__REFERENCES:
				return ((InternalEList<?>) getReferences()).basicRemove(otherEnd, msgs);
			case ViewPackage.DIAGRAM__REFERENCE_EDGES:
				return ((InternalEList<?>) getReferenceEdges()).basicRemove(otherEnd,
						msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case ViewPackage.DIAGRAM__DESCRIBES_MODEL:
				if (resolve)
					return getDescribesModel();
				return basicGetDescribesModel();
			case ViewPackage.DIAGRAM__LAYOUT_DATA:
				return getLayoutData();
			case ViewPackage.DIAGRAM__REFERENCES:
				return getReferences();
			case ViewPackage.DIAGRAM__REFERENCE_EDGES:
				return getReferenceEdges();
		}
		return super.eGet(featureID, resolve, coreType);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case ViewPackage.DIAGRAM__DESCRIBES_MODEL:
				setDescribesModel((Model) newValue);
				return;
			case ViewPackage.DIAGRAM__LAYOUT_DATA:
				getLayoutData().clear();
				getLayoutData().addAll((Collection<? extends LayoutData>) newValue);
				return;
			case ViewPackage.DIAGRAM__REFERENCES:
				getReferences().clear();
				getReferences().addAll((Collection<? extends Reference>) newValue);
				return;
			case ViewPackage.DIAGRAM__REFERENCE_EDGES:
				getReferenceEdges().clear();
				getReferenceEdges()
						.addAll((Collection<? extends ReferenceEdge>) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case ViewPackage.DIAGRAM__DESCRIBES_MODEL:
				setDescribesModel((Model) null);
				return;
			case ViewPackage.DIAGRAM__LAYOUT_DATA:
				getLayoutData().clear();
				return;
			case ViewPackage.DIAGRAM__REFERENCES:
				getReferences().clear();
				return;
			case ViewPackage.DIAGRAM__REFERENCE_EDGES:
				getReferenceEdges().clear();
				return;
		}
		super.eUnset(featureID);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case ViewPackage.DIAGRAM__DESCRIBES_MODEL:
				return describesModel != null;
			case ViewPackage.DIAGRAM__LAYOUT_DATA:
				return layoutData != null && !layoutData.isEmpty();
			case ViewPackage.DIAGRAM__REFERENCES:
				return references != null && !references.isEmpty();
			case ViewPackage.DIAGRAM__REFERENCE_EDGES:
				return referenceEdges != null && !referenceEdges.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DiagramImpl