/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.jwt.meta.model.core.GraphicalElement;
import org.eclipse.jwt.we.model.view.LayoutData;
import org.eclipse.jwt.we.model.view.ViewPackage;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Layout Data</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.LayoutDataImpl#getDescribesElement <em>Describes Element</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.LayoutDataImpl#getViewid <em>Viewid</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.LayoutDataImpl#getWidth <em>Width</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.LayoutDataImpl#getHeight <em>Height</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.LayoutDataImpl#getX <em>X</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.LayoutDataImpl#getY <em>Y</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.impl.LayoutDataImpl#isInitialized <em>Initialized</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LayoutDataImpl
		extends EObjectImpl
		implements LayoutData
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";

	/**
	 * The cached value of the '{@link #getDescribesElement() <em>Describes Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescribesElement()
	 * @generated
	 * @ordered
	 */
	protected GraphicalElement describesElement;

	/**
	 * The default value of the '{@link #getViewid() <em>Viewid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewid()
	 * @generated
	 * @ordered
	 */
	protected static final String VIEWID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getViewid() <em>Viewid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewid()
	 * @generated
	 * @ordered
	 */
	protected String viewid = VIEWID_EDEFAULT;

	/**
	 * The default value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int WIDTH_EDEFAULT = 10;

	/**
	 * The cached value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected int width = WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getHeight() <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeight()
	 * @generated
	 * @ordered
	 */
	protected static final int HEIGHT_EDEFAULT = 10;

	/**
	 * The cached value of the '{@link #getHeight() <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeight()
	 * @generated
	 * @ordered
	 */
	protected int height = HEIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #getX() <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX()
	 * @generated
	 * @ordered
	 */
	protected static final int X_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getX() <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX()
	 * @generated
	 * @ordered
	 */
	protected int x = X_EDEFAULT;

	/**
	 * The default value of the '{@link #getY() <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY()
	 * @generated
	 * @ordered
	 */
	protected static final int Y_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getY() <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY()
	 * @generated
	 * @ordered
	 */
	protected int y = Y_EDEFAULT;

	/**
	 * The default value of the '{@link #isInitialized() <em>Initialized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInitialized()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INITIALIZED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isInitialized() <em>Initialized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInitialized()
	 * @generated
	 * @ordered
	 */
	protected boolean initialized = INITIALIZED_EDEFAULT;


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LayoutDataImpl()
	{
		super();
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ViewPackage.Literals.LAYOUT_DATA;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphicalElement getDescribesElement()
	{
		if (describesElement != null && describesElement.eIsProxy())
		{
			InternalEObject oldDescribesElement = (InternalEObject) describesElement;
			describesElement = (GraphicalElement) eResolveProxy(oldDescribesElement);
			if (describesElement != oldDescribesElement)
			{
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ViewPackage.LAYOUT_DATA__DESCRIBES_ELEMENT,
							oldDescribesElement, describesElement));
			}
		}
		return describesElement;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphicalElement basicGetDescribesElement()
	{
		return describesElement;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescribesElement(GraphicalElement newDescribesElement)
	{
		GraphicalElement oldDescribesElement = describesElement;
		describesElement = newDescribesElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.LAYOUT_DATA__DESCRIBES_ELEMENT, oldDescribesElement,
					describesElement));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getViewid()
	{
		return viewid;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setViewid(String newViewid)
	{
		String oldViewid = viewid;
		viewid = newViewid;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.LAYOUT_DATA__VIEWID, oldViewid, viewid));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWidth()
	{
		return width;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWidth(int newWidth)
	{
		int oldWidth = width;
		width = newWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.LAYOUT_DATA__WIDTH, oldWidth, width));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHeight()
	{
		return height;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeight(int newHeight)
	{
		int oldHeight = height;
		height = newHeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.LAYOUT_DATA__HEIGHT, oldHeight, height));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getX()
	{
		return x;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX(int newX)
	{
		int oldX = x;
		x = newX;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.LAYOUT_DATA__X, oldX, x));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getY()
	{
		return y;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setY(int newY)
	{
		int oldY = y;
		y = newY;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.LAYOUT_DATA__Y, oldY, y));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInitialized()
	{
		return initialized;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialized(boolean newInitialized)
	{
		boolean oldInitialized = initialized;
		initialized = newInitialized;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ViewPackage.LAYOUT_DATA__INITIALIZED, oldInitialized, initialized));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID)
		{
			case ViewPackage.LAYOUT_DATA__DESCRIBES_ELEMENT:
				if (resolve)
					return getDescribesElement();
				return basicGetDescribesElement();
			case ViewPackage.LAYOUT_DATA__VIEWID:
				return getViewid();
			case ViewPackage.LAYOUT_DATA__WIDTH:
				return new Integer(getWidth());
			case ViewPackage.LAYOUT_DATA__HEIGHT:
				return new Integer(getHeight());
			case ViewPackage.LAYOUT_DATA__X:
				return new Integer(getX());
			case ViewPackage.LAYOUT_DATA__Y:
				return new Integer(getY());
			case ViewPackage.LAYOUT_DATA__INITIALIZED:
				return isInitialized() ? Boolean.TRUE : Boolean.FALSE;
		}
		return super.eGet(featureID, resolve, coreType);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID)
		{
			case ViewPackage.LAYOUT_DATA__DESCRIBES_ELEMENT:
				setDescribesElement((GraphicalElement) newValue);
				return;
			case ViewPackage.LAYOUT_DATA__VIEWID:
				setViewid((String) newValue);
				return;
			case ViewPackage.LAYOUT_DATA__WIDTH:
				setWidth(((Integer) newValue).intValue());
				return;
			case ViewPackage.LAYOUT_DATA__HEIGHT:
				setHeight(((Integer) newValue).intValue());
				return;
			case ViewPackage.LAYOUT_DATA__X:
				setX(((Integer) newValue).intValue());
				return;
			case ViewPackage.LAYOUT_DATA__Y:
				setY(((Integer) newValue).intValue());
				return;
			case ViewPackage.LAYOUT_DATA__INITIALIZED:
				setInitialized(((Boolean) newValue).booleanValue());
				return;
		}
		super.eSet(featureID, newValue);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID)
		{
			case ViewPackage.LAYOUT_DATA__DESCRIBES_ELEMENT:
				setDescribesElement((GraphicalElement) null);
				return;
			case ViewPackage.LAYOUT_DATA__VIEWID:
				setViewid(VIEWID_EDEFAULT);
				return;
			case ViewPackage.LAYOUT_DATA__WIDTH:
				setWidth(WIDTH_EDEFAULT);
				return;
			case ViewPackage.LAYOUT_DATA__HEIGHT:
				setHeight(HEIGHT_EDEFAULT);
				return;
			case ViewPackage.LAYOUT_DATA__X:
				setX(X_EDEFAULT);
				return;
			case ViewPackage.LAYOUT_DATA__Y:
				setY(Y_EDEFAULT);
				return;
			case ViewPackage.LAYOUT_DATA__INITIALIZED:
				setInitialized(INITIALIZED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID)
		{
			case ViewPackage.LAYOUT_DATA__DESCRIBES_ELEMENT:
				return describesElement != null;
			case ViewPackage.LAYOUT_DATA__VIEWID:
				return VIEWID_EDEFAULT == null ? viewid != null : !VIEWID_EDEFAULT
						.equals(viewid);
			case ViewPackage.LAYOUT_DATA__WIDTH:
				return width != WIDTH_EDEFAULT;
			case ViewPackage.LAYOUT_DATA__HEIGHT:
				return height != HEIGHT_EDEFAULT;
			case ViewPackage.LAYOUT_DATA__X:
				return x != X_EDEFAULT;
			case ViewPackage.LAYOUT_DATA__Y:
				return y != Y_EDEFAULT;
			case ViewPackage.LAYOUT_DATA__INITIALIZED:
				return initialized != INITIALIZED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (viewid: "); //$NON-NLS-1$
		result.append(viewid);
		result.append(", width: "); //$NON-NLS-1$
		result.append(width);
		result.append(", height: "); //$NON-NLS-1$
		result.append(height);
		result.append(", x: "); //$NON-NLS-1$
		result.append(x);
		result.append(", y: "); //$NON-NLS-1$
		result.append(y);
		result.append(", initialized: "); //$NON-NLS-1$
		result.append(initialized);
		result.append(')');
		return result.toString();
	}

} //LayoutDataImpl