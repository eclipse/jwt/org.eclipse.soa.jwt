/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.we.model.view;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jwt.meta.model.core.GraphicalElement;
import org.eclipse.jwt.meta.model.core.ModelElement;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.meta.model.processes.Scope;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.we.model.view.Reference#getContainedIn <em>Contained In</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.Reference#getReference <em>Reference</em>}</li>
 *   <li>{@link org.eclipse.jwt.we.model.view.Reference#getReferenceEdges <em>Reference Edges</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.we.model.view.ViewPackage#getReference()
 * @model
 * @generated
 */
public interface Reference
		extends ModelElement, GraphicalElement
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * Returns the value of the '<em><b>Contained In</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained In</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained In</em>' reference.
	 * @see #setContainedIn(Scope)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getReference_ContainedIn()
	 * @model required="true"
	 * @generated
	 */
	Scope getContainedIn();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.Reference#getContainedIn <em>Contained In</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contained In</em>' reference.
	 * @see #getContainedIn()
	 * @generated
	 */
	void setContainedIn(Scope value);


	/**
	 * Returns the value of the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' reference.
	 * @see #setReference(ReferenceableElement)
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getReference_Reference()
	 * @model
	 * @generated
	 */
	ReferenceableElement getReference();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.we.model.view.Reference#getReference <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' reference.
	 * @see #getReference()
	 * @generated
	 */
	void setReference(ReferenceableElement value);


	/**
	 * Returns the value of the '<em><b>Reference Edges</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.jwt.we.model.view.ReferenceEdge}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.jwt.we.model.view.ReferenceEdge#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Edges</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Edges</em>' reference list.
	 * @see org.eclipse.jwt.we.model.view.ViewPackage#getReference_ReferenceEdges()
	 * @see org.eclipse.jwt.we.model.view.ReferenceEdge#getReference
	 * @model opposite="reference"
	 * @generated
	 */
	EList<ReferenceEdge> getReferenceEdges();

} // Reference