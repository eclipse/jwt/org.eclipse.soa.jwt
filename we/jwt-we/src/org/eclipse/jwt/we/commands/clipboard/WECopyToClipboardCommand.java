/**
 * File:    WECopyToClipboardCommand.java
 * Created: 14.06.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- rewrote from scratch
 *******************************************************************************/

package org.eclipse.jwt.we.commands.clipboard;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.meta.commands.clipboard.JWTCopyToClipboardCommand;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.util.EMFHelper;
import org.eclipse.jwt.we.misc.views.LayoutDataManager;
import org.eclipse.jwt.we.model.view.ReferenceEdge;

/**
 * A custom CopyToClipboard command, that not only copies the selected objects
 * to the clipboard but also the edges (ActivityEdge, ReferenceEdge) that exist
 * between these objects.
 * 
 * @version $Id: CustomCopyToClipboardCommand.java,v 1.2 2008/09/30 07:53:40
 *          flautenba Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class WECopyToClipboardCommand extends JWTCopyToClipboardCommand
{

	/**
	 * The workflow editor.
	 */
	private WEEditor weeditor;


	/**
	 * The constructor.
	 * 
	 * @param domain
	 * @param collection
	 */
	public WECopyToClipboardCommand(WEEditor weeditor, EditingDomain domain,
			Collection<?> collection)
	{
		super(domain, collection);
		this.weeditor = weeditor;
	}


	/**
	 * This is called before the doExecute method by the command stack. It
	 * replaces the old copy command with a new one that also contains the edges
	 * between the elements to be copied to clipboard. If the copy command
	 * cannot be executed, this method returns false and the CommandStack will
	 * not abort the execution.
	 * 
	 * @return
	 */
	@Override
	public boolean checkIfCommandCanBeExecuted()
	{
		// remove all ReferenceEdges, ActivityEdges and the main Model element
		Collection relevantObjects = new HashSet();
		for (Iterator iterator = sourceObjects.iterator(); iterator.hasNext();)
		{
			Object object = iterator.next();
			if (object instanceof EObject
					&& !(object instanceof ReferenceEdge || object instanceof ActivityEdge))
			{
				// check if the container of the object is in the list
				boolean containerIsAlreadyPresent = false;
				;
				EObject container = ((EObject) object).eContainer();
				while (container != null)
				{
					if (sourceObjects.contains(container))
					{
						containerIsAlreadyPresent = true;
						break;
					}
					container = container.eContainer();
				}

				// add only if the container is not already present
				if (!containerIsAlreadyPresent)
				{
					relevantObjects.add(object);
				}
			}
		}
		sourceObjects = relevantObjects;

		// if no objects remain -> abort
		if (sourceObjects.isEmpty())
		{
			return false;
		}

		// get all ReferenceEdges and ActivityEdges which exist between the
		// affected objects
		Set edges = EMFHelper.getAffectedEdges(sourceObjects);

		// dispose of the old copy command
		if (copyCommand != null)
		{
			copyCommand.dispose();
		}

		// add edges and layoutdata to the objects to be copied to clipboard
		sourceObjects.addAll(edges);
		sourceObjects.addAll(EMFHelper.getAllContainedReferenceEdges(weeditor,
				sourceObjects));
		sourceObjects
				.addAll(EMFHelper.getAllContainedReferences(weeditor, sourceObjects));
		sourceObjects.addAll(LayoutDataManager.getAllContainedLayoutDatas(weeditor,
				sourceObjects));

		// prepare the new copy command and check if it can be executed. if not,
		// abort the execution of the command
		if (prepare())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
