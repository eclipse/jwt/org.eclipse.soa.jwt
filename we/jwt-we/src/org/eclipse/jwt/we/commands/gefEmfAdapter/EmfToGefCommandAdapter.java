/**
 * File:    EmfToGefCommandAdapter.java
 * Created: 03.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.commands.gefEmfAdapter;

import org.eclipse.emf.common.command.Command;

/**
 * Adapts an EMF command to be a GEF command.
 * 
 * @version $Id: EmfToGefCommandAdapter.java,v 1.4 2009/11/26 12:41:39 chsaad
 *          Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 */
public class EmfToGefCommandAdapter extends org.eclipse.gef.commands.Command
{

	/**
	 * The EMF command.
	 */
	private Command emfCommand;


	/**
	 * Creates a new adapter without an EMF command. The EMF command must be set
	 * before calling any other method.
	 */
	public EmfToGefCommandAdapter()
	{
	}


	/**
	 * @param emfCommand
	 *            The EMF command.
	 */
	public EmfToGefCommandAdapter(Command emfCommand)
	{
		this(null, emfCommand);
	}


	/**
	 * @param label
	 *            The Command's label.
	 * @param emfCommand
	 *            The EMF command.
	 */
	public EmfToGefCommandAdapter(String label, Command emfCommand)
	{
		super(label);
		this.emfCommand = emfCommand;
	}


	/**
	 * @return Returns the emfCommand.
	 */
	public Command getEmfCommand()
	{
		return emfCommand;
	}


	/**
	 * @param emfCommand
	 *            The emfCommand to set.
	 */
	public void setEmfCommand(Command emfCommand)
	{
		this.emfCommand = emfCommand;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#getDebugLabel()
	 */
	@Override
	public String getDebugLabel()
	{
		if (super.getDebugLabel() != null)
		{
			return super.getDebugLabel();
		}

		return emfCommand.getDescription();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#getLabel()
	 */
	@Override
	public String getLabel()
	{
		if (super.getLabel() != null)
		{
			return super.getLabel();
		}

		return emfCommand.getLabel();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute()
	{
		return emfCommand.canExecute();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo()
	{
		return emfCommand.canUndo();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute()
	{
		emfCommand.execute();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo()
	{
		emfCommand.redo();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo()
	{
		emfCommand.undo();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#dispose()
	 */
	@Override
	public void dispose()
	{
		emfCommand.dispose();
	}
}
