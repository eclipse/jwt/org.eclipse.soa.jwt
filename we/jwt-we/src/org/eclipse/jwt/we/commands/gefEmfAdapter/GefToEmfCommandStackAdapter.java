/**
 * File:    GefToEmfCommandStackAdapter.java
 * Created: 07.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.commands.gefEmfAdapter;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.command.CommandStackListener;

/**
 * Wraps a GEF command stack to behave like a EMF command stack.
 * 
 * @version $Id: GefToEmfCommandStackAdapter.java,v 1.4 2009/11/26 12:41:39
 *          chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 */
public class GefToEmfCommandStackAdapter implements CommandStack
{

	/**
	 * The GEF command stack.
	 */
	private org.eclipse.gef.commands.CommandStack gefCommandStack;


	/**
	 * @param gefCommandStack
	 *            The GEF command stack.
	 */
	public GefToEmfCommandStackAdapter(
			org.eclipse.gef.commands.CommandStack gefCommandStack)
	{
		assert gefCommandStack != null;
		this.gefCommandStack = gefCommandStack;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.common.command.CommandStack#execute(org.eclipse.emf.common
	 * .command.Command)
	 */
	public void execute(Command command)
	{
		gefCommandStack.execute(new EmfToGefCommandAdapter(command));
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.common.command.CommandStack#canRedo()
	 */
	public boolean canRedo()
	{
		return gefCommandStack.canRedo();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.common.command.CommandStack#redo()
	 */
	public void redo()
	{
		gefCommandStack.redo();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.common.command.CommandStack#getRedoCommand()
	 */
	public Command getRedoCommand()
	{
		if (gefCommandStack.getRedoCommand() instanceof EmfToGefCommandAdapter)
		{
			return ((EmfToGefCommandAdapter) gefCommandStack.getRedoCommand())
					.getEmfCommand();
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.common.command.CommandStack#canUndo()
	 */
	public boolean canUndo()
	{
		return gefCommandStack.canUndo();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.common.command.CommandStack#undo()
	 */
	public void undo()
	{
		gefCommandStack.undo();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.common.command.CommandStack#getUndoCommand()
	 */
	public Command getUndoCommand()
	{
		if (gefCommandStack.getUndoCommand() instanceof EmfToGefCommandAdapter)
		{
			return ((EmfToGefCommandAdapter) gefCommandStack.getUndoCommand())
					.getEmfCommand();
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.common.command.CommandStack#getMostRecentCommand()
	 */
	public Command getMostRecentCommand()
	{
		Object[] commands = gefCommandStack.getCommands();

		if (commands.length > 0)
		{
			Object latest = commands[commands.length - 1];
			if (latest instanceof EmfToGefCommandAdapter)
			{
				return ((EmfToGefCommandAdapter) latest).getEmfCommand();
			}
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.common.command.CommandStack#flush()
	 */
	public void flush()
	{
		gefCommandStack.flush();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.common.command.CommandStack#addCommandStackListener(org
	 * .eclipse.emf.common.command.CommandStackListener)
	 */
	public void addCommandStackListener(CommandStackListener listener)
	{
		gefCommandStack.addCommandStackListener(new EmfToGefCommandStackListenerAdapter(
				listener));
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.common.command.CommandStack#removeCommandStackListener
	 * (org.eclipse.emf.common.command.CommandStackListener)
	 */
	public void removeCommandStackListener(CommandStackListener listener)
	{
		gefCommandStack
				.removeCommandStackListener(new EmfToGefCommandStackListenerAdapter(
						listener));
	}
}
