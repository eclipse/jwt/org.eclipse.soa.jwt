/**
 * File:    WERemoveActivityNodeCommand.java
 * Created: 09.06.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- rewrote from scratch
 *******************************************************************************/

package org.eclipse.jwt.we.commands.processes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.meta.commands.processes.RemoveActivityNodeCommand;
import org.eclipse.jwt.meta.model.processes.Action;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;
import org.eclipse.jwt.meta.model.processes.ActivityNode;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.util.EMFHelper;
import org.eclipse.jwt.we.misc.views.LayoutDataManager;
import org.eclipse.jwt.we.model.view.ReferenceEdge;

/**
 * Removes an {@link ActivityNode} from its {@link Scope}.
 * 
 * This command also removes all {@link ActivityEdge}s and {@link ReferenceEdge}
 * s connected to the node by generating corresponding RemoveCommands.
 * 
 * @version $Id: WERemoveActivityNodeCommand.java,v 1.2 2009/11/04 17:18:24
 *          chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class WERemoveActivityNodeCommand extends RemoveActivityNodeCommand
{

	/**
	 * The workflow editor.
	 */
	private WEEditor weeditor;

	/**
	 * The Editing Domain.
	 */
	private EditingDomain editingDomain;

	/**
	 * The command to delete related view data.
	 */
	private Command removeViewDataCommand;


	/**
	 * Constructor.
	 * 
	 * @param domain
	 * @param owner
	 * @param feature
	 * @param collection
	 */
	public WERemoveActivityNodeCommand(WEEditor weeditor, EditingDomain domain,
			EObject owner, EStructuralFeature feature, Collection<?> collection)
	{
		super(domain, owner, feature, collection);

		this.editingDomain = domain;
		this.weeditor = weeditor;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doExecute()
	 */
	@Override
	public void doExecute()
	{
		// some activitynodes may already be deleted by CustomDeleteCommand if
		// the corresponding activity was deleted
		ArrayList newCollection = new ArrayList();
		for (Iterator iterator = collection.iterator(); iterator.hasNext();)
		{
			ActivityNode actNode = (ActivityNode) iterator.next();

			if (actNode.eContainer() != null)
			{
				newCollection.add(actNode);
			}
		}
		collection = newCollection;

		// prepare remove edges
		removeEdgesCommands = new CompoundCommand();

		// for every ActivityNode to be deleted
		for (Iterator i = collection.iterator(); i.hasNext();)
		{
			ActivityNode node = (ActivityNode) i.next();

			// create RemoveCommands for In/Out ActivityEdges
			if (!node.getIn().isEmpty())
			{
				removeEdgesCommands.append(DeleteCommand.create(editingDomain, node
						.getIn()));
			}
			if (!node.getOut().isEmpty())
			{
				removeEdgesCommands.append(DeleteCommand.create(editingDomain, node
						.getOut()));
			}

			// if the node is an action, create RemoveCommands for
			// ReferenceEdges (RemoveReferenceEdgeCommand)
			if (node instanceof Action
					&& !EMFHelper.getReferenceEdgesForAction(weeditor, (Action) node)
							.isEmpty())
			{
				removeEdgesCommands.append(DeleteCommand.create(editingDomain, EMFHelper
						.getReferenceEdgesForAction(weeditor, (Action) node)));
			}
		}

		// create a delete command for associated view data
		createDeleteViewDataCommand();

		// remove all ActivityEdges and ReferenceEdges
		if (removeEdgesCommands.canExecute())
		{
			removeEdgesCommands.execute();
		}

		// remove the action
		doExecuteOriginal();

		// remove all ActivityEdges and ReferenceEdges
		if (removeViewDataCommand.canExecute())
		{
			removeViewDataCommand.execute();
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.meta.commands.processes.RemoveActivityNodeCommand#doRedo
	 * ()
	 */
	@Override
	public void doRedo()
	{
		super.doRedo();

		// remove all view data
		if (removeViewDataCommand.canExecute())
		{
			removeViewDataCommand.redo();
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.meta.commands.processes.RemoveActivityNodeCommand#doUndo
	 * ()
	 */
	@Override
	public void doUndo()
	{
		// restore all view data
		if (removeViewDataCommand.canUndo())
		{
			removeViewDataCommand.undo();
		}

		super.doUndo();
	}


	/**
	 * Create command to delete all view information.
	 */
	private void createDeleteViewDataCommand()
	{
		// layout data
		Collection laydataCollection = LayoutDataManager.getAllContainedLayoutDatas(
				weeditor, collection);

		// if a embedded scope is contained
		Collection refEdgesCollection = EMFHelper.getAllContainedReferenceEdges(weeditor,
				collection);
		Collection refsCollection = EMFHelper.getAllContainedReferences(weeditor,
				collection);

		Set removeViewDataSet = new HashSet();
		if (laydataCollection != null)
		{
			removeViewDataSet.addAll(laydataCollection);
		}
		if (refEdgesCollection != null)
		{
			removeViewDataSet.addAll(refEdgesCollection);
		}
		if (refsCollection != null)
		{
			removeViewDataSet.addAll(refsCollection);
		}

		// prepare remove view data
		removeViewDataCommand = DeleteCommand.create(editingDomain, removeViewDataSet);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.AbstractOverrideableCommand#doDispose()
	 */
	@Override
	public void doDispose()
	{
		super.doDispose();

		if (removeViewDataCommand != null)
		{
			removeViewDataCommand.dispose();
		}
	}

}