/**
 * File:    RemoveLayoutDataCommand
 * Created: 07.12.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.commands.view;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.meta.model.core.GraphicalElement;
import org.eclipse.jwt.we.model.view.Diagram;
import org.eclipse.jwt.we.model.view.LayoutData;
import org.eclipse.jwt.we.model.view.ReferenceEdge;

/**
 * Removes layout data.
 * 
 * This command also removes all {@link ReferenceEdge}s connected to the
 * reference by generating corresponding RemoveCommands.
 * 
 * @version $Id: RemoveLayoutDataCommand.java,v 1.1 2009/12/10 12:30:53 chsaad
 *          Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class RemoveLayoutDataCommand extends RemoveCommand
{

	/**
	 * Store old layout data.
	 */
	private Map<LayoutData, GraphicalElement> elementMap;

	/**
	 * Store containing diagram.
	 */
	private Map<LayoutData, Diagram> diagramMap;


	/**
	 * Constructor.
	 * 
	 * @param domain
	 * @param owner
	 * @param feature
	 * @param collection
	 */
	public RemoveLayoutDataCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection<?> collection)
	{
		super(domain, owner, feature, collection);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doExecute()
	 */
	@Override
	public void doExecute()
	{
		elementMap = new HashMap<LayoutData, GraphicalElement>();
		diagramMap = new HashMap<LayoutData, Diagram>();

		for (Iterator iter = collection.iterator(); iter.hasNext();)
		{
			LayoutData ld = (LayoutData) iter.next();

			if (ld.eContainer() != null)
			{
				diagramMap.put(ld, (Diagram) ld.eContainer());
				((Diagram) ld.eContainer()).getLayoutData().remove(ld);
			}
			if (ld.getDescribesElement() != null)
			{
				elementMap.put(ld, ld.getDescribesElement());
				ld.setDescribesElement(null);
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doRedo()
	 */
	@Override
	public void doRedo()
	{
		for (Iterator iter = collection.iterator(); iter.hasNext();)
		{
			LayoutData ld = (LayoutData) iter.next();

			if (ld.eContainer() != null)
			{
				((Diagram) ld.eContainer()).getLayoutData().remove(ld);
			}
			if (ld.getDescribesElement() != null)
			{
				ld.setDescribesElement(null);
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doUndo()
	 */
	@Override
	public void doUndo()
	{
		for (Iterator iter = elementMap.entrySet().iterator(); iter.hasNext();)
		{
			Entry<LayoutData, GraphicalElement> entry = (Entry<LayoutData, GraphicalElement>) iter
					.next();

			entry.getKey().setDescribesElement(entry.getValue());
			diagramMap.get(entry.getKey()).getLayoutData().add(entry.getKey());
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.AbstractOverrideableCommand#doDispose()
	 */
	@Override
	public void doDispose()
	{
		super.doDispose();

		elementMap.clear();
		diagramMap.clear();
	}

}