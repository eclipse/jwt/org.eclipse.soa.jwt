/**
 * File:    SetGraphicalElementConstraintCommand.java
 * Created: 04.01.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.commands.view;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.jwt.meta.model.core.GraphicalElement;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.misc.views.LayoutDataManager;
import org.eclipse.jwt.we.model.view.LayoutData;

/**
 * A command to set a constraint on a {@link GraphicalElement}.
 * 
 * @version $Id: SetGraphicalElementConstraintCommand.java 24 2006-02-02
 *          13:25:08Z bauermar $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 */
public class SetGraphicalElementConstraintCommand extends Command
{

	/**
	 * A Logger.
	 */
	private static final Logger logger = Logger
			.getLogger(SetGraphicalElementConstraintCommand.class);

	/**
	 * The workflow editor.
	 */
	private WEEditor weeditor;

	/**
	 * Request that knows the new Object.
	 */
	private CreateRequest createRequest;

	/**
	 * Element to manipulate.
	 */
	private GraphicalElement element;

	/**
	 * The new size and location.
	 */
	private Rectangle newBounds;

	/**
	 * Stores the old layout data.
	 */
	private LayoutData oldLayoutData;


	/**
	 * Creates a new command for changing constraints of a element.
	 * 
	 * @param createRequest
	 *            Request that knows the new Object.
	 * @param newBounds
	 *            The new size and location.
	 */
	public SetGraphicalElementConstraintCommand(WEEditor weeditor,
			CreateRequest createRequest, Rectangle newBounds)
	{
		this.createRequest = createRequest;
		this.newBounds = newBounds;
		this.weeditor = weeditor;
	}


	/**
	 * @param element
	 *            Element to manipulate.
	 * @param newBounds
	 *            The new size and location.
	 */
	public SetGraphicalElementConstraintCommand(WEEditor weeditor,
			GraphicalElement element, Rectangle newBounds)
	{
		this.element = element;
		this.newBounds = newBounds;
		this.weeditor = weeditor;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute()
	{
		return element != null
				|| (createRequest != null
						&& createRequest.getNewObjectType() instanceof Class && GraphicalElement.class
						.isAssignableFrom((Class) createRequest.getNewObjectType()));
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute()
	{
		if (element == null)
		{
			element = (GraphicalElement) createRequest.getNewObject();
		}

		// store a copy of the old layout data
		oldLayoutData = (LayoutData) (new EcoreUtil.Copier()).copy(LayoutDataManager
				.getLayoutData(weeditor, element));

		// set the position
		redo();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo()
	{
		if (newBounds.x != oldLayoutData.getX() || newBounds.y != oldLayoutData.getY()
				|| newBounds.width != oldLayoutData.getWidth()
				|| newBounds.height != oldLayoutData.getHeight())
		{
			if (newBounds.x != -1)
				LayoutDataManager.setX(weeditor, element, newBounds.x);
			if (newBounds.y != -1)
				LayoutDataManager.setY(weeditor, element, newBounds.y);
			if (newBounds.width != -1)
				LayoutDataManager.setWidth(weeditor, element, newBounds.width);
			if (newBounds.height != -1)
				LayoutDataManager.setHeight(weeditor, element, newBounds.height);
			logger.valueChanged("element.layoutdata", oldLayoutData, newBounds); //$NON-NLS-1$
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo()
	{
		LayoutDataManager.setLayoutData(weeditor, element, oldLayoutData);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#dispose()
	 */
	@Override
	public void dispose()
	{
		super.dispose();

		createRequest = null;
		newBounds = null;
		oldLayoutData = null;
	}
}
