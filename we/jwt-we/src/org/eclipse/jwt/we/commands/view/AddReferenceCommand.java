/**
 * File:    AddReferenceCommand.java
 * Created: 28.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.commands.view;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.model.view.Diagram;
import org.eclipse.jwt.we.model.view.Reference;

/**
 * This command extends the EMF {@link AddCommand}.
 * 
 * A new {@link Reference} is added to a {@link Scope}. There may be different
 * {@link Reference}s that point to the same element.
 * 
 * @version $Id: AddReferenceCommand.java,v 1.4 2009-12-10 12:51:18 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 */
public class AddReferenceCommand extends AddCommand
{

	/**
	 * The constructor.
	 * 
	 * @param domain
	 * @param owner
	 * @param feature
	 * @param collection
	 * @param index
	 */
	public AddReferenceCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection collection, int index)
	{
		super(domain, owner, feature, collection, index);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.AddCommand#prepare()
	 */
	@Override
	protected boolean prepare()
	{
		if (getOwner() == null || !(getOwner() instanceof Diagram)
				|| getCollection() == null)
		{
			return false;
		}

		// REMOVED to allow multiple instances of the same Reference

		/*
		 * // make sure the collection is removeabale collection = new
		 * ArrayList(collection); // remove existing References for (Iterator
		 * newReferences = getCollection().iterator(); newReferences
		 * .hasNext();) { ReferenceableElement element = ((Reference)
		 * newReferences.next()).getReference();
		 * 
		 * for (Iterator existingReferences = ((Scope)
		 * getOwner()).getReferences().iterator(); existingReferences
		 * .hasNext();) { if (((Reference)
		 * existingReferences.next()).getReference() == element) {
		 * newReferences.remove(); break; } } }
		 */

		// this will also ensure that the command is only executeable if the
		// collection is not empty.
		return super.prepare();
	}

}
