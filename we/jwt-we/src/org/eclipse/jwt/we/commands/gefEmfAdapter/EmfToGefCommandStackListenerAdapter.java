/**
 * File:    EmfToGefCommandStackListenerAdapter.java
 * Created: 07.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.commands.gefEmfAdapter;

import java.util.EventObject;

import org.eclipse.gef.commands.CommandStackListener;

/**
 * Wraps an EMF {@link org.eclipse.emf.common.command.CommandStackListener
 * CommandStackListener} to behave like a GEF {@link CommandStackListener}.
 * 
 * @version $Id: EmfToGefCommandStackListenerAdapter.java,v 1.4 2009/11/26
 *          12:41:39 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 */
public class EmfToGefCommandStackListenerAdapter implements CommandStackListener
{

	/**
	 * The EMF CommandStackListener.
	 */
	private org.eclipse.emf.common.command.CommandStackListener emfListener;


	/**
	 * @param emfListener
	 *            The EMF CommandStackListener.
	 */
	public EmfToGefCommandStackListenerAdapter(
			org.eclipse.emf.common.command.CommandStackListener emfListener)
	{
		this.emfListener = emfListener;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.commands.CommandStackListener#commandStackChanged(java
	 * .util.EventObject)
	 */
	public void commandStackChanged(EventObject event)
	{
		emfListener.commandStackChanged(event);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj != null && obj instanceof EmfToGefCommandStackListenerAdapter)
		{
			return emfListener
					.equals(((EmfToGefCommandStackListenerAdapter) obj).emfListener);
		}

		return false;
	}
}
