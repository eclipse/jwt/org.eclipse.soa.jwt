package org.eclipse.jwt.we.commands.clipboard;

import org.eclipse.emf.common.command.StrictCompoundCommand;

/**
 * This command always does undo/redo on ALL of its contained commands.
 * 
 * @version $Id: StrictCompoundCommandPessimistic.java,v 1.2 2009/11/04 17:18:40
 *          chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of
 *         Augsburg, Germany, www.ds-lab.org
 */
public class StrictCompoundCommandPessimistic extends StrictCompoundCommand
{

	public StrictCompoundCommandPessimistic()
	{
		isPessimistic = true;
	}

}
