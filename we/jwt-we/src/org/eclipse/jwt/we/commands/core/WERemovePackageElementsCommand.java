/**
 * File:    WERemovePackageElementsCommand
 * Created: 07.12.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial implementation
 *******************************************************************************/

package org.eclipse.jwt.we.commands.core;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.meta.model.core.PackageableElement;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.util.EMFHelper;
import org.eclipse.jwt.we.misc.views.LayoutDataManager;

/**
 * Removes elements from a package.
 * 
 * This command also removes all view data connected to the node by generating
 * corresponding RemoveCommands.
 * 
 * @version $Id: WERemovePackageElementsCommand,v 1.2 2009/11/04 17:18:24 chsaad
 *          Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class WERemovePackageElementsCommand extends RemoveCommand
{

	/**
	 * The workflow editor.
	 */
	private WEEditor weeditor;

	/**
	 * The Editing Domain.
	 */
	private EditingDomain editingDomain;

	/**
	 * The command to delete related view data.
	 */
	private Command removeViewDataCommand;


	/**
	 * Constructor.
	 * 
	 * @param domain
	 * @param owner
	 * @param feature
	 * @param collection
	 */
	public WERemovePackageElementsCommand(WEEditor weeditor, EditingDomain domain,
			EObject owner, EStructuralFeature feature, Collection<?> collection)
	{
		super(domain, owner, feature, collection);

		this.editingDomain = domain;
		this.weeditor = weeditor;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doExecute()
	 */
	@Override
	public void doExecute()
	{
		// collect all contained scopes
		Set containedScopes = new HashSet();

		for (Iterator iterator = collection.iterator(); iterator.hasNext();)
		{
			PackageableElement packElem = (PackageableElement) iterator.next();

			if (packElem instanceof Scope)
			{
				containedScopes.add((Scope) packElem);

				for (Iterator iterator2 = ((Scope) packElem).eAllContents(); iterator2
						.hasNext();)
				{
					Object object = (Object) iterator2.next();

					if (object instanceof Scope)
					{
						containedScopes.add((Scope) object);
					}
				}
			}
		}

		Set removeViewDataSet = new HashSet();
		Collection refEdgesCollection = EMFHelper.getAllContainedReferenceEdges(weeditor,
				containedScopes);
		Collection refsCollection = EMFHelper.getAllContainedReferences(weeditor,
				containedScopes);
		Collection laydataCollection = LayoutDataManager.getAllContainedLayoutDatas(
				weeditor, containedScopes);

		if (refEdgesCollection != null)
		{
			removeViewDataSet.addAll(refEdgesCollection);
		}
		if (refsCollection != null)
		{
			removeViewDataSet.addAll(refsCollection);
		}
		if (laydataCollection != null)
		{
			removeViewDataSet.addAll(laydataCollection);
		}

		// prepare remove view data
		removeViewDataCommand = DeleteCommand.create(editingDomain, removeViewDataSet);

		// remove all view data
		if (removeViewDataCommand.canExecute())
		{
			removeViewDataCommand.execute();
		}

		// remove the elements
		doExecuteOriginal();
	}


	protected void doExecuteOriginal()
	{
		super.doExecute();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doRedo()
	 */
	@Override
	public void doRedo()
	{
		// remove all view data
		if (removeViewDataCommand.canExecute())
		{
			removeViewDataCommand.redo();
		}

		// remove the elements
		super.doRedo();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doUndo()
	 */
	@Override
	public void doUndo()
	{
		// restore the view data
		super.doUndo();

		// restore all elements
		if (removeViewDataCommand.canUndo())
		{
			removeViewDataCommand.undo();
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.AbstractOverrideableCommand#doDispose()
	 */
	@Override
	public void doDispose()
	{
		super.doDispose();

		if (removeViewDataCommand != null)
		{
			removeViewDataCommand.dispose();
		}
	}

}