/**
 * File:    RemoveReferenceCommand.java
 * Created: 09.06.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- rewrote from scratch
 *******************************************************************************/

package org.eclipse.jwt.we.commands.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.model.view.Diagram;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.model.view.ReferenceEdge;

/**
 * Removes a {@link Reference} from its {@link Scope}.
 * 
 * This command also removes all {@link ReferenceEdge}s connected to the
 * reference by generating corresponding RemoveCommands.
 * 
 * @version $Id: RemoveReferenceCommand.java,v 1.3 2009/11/26 12:41:18 chsaad
 *          Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class RemoveReferenceCommand extends RemoveCommand
{

	/**
	 * The Editing Domain.
	 */
	private EditingDomain editingDomain;

	/**
	 * The commands to remove the corresponding edges.
	 */
	private CompoundCommand removeEdgesCommands;

	/**
	 * Store old layout scope.
	 */
	private Map<Reference, Scope> scopeMap;

	/**
	 * Store old ref elem.
	 */
	private Map<Reference, ReferenceableElement> refelemMap;

	/**
	 * Store containing diagram.
	 */
	private Map<Reference, Diagram> diagramMap;


	/**
	 * Constructor.
	 * 
	 * @param domain
	 * @param owner
	 * @param feature
	 * @param collection
	 */
	public RemoveReferenceCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection<?> collection)
	{
		super(domain, owner, feature, collection);

		this.editingDomain = domain;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doExecute()
	 */
	@Override
	public void doExecute()
	{
		// some references may already be deleted by CustomDeleteCommand if the
		// corresponding ReferenceableElement was deleted
		ArrayList newCollection = new ArrayList();
		for (Iterator iterator = collection.iterator(); iterator.hasNext();)
		{
			Reference reference = (Reference) iterator.next();

			if (reference.eContainer() != null)
			{
				newCollection.add(reference);
			}
		}
		collection = newCollection;

		// prepare remove edges
		removeEdgesCommands = new CompoundCommand();

		// for every Reference to be deleted
		for (Iterator i = collection.iterator(); i.hasNext();)
		{
			Reference reference = (Reference) i.next();

			// create RemoveCommands for ReferenceEdges
			// (RemoveReferenceEdgeCommand)
			if (!reference.getReferenceEdges().isEmpty())
			{
				removeEdgesCommands.append(DeleteCommand.create(editingDomain, reference
						.getReferenceEdges()));
			}
		}

		// remove all ReferenceEdges
		if (removeEdgesCommands.canExecute())
		{
			removeEdgesCommands.execute();
		}

		// remove from diagram and scope
		scopeMap = new HashMap<Reference, Scope>();
		refelemMap = new HashMap<Reference, ReferenceableElement>();
		diagramMap = new HashMap<Reference, Diagram>();

		for (Iterator iter = collection.iterator(); iter.hasNext();)
		{
			Reference ref = (Reference) iter.next();

			if (ref.eContainer() != null)
			{
				diagramMap.put(ref, (Diagram) ref.eContainer());
				((Diagram) ref.eContainer()).getReferences().remove(ref);
			}
			if (ref.getContainedIn() != null)
			{
				scopeMap.put(ref, ref.getContainedIn());
				ref.setContainedIn(null);
			}

			if (ref.getReference() != null)
			{
				refelemMap.put(ref, ref.getReference());
				ref.setReference(null);
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doRedo()
	 */
	@Override
	public void doRedo()
	{
		if (removeEdgesCommands.canExecute())
		{
			removeEdgesCommands.redo();
		}

		// remove from diagram and scope
		for (Iterator iter = collection.iterator(); iter.hasNext();)
		{
			Reference ref = (Reference) iter.next();

			if (ref.eContainer() != null)
			{
				((Diagram) ref.eContainer()).getReferences().remove(ref);
			}
			if (ref.getContainedIn() != null)
			{
				ref.setContainedIn(null);
			}
			if (ref.getReference() != null)
			{
				ref.setReference(null);
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doUndo()
	 */
	@Override
	public void doUndo()
	{
		for (Iterator iter = scopeMap.entrySet().iterator(); iter.hasNext();)
		{
			Entry<Reference, Scope> entry = (Entry<Reference, Scope>) iter.next();

			entry.getKey().setContainedIn(entry.getValue());
			entry.getKey().setReference(refelemMap.get(entry.getKey()));
			diagramMap.get(entry.getKey()).getReferences().add(entry.getKey());
		}

		// restore all ReferenceEdges
		if (!removeEdgesCommands.isEmpty())
		{
			removeEdgesCommands.undo();
		}
	}


	@Override
	public void doDispose()
	{
		super.doDispose();

		if (removeEdgesCommands != null)
		{
			removeEdgesCommands.dispose();
		}

		scopeMap.clear();
		refelemMap.clear();
		diagramMap.clear();
	}

}