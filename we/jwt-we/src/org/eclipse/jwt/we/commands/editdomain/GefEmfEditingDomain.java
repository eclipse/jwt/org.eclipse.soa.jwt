/**
 * File:    GefEmfEditingDomain.java
 * Created: 03.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- added interruptible command stack
 *******************************************************************************/

package org.eclipse.jwt.we.commands.editdomain;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jwt.we.commands.editdomain.internal.InterruptibleCommandStack;
import org.eclipse.ui.IEditorPart;

/**
 * A GEF editing domain that also contains a EMF editing domain.
 * 
 * @version $Id: GefEmfEditingDomain.java,v 1.5 2009-12-10 12:51:20 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 * @since 0.6.0
 */
public class GefEmfEditingDomain extends DefaultEditDomain
{

	/**
	 * The editing domain for EMF.
	 */
	private EditingDomain emfEditingDomain;

	/**
	 * A modified command stack that allows commands to be interrupted.
	 */
	private InterruptibleCommandStack iCS;


	/**
	 * @param editorPart
	 *            <code>null</code> or an IEditorPart.
	 */
	public GefEmfEditingDomain(IEditorPart editorPart)
	{
		super(editorPart);
	}


	/**
	 * @return Returns the emfEditingDomain.
	 */
	public EditingDomain getEmfEditingDomain()
	{
		return emfEditingDomain;
	}


	/**
	 * @param emfEditingDomain
	 *            The emfEditingDomain to set.
	 */
	public void setEmfEditingDomain(EditingDomain emfEditingDomain)
	{
		this.emfEditingDomain = emfEditingDomain;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.EditDomain#getCommandStack()
	 */
	@Override
	public CommandStack getCommandStack()
	{
		// use new, improved, all-purpose super duper command stack
		if (iCS == null)
		{
			iCS = new InterruptibleCommandStack();
		}

		return iCS;
	}

}
