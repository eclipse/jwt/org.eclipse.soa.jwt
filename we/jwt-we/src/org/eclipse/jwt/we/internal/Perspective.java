/**
 * File:    WEPerspective.java
 * Created: 17.04.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.internal;

import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;


/**
 * Initial perspective.
 * 
 * @version $Id: Perspective.java,v 1.5 2009-11-26 12:41:52 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class Perspective
		implements IPerspectiveFactory
{

	/**
	 * Id of the model editor.
	 */
	public static final String FOLDER_ID_MODEL_EDITOR = "workflow editor"; //$NON-NLS-1$

	/**
	 * Id of the property sheet.
	 */
	public static final String FOLDER_ID_PROPERTY = "we property"; //$NON-NLS-1$

	/**
	 * Id of the res nagivator sheet.
	 */
	public static final String FOLDER_NAV_PROPERTY = "we navigator"; //$NON-NLS-1$

	/**
	 * Ratio of the space to use for the model editor view.
	 */
	public static final float RATIO_MODEL_EDITOR_FOLDER = 0.75f;

	/**
	 * Ratio of the space to use for the property view.
	 */
	public static final float RATIO_PROPERTY_VIEW = 0.7f;

	/**
	 * Ratio of the space to use for the property view.
	 */
	public static final float RATIO_NAV_VIEW = 0.30f;


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	public void createInitialLayout(IPageLayout layout)
	{
		layout.setEditorAreaVisible(true);
		layout.addPerspectiveShortcut(PluginProperties.perspective_id);

		// outline right
		IFolderLayout mainLayout = layout.createFolder(FOLDER_ID_MODEL_EDITOR,
				IPageLayout.RIGHT, RATIO_MODEL_EDITOR_FOLDER, layout.getEditorArea());
		mainLayout.addView(IPageLayout.ID_OUTLINE);

		// property bottom
		IFolderLayout propertyLayout = layout.createFolder(FOLDER_ID_PROPERTY,
				IPageLayout.BOTTOM, RATIO_PROPERTY_VIEW, layout.getEditorArea());
		propertyLayout.addView(IPageLayout.ID_PROP_SHEET);

		// in plugin mode, add navigator
		if (GeneralHelper.isPluginMode())
		{
			IFolderLayout leftLayout = layout.createFolder(FOLDER_NAV_PROPERTY,
					IPageLayout.LEFT, RATIO_NAV_VIEW, layout.getEditorArea());
			leftLayout.addView("org.eclipse.jdt.ui.PackageExplorer"); //$NON-NLS-1$
		}

		/*
		 * LOG VIEW IN PROPERTY TAB
		 * 
		 * try { propertyLayout.addView("org.eclipse.pde.runtime.LogView"); } catch
		 * (Exception ex) { ex.printStackTrace(); }
		 */
	}

}
