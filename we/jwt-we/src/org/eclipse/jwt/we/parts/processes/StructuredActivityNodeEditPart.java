/**
 * File:    StructuredActivityNodeEditPart.java
 * Created: 19.05.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts.processes;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPolicy;
import org.eclipse.jwt.meta.model.processes.StructuredActivityNode;
import org.eclipse.jwt.we.misc.views.Views;
import org.eclipse.jwt.we.model.view.impl.ReferenceImpl;
import org.eclipse.jwt.we.parts.processes.policies.ScopeLayoutEditPolicy;

/**
 * EditPart for a {@link StructuredActivityNode}.
 * 
 * @version $Id: StructuredActivityNodeEditPart.java,v 1.9 2009/11/26 12:41:35
 *          chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 * @since 0.6.0
 */
public class StructuredActivityNodeEditPart extends ExecutableNodeEditPart
{

	/**
	 * Helper that provides the ScopeEditPart functionality.
	 */
	private final ScopeEditPartHelper scopeEditPartHelper = new ScopeEditPartHelper(this);


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.processes.ScopeEditPart#getModelClass()
	 */
	@Override
	public Class getModelClass()
	{
		return StructuredActivityNode.class;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.parts.processes.ScopeEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		installEditPolicy(EditPolicy.LAYOUT_ROLE, new ScopeLayoutEditPolicy());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@Override
	public List getModelChildren()
	{
		// get all children
		List childrenlist = scopeEditPartHelper.getModelChildren();

		// return only children which are visible in the active view
		List returnlist = new ArrayList();
		Views myviews = Views.getInstance();
		for (Object obj : childrenlist)
		{
			EObject eo = (EObject) obj;

			try
			{
				EClass e = eo.eClass();

				if (eo instanceof ReferenceImpl)
				{
					ReferenceImpl ri = (ReferenceImpl) eo;
					e = ri.basicGetReference().eClass();
				}

				if (myviews.displayObject(e))
				{
					returnlist.add(obj);
				}
			}
			catch (Exception ex)
			{
				returnlist.add(obj);
			}
		}
		return returnlist;
	}

}