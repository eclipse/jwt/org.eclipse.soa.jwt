/**
 * File:    ReferenceGraphicalNodeEditPolicy.java
 * Created: 01.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts.view.policies;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.commands.gefEmfAdapter.EmfToGefCommandAdapter;
import org.eclipse.jwt.we.commands.view.CreateReferenceEdgeCommand;
import org.eclipse.jwt.we.commands.view.ReconnectReferenceEdgeCommand;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.model.view.ReferenceEdge;
import org.eclipse.jwt.we.parts.view.ReferenceEditPart;

/**
 * GraphicalNodeEditPolicy for an {@link ReferenceEditPart}.
 * 
 * A {@link Reference} can be the target of a {@link ReferenceEdge}. For
 * convenience the connection can also be created inversed, i.e. the
 * {@link Reference} receives an connection create request and will set itself
 * as the target.
 * 
 * @version $Id: ReferenceGraphicalNodeEditPolicy.java,v 1.4 2008/01/31 09:51:27
 *          flautenba Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 */
public class ReferenceGraphicalNodeEditPolicy extends GraphicalNodeEditPolicy
{

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#
	 * getConnectionCreateCommand
	 * (org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCreateCommand(CreateConnectionRequest request)
	{
		Object target = getHost().getModel();
		Object parent = getHost().getParent().getModel();
		Object type = request.getNewObjectType();
		EditingDomain editingDomain = (EditingDomain) getHost().getAdapter(
				EditingDomain.class);

		// create reference edge
		if (target instanceof Reference && parent instanceof Scope && type != null
				&& type instanceof Class
				&& ReferenceEdge.class.isAssignableFrom((Class) type)
				&& editingDomain != null)
		{
			// create CreateReferenceEdgeCommand (with emf adapter)
			EmfToGefCommandAdapter connectCommand = new EmfToGefCommandAdapter(
					new CreateReferenceEdgeCommand(GeneralHelper
							.getInstanceForEObject((EObject) target), editingDomain));
			((CreateReferenceEdgeCommand) connectCommand.getEmfCommand())
					.setSource((Reference) target);

			request.setStartCommand(connectCommand);

			return connectCommand;
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#
	 * getConnectionCompleteCommand
	 * (org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCompleteCommand(CreateConnectionRequest request)
	{
		Command command = request.getStartCommand();
		Object target = getHost().getModel();

		// create reference edge
		if (command instanceof EmfToGefCommandAdapter
				&& ((EmfToGefCommandAdapter) command).getEmfCommand() instanceof CreateReferenceEdgeCommand
				&& target instanceof Reference)
		{
			EmfToGefCommandAdapter connectCommand = (EmfToGefCommandAdapter) command;

			((CreateReferenceEdgeCommand) connectCommand.getEmfCommand())
					.setTarget((Reference) target);
			((CreateReferenceEdgeCommand) connectCommand.getEmfCommand()).setDirection(1);

			return command;
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#
	 * getReconnectSourceCommand(org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectSourceCommand(ReconnectRequest request)
	{
		// referenceEdge: refEdge always is connected to the target-end of a
		// reference
		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#
	 * getReconnectTargetCommand(org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectTargetCommand(ReconnectRequest request)
	{
		Object newTarget = getHost().getModel();
		Object parent = getHost().getParent().getModel();
		Object edge = request.getConnectionEditPart().getModel();
		EditingDomain editingDomain = (EditingDomain) getHost().getAdapter(
				EditingDomain.class);

		// referenceEdge: refEdge always is connected to the target-end of a
		// reference
		if (newTarget instanceof Reference && parent instanceof Scope
				&& edge instanceof ReferenceEdge && editingDomain != null)
		{
			ReconnectReferenceEdgeCommand command = new ReconnectReferenceEdgeCommand(
					GeneralHelper.getActiveInstance(), editingDomain,
					(ReferenceEdge) edge);
			command.setNewReference((Reference) newTarget);

			return command;
		}

		return null;
	}
}
