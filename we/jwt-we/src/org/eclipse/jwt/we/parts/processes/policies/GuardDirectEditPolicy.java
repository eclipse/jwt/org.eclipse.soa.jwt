/**
 * File:    GuardDirectEditPolicy.java
 * Created: 20.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - getting EditPartFactory from factory registry.
 *******************************************************************************/

package org.eclipse.jwt.we.parts.processes.policies;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.jwt.meta.model.processes.Guard;
import org.eclipse.jwt.we.commands.gefEmfAdapter.EmfToGefCommandAdapter;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.parts.processes.GuardEditPart;


/**
 * {@link DirectEditPolicy} for the direct editing of {@link Guard}s.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class GuardDirectEditPolicy
		extends DirectEditPolicy
{

	/**
	 * A logger.
	 */
	private static final Logger logger = Logger.getLogger(GuardDirectEditPolicy.class);


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.DirectEditPolicy#getDirectEditCommand(org.eclipse.
	 * gef.requests.DirectEditRequest)
	 */
	@Override
	protected Command getDirectEditCommand(DirectEditRequest request)
	{
		logger.enter();

		Object model = getHost().getModel();
		EditingDomain editingDomain = (EditingDomain) getHost().getAdapter(
				EditingDomain.class);

		if (model instanceof Guard && editingDomain != null)
		{
			GuardEditPart guardEditPart = (GuardEditPart) getHost().getViewer()
					.getEditPartFactory().createEditPart(getHost().getRoot(), model);

			// get the active feature depending on the selected view (textual or short
			// description)
			EAttribute activeTextFeature = guardEditPart.getActiveFeature();

			return new EmfToGefCommandAdapter(
					org.eclipse.jwt.meta.PluginProperties.command_Rename_label,
					SetCommand.create(editingDomain, model, activeTextFeature, request
							.getCellEditor().getValue()));
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.DirectEditPolicy#showCurrentEditValue(org.eclipse.
	 * gef.requests.DirectEditRequest)
	 */
	@Override
	protected void showCurrentEditValue(DirectEditRequest request)
	{
		// do nothing
	}

}