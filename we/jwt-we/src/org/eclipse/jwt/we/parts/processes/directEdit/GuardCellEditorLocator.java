/**
 * File:    GuardCellEditorLocator.java
 * Created: 20.19.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.processes.directEdit;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jwt.we.figures.processes.GuardFigure;
import org.eclipse.swt.widgets.Text;


/**
 * {@link CellEditorLocator} for DirectEdit labels.
 * 
 * This class calculates the location and size of directEdit labels.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class GuardCellEditorLocator
		implements CellEditorLocator
{

	/**
	 * Mimium trim for cell editor.
	 */
	private static final int MINIMUM_TRIM = 1;

	/**
	 * The figure that uses directEdit.
	 */
	private GuardFigure figure;


	/**
	 * @param figure
	 *            The figure that uses directEdit.
	 */
	public GuardCellEditorLocator(GuardFigure figure)
	{
		setFigure(figure);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.CellEditorLocator#relocate(org.eclipse.jface.viewers.CellEditor)
	 */
	public void relocate(CellEditor celleditor)
	{
		Text text = (Text) celleditor.getControl();

		Rectangle labelSize = figure.getCellEditorArea();
		figure.translateToAbsolute(labelSize);

		org.eclipse.swt.graphics.Rectangle trim = text.computeTrim(0, 0, 0, 0);
		labelSize.translate(trim.x - MINIMUM_TRIM, trim.y - MINIMUM_TRIM);
		labelSize.width += trim.width + 2 * MINIMUM_TRIM;
		labelSize.height += trim.height + 2 * MINIMUM_TRIM;

		text.setBounds(labelSize.x, labelSize.y, labelSize.width, labelSize.height);
	}


	/**
	 * @return Returns the figure.
	 */
	public GuardFigure getFigure()
	{
		return figure;
	}


	/**
	 * @param figure
	 *            The figure to set.
	 */
	public void setFigure(GuardFigure figure)
	{
		this.figure = figure;
	}

}