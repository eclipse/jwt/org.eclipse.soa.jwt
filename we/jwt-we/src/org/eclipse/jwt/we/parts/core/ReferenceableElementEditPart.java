/**
 * File:    ReferenceableElementEditPart.java
 * Created: 19.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.core;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.meta.model.data.Data;
import org.eclipse.jwt.meta.model.organisations.Role;
import org.eclipse.jwt.meta.model.processes.Scope;


/**
 * EditPart for a {@link ReferenceableElement}. The figure is provided by a
 * {@link ReferenceableElementProxyEditPart} as it may be displayed more than once in a
 * {@link Scope} if multiple {@link References} point to the same
 * {@link ReferenceableElement}.
 * 
 * @version $Id: ReferenceableElementEditPart.java,v 1.8 2009-11-26 12:41:17 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ReferenceableElementEditPart
		extends NamedElementEditPart
{

	@Override
	public Class getModelClass()
	{
		if (this.getModel() instanceof Application) return Application.class;
		if (this.getModel() instanceof Data) return Data.class;
		if (this.getModel() instanceof Role) return Role.class;

		return ReferenceableElement.class;
	}


	/**
	 * This editPart is not selectable. Selections must be handled by the reference.
	 * 
	 * @return <code>false</code>
	 */
	@Override
	public boolean isSelectable()
	{
		return false;
	}


	/**
	 * Figure is provided by a ProxyEditPart
	 */
	@Override
	protected IFigure createFigure()
	{
		return null;
	}


	/**
	 * Figure is provided by a ProxyEditPart
	 */
	@Override
	protected void refreshVisuals()
	{
	}

}
