/**
 * File:    DirectDelegatingEditPolicy.java
 * Created: 04.04.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts.processes.policies;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.jwt.we.parts.core.ReferenceableElementEditPart;


/**
 * EditPolicy that forwards all request to a given {@link ReferenceableElementEditPart}.
 * 
 * @version $Id: DirectDelegatingEditPolicy.java,v 1.8 2009-12-10 12:30:53 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class DirectDelegatingEditPolicy
		extends AbstractEditPolicy
{

	/**
	 * A key to install this edit policy.
	 */
	public static final String DELEGATING_ROLE = "DirectDelegating"; //$NON-NLS-1$
	private ReferenceableElementEditPart ref;


	/**
	 * Constructs a new edit policy.
	 */
	public DirectDelegatingEditPolicy(ReferenceableElementEditPart ref)
	{
		this.ref = ref;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#getCommand(org.eclipse.gef.Request)
	 */
	@Override
	public Command getCommand(Request request)
	{
		Command command = null;
		command = ref.getCommand(request);
		return command;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#getTargetEditPart(org.eclipse.gef.Request)
	 */
	@Override
	public EditPart getTargetEditPart(Request request)
	{
		EditPart editPart;
		editPart = ((EditPart) ref).getTargetEditPart(request);
		if (editPart != null)
		{
			return editPart;
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#showSourceFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void showSourceFeedback(Request request)
	{
		ref.showSourceFeedback(request);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#eraseSourceFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void eraseSourceFeedback(Request request)
	{
		((EditPart) ref).eraseSourceFeedback(request);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#showTargetFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void showTargetFeedback(Request request)
	{
		((EditPart) ref).showTargetFeedback(request);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#eraseTargetFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void eraseTargetFeedback(Request request)
	{
		((EditPart) ref).eraseTargetFeedback(request);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#understandsRequest(org.eclipse.gef.Request)
	 */
	@Override
	public boolean understandsRequest(Request request)
	{
		if (((EditPart) ref).understandsRequest(request))
		{
			return true;
		}

		return false;
	}
}
