/**
 * File:    GuardDirectEditManager.java
 * Created: 20.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - getting EditPartFactory from factory registry.
 *******************************************************************************/

package org.eclipse.jwt.we.parts.processes.directEdit;

import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gef.tools.DirectEditManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jwt.meta.model.processes.Guard;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.parts.processes.GuardEditPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;


/**
 * {@link DirectEditManager} for a {@link Guard}.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class GuardDirectEditManager
		extends DirectEditManager
{

	/**
	 * A logger.
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(GuardDirectEditManager.class);


	/**
	 * @param source
	 *            The source edit part.
	 * @param locator
	 *            The locator.
	 */
	public GuardDirectEditManager(GraphicalEditPart source, CellEditorLocator locator)
	{
		super(source, null, locator);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.DirectEditManager#initCellEditor()
	 */
	@Override
	public void initCellEditor()
	{
		Object model = getEditPart().getModel();

		if (model instanceof Guard)
		{
			GuardEditPart guardEditPart = (GuardEditPart) Plugin.getInstance()
					.getFactoryRegistry().getEditPartFactory().createEditPart(
							getEditPart(), model);

			// get the action text (textual or short description) but do not trim or wrap
			String initialLabelText = guardEditPart.getActiveText(false);
			if (initialLabelText == null)
			{
				initialLabelText = ""; //$NON-NLS-1$
			}

			// set text
			getCellEditor().setValue(initialLabelText);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.tools.DirectEditManager#createCellEditorOn(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected CellEditor createCellEditorOn(Composite composite)
	{
		return new TextCellEditor(composite, SWT.MULTI | SWT.WRAP);
	}

}