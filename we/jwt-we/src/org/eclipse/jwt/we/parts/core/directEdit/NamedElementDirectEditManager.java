/**
 * File:    NamedElementDirectEditManager.java
 * Created: 02.02.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.core.directEdit;

import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gef.tools.DirectEditManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.meta.model.core.NamedElement;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;


/**
 * {@link DirectEditManager} for a {@link NamedElement}.
 * 
 * @version $Id: NamedElementDirectEditManager.java,v 1.8 2009-11-26 12:41:35 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class NamedElementDirectEditManager
		extends DirectEditManager
{

	/**
	 * A Logger.
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(NamedElementDirectEditManager.class);


	/**
	 * @param source
	 *            The source edit part.
	 * @param locator
	 *            The locator.
	 */
	public NamedElementDirectEditManager(GraphicalEditPart source,
			CellEditorLocator locator)
	{
		super(source, null, locator);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.DirectEditManager#initCellEditor()
	 */
	@Override
	public void initCellEditor()
	{
		Object model = getEditPart().getModel();

		if (model instanceof NamedElement)
		{
			String initialLabelText = ((NamedElement) model).getName();
			if (initialLabelText == null)
			{
				initialLabelText = ""; //$NON-NLS-1$
			}

			getCellEditor().setValue(initialLabelText);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.DirectEditManager#createCellEditorOn(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected CellEditor createCellEditorOn(Composite composite)
	{
		return new TextCellEditor(composite, SWT.MULTI | SWT.WRAP);
	}
}
