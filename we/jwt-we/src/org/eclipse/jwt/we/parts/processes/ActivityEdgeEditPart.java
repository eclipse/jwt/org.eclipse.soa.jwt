/**
 * File:    ActivityEdgeEditPart.java
 * Created: 10.01.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts.processes;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editpolicies.ConnectionEndpointEditPolicy;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;
import org.eclipse.jwt.meta.model.processes.ProcessesPackage;
import org.eclipse.jwt.we.figures.processes.ActivityEdgeFigure;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.parts.core.EdgeModelElementEditPart;
import org.eclipse.jwt.we.parts.processes.policies.ActivityEdgeEditPolicy;


/**
 * Edit part for an {@link ActivityEdge}.
 * 
 * @version $Id: ActivityEdgeEditPart.java,v 1.9 2009-11-26 12:41:35 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ActivityEdgeEditPart
		extends EdgeModelElementEditPart
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.EditPartAdapterSupport#getModelClass()
	 */
	Logger logger = Logger.getLogger(ActivityEdgeEditPart.class);


	@Override
	public Class getModelClass()
	{
		return ActivityEdge.class;
	}


	/**
	 * @return The {@link ActivityEdge} model.
	 */
	public ActivityEdge getActivityEdge()
	{
		return (ActivityEdge) getModel();
	}


	@Override
	protected List getModelChildren()
	{
		List mylist = new ArrayList();
		if (getActivityEdge().getGuard() != null)
		{

			mylist.add(getActivityEdge().getGuard());
			// logger.log(Level.INFO, "Activityedge with Guard: " +
			// getActivityEdge().getGuard().getShortdescription());
		}
		return mylist;
	}


	@Override
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		// Selection handle edit policy.
		// Makes the connection show a feedback, when selected by the user.
		installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE,
				new ConnectionEndpointEditPolicy());

		installEditPolicy(EditPolicy.LAYOUT_ROLE, new ActivityEdgeEditPolicy());
	}


	@Override
	public IFigure getContentPane()
	{
		return ((ActivityEdgeFigure) getFigure()).getContentPane();
	}


	@Override
	public void notifyChanged(Notification notification)
	{
		super.notifyChanged(notification);

		int featureId = notification.getFeatureID(ActivityEdge.class);
		int eventtype = notification.getEventType();
		switch (eventtype)
		{
			case Notification.ADD:
			case Notification.ADD_MANY:
			case Notification.REMOVE:
			case Notification.REMOVE_MANY:
			case Notification.SET:
			case Notification.UNSET:
				switch (featureId)
				{
					case ProcessesPackage.ACTIVITY_EDGE__GUARD:
						refreshChildren();
						break;
				}
				break;
		}
	}
}
