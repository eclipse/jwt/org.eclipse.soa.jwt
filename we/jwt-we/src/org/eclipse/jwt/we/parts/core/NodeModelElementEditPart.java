/**
 * File:    NodeModelElementEditPart.java
 * Created: 24.01.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.core;

import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.jwt.meta.model.core.ModelElement;


/**
 * Basic implementation of a {@link NodeEditPart} for a {@link ModelElement}.
 * 
 * @version $Id: NodeModelElementEditPart.java,v 1.7 2009-11-26 12:41:17 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class NodeModelElementEditPart
		extends GraphicalModelElementEditPart
		implements NodeEditPart
{

	/**
	 * An anchor used for connection on source edit parts.
	 */
	private ConnectionAnchor sourceConnectionAnchor;

	/**
	 * An anchor used for connection on target edit parts.
	 */
	private ConnectionAnchor targetConnectionAnchor;


	/**
	 * Returns the class of the model that the implementing EditPart represents.
	 * 
	 * Sublcasses <emp>must</emp> override this class to return their own model.
	 * 
	 * @return The class of the model.
	 */
	@Override
	public Class getModelClass()
	{
		return ModelElement.class;
	}


	/**
	 * Creates the ConnectionAnchor for this EditPart for source and target connections.
	 * 
	 * Subclasses should overwrite this method, if they need an anchor.
	 * 
	 * @return The connection anchor
	 */
	protected ConnectionAnchor createConnectionAnchor()
	{
		return null;
	}


	/**
	 * Creates the ConnectionAnchor for this EditPart for source connections.
	 * 
	 * @return The connection anchor
	 */
	protected ConnectionAnchor createSourceConnectionAnchor()
	{
		return createConnectionAnchor();
	}


	/**
	 * Creates the ConnectionAnchor for this EditPart for target connections.
	 * 
	 * @return The connection anchor
	 */
	protected ConnectionAnchor createTargetConnectionAnchor()
	{
		return createConnectionAnchor();
	}


	/**
	 * @return Returns the sourceConnectionAnchor.
	 */
	public ConnectionAnchor getSourceConnectionAnchor()
	{
		if (sourceConnectionAnchor == null)
		{
			sourceConnectionAnchor = createSourceConnectionAnchor();
		}

		return sourceConnectionAnchor;
	}


	/**
	 * @return Returns the targetConnectionAnchor.
	 */
	public ConnectionAnchor getTargetConnectionAnchor()
	{
		if (targetConnectionAnchor == null)
		{
			targetConnectionAnchor = createTargetConnectionAnchor();
		}

		return targetConnectionAnchor;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	public ConnectionAnchor getSourceConnectionAnchor(ConnectionEditPart connection)
	{
		return getSourceConnectionAnchor();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	public ConnectionAnchor getTargetConnectionAnchor(ConnectionEditPart connection)
	{
		return getTargetConnectionAnchor();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.Request)
	 */
	public ConnectionAnchor getSourceConnectionAnchor(Request request)
	{
		return getSourceConnectionAnchor();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.Request)
	 */
	public ConnectionAnchor getTargetConnectionAnchor(Request request)
	{
		return getTargetConnectionAnchor();
	}

}
