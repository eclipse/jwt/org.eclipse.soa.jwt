/**
 * File:    CommentEditPart.java
 * Created: 22.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.core;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jwt.meta.model.core.Comment;


/**
 * EditPart for a {@link Comment}.
 * 
 * @version $Id: CommentEditPart.java,v 1.8 2009-11-26 12:41:17 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class CommentEditPart
		extends NamedElementEditPart
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.core.NamedElementEditPart#getModelClass()
	 */
	@Override
	public Class getModelClass()
	{
		return Comment.class;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.core.GraphicalModelElementEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure()
	{
		setDoDirectEdit(true);
		
		return super.createFigure();
	}

}
