/**
 * File:    ActionGraphicalNodeEditPolicy.java
 * Created: 27.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts.processes.policies;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;
import org.eclipse.jwt.meta.model.processes.Action;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.commands.gefEmfAdapter.EmfToGefCommandAdapter;
import org.eclipse.jwt.we.commands.view.CreateReferenceEdgeCommand;
import org.eclipse.jwt.we.commands.view.ReconnectReferenceEdgeCommand;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.model.view.ReferenceEdge;
import org.eclipse.jwt.we.parts.processes.ActionEditPart;

/**
 * GraphicalNodeEditPolicy for an {@link ActionEditPart}.
 * 
 * An {@link Action} can be the source of a {@link ReferenceEdge}. For
 * convenience the connection can also be created inversed, i.e. the Action
 * receives an connection complete request and will set itself as the source.
 * 
 * @version $Id: ActionGraphicalNodeEditPolicy.java,v 1.4 2008/01/31 09:51:27
 *          flautenba Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of
 *         Augsburg, Germany, www.ds-lab.org
 */
public class ActionGraphicalNodeEditPolicy extends ActivityNodeGraphicalNodeEditPolicy
{

	/**
	 * Default constructor.
	 */
	public ActionGraphicalNodeEditPolicy(WEEditor weeditor)
	{
		super();
	}


	/**
	 * @param maximumInEdges
	 *            The maximum number of ingoing activity edges.
	 * @param maximumOutEdges
	 *            The maximum number of outgoing activity edges.
	 */
	public ActionGraphicalNodeEditPolicy(int maximumInEdges, int maximumOutEdges)
	{
		super(maximumInEdges, maximumOutEdges);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.jwt.we.parts.processes.policies.
	 * ActivityNodeGraphicalNodeEditPolicy
	 * #getConnectionCreateCommand(org.eclipse
	 * .gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCreateCommand(CreateConnectionRequest request)
	{
		Object source = getHost().getModel();
		Object parent = getHost().getParent().getModel();
		Object type = request.getNewObjectType();
		EditingDomain editingDomain = (EditingDomain) getHost().getAdapter(
				EditingDomain.class);

		// create reference edge
		if (source instanceof Action && parent instanceof Scope && type != null
				&& type instanceof Class
				&& ReferenceEdge.class.isAssignableFrom((Class) type)
				&& editingDomain != null)
		{
			// create CreateReferenceEdgeCommand (with emf adapter)

			EmfToGefCommandAdapter connectCommand = new EmfToGefCommandAdapter(
					new CreateReferenceEdgeCommand(GeneralHelper
							.getInstanceForEObject((EObject) source), editingDomain));
			((CreateReferenceEdgeCommand) connectCommand.getEmfCommand())
					.setSource((Action) source);

			request.setStartCommand(connectCommand);

			return connectCommand;
		}

		// create activity edge
		return super.getConnectionCreateCommand(request);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.jwt.we.parts.processes.policies.
	 * ActivityNodeGraphicalNodeEditPolicy
	 * #getConnectionCompleteCommand(org.eclipse
	 * .gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCompleteCommand(CreateConnectionRequest request)
	{
		Command command = request.getStartCommand();
		Object source = getHost().getModel();

		// create reference edge
		if (command instanceof EmfToGefCommandAdapter
				&& ((EmfToGefCommandAdapter) command).getEmfCommand() instanceof CreateReferenceEdgeCommand
				&& source instanceof Action)
		{
			EmfToGefCommandAdapter connectCommand = (EmfToGefCommandAdapter) command;

			((CreateReferenceEdgeCommand) connectCommand.getEmfCommand())
					.setTarget((Action) source);
			((CreateReferenceEdgeCommand) connectCommand.getEmfCommand()).setDirection(0);

			return command;
		}

		// create activity edge
		return super.getConnectionCompleteCommand(request);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.jwt.we.parts.processes.policies.
	 * ActivityNodeGraphicalNodeEditPolicy
	 * #getReconnectSourceCommand(org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectSourceCommand(ReconnectRequest request)
	{
		Object newSource = getHost().getModel();
		Object parent = getHost().getParent().getModel();
		Object edge = request.getConnectionEditPart().getModel();
		EditingDomain editingDomain = (EditingDomain) getHost().getAdapter(
				EditingDomain.class);

		// referenceEdge: connected to source-end of the action
		if (newSource instanceof Action && parent instanceof Scope
				&& edge instanceof ReferenceEdge && editingDomain != null)
		{
			ReconnectReferenceEdgeCommand command = new ReconnectReferenceEdgeCommand(
					GeneralHelper.getInstanceForEObject((EObject) newSource),
					editingDomain, (ReferenceEdge) edge);
			command.setNewAction((Action) newSource);

			return command;
		}

		// activityEdge: may be connected to source- or target-end
		return super.getReconnectSourceCommand(request);
	}


	protected Command getReconnectTargetCommand(ReconnectRequest request)
	{
		Object newTarget = getHost().getModel();
		Object parent = getHost().getParent().getModel();
		Object edge = request.getConnectionEditPart().getModel();
		EditingDomain editingDomain = (EditingDomain) getHost().getAdapter(
				EditingDomain.class);

		// referenceEdge: refEdge is always connected to the source-end of an
		// action
		if (newTarget instanceof Action && parent instanceof Scope
				&& edge instanceof ReferenceEdge && editingDomain != null)
		{
			return null;
		}

		// activityEdge: may be connected to source- or target-end
		return super.getReconnectTargetCommand(request);
	}
}
