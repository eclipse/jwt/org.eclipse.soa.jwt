/**
 * File:    ModelElementLayoutEditPolicy.java
 * Created: 24.02.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.core.policies;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.UnexecutableCommand;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.parts.core.GraphicalModelElementEditPart;


/**
 * LayoutEditPolicy for {@link GraphicalModelElementEditPart}.
 * 
 * @version $Id: ModelElementLayoutEditPolicy.java,v 1.6 2009-11-26 12:41:38 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class ModelElementLayoutEditPolicy
		extends XYLayoutEditPolicy
{

	/**
	 * A Logger.
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(ModelElementLayoutEditPolicy.class);


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#createChangeConstraintCommand(org.eclipse.gef.EditPart,
	 *      java.lang.Object)
	 */
	@Override
	protected Command createChangeConstraintCommand(EditPart child, Object constraint)
	{
		return UnexecutableCommand.INSTANCE;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.LayoutEditPolicy#getCreateCommand(org.eclipse.gef.requests.CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(CreateRequest request)
	{
		// Object childClass = request.getNewObjectType();
		// Object model = getHost().getModel();
		// Object constraint = getConstraintFor(request);

		// if (childClass instanceof Class
		// && Comment.class.isAssignableFrom((Class) childClass)
		// && model instanceof ModelElement && constraint instanceof Rectangle)
		// {
		// CreateCommentCommand createCommand = new CreateCommentCommand(
		// request,
		// (ModelElement) model);
		// SetGraphicalElementConstraintCommand constraintCommand = new
		// SetGraphicalElementConstraintCommand(
		// (Rectangle) constraint);
		// createCommand.addSetConstraintCommand(constraintCommand);

		// CompoundCommand command = new CompoundCommand();
		// command.add(createCommand);
		// command.add(constraintCommand);
		// return command;
		// }

		return UnexecutableCommand.INSTANCE;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.XYLayoutEditPolicy#getXYLayout()
	 */
	@Override
	protected XYLayout getXYLayout()
	{
		IFigure container = getLayoutContainer();
		return (XYLayout) container.getParent().getLayoutManager();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.XYLayoutEditPolicy#showSizeOnDropFeedback(org.eclipse.gef.requests.CreateRequest)
	 */
	@Override
	protected void showSizeOnDropFeedback(CreateRequest request)
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#createAddCommand(org.eclipse.gef.EditPart,
	 *      java.lang.Object)
	 */
	@Override
	protected Command createAddCommand(EditPart child, Object constraint)
	{
		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.LayoutEditPolicy#getDeleteDependantCommand(org.eclipse.gef.Request)
	 */
	@Override
	protected Command getDeleteDependantCommand(Request request)
	{
		return null;
	}
}
