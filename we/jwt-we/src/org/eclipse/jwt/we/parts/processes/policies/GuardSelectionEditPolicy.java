/**
 * File:    GuardSelectionEditPolicy.java
 * Created: 20.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.processes.policies;

import org.eclipse.gef.editpolicies.NonResizableEditPolicy;


/**
 * A selection feedback edit policy for guards.
 * 
 * @author Programming distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 */
public class GuardSelectionEditPolicy
		extends NonResizableEditPolicy
{

	/**
	 * Constructor.
	 */
	public GuardSelectionEditPolicy()
	{
		super();

		// do not allow dragging
		setDragAllowed(false);
	}

}