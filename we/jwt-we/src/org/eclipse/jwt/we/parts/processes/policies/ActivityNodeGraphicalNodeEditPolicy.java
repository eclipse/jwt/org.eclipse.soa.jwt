/**
 * File:    ActivityNodeGraphicalNodeEditPolicy.java
 * Created: 15.01.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts.processes.policies;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;
import org.eclipse.jwt.meta.commands.processes.CreateActivityEdgeCommand;
import org.eclipse.jwt.meta.commands.processes.ReconnectActivityEdgeCommand;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;
import org.eclipse.jwt.meta.model.processes.ActivityNode;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.commands.gefEmfAdapter.EmfToGefCommandAdapter;


/**
 * GraphicalNodeEditPolicy for an activity node.
 * 
 * @version $Id: ActivityNodeGraphicalNodeEditPolicy.java,v 1.4 2008/01/31 09:51:27
 *          flautenba Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class ActivityNodeGraphicalNodeEditPolicy
		extends GraphicalNodeEditPolicy
{

	/**
	 * Stores the maximum number of ingoing edges.
	 */
	private int maximumInEdges = Integer.MAX_VALUE;

	/**
	 * Stores the maximum number of outgoing edges.
	 */
	private int maximumOutEdges = Integer.MAX_VALUE;


	/**
	 * Default constructor.
	 */
	public ActivityNodeGraphicalNodeEditPolicy()
	{
	}


	/**
	 * @param maximumInEdges
	 *            The maximum number of ingoing edges.
	 * @param maximumOutEdges
	 *            The maximum number of outgoing edges.
	 */
	public ActivityNodeGraphicalNodeEditPolicy(int maximumInEdges, int maximumOutEdges)
	{
		this.maximumOutEdges = maximumOutEdges;
		this.maximumInEdges = maximumInEdges;
	}


	/**
	 * @return Returns the maximumInNodes.
	 */
	public int getMaximumInActivityEdges()
	{
		return maximumInEdges;
	}


	/**
	 * @param maximumInNodes
	 *            The maximumInNodes to set.
	 */
	public void setMaximumInActivityEdges(int maximumInNodes)
	{
		this.maximumInEdges = maximumInNodes;
	}


	/**
	 * @return Returns the maximumOutNodes.
	 */
	public int getMaximumOutActivityEdges()
	{
		return maximumOutEdges;
	}


	/**
	 * @param maximumOutNodes
	 *            The maximumOutNodes to set.
	 */
	public void setMaximumOutActivityEdges(int maximumOutNodes)
	{
		this.maximumOutEdges = maximumOutNodes;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getConnectionCreateCommand
	 * (org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCreateCommand(CreateConnectionRequest request)
	{
		Object source = getHost().getModel();
		Object parent = getHost().getParent().getModel();
		Object type = request.getNewObjectType();
		EditingDomain editingDomain = (EditingDomain) getHost().getAdapter(
				EditingDomain.class);

		// create connection (activityedge) from activity node
		if (source instanceof ActivityNode && parent instanceof Scope && type != null
				&& type instanceof Class
				&& ActivityEdge.class.isAssignableFrom((Class) type)
				&& editingDomain != null)
		{
			CreateActivityEdgeCommand connectCommand = new CreateActivityEdgeCommand(
					editingDomain);
			connectCommand.setSource((ActivityNode) source);
			connectCommand.setMaximumOutEdges(maximumOutEdges);

			EmfToGefCommandAdapter connectCommandEMF = null;

			// only if the start activitynode has no outgoing activityedges
			if (connectCommand.checkMaxInOutConnections())
			{
				connectCommandEMF = new EmfToGefCommandAdapter(connectCommand);
			}

			request.setStartCommand(connectCommandEMF);
			return connectCommandEMF;
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getConnectionCompleteCommand
	 * (org.eclipse.gef.requests.CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCompleteCommand(CreateConnectionRequest request)
	{
		Command command = request.getStartCommand();
		Object target = getHost().getModel();

		// complete connection (activityedge) to activity node
		if (command instanceof EmfToGefCommandAdapter
				&& ((EmfToGefCommandAdapter) command).getEmfCommand() instanceof CreateActivityEdgeCommand
				&& target instanceof ActivityNode)
		{
			CreateActivityEdgeCommand connectCommand = (CreateActivityEdgeCommand) ((EmfToGefCommandAdapter) command)
					.getEmfCommand();

			connectCommand.setTarget((ActivityNode) target);
			connectCommand.setMaximumInEdges(maximumInEdges);

			return command;
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getReconnectSourceCommand(
	 * org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectSourceCommand(ReconnectRequest request)
	{
		Object edge = request.getConnectionEditPart().getModel();
		Object newSource = getHost().getModel();
		Object parent = getHost().getParent().getModel();
		EditingDomain editingDomain = (EditingDomain) getHost().getAdapter(
				EditingDomain.class);

		// reconnect the source end of an activityedge
		if (edge instanceof ActivityEdge && newSource instanceof ActivityNode
				&& parent instanceof Scope && editingDomain != null)
		{
			ReconnectActivityEdgeCommand command = new ReconnectActivityEdgeCommand(
					editingDomain, (ActivityEdge) edge);
			command.setNewSource((ActivityNode) newSource);
			command.setMaximumOutEdges(maximumOutEdges);

			return new EmfToGefCommandAdapter(command);
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getReconnectTargetCommand(
	 * org.eclipse.gef.requests.ReconnectRequest)
	 */
	@Override
	protected Command getReconnectTargetCommand(ReconnectRequest request)
	{
		Object edge = request.getConnectionEditPart().getModel();
		Object newTarget = getHost().getModel();
		Object parent = getHost().getParent().getModel();
		EditingDomain editingDomain = (EditingDomain) getHost().getAdapter(
				EditingDomain.class);

		// reconnect the target end of an activityedge
		if (edge instanceof ActivityEdge && newTarget instanceof ActivityNode
				&& parent instanceof Scope && editingDomain != null)
		{
			ReconnectActivityEdgeCommand command = new ReconnectActivityEdgeCommand(
					editingDomain, (ActivityEdge) edge);
			command.setNewTarget((ActivityNode) newTarget);
			command.setMaximumInEdges(maximumInEdges);

			return new EmfToGefCommandAdapter(command);
		}

		return null;
	}
}
