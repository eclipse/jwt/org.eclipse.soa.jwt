/**
 * File:    GuardSpecificationEditPart.java
 * Created: 22.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.processes;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPart;
import org.eclipse.jwt.meta.model.processes.GuardSpecification;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.parts.core.GraphicalModelElementEditPart;


/**
 * EditPart for a {@link GuardSpecification}.
 * 
 * @version $Id: GuardSpecificationEditPart.java,v 1.9 2009-11-26 12:41:35 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class GuardSpecificationEditPart
		extends GraphicalModelElementEditPart
{

	Logger logger = Logger.getLogger(GuardSpecificationEditPart.class);


	public GuardSpecificationEditPart()
	{
		super();
	}


	@Override
	public Class getModelClass()
	{
		return GuardSpecification.class;
	}


	@Override
	public boolean isSelectable()
	{
		return false;
	}


	/**
	 * Retrieves the parent node that is the guard of this node
	 * 
	 * @param ep
	 * @return
	 */
	private EditPart getGuardParent(EditPart ep)
	{
		if (ep instanceof GuardEditPart)
			return ep;
		return getGuardParent(ep.getParent());
	}


	/**
	 * @return The {@link GuardSpecification} model.
	 */
	public GuardSpecification getGuardSpecification()
	{
		return (GuardSpecification) getModel();
	}


	@Override
	protected List getModelChildren()
	{
		return getGuardSpecification().getSubSpecification();
	}


	/**
	 * This is some kind of "hack": The normal way of adding my guard parent to my adapter
	 * lists for a notification if changing a propertie didnt work - the diagram would
	 * crash on switching btw. different activities. So i now call the notifyChanged
	 * method manually. This is not as nice as it should be, but it works! (and after
	 * several hours of code review / debugging this is as good as it gets without ruining
	 * my nervs completely :))
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		super.notifyChanged(notification);
		try
		{
			if (notification.getNotifier() instanceof GuardSpecification)
			{
				GuardEditPart gep = (GuardEditPart) getGuardParent(this);
				gep.notifyChanged(notification);
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
