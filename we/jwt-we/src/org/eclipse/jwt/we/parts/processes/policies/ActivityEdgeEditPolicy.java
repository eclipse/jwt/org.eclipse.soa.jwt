/**
 * File:    ActivityEdgeEditPolicy.java
 * Created: 08.10.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts.processes.policies;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.UnexecutableCommand;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.jwt.meta.commands.JWTCreateChildCommand;
import org.eclipse.jwt.meta.commands.processes.AddGuardtoActivitiyEdgeCommand;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;
import org.eclipse.jwt.meta.model.processes.Guard;
import org.eclipse.jwt.meta.model.processes.ProcessesPackage;
import org.eclipse.jwt.we.commands.gefEmfAdapter.EmfToGefCommandAdapter;
import org.eclipse.jwt.we.misc.logging.Logger;


/**
 * Edit Policy for an {@link ActivityEdge}.
 * 
 * @version $Id: ActivityEdgeEditPolicy.java,v 1.8 2009-11-26 12:41:33 chsaad Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class ActivityEdgeEditPolicy
		extends LayoutEditPolicy
{

	Logger logger = Logger.getLogger(ActivityEdgeEditPolicy.class);


	public ActivityEdgeEditPolicy()
	{
		super();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.LayoutEditPolicy#createChildEditPolicy(org.eclipse
	 * .gef.EditPart)
	 */
	@Override
	protected EditPolicy createChildEditPolicy(EditPart child)
	{
		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.LayoutEditPolicy#getCreateCommand(org.eclipse.gef.
	 * requests.CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(CreateRequest request)
	{

		Object childClass = request.getNewObjectType();
		Object model = getHost().getModel();
		EditingDomain editingDomain = (EditingDomain) getHost().getAdapter(
				EditingDomain.class);
		// Guard
		if (Guard.class.isAssignableFrom((Class) childClass))
		{
			// create and add to scope (which is the container)
			Command creationCommand = new EmfToGefCommandAdapter(
					new JWTCreateChildCommand(editingDomain, model,
							ProcessesPackage.Literals.ACTIVITY_EDGE__GUARD, request
									.getNewObject()));

			Command activityedgecommand = new EmfToGefCommandAdapter(
					new AddGuardtoActivitiyEdgeCommand((ActivityEdge) model, request
							.getNewObject()));

			return creationCommand.chain(activityedgecommand);
		}
		return UnexecutableCommand.INSTANCE;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.LayoutEditPolicy#getMoveChildrenCommand(org.eclipse
	 * .gef.Request)
	 */
	@Override
	protected Command getMoveChildrenCommand(Request request)
	{
		return null;
	}

}
