/**
 * File:    DoubleClickCommandFactory.java
 * Created: 20.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009   Open Wide <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France 
 *    	- initial API
 *******************************************************************************/

package org.eclipse.jwt.we.parts.core.doubleClick;

import org.eclipse.gef.EditDomain;
import org.eclipse.jwt.meta.model.core.ModelElement;

/**
 * 
 * This interface is intended to be used by <b>org.eclipse.jwt.we.doubleclick</b>
 * extensions.
 * It specifies a factory that, in case it can apply to a model element,
 * creates a command and adds it to the command stack.
 * 
 * @since 0.6.0
 * @author Mickael Istria
 */
public interface DoubleClickHandler {

	/**
	 * Test run by clients to check whether this command applies to the
	 * specified element.
	 * @param modelElement
	 * @return True if this factory can create a command for this element
	 */
	public boolean appliesTo(ModelElement modelElement);
	
	/**
	 * process double click for the given element.
	 * 
	 * At this step, the <i>appliesTo</i> method has already tested
	 * that the element conforms to the factory requirements. 
	 * @param modelElement
	 * @param editDomain 
	 */
	public void processDoubleClick(ModelElement modelElement, EditDomain editDomain);
}
