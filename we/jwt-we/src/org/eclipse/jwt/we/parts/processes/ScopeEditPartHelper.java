/**
 * File:    ScopeEditPartHelper.java
 * Created: 22.05.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts.processes;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartListener;
import org.eclipse.gef.editparts.AbstractEditPart;
import org.eclipse.jwt.meta.model.processes.ProcessesPackage;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.util.EMFHelper;
import org.eclipse.jwt.we.model.view.Diagram;
import org.eclipse.jwt.we.model.view.ViewPackage;
import org.eclipse.jwt.we.parts.core.GraphicalModelElementEditPart;

/**
 * Helper for an EditPart for a {@link Scope}.
 * 
 * <p>
 * This class contains all functionality of the ScopeEditPart. This is done in
 * the helper class in order to support multiple inheritance from the
 * ScopeEditPart.
 * </p>
 * 
 * <p>
 * The following methods from the EditPart must be delegated to the helper:
 * </p>
 * <ul>
 * <li>{@link AbstractEditPart}.getModelChildren()</li>
 * </ul>
 * 
 * @version $Id: ScopeEditPartHelper.java,v 1.9 2009-12-21 10:30:46 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 * @since 0.6.0
 */
public class ScopeEditPartHelper extends AdapterImpl implements EditPartListener
{

	/**
	 * The EditPart this helper supports.
	 * 
	 * <p>
	 * Never <code>null</code>.
	 * </p>
	 */
	private final EditPart editPart;


	/**
	 * @param editPart
	 *            The EditPart this helper supports.
	 */
	public ScopeEditPartHelper(EditPart editPart)
	{
		this.editPart = editPart;

		// this also asserts editPart != null
		editPart.addEditPartListener(this);
	}


	/**
	 * @return The model element.
	 */
	public Object getModel()
	{
		return editPart.getModel();
	}


	/**
	 * @return The scope model element.
	 */
	public Scope getScope()
	{
		return (Scope) editPart.getModel();
	}


	/**
	 * @return The children of the Scope model.
	 */
	public List getModelChildren()
	{
		BasicEList result = new BasicEList(getScope().getNodes());
		result.addAll(EMFHelper.getReferencesForScope(
				((GraphicalModelElementEditPart) editPart).getWEEditor(), getScope()));

		return result;
	}


	/**
	 * Adds this class as adapter to a notifier.
	 * 
	 * @param notifier
	 *            The notifier.
	 */
	private void addNotify(Notifier notifier)
	{
		if (notifier != null)
		{
			notifier.eAdapters().add(this);
		}
	}


	/**
	 * Removes this class from a notifier.
	 * 
	 * @param notifier
	 *            The notifier.
	 */
	private void removeNotify(Notifier notifier)
	{
		if (notifier != null)
		{
			notifier.eAdapters().remove(this);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.common.notify.impl.AdapterImpl#notifyChanged(org.eclipse
	 * .emf.common .notify.Notification)
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		int featureId = notification.getFeatureID(Scope.class);

		switch (featureId)
		{
			case ProcessesPackage.SCOPE__NODES:
				editPart.refresh();
				break;
		}

		featureId = notification.getFeatureID(Diagram.class);

		switch (featureId)
		{
			case ViewPackage.DIAGRAM__REFERENCES:
				editPart.refresh();
				break;
		}

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartListener#partActivated(org.eclipse.gef.EditPart)
	 */
	public void partActivated(EditPart editpart)
	{
		addNotify(getScope());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartListener#partDeactivated(org.eclipse.gef.EditPart
	 * )
	 */
	public void partDeactivated(EditPart editpart)
	{
		removeNotify(getScope());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartListener#removingChild(org.eclipse.gef.EditPart,
	 * int)
	 */
	public void removingChild(EditPart child, int index)
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartListener#childAdded(org.eclipse.gef.EditPart,
	 * int)
	 */
	public void childAdded(EditPart child, int index)
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartListener#selectedStateChanged(org.eclipse.gef
	 * .EditPart)
	 */
	public void selectedStateChanged(EditPart editpart)
	{
	}
}
