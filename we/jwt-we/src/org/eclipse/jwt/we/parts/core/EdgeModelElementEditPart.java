/**
 * File:    EdgeModelElementEditPart.java
 * Created: 24.01.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - getting FigureFactory from factory registry.
 *******************************************************************************/


package org.eclipse.jwt.we.parts.core;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.jwt.meta.model.core.ModelElement;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.parts.EditPartAdapter;
import org.eclipse.jwt.we.parts.internal.EditPartAdapterSupport;


/**
 * Basic implementation of a connection EditPart for a {@link ModelElement}.
 * 
 * <p>
 * This class can also be used for elements that are not subclasses of a
 * {@link ModelElement} but also represent a connection.
 * </p>
 * 
 * <p>
 * Subclasses <emp>must</emp> override {@link #getModelClass()}.
 * </p>
 * 
 * @version $Id: EdgeModelElementEditPart.java,v 1.9 2009-11-26 12:41:17 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class EdgeModelElementEditPart
		extends AbstractConnectionEditPart
		implements EditPartAdapterSupport
{

	/**
	 * Adapter which forwards the changes to this edit part.
	 */
	private EditPartAdapter adapter = new EditPartAdapter(this);


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.EditPartAdapterSupport#getAdapter()
	 */
	public Adapter getAdapter()
	{
		return adapter;
	}


	/**
	 * Returns the class of the model that the implementing EditPart represents.
	 * 
	 * Sublcasses <emp>must</emp> override this class to return their own model.
	 * 
	 * @return The class of the model.
	 */
	public Class getModelClass()
	{
		return ModelElement.class;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure()
	{
		// create the figure for the model class using the figure factory
		IFigure figure = Plugin.getInstance().getFactoryRegistry().getFigureFactory().createFigure(getModelClass());

		return figure;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#getAdapter(java.lang.Class)
	 */
	@Override
	public Object getAdapter(Class key)
	{
		if (EditingDomain.class.isAssignableFrom(key))
		{
			return adapter.getEmfEditingDomain();
		}

		return super.getAdapter(key);
	}


	/**
	 * Does nothing. Sublcasses should override this method.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.EditPartAdapterSupport#notifyChanged(org.eclipse.emf.common.notify.Notification)
	 */
	public void notifyChanged(Notification notification)
	{
	}
}
