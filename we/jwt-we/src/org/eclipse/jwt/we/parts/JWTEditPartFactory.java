/**
 * File:    JwtEditPartFactory.java
 * Created: 09.12.2005
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - moving class name from EditPartFactory to JwtEditPartFactory.
 *******************************************************************************/

package org.eclipse.jwt.we.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.jwt.we.misc.logging.Logger;

/**
 * Factory that maps model elements to edit parts.
 * 
 * The EditPartFactory uses an {@link EditPartAdapterFactory} to create the
 * EditParts. That means that the model elements are actually adapted to
 * {@link EditPartAdapter}s.
 * 
 * @version $Id: JWTEditPartFactory.java,v 1.5 2009-12-21 10:30:47 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 * @since 0.6.0
 */
public class JWTEditPartFactory implements org.eclipse.gef.EditPartFactory
{

	/**
	 * The Logger.
	 */
	private static final Logger logger = Logger.getLogger(JWTEditPartFactory.class);

	/**
	 * Factory for EditParts.
	 */
	private EditPartAdapterFactory adapterFactory = new EditPartAdapterFactory(
			JWTEditPartFactory.class.getPackage().getName(),
			EditPartAdapterFactory.MODEL_VIEW_ROOT_PACKAGE, null);


	/**
	 * Constructor.
	 */
	public JWTEditPartFactory()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(EditPart context, Object model)
	{
		if (model == null)
		{
			return null;
		}

		logger.debug("Creating EditPart for \"" + model.getClass().getName() + "\"."); //$NON-NLS-1$ //$NON-NLS-2$

		EditPartAdapter adapter = (EditPartAdapter) adapterFactory.adapt(model,
				EditPartAdapter.class);

		if (adapter == null)
		{
			return null;
		}

		return adapter.getEditPart();
	}

}
