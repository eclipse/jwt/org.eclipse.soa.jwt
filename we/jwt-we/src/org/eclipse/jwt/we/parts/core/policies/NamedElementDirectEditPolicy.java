/**
 * File:    NamedElementDirectEditPolicy.java
 * Created: 02.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts.core.policies;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.jwt.meta.model.core.CorePackage;
import org.eclipse.jwt.meta.model.core.NamedElement;
import org.eclipse.jwt.we.commands.gefEmfAdapter.EmfToGefCommandAdapter;
import org.eclipse.jwt.we.figures.core.NamedElementFigure;
import org.eclipse.jwt.we.misc.logging.Logger;


/**
 * {@link DirectEditPolicy} for the direct editing of {@link NamedElement}s.
 * 
 * @version $Id: NamedElementDirectEditPolicy.java,v 1.6.2.2 2009/03/09 13:04:41 chsaad
 *          Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class NamedElementDirectEditPolicy
		extends DirectEditPolicy
{

	/**
	 * A Logger.
	 */
	private static final Logger logger = Logger
			.getLogger(NamedElementDirectEditPolicy.class);


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.DirectEditPolicy#getDirectEditCommand(org.eclipse.
	 * gef.requests.DirectEditRequest)
	 */
	@Override
	protected Command getDirectEditCommand(DirectEditRequest request)
	{
		logger.enter();

		Object model = getHost().getModel();
		EditingDomain editingDomain = (EditingDomain) getHost().getAdapter(
				EditingDomain.class);

		if (model instanceof NamedElement && editingDomain != null)
		{
			return new EmfToGefCommandAdapter(
					org.eclipse.jwt.meta.PluginProperties.command_Rename_label,
					SetCommand.create(editingDomain, model, CorePackage.eINSTANCE
							.getNamedElement_Name(), request.getCellEditor().getValue()));
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.DirectEditPolicy#showCurrentEditValue(org.eclipse.
	 * gef.requests.DirectEditRequest)
	 */
	@Override
	protected void showCurrentEditValue(DirectEditRequest request)
	{
		logger.enter();

		String text = (String) request.getCellEditor().getValue();
		IFigure figure = getHostFigure();

		if (figure instanceof NamedElementFigure)
		{
			((NamedElementFigure) figure).setName(text);
		}
	}
}
