/**
 * File:    NamedElementEditPart.java
 * Created: 02.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.core;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.tools.DirectEditManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.meta.model.core.CorePackage;
import org.eclipse.jwt.meta.model.core.NamedElement;
import org.eclipse.jwt.we.figures.core.NamedElementFigure;
import org.eclipse.jwt.we.parts.core.directEdit.NamedElementCellEditorLocator;
import org.eclipse.jwt.we.parts.core.directEdit.NamedElementDirectEditManager;
import org.eclipse.jwt.we.parts.core.policies.NamedElementDirectEditPolicy;


/**
 * EditPart for a {@link NamedElement}.
 * 
 * @version $Id: NamedElementEditPart.java,v 1.7 2009-11-26 12:41:17 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class NamedElementEditPart
		extends NodeModelElementEditPart
{

	/**
	 * The manager for direct edit.
	 */
	private DirectEditManager directEditManager;

	/**
	 * Indicates if directEdit should be done.
	 */
	private boolean doDirectEdit = false;


	/**
	 * Default constructor.
	 */
	public NamedElementEditPart()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.core.NodeModelElementEditPart#getModelClass()
	 */
	@Override
	public Class getModelClass()
	{
		return NamedElement.class;
	}


	/**
	 * Set it to <code>true</code>, if directEdit requests should be proceeded.
	 * Otherwise they are ignored.
	 * 
	 * @param doDirectEdit
	 *            The doDirectEdit to set.
	 */
	public void setDoDirectEdit(boolean doDirectEdit)
	{
		this.doDirectEdit = doDirectEdit;

		if (doDirectEdit)
		{
			installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
					new NamedElementDirectEditPolicy());
		}
		else
		{
			installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, null);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#performRequest(org.eclipse.gef.Request)
	 */
	@Override
	public void performRequest(Request request)
	{
		super.performRequest(request);

		if (doDirectEdit && request.getType() == RequestConstants.REQ_DIRECT_EDIT)
		{
			performDirectEdit();
		}
	}


	/**
	 * Performs a directEdit request.
	 * 
	 * Subclasses should override this method if they need a directEdit.
	 */
	protected void performDirectEdit()
	{
		if (directEditManager == null)
		{
			directEditManager = new NamedElementDirectEditManager(this,
					new NamedElementCellEditorLocator((NamedElementFigure) getFigure()));
		}

		directEditManager.show();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.core.GraphicalModelElementEditPart#notifyChanged(org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		super.notifyChanged(notification);

		int featureId = notification.getFeatureID(NamedElement.class);

		switch (notification.getEventType())
		{
			case Notification.SET:
			case Notification.UNSET:
				switch (featureId)
				{
					case CorePackage.NAMED_ELEMENT__NAME:
					case CorePackage.NAMED_ELEMENT__ICON:
						refreshVisuals();
				}
				break;
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals()
	{
		super.refreshVisuals();

		// set the name and icon to the figure, if the figure supports it
		IFigure figure = getFigure();
		NamedElement element = (NamedElement) getModel();

		if (figure instanceof NamedElementFigure)
		{
			NamedElementFigure namedFigure = (NamedElementFigure) figure;
			namedFigure.setName(element.getName());

			// if (element.getIcon() == null || !element.getIcon().equals(backupicon))
			{
				IItemLabelProvider itemLabelProvider = (IItemLabelProvider) getModelAdapterFactory()
						.adapt(element, IItemLabelProvider.class);
				if (itemLabelProvider != null)
				{
					Object icon = itemLabelProvider.getImage(element);
					if (icon instanceof ImageDescriptor)
					{
						namedFigure.setIcon((ImageDescriptor) icon);
					}
				}
			}
		}
	}
}
