/**
 * File:    ActivityLinkNodeEditPart.java
 * Created: 14.12.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts.processes;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.jwt.meta.model.processes.Action;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.jwt.meta.model.processes.ActivityLinkNode;
import org.eclipse.jwt.meta.model.processes.ProcessesFactory;
import org.eclipse.jwt.meta.model.processes.ProcessesPackage;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.parts.processes.policies.ActionGraphicalNodeEditPolicy;


/**
 * EditPart for a {@link ActivityLinkNode}.
 * 
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ActivityLinkNodeEditPart
		extends ExecutableNodeEditPart
{

	/**
	 * Maximum number of ingoing edges.
	 */
	private static final int MAXIMUM_IN_EDGES = 1;

	/**
	 * Maximum number of outgoing edges.
	 */
	private static final int MAXIMUM_OUT_EDGES = 1;


	/**
	 * Constructor.
	 */
	public ActivityLinkNodeEditPart()
	{
		setMaximumInActivityEdges(MAXIMUM_IN_EDGES);
		setMaximumOutActivityEdges(MAXIMUM_OUT_EDGES);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.processes.ScopeEditPart#getModelClass()
	 */
	@Override
	public Class getModelClass()
	{
		return ActivityLinkNode.class;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.core.GraphicalModelElementEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure()
	{
		setDoDirectEdit(true);

		return super.createFigure();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editparts.AbstractEditPart#performRequest(org.eclipse.gef.Request)
	 */
	@Override
	public void performRequest(Request request)
	{
		super.performRequest(request);

		if (request.getType() == RequestConstants.REQ_OPEN)
		{
			// the linked activity
			Activity linkedactivity = ((ActivityLinkNode) getModel()).getLinksto();
			WEEditor ape = getWEEditor();

			if (ape != null)
			{
				// if no activity is linked, search for the package recursivly and
				// create an activity in it
				if (linkedactivity == null)
				{

					EObject object = (EObject) getModel();

					while ((object.eContainer() != null)
							&& !(object instanceof org.eclipse.jwt.meta.model.core.Package))
					{
						object = object.eContainer();
					}

					Activity newActivity = ProcessesFactory.eINSTANCE.createActivity();
					((org.eclipse.jwt.meta.model.core.Package) object).getElements().add(
							newActivity);
					newActivity.setName(PluginProperties.wizards_ModelWizardActivity_std);
					linkedactivity = newActivity;
					((ActivityLinkNode) getModel()).setLinksto(linkedactivity);
				}

				// open activity in new page
				ape.openActivityInNewPage(linkedactivity);
			}
		}

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.processes.ScopeEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE,
				new ActionGraphicalNodeEditPolicy(getMaximumInActivityEdges(),
						getMaximumOutActivityEdges()));
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.parts.processes.ActivityNodeEditPart#notifyChanged(org.eclipse
	 * .emf.common.notify.Notification)
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		super.notifyChanged(notification);

		int featureId = notification.getFeatureID(Action.class);

		switch (featureId)
		{
			// load icon from linked activity if possible
			case ProcessesPackage.ACTIVITY_LINK_NODE__LINKSTO:
				this.refreshVisuals();
				break;
		}
	}

}