/**
 * File:    ReferenceableElementProxyEditPart.java
 * Created: 19.12.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.core;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.editparts.AbstractEditPart;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.parts.processes.policies.DirectDelegatingEditPolicy;


/**
 * Proxy-EditPart for a {@link ReferenceableElement}. It provides {@link Reference}s
 * with the corresponding figures, because the figure of a {@link ReferenceableElement}
 * may be used in mor than one {@link Reference}
 * 
 * @version $Id: ReferenceableElementProxyEditPart.java,v 1.8 2009-11-26 12:41:17 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ReferenceableElementProxyEditPart
		extends NamedElementEditPart
{

	/**
	 * The {@link ReferenceableElementEditPart}
	 */
	private ReferenceableElementEditPart ref;


	/**
	 * Proxy for the {@link ReferenceableElementEditPart}.
	 * 
	 * @param ref
	 *            The ReferenceableElementEditPart.
	 */
	public ReferenceableElementProxyEditPart(ReferenceableElementEditPart ref)
	{
		super();
		this.ref = ref;
	}


	/**
	 * Add Adapter to the {@link ReferenceableElement}.
	 */
	@Override
	public void activate()
	{
		super.activate();
		((ReferenceableElement) ref.getModel()).eAdapters().add(this.getAdapter());
	}


	/**
	 * Remove Adapter from the {@link ReferenceableElement}.
	 */
	@Override
	public void deactivate()
	{
		super.activate();
		((ReferenceableElement) ref.getModel()).eAdapters().remove(this.getAdapter());
	}


	/**
	 * Return the class of the {@link ReferenceableElementEditPart}.
	 */
	@Override
	public Class getModelClass()
	{
		if (ref != null)
			return ref.getModelClass();
		else
			return ReferenceableElement.class;
	}


	/**
	 * Return the model of the {@link ReferenceableElementEditPart}.
	 */
	@Override
	public Object getModel()
	{
		if (ref != null)
			return ref.getModel();
		else
			return null;
	}
	
	/**
	 * We must not register the model as in {@link AbstractEditPart}, since the
	 * model is already registered by the referenced element. Moreover, there
	 * might be multiple proxies for one model element, but the
	 * {@link EditPartViewer#getEditPartRegistry() registry} only accepts
	 * one-to-one relationships (it's simply a map).
	 * 	
	 * @see <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=383405">Bug 383405</a>
	 */
	@Override
	protected void register() {
		registerVisuals();
		registerAccessibility();
	}


	@Override
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		if (ref == null)
			return;

		// delegate all other request to the referenced element
		installEditPolicy(DirectDelegatingEditPolicy.DELEGATING_ROLE,
				new DirectDelegatingEditPolicy(ref));
	}


	/**
	 * This editPart is not selectable. Selections must be handled by the reference.
	 * 
	 * @return <code>false</code>
	 */
	@Override
	public boolean isSelectable()
	{
		return false;
	}


	/**
	 * Create a figure for the {@link ReferenceableElementEditPart}.
	 */
	@Override
	protected IFigure createFigure()
	{
		setDoDirectEdit(true);
		
		return super.createFigure();
	}
}
