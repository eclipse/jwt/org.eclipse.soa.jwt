/**
 * File:    ScopeEditPart.java
 * Created: 17.12.2005
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts.processes;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.AbstractRouter;
import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.FanRouter;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ManhattanConnectionRouter;
import org.eclipse.draw2d.ShortestPathConnectionRouter;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.CompoundSnapToHelper;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.SnapToGeometry;
import org.eclipse.gef.SnapToGrid;
import org.eclipse.gef.SnapToGuides;
import org.eclipse.gef.SnapToHelper;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;
import org.eclipse.gef.editpolicies.SnapFeedbackPolicy;
import org.eclipse.gef.rulers.RulerProvider;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.misc.views.Views;
import org.eclipse.jwt.we.model.view.impl.ReferenceImpl;
import org.eclipse.jwt.we.parts.core.GraphicalModelElementEditPart;
import org.eclipse.jwt.we.parts.processes.policies.ScopeLayoutEditPolicy;
import org.eclipse.swt.SWT;

/**
 * EditPart for a {@link Scope} model object.
 * 
 * @version $Id: ScopeEditPart.java,v 1.13 2009-12-21 10:30:47 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 * @since 0.6.0
 */
public class ScopeEditPart extends GraphicalModelElementEditPart
{

	/**
	 * A Logger.
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(ScopeEditPart.class);

	/**
	 * Helper that provides the ScopeEditPart functionality.
	 */
	private final ScopeEditPartHelper scopeEditPartHelper = new ScopeEditPartHelper(this);

	public static final String ROUTER_SHORTESTPATH = "shortestpath";//$NON-NLS-1$
	public static final String ROUTER_MANHATTAN = "manhattan";//$NON-NLS-1$
	public static final String ROUTER_FAN = "fan";//$NON-NLS-1$
	public static final String ROUTER_NULL = "null";//$NON-NLS-1$


	/**
	 * Constructor, empty.
	 */
	public ScopeEditPart()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.we.parts.core.GraphicalModelElementEditPart#getModelClass
	 * ()
	 */
	@Override
	public Class getModelClass()
	{
		return Scope.class;
	}


	/**
	 * @return The scope model.
	 */
	public Scope getScope()
	{
		return (Scope) getModel();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.jwt.we.parts.core.GraphicalModelElementEditPart#
	 * createEditPolicies()
	 */
	@Override
	protected void createEditPolicies()
	{
		// disallows the removal of this edit part from its parent
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());

		// handles constraint changes (e.g. moving and/or resizing) of model
		// elements and creation of new model elements
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new ScopeLayoutEditPolicy());

		// install a grid snap feedback policy
		installEditPolicy("Snap Feedback", new SnapFeedbackPolicy());//$NON-NLS-1$
	}


	/**
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	public Object getAdapter(Class adapter)
	{
		if (adapter == SnapToHelper.class)
		{
			List snapStrategies = new ArrayList();
			Boolean val = (Boolean) getViewer().getProperty(
					RulerProvider.PROPERTY_RULER_VISIBILITY);
			if (val != null && val.booleanValue())
				snapStrategies.add(new SnapToGuides(this));
			val = (Boolean) getViewer().getProperty(SnapToGeometry.PROPERTY_SNAP_ENABLED);
			if (val != null && val.booleanValue())
				snapStrategies.add(new SnapToGeometry(this));
			val = (Boolean) getViewer().getProperty(SnapToGrid.PROPERTY_GRID_ENABLED);
			if (val != null && val.booleanValue())
				snapStrategies.add(new SnapToGrid(this));

			if (snapStrategies.size() == 0)
				return null;
			if (snapStrategies.size() == 1)
				return snapStrategies.get(0);

			SnapToHelper ss[] = new SnapToHelper[snapStrategies.size()];
			for (int i = 0; i < snapStrategies.size(); i++)
				ss[i] = (SnapToHelper) snapStrategies.get(i);
			return new CompoundSnapToHelper(ss);
		}

		return super.getAdapter(adapter);
	}


	@Override
	protected IFigure createFigure()
	{
		IFigure figure = super.createFigure();

		// Create the static router for the connection layer
		ConnectionLayer conLayer = (ConnectionLayer) getLayer(LayerConstants.CONNECTION_LAYER);

		// enable antialiasing?
		if (PreferenceReader.appearanceAntialiasing.get())
		{
			conLayer.setAntialias(SWT.ON);
		}

		// create a router corresponding to the preferences
		AbstractRouter router = null;

		String preferenceRouter = PreferenceReader.appearanceConRouter.get();

		if (preferenceRouter.equals(ROUTER_SHORTESTPATH))
		{
			router = new ShortestPathConnectionRouter(figure);
		}
		else if (preferenceRouter.equals(ROUTER_MANHATTAN))
		{
			router = new ManhattanConnectionRouter();
		}
		else if (preferenceRouter.equals(ROUTER_FAN))
		{
			router = new FanRouter();
			((FanRouter) router).setNextRouter(new ShortestPathConnectionRouter(figure));
		}
		else if (preferenceRouter.equals(ROUTER_NULL))
		{
			router = new FanRouter();
			((FanRouter) router).setSeparation(1);
		}
		else
		{
			router = new ShortestPathConnectionRouter(figure);
		}

		conLayer.setConnectionRouter(router);

		return figure;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@Override
	public List getModelChildren()
	{
		// get all children
		List childrenlist = scopeEditPartHelper.getModelChildren();

		// return only children which are visible in the active view
		List returnlist = new ArrayList();
		Views myviews = Views.getInstance();
		for (Object obj : childrenlist)
		{
			EObject eo = (EObject) obj;

			try
			{
				EClass e = eo.eClass();

				if (eo instanceof ReferenceImpl)
				{
					ReferenceImpl ri = (ReferenceImpl) eo;
					e = ri.basicGetReference().eClass();
				}

				if (myviews.displayObject(e))
				{
					returnlist.add(obj);
				}
			}
			catch (Exception ex)
			{
				returnlist.add(obj);
			}
		}
		return returnlist;
	}

}