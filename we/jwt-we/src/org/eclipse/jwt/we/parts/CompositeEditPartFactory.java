/**
 * File:    CompositeEditPartFactory.java
 * Created: 02.02.2009
 *
/*******************************************************************************
 * Copyright (C) 2007  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.parts;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

/**
 * This factory for editParts contains a collection of EditPartFactory and
 * implements also the EditPartFactory interface. It allows to create editPart
 * with one of the EditPartFactory in the collection.
 * 
 * @author Marc Blachon, Bull SAS, France, www.bull.com
 * @since 0.6.0
 */
public class CompositeEditPartFactory implements EditPartFactory
{

	/**
	 * Collection of EditPartFactory
	 */
	Collection<EditPartFactory> collection = new ArrayList<EditPartFactory>();


	public EditPart createEditPart(EditPart context, Object model)
	{
		for (final EditPartFactory editPartFactory : collection)
		{
			EditPart result = editPartFactory.createEditPart(context, model);
			if (result != null)
				return result;
		}
		return null;
	}


	public void addEditPartFactory(EditPartFactory editPartFactory)
	{
		if (editPartFactory != null)
		{
			collection.add(editPartFactory);
		}
		else
		{
			throw new IllegalArgumentException("Error: the argument is null!"); //$NON-NLS-1$
		}
	}


	public void removeEditPartFactory(EditPartFactory editPartFactory)
	{
		if (editPartFactory != null)
		{
			if (collection.contains(editPartFactory))
			{
				collection.remove(editPartFactory);
			}
		}
		else
		{
			throw new IllegalArgumentException("Error: the argument is null!"); //$NON-NLS-1$
		}

	}


	public void setEditPartFactory(Collection<EditPartFactory> collecEditPartFactory)
	{
		if (collecEditPartFactory != null)
		{
			collection = collecEditPartFactory;
		}
		else
		{
			throw new IllegalArgumentException("Error: the argument is null!"); //$NON-NLS-1$
		}
	}

}
