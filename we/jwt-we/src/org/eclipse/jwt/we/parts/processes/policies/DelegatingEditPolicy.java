/**
 * File:    DelegatingEditPolicy.java
 * Created: 04.04.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.processes.policies;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;


/**
 * EditPolicy that forwards all request to the children of the editpart.
 * 
 * @version $Id: DelegatingEditPolicy.java,v 1.7 2009-11-26 12:41:34 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class DelegatingEditPolicy
		extends AbstractEditPolicy
{

	/**
	 * A key to install this edit policy.
	 */
	public static final String DELEGATING_ROLE = "Delegating"; //$NON-NLS-1$


	/**
	 * Constructs a new edit policy.
	 */
	public DelegatingEditPolicy()
	{
	}


	/**
	 * @return The children to which the requests are forwarded.
	 */
	protected List getChildren()
	{
		if (getHost() == null)
		{
			return Collections.EMPTY_LIST;
		}

		return getHost().getChildren();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#getCommand(org.eclipse.gef.Request)
	 */
	@Override
	public Command getCommand(Request request)
	{
		Command command = null;
		Iterator childParts = getChildren().iterator();

		while (childParts.hasNext())
		{
			if (command != null)
			{
				command = command.chain(((EditPart) childParts.next())
						.getCommand(request));
			}
			else
			{
				command = ((EditPart) childParts.next()).getCommand(request);
			}
		}
		return command;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#getTargetEditPart(org.eclipse.gef.Request)
	 */
	@Override
	public EditPart getTargetEditPart(Request request)
	{
		Iterator childParts = getChildren().iterator();
		EditPart editPart;
		while (childParts.hasNext())
		{
			editPart = ((EditPart) childParts.next()).getTargetEditPart(request);
			if (editPart != null)
			{
				return editPart;
			}
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#showSourceFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void showSourceFeedback(Request request)
	{
		Iterator childParts = getChildren().iterator();
		while (childParts.hasNext())
		{
			((EditPart) childParts.next()).showSourceFeedback(request);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#eraseSourceFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void eraseSourceFeedback(Request request)
	{
		Iterator childParts = getChildren().iterator();
		while (childParts.hasNext())
		{
			((EditPart) childParts.next()).eraseSourceFeedback(request);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#showTargetFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void showTargetFeedback(Request request)
	{
		Iterator childParts = getChildren().iterator();
		while (childParts.hasNext())
		{
			((EditPart) childParts.next()).showTargetFeedback(request);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#eraseTargetFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void eraseTargetFeedback(Request request)
	{
		Iterator childParts = getChildren().iterator();
		while (childParts.hasNext())
		{
			((EditPart) childParts.next()).eraseTargetFeedback(request);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#understandsRequest(org.eclipse.gef.Request)
	 */
	@Override
	public boolean understandsRequest(Request request)
	{
		Iterator childParts = getChildren().iterator();
		while (childParts.hasNext())
		{
			if (((EditPart) childParts.next()).understandsRequest(request))
			{
				return true;
			}
		}

		return false;
	}
}
