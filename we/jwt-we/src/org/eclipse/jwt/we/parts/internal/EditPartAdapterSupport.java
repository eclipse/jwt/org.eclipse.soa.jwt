/**
 * File:    EditPartAdapterSupport.java
 * Created: 23.01.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.internal;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPart;
import org.eclipse.jwt.we.parts.EditPartAdapter;
import org.eclipse.jwt.we.parts.EditPartAdapterFactory;


/**
 * An EditPart that is also an adapter (see {@link EditPartAdapter}).
 * 
 * All implementing EditPartAdapters must have a constructor with no paramters. Otherwise
 * the {@link EditPartAdapterFactory} is not able to create an instance.
 * 
 * @version $Id: EditPartAdapterSupport.java,v 1.4 2009-12-10 12:30:54 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.5.0      
 */
public interface EditPartAdapterSupport
		extends EditPart
{

	/**
	 * @return The adapter that the EditPart represents.
	 */
	public Adapter getAdapter();


	/**
	 * Returns the class of the model that the implementing EditPart represents.
	 * 
	 * @return The class of the model.
	 */
	public Class getModelClass();


	/**
	 * Notifies that a change to some feature has occurred.
	 * 
	 * The included adapter should forward its notification to this method.
	 * 
	 * @param notification
	 *            a description of the change.
	 */
	public void notifyChanged(Notification notification);
}
