/**
 * File:    EventEditPart.java
 * Created: 04.03.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.parts.events;

import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.EllipseAnchor;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.jwt.meta.model.events.Event;
import org.eclipse.jwt.we.parts.processes.ActivityNodeEditPart;


/**
 * EditPart for an {@link Event}.
 * 
 * @version $Id: EventEditPart.java,v 1.8 2009-11-26 12:41:48 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class EventEditPart
		extends ActivityNodeEditPart
{

	/**
	 * Maximum number of ingoing edges.
	 */
	private static final int MAXIMUM_IN_EDGES = 1;

	/**
	 * Maximum number of outgoing edges.
	 */
	private static final int MAXIMUM_OUT_EDGES = 1;


	/**
	 * Constructor.
	 */
	public EventEditPart()
	{
		setMaximumInActivityEdges(MAXIMUM_IN_EDGES);
		setMaximumOutActivityEdges(MAXIMUM_OUT_EDGES);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.processes.ActivityNodeEditPart#getModelClass()
	 */
	@Override
	public Class getModelClass()
	{
		return Event.class;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.processes.ActivityNodeEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies()
	{
		super.createEditPolicies();

		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE, new NonResizableEditPolicy());

	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.parts.ActivityNodeEditPart#getConnectionAnchor()
	 */
	@Override
	protected ConnectionAnchor createConnectionAnchor()
	{
		return new EllipseAnchor(getFigure());
	}
}
