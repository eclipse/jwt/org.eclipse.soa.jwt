/**
 * File:    IFactoryRegistry.java
 * Created: 02.02.2009
 *
/*******************************************************************************
 * Copyright (C) 2007  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - initial API of the FactoryRegistry concept.
 *******************************************************************************/

package org.eclipse.jwt.we;

import org.eclipse.gef.EditPartFactory;
import org.eclipse.jwt.we.editors.palette.IPaletteFactory;
import org.eclipse.jwt.we.figures.IFigureFactory;
import org.eclipse.jwt.we.figures.IImageFactory;

/**
 * This is an interface for the registry of factories.
 * 
 * @version $Id: IFactoryRegistry.java,v 1.4 2009-12-21 10:30:47 chsaad Exp $
 * @author Marc Blachon, Bull SAS, France, www.bull.com
 * @since 0.6.0
 */
public interface IFactoryRegistry
{

	/**
	 * Returns an instance of ImageFactory.
	 * 
	 * @return Returns ImageFactory.
	 */
	IImageFactory getImageFactory(Object... objects);


	/**
	 * Returns an instance of paletteFactory.
	 * 
	 * @return Returns a paletteFactory.
	 */
	IPaletteFactory getPaletteFactory();


	/**
	 * Returns an instance of figureFactory.
	 * 
	 * @return Returns a figureFactory.
	 */
	IFigureFactory getFigureFactory();


	/**
	 * Returns an instance of editPartFactory.
	 * 
	 * @return Returns a editPartFactory.
	 */
	EditPartFactory getEditPartFactory();
}
