/**
 * File:    ColorPreferenceWrapper.java
 * Created: 27.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.preferences.wrappers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.Color;

/**
 * This is a preference wrapper for a color value. It provides getValue,
 * setValue and getDefaultValue methods. The connection to a preference store
 * value is made through its corresponding preference key. To access the
 * preference store, the class WEPreferenceStoreInterface is used.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ColorPreferenceWrapper
{

	/**
	 * The key of the preference.
	 */
	private String preferenceKey;

	/**
	 * The cached color.
	 */
	private Color cachedColor;

	/**
	 * The preference string of the cached color.
	 */
	private String cachedColorString;

	/**
	 * The cached default color.
	 */
	private Color cachedColorDefault;

	/**
	 * The preference string of the cached default color.
	 */
	private String cachedColorStringDefault;

	private static List<Color> colorRegistry;


	/**
	 * @param preferenceStore
	 *            The preferenceStore where the value is stored.
	 * @param key
	 *            The key of the preference.
	 * @param defaultValue
	 *            The default value.
	 */
	public ColorPreferenceWrapper(String preferenceKey)
	{
		this.preferenceKey = preferenceKey;
		colorRegistry = new ArrayList<Color>();
	}


	/**
	 * @return The value of the preference.
	 */
	public Color get()
	{
		// read color from prefs
		String colorString = WEPreferenceStoreInterface.getValueAsString(preferenceKey);

		// if cached color == pref color -> use cached color
		if (cachedColor != null && !cachedColor.isDisposed()
				&& colorString.equals(cachedColorString))
		{
			return cachedColor;
		}

		String[] colorStringSplit = colorString.split(","); //$NON-NLS-1$
		cachedColor = new Color(null, new Integer(colorStringSplit[0]), new Integer(
				colorStringSplit[1]), new Integer(colorStringSplit[2]));
		cachedColorString = colorString;
		colorRegistry.add(cachedColor);

		return cachedColor;
	}


	/**
	 * @param value
	 *            The new value of the preference.
	 */
	public void set(Color value)
	{
		String colorString;

		colorString = value.getRed() + "," + value.getGreen() + "," + value.getBlue(); //$NON-NLS-1$ //$NON-NLS-2$
		WEPreferenceStoreInterface.setValueString(preferenceKey, colorString);
	}


	/**
	 * @return The default value of the preference.
	 */
	public Color getDefault()
	{
		// read color from prefs
		String colorString = WEPreferenceStoreInterface
				.getDefaultValueAsString(preferenceKey);

		// if cached color == pref color -> use cached color
		if (cachedColorDefault != null && !cachedColorDefault.isDisposed()
				&& colorString.equals(cachedColorStringDefault))
		{
			return cachedColorDefault;
		}

		String[] colorStringSplit = colorString.split(","); //$NON-NLS-1$
		cachedColorDefault = new Color(null, new Integer(colorStringSplit[0]),
				new Integer(colorStringSplit[1]), new Integer(colorStringSplit[2]));
		cachedColorStringDefault = colorString;
		colorRegistry.add(cachedColorDefault);

		return cachedColorDefault;
	}


	/**
	 * Dispose color resources.
	 */
	public static void disposeColors()
	{
		for (Color color : colorRegistry)
		{
			if (!color.isDisposed())
			{
				color.dispose();
			}
		}

		colorRegistry.clear();
	}

}