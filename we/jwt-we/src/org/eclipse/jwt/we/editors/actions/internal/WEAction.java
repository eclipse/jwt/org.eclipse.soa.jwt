/**
 * File:    WEAction.java
 * Created: 22.05.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.internal;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.we.misc.util.GeneralHelper;


/**
 * This is a base class which actions may extend if they should only be enabled if an
 * editor is open.
 * 
 * Important: The enabled state has to be refreshed in the menu manager
 * 
 * @version $Id: WEAction.java,v 1.3 2009-11-26 12:41:37 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public abstract class WEAction
		extends Action
{

	/**
	 * Indicates whether this action should only be enabled if an WEEditor is active
	 */
	private boolean requiresOpenEditor = false;


	/**
	 * The constructor.
	 */
	public WEAction()
	{
		super();
	}


	/**
	 * The constructor.
	 */
	protected WEAction(String text, ImageDescriptor image)
	{
		super(text, image);
	}


	public boolean isRequiresOpenEditor()
	{
		return requiresOpenEditor;
	}


	public void setRequiresOpenEditor(boolean requiresOpenEditor)
	{
		this.requiresOpenEditor = requiresOpenEditor;
	}


	@Override
	public boolean isEnabled()
	{
		// only if it is required
		if (isRequiresOpenEditor())
		{
			if (GeneralHelper.getActiveInstance() != null)
			{
				return true;
			}
			return false;
		}
		return super.isEnabled();
	}

}