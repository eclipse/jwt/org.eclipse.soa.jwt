/**
 * File:    PasteAction.java
 * Created: 02.04.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- improved version
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.internalActions;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jwt.we.commands.clipboard.WEPasteFromClipboardCommand;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;


/**
 * Action to paste previous copied model elements.
 * 
 * <p>
 * If model element where previously copied into the {@link EditingDomain} the action
 * pastes the these elements under the currently selected element provided by the
 * {@link #selectionProvider}. The action updates itself on selection change.
 * </p>
 * 
 * The pasted elements are selected after inserting them.
 * 
 * @version $Id: PasteAction.java,v 1.3 2009-11-26 12:41:20 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class PasteAction
		extends org.eclipse.emf.edit.ui.action.PasteAction
{

	/**
	 * Provides the selection for the element to paste into.
	 */
	private ISelectionProvider selectionProvider;


	/**
	 * Creates a new action.
	 * 
	 * @param domain
	 *            The EditingDomain to create the paste command.
	 * @param selectionProvider
	 *            Provides the selection for the parent element.
	 */
	public PasteAction(EditingDomain domain, ISelectionProvider selectionProvider)
	{
		super(domain);

		ISharedImages sharedImages = PlatformUI.getWorkbench().getSharedImages();
		setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_PASTE));
		setDisabledImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_PASTE_DISABLED));
		setId(ActionFactory.PASTE.getId());

		selectionProvider.addSelectionChangedListener(this);
		this.selectionProvider = selectionProvider;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.ui.action.StaticSelectionCommandAction#run()
	 */
	@Override
	public void run()
	{
		super.run();

		// select all elements which were pasted. this also removes a bug because if the
		// selection stays the same after pasting, no new command is generated and a
		// following paste would not be executed correctly		
		if (command instanceof WEPasteFromClipboardCommand)
		{
			GeneralHelper.getActiveInstance().getSelectionSynchronizer().setSelection(
					new StructuredSelection(((WEPasteFromClipboardCommand)command).elementsToSelect().toArray()));
		}
		else
		{
			GeneralHelper.getActiveInstance().getSelectionSynchronizer().setSelection(
					new StructuredSelection(command.getResult().toArray()));
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.Disposable#dispose()
	 */
	public void dispose()
	{
		selectionProvider.removeSelectionChangedListener(this);
	}

}
