/**
 * File:    WEPreferencesGuard.java
 * Created: 20.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.preferences.pages;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.preferences.PreferenceConstants;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;


/**
 * This class represents a preference page that is contributed to the Preferences dialog.
 * By subclassing <samp>FieldEditorPreferencePage</samp>, we can use the field support
 * built into JFace that allows us to create a page that is small and knows how to save,
 * restore and apply itself.
 * 
 * This page is used to modify preferences only. They are stored in the preference store
 * that belongs to the main plug-in class. That way, preferences can be accessed directly
 * via the preference store.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class WEPreferencesGuard
		extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage
{

	/**
	 * A logger.
	 */
	Logger logger = Logger.getLogger(WEPreferencesGuard.class);


	/**
	 * Constructor.
	 */
	public WEPreferencesGuard()
	{
		super(GRID);

		setPreferenceStore(Plugin.getDefault().getPreferenceStore());
		setDescription("JWT Workflow Editor - " //$NON-NLS-1$
				+ PluginProperties.preferences_guard_label);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench)
	{
	}


	/**
	 * Adds an empty line to the editor page.
	 */
	private void addSpacing()
	{
		new Label(getFieldEditorParent(), SWT.LEFT);
		new Label(getFieldEditorParent(), SWT.LEFT);
	}


	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI blocks
	 * needed to manipulate various types of preferences. Each field editor knows how to
	 * save and restore itself.
	 */
	@Override
	public void createFieldEditors()
	{
		try
		{
			addSpacing();

			// guard textual description wrap
			IntegerFieldEditor guard_text_wrap_field = new IntegerFieldEditor(
					PreferenceConstants.C_GUARD_T_WRAP,
					PluginProperties.preferences_guardtwrap_label,
					getFieldEditorParent());

			// guard textual description cut
			IntegerFieldEditor guard_text_cut_field = new IntegerFieldEditor(
					PreferenceConstants.C_GUARD_T_CUT,
					PluginProperties.preferences_guardtcut_label,
					getFieldEditorParent());

			addSpacing();

			// guard short description wrap
			IntegerFieldEditor guard_short_wrap_field = new IntegerFieldEditor(
					PreferenceConstants.C_GUARD_S_WRAP,
					PluginProperties.preferences_guardswrap_label,
					getFieldEditorParent());

			// guard short description cut
			IntegerFieldEditor guard_short_cut_field = new IntegerFieldEditor(
					PreferenceConstants.C_GUARD_S_CUT,
					PluginProperties.preferences_guardscut_label,
					getFieldEditorParent());

			addSpacing();

			// wrap only at control chars
			BooleanFieldEditor guard_autowrap_field = new BooleanFieldEditor(
					PreferenceConstants.C_GUARD_AUTOWRAP,
					PluginProperties.preferences_guardautowrap_label,
					getFieldEditorParent());

			// set valid ranges
			guard_text_wrap_field.setValidRange(0, 999);
			guard_text_cut_field.setValidRange(0, 999);
			guard_short_wrap_field.setValidRange(0, 999);
			guard_short_cut_field.setValidRange(0, 999);

			// add the fields
			addField(guard_text_cut_field);
			addField(guard_text_wrap_field);

			addField(guard_short_cut_field);
			addField(guard_short_wrap_field);

			addField(guard_autowrap_field);

		}
		catch (Exception ex)
		{
			logger.debug(ex);
		}
	}

}