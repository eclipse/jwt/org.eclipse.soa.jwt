/**
 * File:    PreferenceConstants.java
 * Created: 01.08.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.preferences;

/**
 * Constant definitions for plug-in preferences.
 * 
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class PreferenceConstants
{

	// the PREFIXES for the preference groups
	public static final String P_SIMULATOR = "WEsimulator";
	public static final String P_VIEW = "WEview";
	public static final String P_GRID = "WEgrid";
	public static final String P_GUARD = "WEguard";
	public static final String P_APPEARANCE = "WEapp";
	public static final String P_PALETTE = "WEpalette";
	public static final String P_OUTLINE = "WEoutline";

	// ----------------------------
	// group: SIMULATOR preferences
	// ----------------------------
	public static final String C_SIMULATOR_PATH = P_SIMULATOR + "Path";

	// -----------------------
	// group: VIEW preferences
	// -----------------------
	public static final String C_VIEW_SELECTED = P_VIEW + "Selected";
	public static final String C_VIEW_LAYOUTDATA_HANDLING = P_VIEW + "CheckLayoutData";

	// -----------------------
	// group: GRID preferences
	// -----------------------
	public static final String C_GRID_WIDTH = P_GRID + "Width";
	public static final String C_GRID_HEIGHT = P_GRID + "Height";
	public static final String C_GRID_VISIBLE = P_GRID + "Visible";
	public static final String C_GRID_SNAP = P_GRID + "Snap";

	// ------------------------
	// group: GUARD preferences
	// ------------------------
	public static final String C_GUARD_T_CUT = P_GUARD + "TextCut";
	public static final String C_GUARD_S_CUT = P_GUARD + "ShortCut";
	public static final String C_GUARD_T_WRAP = P_GUARD + "TextWrap";
	public static final String C_GUARD_S_WRAP = P_GUARD + "ShortWrap";
	public static final String C_GUARD_AUTOWRAP = P_GUARD + "SAutoWrap";

	// -----------------------------
	// group: APPEARANCE preferences
	// -----------------------------
	public static final String C_APPEARANCE_COLOR_EDITOR = P_APPEARANCE + "EditorColor";
	public static final String C_APPEARANCE_COLOR_TEXT = P_APPEARANCE + "TextColor";
	public static final String C_APPEARANCE_COLOR_BORDER = P_APPEARANCE + "BorderColor";
	public static final String C_APPEARANCE_COLOR_FILL = P_APPEARANCE + "FillColor";
	public static final String C_APPEARANCE_COLOR_SHADOW = P_APPEARANCE + "ShadowColor";
	public static final String C_APPEARANCE_COLOR_GUARD_T = P_GUARD + "TextColor";
	public static final String C_APPEARANCE_COLOR_GUARD_S = P_GUARD + "ShortColor";

	public static final String C_APPEARANCE_DEFAULT_FONT = P_APPEARANCE + "DefaultFont";
	public static final String C_APPEARANCE_REFELEMENT_FONT = P_APPEARANCE
			+ "ReferenceableElementFont";
	public static final String C_APPEARANCE_GUARD_FONT = P_APPEARANCE + "GuardFont";

	public static final String C_APPEARANCE_MINIMUM_SIZE = P_APPEARANCE + "MinimumSize";
	public static final String C_APPEARANCE_FIGURE_ICON_SIZE = P_APPEARANCE
			+ "FigureIconSize";
	public static final String C_APPEARANCE_LINE_WIDTH = P_APPEARANCE + "LineWidth";
	public static final String C_APPEARANCE_FIGURE_BAR_WIDTH = P_APPEARANCE + "BarWidth";
	public static final String C_APPEARANCE_CORNER_SIZE = P_APPEARANCE + "CornerSize";
	public static final String C_APPEARANCE_ANTIALIASING = P_APPEARANCE + "Antialiasing";
	public static final String C_APPEARANCE_SHADOW_VISIBLE = P_APPEARANCE
			+ "ShadowVisible";
	public static final String C_APPEARANCE_FEEDBACK_SHOW = P_APPEARANCE + "FeedbackShow";
	public static final String C_APPEARANCE_MOUSEPOS_SHOW = P_APPEARANCE + "MousePosShow";
	public static final String C_APPEARANCE_CON_ROUTER = P_APPEARANCE
			+ "ConnectionRouter";
	public static final String C_APPEARANCE_OVERVIEW_SHOW = P_APPEARANCE + "OverviewShow";

	// --------------------------
	// group: PALETTE preferences
	// --------------------------
	public static final String C_PALETTE_DOCK_LOCATION = P_PALETTE + "Docklocation";
	public static final String C_PALETTE_STATE = P_PALETTE + "State";
	public static final String C_PALETTE_WIDTH = P_PALETTE + "Width";

	// --------------------------
	// group: OUTLINE preferences
	// --------------------------
	public static final String C_OUTLINE_HIDE_ACTEDGES = P_OUTLINE + "ActEdges";
	public static final String C_OUTLINE_SORT_ELEMENTS = P_OUTLINE + "Sort";
	public static final String C_OUTLINE_SHOW_AREA = P_OUTLINE + "ShowArea";
	public static final String C_OUTLINE_OVERVIEW_SIZE = P_OUTLINE + "OverviewSize";
	public static final String C_OUTLINE_TREEVIEW_SIZE = P_OUTLINE + "TreeviewSize";



}