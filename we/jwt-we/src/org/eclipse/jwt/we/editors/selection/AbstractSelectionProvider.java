/**
 * File:    AbstractSelectionProvider.java
 * Created: 25.03.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.selection;

import org.eclipse.core.runtime.ListenerList;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jwt.we.misc.logging.Logger;


/**
 * Abstract implementation of an {@link ISelectionProvider}.
 * 
 * @version $Id: AbstractSelectionProvider.java,v 1.6 2009-11-26 12:41:37 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public abstract class AbstractSelectionProvider
		implements ISelectionProvider
{

	/**
	 * A Logger.
	 */
	private static final Logger logger = Logger
			.getLogger(AbstractSelectionProvider.class);

	/**
	 * List of selection change listeners (element type:
	 * <code>ISelectionChangedListener</code>).
	 */
	private ListenerList selectionChangedListeners = new ListenerList();


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ISelectionProvider#addSelectionChangedListener(org.eclipse.jface.viewers.ISelectionChangedListener)
	 */
	public void addSelectionChangedListener(ISelectionChangedListener listener)
	{
		selectionChangedListeners.add(listener);
	}


	/**
	 * @return An array containing all the registered listeners.
	 */
	protected Object[] getSelectionChangedListener()
	{
		return selectionChangedListeners.getListeners();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ISelectionProvider#removeSelectionChangedListener(org.eclipse.jface.viewers.ISelectionChangedListener)
	 */
	public void removeSelectionChangedListener(ISelectionChangedListener listener)
	{
		selectionChangedListeners.remove(listener);
	}


	/**
	 * Notifies any selection changed listeners that the viewer's selection has changed.
	 * Only listeners registered at the time this method is called are notified.
	 * 
	 * @param event
	 *            a selection changed event
	 * @see ISelectionChangedListener#selectionChanged
	 */
	protected void fireSelectionChanged(final SelectionChangedEvent event)
	{
		for (Object listener : getSelectionChangedListener())
		{
			try
			{
				((ISelectionChangedListener) listener).selectionChanged(event);
			}
			catch (Exception e)
			{
				logger.warning(e);
			}
		}
	}
}
