/**
 * File:    ViewContributionItems.java
 * Created: 06.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.managed.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jwt.we.misc.views.ViewDescriptor;
import org.eclipse.jwt.we.misc.views.Views;
import org.eclipse.ui.actions.CompoundContributionItem;


/**
 * This class returns a set of the ViewActions wrapped in ActionContributionItems. This
 * method is called when the view dropdown in the toolbar or a view menu is populated.
 * 
 * @version $Id: ViewContributionItems.java,v 1.6 2009-11-26 12:41:34 chsaad Exp $
 * @author hristian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class ViewContributionItems
		extends CompoundContributionItem
{

	/**
	 * Constructor.
	 */
	public ViewContributionItems()
	{
		super();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.actions.CompoundContributionItem#getContributionItems()
	 */
	@Override
	protected IContributionItem[] getContributionItems()
	{
		// get the list of available view actions
		List<ViewsAction> viewsActions = new ArrayList<ViewsAction>();

		for (ViewDescriptor viewDesc : Views.getInstance().getAvailableViews())
		{
			ViewsAction viewAction = viewDesc.getViewAction();
			if (viewAction != null)
			{
				viewsActions.add(viewAction);
			}
		}

		IContributionItem[] list = new IContributionItem[viewsActions.size()];

		// add all views to toolbar/menu
		for (int i = 0; i < viewsActions.size(); i++)
		{
			ActionContributionItem actionItem = new ActionContributionItem(
					(ViewsAction) viewsActions.get(i));

			actionItem.setMode(1);

			list[i] = actionItem;
		}

		return list;
	}

}
