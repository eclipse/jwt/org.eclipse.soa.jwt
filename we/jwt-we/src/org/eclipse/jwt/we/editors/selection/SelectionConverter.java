/**
 * File:    SelectionConverter.java
 * Created: 25.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.selection;

import org.eclipse.jface.viewers.ISelection;


/**
 * Converts a selection of a target object.
 * 
 * @version $Id: SelectionConverter.java,v 1.5 2009-11-26 12:41:36 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class SelectionConverter
{

	/**
	 * A converter that does not convert the selections, but simply returns the given
	 * selections.
	 */
	public static final SelectionConverter IDENTITY_CONVERTER = new SelectionConverter();


	/**
	 * Converts a selection of the target object to another selection.
	 * 
	 * @param targetSelection
	 *            The selection of the target object.
	 * @return The converted selection.
	 */
	public ISelection convertFromTargetSelection(ISelection targetSelection)
	{
		return targetSelection;
	}


	/**
	 * Converts a selection to a selection that the target object understands.
	 * 
	 * @param selection
	 *            The selection.
	 * @return A selection for the target object.
	 */
	public ISelection convertToTargetSelection(ISelection selection)
	{
		return selection;
	}
}
