/**
 * File:    ViewLabelControlContribution.java
 * Created: 07.10.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.managed.views;

import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.menus.WorkbenchWindowControlContribution;


/**
 * A label that can be added to the toolbar.
 * 
 * @version $Id: ViewLabelControlContribution.java,v 1.5 2009-11-26 12:41:34 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class ViewLabelControlContribution
		extends WorkbenchWindowControlContribution
{

	/**
	 * The constructor.
	 */
	public ViewLabelControlContribution()
	{
		super();
	}

	/**
	 * The constructor.
	 */
	public ViewLabelControlContribution(String id)
	{
		super(id);
	}


	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.ControlContribution#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createControl(Composite parent)
	{
		// the composite
		Composite c = new Composite(parent, SWT.NONE);
		c.setLayout(new GridLayout(1, false));
		((GridLayout) c.getLayout()).horizontalSpacing = 0;
		((GridLayout) c.getLayout()).verticalSpacing = 0;
		((GridLayout) c.getLayout()).marginHeight = 0;
		((GridLayout) c.getLayout()).marginWidth = 0;

		// create a label
		Label label = new Label(c, SWT.NONE);
		label.setText(PluginProperties.menu_Views_label + " "); //$NON-NLS-1$
		label
				.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, true,
						true));
		label.setBounds(label.getBounds().x, label.getBounds().y, 150,
				label.getBounds().height);
		
		// set the font
		Display current = Display.getCurrent();
		FontData fd = current.getSystemFont().getFontData()[0];
		fd.setHeight(fd.getHeight() + 1);
		Font font = new Font(null, fd);
		label.setFont(font);

		return c;
	}

}
