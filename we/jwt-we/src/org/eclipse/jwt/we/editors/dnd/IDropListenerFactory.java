/**
 * File:    IDropListenerFactory.java
 * Created: 13.02.2009
 *
/*******************************************************************************
 * Copyright (c) 2009   Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.dnd;

import org.eclipse.gef.commands.Command;
import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jwt.meta.model.core.Model;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.jwt.we.commands.editdomain.GefEmfEditingDomain;
import org.eclipse.jwt.we.editors.pages.activityEditor.WEEditorSheet;

/**
 * 
 * This interface is intended to be implemented when one wants to
 * add support for drag n drop into JWT, using extension point
 * org.eclipse.jwt.we.dnd
 * 
 * @author Mickael Istria, Open Wide, Lyon, France
 * @since 0.6.0
 */
public interface IDropListenerFactory {

	/**
	 * Adds a Drop listener on the {@link WEEditorSheet} of an activity
	 * @param weEditorSheet The {@link WEEditorSheet} that will receive the drop, from where we can get the {@link WEEditorSheet#getEditDomain()} for {@link Command}
	 * @param activity The {@link Activity} that is currently displayed in this viewer
	 * @return the {@link TransferDropTargetListener} that will be added to the viewer
	 */
	public TransferDropTargetListener createWEEditorSheetDropListener(WEEditorSheet weEditorSheet, Activity activity);

	/**
	 * Creates a Drop listener to be added to all {@link TreeViewer}s that are
	 * used into the JWT WE.
	 * @param viewer The {@link TreeViewer} whom will receive this {@link TransferDropTargetListener}
	 * @param model The workflow {@link Model} that is currently edited 
	 * @param editingDomain the {@link GefEmfEditingDomain}, needed to use {@link Command} and then enable save, undo and redo actions
	 * @return the {@link TransferDropTargetListener} to add to the viewer
	 */
	public TransferDropTargetListener createWEOutlineDropListerner(TreeViewer viewer, Model model, GefEmfEditingDomain editingDomain);
}

