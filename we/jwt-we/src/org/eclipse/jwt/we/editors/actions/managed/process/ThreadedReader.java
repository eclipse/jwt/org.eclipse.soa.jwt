/**
 * File:    ThreadedReader.java
 * Created: 01.12.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.managed.process;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Asynchronously reads an InputStream
 * 
 * @version $Id: ThreadedReader.java,v 1.5 2009-11-26 12:41:39 chsaad Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class ThreadedReader
		extends Thread
{

	InputStream is = null;
	boolean output = false;


	/**
	 * Standardconstructor. Automatically starts the reading thread
	 * 
	 * @param inputstream
	 *            The instance of an inputstream
	 * @param output
	 *            If true, outputs the inputstream to the console. Else false.
	 */
	public ThreadedReader(InputStream inputstream, boolean output)
	{
		is = inputstream;
		this.output = output;
		this.start();
	}


	@Override
	public void run()
	{
		try
		{
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null)
			{
				if (output)
					System.out.println(line);
			}
		}
		catch (Exception ex)
		{

		}
	}
}
