/**
 * File:    UndoAction.java
 * Created: 26.03.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.internalActions;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.commands.CommandStackEvent;
import org.eclipse.gef.commands.CommandStackEventListener;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.IEditorPart;


/**
 * The action performs an undo on a GEF {@link CommandStack}.
 * 
 * <p>
 * The action updates itself on changes on the command stack.
 * </p>
 * 
 * @version $Id: UndoAction.java,v 1.3 2009-11-26 12:41:21 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class UndoAction
		extends org.eclipse.gef.ui.actions.UndoAction
		implements CommandStackEventListener
{

	/**
	 * Creates an <code>UndoAction</code> and associates it with the given editor.
	 * 
	 * @param editor
	 *            The editor this action is associated with.
	 */
	public UndoAction(IEditorPart editor)
	{
		super(editor);

		getCommandStack().addCommandStackEventListener(this);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.CommandStackEventListener#stackChanged(org.eclipse.gef.commands.CommandStackEvent)
	 */
	public void stackChanged(CommandStackEvent event)
	{
		refresh();
	}


	/* (non-Javadoc)
	 * @see org.eclipse.gef.ui.actions.UndoAction#run()
	 */
	@Override
	public void run()
	{
		super.run();
		
		// select displayed activity
		if (GeneralHelper.getActiveInstance().getDisplayedActivityModel() != null)
		{
			GeneralHelper.getActiveInstance().getSelectionSynchronizer().setSelection(
					new StructuredSelection(GeneralHelper.getActiveInstance().getDisplayedActivityModel()));
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#update()
	 */
	@Override
	public void update()
	{
		// this method for updating the enabled state would only work for the
		// activityeditor but not for the overview sheet
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#dispose()
	 */
	@Override
	public void dispose()
	{
		getCommandStack().removeCommandStackEventListener(this);
		super.dispose();
	}

}
