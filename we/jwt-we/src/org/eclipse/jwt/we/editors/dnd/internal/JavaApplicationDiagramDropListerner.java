/**
 * File:    JavaApplicationDiagramDropListener.java
 * Created: 13.02.2009
 *
/*******************************************************************************
 * Copyright (c) 2009   Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.editors.dnd.internal;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.jwt.meta.model.core.Model;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.jwt.we.editors.pages.activityEditor.WEEditorSheet;
import org.eclipse.jwt.we.misc.views.Views;
import org.eclipse.jwt.we.model.view.LayoutData;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.jwt.we.model.view.ViewPackage;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;


public class JavaApplicationDiagramDropListerner implements TransferDropTargetListener {

	private WEEditorSheet sheet;
	private Activity activity;

	public JavaApplicationDiagramDropListerner(WEEditorSheet viewer, Activity activity) {
		this.sheet = viewer;
		this.activity = activity;
	}

	public Transfer getTransfer() {
		return FileTransfer.getInstance();
	}

	public boolean isEnabled(DropTargetEvent event) {
		String[] data = (String[])event.data;
		if (data == null)
			return true;
		else return (data.length == 1 && data[0].endsWith(".java")); 
	}

	public void dragEnter(DropTargetEvent event) {
	}

	public void dragLeave(DropTargetEvent event) {
	}

	public void dragOperationChanged(DropTargetEvent event) {
	}

	public void dragOver(DropTargetEvent event) {
	}

	public void drop(final DropTargetEvent event) {
		final Application app = ApplicationDropListenerFactory.createApplication((String[])event.data);
		if (app == null)
			return;

		this.sheet.getEditDomain().getCommandStack().execute(new Command(){
			@Override
			public void execute() {
				EObject obj = activity.eContainer();
				while (!(obj instanceof Model)) {
					obj = obj.eContainer();
				}
				Model model = (Model)obj;
				model.getElements().add(app);
				Reference ref = (Reference)ViewPackage.eINSTANCE.getEFactoryInstance().create(ViewPackage.Literals.REFERENCE);
				ref.setReference(app);
				LayoutData location = (LayoutData) ViewPackage.eINSTANCE.getEFactoryInstance().create(ViewPackage.Literals.LAYOUT_DATA);
				location.setX(sheet.absoluteToRelativeX(event.x));
				location.setY(sheet.absoluteToRelativeY(event.y));
				location.setDescribesElement(ref);
				location.setInitialized(true);
				location.setViewid(Views.getInstance().getSelectedView().getInternalName());

				ref.setContainedIn(activity);
			}
		});
	}

	public void dropAccept(DropTargetEvent event) {
	}

}
