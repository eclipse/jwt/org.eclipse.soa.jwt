/**
 * File:    PropertySheetPageDescriptor.java
 * Created: 11.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Open Wide SA, France <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *    Marc Dutoo, Open Wide, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.properties.extension.internal;

import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.ui.views.properties.IPropertySheetPage;


/**
 * Describes a PropertySheetPage in order to be able to create it.
 * 
 * @version $Id: PropertySheetPageDescriptor.java,v 1.3 2009-11-26 12:41:38 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA, France <www.openwide.fr>
 */
public interface PropertySheetPageDescriptor {
	
	/**
	 * 
	 * Creates a new IPropertySheetPage for the given editor
	 * @param weEditor
	 * @return new one
	 */
	public IPropertySheetPage createPropertySheetPage(WEEditor weEditor);
	
}
