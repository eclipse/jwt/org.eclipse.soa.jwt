/**
 * File:    PropertyDescriptorFactory.java
 * Created: 11.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *    Mickael Istria, Open Wide, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.properties.extension;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

/**
 *	This interface defines the expected class for extension point
 * to add custom PropertyDescriptor. See also documentation of extension point
 * "org.eclipse.jwt.we.propertyDescriptor"
 * 
 * 	It is a Factory for {@link IPropertyDescriptor}
 *  @since 0.6.0
 */
public interface PropertyDescriptorFactory {

	/**
	 * 
	 * @param eObject the current eObject which contains the property
	 * @param itemPropertyDescriptor the descriptor of the property
	 * @return an intance of IPropertyDescriptor for specified property
	 */
	public IPropertyDescriptor getPropertyDescriptor(Object eObject, IItemPropertyDescriptor itemPropertyDescriptor);
}
