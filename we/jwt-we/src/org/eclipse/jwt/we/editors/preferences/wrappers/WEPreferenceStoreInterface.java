/**
 * File:    WEPreferenceWrapperInterface.java
 * Created: 27.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.preferences.wrappers;

import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.editors.preferences.internal.PreferenceChangeListener;


/**
 * This class provides access for primitive reading/writing operations on the preference
 * store. IT IS ONLY INTENDED FOR INTERNAL USE IN THE PREFERENCEWRAPPERS! The proper way
 * to access preferences is through the class PreferenceReader.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class WEPreferenceStoreInterface
{

	private static PreferenceChangeListener preferenceChangeListener = new PreferenceChangeListener();


	/**
	 * Private method for retrieving a preference value.
	 * 
	 * @param preferenceKey
	 * @return Value.
	 */
	protected static String getValueAsString(String preferenceKey)
	{
		if (Plugin.getDefault() == null)
			return null;
		return Plugin.getDefault().getPreferenceStore().getString(preferenceKey);
	}


	/**
	 * Private method for retrieving a preference value.
	 * 
	 * @param preferenceKey
	 * @return Value.
	 */
	protected static int getValueAsInt(String preferenceKey)
	{
		if (Plugin.getDefault() == null)
			return -1;
		return Plugin.getDefault().getPreferenceStore().getInt(preferenceKey);
	}


	/**
	 * Private method for retrieving a preference value.
	 * 
	 * @param preferenceKey
	 * @return Value.
	 */
	protected static boolean getValueAsBool(String preferenceKey)
	{
		if (Plugin.getDefault() == null)
			return false;
		return Plugin.getDefault().getPreferenceStore().getBoolean(preferenceKey);
	}


	/**
	 * Private method for retrieving a default preference value.
	 * 
	 * @param preferenceKey
	 * @return Default value.
	 */
	protected static String getDefaultValueAsString(String preferenceKey)
	{
		if (Plugin.getDefault() == null)
			return null;
		return Plugin.getDefault().getPreferenceStore().getDefaultString(preferenceKey);
	}


	/**
	 * Private method for retrieving a default preference value.
	 * 
	 * @param preferenceKey
	 * @return Default value.
	 */
	protected static int getDefaultValueAsInt(String preferenceKey)
	{
		if (Plugin.getDefault() == null)
			return -1;
		return Plugin.getDefault().getPreferenceStore().getDefaultInt(preferenceKey);
	}


	/**
	 * Private method for retrieving a default preference value.
	 * 
	 * @param preferenceKey
	 * @return Default value.
	 */
	protected static boolean getDefaultValueAsBool(String preferenceKey)
	{
		if (Plugin.getDefault() == null)
			return false;
		return Plugin.getDefault().getPreferenceStore()
				.getDefaultBoolean(preferenceKey);
	}


	/**
	 * Private method for setting a preference value.
	 * 
	 * @param preferenceKey
	 * @param value
	 */
	protected static void setValueString(String preferenceKey, String value)
	{
		Plugin instance = Plugin.getDefault();
		if (instance == null)
			return;
		instance.getPreferenceStore().setValue(preferenceKey, value);
	}


	/**
	 * Private method for setting a preference value.
	 * 
	 * @param preferenceKey
	 * @param value
	 */
	protected static void setValueBool(String preferenceKey, boolean value)
	{
		Plugin instance = Plugin.getDefault();
		if (instance == null)
			return;
		instance.getPreferenceStore().setValue(preferenceKey, value);
	}


	/**
	 * Private method for setting a preference value.
	 * 
	 * @param preferenceKey
	 * @param value
	 */
	protected static void setValueInt(String preferenceKey, int value)
	{
		Plugin instance = Plugin.getDefault();
		if (instance == null)
			return;
		instance.getPreferenceStore().setValue(preferenceKey, value);
	}


	/**
	 * Private method for setting a preference value.
	 * 
	 * @param preferenceKey
	 * @param value
	 */
	protected static void setDefaultValueString(String preferenceKey)
	{
		Plugin instance = Plugin.getDefault();
		if (instance == null)
			return;
		instance.getPreferenceStore().setToDefault(preferenceKey);
	}


	/**
	 * Add a property listener to a preference store.
	 */
	public static void activatePreferenceListener()
	{
		Plugin instance = Plugin.getDefault();
		if (instance == null)
			return;
		instance.getPreferenceStore().addPropertyChangeListener(preferenceChangeListener);
	}


	/**
	 * Add a property listener to a preference store.
	 */
	public static void deactivatePreferenceListener()
	{
		Plugin instance = Plugin.getDefault();
		if (instance == null)
			return;
		instance.getPreferenceStore().removePropertyChangeListener(
				preferenceChangeListener);
	}

}