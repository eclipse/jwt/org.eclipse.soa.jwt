/**
 * File:    CustomAdapterFactoryContentProvider.java
 * Created: 17.12.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Mickael Istria, Open Wide, France
 *      - Add of extension point to get custom PropertyDescriptor
 *******************************************************************************/

package org.eclipse.jwt.we.editors.properties;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.PropertySource;
import org.eclipse.jwt.we.editors.properties.extension.internal.PropertyDescriptorExtensionPoint;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;


/**
 * A custom AdapterFactoryContentProvider which overrides createPropertySource which in
 * turn overrides createPropertyDescriptor of the PropertySource to call a custom
 * PropertyDescriptor
 * 
 * @version $Id: CustomAdapterFactoryContentProvider.java,v 1.3 2008/01/31 09:51:28
 *          flautenba Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class CustomAdapterFactoryContentProvider
		extends AdapterFactoryContentProvider
{

	/**
	 * The constructor. Calls the super constructor.
	 * 
	 * @param adapterFactory
	 *            The adapterFactory.
	 */
	public CustomAdapterFactoryContentProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * Returns a new PropertySource which returns a custom PropertyDescriptor
	 * 
	 * @param object
	 *            The object.
	 * @param itemPropertySource
	 *            The itemPropertySource.
	 * @return The custom PropertySource.
	 */
	@Override
	protected IPropertySource createPropertySource(Object object,
			IItemPropertySource itemPropertySource)
	{
		
		return new PropertySource(object, itemPropertySource)
		{

			@Override
			protected IPropertyDescriptor createPropertyDescriptor(
					IItemPropertyDescriptor itemPropertyDescriptor)
			{

				// tries to load a property descriptor from extension point
				try {
					IPropertyDescriptor customPropertyDescriptor = PropertyDescriptorExtensionPoint.getPropertyDescriptor(object, itemPropertyDescriptor);
					if (customPropertyDescriptor != null)
						return customPropertyDescriptor;
				} catch (Exception ex) {
					Logger.getLogger(CustomAdapterFactoryContentProvider.class).warning("Could not use custom propertyDescriptor from extension for " + itemPropertyDescriptor.getDisplayName(object));
				}
				return super.createPropertyDescriptor(itemPropertyDescriptor);
			}
		};
	}

}
