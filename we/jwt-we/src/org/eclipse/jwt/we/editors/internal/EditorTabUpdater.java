/**
 * File:    EditorTabUpdater.java
 * Created: 26.05.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.internal;

import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.pages.activityEditor.WEEditorSheet;
import org.eclipse.swt.custom.CTabFolder2Listener;
import org.eclipse.swt.custom.CTabFolderEvent;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IEditorPart;

/**
 * This class is a listener for the CTabFolder of the MultiPageEditor. It serves
 * two purposes: - Update the title name and title image if a different tab is
 * selected - If a tab is closed, the {@link ActivityEditor} of a tab has to be
 * disposed
 * 
 * The selection listener works only for manual selection and not if a new tab
 * is opened/selected/closed automatically. These cases are handled by the
 * openActivity/removeActivity functions in the {@link ActivityEditor} which are
 * responsible for disposing the {@link WEEditorSheet}.
 * 
 * @version $Id
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class EditorTabUpdater implements CTabFolder2Listener, SelectionListener,
		IDisposable
{

	/**
	 * The editor which should be updated.
	 */
	private WEEditor editor;


	/**
	 * @param editor
	 *            The editor which title is updated.
	 */
	public EditorTabUpdater(WEEditor editor)
	{
		assert editor != null;
		this.editor = editor;

		editor.getTabFolder().addSelectionListener(this);
		editor.getTabFolder().addCTabFolder2Listener(this);
	}


	// ///////////////////////////////////////////////////////////////////////////
	// Listen for tab selection
	//

	/**
	 * Update the title name and image, selection and palette provider and grid
	 * management if different tab is selected.
	 * 
	 * @param e
	 *            an event containing information about the focus change
	 */
	public void widgetSelected(SelectionEvent e)
	{
		editor.activatePage(editor.getActivePage());
	}


	/**
	 * Sent when a control loses focus.
	 * 
	 * @param e
	 *            an event containing information about the focus change
	 */
	public void widgetDefaultSelected(SelectionEvent e)
	{
	}


	// ///////////////////////////////////////////////////////////////////////////
	// Listen for tab close
	//

	/**
	 * Dispose of the editor if tab is closed.
	 * 
	 * @param e
	 *            an event containing information about the focus change
	 */
	public void close(CTabFolderEvent event)
	{
		CTabItem tabItem = (CTabItem) event.item;

		// dispose of IEditorPart or Control
		if (tabItem.getData() instanceof IEditorPart)
		{
			// find out the number of the editor
			int i = -1;
			for (CTabItem item : tabItem.getParent().getItems())
			{
				i++;

				if (item == tabItem)
				{
					break;
				}
			}

			// properly dispose of editor
			if (i > -1)
			{
				editor.removePage(i);
			}
			else
			{
				((IEditorPart) tabItem.getData()).dispose();
			}

			// fix: if a background tab is closed, its dispose removes the
			// default
			// tool and no new tool is set
			editor.getEditDomain().loadDefaultTool();
		}
		else
		{
			((Control) tabItem.getControl()).dispose();
		}
	}


	public void minimize(CTabFolderEvent event)
	{
	}


	public void maximize(CTabFolderEvent event)
	{
	}


	public void showList(CTabFolderEvent event)
	{
	}


	public void restore(CTabFolderEvent event)
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.services.IDisposable#dispose()
	 */
	public void dispose()
	{
		// remove listeners
		// the TabFolder may be already disposed - dont know
		if (!editor.getTabFolder().isDisposed())
		{
			editor.getTabFolder().removeSelectionListener(this);
			editor.getTabFolder().removeCTabFolder2Listener(this);
		}
	}

}
