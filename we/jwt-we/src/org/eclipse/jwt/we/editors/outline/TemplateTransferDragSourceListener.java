/**
 * File:    TemplateTransferDragSourceListener.java
 * Created: 02.04.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.outline;

import org.eclipse.gef.dnd.TemplateTransfer;
import org.eclipse.jface.util.TransferDragSourceListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.factories.EcoreCopyFactory;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;

/**
 * Listener to transfer model element from the outline to the editor via drag &
 * drop.
 * 
 * @version $Id: TemplateTransferDragSourceListener.java,v 1.5 2008/09/30
 *          07:53:42 flautenba Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 */
public class TemplateTransferDragSourceListener implements
		TransferDragSourceListener
{

	/**
	 * The workflow editor.
	 */
	private WEEditor weeditor;

	/**
	 * The source of the drag action.
	 */
	private ISelectionProvider dragSource;


	/**
	 * @param dragSource
	 *            The source of the drag action.
	 */
	public TemplateTransferDragSourceListener(WEEditor weeditor,
			ISelectionProvider dragSource)
	{
		assert dragSource != null;
		this.dragSource = dragSource;
		this.weeditor = weeditor;
	}


	/**
	 * Creates a template for transfer.
	 * 
	 * @return A template.
	 */
	private Object getTemplate()
	{
		ISelection selection = dragSource.getSelection();

		if (selection instanceof IStructuredSelection)
		{
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;

			return new EcoreCopyFactory(weeditor, structuredSelection.toList());
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.util.TransferDragSourceListener#getTransfer()
	 */
	public Transfer getTransfer()
	{
		return TemplateTransfer.getInstance();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.dnd.DragSourceListener#dragStart(org.eclipse.swt.dnd.
	 * DragSourceEvent)
	 */
	public void dragStart(DragSourceEvent event)
	{
		Object template = getTemplate();
		if (template == null)
		{
			event.doit = false;
		}
		TemplateTransfer.getInstance().setTemplate(template);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.dnd.DragSourceListener#dragSetData(org.eclipse.swt.dnd
	 * .DragSourceEvent)
	 */
	public void dragSetData(DragSourceEvent event)
	{
		event.data = getTemplate();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.dnd.DragSourceListener#dragFinished(org.eclipse.swt.dnd
	 * .DragSourceEvent)
	 */
	public void dragFinished(DragSourceEvent event)
	{
		TemplateTransfer.getInstance().setTemplate(null);
	}
}
