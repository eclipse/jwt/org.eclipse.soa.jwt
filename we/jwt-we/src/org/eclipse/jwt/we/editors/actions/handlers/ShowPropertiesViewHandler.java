/**
 * File:    ShowPropertiesViewHandler.java
 * Created: 26.09.2007
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.meta.model.core.ModelElement;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;


/**
 * Shows the property view for the selected element.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class ShowPropertiesViewHandler
		extends WEActionHandler
{

	/**
	 * Constructor.
	 */
	public ShowPropertiesViewHandler()
	{
		// requires open WEEditor
		super(true);
	}


	/**
	 * Refresh enabled state if selection has changed.
	 */
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection)
	{
		if (GeneralHelper.getActiveInstance() != null
				&& selection instanceof IStructuredSelection
				&& ((IStructuredSelection) selection).getFirstElement() instanceof ModelElement)
		{
			setEnabled(true);
		}
		else
		{
			setEnabled(false);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		try
		{
			GeneralHelper.getActiveInstance().getSite().getPage().showView(
					IPageLayout.ID_PROP_SHEET);
		}
		catch (PartInitException exception)
		{
			exception.printStackTrace();
		}

		return null;
	}

}
