/**
 * File:    WEActionDelegate.java
 * Created: 12.08.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.internal;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;


/**
 * This is a base class for an action which is as the same time an
 * "org.eclipse.jface.action.Action" and implements
 * "org.eclipse.ui.IWorkbenchWindowActionDelegate" as well as
 * "org.eclipse.ui.ISelectionListener". As a consequence this action can be added to
 * menus/toolbars either manually or by using an extension point (in this case two
 * instances are created, one of which acts as a proxy).
 * 
 * Additionally, it can be set to be only activated if an WEEditor is active.
 * 
 * @version $Id: WEActionDelegate.java
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public abstract class WEActionDelegate
		extends Action
		implements IWorkbenchWindowActionDelegate, ISelectionListener
{

	/**
	 * The workbench window
	 */
	private IWorkbenchWindow window;

	/**
	 * Indicates wheter this action should only be enabled if an WEEditor is active
	 */
	private boolean requiresOpenEditor = false;


	/**
	 * Constructor
	 */
	public WEActionDelegate()
	{
		super();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWindowActionDelegate#init(org.eclipse.ui.IWorkbenchWindow)
	 */
	public void init(IWorkbenchWindow window)
	{
		this.window = window;
	}


	protected IWorkbenchWindow getWindow()
	{
		return window;
	}


	public boolean isRequiresOpenEditor()
	{
		return requiresOpenEditor;
	}


	public void setRequiresOpenEditor(boolean requiresOpenEditor)
	{
		this.requiresOpenEditor = requiresOpenEditor;
	}


	/**
	 * Implements the ActionDelegate interface This gets called if action was added using
	 * an extension point
	 */
	public void selectionChanged(IAction action, ISelection selection)
	{
		// workaround:
		// if another plugin loads this action using an extension point
		// it cannot access the plugin.properties of jwt-we, so the text
		// has to be set manually in the action and is updated at the
		// first possible occasion.
		if (action.getText() == null)
		{
			action.setText(getText());
		}

		if (action.getDescription() == null)
		{
			action.setDescription(getDescription());
			action.setToolTipText(getDescription());
		}

		if (action.getImageDescriptor() == null)
		{
			action.setImageDescriptor(getImageDescriptor());
		}

		// set the enabled state of the proxy action
		setEnabledState(action);
	}


	/**
	 * Implements the SelectionListener interface This gets called if action was added
	 * manually
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection)
	{
		// set the enabled state of this action
		setEnabledState(this);
	}


	/**
	 * Set the enabled state of the action
	 */
	public void setEnabledState(IAction action)
	{
		// only if it is required
		if (isRequiresOpenEditor())
		{
			if (GeneralHelper.getActiveInstance() != null)
			{
				action.setEnabled(true);
			}
			else
			{
				action.setEnabled(false);
			}
		}
	}


	/**
	 * This gets called if the action was added using an extension point.
	 */
	// @Override
	public void run(IAction action)
	{
		run();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWindowActionDelegate#dispose()
	 */
	public void dispose()
	{
		window = null;
	}

}
