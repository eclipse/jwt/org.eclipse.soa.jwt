/**
 * File:    EditorTabUpdater.java
 * Created: 26.05.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.editors.internal;

import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.contexts.IContextActivation;
import org.eclipse.ui.contexts.IContextService;


/**
 * This class listenes to part activation.
 * 
 * @version $Id: PartActivationListener.java,v 1.3 2009-11-26 12:41:32 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class PartActivationListener
		implements IPartListener
{

	/**
	 * The WE context ID
	 */
	private static final String WE_EDITOR_CONTEXT = "org.eclipse.jwt.we.contexts.weeditor"; //$NON-NLS-1$

	/**
	 * The Workflow Editor part.
	 */
	private WEEditor multiPageEditor;

	/**
	 * Store the context activation.
	 */
	private IContextActivation fContextActivation;


	/**
	 * The constructor.
	 * 
	 * @param multiPageEditor
	 */
	public PartActivationListener(WEEditor multiPageEditor)
	{
		super();
		this.multiPageEditor = multiPageEditor;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener#partActivated(org.eclipse.ui.IWorkbenchPart)
	 */
	public void partActivated(IWorkbenchPart part)
	{
		// activate the context
		if (part == multiPageEditor)
		{
			IWorkbench workbench = PlatformUI.getWorkbench();
			IContextService contextService = (IContextService) workbench
					.getAdapter(IContextService.class);
			fContextActivation = contextService.activateContext(WE_EDITOR_CONTEXT);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener#partDeactivated(org.eclipse.ui.IWorkbenchPart)
	 */
	public void partDeactivated(IWorkbenchPart part)
	{
		// deactivate the context
		if (part == multiPageEditor)
		{
			IWorkbench workbench = PlatformUI.getWorkbench();
			if (fContextActivation != null)
			{
				IContextService contextService = (IContextService) workbench
						.getAdapter(IContextService.class);
				contextService.deactivateContext(fContextActivation);
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener#partBroughtToTop(org.eclipse.ui.IWorkbenchPart)
	 */
	public void partBroughtToTop(IWorkbenchPart part)
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener#partClosed(org.eclipse.ui.IWorkbenchPart)
	 */
	public void partClosed(IWorkbenchPart part)
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener#partOpened(org.eclipse.ui.IWorkbenchPart)
	 */
	public void partOpened(IWorkbenchPart part)
	{
	}

}
