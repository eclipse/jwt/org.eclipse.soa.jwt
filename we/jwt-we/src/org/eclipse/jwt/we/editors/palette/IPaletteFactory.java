/**
 * File:    IPaletteFactory.java
 * Created: 02.02.2009
 *
/*******************************************************************************
 * Copyright (C) 2007  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - initial API of the palette factory.
 *******************************************************************************/

package org.eclipse.jwt.we.editors.palette;

import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.jwt.we.editors.WEEditor;

/**
 * 
 * @author Marc Blachon, Bull SAS, France, www.bull.com
 */
public interface IPaletteFactory {

	/**
	 * Returns PaletteRoot palette.
	 * @param editor the Workflow Editor.
	 * @return returns a PaletteRoot .
	 */
	public abstract PaletteRoot getPaletteRoot(WEEditor editor);
}
