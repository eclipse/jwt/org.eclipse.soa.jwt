/**
 * File:    ModelOutlineAreaOverview.java
 * Created: 12.12.2008
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    - Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg 
 *    - Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *******************************************************************************/

package org.eclipse.jwt.we.editors.outline;

import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.draw2d.Viewport;
import org.eclipse.draw2d.parts.ScrollableThumbnail;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.pages.activityEditor.WEEditorSheet;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

/**
 * A model overview outline control for the outline page.
 * 
 * @version $Id: ModelOutlineAreaOverview.java,v 1.2 2009/11/26 12:41:38 chsaad
 *          Exp $
 * @author Florian Lautenbacher, Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class ModelOutlineAreaOverview
{

	/**
	 * The canvas.
	 */
	private Canvas overview;

	/**
	 * Rhe rooteditpart which is displayed.
	 */
	private ScalableFreeformRootEditPart loadedRootEditPart;

	/**
	 * The thumbnail.
	 */
	private ScrollableThumbnail thumbnail;

	/**
	 * The lightweightsystem.
	 */
	private LightweightSystem lws;


	/**
	 * The constructor.
	 * 
	 * @param parent
	 *            The parent composite.
	 */
	public ModelOutlineAreaOverview(ModelOutlinePage outlinePage)
	{
		// create the canvas
		overview = new Canvas(((Composite) outlinePage.getControl()), SWT.NONE);
		overview.setBackground(PreferenceReader.appearanceEditorColor.get());

		final ModelOutlinePage outlineP = outlinePage;

		// add listener that stores the new weight if resized
		overview.addControlListener(new ControlListener()
		{

			public void controlMoved(ControlEvent e)
			{
			}


			public void controlResized(ControlEvent e)
			{
				// save positions if both are displayed (i.e. preference setting
				// and
				// current mode both indicate that both areas are displayed)
				if (PreferenceReader.outlineShowArea.get().equals(
						ModelOutlinePage.OUTLINE_AREA_BOTH)
						&& outlineP.getCurrentDisplayMode().equals(
								ModelOutlinePage.OUTLINE_AREA_BOTH))
				{
					PreferenceReader.outlineOverviewSize.set(((SashForm) outlineP
							.getControl()).getWeights()[0]);
					PreferenceReader.outlineTreeviewSize.set(((SashForm) outlineP
							.getControl()).getWeights()[1]);
				}
			}
		});
	}


	/**
	 * Shows a thumbnail of the given root edit part and deactivates the old
	 * one.
	 * 
	 * @param rootEditPart
	 */
	public void showThumbnail(ScalableFreeformRootEditPart rootEditPart)
	{
		// exit if the same part (or null) is already loaded
		if (loadedRootEditPart == rootEditPart)
		{
			return;
		}
		else if (rootEditPart == null)
		{
			rootEditPart = new ScalableFreeformRootEditPart();
		}

		if (thumbnail != null)
		{
			overview.redraw();
		}

		if (thumbnail == null)
		{
			// create new thumbnail
			thumbnail = new ScrollableThumbnail((Viewport) rootEditPart.getFigure());
		}
		else
		{
			// update old thumbnail
			thumbnail.setViewport((Viewport) rootEditPart.getFigure());
		}
		thumbnail.setSource(rootEditPart.getLayer(LayerConstants.PRINTABLE_LAYERS));

		// create lws
		lws = new LightweightSystem(overview);
		lws.setContents(thumbnail);

		loadedRootEditPart = rootEditPart;
	}


	/**
	 * Refreshes the outline overview (called on view, preference change).
	 */
	public void refresh(WEEditor editor)
	{
		if (editor.getActiveEditor() instanceof WEEditorSheet)
		{
			showThumbnail(editor.getCurrentActivitySheet().getRootEditPart());
		}
		else
		{
			showThumbnail(null);
		}
	}


	/**
	 * Dispose.
	 */
	public void dispose()
	{
		// deactivate thumbnail
		if (thumbnail != null)
		{
			thumbnail.deactivate();
			thumbnail = null;
		}

		// remove overview
		if (overview != null && !overview.isDisposed())
		{
			overview.dispose();
		}
		overview = null;

		// the lightweightsystem
		lws = null;

		// the reference to the loaded root edit part
		loadedRootEditPart = null;
	}
}
