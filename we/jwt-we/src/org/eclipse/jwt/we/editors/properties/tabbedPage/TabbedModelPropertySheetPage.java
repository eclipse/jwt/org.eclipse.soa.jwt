/**
 * File:    TabbedModelPropertySheetPage.java
 * Created: 09.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.properties.tabbedPage;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.ui.views.properties.IPropertySourceProvider;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;


/**
 * This is a standard page for a multitab property sheet.
 * 
 * @version $Id: TabbedModelPropertySheetPage.java,v 1.5 2009-11-26 12:41:47 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class TabbedModelPropertySheetPage
		extends TabbedPropertySheetPage
{

	/**
	 * The main editor
	 */
	private WEEditor editor;


	/**
	 * Creates a new TabbedModelPropertySheetPage.
	 * 
	 * @param adapterFactory
	 *            The factory that provides an {@link IPropertySourceProvider}.
	 */
	public TabbedModelPropertySheetPage(WEEditor editor)
	{
		super(editor);
		this.editor = editor;
	}


	/**
	 * Get the EMF AdapterFactory for this editor.
	 * 
	 * @return the EMF AdapterFactory for this editor.
	 */
	public AdapterFactory getAdapterFactory()
	{
		return editor.getAdapterFactory();
	}

}