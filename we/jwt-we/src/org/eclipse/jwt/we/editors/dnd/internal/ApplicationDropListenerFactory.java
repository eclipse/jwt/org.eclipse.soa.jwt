/**
 * File:    ApplicationDropListenerFactory
 * Created: 13.02.2009
 *
/*******************************************************************************
 * Copyright (c) 2009   Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and Implementation
 *******************************************************************************/
package org.eclipse.jwt.we.editors.dnd.internal;

import java.io.File;

import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.jwt.meta.model.application.ApplicationPackage;
import org.eclipse.jwt.meta.model.core.Model;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.jwt.we.commands.editdomain.GefEmfEditingDomain;
import org.eclipse.jwt.we.editors.dnd.IDropListenerFactory;
import org.eclipse.jwt.we.editors.pages.activityEditor.WEEditorSheet;


public class ApplicationDropListenerFactory implements IDropListenerFactory {

	public TransferDropTargetListener createWEEditorSheetDropListener(WEEditorSheet viewer, Activity activity) {
		return new JavaApplicationDiagramDropListerner(viewer, activity);
	}

	public TransferDropTargetListener createWEOutlineDropListerner(TreeViewer viewer, Model model, GefEmfEditingDomain editingDomain) {
		return new JavaApplicationTreeDropListener(model, editingDomain);
	}

	/**
	 * 
	 * @param files
	 * @return an {@link Application} with Java Class attribute set
	 */
	static Application createApplication(String[] files) {
		if (files.length != 1 || !files[0].toLowerCase().endsWith(".java")) //$NON-NLS-1$
			return null;
		
		String javaFile = new File(files[0]).getName();
		String className = javaFile.substring(0, javaFile.length() - ".java".length()); //$NON-NLS-1$
		
		Application application = (Application)ApplicationPackage.eINSTANCE.getEFactoryInstance().create(ApplicationPackage.Literals.APPLICATION);
		application.setName(className);
		application.setJavaClass(className); // TODO use real class name (with package
		return application;
	}
}
