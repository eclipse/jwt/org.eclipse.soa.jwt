/**
 * File:    PrintAction.java
 * Created: 11.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.internalActions;

import org.eclipse.gef.GraphicalViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jwt.we.editors.pages.activityEditor.internal.IActivityEditor;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;


/**
 * This action provides suppport for printing the content of a {@link GraphicalViewer}.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class PrintAction
		extends org.eclipse.gef.ui.actions.PrintAction
		implements ISelectionListener
{

	/**
	 * @param part
	 *            The workbench part associated with this PrintAction.
	 */
	public PrintAction(IWorkbenchPart part)
	{
		super(part);

		// add selection listener
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService()
				.addSelectionListener(this);
	}


	/**
	 * Refresh enabled state if selection has changed. Only enabled when an activity
	 * editor is active.
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection)
	{
		// set to false if no JWT instance is active
		if (GeneralHelper.getActiveInstance() == null)
		{
			setEnabled(false);
			return;
		}

		// set enabled to true only if an IActivityEditor is enabled
		if (GeneralHelper.getActiveInstance().getActiveEditor() instanceof IActivityEditor)
		{
			setEnabled(calculateEnabled());
		}
		else
		{
			setEnabled(false);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#update()
	 */
	@Override
	public void update()
	{
		// this method for updating the enabled state would only work for the
		// activityeditor but not for the overview sheet
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#dispose()
	 */
	@Override
	public void dispose()
	{
		super.dispose();

		// remove selection listener
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService()
				.removeSelectionListener(this);
	}

}
