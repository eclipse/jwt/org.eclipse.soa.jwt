/**
 * File:    WEExternalActionsManager.java
 * Created: 07.07.2008
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.external;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.misc.extensions.ExtensionsHelper;
import org.eclipse.jwt.we.misc.logging.Logger;


/**
 * Loads and manages the external actions which are added to JWT using the
 * 'org.eclipse.jwt.we.menu' extension point.
 * 
 * @version $Id: WEExternalActionsManager.java,v 1.10 2009-11-26 12:41:49 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class WEExternalActionsManager
{

	/**
	 * A Logger.
	 */
	private static final Logger logger = Logger.getLogger(WEExternalActionsManager.class);

	/**
	 * The instance.
	 */
	private static WEExternalActionsManager myinstance = null;

	/**
	 * The list of external actions loaded from the extension point.
	 */
	private List<WEExternalAction> externalActionList = new ArrayList<WEExternalAction>();


	/**
	 * Constructor.
	 */
	private WEExternalActionsManager()
	{
		super();

		// read the actions
		if (externalActionList.size() == 0)
		{
			readActions();
		}
	}


	/**
	 * Returns the singleton instance.
	 */
	public static WEExternalActionsManager getInstance()
	{
		if (myinstance == null)
			myinstance = new WEExternalActionsManager();
		return myinstance;
	}


	/**
	 * Read the external actions from the extension point.
	 */
	private void readActions()
	{
		// the extension point id is read from the properties file
		String extension_point_id = PluginProperties.extension_point_menu;

		try
		{
			IConfigurationElement[] confElements = ExtensionsHelper
					.findConfigurationElements(extension_point_id);

			// exit if there are no extensions registered
			if (confElements == null)
			{
				return;
			}

			// read extensions
			for (IConfigurationElement point : confElements)
			{
				// instantiate the extern action class (derived from WEExternAction)
				WEExternalAction action = (WEExternalAction) point
						.createExecutableExtension("class"); //$NON-NLS-1$

				// read extension properties and set them in the created action class
				action.setText(point.getAttribute("name")); //$NON-NLS-1$

				action.setDescription(point.getAttribute("description")); //$NON-NLS-1$

				action.setId(point.getAttribute("name") + "ExternalAction"); //$NON-NLS-1$ //$NON-NLS-2$

				if (point.getAttribute("showInMenu").equals("true")) //$NON-NLS-1$ //$NON-NLS-2$
					action.setShowInMenu(true);
				else
					action.setShowInMenu(false);

				if (point.getAttribute("showInToolbar").equals("true")) //$NON-NLS-1$ //$NON-NLS-2$
					action.setShowInToolbar(true);
				else
					action.setShowInToolbar(false);

				if (point.getAttribute("requiresOpenEditor").equals("true")) //$NON-NLS-1$ //$NON-NLS-2$
					action.setRequiresOpenEditor(true);
				else
					action.setRequiresOpenEditor(false);

				if (point.getAttribute("forceText").equals("true")) //$NON-NLS-1$ //$NON-NLS-2$
					action.setForceText(true);
				else
					action.setForceText(false);

				// set the action image descriptor if supplied
				if (action.getImage() != null)
				{
					action.setImageDescriptor(action.getImage());
				}

				// add the action to the action list
				externalActionList.add(action);

				// create log entry
				logger.info("JWT Extension - found ACTION at " + extension_point_id //$NON-NLS-1$
						+ ": " + action.getText()); //$NON-NLS-1$
			}
		}
		catch (Exception e)
		{
			logger.severe("Extension point load failed for " + extension_point_id, e); //$NON-NLS-1$
		}
	}


	/**
	 * Returns the list of external actions.
	 */
	public List<WEExternalAction> getExternalActions()
	{
		return (ArrayList) externalActionList;
	}

}