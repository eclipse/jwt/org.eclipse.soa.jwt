/**
 * File:    ViewHandler.java
 * Created: 06.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.managed.views;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;


/**
 * This is a handler for the toolbar dropdown which contains a list of all available
 * views. When the dropdown button (not the triangle) is selected, an event is created
 * that opens the dropdown menu by simulating a click on the triangle button.
 * 
 * @version $Id: ViewDropDownHandler.java,v 1.3 2009-11-26 12:41:34 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class ViewDropDownHandler
		extends WEActionHandler
{

	/**
	 * Constructor.
	 */
	public ViewDropDownHandler()
	{
		// does not require open WEEditor
		super(false);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.
	 * ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		// get the original event
		Event newEvent = (Event) event.getTrigger();

		// set the event type to "open dropdown menu"
		newEvent.detail = 4;

		// send it to the toolitem
		((Listener) newEvent.widget.getListeners(SWT.Selection)[0]).handleEvent(newEvent);

		return null;
	}

}
