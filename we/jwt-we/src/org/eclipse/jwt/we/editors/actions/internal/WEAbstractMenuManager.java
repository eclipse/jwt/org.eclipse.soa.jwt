/**
 * File:    WEAbstractMenuManager.java
 * Created: 29.11.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.internal;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.application.IActionBarConfigurer;


/**
 * Abstract Menu Manager class
 * 
 * @version $Id: WEAbstractMenuManager.java,v 1.3 2009-11-26 12:41:37 chsaad Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public abstract class WEAbstractMenuManager
{

	protected IMenuManager menu = null;
	protected IActionBarConfigurer actionbar = null;
	protected IWorkbenchWindow window = null;


	protected WEAbstractMenuManager()
	{
	}


	/**
	 * Sets the handle to the current window
	 * 
	 * @param window
	 */
	public void setWindowHandle(IWorkbenchWindow window)
	{
		this.window = window;
	}


	/**
	 * Sets the handle to the current menu
	 * 
	 * @param menu
	 */
	public void setMenu(IMenuManager menu)
	{
		this.menu = menu;
	}


	/**
	 * Sets the action bar configurer
	 * 
	 * @param acbar
	 *            The actionbar to be used
	 */
	public void setActionBarConfigurer(IActionBarConfigurer acbar)
	{
		actionbar = acbar;
	}


	/**
	 * Fills the menu Should be overriden
	 */
	public void fillMenu()
	{

	}


	/**
	 * Adds the specified action to the given menu and also registers the action with the
	 * action bar configurer, in order to activate its key binding.
	 */
	protected void addToMenuAndRegister(IAction action)
	{
		if (menu != null)
		{
			menu.add(action);
		}
		if (actionbar != null)
		{
			actionbar.registerGlobalAction(action);
		}
	}
}
