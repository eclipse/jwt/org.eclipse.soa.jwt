/**
 * File:    CustomViewerDragAdapter.java
 * Created: 16.06.2008
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.outline;

import org.eclipse.emf.edit.ui.dnd.LocalTransfer;
import org.eclipse.emf.edit.ui.dnd.ViewerDragAdapter;
import org.eclipse.jface.util.TransferDragSourceListener;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.dnd.Transfer;


/**
 * A viewer drag adapter that is compatible with the TransferDragSourceListener interface.
 * 
 * @version $Id: CustomViewerDragAdapter.java,v 1.3 2009-11-26 12:41:38 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class CustomViewerDragAdapter
		extends ViewerDragAdapter
		implements TransferDragSourceListener
{

	/**
	 * The constructor.
	 * 
	 * @param viewer
	 */
	public CustomViewerDragAdapter(Viewer viewer)
	{
		super(viewer);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.util.TransferDragSourceListener#getTransfer()
	 */
	public Transfer getTransfer()
	{
		return LocalTransfer.getInstance();
	}

}