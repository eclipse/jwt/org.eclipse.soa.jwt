/**
 * File:    WEEditorSheet.java
 * Created: 15.06.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.pages.activityEditor.internal;

import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.dnd.TemplateTransferDropTargetListener;


/**
 * Thie template transfer drop target listener activates the enablement check using the
 * commands. This means that the generated commands that handle the drop are asked for
 * their enablement state and the drop is canceled if they return false.
 * 
 * @version $Id: CustomTemplateTransferDropTargetListener.java,v 1.3 2009-11-26 12:41:45 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class CustomTemplateTransferDropTargetListener
		extends TemplateTransferDropTargetListener
{

	/**
	 * The constructor.
	 * 
	 * @param viewer
	 */
	public CustomTemplateTransferDropTargetListener(EditPartViewer viewer)
	{
		super(viewer);

		// activate the command enablement check
		setEnablementDeterminedByCommand(true);
	}

}
