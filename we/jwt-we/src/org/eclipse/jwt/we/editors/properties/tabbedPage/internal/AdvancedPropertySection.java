/**
 * File:    AdvancedPropertySection.java
 * Created: 09.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.editors.properties.tabbedPage.internal;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jwt.we.editors.properties.CustomAdapterFactoryContentProvider;
import org.eclipse.jwt.we.editors.properties.ModelPropertySourceProvider;
import org.eclipse.jwt.we.editors.properties.singlePage.ModelPropertySheetPage;
import org.eclipse.jwt.we.editors.properties.tabbedPage.TabbedModelPropertySheetPage;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.PropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;


/**
 * This class represents a section in a property tab (TabbedModelPropertySheetPage). The
 * content of the section is the standard ModelPropertySheetPage which displays the
 * properties of the selected model element.
 * 
 * @version $Id: AdvancedPropertySection.java,v 1.3 2009-11-26 12:41:53 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class AdvancedPropertySection
		extends AbstractPropertySection
{

	/**
	 * The Property Sheet Page.
	 */
	protected PropertySheetPage page;


	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#createControls(org.eclipse.swt.widgets.Composite,
	 *      org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage)
	 */
	public void createControls(Composite parent,
			TabbedPropertySheetPage tabbedPropertySheetPage)
	{
		super.createControls(parent, tabbedPropertySheetPage);
		Composite composite = getWidgetFactory().createFlatFormComposite(parent);
		page = new ModelPropertySheetPage(
				((TabbedModelPropertySheetPage) tabbedPropertySheetPage)
						.getAdapterFactory());

		page.createControl(composite);
		FormData data = new FormData();
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(100, 0);
		data.top = new FormAttachment(0, 0);
		data.bottom = new FormAttachment(100, 0);
		page.getControl().setLayoutData(data);

		TabbedModelPropertySheetPage TabbedModelPropertySheetPage = (TabbedModelPropertySheetPage) tabbedPropertySheetPage;
		page.setPropertySourceProvider(new ModelPropertySourceProvider(
				new CustomAdapterFactoryContentProvider(TabbedModelPropertySheetPage
						.getAdapterFactory())));

	}


	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#setInput(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	public void setInput(IWorkbenchPart part, ISelection selection)
	{
		super.setInput(part, selection);
		page.selectionChanged(part, selection);
	}


	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#refresh()
	 */
	public void refresh()
	{
		page.refresh();
	}


	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#shouldUseExtraSpace()
	 */
	public boolean shouldUseExtraSpace()
	{
		return true;
	}


	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#dispose()
	 */
	public void dispose()
	{
		super.dispose();

		if (page != null)
		{
			page.dispose();
			page = null;
		}
	}
}