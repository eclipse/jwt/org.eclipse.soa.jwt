/**
 * File:    PreferenceInitializer.java
 * Created: 01.08.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.preferences.internal;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.editors.outline.ModelOutlinePage;
import org.eclipse.jwt.we.editors.preferences.PreferenceConstants;
import org.eclipse.jwt.we.editors.preferences.wrappers.WEPreferenceStoreInterface;
import org.eclipse.jwt.we.misc.util.FontUtil;
import org.eclipse.jwt.we.misc.views.LayoutDataManager;
import org.eclipse.jwt.we.parts.processes.ScopeEditPart;


/**
 * Class used to initialize default preference values.
 * 
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class PreferenceInitializer
		extends AbstractPreferenceInitializer
{

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences()
	{
		IPreferenceStore store = Plugin.getDefault().getPreferenceStore();

		// default values: simulator
		store.setDefault(PreferenceConstants.C_SIMULATOR_PATH, "");

		// default values: views
		store.setDefault(PreferenceConstants.C_VIEW_SELECTED, "Technical");
		store.setDefault(PreferenceConstants.C_VIEW_LAYOUTDATA_HANDLING, LayoutDataManager.VIEW_LAYOUTDATA_DIALOG);
		
		// default values: grid
		store.setDefault(PreferenceConstants.C_GRID_WIDTH, "14");
		store.setDefault(PreferenceConstants.C_GRID_HEIGHT, "14");
		store.setDefault(PreferenceConstants.C_GRID_VISIBLE, false);
		store.setDefault(PreferenceConstants.C_GRID_SNAP, true);

		// default values: guards
		store.setDefault(PreferenceConstants.C_GUARD_T_CUT, 70);
		store.setDefault(PreferenceConstants.C_GUARD_S_CUT, 70);
		store.setDefault(PreferenceConstants.C_GUARD_T_WRAP, 15);
		store.setDefault(PreferenceConstants.C_GUARD_S_WRAP, 15);
		store.setDefault(PreferenceConstants.C_GUARD_AUTOWRAP, true);

		// default values: appearance
		store.setDefault(PreferenceConstants.C_APPEARANCE_COLOR_EDITOR, "255,255,255");
		store.setDefault(PreferenceConstants.C_APPEARANCE_COLOR_TEXT, "0,0,128");
		store.setDefault(PreferenceConstants.C_APPEARANCE_COLOR_BORDER, "0,0,128");
		store.setDefault(PreferenceConstants.C_APPEARANCE_COLOR_FILL, "255,255,255");
		store.setDefault(PreferenceConstants.C_APPEARANCE_COLOR_SHADOW, "128,128,128");
		store.setDefault(PreferenceConstants.C_APPEARANCE_COLOR_GUARD_T, "0,0,128");
		store.setDefault(PreferenceConstants.C_APPEARANCE_COLOR_GUARD_S, "0,0,255");

		store.setDefault(PreferenceConstants.C_APPEARANCE_DEFAULT_FONT, FontUtil
				.serializeFont(FontUtil.getResizedFont(FontUtil.getSystemDefault(), 10)));
		store.setDefault(PreferenceConstants.C_APPEARANCE_REFELEMENT_FONT, FontUtil
				.serializeFont(FontUtil.getResizedFont(FontUtil.getSystemDefault(), 9)));
		store.setDefault(PreferenceConstants.C_APPEARANCE_GUARD_FONT, FontUtil
				.serializeFont(FontUtil.getResizedFont(FontUtil.getSystemDefault(), 8)));

		store.setDefault(PreferenceConstants.C_APPEARANCE_MINIMUM_SIZE, "24,24");
		store.setDefault(PreferenceConstants.C_APPEARANCE_FIGURE_ICON_SIZE, "24,24");
		store.setDefault(PreferenceConstants.C_APPEARANCE_LINE_WIDTH, 1);
		store.setDefault(PreferenceConstants.C_APPEARANCE_FIGURE_BAR_WIDTH, 5);
		store.setDefault(PreferenceConstants.C_APPEARANCE_CORNER_SIZE, 15);
		store.setDefault(PreferenceConstants.C_APPEARANCE_ANTIALIASING, true);
		store.setDefault(PreferenceConstants.C_APPEARANCE_SHADOW_VISIBLE, true);
		store.setDefault(PreferenceConstants.C_APPEARANCE_FEEDBACK_SHOW, true);
		store.setDefault(PreferenceConstants.C_APPEARANCE_OVERVIEW_SHOW, true);
		store.setDefault(PreferenceConstants.C_APPEARANCE_MOUSEPOS_SHOW, true);
		store.setDefault(PreferenceConstants.C_APPEARANCE_CON_ROUTER,
				ScopeEditPart.ROUTER_SHORTESTPATH);

		// default values: palette
		store.setDefault(PreferenceConstants.C_PALETTE_DOCK_LOCATION, 0);
		store.setDefault(PreferenceConstants.C_PALETTE_STATE, 0);
		store.setDefault(PreferenceConstants.C_PALETTE_WIDTH, 0);

		// default values: outline
		store.setDefault(PreferenceConstants.C_OUTLINE_SORT_ELEMENTS, true);
		store.setDefault(PreferenceConstants.C_OUTLINE_HIDE_ACTEDGES, true);
		store.setDefault(PreferenceConstants.C_OUTLINE_SHOW_AREA, ModelOutlinePage.OUTLINE_AREA_BOTH);
		store.setDefault(PreferenceConstants.C_OUTLINE_OVERVIEW_SIZE, 23);
		store.setDefault(PreferenceConstants.C_OUTLINE_TREEVIEW_SIZE, 77);
		
		// activate preference listener of the store
		WEPreferenceStoreInterface.activatePreferenceListener();
	}

}