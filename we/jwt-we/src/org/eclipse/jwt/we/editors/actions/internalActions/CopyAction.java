/**
 * File:    CopyAction.java
 * Created: 02.04.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- improved version
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.internalActions;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.gef.Disposable;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;


/**
 * Action to copy model elements.
 * 
 * <p>
 * The action copies the currently selected elements provided by the
 * {@link #selectionProvider}. The action updates itself on selection change.
 * </p>
 * 
 * After the copying, the currently displayed activity is selected, or, if no activity is
 * active, the top model element.
 * 
 * @version $Id: CopyAction.java,v 1.3 2009-11-26 12:41:20 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class CopyAction
		extends org.eclipse.emf.edit.ui.action.CopyAction
		implements Disposable
{

	/**
	 * Provides the selection for the elements to copy.
	 */
	private ISelectionProvider selectionProvider;


	/**
	 * Creates a new action.
	 * 
	 * @param domain
	 *            The EditingDomain to create the copy command.
	 * @param selectionProvider
	 *            Provides the selection for the elements to delete.
	 */
	public CopyAction(EditingDomain domain, ISelectionProvider selectionProvider)
	{
		super(domain);

		ISharedImages sharedImages = PlatformUI.getWorkbench().getSharedImages();
		setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_COPY));
		setDisabledImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_COPY_DISABLED));
		setId(ActionFactory.COPY.getId());

		selectionProvider.addSelectionChangedListener(this);
		this.selectionProvider = selectionProvider;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.ui.action.StaticSelectionCommandAction#run()
	 */
	@Override
	public void run()
	{
		super.run();

		// select the activity or the model after copying. this is important to update
		// the state of all actions/commands
		Activity displayedActivity = GeneralHelper.getActiveInstance()
				.getDisplayedActivityModel();

		if (displayedActivity != null)
		{
			GeneralHelper.getActiveInstance().getSelectionSynchronizer().setSelection(
					new StructuredSelection(displayedActivity));
		}
		else
		{
			GeneralHelper.getActiveInstance().getSelectionSynchronizer().setSelection(
					new StructuredSelection(GeneralHelper.getActiveInstance().getModel()));
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.Disposable#dispose()
	 */
	public void dispose()
	{
		selectionProvider.removeSelectionChangedListener(this);
	}
}
