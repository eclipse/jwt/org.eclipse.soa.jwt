/**
 * File:    IWEExternalSheetProvider.java
 * Created: 23.09.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.pages.externalSheet;

import org.eclipse.jwt.we.editors.WEEditor;


/**
 * An interface that must be implemented by external plugins that provide external tabs
 * for the JWT WE editor.
 * 
 * @version $Id: IWEExternalSheetProvider.java,v 1.5 2009-11-26 12:41:49 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 * @since 0.6.0
 */
public interface IWEExternalSheetProvider
{

	/**
	 * This method is called when the sheet is loaded into the main editor.
	 * 
	 * @param editor
	 *            The parent editor
	 */
	public void init(WEEditor editor);


	/**
	 * This method is called to obtain the editor sheet. It can be of type Control or
	 * IEditorPart.
	 * 
	 * @return editor sheet (Control or IEditorPart).
	 */
	public Object getExternalSheet();

}
