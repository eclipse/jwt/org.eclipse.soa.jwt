/**
 * File:    PalettePreferences.java
 * Created: 28.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.palette.internal;

import org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;


/**
 * This class redirects the setting/reading of palette preferences to the preference
 * store.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class PalettePreferences
		implements FlyoutPreferences
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences#getDockLocation()
	 */
	public int getDockLocation()
	{
		return PreferenceReader.paletteDockLocation.get();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences#setDockLocation(int)
	 */
	public void setDockLocation(int location)
	{
		PreferenceReader.paletteDockLocation.set(location);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences#getPaletteState()
	 */
	public int getPaletteState()
	{
		return PreferenceReader.paletteState.get();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences#setPaletteState(int)
	 */
	public void setPaletteState(int state)
	{
		PreferenceReader.paletteState.set(state);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences#getPaletteWidth()
	 */
	public int getPaletteWidth()
	{
		return PreferenceReader.paletteWidth.get();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences#setPaletteWidth(int)
	 */
	public void setPaletteWidth(int width)
	{
		PreferenceReader.paletteWidth.set(width);
	}

}