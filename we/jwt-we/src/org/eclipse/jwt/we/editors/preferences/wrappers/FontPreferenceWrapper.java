/**
 * File:    FontPreferenceWrapper.java
 * Created: 27.10.2007
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.preferences.wrappers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jwt.we.misc.util.FontUtil;
import org.eclipse.swt.graphics.Font;

/**
 * This is a preference wrapper for a font value. It provides getValue, setValue
 * and getDefaultValue methods. The connection to a preference store value is
 * made through its corresponding preference key. To access the preference
 * store, the class WEPreferenceStoreInterface is used.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class FontPreferenceWrapper
{

	/**
	 * The key of the preference.
	 */
	private String preferenceKey;

	/**
	 * The cached font.
	 */
	private Font cachedFont;
	/**
	 * The cached default font preference string.
	 */
	private String cachedFontString;

	/**
	 * The cached default font.
	 */

	private Font cachedFontDefault;
	/**
	 * The cached font preference string.
	 */
	private String cachedFontStringDefault;

	private static List<Font> fontRegistry;


	/**
	 * @param preferenceStore
	 *            The preferenceStore where the value is stored.
	 * @param key
	 *            The key of the preference.
	 */
	public FontPreferenceWrapper(String preferenceKey)
	{
		this.preferenceKey = preferenceKey;
		fontRegistry = new ArrayList<Font>();
	}


	/**
	 * @return The value of the preference.
	 */
	public Font get()
	{
		String fontDataString = WEPreferenceStoreInterface
				.getValueAsString(preferenceKey);

		if (cachedFont != null && !cachedFont.isDisposed()
				&& cachedFontString.equals(fontDataString))
		{
			return cachedFont;
		}

		cachedFont = FontUtil.deSerializeFont(fontDataString);
		cachedFontString = fontDataString;
		fontRegistry.add(cachedFont);

		return cachedFont;
	}


	/**
	 * @return The value of the preference.
	 */
	public void set(Font value)
	{
		String fontDataString = FontUtil.serializeFont(value);

		WEPreferenceStoreInterface.setValueString(preferenceKey, fontDataString);
	}


	/**
	 * @return The value of the preference.
	 */
	public Font getDefault()
	{
		String fontDataString = WEPreferenceStoreInterface
				.getDefaultValueAsString(preferenceKey);

		if (cachedFontDefault != null && !cachedFontDefault.isDisposed()
				&& cachedFontStringDefault.equals(fontDataString))
		{
			return cachedFontDefault;
		}

		cachedFontDefault = FontUtil.deSerializeFont(fontDataString);
		cachedFontStringDefault = fontDataString;
		fontRegistry.add(cachedFontDefault);

		return cachedFontDefault;
	}


	/**
	 * @return The value of the preference.
	 */
	public void setDefault()
	{
		WEPreferenceStoreInterface.setDefaultValueString(preferenceKey);
	}


	/**
	 * Dispose of all font resources.
	 */
	public static void disposeFonts()
	{
		for (Font font : fontRegistry)
		{
			if (!font.isDisposed())
			{
				font.dispose();
			}
		}

		fontRegistry.clear();
	}

}