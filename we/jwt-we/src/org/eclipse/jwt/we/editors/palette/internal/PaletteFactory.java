/**
 * File:    PaletteFactory.java
 * Created: 02.02.2009
 *
/*******************************************************************************
 * Copyright (C) 2007  Bull S. A. S.
 * Bull, Rue Jean Jaures, B.P.68, 78340, Les Clayes-sous-Bois
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - initial API and implementation of the palette factory.
 *******************************************************************************/

package org.eclipse.jwt.we.editors.palette.internal;

import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.palette.Palette;

/**
 * Palette factory implementing the IPaletteFactory interface providing the root
 * palette of the jwt-we editor.
 * 
 * @author Marc Blachon, Bull SAS, France, www.bull.com
 */
public class PaletteFactory implements
		org.eclipse.jwt.we.editors.palette.IPaletteFactory
{

	public PaletteRoot getPaletteRoot(final WEEditor editor)
	{
		final PaletteRoot paletteDefault = new Palette(editor, editor
				.getEmfEditingDomain().getResourceSet(), editor
				.getAdapterFactory());
		final PaletteRoot palette = new PaletteRoot();
		palette.setChildren(paletteDefault.getChildren());
		return palette;
	}

}
