/**
 * File:    RecentFileAction.java
 * Created: 10.10.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.managed.recentfiles;

import java.io.Serializable;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.PlatformUI;


/**
 * Standard Action for a recent file menu item
 * 
 * @version $Id: RecentFileAction.java,v 1.10 2009-11-26 12:41:48 chsaad Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class RecentFileAction
		extends Action
		implements Serializable
{

	private static final long serialVersionUID = -1469147461918518088L;
	String filename = "";  //$NON-NLS-1$


	/**
	 * Standardkonstruktor
	 * 
	 * @param filename
	 *            The name of the file
	 * @param id
	 *            The ID of this
	 */
	public RecentFileAction(String filename, String id)
	{
		super();
		this.setFilename(filename);
		super.setId(id);
		super.setText(this.filename);
	}


	/**
	 * Returns the current filename
	 * 
	 * @return String
	 */
	public String getFilename()
	{
		return filename;
	}


	/**
	 * Sets the current filename
	 * 
	 * @param filename
	 */
	public void setFilename(String filename)
	{
		this.filename = makeFilename(filename);
		super.setText(this.filename);
	}


	/**
	 * Simply cuts the "file:/" from the beginning of a filepath
	 * 
	 * @param filename
	 *            The filename
	 * @return String
	 */
	public static String makeFilename(String filename)
	{
		return filename.replace("file:/", "");  //$NON-NLS-1$  //$NON-NLS-2$
	}


	/**
	 * Always returns true
	 */
	@Override
	public boolean isEnabled()
	{
		return true;
	}


	/**
	 * Simply opens the file (if there is one)
	 */
	@Override
	public void run()
	{
		if (filename.equals(""))  //$NON-NLS-1$
			return;
		try
		{
			URI fileURI = null;

			try
			{
				fileURI = URI.createFileURI(filename);
			}
			catch (Exception e)
			{
			}

			if (fileURI == null)
			{
				try
				{
					fileURI = URI.createURI(filename);
				}
				catch (Exception e)
				{
				}
			}

			if (fileURI != null)
			{
				Plugin.openEditor(PlatformUI.getWorkbench(), fileURI);
			}
			else
			{
				MessageDialog.openError(null,
						PluginProperties.editor_ErrorMessage_title,
						NLS.bind(PluginProperties.editor_ErrorLoadingFile_message, filename));
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
