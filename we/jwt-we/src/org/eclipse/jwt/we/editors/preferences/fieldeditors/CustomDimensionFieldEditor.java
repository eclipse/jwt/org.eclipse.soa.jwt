/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.preferences.fieldeditors;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.widgets.Composite;


/**
 * A field editor for 2d-dimensions based on a string editor.
 * 
 * @version $Id: CustomDimensionFieldEditor.java,v 1.6 2009-11-26 12:41:52 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class CustomDimensionFieldEditor
		extends StringFieldEditor
{

	/**
	 * Constructor.
	 */
	public CustomDimensionFieldEditor(String name, String labelText, int width,
			int strategy, Composite parent)
	{
		super(name, labelText, width, strategy, parent);
	}


	/**
	 * Constructor.
	 */
	public CustomDimensionFieldEditor(String name, String labelText, int width,
			Composite parent)
	{
		super(name, labelText, width, VALIDATE_ON_KEY_STROKE, parent);
	}


	/**
	 * Constructor.
	 */
	public CustomDimensionFieldEditor(String name, String labelText, Composite parent)
	{
		super(name, labelText, UNLIMITED, parent);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.preference.StringFieldEditor#doCheckState()
	 */
	@Override
	protected boolean doCheckState()
	{

		// convert the text in the text field to dimension
		// if this fails, return false to display error message
		String[] dimensionString;

		try
		{
			dimensionString = getTextControl().getText().split(","); //$NON-NLS-1$
			new Dimension(new Integer(dimensionString[0]),
					new Integer(dimensionString[1]));

			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

}
