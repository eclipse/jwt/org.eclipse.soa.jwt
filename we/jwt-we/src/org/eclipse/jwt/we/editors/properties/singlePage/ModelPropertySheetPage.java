/**
 * File:    ModelPropertySheetPage.java
 * Created: 24.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.properties.singlePage;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.jwt.we.editors.properties.CustomAdapterFactoryContentProvider;
import org.eclipse.jwt.we.editors.properties.ModelPropertySourceProvider;
import org.eclipse.jwt.we.editors.properties.internal.IdentitySorter;
import org.eclipse.ui.views.properties.IPropertySourceProvider;
import org.eclipse.ui.views.properties.PropertySheetPage;


/**
 * {@link PropertySheetPage} to display the properties of the model elements.
 * 
 * @version $Id: ModelPropertySheetPage.java,v 1.5 2009-11-26 12:41:05 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ModelPropertySheetPage
		extends PropertySheetPage
{

	/**
	 * Creates a new PropertySheetPage.
	 * 
	 * @param adapterFactory
	 *            The factory that provides an {@link IPropertySourceProvider}.
	 */
	public ModelPropertySheetPage(AdapterFactory adapterFactory)
	{
		IPropertySourceProvider provider = new CustomAdapterFactoryContentProvider(
				adapterFactory);
		setPropertySourceProvider(new ModelPropertySourceProvider(provider));

		setSorter(new IdentitySorter());
	}
}
