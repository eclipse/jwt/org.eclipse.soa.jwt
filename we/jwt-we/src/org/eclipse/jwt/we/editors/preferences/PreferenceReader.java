/**
 * File:    PreferenceReader.java
 * Created: 27.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.preferences;

import org.eclipse.jwt.we.editors.preferences.wrappers.BooleanPreferenceWrapper;
import org.eclipse.jwt.we.editors.preferences.wrappers.ColorPreferenceWrapper;
import org.eclipse.jwt.we.editors.preferences.wrappers.DimensionPreferenceWrapper;
import org.eclipse.jwt.we.editors.preferences.wrappers.FontPreferenceWrapper;
import org.eclipse.jwt.we.editors.preferences.wrappers.IntPreferenceWrapper;
import org.eclipse.jwt.we.editors.preferences.wrappers.StringPreferenceWrapper;
import org.eclipse.jwt.we.figures.core.NamedIconFigure;
import org.eclipse.jwt.meta.model.core.ReferenceableElement;
import org.eclipse.jwt.meta.model.processes.Guard;


/**
 * This class provides access to the preferences which are stored in the preference store
 * through corresponding wrappers. This is the interface for preference access for the
 * application.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class PreferenceReader
{

	// -------------------------------------------------------------
	// Simulator Preferences
	// -------------------------------------------------------------

	/**
	 * The simulator path.
	 */
	public static final StringPreferenceWrapper simulatorPath = new StringPreferenceWrapper(
			PreferenceConstants.C_SIMULATOR_PATH);

	// -------------------------------------------------------------
	// View Preferences
	// -------------------------------------------------------------

	/**
	 * The selected view.
	 */
	public static final StringPreferenceWrapper viewSelected = new StringPreferenceWrapper(
			PreferenceConstants.C_VIEW_SELECTED);

	/**
	 * What to do when layoutdata is missing.
	 */
	public static final StringPreferenceWrapper viewLayoutData = new StringPreferenceWrapper(
			PreferenceConstants.C_VIEW_LAYOUTDATA_HANDLING);
	
	// -------------------------------------------------------------
	// Grid Preferences
	// -------------------------------------------------------------

	/**
	 * The grid width.
	 */
	public static final IntPreferenceWrapper gridWidth = new IntPreferenceWrapper(
			PreferenceConstants.C_GRID_WIDTH);
	/**
	 * The grid height
	 */
	public static final IntPreferenceWrapper gridHeight = new IntPreferenceWrapper(
			PreferenceConstants.C_GRID_HEIGHT);
	/**
	 * The grid visibility.
	 */
	public static final BooleanPreferenceWrapper gridVisible = new BooleanPreferenceWrapper(
			PreferenceConstants.C_GRID_VISIBLE);
	/**
	 * The snap to grid function.
	 */
	public static final BooleanPreferenceWrapper gridSnap = new BooleanPreferenceWrapper(
			PreferenceConstants.C_GRID_SNAP);

	// -------------------------------------------------------------
	// Guard Preferences
	// -------------------------------------------------------------

	/**
	 * The maximal length for textual guard description cut.
	 */
	public static final IntPreferenceWrapper guardTextualCut = new IntPreferenceWrapper(
			PreferenceConstants.C_GUARD_T_CUT);

	/**
	 * The maximal length for short guard description wrap.
	 */
	public static final IntPreferenceWrapper guardShortCut = new IntPreferenceWrapper(
			PreferenceConstants.C_GUARD_S_CUT);

	/**
	 * The maximal length for textual guard description cut.
	 */
	public static final IntPreferenceWrapper guardTextualWrap = new IntPreferenceWrapper(
			PreferenceConstants.C_GUARD_T_WRAP);

	/**
	 * The maximal length for short guard description wrap.
	 */
	public static final IntPreferenceWrapper guardShortWrap = new IntPreferenceWrapper(
			PreferenceConstants.C_GUARD_S_WRAP);

	/**
	 * The guard description autowrap feature.
	 */
	public static final BooleanPreferenceWrapper guardAutoWrap = new BooleanPreferenceWrapper(
			PreferenceConstants.C_GUARD_AUTOWRAP);

	// -------------------------------------------------------------
	// Appearance Preferences
	// -------------------------------------------------------------

	/**
	 * Editor background color.
	 */
	public static final ColorPreferenceWrapper appearanceEditorColor = new ColorPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_COLOR_EDITOR);

	/**
	 * Default text color.
	 */
	public static final ColorPreferenceWrapper appearanceTextColor = new ColorPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_COLOR_TEXT);

	/**
	 * Default border color.
	 */
	public static final ColorPreferenceWrapper appearanceBorderColor = new ColorPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_COLOR_BORDER);

	/**
	 * Default fill color.
	 */
	public static final ColorPreferenceWrapper appearanceFillColor = new ColorPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_COLOR_FILL);

	/**
	 * The color of element shadows.
	 */
	public static final ColorPreferenceWrapper appearanceShadowColor = new ColorPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_COLOR_SHADOW);

	/**
	 * The textual guard description color.
	 */
	public static final ColorPreferenceWrapper appearanceguardTextualColor = new ColorPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_COLOR_GUARD_T);

	/**
	 * The short guard description color.
	 */
	public static final ColorPreferenceWrapper appearanceguardShortColor = new ColorPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_COLOR_GUARD_S);

	/**
	 * The default font of the figures.
	 */
	public static final FontPreferenceWrapper appearanceDefaultFont = new FontPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_DEFAULT_FONT);

	/**
	 * The font of the {@link ReferenceableElement}s.
	 */
	public static final FontPreferenceWrapper appearanceReferenceableElementFont = new FontPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_REFELEMENT_FONT);

	/**
	 * The font of the {@link Guard}s.
	 */
	public static final FontPreferenceWrapper appearanceGuardFont = new FontPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_GUARD_FONT);

	/**
	 * The absolut minimum size of figures. No figure must be displayed smaller than this
	 * size.
	 */
	public static final DimensionPreferenceWrapper appearanceMinimumSize = new DimensionPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_MINIMUM_SIZE);

	/**
	 * The icon size of icons in an {@link NamedIconFigure}.
	 */
	public static final DimensionPreferenceWrapper appearanceFigureIconSize = new DimensionPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_FIGURE_ICON_SIZE);

	/**
	 * Width of lines.
	 */
	public static final IntPreferenceWrapper appearanceLineWidth = new IntPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_LINE_WIDTH);

	/**
	 * The absolute size of the corner of a rounded rectangle.
	 */
	public static final IntPreferenceWrapper appearanceCornerSize = new IntPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_CORNER_SIZE);

	/**
	 * Sets the value for antialiasing.
	 */
	public static final BooleanPreferenceWrapper appearanceAntialiasing = new BooleanPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_ANTIALIASING);

	/**
	 * The width of a bar figure.
	 */
	public static final IntPreferenceWrapper appearanceBarFigureWidth = new IntPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_FIGURE_BAR_WIDTH);

	/**
	 * The enabled state of element shadows.
	 */
	public static final BooleanPreferenceWrapper appearanceShadowVisible = new BooleanPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_SHADOW_VISIBLE);

	/**
	 * The enabled state of the feedback figure.
	 */
	public static final BooleanPreferenceWrapper appearanceFeedbackVisible = new BooleanPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_FEEDBACK_SHOW);

	/**
	 * The enabled state of show mouse position.
	 */
	public static final BooleanPreferenceWrapper appearanceMousePosShow = new BooleanPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_MOUSEPOS_SHOW);


	/**
	 * The enabled state of the overview display
	 */
	public static final BooleanPreferenceWrapper appearanceOverviewShow = new BooleanPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_OVERVIEW_SHOW);
	
	/**
	 * The router for connections.
	 */
	public static final StringPreferenceWrapper appearanceConRouter = new StringPreferenceWrapper(
			PreferenceConstants.C_APPEARANCE_CON_ROUTER);

	// -------------------------------------------------------------
	// Palette Preferences
	// -------------------------------------------------------------

	/**
	 * Palette dock location.
	 */
	public static IntPreferenceWrapper paletteDockLocation = new IntPreferenceWrapper(
			PreferenceConstants.C_PALETTE_DOCK_LOCATION);

	/**
	 * Palette state.
	 */
	public static IntPreferenceWrapper paletteState = new IntPreferenceWrapper(
			PreferenceConstants.C_PALETTE_STATE);

	/**
	 * Palette width.
	 */
	public static IntPreferenceWrapper paletteWidth = new IntPreferenceWrapper(
			PreferenceConstants.C_PALETTE_WIDTH);

	// -------------------------------------------------------------
	// Outline Preferences
	// -------------------------------------------------------------

	/**
	 * Outline sort elements.
	 */
	public static BooleanPreferenceWrapper outlineSortByElement = new BooleanPreferenceWrapper(
			PreferenceConstants.C_OUTLINE_SORT_ELEMENTS);

	/**
	 * Outline hide ActEdges.
	 */
	public static BooleanPreferenceWrapper outlineHideActEdges = new BooleanPreferenceWrapper(
			PreferenceConstants.C_OUTLINE_HIDE_ACTEDGES);

	/**
	 * Show outline overview/treeview/both area.
	 */
	public static StringPreferenceWrapper outlineShowArea = new StringPreferenceWrapper(
			PreferenceConstants.C_OUTLINE_SHOW_AREA);
	
	/**
	 * Size of the outline overview area.
	 */
	public static IntPreferenceWrapper outlineOverviewSize = new IntPreferenceWrapper(
			PreferenceConstants.C_OUTLINE_OVERVIEW_SIZE);
	
	/**
	 * Size of the outline treeview area.
	 */
	public static IntPreferenceWrapper outlineTreeviewSize = new IntPreferenceWrapper(
			PreferenceConstants.C_OUTLINE_TREEVIEW_SIZE);

}