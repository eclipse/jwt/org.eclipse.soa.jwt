/**
 * File:    IActivityEditor.java
 * Created: 19.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.pages.activityEditor.internal;

import org.eclipse.gef.EditDomain;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.ui.IWorkbenchPart;


/**
 * This is an interface for an editor that shows activity models.
 * 
 * @version $Id: IActivityEditor.java,v 1.3 2009-11-26 12:41:45 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public interface IActivityEditor
		extends IWorkbenchPart
{

	/**
	 * Sets the activity model that is shown in the editor.
	 * 
	 * @param activity
	 *            The model.
	 */
	public void loadActivityModel(Activity activity);


	/**
	 * Returns the activity model that is shown in the editor.
	 * 
	 * @return The model.
	 */
	public Activity getActivityModel();


	/**
	 * Returns the edit domain of the editor.
	 * 
	 * @return The edit domain.
	 */
	public EditDomain getEditDomain();


	/**
	 * Updates the displayed model and editor (called e.g. after view has changed).
	 */
	public void refreshEditorSheet();

}
