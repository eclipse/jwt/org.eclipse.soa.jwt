/**
 * File:    WEPreferencesAppearanceOther.java
 * Created: 12.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.preferences.pages;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.preferences.PreferenceConstants;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.parts.processes.ScopeEditPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;


/**
 * This class represents a preference page that is contributed to the Preferences dialog.
 * By subclassing <samp>FieldEditorPreferencePage</samp>, we can use the field support
 * built into JFace that allows us to create a page that is small and knows how to save,
 * restore and apply itself.
 * 
 * This page is used to modify preferences only. They are stored in the preference store
 * that belongs to the main plug-in class. That way, preferences can be accessed directly
 * via the preference store.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class WEPreferencesAppearanceOther
		extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage
{

	/**
	 * A logger.
	 */
	Logger logger = Logger.getLogger(WEPreferencesAppearanceOther.class);


	/**
	 * Constructor
	 */
	public WEPreferencesAppearanceOther()
	{
		super(GRID);

		setPreferenceStore(Plugin.getDefault().getPreferenceStore());
		setDescription("JWT Workflow Editor - " //$NON-NLS-1$
				+ PluginProperties.preferences_appother_label);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench)
	{
	}


	/**
	 * Adds a label to the editor page.
	 */
	private void addLabel(String label)
	{
		Label textLabel = new Label(getFieldEditorParent(), SWT.LEFT);
		textLabel.setText(label);

		Font font = textLabel.getFont();
		FontData fontData = (FontData) font.getFontData()[0];
		fontData.setStyle(fontData.getStyle() | SWT.BOLD);

		textLabel.setFont(new Font(font.getDevice(), fontData));

		new Label(getFieldEditorParent(), SWT.LEFT);
	}


	/**
	 * Adds an empty line to the editor page.
	 */
	private void addSpacing()
	{
		new Label(getFieldEditorParent(), SWT.LEFT);
		new Label(getFieldEditorParent(), SWT.LEFT);
	}


	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI blocks
	 * needed to manipulate various types of preferences. Each field editor knows how to
	 * save and restore itself.
	 */
	@Override
	public void createFieldEditors()
	{
		try
		{
			// -effects-
			addSpacing();
			addLabel(PluginProperties.preferences_appother_eff_label);

			// show element shadow
			BooleanFieldEditor appearance_shadow_visible_field = new BooleanFieldEditor(
					PreferenceConstants.C_APPEARANCE_SHADOW_VISIBLE,
					PluginProperties.preferences_appshadowvisible_label,
					getFieldEditorParent());

			// show element feedback on drop
			BooleanFieldEditor appearance_feedback_show_field = new BooleanFieldEditor(
					PreferenceConstants.C_APPEARANCE_FEEDBACK_SHOW,
					PluginProperties.preferences_appfeedbackshow_label,
					getFieldEditorParent());

			// show element feedback on drop
			BooleanFieldEditor appearance_antialiasing_field = new BooleanFieldEditor(
					PreferenceConstants.C_APPEARANCE_ANTIALIASING,
					PluginProperties.preferences_appantialiasing_label,
					getFieldEditorParent());
	
			// -other preferences-
			addSpacing();
			addLabel(PluginProperties.preferences_appother_oth_label);

			// show mouse position
			BooleanFieldEditor appearance_mousepos_show_field = new BooleanFieldEditor(
					PreferenceConstants.C_APPEARANCE_MOUSEPOS_SHOW,
					PluginProperties.preferences_appmouseposshow_label,
					getFieldEditorParent());

			// show mouse position
			BooleanFieldEditor appearance_overview_show_field = new BooleanFieldEditor(
					PreferenceConstants.C_APPEARANCE_OVERVIEW_SHOW,
					PluginProperties.preferences_appoverview_show_label,
					getFieldEditorParent());

			// connection router
			String[][] conRouterEntries =
			{
					{ PluginProperties.preferences_appconrouter_sp_label,
							ScopeEditPart.ROUTER_SHORTESTPATH },
					{ PluginProperties.preferences_appconrouter_m_label,
							ScopeEditPart.ROUTER_MANHATTAN },
					{ PluginProperties.preferences_appconrouter_f_label,
							ScopeEditPart.ROUTER_FAN },
					{ PluginProperties.preferences_appconrouter_n_label,
							ScopeEditPart.ROUTER_NULL } };
			ComboFieldEditor appearance_con_router_field = new ComboFieldEditor(
					PreferenceConstants.C_APPEARANCE_CON_ROUTER,
					PluginProperties.preferences_appconrouter_label,
					conRouterEntries, getFieldEditorParent());

			// add the fields
			addField(appearance_shadow_visible_field);
			addField(appearance_feedback_show_field);
			addField(appearance_antialiasing_field);
			addField(appearance_mousepos_show_field);
			addField(appearance_overview_show_field);
			addField(appearance_con_router_field);
		}
		catch (Exception ex)
		{
			logger.debug(ex);
		}
	}

}