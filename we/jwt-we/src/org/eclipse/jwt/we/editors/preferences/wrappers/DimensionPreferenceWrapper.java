/**
 * File:    DimensionPreferenceWrapper.java
 * Created: 27.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.preferences.wrappers;

import org.eclipse.draw2d.geometry.Dimension;


/**
 * This is a preference wrapper for a dimension value. It provides getValue, setValue and
 * getDefaultValue methods. The connection to a preference store value is made through its
 * corresponding preference key. To access the preference store, the class
 * WEPreferenceStoreInterface is used.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class DimensionPreferenceWrapper
{

	/**
	 * The key of the preference.
	 */
	private String preferenceKey;


	/**
	 * @param preferenceStore
	 *            The preferenceStore where the value is stored.
	 * @param preferenceKey
	 *            The key of the preference.
	 * @param defaultValue
	 *            The default value.
	 */
	public DimensionPreferenceWrapper(String preferenceKey)
	{
		this.preferenceKey = preferenceKey;
	}


	/**
	 * @return The value of the preference.
	 */
	public Dimension get()
	{
		String[] dimensionString;
		Dimension dimension;

		dimensionString = WEPreferenceStoreInterface.getValueAsString(preferenceKey)
				.split(","); //$NON-NLS-1$
		dimension = new Dimension(new Integer(dimensionString[0]), new Integer(
				dimensionString[1]));

		return dimension;
	}


	/**
	 * @param value
	 *            The new value of the preference. If <code>null</code>, values are set
	 *            to the default.
	 */
	public void set(Dimension value)
	{
		String dimensionString;

		dimensionString = value.width + "," + value.height; //$NON-NLS-1$
		WEPreferenceStoreInterface.setValueString(preferenceKey, dimensionString);
	}


	/**
	 * @return The default value of the preference.
	 */
	public Dimension getDefault()
	{
		String[] dimensionString;
		Dimension dimension;

		dimensionString = WEPreferenceStoreInterface.getDefaultValueAsString(
				preferenceKey).split(","); //$NON-NLS-1$
		dimension = new Dimension(new Integer(dimensionString[0]), new Integer(
				dimensionString[1]));

		return dimension;
	}

}