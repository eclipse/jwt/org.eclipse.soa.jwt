/**
 * File:    ExportProcessHandler.java
 * Created: 20.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.handlers;

import java.io.File;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.jwt.we.misc.wizards.template.TemplateWizard;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;


/**
 * Handler for export process command.
 * 
 * @version $Id: TemplateHandler.java,v 1.4 2009-11-26 12:41:14 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class TemplateHandler
		extends WEActionHandler
{

	/**
	 * Constructor.
	 */
	public TemplateHandler()
	{
		// requires open WEEditor
		super(true);
	}


	/**
	 * Opens a file dialog for the template export.
	 * 
	 * @param shell
	 *            A shell which will be the parent of the new instance.
	 * @param extension
	 *            The file extension
	 * @param extensionName
	 *            The file extension name
	 * @param style
	 *            The style of dialog to construct.
	 * @return The path the user selected.
	 */
	public static String saveFilePathDialog(Shell shell, String extension,
			String extensionName, int style)
	{
		FileDialog fileDialog = new FileDialog(shell, style);
		extension = "*." + extension; //$NON-NLS-1$

		fileDialog.setFilterExtensions(new String[]
		{ extension });
		fileDialog.setFilterNames(new String[]
		{ extensionName });

		fileDialog.open();
		if (fileDialog.getFileName() != null && fileDialog.getFileName().length() > 0)
		{
			return fileDialog.getFilterPath() + File.separator + fileDialog.getFileName();
		}
		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		StructuredSelection selection = new StructuredSelection(HandlerUtil
				.getCurrentSelection(event));

		// open export wizard
		TemplateWizard wizard = new TemplateWizard();
		wizard.init(PlatformUI.getWorkbench(), selection);
		WizardDialog wizardDialog = new WizardDialog(HandlerUtil.getActiveShell(event),
				wizard);
		wizardDialog.open();

		return null;
	}

}
