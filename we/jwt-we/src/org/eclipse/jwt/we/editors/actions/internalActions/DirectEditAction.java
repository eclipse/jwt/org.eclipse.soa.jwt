/**
 * File:    DirectEditAction.java
 * Created: 09.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Marc Blachon, Bull SAS, Grenoble, France
 *      - getting ImageFactory from factoryRegistry
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.internalActions;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.pages.activityEditor.internal.IActivityEditor;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.misc.views.Views;
import org.eclipse.ui.IWorkbenchPart;


/**
 * This is a direct edit action for edit parts which builds upon the GEF direct edit
 * action.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class DirectEditAction
		extends org.eclipse.gef.ui.actions.DirectEditAction
		implements ISelectionChangedListener
{

	/**
	 * A selection provider which returns edit parts.
	 */
	private ISelectionProvider selectionProvider;


	/**
	 * Constructs a DirectEditAction using the specified part.
	 * 
	 * @param part
	 *            The workbench part.
	 */
	public DirectEditAction(IWorkbenchPart part, ISelectionProvider selectionProvider)
	{
		super(part);

		// add selection listener
		this.selectionProvider = selectionProvider;
		this.selectionProvider.addSelectionChangedListener(this);

		// set text, image and description
		setText(PluginProperties.menu_Rename_item);
		setDescription(PluginProperties.menu_Rename_description);


		setImageDescriptor(Plugin.getInstance().getFactoryRegistry()
		    	.getImageFactory(Views.getInstance().getSelectedView()).createImageDescriptor("menu_directedit.png")); //$NON-NLS-1$
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
	 */
	public void selectionChanged(SelectionChangedEvent event)
	{
		// if an activityeditor is displayed, set the selection to the editparts of the
		// selected elements (the selectionsynchronizer returns the model elements - bah)
		if (GeneralHelper.getActiveInstance() != null
				&& GeneralHelper.getActiveInstance().getActiveEditor() instanceof IActivityEditor)
		{
			setSelection(GeneralHelper.getActiveInstance().getCurrentActivitySheet()
					.getGraphicalViewer().getSelection());
		}
		else
		{
			setSelection(StructuredSelection.EMPTY);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
	 */
	@Override
	public void update()
	{
		// this method for updating the enabled state would only work for the
		// activityeditor but not for the overview sheet
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.SelectionAction#dispose()
	 */
	@Override
	public void dispose()
	{
		super.dispose();

		// remove selection listener
		this.selectionProvider.removeSelectionChangedListener(this);
		this.selectionProvider = null;
	}

}
