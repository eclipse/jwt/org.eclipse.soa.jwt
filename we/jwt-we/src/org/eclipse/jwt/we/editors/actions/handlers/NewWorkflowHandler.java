/**
 * File:    NewWorkflowHandler.java
 * Created: 20.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.handlers;

import java.io.File;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.misc.wizards.creation.CreationWizard1;
import org.eclipse.jwt.we.misc.wizards.creation.CreationWizard2;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * Handler for new workflow command. This executes either the new workflow
 * wizard which adds the new file to a workspace project (in plugin mode) oder
 * the new workflow wizard which doesn't add the new file to a project (in RCP
 * mode).
 * 
 * @version $Id: NewWorkflowHandler.java,v 1.14 2009-12-17 09:50:50 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class NewWorkflowHandler extends WEActionHandler
{

	/**
	 * Constructor.
	 */
	public NewWorkflowHandler()
	{
		// does not require open WEEditor
		super(false);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.
	 * commands. ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		StructuredSelection selection = new TreeSelection();
		if (HandlerUtil.getCurrentSelection(event) != null)
		{
			selection = new StructuredSelection(HandlerUtil.getCurrentSelection(event));
		}

		Wizard wizard;
		WizardDialog wizardDialog;

		// in plugin mode, open the wizard that allows to choose
		// a project in which to add the created file, in RCP mode
		// open the wizard without project selection.
		if (GeneralHelper.isPluginMode())
		{
			wizard = new CreationWizard2();
			((CreationWizard2) wizard).init(PlatformUI.getWorkbench(), selection);
			wizardDialog = new WizardDialog(HandlerUtil.getActiveShell(event), wizard);
		}
		else
		{
			wizard = new CreationWizard1();
			((CreationWizard1) wizard).init(PlatformUI.getWorkbench(), selection);
			wizardDialog = new WizardDialog(HandlerUtil.getActiveShell(event), wizard);
		}

		// open wizard dialog in blocking mode
		wizardDialog.setBlockOnOpen(true);
		int returnCode = wizardDialog.open();
		if (returnCode == 1)
		{
			// cancelled
			return null;
		}

		// get the URI of the new file
		URI newFileURI;
		boolean newFileExists;
		if (wizard instanceof CreationWizard1)
		{
			newFileURI = ((CreationWizard1) wizard).getNewFileURI();
		}
		else
		{
			newFileURI = ((CreationWizard2) wizard).getNewFileURI();
		}

		// find out whether the file was created successfully
		newFileExists = (new File(newFileURI.toFileString())).exists();

		// if ok was pressed and file could be created -> open file
		if (returnCode == 0 && newFileExists)
		{
			// is now done in the wizards themselves
			// Plugin.openEditor(PlatformUI.getWorkbench(), newFileURI);
		}
		else
		{
			MessageDialog.openError(GeneralHelper.getActiveShell(),
					PluginProperties.editor_ErrorMessage_title, NLS.bind(
							PluginProperties.editor_ErrorCreatingFile_message, newFileURI
									.toFileString()));
		}

		return null;
	}
}
