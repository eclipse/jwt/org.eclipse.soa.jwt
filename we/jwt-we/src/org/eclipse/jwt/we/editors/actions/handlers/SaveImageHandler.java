/**
 * File:    WEActionHandler.java
 * Created: 25.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Seitz, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Yoann Rodiere, Open Wide, Lyon, France
 *    	- added SVG handling
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.handlers;

import java.io.File;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.misc.util.SaveImage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IWorkbenchPart;


/**
 * Handler for save process as image.
 * 
 * @version $Id: SaveImageHandler.java,v 1.14 2009-11-26 12:41:14 chsaad Exp $
 * @author Christian Seitz (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class SaveImageHandler
		extends WEActionHandler
{

	/**
	 * Constructor.
	 */
	public SaveImageHandler()
	{
		// requires open WEEditor
		super(true);
	}


	/**
	 * Refresh enabled state if selection has changed.
	 */
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection)
	{
		if (GeneralHelper.getActiveInstance() != null
				&& selection instanceof IStructuredSelection
				&& ((IStructuredSelection)selection).size() == 1
				&& ((IStructuredSelection)selection).getFirstElement() == GeneralHelper.getActiveInstance().getDisplayedActivityModel())
		{
			setEnabled(true);
		}
		else
		{
			setEnabled(false);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		GraphicalViewer viewer = GeneralHelper.getActiveInstance()
				.getCurrentActivitySheet().getGraphicalViewer();

		String[] filterExtensions =
		{ "*.png", "*.jpg", "*.bmp", "*.svg" }; //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$//$NON-NLS-4$
		FileDialog filedialog = new FileDialog(GeneralHelper.getActiveShell(), SWT.SAVE);
		filedialog.setFilterExtensions(filterExtensions);

		String filepath = ""; //$NON-NLS-1$
		boolean fileDoesExist = false;

		// ask until filepath == null or existing file should be overwritten
		do
		{
			filepath = filedialog.open();

			if (filepath != null && (new File(filepath)).exists())
			{
				fileDoesExist = !MessageDialog.openQuestion(
						GeneralHelper.getActiveShell(),
						PluginProperties.dialog_overwrite_title,
						PluginProperties.dialog_overwrite_message);
			}

		}
		while (filepath != null && fileDoesExist);

		// save image
		if (filepath != null)
		{
			if (filepath.endsWith(".jpg")) //$NON-NLS-1$
			{
				SaveImage.save(viewer, filepath, SWT.IMAGE_JPEG);
			}
			else if (filepath.endsWith(".bmp")) //$NON-NLS-1$
			{
				SaveImage.save(viewer, filepath, SWT.IMAGE_BMP);
			}
			else if (filepath.endsWith(".png"))
			{
				SaveImage.save(viewer, filepath, SWT.IMAGE_PNG);
			}
			else
				SaveImage.saveSVG(viewer, filepath);
		}

		return null;
	}

}
