/**
 * File:    WEDelegate.java
 * Created: 12.08.2007
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.internal;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;


/**
 * This is a base class for an action which implements
 * "org.eclipse.ui.IWorkbenchWindowActionDelegate"
 * 
 * As a consequence this action can be added to menus/toolbars by using an extension point
 * 
 * Additionally, it can be set to be only activated if an WEEditor is active.
 * 
 * @version $Id: WEDelegate.java
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public abstract class WEDelegate
		implements IWorkbenchWindowActionDelegate
{

	/**
	 * The workbench window
	 */
	private IWorkbenchWindow window;

	/**
	 * Indicates wheter this action should only be enabled if an WEEditor is active
	 */
	private boolean requiresOpenEditor = false;


	/**
	 * Constructor
	 */
	public WEDelegate()
	{
		super();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWindowActionDelegate#init(org.eclipse.ui.IWorkbenchWindow)
	 */
	public void init(IWorkbenchWindow window)
	{
		this.window = window;
	}


	protected IWorkbenchWindow getWindow()
	{
		return window;
	}


	public boolean isRequiresOpenEditor()
	{
		return requiresOpenEditor;
	}


	public void setRequiresOpenEditor(boolean requiresOpenEditor)
	{
		this.requiresOpenEditor = requiresOpenEditor;
	}


	/**
	 * Implements the ActionDelegate interface This gets called if action was added using
	 * an extension point
	 */
	public void selectionChanged(IAction action, ISelection selection)
	{
		if (isRequiresOpenEditor())
		{
			if (GeneralHelper.getActiveInstance() != null)
			{
				action.setEnabled(true);
			}
			else
			{
				action.setEnabled(false);
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWindowActionDelegate#dispose()
	 */
	public void dispose()
	{
		window = null;
	}

}
