/**
 * File:    EditorPropertyUpdater.java
 * Created: 26.05.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.internal;

import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.jwt.meta.model.processes.ProcessesPackage;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.pages.activityEditor.internal.IActivityEditor;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPropertyListener;

/**
 * This is a listener which: - Updates the title text and image if the editor
 * input changes - Is attached to each opened activity and its container by the
 * WEEditorSheet and updates the title text/image of any open activity tab and
 * closes it if the activity was deleted.
 * 
 * @version $Id
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class EditorPropertyUpdater extends AdapterImpl implements IPropertyListener,
		IDisposable
{

	/**
	 * The editor which title is updated.
	 */
	private WEEditor editor;


	/**
	 * @param editor
	 *            The editor which title should be updated.
	 */
	public EditorPropertyUpdater(WEEditor editor)
	{
		assert editor != null;
		this.editor = editor;

		editor.addPropertyListener(this);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.common.notify.impl.AdapterImpl#notifyChanged(org.eclipse
	 * .emf.common .notify.Notification)
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		// if activity name or icon has changed, update the corresponding tab
		if (notification.getNotifier() instanceof Activity)
		{
			int featureId = notification.getFeatureID(Activity.class);

			switch (notification.getEventType())
			{
				case Notification.SET:
				case Notification.UNSET:
					switch (featureId)
					{
						case ProcessesPackage.ACTIVITY__NAME:
						case ProcessesPackage.ACTIVITY__ICON:
							// update the currently displayed sheet
							editor.updateTitleName();
							editor.updateTitleImage();
							// look if the activity is displayed in a background
							// tab
							for (CTabItem item : editor.getTabFolder().getItems())

							{
								if (item.getData() instanceof IActivityEditor
										&& ((IActivityEditor) item.getData())
												.getActivityModel() == notification
												.getNotifier())
								{
									String newTitle = ((Activity) notification
											.getNotifier()).getName();
									if (newTitle == null || newTitle.length() == 0)
									{
										newTitle = org.eclipse.jwt.meta.PluginProperties.model_Unnamed_name;
									}
									item.setText(newTitle);
								}
							}
					}
					break;
			}
		}

		// if activity was deleted, close the page
		else if (notification.getNotifier() instanceof org.eclipse.jwt.meta.model.core.Package)
		{
			switch (notification.getEventType())
			{
				case Notification.REMOVE:
					if (notification.getOldValue() instanceof Activity)
					{
						editor.removeActivityFromPage((Activity) notification
								.getOldValue());
					}
					else if (notification.getOldValue() instanceof org.eclipse.jwt.meta.model.core.Package)
					{
						org.eclipse.jwt.meta.model.core.Package pack = (org.eclipse.jwt.meta.model.core.Package) notification
								.getOldValue();

						for (Iterator iterator2 = pack.eAllContents(); iterator2
								.hasNext();)
						{
							EObject content = (EObject) iterator2.next();

							if (content instanceof Activity)
							{
								editor.removeActivityFromPage((Activity) content);
							}
						}
					}
					break;

				case Notification.REMOVE_MANY:
					for (Iterator i = ((Iterable) notification.getOldValue()).iterator(); i
							.hasNext();)
					{
						Object obj = i.next();

						if (obj instanceof Activity)
						{
							editor.removeActivityFromPage((Activity) notification
									.getOldValue());
						}
						else if (obj instanceof org.eclipse.jwt.meta.model.core.Package)
						{
							for (Iterator iterator2 = ((org.eclipse.jwt.meta.model.core.Package) obj)
									.eAllContents(); iterator2.hasNext();)
							{
								EObject content = (EObject) iterator2.next();

								if (content instanceof Activity)
								{
									editor.removeActivityFromPage((Activity) content);
								}
							}
						}
					}
					break;
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPropertyListener#propertyChanged(java.lang.Object,
	 * int)
	 */
	public void propertyChanged(Object source, int propId)
	{
		switch (propId)
		{
			case IEditorPart.PROP_INPUT:
				editor.updateTitleName();
				editor.updateTitleImage();
				break;
		}
	}


	/**
	 * Add this class as listener to the activity. The listener is removed, if a
	 * different activity is set in the ActivityEditor oder the ActivityEditor
	 * is closed.
	 * 
	 * @param activity
	 *            The activity.
	 */
	public void addUpdateListener(Activity activity)
	{
		if (activity != null)
		{
			activity.eAdapters().add(this);

			EObject container = activity.eContainer();
			while (container != null)
			{
				container.eAdapters().add(this);
				container = container.eContainer();
			}
		}
	}


	/**
	 * Remove this class as listener from the activity. The listener is removed,
	 * if a different activity is set in the ActivityEditor oder the
	 * ActivityEditor is closed.
	 * 
	 * @param activity
	 *            The activity.
	 */
	public void removeUpdateListener(Activity activity)
	{
		if (activity != null)
		{
			activity.eAdapters().remove(this);

			EObject container = activity.eContainer();
			while (container != null)
			{
				container.eAdapters().remove(this);
				container = container.eContainer();
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.services.IDisposable#dispose()
	 */
	public void dispose()
	{
		// remove the listener from the editor
		editor.removePropertyListener(this);
	}

}
