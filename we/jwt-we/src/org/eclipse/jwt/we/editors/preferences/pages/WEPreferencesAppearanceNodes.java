/**
 * File:    WEPreferencesAppearanceNodes.java
 * Created: 12.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.preferences.pages;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.preferences.PreferenceConstants;
import org.eclipse.jwt.we.editors.preferences.fieldeditors.CustomDimensionFieldEditor;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;


/**
 * This class represents a preference page that is contributed to the Preferences dialog.
 * By subclassing <samp>FieldEditorPreferencePage</samp>, we can use the field support
 * built into JFace that allows us to create a page that is small and knows how to save,
 * restore and apply itself.
 * 
 * This page is used to modify preferences only. They are stored in the preference store
 * that belongs to the main plug-in class. That way, preferences can be accessed directly
 * via the preference store.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class WEPreferencesAppearanceNodes
		extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage
{

	/**
	 * A logger.
	 */
	Logger logger = Logger.getLogger(WEPreferencesAppearanceNodes.class);


	/**
	 * Constructor
	 */
	public WEPreferencesAppearanceNodes()
	{
		super(GRID);

		setPreferenceStore(Plugin.getDefault().getPreferenceStore());
		setDescription("JWT Workflow Editor - " //$NON-NLS-1$
				+ PluginProperties.preferences_appother_label);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench)
	{
	}


	/**
	 * Adds a label to the editor page.
	 */
	private void addLabel(String label)
	{
		Label textLabel = new Label(getFieldEditorParent(), SWT.LEFT);
		textLabel.setText(label);

		Font font = textLabel.getFont();
		FontData fontData = (FontData) font.getFontData()[0];
		fontData.setStyle(fontData.getStyle() | SWT.BOLD);

		textLabel.setFont(new Font(font.getDevice(), fontData));

		new Label(getFieldEditorParent(), SWT.LEFT);
	}


	/**
	 * Adds an empty line to the editor page.
	 */
	private void addSpacing()
	{
		new Label(getFieldEditorParent(), SWT.LEFT);
		new Label(getFieldEditorParent(), SWT.LEFT);
	}


	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI blocks
	 * needed to manipulate various types of preferences. Each field editor knows how to
	 * save and restore itself.
	 */
	@Override
	public void createFieldEditors()
	{
		try
		{
			// -activity node-
			addSpacing();
			addLabel(org.eclipse.jwt.meta.PluginProperties.model_ActivityNode_type);

			// line width
			IntegerFieldEditor appearance_line_width_field = new IntegerFieldEditor(
					PreferenceConstants.C_APPEARANCE_LINE_WIDTH,
					PluginProperties.preferences_applinewidth_label,
					getFieldEditorParent());

			// corner size
			IntegerFieldEditor appearance_corner_size_field = new IntegerFieldEditor(
					PreferenceConstants.C_APPEARANCE_CORNER_SIZE,
					PluginProperties.preferences_appcornersize_label,
					getFieldEditorParent());

			// -graphical element-
			addSpacing();
			addLabel(org.eclipse.jwt.meta.PluginProperties.model_GraphicalElement_type);

			// figure minimum size
			CustomDimensionFieldEditor appearance_figure_minimum_size_field = new CustomDimensionFieldEditor(
					PreferenceConstants.C_APPEARANCE_MINIMUM_SIZE,
					PluginProperties.preferences_appfigureminimum_label,
					getFieldEditorParent());

			// icon minimum size
			CustomDimensionFieldEditor appearance_figure_icon_size_field = new CustomDimensionFieldEditor(
					PreferenceConstants.C_APPEARANCE_FIGURE_ICON_SIZE,
					PluginProperties.preferences_appfigureicon_label,
					getFieldEditorParent());

			// -fork node/join node-
			addSpacing();
			addLabel(org.eclipse.jwt.meta.PluginProperties.model_ForkNode_type + ", " //$NON-NLS-1$
					+ org.eclipse.jwt.meta.PluginProperties.model_JoinNode_type);

			// bar figure width
			IntegerFieldEditor appearance_figure_bar_width_field = new IntegerFieldEditor(
					PreferenceConstants.C_APPEARANCE_FIGURE_BAR_WIDTH,
					PluginProperties.preferences_appfigurebarwidth_label,
					getFieldEditorParent());

			// range restrictions
			appearance_line_width_field.setValidRange(1, 999);
			appearance_figure_bar_width_field.setValidRange(1, 999);
			appearance_corner_size_field.setValidRange(1, 999);

			// add the fields
			addField(appearance_line_width_field);
			addField(appearance_figure_bar_width_field);
			addField(appearance_corner_size_field);

			addField(appearance_figure_minimum_size_field);
			addField(appearance_figure_icon_size_field);
		}
		catch (Exception ex)
		{
			logger.debug(ex);
		}
	}

}