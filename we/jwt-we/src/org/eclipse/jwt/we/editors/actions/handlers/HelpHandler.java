/**
 * File:    HelpHandler.java
 * Created: 20.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.handlers;

import java.io.File;
import java.net.URL;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.jwt.we.editors.actions.managed.recentfiles.RecentFilesListManager;
import org.eclipse.jwt.we.misc.dialogs.about.WEAboutDialog;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;


/**
 * Handler for help commands (tutorial and about).
 * 
 * @version $Id: HelpHandler.java,v 1.12 2009-11-26 12:41:13 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class HelpHandler
		extends WEActionHandler
{

	/**
	 * Constructor.
	 */
	public HelpHandler()
	{
		// does not require open WEEditor
		super(false);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		// get the name of the command
		String commandName = ""; //$NON-NLS-1$

		try
		{
			commandName = event.getCommand().getName();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		// check which command was called and execute corresponding action
		if (commandName.equals(PluginProperties.menu_Tutorial_label))
		{
			try
			{
				// read path and url
				String tutorialPath = PluginProperties.tutorial_path;
				String tutorialURL = PluginProperties.tutorial_url;
				
				File tutorial = RecentFilesListManager.getFileHandle(tutorialPath);
				URL url = tutorial.toURI().toURL();
				
				// if file cannot be found, use url
				if (!(new File(url.getFile())).exists())
				{
					url = new URL(tutorialURL);
				}

				// open file or url
				PlatformUI.getWorkbench().getBrowserSupport().createBrowser(
						IWorkbenchBrowserSupport.AS_VIEW, "", "Tutorial", "") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						.openURL(url);
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else if (commandName.equals(PluginProperties.menu_About_item))
		{
			WEAboutDialog aboutDialog = new WEAboutDialog(GeneralHelper.getActiveShell());
			aboutDialog.open();
		}

		return null;
	}
}
