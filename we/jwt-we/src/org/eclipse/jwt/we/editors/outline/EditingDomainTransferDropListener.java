/**
 * File:    EditingDomainTransferDropListener.java
 * Created: 20.02.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Extracted from ModelOutlineViewer
 *******************************************************************************/

package org.eclipse.jwt.we.editors.outline;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.dnd.EditingDomainViewerDropAdapter;
import org.eclipse.emf.edit.ui.dnd.LocalTransfer;
import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.Transfer;

/**
 * 
 * This class is the "internal" drop listener that allows TreeItems to
 * be dragged and dropped into the tree.
 * It uses a {@link LocalTransfer}
 * 
 * @author Mickael Istria, Open Wide, Lyon, France
 */
public class EditingDomainTransferDropListener extends EditingDomainViewerDropAdapter implements TransferDropTargetListener {

	public EditingDomainTransferDropListener(EditingDomain domain, Viewer viewer) {
		super(domain, viewer);
	}

	public Transfer getTransfer() {
		return LocalTransfer.getInstance();
	}

	public boolean isEnabled(DropTargetEvent event) {
		return true;
	}

}
