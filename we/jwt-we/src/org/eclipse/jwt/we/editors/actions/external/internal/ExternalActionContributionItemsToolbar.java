/**
 * File:    ExternalActionContributionItemsToolbar.java
 * Created: 07.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.external.internal;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jwt.we.editors.actions.external.WEExternalAction;
import org.eclipse.jwt.we.editors.actions.external.WEExternalActionsManager;
import org.eclipse.ui.actions.CompoundContributionItem;


/**
 * This class returns a set of the ExternalActions wrapped in ActionContributionItems.
 * This method is called when the external dropdown in the toolbar is populated.
 * 
 * @version $Id: ExternalActionContributionItemsToolbar.java,v 1.1 2008/07/07 15:45:27
 *          chsaad Exp $
 * @author hristian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class ExternalActionContributionItemsToolbar
		extends CompoundContributionItem
{

	/**
	 * Constructor.
	 */
	public ExternalActionContributionItemsToolbar()
	{
		super();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.actions.CompoundContributionItem#getContributionItems()
	 */
	@Override
	protected IContributionItem[] getContributionItems()
	{
		// get the list of available external actions
		List<WEExternalAction> externalActions = WEExternalActionsManager.getInstance()
				.getExternalActions();

		List<ContributionItem> contributionList = new ArrayList();

		// add all external actions to toolbar/menu
		for (int i = 0; i < externalActions.size(); i++)
		{
			// if the external action should be visible in toolbar
			if (externalActions.get(i).isShowInToolbar())
			{
				ActionContributionItem actionItem = new ActionContributionItem(
						externalActions.get(i));

				// force text?
				if (externalActions.get(i).isForceText())
				{
					actionItem.setMode(ActionContributionItem.MODE_FORCE_TEXT);
				}

				contributionList.add(actionItem);
			}
		}

		return contributionList.toArray(new ContributionItem[contributionList.size()]);
	}

}
