/**
 * File:    DefaultComposedAdapterFactoryDescriptor.java
 * Created: 11.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Open Wide SA, France <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *    Marc Dutoo, Open Wide, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.model;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;


/**
 * Able to create a new composedadapterfactory based on the
 * EMF registry with a reflectiveitemprovideradapterfactory.
 * 
 * @deprecated not used
 * 
 * @version $Id: DefaultComposedAdapterFactoryDescriptor.java,v 1.3 2009-11-26 12:41:16 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA, France <www.openwide.fr>
 */
public class DefaultComposedAdapterFactoryDescriptor
		implements ComposedAdapterFactory.Descriptor {

	/**
	 * Creates a new DefaultComposedAdapterFactory 
	 */
	public AdapterFactory createAdapterFactory() {
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
		return adapterFactory;
	}

}
