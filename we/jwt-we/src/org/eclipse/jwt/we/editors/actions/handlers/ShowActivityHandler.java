/**
 * File:    ShowActivityHandler.java
 * Created: 26.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.jwt.we.editors.pages.activityEditor.internal.IActivityEditor;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.ui.IWorkbenchPart;


/**
 * Shows a selected {@link Activity} in the active {@link IActivityEditor}.
 * 
 * @version $Id: ShowActivityHandler.java,v 1.11 2009-11-26 12:41:14 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class ShowActivityHandler
		extends WEActionHandler
{

	/**
	 * The selected activity.
	 */
	private Activity selectedActivity;


	/**
	 * @param editor
	 *            The editor that can display the activity.
	 * @param selectionProvider
	 *            The selection that can be an activity.
	 */
	public ShowActivityHandler()
	{
		// requires open WEEditor
		super(true);
	}


	/**
	 * Refresh enabled state if selection has changed.
	 */
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection)
	{
		if (GeneralHelper.getActiveInstance() != null
				&& GeneralHelper.getActiveInstance().getActiveEditor() instanceof IActivityEditor
				&& selection instanceof IStructuredSelection
				&& ((IStructuredSelection) selection).getFirstElement() instanceof Activity
				&& GeneralHelper.getActiveInstance().getCurrentActivitySheet()
						.getActivityModel() != ((IStructuredSelection) selection)
						.getFirstElement())
		{
			selectedActivity = (Activity) ((IStructuredSelection) selection)
					.getFirstElement();
			setEnabled(true);
		}
		else
		{
			selectedActivity = null;
			setEnabled(false);
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		// open the selected activity in the current page
		GeneralHelper.getActiveInstance().openActivityInCurrentPage(selectedActivity);

		return null;
	}


	/* (non-Javadoc)
	 * @see org.eclipse.jwt.we.editors.actions.WEActionHandler#dispose()
	 */
	@Override
	public void dispose()
	{
		super.dispose();
		
		selectedActivity = null;
	}

}
