/**
 * File:    DropListenerExtensionPoint.java
 * Created: 13.02.2009
 *
/*******************************************************************************
 * Copyright (c) 2009   Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.dnd.internal;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jwt.we.editors.dnd.IDropListenerFactory;
import org.eclipse.jwt.we.misc.logging.Logger;


public class DropListenerExtensionPoint {

	public static final String DRAG_N_DROP_EXTENSION_POINT = "org.eclipse.jwt.we.dnd"; //$NON-NLS-1$
	public static final String DROP_LISTENER_FACTORY = "DropListenerFactory"; //$NON-NLS-1$
	
	private static Logger logger = Logger.getLogger(DropListenerExtensionPoint.class);
	
	public static List<IDropListenerFactory> getDropListenersFactories() {
		List<IDropListenerFactory> res = new ArrayList<IDropListenerFactory>();
		for (IConfigurationElement extension : Platform.getExtensionRegistry().getConfigurationElementsFor(DRAG_N_DROP_EXTENSION_POINT)) {
			try {
				res.add((IDropListenerFactory)extension.createExecutableExtension(DROP_LISTENER_FACTORY));
			} catch (CoreException ex) {
				logger.severe("Could not load DropFactory for extension " + extension.getName(), ex); 
			}
		}
		return res;
	}

}
