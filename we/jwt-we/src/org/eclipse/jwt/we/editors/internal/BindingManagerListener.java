/**
 * File:    BindingManagerListener.java
 * Created: 26.11.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.we.editors.internal;

import org.eclipse.jface.bindings.BindingManagerEvent;
import org.eclipse.jface.bindings.IBindingManagerListener;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.misc.views.Views;

/**
 * This is a listener for changes in the binding manager.
 * 
 * @version $Id: BindingManagerListener.java,v 1.2 2009-11-26 12:41:32 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of
 *         Augsburg, Germany, www.ds-lab.org
 */
public class BindingManagerListener implements IBindingManagerListener
{

	/**
	 * The workflow editor.
	 */
	private WEEditor weeditor;


	/**
	 * The constructor.
	 * 
	 * @param weeditor
	 */
	public BindingManagerListener(WEEditor weeditor)
	{
		super();
		this.weeditor = weeditor;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.bindings.IBindingManagerListener#bindingManagerChanged
	 * (org.eclipse.jface.bindings.BindingManagerEvent)
	 */
	public void bindingManagerChanged(BindingManagerEvent event)
	{
		if (event.isActiveBindingsChanged())
		{
			Views.getInstance().refreshViewsState(weeditor);
		}
	}

}
