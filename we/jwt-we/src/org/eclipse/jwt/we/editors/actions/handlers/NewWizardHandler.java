/**
 * File:    NewWizardHandler.java
 * Created: 20.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.jwt.we.misc.wizards.NotResizableWizardDialog;
import org.eclipse.jwt.we.misc.wizards.model.CreateApplicationWizard;
import org.eclipse.jwt.we.misc.wizards.model.CreateDataWizard;
import org.eclipse.jwt.we.misc.wizards.model.CreateProcessWizard;
import org.eclipse.jwt.we.misc.wizards.model.CreateRoleWizard;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;


/**
 * Handler for wizard commands (new process, new application, new role, new data).
 * 
 * @version $Id: NewWizardHandler.java,v 1.10 2009-11-26 12:41:13 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class NewWizardHandler
		extends WEActionHandler
{

	/**
	 * Constructor.
	 */
	public NewWizardHandler()
	{
		// requires an open WEEditor
		super(true);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		Wizard wizard = null;

		StructuredSelection selection = new StructuredSelection(HandlerUtil
				.getCurrentSelection(event));

		// get the name of the command
		String commandName = ""; //$NON-NLS-1$

		try
		{
			commandName = event.getCommand().getName();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		// check which command was called and execute corresponding action
		if (commandName.equals(PluginProperties.menu_Process_item))
		{
			wizard = new CreateProcessWizard();
			((CreateProcessWizard) wizard).init(PlatformUI.getWorkbench(), selection);
		}
		else if (commandName.equals(PluginProperties.menu_Application_item))
		{
			wizard = new CreateApplicationWizard();
			((CreateApplicationWizard) wizard).init(PlatformUI.getWorkbench(), selection);
		}
		else if (commandName.equals(PluginProperties.menu_Data_item))
		{
			wizard = new CreateDataWizard();
			((CreateDataWizard) wizard).init(PlatformUI.getWorkbench(), selection);
		}
		else if (commandName.equals(PluginProperties.menu_Role_item))
		{
			wizard = new CreateRoleWizard();
			((CreateRoleWizard) wizard).init(PlatformUI.getWorkbench(), selection);
		}

		if (wizard != null)
		{
			WizardDialog dialog = new NotResizableWizardDialog(HandlerUtil
					.getActiveShell(event), wizard);
			dialog.create();
			dialog.open();
		}

		return null;
	}
}
