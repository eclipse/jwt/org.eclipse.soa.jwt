/**
 * File:    ClassPropertySheetPageDescriptor.java
 * Created: 11.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2007-2010
 * Open Wide SA, France <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.properties.extension.internal;

import java.lang.reflect.Constructor;

import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.properties.singlePage.ModelPropertySheetPage;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;


/**
 * Able to create a new propertySheetPage, configured from its class.
 * Supports three cases : TabbedPropertySheetPage and descendants,
 * JWT ModelPropertySheetPage and descendants, and finally other
 * IPropertySheetPage impls.
 * 
 * @version $Id: ClassPropertySheetPageDescriptor.java,v 1.3 2009-11-26 12:41:38 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA, France <www.openwide.fr>
 */
public class ClassPropertySheetPageDescriptor
		implements PropertySheetPageDescriptor {

	private static Logger log = Logger.getLogger(ClassPropertySheetPageDescriptor.class);

	private Class<?> propertySheetPageClass;

	public ClassPropertySheetPageDescriptor(Class<?> propertySheetPageClass) {
		this.propertySheetPageClass = propertySheetPageClass;
	}

	
	/**
	 * Creates a new IPropertySheetPage based on the class
	 * @param weEditor
	 * @return new one
	 */
	public final IPropertySheetPage createPropertySheetPage(WEEditor weEditor) {
		IPropertySheetPage propertySheetPage = null;

		try {
			if (TabbedPropertySheetPage.class.isAssignableFrom(propertySheetPageClass)) {
				// NB. is notably the default tabbed JWT case
				Constructor<?> pageCons = propertySheetPageClass.getConstructors()[0]; // must be single cons with a single WEEditor arg
				propertySheetPage = (IPropertySheetPage) pageCons.newInstance(weEditor);
				
			} else if (ModelPropertySheetPage.class.isAssignableFrom(propertySheetPageClass)) {
				// NB. is notably the default not tab JWT case
				Constructor<?> pageCons = propertySheetPageClass.getConstructors()[0]; // must be single cons with a single adapterFactory arg
				propertySheetPage = (IPropertySheetPage) pageCons.newInstance(weEditor.getAdapterFactory());
				
			} else if (IPropertySheetPage.class.isAssignableFrom(propertySheetPageClass)) {
				// other cases
				propertySheetPage = (IPropertySheetPage) propertySheetPageClass.newInstance(); // must be a no arg cons
  				
			} else {
					log.warning("JWT Property Sheet Extension - at " //$NON-NLS-1$
							+ PropertySheetPageExtensionPoint.EXTENSION_POINT + ": " //$NON-NLS-1$
							+ propertySheetPageClass + " is not a custom property sheet page class"); //$NON-NLS-1$
			}
			
		} catch (Exception e) { // ClassNotFoundException, InvocationTargetException TODO nosuchMeth
			log.severe("Error loading custom property sheet", e); //$NON-NLS-1$
		}
		
		return propertySheetPage;
	}

}
