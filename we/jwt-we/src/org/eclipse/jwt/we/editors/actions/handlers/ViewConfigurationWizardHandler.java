/**
 * File:    ViewWizardHandler.java
 * Created: 16.12.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.jwt.we.misc.wizards.view.ViewConfWizard;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;


/**
 * Handler for view configuration process command.
 * 
 * @version $Id: ViewConfigurationWizardHandler.java,v 1.3 2009-11-26 12:41:14 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class ViewConfigurationWizardHandler
		extends WEActionHandler
{

	/**
	 * Constructor.
	 */
	public ViewConfigurationWizardHandler()
	{
		// requires open WEEditor
		super(true);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		StructuredSelection selection = new StructuredSelection(HandlerUtil
				.getCurrentSelection(event));

		// open view configuration wizard
		ViewConfWizard wizard = new ViewConfWizard();
		wizard.init(PlatformUI.getWorkbench(), selection);
		WizardDialog wizardDialog = new WizardDialog(HandlerUtil.getActiveShell(event),
				wizard);
		wizardDialog.open();

		return null;
	}

}
