/**
 * File:    ModelPropertySourceProvider.java
 * Created: 24.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.properties;

import org.eclipse.gef.EditPart;
import org.eclipse.jwt.we.model.view.Reference;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.IPropertySourceProvider;


/**
 * This class provides PropertySources for model elements. It adapts other elements like
 * EditPart to their model elements.
 * 
 * @version $Id: ModelPropertySourceProvider.java,v 1.7 2009-11-26 12:41:47 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class ModelPropertySourceProvider
		implements IPropertySourceProvider
{

	/**
	 * The provider which object are adapted to the corresponding model elements.
	 */
	private IPropertySourceProvider propertySourceProvider;


	/**
	 * @param propertySourceProvider
	 *            The provider which object are adapted to the corresponding model
	 *            elements.
	 */
	public ModelPropertySourceProvider(IPropertySourceProvider propertySourceProvider)
	{
		assert propertySourceProvider != null;
		this.propertySourceProvider = propertySourceProvider;
	}


	/**
	 * If the object is an EditPart it returns its model otherwise the object itself.
	 * 
	 * If the model is a {@link Reference} the referenced element is returned. This allows
	 * to edit the properties of the element by selecting its Reference.
	 * 
	 * @param object
	 *            The object.
	 * @return The model for the object.
	 */
	private Object getModel(Object object)
	{
		Object model = object;

		if (object != null && object instanceof EditPart)
		{
			model = ((EditPart) object).getModel();
		}

		if (model != null && model instanceof Reference)
		{
			model = ((Reference) model).getReference();
		}

		return model;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.IPropertySourceProvider#getPropertySource(java.lang.Object)
	 */
	public IPropertySource getPropertySource(Object object)
	{
		return propertySourceProvider.getPropertySource(getModel(object));
	}

}
