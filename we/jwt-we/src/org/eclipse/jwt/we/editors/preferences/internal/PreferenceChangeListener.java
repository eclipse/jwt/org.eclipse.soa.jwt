/**
 * File:    PreferenceChangeListener.java
 * Created: 13.10.2007
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.preferences.internal;

import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.preferences.PreferenceConstants;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.jwt.we.misc.views.LayoutDataManager;
import org.eclipse.jwt.we.misc.views.Views;

/**
 * This class listenes to changes of the preference store.
 * 
 * Note: Depending on the preference type that was changed it may be possible
 * that some or all editors have to be updated to reflect the new settings.
 * 
 * @version $Id: PreferenceChangeListener.java,v 1.2 2009/11/04 17:18:48 chsaad
 *          Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class PreferenceChangeListener implements IPropertyChangeListener
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Preferences.IPropertyChangeListener#propertyChange
	 * (org .eclipse.core.runtime.Preferences.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent event)
	{
		// if grid preferences changed: update grid of active page
		if (event.getProperty().contains(PreferenceConstants.P_GRID))
		{
			GeneralHelper.getActiveInstance().getCurrentActivitySheet()
					.setGridProperties();
		}

		// if guard preferences changed: refresh all editors
		if (event.getProperty().contains(PreferenceConstants.P_GUARD))
		{
			for (WEEditor ed : GeneralHelper.getEditors())
			{
				// update all open pages of this model
				ed.refreshPages();
			}
		}

		// if view preferences changed: refresh all editors, palette and
		// properties
		if (event.getProperty().contains(PreferenceConstants.P_VIEW)
				&& !(event.getProperty()
						.contains(PreferenceConstants.C_VIEW_LAYOUTDATA_HANDLING)))
		{
			for (WEEditor ed : GeneralHelper.getEditors())
			{
				// update the palette
				ed.refreshPalette();
				// update all open pages of this model to new view
				ed.refreshPages();
				// refresh property sheet
				ed.refreshProperties();
				// refresh outline view
				ed.refreshOutline();

				// refresh views display state
				Views.getInstance().refreshViewsState(ed);

				if (ed == GeneralHelper.getActiveInstance())
				{
					// check if the layoutdata is present for the current view
					LayoutDataManager.uninitializedLayoutDataCheck(ed);
				}
			}
		}

		// if appearance preferences changed: refresh all editors
		if (event.getProperty().contains(PreferenceConstants.P_APPEARANCE))
		{
			for (WEEditor ed : GeneralHelper.getEditors())
			{
				// update all open pages of this model
				ed.refreshPages();
				// refresh property sheet
				ed.refreshProperties();
				// refresh outline view
				ed.refreshOutline();
			}
		}

		// if outline preferences changed: refresh all editors
		// (except if the size of the areas has changed)
		if (event.getProperty().contains(PreferenceConstants.P_OUTLINE)
				&& !(event.getProperty().contains(
						PreferenceConstants.C_OUTLINE_OVERVIEW_SIZE) || event
						.getProperty().contains(
								PreferenceConstants.C_OUTLINE_TREEVIEW_SIZE)))
		{
			for (WEEditor ed : GeneralHelper.getEditors())
			{
				// refresh outline view
				ed.refreshOutline();
			}
		}

	}

}