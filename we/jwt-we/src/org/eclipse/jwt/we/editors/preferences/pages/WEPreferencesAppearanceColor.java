/**
 * File:    WEPreferencesColor.java
 * Created: 12.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.preferences.pages;

import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.preferences.PreferenceConstants;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;


/**
 * This class represents a preference page that is contributed to the Preferences dialog.
 * By subclassing <samp>FieldEditorPreferencePage</samp>, we can use the field support
 * built into JFace that allows us to create a page that is small and knows how to save,
 * restore and apply itself.
 * 
 * This page is used to modify preferences only. They are stored in the preference store
 * that belongs to the main plug-in class. That way, preferences can be accessed directly
 * via the preference store.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class WEPreferencesAppearanceColor
		extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage
{

	/**
	 * A logger.
	 */
	Logger logger = Logger.getLogger(WEPreferencesAppearanceColor.class);


	/**
	 * Constructor
	 */
	public WEPreferencesAppearanceColor()
	{
		super(GRID);

		setPreferenceStore(Plugin.getDefault().getPreferenceStore());
		setDescription("JWT Workflow Editor - " //$NON-NLS-1$
				+ PluginProperties.preferences_color_label);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench)
	{
	}


	/**
	 * Adds a label to the editor page.
	 */
	private void addLabel(String label)
	{
		Label textLabel = new Label(getFieldEditorParent(), SWT.LEFT);
		textLabel.setText(label);

		Font font = textLabel.getFont();
		FontData fontData = (FontData) font.getFontData()[0];
		fontData.setStyle(fontData.getStyle() | SWT.BOLD);

		textLabel.setFont(new Font(font.getDevice(), fontData));

		new Label(getFieldEditorParent(), SWT.LEFT);
	}


	/**
	 * Adds an empty line to the editor page.
	 */
	private void addSpacing()
	{
		new Label(getFieldEditorParent(), SWT.LEFT);
		new Label(getFieldEditorParent(), SWT.LEFT);
	}


	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI blocks
	 * needed to manipulate various types of preferences. Each field editor knows how to
	 * save and restore itself.
	 */
	@Override
	public void createFieldEditors()
	{
		try
		{
			addSpacing();
			addLabel(PluginProperties.preferences_appcolor_ed_label);

			// editor background color
			ColorFieldEditor app_editor_color_field = new ColorFieldEditor(
					PreferenceConstants.C_APPEARANCE_COLOR_EDITOR,
					PluginProperties.preferences_appeditorcolor_label,
					getFieldEditorParent());

			addSpacing();
			addLabel(PluginProperties.preferences_appcolor_elem_label);

			// standard text color
			ColorFieldEditor app_default_text_color_field = new ColorFieldEditor(
					PreferenceConstants.C_APPEARANCE_COLOR_TEXT,
					PluginProperties.preferences_apptextcolor_label,
					getFieldEditorParent());

			// standard border color
			ColorFieldEditor app_default_border_color_field = new ColorFieldEditor(
					PreferenceConstants.C_APPEARANCE_COLOR_BORDER,
					PluginProperties.preferences_appbordercolor_label,
					getFieldEditorParent());

			// standard fill color
			ColorFieldEditor app_default_fill_color_field = new ColorFieldEditor(
					PreferenceConstants.C_APPEARANCE_COLOR_FILL,
					PluginProperties.preferences_appfillcolor_label,
					getFieldEditorParent());

			// shadow color for elements
			ColorFieldEditor app_element_shadow_color_field = new ColorFieldEditor(
					PreferenceConstants.C_APPEARANCE_COLOR_SHADOW,
					PluginProperties.preferences_appshadowcolor_label,
					getFieldEditorParent());

			addSpacing();
			addLabel(org.eclipse.jwt.meta.PluginProperties.model_Guard_type);

			// guard textual font color
			ColorFieldEditor app_guard_text_color_field = new ColorFieldEditor(
					PreferenceConstants.C_APPEARANCE_COLOR_GUARD_T,
					PluginProperties.preferences_appguardtcolor_label,
					getFieldEditorParent());

			// guard short font color
			ColorFieldEditor app_guard_short_color_field = new ColorFieldEditor(
					PreferenceConstants.C_APPEARANCE_COLOR_GUARD_S,
					PluginProperties.preferences_appguardscolor_label,
					getFieldEditorParent());

			// add the fields
			addField(app_editor_color_field);
			addField(app_default_text_color_field);
			addField(app_default_border_color_field);
			addField(app_default_fill_color_field);
			addField(app_element_shadow_color_field);

			addField(app_guard_text_color_field);
			addField(app_guard_short_color_field);
		}
		catch (Exception ex)
		{
			logger.debug(ex);
		}
	}

}