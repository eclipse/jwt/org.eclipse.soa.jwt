/**
 * File:    EditPartViewerSelectionConverter.java
 * Created: 25.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.selection;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;

/**
 * Converts selections for an {@link EditPartViewer}.
 * 
 * The selection of the {@link EditPartViewer}, which is a selection of
 * {@link EditPart}s, is converted to a selection of corresponding model
 * elements. A selection of model elements is converted to a selection of
 * {@link EditPart}s.
 * 
 * @version $Id: EditPartViewerSelectionConverter.java,v 1.5 2009/11/26 12:41:36
 *          chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 */
public class EditPartViewerSelectionConverter extends SelectionConverter
{

	/**
	 * The EditPartViewer that provides and needs a selection of editparts.
	 */
	private EditPartViewer editPartViewer;


	/**
	 * @param editPartViewer
	 *            The EditPartViewer that provides and needs a selection of
	 *            editparts.
	 */
	public EditPartViewerSelectionConverter(EditPartViewer editPartViewer)
	{
		assert editPartViewer != null;
		this.editPartViewer = editPartViewer;
	}


	/**
	 * Converts a selection of EditParts to a selection of model elements.
	 * 
	 * @param editPartSelection
	 *            The selection of editParts.
	 * @return The selection of model elements.
	 */
	@Override
	public ISelection convertFromTargetSelection(ISelection editPartSelection)
	{
		if (!(editPartSelection instanceof IStructuredSelection))
		{
			return StructuredSelection.EMPTY;
		}

		List editPartList = ((IStructuredSelection) editPartSelection).toList();
		ArrayList modelList = new ArrayList(editPartList.size());

		for (Object element : editPartList)
		{
			if (element != null && element instanceof EditPart)
			{
				Object model = ((EditPart) element).getModel();

				modelList.add(model);
			}
		}

		return new StructuredSelection(modelList);
	}


	/**
	 * Converts a selection of model elements to a selection of EditParts for
	 * the editPartViewer.
	 * 
	 * @param modelSelection
	 *            The selection of model elements.
	 * @return The selection of editParts.
	 */
	@Override
	public ISelection convertToTargetSelection(ISelection modelSelection)
	{
		if (!(modelSelection instanceof IStructuredSelection))
		{
			return StructuredSelection.EMPTY;
		}

		List modelList = ((IStructuredSelection) modelSelection).toList();
		ArrayList editPartList = new ArrayList(modelList.size());

		for (Object element : modelList)
		{
			Object editPart = editPartViewer.getEditPartRegistry().get(element);
			if (editPart != null)
			{
				editPartList.add(editPart);
			}
		}

		return new StructuredSelection(editPartList);
	}
}
