/**
 * File:    CutAction.java
 * Created: 02.04.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- improved version
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.internalActions;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;


/**
 * Action to cut model elements.
 * 
 * <p>
 * The action cuts the currently selected elements provided by the
 * {@link #selectionProvider}. The action updates itself on selection change.
 * </p>
 * 
 * @version $Id: CutAction.java,v 1.3 2009-11-26 12:41:20 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class CutAction
		extends org.eclipse.emf.edit.ui.action.CutAction
{

	/**
	 * Provides the selection for the elements to copy.
	 */
	private ISelectionProvider selectionProvider;


	/**
	 * Creates a new action.
	 * 
	 * @param domain
	 *            The EditingDomain to create the cut command.
	 * @param selectionProvider
	 *            Provides the selection for the elements to delete.
	 */
	public CutAction(EditingDomain domain, ISelectionProvider selectionProvider)
	{
		super(domain);

		ISharedImages sharedImages = PlatformUI.getWorkbench().getSharedImages();
		setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_CUT));
		setDisabledImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_CUT_DISABLED));
		setId(ActionFactory.CUT.getId());

		selectionProvider.addSelectionChangedListener(this);
		this.selectionProvider = selectionProvider;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.Disposable#dispose()
	 */
	public void dispose()
	{
		selectionProvider.removeSelectionChangedListener(this);
	}
}
