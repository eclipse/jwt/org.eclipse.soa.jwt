/**
 * File:    ZoomOutHandler.java
 * Created: 23.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.jwt.we.editors.actions.managed.zoom.ZoomControl;


/**
 * A handler for zoom out actions.
 * 
 * @version $Id: ZoomOutHandler.java,v 1.6 2009-11-26 12:41:13 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class ZoomOutHandler
		extends WEActionHandler
{

	/**
	 * Constructor.
	 */
	public ZoomOutHandler()
	{
		// enabled state controlled by ZoomControl
		super(false);

		ZoomControl.getInstance().registerZoomOutHandler(this);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		ZoomControl.getInstance().zoomOut();

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.editors.actions.WEActionHandler#dispose()
	 */
	@Override
	public void dispose()
	{
		super.dispose();

		ZoomControl.getInstance().unRegisterZoomOutHandler(this);
	}

}
