/**
 * File:    HelpHandler.java
 * Created: 20.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.jwt.we.editors.actions.managed.process.ExecuteProcessThread;
import org.eclipse.jwt.we.editors.pages.activityEditor.internal.IActivityEditor;
import org.eclipse.jwt.we.editors.preferences.PreferenceReader;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;


/**
 * Handler for start process command.
 * 
 * @version $Id: StartProcessHandler.java,v 1.15 2009-11-26 12:41:14 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class StartProcessHandler
		extends WEActionHandler
{

	/**
	 * Constructor.
	 */
	public StartProcessHandler()
	{
		// requires open WEEditor
		super(true);
	}


	/**
	 * Refresh enabled state if selection has changed. Only enabled when an activity
	 * editor is active.
	 */
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection)
	{
		// set to false if no JWT instance is active
		if (GeneralHelper.getActiveInstance() == null)
		{
			setEnabled(false);
			return;
		}

		// set enabled to true only if an IActivityEditor is enabled
		if (GeneralHelper.getActiveInstance().getActiveEditor() instanceof IActivityEditor)
		{
			setEnabled(true);
		}
		else
		{
			setEnabled(false);
		}
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		try
		{
			WEEditor ape = GeneralHelper.getActiveInstance();
			if (ape == null)
			{
				return null;
			}
			String path = ape.getNameofProcess();
			if (path == null)
			{
				return null;
			}
			String pSimPath = PreferenceReader.simulatorPath.get();

			if (pSimPath == null || pSimPath.equals("")) //$NON-NLS-1$
			{
				MessageDialog.openInformation(HandlerUtil.getActiveShell(event), Plugin
						.getDefault().getString("dialog_no_simulator_path_title"), //$NON-NLS-1$
						Plugin.getDefault().getString(
								"dialog_no_simulator_path_message")); //$NON-NLS-1$

			}
			else
			{
				new ExecuteProcessThread(pSimPath, "\"" + path + "\""); //$NON-NLS-1$ //$NON-NLS-2$
			}

		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return null;
	}

}
