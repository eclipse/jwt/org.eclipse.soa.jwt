/**
 * File:    RecentFilesMenuListener.java
 * Created: 10.10.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.managed.recentfiles;

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jwt.we.misc.logging.Logger;


/**
 * Simple class for catching menu event. Invokes the
 * {@link RecentFilesListManager#loadPreferences()}
 * 
 * @version $Id: RecentFilesMenuListener.java,v 1.6 2009-11-26 12:41:48 chsaad Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class RecentFilesMenuListener
		implements IMenuListener
{

	Logger logger = Logger.getLogger(RecentFilesMenuListener.class);


	public void menuAboutToShow(IMenuManager manager)
	{
		RecentFilesListManager.getInstance().fillMenuInitial();
	}

}
