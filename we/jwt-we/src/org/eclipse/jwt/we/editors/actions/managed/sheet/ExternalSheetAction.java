/**
 * File:    ExternalSheetAction.java
 * Created: 23.09.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.managed.sheet;

import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.actions.internal.WEAction;
import org.eclipse.jwt.we.editors.pages.externalSheet.internal.WEExternalSheetManager;
import org.eclipse.jwt.we.misc.util.GeneralHelper;


/**
 * An action that opens an external editor sheet.
 * 
 * @version $Id: ExternalSheetAction.java,v 1.6 2009-11-26 12:41:52 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class ExternalSheetAction
		extends WEAction
{

	/**
	 * Constructor.
	 * 
	 * @param sheetTitle
	 *            The title of the sheet.
	 */
	public ExternalSheetAction(String sheetTitle)
	{
		super();

		setRequiresOpenEditor(true);

		// set id and text
		super.setId(sheetTitle);
		super.setText(sheetTitle);

		// set description
		String desc = PluginProperties.menu_External_description;
		setDescription(desc);
		setToolTipText(desc);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run()
	{
		// open the external sheet in the active editor
		WEExternalSheetManager.getInstance().displayExternalSheet(
				GeneralHelper.getActiveInstance(), getText());
	}

}
