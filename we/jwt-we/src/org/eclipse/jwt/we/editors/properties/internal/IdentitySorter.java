/**
 * File:    IdentitySorter.java
 * Created: 24.03.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.properties.internal;

import org.eclipse.ui.views.properties.IPropertySheetEntry;
import org.eclipse.ui.views.properties.PropertySheetSorter;


/**
 * This sorter does not sort the entries, but leaves them in the order in which they are
 * given.
 * 
 * <p>
 * The default sorter of the properties view sort the entries alphabetically on their
 * names. This is not very good, especially because due to localization the ordering
 * changes.
 * </p>
 * 
 * @version $Id: IdentitySorter.java,v 1.3 2009-11-26 12:41:47 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class IdentitySorter
		extends PropertySheetSorter
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.PropertySheetSorter#compare(org.eclipse.ui.views.properties.IPropertySheetEntry,
	 *      org.eclipse.ui.views.properties.IPropertySheetEntry)
	 */
	@Override
	public int compare(IPropertySheetEntry entryA, IPropertySheetEntry entryB)
	{
		return 0;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.properties.PropertySheetSorter#compareCategories(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public int compareCategories(String categoryA, String categoryB)
	{
		return 0;
	}

}
