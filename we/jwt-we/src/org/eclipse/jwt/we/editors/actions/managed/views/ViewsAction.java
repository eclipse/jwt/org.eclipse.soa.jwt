/**
 * File:    ViewsAction.java
 * Created: 23.11.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- added support for external views
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.managed.views;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.actions.internal.WEAction;
import org.eclipse.jwt.we.misc.views.Views;


/**
 * Standard Action for a views item. It changes the current view when executed.
 * 
 * @version $Id: ViewsAction.java,v 1.17 2009-11-26 12:41:34 chsaad Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class ViewsAction
		extends WEAction
{

	/**
	 * The internal name of the view.
	 */
	private String nameofview = ""; //$NON-NLS-1$


	/**
	 * Constructor.
	 * 
	 * @param internalViewName
	 *            The internal name of the view.
	 */
	public ViewsAction(String internalViewName, String officialViewName, ImageDescriptor imageDesc)
	{
		super();

		setRequiresOpenEditor(true);
		nameofview = internalViewName;


		// set the display name of the view
		if (officialViewName == null || officialViewName == "") //$NON-NLS-1$
		{
			officialViewName = internalViewName;
		}

		super.setId(internalViewName);
		super.setText(officialViewName);

		// set icon
		try
		{
			setImageDescriptor(imageDesc);
		}
		catch (Exception e)
		{
		}

		// set description
		String desc = PluginProperties.view_Tooltip_description;
		setDescription(desc);
		setToolTipText(desc);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run()
	{
		// simply initiates the view change and saves the new value
		try
		{
			// make sure this action is checked
			setChecked(true);

			// change the view
			Views.getInstance().changeView(nameofview);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

}
