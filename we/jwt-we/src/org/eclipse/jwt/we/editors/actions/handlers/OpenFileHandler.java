/**
 * File:    OpenFileHandler.java
 * Created: 20.09.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.handlers;

import java.io.File;
import java.util.Iterator;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.edit.ui.action.LoadResourceAction;
import org.eclipse.jface.window.Window;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.actions.WEActionHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;


/**
 * Handler for help commands (open workflow and open workflow uri).
 * 
 * @version $Id: OpenFileHandler.java,v 1.12 2009-11-26 12:41:14 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class OpenFileHandler
		extends WEActionHandler
{

	/**
	 * Constructor.
	 */
	public OpenFileHandler()
	{
		// does not require open WEEditor
		super(false);
	}


	/**
	 * Opens a file dialog.
	 * 
	 * @param shell
	 *            A shell which will be the parent of the new instance.
	 * @param fileExtensionFilter
	 *            The file extensions which the dialog will use to filter the files it
	 *            shows to the argument, which may be <code>null</code>.
	 * @param style
	 *            The style of dialog to construct.
	 * @return The path the user selected.
	 */
	public static String openFilePathDialog(Shell shell, String fileExtensionFilter,
			int style)
	{
		FileDialog fileDialog = new FileDialog(shell, style);
		if (fileExtensionFilter == null)
		{
			fileExtensionFilter = "*." + WEEditor.getWorkflowExtension() + ";*.agilpro;*.conf;*.view"; //$NON-NLS-1$ //$NON-NLS-2$
		}
		fileDialog.setFilterExtensions(new String[]
		{ fileExtensionFilter });
		fileDialog.setFilterNames(new String[]
		{ PluginProperties.wizards_Files_files });

		fileDialog.open();
		if (fileDialog.getFileName() != null && fileDialog.getFileName().length() > 0)
		{
			return fileDialog.getFilterPath() + File.separator + fileDialog.getFileName();
		}
		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.
	 * ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		// get the name of the command
		String commandName = ""; //$NON-NLS-1$

		try
		{
			commandName = event.getCommand().getName();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		// check which command was called and execute corresponding action
		if (commandName.equals(PluginProperties.menu_Open_item))
		{
			String filePath = openFilePathDialog(HandlerUtil.getActiveShell(event), null,
					SWT.OPEN);
			if (filePath != null)
			{
				Plugin.openEditor(HandlerUtil.getActiveWorkbenchWindow(event)
						.getWorkbench(), URI.createFileURI(filePath));
			}
		}
		else if (commandName.equals(PluginProperties.menu_OpenUri_item))
		{
			LoadResourceAction.LoadResourceDialog loadResourceDialog = new LoadResourceAction.LoadResourceDialog(
					HandlerUtil.getActiveShell(event));
			if (Window.OK == loadResourceDialog.open())
			{
				for (Iterator i = loadResourceDialog.getURIs().iterator(); i.hasNext();)
				{
					Plugin.openEditor(HandlerUtil.getActiveWorkbenchWindow(event)
							.getWorkbench(), (URI) i.next());
				}
			}
		}

		return null;
	}

}
