/**
 * File:    WEPreferencesFont.java
 * Created: 12.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.preferences.pages;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.PluginProperties;
import org.eclipse.jwt.we.editors.preferences.PreferenceConstants;
import org.eclipse.jwt.we.editors.preferences.fieldeditors.CustomFontFieldEditor;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;


/**
 * This class represents a preference page that is contributed to the Preferences dialog.
 * By subclassing <samp>FieldEditorPreferencePage</samp>, we can use the field support
 * built into JFace that allows us to create a page that is small and knows how to save,
 * restore and apply itself.
 * 
 * This page is used to modify preferences only. They are stored in the preference store
 * that belongs to the main plug-in class. That way, preferences can be accessed directly
 * via the preference store.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public class WEPreferencesAppearanceFont
		extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage
{

	/**
	 * A logger.
	 */
	Logger logger = Logger.getLogger(WEPreferencesAppearanceFont.class);


	/**
	 * Constructor
	 */
	public WEPreferencesAppearanceFont()
	{
		super(GRID);

		setPreferenceStore(Plugin.getDefault().getPreferenceStore());
		setDescription("JWT Workflow Editor - " //$NON-NLS-1$
				+ PluginProperties.preferences_font_label);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench)
	{
	}


	/**
	 * Adds a label to the editor page.
	 */
	private void addLabel(String label)
	{
		Label textLabel = new Label(getFieldEditorParent(), SWT.LEFT);
		textLabel.setText(label);

		Font font = textLabel.getFont();
		FontData fontData = (FontData) font.getFontData()[0];
		fontData.setStyle(fontData.getStyle() | SWT.BOLD);

		textLabel.setFont(new Font(font.getDevice(), fontData));

		new Label(getFieldEditorParent(), SWT.LEFT);
		new Label(getFieldEditorParent(), SWT.LEFT);
	}


	/**
	 * Adds an empty line to the editor page.
	 */
	private void addSpacing()
	{
		new Label(getFieldEditorParent(), SWT.LEFT);
		new Label(getFieldEditorParent(), SWT.LEFT);
		new Label(getFieldEditorParent(), SWT.LEFT);
	}


	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI blocks
	 * needed to manipulate various types of preferences. Each field editor knows how to
	 * save and restore itself.
	 */
	@Override
	public void createFieldEditors()
	{
		try
		{
			addSpacing();
			addLabel(PluginProperties.preferences_appdefaultfont_label);

			// default font
			CustomFontFieldEditor app_default_font = new CustomFontFieldEditor(
					PreferenceConstants.C_APPEARANCE_DEFAULT_FONT, getFieldEditorParent());

			addSpacing();
			addLabel(PluginProperties.preferences_refelementfont_label);

			// refelement font
			CustomFontFieldEditor app_refelement_font = new CustomFontFieldEditor(
					PreferenceConstants.C_APPEARANCE_REFELEMENT_FONT,
					getFieldEditorParent());

			addSpacing();
			addLabel(PluginProperties.preferences_guardfont_label);

			// guard font
			CustomFontFieldEditor app_guard_font = new CustomFontFieldEditor(
					PreferenceConstants.C_APPEARANCE_GUARD_FONT, getFieldEditorParent());

			addField(app_default_font);
			addField(app_refelement_font);
			addField(app_guard_font);

		}
		catch (Exception ex)
		{
			logger.debug(ex);
		}
	}

}