/**
 * File:    StringPreferenceWrapper.java
 * Created: 27.10.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.preferences.wrappers;



/**
 * This is a preference wrapper for a string value. It provides getValue, setValue and
 * getDefaultValue methods. The connection to a preference store value is made through its
 * corresponding preference key. To access the preference store, the class
 * WEPreferenceStoreInterface is used.
 * 
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public class StringPreferenceWrapper
{

	/**
	 * The key of the preference.
	 */
	private String preferenceKey;


	/**
	 * @param preferenceStore
	 *            The preferenceStore where the value is stored.
	 * @param key
	 *            The key of the preference.
	 */
	public StringPreferenceWrapper(String preferenceKey)
	{
		this.preferenceKey = preferenceKey;
	}


	/**
	 * @return The value of the preference.
	 */
	public String get()
	{
		return WEPreferenceStoreInterface.getValueAsString(preferenceKey);
	}


	/**
	 * @param value
	 *            The new value of the preference.
	 */
	public void set(String value)
	{
		WEPreferenceStoreInterface.setValueString(preferenceKey, value);
	}


	/**
	 * @return The default value of the preference.
	 */
	public String getDefault()
	{
		return WEPreferenceStoreInterface.getDefaultValueAsString(preferenceKey);
	}
}
