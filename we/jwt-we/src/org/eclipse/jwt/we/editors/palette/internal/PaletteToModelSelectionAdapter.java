/**
 * File:    PaletteToModelSelectionAdapter.java
 * Created: 20.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.palette.internal;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.tools.CreationTool;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.jwt.we.editors.selection.AbstractSelectionProvider;
import org.eclipse.jwt.we.misc.factories.TransferFactory;
import org.eclipse.jwt.we.misc.logging.Logger;


/**
 * Transforms a selection of a palette entry to its corresponding model element.
 * 
 * @version $Id: PaletteToModelSelectionAdapter.java,v 1.3 2009-11-26 12:41:24 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class PaletteToModelSelectionAdapter
		extends AbstractSelectionProvider
		implements ISelectionChangedListener
{

	/**
	 * A Logger.
	 */
	private static final Logger logger = Logger
			.getLogger(PaletteToModelSelectionAdapter.class);

	/**
	 * Provides selections in a palette.
	 */
	@SuppressWarnings("unused")
	private ISelectionProvider palette;

	/**
	 * The current selection.
	 */
	private ISelection currentSelection;


	/**
	 * @param palette
	 *            Provides selections in a palette.
	 */
	public PaletteToModelSelectionAdapter(ISelectionProvider palette)
	{
		assert palette != null;
		this.palette = palette;
		palette.addSelectionChangedListener(this);
	}


	public ISelection getSelection()
	{
		return currentSelection;
	}


	public void setSelection(ISelection selection)
	{
		throw new UnsupportedOperationException();
	}


	public void selectionChanged(SelectionChangedEvent event)
	{
		logger.valueChanged("selection", event.getSelection()); //$NON-NLS-1$
		currentSelection = null;

		// search for EditPart -> CreationToolEntry -> CreationFactory -> model
		// element
		if (event.getSelection() instanceof StructuredSelection)
		{
			StructuredSelection selection = (StructuredSelection) event.getSelection();

			if (selection.size() == 1)
			{
				Object selectedObject = selection.getFirstElement();

				if (selectedObject instanceof EditPart)
				{
					selectedObject = ((EditPart) selectedObject).getModel();

					if (selectedObject != null
							&& selectedObject instanceof CreationToolEntry)
					{
						Object factory = ((CreationToolEntry) selectedObject)
								.getToolProperty(CreationTool.PROPERTY_CREATION_FACTORY);

						if (factory != null && factory instanceof TransferFactory)
						{
							Object selectedModel = ((TransferFactory) factory)
									.getTransferObject();

							if (selectedModel != null
									&& selectedModel instanceof Activity)
							{
								currentSelection = new StructuredSelection(selectedModel);
							}
						}
					}
				}
			}
		}

		if (currentSelection == null)
		{
			// no selection that can be handled by the listener
			currentSelection = StructuredSelection.EMPTY;
		}

		SelectionChangedEvent adaptedEvent = new SelectionChangedEvent(event
				.getSelectionProvider(), currentSelection);

		fireSelectionChanged(adaptedEvent);
	}
}
