/**
 * File:    RootAdapterFactoryExtensionPoint.java
 * Created: 11.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * Open Wide SA, France <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *    Marc Dutoo, Open Wide, France
 *      - Creation and Implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.model;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.jwt.we.Plugin;
import org.eclipse.jwt.we.misc.extensions.ExtensionsHelper;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.ui.WorkbenchException;


/**
 * Extension point related code to get the custom rootAdapterFactoryDescriptor,
 * or the default one if none. Uses ComposedAdapterFactory.Descriptor.
 * 
 * @deprecated not used
 * 
 * @version $Id: RootAdapterFactoryExtensionPoint.java,v 1.4 2009-11-26 12:41:16 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA, France <www.openwide.fr>
 */
public class RootAdapterFactoryExtensionPoint {
	
	/** The extension point, read from the property file */
	public static final String EXTENSION_POINT = Plugin.getId() + "editorRootAdapterFactory"; //$NON-NLS-1$
	private static final String CLASS = "class"; //$NON-NLS-1$

	private static Logger log = Logger.getLogger(RootAdapterFactoryExtensionPoint.class);

	/** Found configured rootAdapterFactoryDescriptor */
	private static ComposedAdapterFactory.Descriptor rootAdapterFactoryDescriptor = null;
	private static boolean initialized = false;


	/**
	 * Inits.
	 * Processes ext point and finds configured rootAdapterFactoryDescriptor
	 * if any, else uses the default one.
	 */
	private synchronized static void init() {
		if (!initialized) {
			try {
				processExtensionPoint();
			} catch (WorkbenchException ex) {
				log.severe("Error processing extension point for custom RootAdapterFactory"); //$NON-NLS-1$
			}
			
			if (rootAdapterFactoryDescriptor == null) {
				// TODO create default
			}
			
			initialized = true;
		}
	}


	/**
	 * Inits
	 * @return a new custom rootAdapterFactory, according to the
	 * configured rootAdapterFactoryDescriptor, or the default one if none
	 */
	public static AdapterFactory getAdapterFactory() {
		init();
		
		return rootAdapterFactoryDescriptor.createAdapterFactory();
	}


	/**
	 * Process the extension point i.e. get the configured
	 * rootAdapterFactoryDescriptor.
	 * 
	 * @throws WorkbenchException
	 */
	private static void processExtensionPoint() throws WorkbenchException
	{
		log.debug("Processing extension point " + EXTENSION_POINT); //$NON-NLS-1$

		IConfigurationElement[] confElements = ExtensionsHelper
				.findConfigurationElements(EXTENSION_POINT);

		// get custom root adapter factory if any
		if (confElements != null && confElements.length == 1) {
			IConfigurationElement confElement = confElements[0];
			Object factory;
			try {
				factory = confElement.createExecutableExtension(CLASS);
			} catch (CoreException e) {
				log.severe("JWT Extension at " + EXTENSION_POINT //$NON-NLS-1$
						+ " - error while instanciating AdapterFactoryDescriptor", e); //$NON-NLS-1$
				return;
			}
				
			if (factory instanceof ComposedAdapterFactory.Descriptor) {
				rootAdapterFactoryDescriptor = (ComposedAdapterFactory.Descriptor) factory;
				log.info("JWT Extension at " + EXTENSION_POINT //$NON-NLS-1$
						+ " - found custom AdapterFactoryDescriptor : " //$NON-NLS-1$
						+ rootAdapterFactoryDescriptor);
				
			} else {
				log.warning("JWT Extension at " + EXTENSION_POINT + " - " //$NON-NLS-1$ //$NON-NLS-2$
						+ factory + " is not a AdapterFactoryDescriptor"); //$NON-NLS-1$
			}
			
		} else {
			log.warning("JWT Extension at " + EXTENSION_POINT //$NON-NLS-1$
					+ " - found no custom AdapterFactoryDescriptor"); //$NON-NLS-1$
		}
	}

}
