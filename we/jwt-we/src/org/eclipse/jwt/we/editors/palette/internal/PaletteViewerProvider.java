/**
 * File:    PaletteViewerProvider.java
 * Created: 23.02.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.palette.internal;

import org.eclipse.gef.dnd.TemplateTransferDragSourceListener;
import org.eclipse.gef.ui.palette.PaletteViewer;
import org.eclipse.jwt.we.editors.pages.activityEditor.internal.IActivityEditor;


/**
 * PaletteViewerProvider for drag & drop.
 * 
 * @version $Id: PaletteViewerProvider.java,v 1.3 2009-11-26 12:41:24 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class PaletteViewerProvider
		extends org.eclipse.gef.ui.palette.PaletteViewerProvider
{

	/**
	 * @param activityEditor
	 *            The editor that can display the activity.
	 */
	public PaletteViewerProvider(IActivityEditor activityEditor)
	{
		super(activityEditor.getEditDomain());
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.palette.PaletteViewerProvider#configurePaletteViewer(org.eclipse.gef.ui.palette.PaletteViewer)
	 */
	@Override
	protected void configurePaletteViewer(PaletteViewer viewer)
	{

		// create a drag source listener for this palette viewer
		// together with an appropriate transfer drop target listener, this will
		// enable
		// model element creation by dragging a CombinatedTemplateCreationEntries
		// from the palette into the editor
		// @see ShapesEditor#createTransferDropTargetListener()
		viewer.addDragSourceListener(new TemplateTransferDragSourceListener(viewer));
	}

}
