/**
 * File:    CreateChildAction.java
 * Created: 23.01.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.editors.actions.internalActions;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;

/**
 * Custom CreateChildAction
 * 
 * @version $Id: CreateChildAction.java,v 1.4 2009-12-04 15:27:53 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class CreateChildAction extends org.eclipse.emf.edit.ui.action.CreateChildAction
{

	/**
	 * Save the selected element.
	 */
	private ISelection selection;


	/**
	 * The constructor.
	 * 
	 * @param workbenchPart
	 * @param selection
	 * @param descriptor
	 */
	public CreateChildAction(IWorkbenchPart workbenchPart, ISelection selection,
			Object descriptor)
	{
		super(workbenchPart, selection, descriptor);
	}


	/**
	 * The constructor.
	 * 
	 * @param editorPart
	 * @param selection
	 * @param descriptor
	 */
	public CreateChildAction(IEditorPart editorPart, ISelection selection,
			Object descriptor)
	{
		super(editorPart, selection, descriptor);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.edit.ui.action.StaticSelectionCommandAction#configureAction
	 * (org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void configureAction(ISelection selection)
	{
		// save the selected element
		this.selection = selection;

		super.configureAction(selection);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.ui.action.StaticSelectionCommandAction#run()
	 */
	@Override
	public void run()
	{
		super.run();

		// bugfix: update the selection (this action normally only works if the
		// user
		// changes the selection after its execution. Otherwise the second time,
		// it will
		// not be executed since it tries to add the same element to the model
		// as in the
		// first execution. So we just select the same element again and the
		// action will
		// automatically create a new working command)
		GeneralHelper.getActiveInstance().getSelectionSynchronizer().setSelection(
				selection);
	}

}
