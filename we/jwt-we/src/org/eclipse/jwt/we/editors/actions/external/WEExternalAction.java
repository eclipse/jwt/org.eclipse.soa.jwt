/**
 * File:    WEExternlAction.java
 * Created: 04.08.2007
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/


package org.eclipse.jwt.we.editors.actions.external;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.pages.activityEditor.WEEditorSheet;
import org.eclipse.jwt.we.misc.util.GeneralHelper;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;


/**
 * This class can be extended by other plugins to create actions which can than be
 * integrated into JWT using an extension point.
 * 
 * @version $Id: WEExternalAction.java
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 * @since 0.6.0
 */
public abstract class WEExternalAction
		extends Action
		implements ISelectionListener
{

	/**
	 * Indicates whether the action should be shown in the JWT toolbar.
	 */
	private boolean showInToolbar;

	/**
	 * Indicates whether the action should be shown in the JWT menu.
	 */
	private boolean showInMenu;

	/**
	 * Indicates whether the action should only be enabled if an JWT editor is active.
	 */
	private boolean requiresOpenEditor;

	/**
	 * Indicates whether the action text should be shown in the toolbar even if an image
	 * was supplied.
	 */
	private boolean forceText;


	/**
	 * Constructor.
	 */
	public WEExternalAction()
	{
	}


	// ---------------------- Getter & Setter ----------------------

	public boolean isRequiresOpenEditor()
	{
		return requiresOpenEditor;
	}


	public void setRequiresOpenEditor(boolean requiresOpenEditor)
	{

		this.requiresOpenEditor = requiresOpenEditor;
	}


	public boolean isShowInMenu()
	{
		return showInMenu;
	}


	public void setShowInMenu(boolean showInMenu)
	{
		this.showInMenu = showInMenu;
	}


	public boolean isShowInToolbar()
	{
		return showInToolbar;
	}


	public void setShowInToolbar(boolean showInToolbar)
	{
		this.showInToolbar = showInToolbar;
	}


	public boolean isForceText()
	{
		return forceText;
	}


	public void setForceText(boolean forceText)
	{
		this.forceText = forceText;
	}


	// ---------------------- Internal Methods ----------------------

	/**
	 * This is called by the window selection listener and disables the action if no
	 * WEEditor is active (only if the according property was set).
	 */
	public void selectionChanged(IWorkbenchPart wbp, ISelection selection)
	{
		// test only if property was set
		if (isRequiresOpenEditor())
		{
			if (GeneralHelper.getActiveInstance() != null)
			{
				setEnabled(true);
			}
			else
			{
				setEnabled(false);
			}
		}
	}


	// ---------------------- Abstract Methods ----------------------

	/**
	 * If the action should be represented by an image in the toolbar, an ImageDescriptor
	 * can be supplied. If null is returned, no image will be displayed in the toolbar.
	 * 
	 * @return The ImageDescriptor.
	 */
	public abstract ImageDescriptor getImage();


	/**
	 * This gets executed when the action is run.
	 */
	public abstract void run();


	// ---------------------- Provide Information Methods ----------------------

	/**
	 * Returns the active WEEditor.
	 */
	public WEEditor getActiveWEEditor()
	{
		return GeneralHelper.getActiveInstance();
	}


	/**
	 * Returns the active editor part.
	 */
	public WEEditorSheet getActiveActivitySheet()
	{
		if (getActiveWEEditor() != null)
		{
			return getActiveWEEditor().getCurrentActivitySheet();
		}
		return null;
	}


	/**
	 * Returns the active Resource.
	 */
	public Resource getActiveResource()
	{
		if (getActiveWEEditor() != null)
		{
			return getActiveWEEditor().getMainModelResource();
		}
		return null;
	}


	/**
	 * Returns the active Shell.
	 */
	public Shell getActiveShell()
	{
		return GeneralHelper.getActiveShell();
	}

}
