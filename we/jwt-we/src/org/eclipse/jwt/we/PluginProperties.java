/**
 * File:    PluginProperties.java
 * Created: 11.03.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Use NLS and static fields
 *    Marc Blachon, Bull SAS
 *      - adding extension point for the factoryRegistry
 *******************************************************************************/

package org.eclipse.jwt.we;

import java.util.MissingResourceException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.osgi.util.NLS;

/**
 * Provied access the <code>plugin.properties</code> file.
 * 
 * <p>
 * All keys in the <code>plugin.properties</code> are access with static
 * members. Some properties need parameters and they are represented by member
 * functions. They may also encapsulate a simple logic to find the right
 * property based on the parameters.
 * </p>
 * 
 * @version $Id: PluginProperties.java,v 1.24 2010-06-30 10:46:33 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming
 *         distributed Systems Lab, University of Augsburg, Germany,
 *         www.ds-lab.org
 * @since 0.7
 */
public class PluginProperties extends NLS
{

	// "plugin" references the plugin[_locale].properties file path
	// at the root of the jwt-we bundle.
	private static final String BUNDLE_NAME = "plugin"; //$NON-NLS-1$

	private static final String NLS_MISSING = "NLS missing message"; //$NON-NLS-1$

	static
	{
		NLS.initializeMessages(BUNDLE_NAME, PluginProperties.class);
	}


	/**
	 * Returns the value associated to a property. Prefer using directly the
	 * field for this property when possible
	 * 
	 * @param string
	 * @return
	 */
	public static String getString(String key)
	{
		try
		{
			return (String) PluginProperties.class.getField(key).get(null);
		}
		catch (Exception ex)
		{
			return "!" + key + "!"; //$NON-NLS-1$ //$NON-NLS-2$
		}
	}


	/**
	 * Returns the value from the <code>plugin.properties</code> identified by a
	 * key.
	 * 
	 * <p>
	 * It the resource is not available a {@link MissingResourceException} is
	 * thrown.
	 * </p>
	 * 
	 * @param key
	 *            The key of the property.
	 * @return The value of the key.
	 * @throws MissingResourceException
	 *             If the resource can not be found.
	 */
	public static String getStringExpectMissing(String key)
			throws MissingResourceException
	{
		try
		{
			return (String) PluginProperties.class.getField(key).get(null);
		}
		catch (Exception ex)
		{
			throw new MissingResourceException(
					"Cannot find value associated to key [" + key + "]", PluginProperties.class.getName(), key); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}


	/**
	 * Returns the value from the <code>plugin.properties</code> identified by a
	 * key and filled with a parameter.
	 * 
	 * <p>
	 * It the resource is not available a {@link MissingResourceException} is
	 * thrown.
	 * </p>
	 * 
	 * @param key
	 *            The key of the property.
	 * @param data
	 *            The parameter.
	 * @return The value of the key.
	 * @throws MissingResourceException
	 *             If the resource can not be found.
	 */
	public static String getStringExpectMissing(String key, Object[] data)
			throws MissingResourceException
	{
		return bind(getStringExpectMissing(key), data);
	}

	//
	//
	//
	//
	//

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Plugin data
	//

	public static String plugin_Name; // JWT Workflow Editor
	public static String plugin_ProviderName; // http://www.eclipse.org/jwt
	public static String pluginName; // JWT
	public static String providerName; // http://www.eclipse.org/jwt

	public static String category_id; // Java Workflow Tooling
	public static String perspective_id; // org.eclipse.jwt.we.perspective

	public static String tutorial_path; // doc/tutorial/JWTUsageTutorial.pdf
	public static String tutorial_url; // http://wiki.eclipse.org/images/3/3b/JWTUsageTutorial.pdf

	public static String plugin_workflow_extension; // workflow
	public static String plugin_diagram_extension; // 
	public static String plugin_template_extension; // workflowtemplate

	public static String about_image_file; // jwt_about.gif
	public static String about_text_file; // abouttext.txt

	public static String logging_level; // 

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Editor
	//

	// {0}: Name of file
	// {1}: Name of displayed model
	public static String editor_Title_text; // {0} ({1})

	public static String editor_Default_text; // Default

	public static String editor_ErrorMessage_title; // Error
	public static String editor_WarnNoEditor_message; // There is no editor
	// registered for
	// the file "{0}".
	public static String editor_ErrorOpenEditor_title; // Error Open Editor
	public static String editor_ErrorReferences_message; // Error Checking
	public static String editor_ErrorReferences2_message; // Error Checking
	// References
	public static String editor_ErrorSavingFile_message; // Error saving file
	// "{0}".
	public static String editor_ErrorLoadingFile_message; // Error loading file
	// "{0}".
	public static String editor_ErrorCreatingFile_message; // Error creating
	// file "{0}".
	public static String editor_ErrorCurrentlyOpen_message; // "{0}" is
	// currently open.
	public static String editor_MissingResource_message; // The resource "{0}"
	// is missing.

	public static String editor_StillReferenced_title; // Element(s) still
	// referenced
	public static String editor_StillReferenced_message; // Cannot be removed.
	// The
	// following elements are
	// still referenced: {0}
	public static String editor_StillReferencedDelete_message; // The following
	// elements
	// are still referenced:
	// {0} Should all
	// references to this
	// element be deleted?
	public static String editor_StillReferencedConfirmation_message; // All
	// references
	// to
	// these elements
	// as well as the
	// elements
	// themselves will
	// be deleted. Are
	// you sure?
	public static String editor_DeleteActivities_title;
	public static String editor_DeleteActivities_message;

	public static String editor_Closing_title; // Save Resource
	public static String editor_Closing_message; // "{0}" has been modified.
	// Save changes
	// ?

	public static String editor_QuestionOverwriteFile_title; // Overwrite file
	public static String editor_QuestionOverwriteFile_message; // "{0}" exists.
	// Overwrite?

	public static String editor_Wizard_description; // Create a new Workflow
	// Model
	public static String editor_Wizard_icon; // jwt16x16.gif

	public static String editor_WizardExportRefAdded_title; // Referenced
	// elements added
	public static String editor_WizardExportRefAdded_description; // The
	// selected
	// elements
	// have active
	// references in the
	// workflow. One or
	// more elements have
	// been added to the
	// template.

	public static String editor_DropActivity_Title; // Drop activity
	public static String editor_DropActivity_Question; // How should the
	// activity "{1}" be
	// inserted into the activity
	// "{0}":
	public static String editor_DropActivity_Copy; // Create Copy
	public static String editor_DropActivity_SAN; // Create as Embedded
	// Subprocess
	public static String editor_DropActivity_LAN; // Create as Subprocess Call

	public static String editor_Yes_message; // Yes
	public static String editor_No_message; // No
	public static String editor_Cancel_message; // Cancel
	public static String editor_Ok_message; // OK

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Guards
	//

	public static String guards_ParseError_title; // Error parsing guard
	public static String guards_ParseError_message; // Your entered guard is not
	// valid
	public static String guards_ParseError_message2; // Please enter your guard
	// in the
	// form '(data.attribute > 5) &&&&
	// (data.attribute < 10)'.

	public static String guards_ExceptionClosingBracket_message; // missing
	// closing
	// bracket
	public static String guards_ExceptionUnknownSign_message; // unknown sign
	public static String guards_ExceptionUnevenBracket_message; // uneven number
	// of
	// brackets
	public static String guards_ExceptionUnknownOperator_message; // unknown
	// operator
	public static String guards_ExceptionNotCorrectTerm_message; // expression
	// missing
	public static String guards_ExceptionUnknownCombination_message; // wrong
	// combination
	// of terms
	public static String guards_ExceptionUnknownAttribute_message; // unknown
	// attribute

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Preferences
	//

	public static String preferences_main_label; // Preferences
	public static String preferences_simulator_label; // Simulator
	public static String preferences_grid_label; // Grid
	public static String preferences_guard_label; // Guards
	public static String preferences_appearance_label; // Appearance
	public static String preferences_outline_label; // Outline
	public static String preferences_font_label; // Font
	public static String preferences_color_label; // Color
	public static String preferences_appnodes_label; // Nodes
	public static String preferences_appother_label; // Other
	public static String preferences_view_label; // Other

	public static String preferences_appcolor_ed_label; // Editor
	public static String preferences_appcolor_elem_label; // Elements

	public static String preferences_appother_eff_label; // Effects
	public static String preferences_appother_oth_label; // Other preferences

	public static String preferences_appoverview_show_label; // Display overview
	// page

	public static String preferences_Simulatorpath_label; // Path to simulator:

	public static String preferences_gridwidth_label; // Grid width:
	public static String preferences_gridheight_label; // Grid height:

	public static String preferences_guardtcut_label; // Trim textual
	// description after (0
	// to deactivate):
	public static String preferences_guardscut_label; // Trim short description
	// after (0
	// to deactivate):
	public static String preferences_guardtwrap_label; // Wrap textual
	// description after
	// (0 to deactivate):
	public static String preferences_guardswrap_label; // Wrap short description
	// after (0
	// to deactivate):
	public static String preferences_guardautowrap_label; // Wrap only after
	// white spaces
	// and control characters

	public static String preferences_outlineviews_label; // Views
	public static String preferences_outlineshowareas_label; // Views displayed
	// in the
	// outline
	public static String preferences_outlinebothview_label; // Graphical
	// Overview and Tree
	// View
	public static String preferences_outlineoverview_label; // Only Graphical
	// Overview
	public static String preferences_outlinetreeview_label; // Only Tree View
	public static String preferences_outlinetreeoptions_label; // Tree View
	// Options
	public static String preferences_outlinesort_label; // Sort by element type
	// outline view
	public static String preferences_outlineactedges_label; // Hide activity
	// edges in
	// outline view

	public static String preferences_appeditorcolor_label; // Editor background
	// color:
	public static String preferences_apptextcolor_label; // Text color:
	public static String preferences_appbordercolor_label; // Border color:
	public static String preferences_appfillcolor_label; // Fill color:
	public static String preferences_appguardtcolor_label; // Textual
	// description font
	// color:
	public static String preferences_appguardscolor_label; // Short description
	// font
	// color:

	public static String preferences_appdefaultfont_label; // Default
	public static String preferences_refelementfont_label; // Reference
	public static String preferences_guardfont_label; // Guard

	public static String preferences_appshadowvisible_label; // Shadow for model
	// elements
	public static String preferences_appfeedbackshow_label; // Visual feedback
	// on dropping
	public static String preferences_appantialiasing_label; // Antialiasing
	public static String preferences_appmouseposshow_label; // Show mouse
	// position
	public static String preferences_applinewidth_label; // Line width:
	public static String preferences_appcornersize_label; // Corner size:
	public static String preferences_appfigurebarwidth_label; // Node width:
	public static String preferences_appfigureminimum_label; // Minimal size:
	public static String preferences_appfigureicon_label; // Minimal icon size:
	public static String preferences_appshadowcolor_label; // Shadow color:
	public static String preferences_appconrouter_label; // Connection router:
	public static String preferences_appconrouter_sp_label; // Shortest Path
	// Router
	public static String preferences_appconrouter_m_label; // Manhattan Router
	public static String preferences_appconrouter_f_label; // Fan Router
	public static String preferences_appconrouter_n_label; // No Router

	public static String preferences_viewlayoutdata_label; // 
	public static String preferences_viewlayoutdatadialog_label; // 
	public static String preferences_viewlayoutdatawizard_label; // 
	public static String preferences_viewlayoutdatalayout_label; // 
	public static String preferences_viewlayoutdataimport_label; // 
	public static String preferences_viewlayoutdatanothing_label; // 

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Menus
	//

	public static String menu_ShowPropertiesView_item; // Show Properties
	public static String menu_ShowPropertiesView_description; // Show Properties
	// View

	public static String menu_ShowActivity_item; // Show in current Editor
	public static String menu_ShowActivity_description; // Show in current
	// Editor

	public static String menu_CreateChild_item; // &New Child
	public static String menu_CreateSibling_item; // N&ew Sibling

	public static String menu_WE_item; // New Workflow model...
	public static String menu_WE_description; // Creates a new Workflow model
	public static String menu_WE_wizard; // New Workflow model
	public static String menu_WEP_wizard; // Add new Workflow model to project

	public static String menu_Process_item; // New Process...
	public static String menu_Process_description; // Creates a new process

	public static String menu_Application_item; // New Application...
	public static String menu_Application_description; // Creates a new
	// application

	public static String menu_Data_item; // New Data...
	public static String menu_Data_description; // Creates a new data

	public static String menu_Role_item; // New Role...
	public static String menu_Role_description; // Creates a new role

	public static String menu_Open_item; // Open workflow
	public static String menu_Open_description; // Opens a workflow file

	public static String menu_OpenUri_item; // Open workflow URI
	public static String menu_OpenUri_description; // Opens a workflow loading
	// it from an
	// URI

	public static String menu_ImportExport_item; // Import/Export workflow
	// template...
	public static String menu_ImportExport_description; // Import or export
	// parts of the
	// workflow as template

	public static String menu_StartProcess_label; // Execute process
	public static String menu_StartProcess_description; // Execute process

	public static String menu_ZoomIn_item; // Zoom In
	public static String menu_ZoomIn_description; // Zoom In

	public static String menu_ZoomOut_item; // Zoom Out
	public static String menu_ZoomOut_description; // Zoom Out

	public static String menu_Tutorial_label; // Tutorial
	public static String menu_Tutorial_description; // Shows the tutorial

	public static String menu_About_item; // Workflow Editor About...
	public static String menu_About_description; // About Workflow Editor

	public static String menu_SaveImage_item; // Save process as image
	public static String menu_SaveImage_description; // Save currently displayed
	// process
	// as image

	public static String menu_Rename_item; // Rename...
	public static String menu_Rename_description; // Rename selected object

	public static String menu_Grid_item; // Grid
	public static String menu_Grid_description; // Grid

	public static String menu_GridVisible_item; // Toggle grid visibility
	public static String menu_GridVisible_description; // Toggle grid visibility

	public static String menu_GridSnap_item; // Toggle automatic snap to grid
	public static String menu_GridSnap_description; // Toggle automatic snap to
	// grid
	// function

	public static String menu_GridSnapNowCorner_item; // Align items to grid
	public static String menu_GridSnapNowCorner_description; // Snap all items
	// to the grid

	public static String menu_GridSnapNowCentered_item; // Align items to grid
	// (centered)
	public static String menu_GridSnapNowCentered_description; // Snap all items
	// to the
	// grid centered

	public static String menu_Alignment_item; // Alignment
	public static String menu_Alignment_description; // Element alignment

	public static String menu_AlignmentTop_item; // Alignment top
	public static String menu_AlignmentTop_description; // Align the selected
	// elements to
	// the top

	public static String menu_AlignmentBottom_item; // Alignment bottom
	public static String menu_AlignmentBottom_description; // Align the selected
	// elements
	// to the bottom

	public static String menu_AlignmentRight_item; // Alignment right
	public static String menu_AlignmentRight_description; // Align the selected
	// elements
	// to the right

	public static String menu_AlignmentLeft_item; // Alignment left
	public static String menu_AlignmentLeft_description; // Align the selected
	// elements to
	// the left

	public static String menu_AlignmentCenter_item; // Alignment horizontal
	public static String menu_AlignmentCenter_description; // Align the selected
	// elements
	// horizontal

	public static String menu_AlignmentMiddle_item; // Alignment vertical
	public static String menu_AlignmentMiddle_description; // Align the selected
	// elements
	// vertical

	public static String menu_External_label; // External Functions
	public static String menu_External_description; // External Functions

	public static String menu_ExternalSheet_label; // External Editor Sheets
	public static String menu_ExternalSheet_description; // External Editor
	// Sheets

	public static String menu_Views_label; // View:
	public static String menu_Views_description; // Change current view

	public static String menu_ViewConf_item; // 
	public static String menu_ViewConf_description; // 

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Properties
	//

	public static String properties_Standard_label; // Standard

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Views
	//

	public static String view_NewView_name; // NewView

	public static String view_Business_name; // Business
	public static String view_Business_icon; // view_business.gif

	public static String view_Technical_name; // Technical
	public static String view_Technical_icon; // view_technical.gif

	public static String view_Tooltip_description; // Workflow View

	public static String view_LayoutData_label; // 
	public static String view_NoLayoutData_label; // 
	public static String view_NoLayoutData2_label; // 
	public static String view_NoLayoutDataWizard_label; // 
	public static String view_NoLayoutDataNothing_label; // 
	public static String view_NoLayoutDataDeactivate_label; // 
	public static String view_NoLayoutDataImport_label; // 

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Palette
	//

	public static String palette_ToolsGroup_name; // Tools
	public static String palette_ActivityElementsGroups_name; // Activity
	// Elements
	public static String palette_PackageableElementsGroups_name; // New Elements

	// {0}: Type of entry
	// {1}: Name of entry
	public static String palette_CreationEntry_description; // Add a new {1}
	public static String palette_DynamicEntry_description; // Add the {1}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Wizards
	//

	public static String wizards_TemplateWizard_title; // Import/Export Workflow
	// Templates
	public static String wizards_ModeTemplateWizard_title; // Select
	// Import/Export
	// mode
	public static String wizards_ImportTemplateWizard_title; // Import Workflow
	// Templates
	public static String wizards_ExportTemplateWizard_title; // Export Workflow
	// Template
	public static String wizards_RoleWizard_title; // New Role
	public static String wizards_DataWizard_title; // New Data
	public static String wizards_DatatypeWizard_title; // New DataType
	public static String wizards_AppWizard_title; // New Application
	public static String wizards_ProcessWizard_title; // New Process
	public static String wizards_Page_name_package; // Select name, package and
	// icon!
	public static String wizards_Page_jar; // Select JAR-Archive, Java class and
	// Method!
	public static String wizards_Page_datatype; // Select datatype and value!
	public static String wizards_Files_iconfiles; // Icon files
	public static String wizards_Files_jarfiles; // JAR Archive
	public static String wizards_Files_templatefiles; // Template files
	public static String wizards_Files_files; // Workflow files
	public static String wizards_NewPackage; // New Package

	public static String wizards_ModelWizardPackage_label; // Workflow name
	public static String wizards_ModelWizardPackage_std; // Workflow
	public static String wizards_ModelWizardActivity_label; // First model
	public static String wizards_ModelWizardActivity_std; // Diagram1
	public static String wizards_ModelWizardActivityComment_std; // This is a
	// basic
	// activity
	public static String wizards_ModelWizardAuthor_label; // Author
	public static String wizards_ModelWizardVersion_label; // Version
	public static String wizards_ModelWizardFile_label; // File name
	public static String wizards_ModelWizardStandardPackages_label; // Create
	// packages
	// for
	// applications, roles
	// and data
	public static String wizards_ModelWizardApplicationPackage_std; // Applications
	public static String wizards_ModelWizardApplicationComment_std; // The
	// standard
	// package for
	// applications
	public static String wizards_ModelWizardRolePackage_std; // Roles
	public static String wizards_ModelWizardRoleComment_std; // The standard
	// package for
	// roles
	public static String wizards_ModelWizardDataPackage_std; // Data
	public static String wizards_ModelWizardDataComment_std; // The standard
	// package for
	// data
	public static String wizards_ModelWizardDatatypesComment_std; // The
	// standard
	// package
	// for datatypes
	public static String wizards_ModelWizardDatatypesPackage_std; // Datatypes

	public static String wizards_Import_title; // Template Import
	public static String wizards_Import_ok; // The import of the selected
	// elements was
	// successful.
	public static String wizards_Import_failed; // The import of the selected
	// elements
	// failed.

	public static String wizards_Export_title; // Template Export
	public static String wizards_Export_ok; // The export of the selected
	// elements to
	// "{0}" was successful.
	public static String wizards_Export_failed; // The export of the seleceted
	// elements to
	// "{0}" failed.

	public static String wizards_TemplateContents_label; // Template content
	public static String wizards_TemplateSelected_label; // Selected templates

	public static String wizards_Browse_label; // &Browse
	public static String wizards_Add_label; // &Add
	public static String wizards_Remove_label; // &Remove
	public static String wizards_RemoveAll_label; // R&emove all
	public static String wizards_ModelError_label; // < could not load file >
	public static String wizards_ImportSelect_label; // Select elements to
	// import
	public static String wizards_ImportNoSelected_error; // No template is
	// selected.
	public static String wizards_ExportTemplate_label; // Template File:
	public static String wizards_ExportComments_label; // Export comments?
	public static String wizards_ExportSelect_label; // Select elements to
	// export
	public static String wizards_ExportFilename_error; // No file name.
	public static String wizards_ExportChecked_error; // No element is selected.
	public static String wizards_AddDatatype_label; // &New Data Type

	public static String wizards_PackagDialog_name; // Name of the new package:
	public static String wizards_IsWebServiceApplication_name; // Whether web
	// service
	// application shall be
	// created or not in the
	// wizard

	public static String wizards_ViewConf_title; // 
	public static String wizards_ViewConfSelect_title; // 
	public static String wizards_ViewConfImport_title; // 
	public static String wizards_ViewConfCreate_title; // 
	public static String wizards_ViewConfActivity_title; // 
	public static String wizards_ViewConfPreview_title; // 
	public static String wizards_ViewConfActivity_message; // 
	public static String wizards_ViewConfImport_message; // 
	public static String wizards_ViewConfCreate_message; // 

	public static String wizards_ViewConfImport_label; // 
	public static String wizards_ViewConfCreate_label; // 
	public static String wizards_ViewConfCurrentView_label; // 
	public static String wizards_ViewConfCreateSelect_label; // 
	public static String wizards_ViewConfImportSelect_label; // 
	public static String wizards_ViewConfImportOnlyNew_label; // 
	public static String wizards_ViewConfImportRelevant_label; // 
	public static String wizards_ViewConfActivityAll_label; // 
	public static String wizards_ViewConfActivitySelected_label; // 
	public static String wizards_ViewConfPreviewSelect_label; // 

	public static String wizards_ViewConfAlgorithm_HorizontalTree_name; // 
	public static String wizards_ViewConfAlgorithm_Tree_name; // 
	public static String wizards_ViewConfAlgorithm_Radial_name; // 
	public static String wizards_ViewConfAlgorithm_Spring_name; // 
	public static String wizards_ViewConfAlgorithm_HorizontalShift_name; // 
	public static String wizards_ViewConfAlgorithm_Horizontal_name; // 
	public static String wizards_ViewConfAlgorithm_Vertical_name; // 
	public static String wizards_ViewConfAlgorithm_HorizontalTree_description; // 
	public static String wizards_ViewConfAlgorithm_Tree_description; // 
	public static String wizards_ViewConfAlgorithm_Radial_description; // 
	public static String wizards_ViewConfAlgorithm_Spring_description; // 
	public static String wizards_ViewConfAlgorithm_HorizontalShift_description; // 
	public static String wizards_ViewConfAlgorithm_Horizontal_description; // 
	public static String wizards_ViewConfAlgorithm_Vertical_description; // 

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Configuration Page
	//

	public static String configPage_Properties; // Properties
	public static String configPage_Title; // Overview
	public static String configPage_Name; // Name
	public static String configPage_Location; // Location
	public static String configPage_Author; // Author
	public static String configPage_VNumber; // Version Number
	public static String configPage_LastModified; // Last Modified
	public static String configPage_Documentation; // Documentation
	public static String configPage_Roles; // Roles
	public static String configPage_Data; // Data
	public static String configPage_Application; // Applications
	public static String configPage_Activity; // Processes
	public static String configPage_Add; // Add
	public static String configPage_Delete; // Delete
	public static String configPage_Edit; // Edit

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Extension Points
	//

	public static String extension_point_menu; // org.eclipse.jwt.we.menu
	public static String extension_point_view; // org.eclipse.jwt.we.view
	public static String extension_point_propertydescriptor; // org.eclipse.jwt.we.propertyDescriptor
	public static String extension_point_notifychangedlistener; // org.eclipse.jwt.we.changeListener
	public static String extension_point_sheet; // org.eclipse.jwt.we.sheet
	public static String extension_point_registry; // org.eclipse.jwt.we.factoryRegistry

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Simulator
	//

	public static String dialog_no_simulator_path_title; // No simulator path
	public static String dialog_no_simulator_path_message; // No simulator path
	// was
	// configured on menu window
	// -> preferences

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Dialogs
	//
	public static String dialog_overwrite_title; // Overwrite file?
	public static String dialog_overwrite_message; // The chosen file already
	// exists. Do
	// you want to overwrite it?

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Datatypes
	//
	public static String Datatypes_URL; // URL
	public static String Datatypes_dioParameter; // dioParameter
	public static String Datatypes_qualifier; // qualifier
	public static String Datatypes_searchquery; // searchquery
	public static String Datatypes_filename; // filename

	public static String browse_button;

	public static String insertActivity;

	public static String choose_jar_title;

	public static String about_title;

	public static String incomplete_jwt_extension_conf_title;

	public static String incomplete_jwt_extension_conf_details;

	public static String incorrect_metamodel_extension_definition_title;

	public static String incorrect_metamodel_extension_definition_details;

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Model
	//

	// model_ModelElement_icon ; //
	public static String model_Comment_icon; // model_comment.png
	// model_NamedElement_icon ; //
	public static String model_Package_icon; // model_package.gif
	// model_PackageableElement_icon ; //
	// model_ReferenceableElement_icon ; //
	public static String model_Model_icon; // model_model.gif

	// model_Scope_icon ; //
	public static String model_Activity_icon; // model_activity.gif
	public static String model_StructuredActivityNode_icon; // model_structuredactivitynode.gif
	public static String model_ActivityLinkNode_icon; // model_activitylinknode.png
	// model_ActivityNode_icon ; //
	public static String model_ActivityEdge_icon; // model_activityedge.gif
	public static String model_ReferenceEdge_icon; // model_referenceedge.gif
	// model_GuardSpecification_icon ; //
	public static String model_Guard_icon; // model_guard.gif
	// model_ExecutableNode_icon ; //
	// model_ControlNode_icon ; //
	public static String model_Action_icon; // model_action.gif
	public static String model_InitialNode_icon; // model_initialnode.gif
	public static String model_FinalNode_icon; // model_finalnode.gif
	public static String model_DecisionNode_icon; // model_decisionnode.png
	public static String model_DecisionNode_largeIcon; // model_decisionnode_large.png
	public static String model_ForkNode_icon; // model_forknode.gif
	public static String model_JoinNode_icon; // model_joinnode.gif
	public static String model_MergeNode_icon; // model_mergenode.png
	public static String model_MergeNode_largeIcon; // model_mergenode_large.png
	// model_Reference_icon ; //
	public static String model_Event_icon; // model_event.png
	// model_EventHandler_icon ; //
	// model_PrimitiveType_icon ; //
	// model_StringType_icon ; //
	// model_IntegerType_icon ; //
	// model_Function_icon ; //
	public static String model_Role_icon; // model_role.png
	public static String model_OrganisationUnit_icon; // model_organisationunit.gif
	public static String model_Application_icon; // model_application.png
	public static String model_WebServiceApplication_icon; // model_webserviceapplication.png
	// model_ApplicationType_icon ; //

	public static String model_Data_icon; // model_data.png

	// model_DataType_icon ; //
	// model_Parameter_icon ; //
	// model_DataMapping_icon ; //
	// model_InformationType_icon ; //

	// model_GraphicalElement_icon ; //

	public static String model_Diagram_type; // 
	public static String model_ReferenceEdge_type; // 
	public static String model_Reference_type; // 
	public static String model_LayoutData_type; // 

	public static String model_Reference_text; // 

	public static String model_Reference_reference_name; // 
	public static String model_Reference_reference_description; // 
	public static String model_Reference_referenceEdges_name; // 
	public static String model_Reference_referenceEdges_description; // 

	public static String model_ReferenceEdge_reference_name; // 
	public static String model_ReferenceEdge_action_name; // 
	public static String model_ReferenceEdge_direction_name; // 

	public static String model_ReferenceEdge_reference_description; // 
	public static String model_ReferenceEdge_action_description; // 
	public static String model_ReferenceEdge_direction_description; // 

	public static String _UI_CreateChild_label; // Create child
	public static String _UI_CreateChild_tooltip; // Create New {0} Under {1}
	// Feature
	public static String _UI_CreateChild_description; // Create a new child of
	// type {0} for the {1}
	// feature of the
	// selected {2}.
	public static String _UI_CreateSibling_description; // Create a new sibling
	// of type {0} for the
	// selected {2}, under
	// the {1} feature of
	// their parent.

	public static String _UI_PropertyDescriptor_description; // The {0} of the
	// {1}

	public static String _UI_LayoutData_type; // Layout Data
	public static String _UI_Reference_type; // Reference
	public static String _UI_ReferenceEdge_type; // Reference Edge
	public static String _UI_Diagram_type; // Diagram
	public static String _UI_Unknown_type; // Object

	public static String _UI_Unknown_datatype; // Value

	public static String _UI_LayoutData_describesElement_feature; // Describes
	// Element
	public static String _UI_LayoutData_viewid_feature; // Viewid
	public static String _UI_LayoutData_width_feature; // Width
	public static String _UI_LayoutData_height_feature; // Height
	public static String _UI_LayoutData_x_feature; // X
	public static String _UI_LayoutData_y_feature; // Y
	public static String _UI_LayoutData_initialized_feature; // Initialized
	public static String _UI_Reference_containedIn_feature; // Contained In
	public static String _UI_Reference_reference_feature; // Reference
	public static String _UI_Reference_referenceEdges_feature; // Reference
	// Edges
	public static String _UI_ReferenceEdge_containedIn_feature; // Contained In
	public static String _UI_ReferenceEdge_reference_feature; // Reference
	public static String _UI_ReferenceEdge_action_feature; // Action
	public static String _UI_ReferenceEdge_direction_feature; // Direction
	public static String _UI_Diagram_describesModel_feature; // Describes Model
	public static String _UI_Diagram_layoutData_feature; // Layout Data
	public static String _UI_Diagram_references_feature; // References
	public static String _UI_Diagram_referenceEdges_feature; // Reference Edges
	public static String _UI_Unknown_feature; // Unspecified

	public static String _UI_EdgeDirection_default_literal; // default
	public static String _UI_EdgeDirection_none_literal; // none
	public static String _UI_EdgeDirection_in_literal; // in
	public static String _UI_EdgeDirection_out_literal; // out
	public static String _UI_EdgeDirection_inout_literal; // inout


	//
	//
	//
	//
	//
	//

	/**
	 * Question for the overwrite file dialog.
	 * 
	 * @param fileName
	 *            The name of the file that is overwritten.
	 * @return The message.
	 */
	public static String editor_QuestionOverwriteFile_message(String fileName)
	{
		return NLS.bind(editor_QuestionOverwriteFile_message, fileName);
	}


	/**
	 * Title for the editor.
	 * 
	 * @param filename
	 *            The name of the opened file.
	 * @param modelName
	 *            The name of the shown model.
	 * @return The title.
	 */
	public static String editor_Title_text(String filename, String modelName)
	{
		return NLS.bind(editor_Title_text, filename, modelName);
	}


	/**
	 * Title for error messages.
	 */

	/**
	 * Error message if a file to open has no editor.
	 * 
	 * @param filename
	 *            The name of the file.
	 * @return The message.
	 */
	public static String editor_WarnNoEditor_message(String filename)
	{
		return NLS.bind(editor_WarnNoEditor_message, filename);
	}


	/**
	 * Title for open editor message.
	 */

	/**
	 * Message if saving a file fails.
	 * 
	 * @param filename
	 *            The name of the file.
	 * @return The message.
	 */
	public static String editor_ErrorSavingFile_message(String filename)
	{
		return NLS.bind(editor_ErrorSavingFile_message, filename);
	}


	/**
	 * Message if loading a file fails.
	 * 
	 * @param filename
	 *            The name of the file.
	 * @return The message.
	 */
	public static String editor_ErrorLoadingFile_message(String filename)
	{
		return NLS.bind(editor_ErrorLoadingFile_message, filename);
	}


	/**
	 * Creates a name for a creation entry of a model element.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return The name for the entry.
	 */
	public static String palette_CreationEntry_name(Object model)
	{
		String result = org.eclipse.jwt.meta.PluginProperties.model_type(model);

		if (result.startsWith(NLS_MISSING))
		{
			result = model_type(model);
		}

		return result;
	}


	/**
	 * Creates a description for a creation entry of a model element.
	 * 
	 * @param model
	 *            The class of the model that is created.
	 * @return The content of the description.
	 */
	public static String palette_CreationEntry_description(Object model)
	{
		String entryName = palette_CreationEntry_name(model);
		String typeName = org.eclipse.jwt.meta.PluginProperties.model_type(model);
		if (typeName.startsWith(NLS_MISSING))
		{
			typeName = model_type(model);
		}

		return NLS.bind(palette_CreationEntry_description, entryName, typeName);
	}


	/**
	 * Creates the name of a dynamic group based on the class of the model
	 * elements to show in that group.
	 * 
	 * @param model
	 *            Class of model elements.
	 * @return Title of the group.
	 */
	public static String palette_DynamicGroup_name(Object model)
	{
		return org.eclipse.jwt.meta.PluginProperties.getModelTypeName(model);
	}


	/**
	 * Return a description for a dynamic entry.
	 * 
	 * @param entryName
	 *            The name of the entry.
	 * @param model
	 *            The model.
	 * @return A description.
	 */
	public static String palette_DynamicEntry_description(String entryName, Object model)
	{
		String typeName = org.eclipse.jwt.meta.PluginProperties.model_type(model);
		return NLS.bind(palette_DynamicEntry_description, entryName, typeName);
	}


	/**
	 * Returns a path for an icon for a model element.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return A path to an icon.
	 */
	public static String model_icon(Object model)
	{
		String modelName = org.eclipse.jwt.meta.PluginProperties.getModelTypeName(model);
		String result = null;

		// try to get image name from resource bundle
		try
		{
			result = getStringExpectMissing("model_" + modelName + "_icon"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (MissingResourceException e)
		{
		}

		// if image not found, try loading default image
		if (result == null || result.startsWith(NLS_MISSING))
		{
			try
			{
				result = getStringExpectMissing("model_Default_icon"); //$NON-NLS-1$
			}
			catch (MissingResourceException e)
			{
			}
		}

		return result;
	}


	/**
	 * Returns a path for an icon for a model element.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @param largeIcon
	 *            If <code>true</code>, a large icon will be retured, otherwise
	 *            a small icon.
	 * @return A path to an icon.
	 */
	public static String model_icon(Object model, boolean largeIcon)
	{
		String modelName = org.eclipse.jwt.meta.PluginProperties.getModelTypeName(model);
		String result = null;
		String imageType = largeIcon ? "_largeIcon" : "_smallIcon"; //$NON-NLS-1$ //$NON-NLS-2$

		// try to get image name from resource bundle
		try
		{
			result = getStringExpectMissing("model_" + modelName + imageType); //$NON-NLS-1$
		}
		catch (MissingResourceException e)
		{
		}

		if (result == null || result.startsWith(NLS_MISSING))
		{
			// no small image found, try normal image
			try
			{
				result = model_icon(model);
			}
			catch (MissingResourceException e)
			{
			}

			// if image not found, try loading default image
			if (result == null)
			{
				result = getStringExpectMissing("model_Default" + imageType); //$NON-NLS-1$
			}
		}

		return result;
	}


	/**
	 * Returns a path for a small icon for a model element.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return A path to a small icon.
	 */
	public static String model_smallIcon(Object model)
	{
		return model_icon(model, false);
	}


	/**
	 * Returns a path for a large icon for a model element.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return A path to a large icon.
	 */
	public static String model_largeIcon(Object model)
	{
		return model_icon(model, true);
	}


	/**
	 * The name of the type of a model.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return A name of the type.
	 */
	public static String model_type(Object model)
	{
		String result = org.eclipse.jwt.meta.PluginProperties.model_type(model);

		if (result.startsWith(NLS_MISSING))
		{
			if (model instanceof EAttribute)
			{
				return model_datatype((EAttribute) model);
			}

			String modelType = org.eclipse.jwt.meta.PluginProperties
					.getModelTypeName(model);
			try
			{
				return getStringExpectMissing("model_" + modelType + "_type"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			catch (MissingResourceException e2)
			{
			}

			return modelType;

		}

		return result;
	}


	/**
	 * The name of an attribute.
	 * 
	 * @param attribute
	 *            The attribute.
	 * @return A name of the type.
	 */
	public static String model_datatype(EAttribute attribute)
	{
		String result = org.eclipse.jwt.meta.PluginProperties.model_datatype(attribute);

		if (result.startsWith(NLS_MISSING))
		{
			String name = attribute.getName();
			try
			{
				return getStringExpectMissing("model_" + name + "_datatype"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			catch (MissingResourceException e2)
			{
			}

			return name;
		}

		return result;
	}


	/**
	 * The name of a feature.
	 * 
	 * @param feature
	 *            The feature.
	 * @return The name of a feature.
	 */
	public static String model_feature_name(Object feature)
	{
		String result = org.eclipse.jwt.meta.PluginProperties.model_feature_name(feature);

		if (result.startsWith(NLS_MISSING))
		{
			if (feature instanceof EStructuralFeature)
			{
				String featureName = ((EStructuralFeature) feature).getName();
				String modelType = org.eclipse.jwt.meta.PluginProperties
						.getModelTypeName(((EStructuralFeature) feature)
								.getEContainingClass());

				try
				{
					return getStringExpectMissing("model_" + modelType + "_" + featureName //$NON-NLS-1$ //$NON-NLS-2$
							+ "_name"); //$NON-NLS-1$
				}
				catch (MissingResourceException e2)
				{
				}

				return featureName;
			}

			return org.eclipse.jwt.meta.PluginProperties.model_Unknown_feature_name;
		}

		return result;
	}


	/**
	 * The default description of a feature.
	 * 
	 * @param feature
	 *            The feature.
	 * @return The default description of a feature.
	 */
	public static String model_Default_feature_description(EStructuralFeature feature)
	{
		String result = org.eclipse.jwt.meta.PluginProperties
				.model_Default_feature_description(feature);

		if (result.startsWith(NLS_MISSING))
		{

			String featureName = model_feature_name(feature);
			String modelType = model_type(feature.getEContainingClass());

			return bind(
					org.eclipse.jwt.meta.PluginProperties.model_Default_feature_description,
					modelType, featureName);
		}
		return result;
	}


	/**
	 * The description of a feature.
	 * 
	 * @param feature
	 *            The feature.
	 * @return The description of a feature.
	 */
	public static String model_feature_description(EStructuralFeature feature)
	{
		String result = org.eclipse.jwt.meta.PluginProperties
				.model_feature_description(feature);

		if (result.startsWith(NLS_MISSING))
		{
			String featureName = feature.getName();
			String modelType = org.eclipse.jwt.meta.PluginProperties
					.getModelTypeName(feature.getEContainingClass());
			String result2 = null;

			try
			{
				result2 = getStringExpectMissing("model_" + modelType + "_" + featureName //$NON-NLS-1$ //$NON-NLS-2$
						+ "_description"); //$NON-NLS-1$
			}
			catch (MissingResourceException e2)
			{
			}

			if (result2 == null)
			{
				// try default
				result2 = model_Default_feature_description(feature);
			}

			return result2;
		}
		return result;
	}


	/**
	 * The text to represent a model.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @param features
	 *            Describes the features.
	 * @return A text representation of the model element.
	 */
	public static String model_text(Object model, Object[] features)
	{
		String result = org.eclipse.jwt.meta.PluginProperties.model_text(model, features);

		if (result.startsWith(NLS_MISSING))
		{
			String modelType = org.eclipse.jwt.meta.PluginProperties
					.getModelTypeName(model);
			try
			{
				return getStringExpectMissing("model_" + modelType + "_text", features); //$NON-NLS-1$ //$NON-NLS-2$
			}
			catch (MissingResourceException e2)
			{
			}

			return modelType;
		}
		return result;
	}


	/**
	 * Label text for the create child command.
	 * 
	 * @param child
	 *            The child object.
	 * @param feature
	 *            The feature.
	 * @param owner
	 *            The owner object.
	 * @return The text.
	 */
	public static String command_CreateChild_text(Object child, Object feature,
			Object owner)
	{
		return org.eclipse.jwt.meta.PluginProperties.command_CreateChild_text(child,
				feature, owner);
	}


	/**
	 * Description for the create child command.
	 * 
	 * @param child
	 *            The child object.
	 * @param feature
	 *            The feature.
	 * @param owner
	 *            The owner object.
	 * @return The description.
	 */
	public static String command_CreateChild_description(Object child, Object feature,
			Object owner)
	{
		return org.eclipse.jwt.meta.PluginProperties.command_CreateChild_description(
				child, feature, owner);
	}


	/**
	 * Tooltip for the create child command.
	 * 
	 * @param child
	 *            The child object.
	 * @param feature
	 *            The feature.
	 * @param owner
	 *            The owner object.
	 * @return The tooltip.
	 */
	public static String command_CreateChild_tooltip(Object child, Object feature,
			Object owner)
	{
		return org.eclipse.jwt.meta.PluginProperties.command_CreateChild_tooltip(child,
				feature, owner);
	}


	/**
	 * Description text for the create sibling command.
	 * 
	 * @param child
	 *            The child object.
	 * @param feature
	 *            The feature.
	 * @param owner
	 *            The owner object.
	 * @return The description.
	 */
	public static String command_CreateSibling_description(Object child, Object feature,
			Object owner)
	{
		return org.eclipse.jwt.meta.PluginProperties.command_CreateSibling_description(
				child, feature, owner);
	}


	/**
	 * Returns a label for a delete command.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return A label.
	 */
	public static String command_Delete_label(Object model)
	{
		return org.eclipse.jwt.meta.PluginProperties.command_Delete_label(model);
	}


	/**
	 * Returns a label for a create command.
	 * 
	 * @param model
	 *            The model or the type of the model.
	 * @return A label.
	 */
	public static String command_Create_label(Object model)
	{
		return org.eclipse.jwt.meta.PluginProperties.command_Create_label(model);
	}

}
