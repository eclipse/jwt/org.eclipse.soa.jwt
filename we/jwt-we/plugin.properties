#
# File:    plugin.properties
# Created: 02.02.2006
#
#
#/*******************************************************************************
# * Copyright (c) 2005-2012
# * University of Augsburg, Germany, <www.ds-lab.org>
# *
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
# *    	- initial API and implementation
# *    Mickael Istria, Open Wide, Lyon, France
# *     - Notes and fixes  NLS and internationalization
# *    Marc Blachon, Bull SAS, Grenoble, France
# *     - add extension point string for the factory registry
# *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
# *     - maintenance and extension
# *******************************************************************************/
#
################################################################################

################################################################################
#
# NOTES:
# - DO NOT ADD .properties FILES FOR TRANSLATION, USE Eclipse Babel INSTEAD
# - IF YOU NEED TO ADD A KEY, ADD IT ALSO AS A FIELD IN org.eclipse.jwt.we.PluginProperties
# - This file is strongly coupled with PluginProperties class using NLS
#
################################################################################




################################################################################
#
# Plugin data
#

plugin_Name = JWT Workflow Editor
plugin_ProviderName = http://www.eclipse.org/jwt
pluginName = JWT
providerName = http://www.eclipse.org/jwt

category_id = Java Workflow Tooling
perspective_id = org.eclipse.jwt.we.perspective

tutorial_path = doc/JWTWorkflowEditorUsageTutorial.pdf
tutorial_url = http://wiki.eclipse.org/JWT_Tutorial

plugin_workflow_extension = workflow
plugin_diagram_extension = workflow_view
plugin_template_extension = workflowtemplate

about_image_file = jwt_about.gif
about_text_file = abouttext.txt
about_title = About

logging_level = WARNING

################################################################################
#
# Model
#

#
# Icons:
#
# For a model "Foo" there exists the following icons:
#
# model_Foo_largeIcon: A large icon.
# model_Foo_smallIcon: A small icon.
# model_Foo_icon: Icon used if no large or small icon was found.
#
# model_Default_largeIcon: Used if no large Foo icon found.
# model_Default_smallIcon: Used if no small Foo icon found.
# model_DefaultIcon: Used if no large or small default icon was found.
#

#model_Default_icon = eclipse.gif

#model_ModelElement_icon =
model_Comment_icon = model_comment.png
#model_NamedElement_icon =
model_Package_icon = model_package.gif
#model_PackageableElement_icon =
#model_ReferenceableElement_icon =
model_Model_icon = model_model.gif

#model_Scope_icon =
model_Activity_icon = model_activity.gif
model_StructuredActivityNode_icon = model_structuredactivitynode.gif
model_ActivityLinkNode_icon = model_activitylinknode.png
#model_ActivityNode_icon =
model_ActivityEdge_icon = model_activityedge.gif
model_ReferenceEdge_icon = model_referenceedge.gif
#model_GuardSpecification_icon =
model_Guard_icon = model_guard.gif
#model_ExecutableNode_icon =
#model_ControlNode_icon =
model_Action_icon = model_action.gif
model_InitialNode_icon = model_initialnode.gif
model_FinalNode_icon = model_finalnode.gif
model_DecisionNode_icon = model_decisionnode.png
model_DecisionNode_largeIcon = model_decisionnode_large.png
model_ForkNode_icon = model_forknode.gif
model_JoinNode_icon = model_joinnode.gif
model_MergeNode_icon = model_mergenode.png
model_MergeNode_largeIcon = model_mergenode_large.png
#model_Reference_icon =
model_Event_icon = model_event.png
#model_EventHandler_icon =
#model_PrimitiveType_icon =
#model_StringType_icon =
#model_IntegerType_icon =
#model_Function_icon =
model_Role_icon = model_role.png
model_OrganisationUnit_icon = model_organisationunit.gif
model_Application_icon = model_application.png
model_WebServiceApplication_icon = model_webserviceapplication.png
#model_ApplicationType_icon =

model_Data_icon = model_data.png
#model_DataType_icon =
#model_Parameter_icon =
#model_DataMapping_icon =
#model_InformationType_icon =

#model_GraphicalElement_icon =


#
# Types:
#
# Name for the model and attribute types. The default is the typename itself.
#
# Example:
#   model_Foo_type = FooType
#   model_Bar_datatype = Bar data type
#

model_Diagram_type = Diagram
model_ReferenceEdge_type = Reference Edge
model_Reference_type = Reference
model_LayoutData_type = LayoutData

model_Reference_text = ({0}) {1}

#
# Features:
#
# Name and description for features of model elements.
#
# The default name for a feature is the name of the feature.
#
# The default description is taken from the property:
# model_Default_feature_description
# where:
# {0}: Name of the type.
# {1}: Name of the feature
#
#
# Examples:
#   model_Foo_bar_name = bar of Foo
#   model_Foo_bar_description = The bar of the Foo.
#   model_Default_feature_description = The {0} of the {1}
#


model_Reference_reference_name = Referenced Element
model_Reference_reference_description = Referenced Element
model_Reference_referenceEdges_name = Reference Edges
model_Reference_referenceEdges_description = Reference Edges

model_ReferenceEdge_reference_name = Reference
model_ReferenceEdge_action_name = Action
model_ReferenceEdge_direction_name = Direction

model_ReferenceEdge_reference_description = Reference of the egde
model_ReferenceEdge_action_description = Action of the edge
model_ReferenceEdge_direction_description = Direction of the edge


################################################################################
#
# Commands
#


################################################################################
#
# Editor
#

# {0}: Name of file
# {1}: Name of displayed model
editor_Title_text = {0} ({1})

editor_Default_text = Default

editor_ErrorMessage_title = Error
editor_WarnNoEditor_message = There is no editor registered for the file "{0}".
editor_ErrorOpenEditor_title = Error Open Editor
editor_ErrorReferences_message = Inconsistency in References detected and fixed. Should the changes be saved?
editor_ErrorReferences2_message = Inconsistency in References detected and fixed.
editor_ErrorSavingFile_message = Error saving file "{0}".
editor_ErrorLoadingFile_message = Error loading file "{0}".
editor_ErrorCreatingFile_message = Error creating file "{0}".
editor_ErrorCurrentlyOpen_message = "{0}" is currently open.
editor_MissingResource_message = The resource "{0}" is missing.

editor_StillReferenced_title = Element(s) still referenced 
editor_StillReferenced_message = Cannot be removed. The following elements are still referenced: {0} 
editor_DeleteActivities_title = Activities will be deleted
editor_DeleteActivities_message = Are you sure you want to delete the following activities: {0} 
editor_StillReferencedDelete_message = The following elements are still referenced: {0} Should the elements and all references to them be deleted? 
editor_StillReferencedConfirmation_message = All references to these elements as well as the elements themselves will be deleted. Are you sure? 

editor_Closing_title = Save Resource 
editor_Closing_message = "{0}" has been modified. Save changes ?

editor_QuestionOverwriteFile_title = Overwrite file
editor_QuestionOverwriteFile_message = "{0}" exists. Overwrite?

editor_Wizard_description = Create a new Workflow Model
editor_Wizard_icon = jwt16x16.gif

editor_WizardExportRefAdded_title = Referenced elements added
editor_WizardExportRefAdded_description = The selected elements have active references in the workflow. One or more elements have been added to the template. 

editor_DropActivity_Title = Drop activity
editor_DropActivity_Question = How should the activity "{1}" be inserted into the activity "{0}":
editor_DropActivity_Copy = Create Copy
editor_DropActivity_SAN = Create as Embedded Subprocess
editor_DropActivity_LAN = Create as Subprocess Call

editor_Yes_message = Yes 
editor_No_message = No  
editor_Cancel_message = Cancel  
editor_Ok_message = OK


################################################################################
#
# Guards
#

guards_ParseError_title = Error parsing guard
guards_ParseError_message = Your entered guard is not valid
guards_ParseError_message2 = Please enter your guard in the form '(data.attribute > 5) &&&& (data.attribute < 10)'.

guards_ExceptionClosingBracket_message = missing closing bracket
guards_ExceptionUnknownSign_message = unknown sign
guards_ExceptionUnevenBracket_message = uneven number of brackets
guards_ExceptionUnknownOperator_message = unknown operator
guards_ExceptionNotCorrectTerm_message = expression missing
guards_ExceptionUnknownCombination_message = wrong combination of terms
guards_ExceptionUnknownAttribute_message = unknown attribute


################################################################################
#
# Preferences
#

preferences_main_label = Preferences
preferences_simulator_label = Simulator
preferences_grid_label = Grid
preferences_guard_label = Guards
preferences_appearance_label = Appearance
preferences_outline_label = Outline
preferences_font_label = Font
preferences_color_label = Color
preferences_appnodes_label = Nodes
preferences_appother_label = Other
preferences_view_label = Views

preferences_appcolor_ed_label = Editor
preferences_appcolor_elem_label = Elements

preferences_appother_eff_label = Effects
preferences_appother_oth_label = Other preferences

preferences_appoverview_show_label = Display overview page

preferences_Simulatorpath_label = Path to simulator:

preferences_gridwidth_label = Grid width:
preferences_gridheight_label = Grid height:

preferences_guardtcut_label = Trim textual description after (0 to deactivate):
preferences_guardscut_label = Trim short description after (0 to deactivate):
preferences_guardtwrap_label = Wrap textual description after (0 to deactivate):
preferences_guardswrap_label = Wrap short description after (0 to deactivate):
preferences_guardautowrap_label = Wrap only after white spaces and control characters

preferences_outlineviews_label = Views
preferences_outlineshowareas_label = Views displayed in the outline
preferences_outlinebothview_label = Graphical Overview and Tree View
preferences_outlineoverview_label = Only Graphical Overview
preferences_outlinetreeview_label = Only Tree View
preferences_outlinetreeoptions_label = Tree View Options
preferences_outlinesort_label = Sort by element type
preferences_outlineactedges_label = Hide activity edges in outline view

preferences_appeditorcolor_label = Editor background color:
preferences_apptextcolor_label = Text color:
preferences_appbordercolor_label = Border color:
preferences_appfillcolor_label = Fill color:
preferences_appguardtcolor_label = Textual description font color:
preferences_appguardscolor_label = Short description font color:

preferences_appdefaultfont_label = Default
preferences_refelementfont_label = Reference
preferences_guardfont_label = Guard

preferences_appshadowvisible_label = Shadow for model elements 
preferences_appfeedbackshow_label = Visual feedback on dropping
preferences_appantialiasing_label = Antialiasing
preferences_appmouseposshow_label = Show mouse position
preferences_applinewidth_label = Line width:
preferences_appcornersize_label = Corner size:
preferences_appfigurebarwidth_label = Node width:
preferences_appfigureminimum_label = Minimal size:
preferences_appfigureicon_label = Minimal icon size:
preferences_appshadowcolor_label = Shadow color:
preferences_appconrouter_label = Connection router:
preferences_appconrouter_sp_label = Shortest Path Router
preferences_appconrouter_m_label = Manhattan Router
preferences_appconrouter_f_label = Fan Router
preferences_appconrouter_n_label = No Router

preferences_viewlayoutdata_label = If layout information is missing
preferences_viewlayoutdatadialog_label = Show dialog with options
preferences_viewlayoutdatawizard_label = Show view configuration wizard
preferences_viewlayoutdatalayout_label = Apply layout algorithm
preferences_viewlayoutdataimport_label = Import based on a best guess
preferences_viewlayoutdatanothing_label = Do nothing

################################################################################
#
# Menus
#

menu_ShowPropertiesView_item = Show Properties
menu_ShowPropertiesView_description = Show Properties View

menu_ShowActivity_item = Show in current Editor
menu_ShowActivity_description = Show in current Editor


menu_CreateChild_item = &New Child
menu_CreateSibling_item = N&ew Sibling

browse_button=&Browse...

menu_WE_item = New Workflow model...
menu_WE_description = Creates a new Workflow model
menu_WE_wizard = New Workflow model
menu_WEP_wizard = Add new Workflow model to project


menu_Process_item = New Process...
menu_Process_description = Creates a new process

menu_Application_item = New Application...
menu_Application_description = Creates a new application

menu_Data_item = New Data...
menu_Data_description = Creates a new data

menu_Role_item = New Role...
menu_Role_description = Creates a new role


menu_Open_item = Open workflow
menu_Open_description = Opens a workflow file

menu_OpenUri_item = Open workflow URI
menu_OpenUri_description = Opens a workflow loading it from an URI


menu_ImportExport_item = Import/Export workflow template...
menu_ImportExport_description = Import or export parts of the workflow as template

menu_StartProcess_label = Execute process
menu_StartProcess_description = Execute process


menu_ZoomIn_item = Zoom In
menu_ZoomIn_description = Zoom In

menu_ZoomOut_item = Zoom Out
menu_ZoomOut_description = Zoom Out


menu_Tutorial_label = Tutorial
menu_Tutorial_description = Shows the tutorial

menu_About_item = Workflow Editor About...
menu_About_description = About Workflow Editor

menu_SaveImage_item = Save process as image
menu_SaveImage_description = Save currently displayed process as image


menu_Rename_item = Rename...
menu_Rename_description = Rename selected object


menu_Grid_item = Grid
menu_Grid_description = Grid

menu_GridVisible_item = Toggle grid visibility
menu_GridVisible_description = Toggle grid visibility

menu_GridSnap_item = Toggle automatic snap to grid
menu_GridSnap_description = Toggle automatic snap to grid function

menu_GridSnapNowCorner_item = Align items to grid
menu_GridSnapNowCorner_description = Snap all items to the grid

menu_GridSnapNowCentered_item = Align items to grid (centered)
menu_GridSnapNowCentered_description = Snap all items to the grid centered


menu_Alignment_item = Alignment
menu_Alignment_description = Element alignment

menu_AlignmentTop_item = Alignment top
menu_AlignmentTop_description = Align the selected elements to the top

menu_AlignmentBottom_item = Alignment bottom
menu_AlignmentBottom_description = Align the selected elements to the bottom

menu_AlignmentRight_item = Alignment right
menu_AlignmentRight_description = Align the selected elements to the right

menu_AlignmentLeft_item = Alignment left
menu_AlignmentLeft_description = Align the selected elements to the left

menu_AlignmentCenter_item = Alignment horizontal
menu_AlignmentCenter_description = Align the selected elements horizontal

menu_AlignmentMiddle_item = Alignment vertical
menu_AlignmentMiddle_description = Align the selected elements vertical


menu_External_label = External
menu_External_description = External Functions

menu_ExternalSheet_label = External Editor Sheets
menu_ExternalSheet_description = External Editor Sheets

menu_Views_label = View:
menu_Views_description = Change current view

menu_ViewConf_item = Configure view layout
menu_ViewConf_description = Configure the layout of the selected view

################################################################################
#
# Properties
#

properties_Standard_label = Standard


################################################################################
#
# Views
#

view_NewView_name = NewView

view_Business_name = Business
view_Business_icon = view_business.gif

view_Technical_name = Technical
view_Technical_icon = view_technical.gif

view_Tooltip_description = Workflow View

view_LayoutData_label = View configuration
view_NoLayoutData_label = Some elements in this view have no layout information. Should the view configuration wizard be started now? (Alternatively you can directly import the layout data from view <{0}> which seems to be the best match)
view_NoLayoutData2_label = Some elements in this view have no layout information. Should the view configuration wizard be started now?
view_NoLayoutDataWizard_label = Open wizard
view_NoLayoutDataNothing_label = Do nothing
view_NoLayoutDataDeactivate_label = Do not ask again
view_NoLayoutDataImport_label = Import from <{0}>

################################################################################
#
# Palette
#

palette_ToolsGroup_name = Tools
palette_ActivityElementsGroups_name = Activity Elements
palette_PackageableElementsGroups_name = New Elements

# {0}: Type of entry
# {1}: Name of entry
palette_CreationEntry_description = Add a new {1}
palette_DynamicEntry_description = Add the {1}


################################################################################
#
# Wizards
#

wizards_TemplateWizard_title = Import/Export Workflow Templates
wizards_ModeTemplateWizard_title = Select Import/Export mode
wizards_ImportTemplateWizard_title = Import Workflow Templates
wizards_ExportTemplateWizard_title = Export Workflow Template
wizards_RoleWizard_title = New Role
wizards_DataWizard_title = New Data
wizards_DatatypeWizard_title = New DataType
wizards_AppWizard_title = New Application
wizards_ProcessWizard_title = New Process
wizards_Page_name_package = Select name, package and icon!
wizards_Page_jar = Select JAR-Archive, Java class and Method!
wizards_Page_datatype = Select datatype and value!
wizards_Files_iconfiles = Icon files
wizards_Files_jarfiles = JAR Archive
wizards_Files_templatefiles = Template files
wizards_Files_files = JWT files
wizards_NewPackage = New Package

wizards_ModelWizardPackage_label = Workflow name
wizards_ModelWizardPackage_std = Workflow
wizards_ModelWizardActivity_label = First model
wizards_ModelWizardActivity_std = Diagram1
wizards_ModelWizardActivityComment_std = This is a basic activity
wizards_ModelWizardAuthor_label = Author
wizards_ModelWizardVersion_label = Version
wizards_ModelWizardFile_label = File name
wizards_ModelWizardStandardPackages_label = Create packages for applications, roles and data
wizards_ModelWizardApplicationPackage_std = Applications
wizards_ModelWizardApplicationComment_std = The standard package for applications
wizards_ModelWizardRolePackage_std = Roles
wizards_ModelWizardRoleComment_std = The standard package for roles
wizards_ModelWizardDataPackage_std = Data
wizards_ModelWizardDataComment_std = The standard package for data
wizards_ModelWizardDatatypesComment_std = The standard package for datatypes
wizards_ModelWizardDatatypesPackage_std = Datatypes

wizards_Import_title = Template Import 
wizards_Import_ok = The import of the selected elements was successful. 
wizards_Import_failed = The import of the selected elements failed.
 
wizards_Export_title = Template Export 
wizards_Export_ok = The export of the selected elements to "{0}" was successful. 
wizards_Export_failed = The export of the seleceted elements to "{0}" failed.

wizards_TemplateContents_label = Template content 
wizards_TemplateSelected_label = Selected templates

wizards_Browse_label = &Browse 
wizards_Add_label = &Add 
wizards_Remove_label = &Remove 
wizards_RemoveAll_label = R&emove all
wizards_ModelError_label = < could not load file >
wizards_ImportSelect_label = Select elements to import
wizards_ImportNoSelected_error = No template is selected.
wizards_ExportTemplate_label = Template File:
wizards_ExportComments_label = Export comments?
wizards_ExportSelect_label = Select elements to export
wizards_ExportFilename_error = No file name.
wizards_ExportChecked_error = No element is selected.
wizards_AddDatatype_label = &New Data Type

wizards_PackagDialog_name = Name of the new package:

wizards_IsWebServiceApplication_name = Is Web Service

insertActivity=Insert Activity
choose_jar_title=Please choose a JAR Archive...

wizards_ViewConf_title = View layout configuration
wizards_ViewConfSelect_title = Select the source for the layout data
wizards_ViewConfImport_title = Import layout data from another view
wizards_ViewConfCreate_title = Use a layouting algorithm
wizards_ViewConfActivity_title = Select the activities
wizards_ViewConfPreview_title = Preview
wizards_ViewConfActivity_message = Select the activities which should be affected
wizards_ViewConfImport_message = Select a view for importing the layout data
wizards_ViewConfCreate_message = Select a layout algorithm for creating the layout data

wizards_ViewConfImport_label = Import layout data from another view
wizards_ViewConfCreate_label = Use a layouting algorithm to set layout data
wizards_ViewConfCurrentView_label = Set layout preferences for the view:
wizards_ViewConfCreateSelect_label = Please select a layouting algorithm:
wizards_ViewConfImportSelect_label = Please select a view which will serve as source for the layout information:
wizards_ViewConfImportOnlyNew_label = Import layout data only for elements that currently have no layout information
wizards_ViewConfImportRelevant_label =  (ID {0} , contains {1} relevant elements)
wizards_ViewConfActivityAll_label = Apply to all activities in the workflow
wizards_ViewConfActivitySelected_label = Apply only to selected activities
wizards_ViewConfPreviewSelect_label = Select activity

wizards_ViewConfAlgorithm_HorizontalTree_name = Horizontal Tree
wizards_ViewConfAlgorithm_Tree_name = Tree
wizards_ViewConfAlgorithm_Radial_name = Radial
wizards_ViewConfAlgorithm_Spring_name = Spring
wizards_ViewConfAlgorithm_HorizontalShift_name = Horizontal Shift
wizards_ViewConfAlgorithm_Horizontal_name = Horizontal
wizards_ViewConfAlgorithm_Vertical_name = Vertical
wizards_ViewConfAlgorithm_HorizontalTree_description = A horizontal tree layout.
wizards_ViewConfAlgorithm_Tree_description = A standard tree layout.
wizards_ViewConfAlgorithm_Radial_description = A radial layout.
wizards_ViewConfAlgorithm_Spring_description = A spring layout.
wizards_ViewConfAlgorithm_HorizontalShift_description = Horizontal shift layout.
wizards_ViewConfAlgorithm_Horizontal_description = A horizontal layout.
wizards_ViewConfAlgorithm_Vertical_description = A vertical layout.

################################################################################
#
# Configuration Page
#

configPage_Properties = Properties
configPage_Title = Overview
configPage_Name = Name
configPage_Location = Location
configPage_Author = Author
configPage_VNumber = Version Number
configPage_LastModified = Last Modified
configPage_Documentation = Documentation
configPage_Roles = Roles
configPage_Data = Data
configPage_Application = Applications
configPage_Activity = Processes
configPage_Add = Add
configPage_Delete = Delete
configPage_Edit = Edit


################################################################################
#
# Extension Points
#

extension_point_menu = org.eclipse.jwt.we.menu
extension_point_view = org.eclipse.jwt.we.view
extension_point_propertydescriptor = org.eclipse.jwt.we.propertyDescriptor
extension_point_notifychangedlistener = org.eclipse.jwt.we.changeListener
extension_point_sheet = org.eclipse.jwt.we.sheet
extension_point_registry = org.eclipse.jwt.we.factoryRegistry


################################################################################
#
# Simulator
#

dialog_no_simulator_path_title=No simulator path
dialog_no_simulator_path_message=No simulator path was configured on menu window -> preferences

################################################################################
#
# Dialogs
#
dialog_overwrite_title = Overwrite file?
dialog_overwrite_message = The chosen file already exists. Do you want to overwrite it?

################################################################################
#
# Datatypes
#
Datatypes_URL = URL
Datatypes_dioParameter = dioParameter
Datatypes_qualifier = qualifier
Datatypes_searchquery = searchquery
Datatypes_filename = filename

##########################
#
# Aspects
#
incomplete_jwt_extension_conf_title=Incomplete JWT metamodel extension configuration
incomplete_jwt_extension_conf_details=The workflow file {1} can't be loaded because it refers to unknown metamodel extensions. Your JWT metamodel extension configuration is incomplete, possibly because you are using a wrong packaging of the JWT Workflow Editor. Details : Can't find EMF definition of package "{1}". Its .ecore file should be hooked either in an Eclipse plugin configuration (plugin.xml), or in the workflow model itself.
incorrect_metamodel_extension_definition_title=Incorrect JWT metamodel extension definition"
incorrect_metamodel_extension_definition_details=The workflow file {1} can't be loaded because it refers to unknown types of a metamodel extension. Your JWT metamodel extension configuration is incomplete, possibly because you are using a wrong version of a JWT Workflow Editor extension. Details : Can't find EMF definition of class "{1}" in package "{2}"

_UI_CreateChild_label = {0}
_UI_CreateChild_tooltip = Create New {0} Under {1} Feature
_UI_CreateChild_description = Create a new child of type {0} for the {1} feature of the selected {2}.
_UI_CreateSibling_description = Create a new sibling of type {0} for the selected {2}, under the {1} feature of their parent.
_UI_PropertyDescriptor_description = The {0} of the {1}
_UI_LayoutData_type = Layout Data
_UI_Reference_type = Reference
_UI_ReferenceEdge_type = Reference Edge
_UI_Diagram_type = Diagram
_UI_Unknown_type = Object
_UI_Unknown_datatype= Value
_UI_LayoutData_describesElement_feature = Describes Element
_UI_LayoutData_viewid_feature = Viewid
_UI_LayoutData_width_feature = Width
_UI_LayoutData_height_feature = Height
_UI_LayoutData_x_feature = X
_UI_LayoutData_y_feature = Y
_UI_LayoutData_initialized_feature = Initialized
_UI_Reference_containedIn_feature = Contained In
_UI_Reference_reference_feature = Reference
_UI_Reference_referenceEdges_feature = Reference Edges
_UI_ReferenceEdge_containedIn_feature = Contained In
_UI_ReferenceEdge_reference_feature = Reference
_UI_ReferenceEdge_action_feature = Action
_UI_ReferenceEdge_direction_feature = Direction
_UI_Diagram_describesModel_feature = Describes Model
_UI_Diagram_layoutData_feature = Layout Data
_UI_Diagram_references_feature = References
_UI_Diagram_referenceEdges_feature = Reference Edges
_UI_Unknown_feature = Unspecified
_UI_EdgeDirection_default_literal = default
_UI_EdgeDirection_none_literal = none
_UI_EdgeDirection_in_literal = in
_UI_EdgeDirection_out_literal = out
_UI_EdgeDirection_inout_literal = inout
