/**
 * File:    ConfEditorMessages.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.presentation.i18n;

import org.eclipse.osgi.util.NLS;

/**
 * Conf editor messages.
 * 
 * @version $Id: ConfEditorMessages.java,v 1.2 2009-03-05 19:38:40 mdutoo Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class ConfEditorMessages extends NLS {
	
	private static String BUNDLE_NAME = "org/eclipse/jwt/we/conf/presentation/i18n/messages"; //$NON-NLS-1$
	
	public static String selectRegisteredPackageUri;
	public static String packageSelection;
	public static String browseRegisteredPackages;
	public static String reloadConfModel;
	public static String confModelReloadError;
	public static String unknownErrorWhileReloadingConfModel;

	static {
		NLS.initializeMessages(BUNDLE_NAME, ConfEditorMessages.class);
	}
}
