/**
 * File:    DevtimeToRuntimePackageConverterHelper.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.presentation.resource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;


/**
 * Helps DevtimeToRuntimePackageConverterXmiURIHandlerImpl
 * save EMF package URIs as runtime ones in conf files.
 * 
 * @version $Id: DevtimeToRuntimePackageConverterHelper.java,v 1.1 2009-02-25 14:30:49 mdutoo Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class DevtimeToRuntimePackageConverterHelper {

	/**
	 * Computes and returns all EMF package of the given (ecore) resource.
	 * @param resource
	 * @return a list of EPackage
	 */
    protected static Collection<EPackage> getAllPackages(Resource resource) {
		List<EPackage> result = new ArrayList<EPackage>();
		for (TreeIterator<?> j = new EcoreUtil.ContentTreeIterator<Object>(resource.getContents()) {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<? extends EObject> getEObjectChildren(EObject eObject) {
				return eObject instanceof EPackage ?
						((EPackage) eObject).getESubpackages().iterator() : Collections.<EObject> emptyList().iterator();
			}
		}; j.hasNext();) {

			Object content = j.next();
			if (content instanceof EPackage) {
				result.add((EPackage) content);
			}
		}

		return result;
	}
    

    /**
     * Builds the map of the currently loaded EMF resources' resource package to
     * EMF namespace URI. Used by doSave() to convert devtime "platform:/plugin/*" URIs to
     * runtime package URIs (rather than deresolving them to relative ones) when saving).
     * @author Marc Dutoo
     * @param resources to build the URI map of
     * @return a map of resource package URI to EMF namespace URI.
     */
    public static HashMap<URI,String> buildPackageResourceToNsUriMap(Collection<Resource> resources) {
		// NB. its base URI is set at save time by XMLHelperImpl
		final HashMap<URI,String> packageResourceToNsUriMap = new HashMap<URI,String>();
		for (Resource resource : resources) {
			Collection<EPackage> resourcePackages = getAllPackages(resource); // TODO
			for (EPackage resourcePackage : resourcePackages) {
				URI packageResourceUri = EcoreUtil.getURI(resourcePackage);
				packageResourceToNsUriMap.put(packageResourceUri, resourcePackage.getNsURI());
			}
		}
		return packageResourceToNsUriMap;
    }

}
