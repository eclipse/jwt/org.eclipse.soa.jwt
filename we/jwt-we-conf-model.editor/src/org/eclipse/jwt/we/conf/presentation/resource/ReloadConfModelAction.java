/**
 * File:    ReloadConfModelAction.java
 * Created: 05.03.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.presentation.resource;

import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jwt.we.conf.model.ConfModel;
import org.eclipse.jwt.we.conf.model.resource.ConfModelResourceManager;
import org.eclipse.jwt.we.conf.model.resource.ConfResourceException;
import org.eclipse.jwt.we.conf.presentation.i18n.ConfEditorMessages;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

/**
 * Reloads conf model resource (including unavailability)
 * 
 * @version $Id: ReloadConfModelAction.java,v 1.2 2010-05-10 08:27:13 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class ReloadConfModelAction extends Action {

	protected Shell parent;
	protected EditingDomain domain;

	public ReloadConfModelAction() {
	    super(ConfEditorMessages.reloadConfModel);
	    //setDescription(ConfEditorMessages.reloadConfModelDescription);
	}

	/**
	 * This returns the action's domain.
	 */
	public EditingDomain getEditingDomain() {
		return domain;
	}

	/**
	 * This sets the action's domain.
	 */
	public void setEditingDomain(EditingDomain domain) {
		this.domain = domain;
	}

	public void update() {
		setEnabled(domain != null);
	}

	public void setActiveWorkbenchPart(IWorkbenchPart workbenchPart) {
		setEditingDomain(workbenchPart instanceof IEditingDomainProvider ? ((IEditingDomainProvider) workbenchPart).getEditingDomain() : null);
	}
	  
	  
	@Override
	public void run() {
		IEditorPart activeEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		URI resourceURI = EditUIUtil.getURI(activeEditor.getEditorInput());
		try {
			// Reload the existing conf model through the editing domain.
			ConfModel reloadedConfModel = ConfModelResourceManager.INSTANCE
					.loadConfModel(resourceURI, domain.getResourceSet(), null, false);
			
			// reload from enriched model (including unavailability)
			if (reloadedConfModel != null && reloadedConfModel.getEnrichedModel() != null) {
				reloadedConfModel = ConfModelResourceManager.INSTANCE
						.loadConfModelOfModel(reloadedConfModel.getEnrichedModel(), true);
			}
			
			if (reloadedConfModel == null) {
				String msg = ConfEditorMessages.confModelReloadError;
				ErrorDialog.openError(parent, ConfEditorMessages.unknownErrorWhileReloadingConfModel,
						msg, new Status(Status.ERROR, "org.eclipse.jwt.we.conf.model.editor", Status.OK, msg, null)); //$NON-NLS-1$
			}
			
		} catch (ConfResourceException crex) {
			ErrorDialog.openError(parent, ConfEditorMessages.confModelReloadError, "" //$NON-NLS-1$
					+ crex.getMessage(), new Status(Status.ERROR, "org.eclipse.jwt.we.conf.model.editor", //$NON-NLS-1$
							Status.OK, crex.getMessage(), crex));
			
		} catch (Exception ex) {
			ErrorDialog.openError(parent, ConfEditorMessages.confModelReloadError, "" //$NON-NLS-1$
					+ ex.getMessage(), new Status(Status.ERROR, "org.eclipse.jwt.we.conf.model.editor", //$NON-NLS-1$
							Status.OK, ex.getMessage(), ex));
		}
		
	}
}
