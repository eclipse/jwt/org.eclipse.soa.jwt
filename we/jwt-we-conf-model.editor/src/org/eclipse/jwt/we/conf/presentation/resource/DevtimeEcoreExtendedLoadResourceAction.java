/**
 * File:    DevtimeEcoreExtendedLoadResourceAction.java
 * Created: 10.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide SA 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.presentation.resource;

import org.eclipse.emf.edit.ui.action.LoadResourceAction;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

/**
 * Customization of LoadResourceAction (like the one done in EcoreActionBarContributor)
 * so it provides a button triggering the display of a dialog box allowing
 * to choose and load devtime (plugin) EMF packages.
 * 
 * @version $Id: DevtimeEcoreExtendedLoadResourceAction.java,v 1.3 2009-02-25 14:57:48 mdutoo Exp $
 * @author Marc Dutoo, Open Wide SA
 *
 */
public class DevtimeEcoreExtendedLoadResourceAction extends LoadResourceAction {
	
	@Override
	public void run() {
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		DevtimeEcoreExtendedLoadResourceDialog loadResourceDialog = new DevtimeEcoreExtendedLoadResourceDialog(shell, domain);

		loadResourceDialog.open();
	}
}
