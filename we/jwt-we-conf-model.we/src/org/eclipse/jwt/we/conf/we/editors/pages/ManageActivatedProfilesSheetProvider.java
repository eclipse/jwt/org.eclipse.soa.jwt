/**
 * File:    ManageActivatedProfilesSheetProvider.java
 * Created: 03.12.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.we.editors.pages;

import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.pages.externalSheet.IWEExternalSheetProvider;
import org.eclipse.swt.widgets.Control;

/**
 * Provides a ManageActivatedProfiles editor sheet.
 * 
 * @version $Id: ManageActivatedProfilesSheetProvider.java,v 1.2 2009-01-05 16:35:43 mdutoo Exp $
 * @author Marc Dutoo, Open Wide SA, France <www.openwide.fr>
 */
public class ManageActivatedProfilesSheetProvider
		implements IWEExternalSheetProvider {

	/**
	 * A reference to the parent editor.
	 */
	private WEEditor editor;


	/* (non-Javadoc)
	 * @see org.eclipse.jwt.we.editors.pages.externalSheet.IWEExternalSheetProvider#getExternalSheet()
	 */
	public Control getExternalSheet() {
		return new ManageActivatedProfilesSheet(editor);
	}


	/* (non-Javadoc)
	 * @see org.eclipse.jwt.we.editors.pages.externalSheet.IWEExternalSheetProvider#init(org.eclipse.jwt.we.editors.WEEditor)
	 */
	public void init(WEEditor editor) {
		this.editor = editor;
	}

}
