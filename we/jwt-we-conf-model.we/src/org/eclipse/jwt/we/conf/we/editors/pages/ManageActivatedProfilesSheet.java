/**
 * File:    ManageActivatedProfilesSheet.java
 * Created: 03.12.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.we.editors.pages;

import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jwt.we.conf.edit.ui.ManageActivatedProfilesUI;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Composite;


/**
 * An example for a simple editor sheet.
 * 
 * @version $Id: ManageActivatedProfilesSheet.java,v 1.5 2010-05-10 08:27:19 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA, France <www.openwide.fr>
 */
public class ManageActivatedProfilesSheet
		extends ScrolledComposite {

	/** The wrapping WE Editor */
	//private WEEditor weEditor;

	/** The Manage Profile UI */
	private ManageActivatedProfilesUI profileUi;


	/**
	 * Constructor.
	 * 
	 * @param weEditor The Container
	 */
	public ManageActivatedProfilesSheet(WEEditor weEditor) {
		super(weEditor.getTabFolder(), SWT.NONE | SWT.V_SCROLL | SWT.H_SCROLL);
		//this.weEditor = weEditor;

		setExpandHorizontal(true);
		
		EObject model = (EObject) weEditor.getModel();
		AdapterFactory adapterFactory = weEditor.getAdapterFactory();
		createPage(model, weEditor.getEmfEditingDomain().getCommandStack(), adapterFactory);
		
		//checkMissingInstalledProfiles(model); // obsolete, rather done in ManageProfile UI
	}


	/**
	 * Builds the components of this page.
	 * 
	 */
	protected void createPage(EObject model, CommandStack commandStack, AdapterFactory adapterFactory) {
		profileUi = new ManageActivatedProfilesUI(adapterFactory);
		
		// setting the model
		profileUi.setSelectedModel(model);
		
		// actual UI creation :
		Composite profileUiComposite = profileUi.createControl(this, commandStack);
		profileUi.refresh();
		profileUiComposite.pack();
		this.setContent(profileUiComposite);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.common.notify.Adapter#setTarget(org.eclipse.emf.common.notify.Notifier
	 * )
	 */
	@Override
	public void dispose() {
		profileUi.dispose();
		profileUi = null;
		super.dispose();
	}



	/**
	 * Checking required profiles against installed ones at load time.
	 * @param model
	 * @param weEditor 
	 */
	/*protected void checkMissingInstalledProfiles(EObject model) { 
		// TODO check versions, help migrate old ones
		List<Profile> missingProfiles = AspectManager.INSTANCE.getMissingInstalledProfiles(model);
		if (!missingProfiles.isEmpty()) {
			//ManageActivatedProfileDialog dialog = new ManageActivatedProfileDialog(
			//		(AdapterFactory) this.getAdapter(AdapterFactory.class), (EObject) getModel(), false);
			//dialog.open();
			MessageDialog.openWarning(new Shell(Display.getCurrent(), SWT.APPLICATION_MODAL),
					"Warning : missing profiles", "Missing profiles : \n" + missingProfiles);
			// TODO set as active editor, through extending AbstractEditor
			// TODO better ; enable only sheet if missing ?
		}
		// TODO if has no ConfModel, propose to migrate ; or in ManageActivatedProfilesAction ?
		// TODO handle profile-related load errors, through WE converter & load refactoring
	}*/
}