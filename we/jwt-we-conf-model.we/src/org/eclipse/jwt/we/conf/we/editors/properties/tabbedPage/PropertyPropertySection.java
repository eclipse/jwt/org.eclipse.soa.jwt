/**
 * File:    PropertyPropertySection.java
 * Created: 28.07.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.we.editors.properties.tabbedPage;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jwt.we.conf.model.AspectInstance;
import org.eclipse.jwt.we.editors.properties.CustomAdapterFactoryContentProvider;
import org.eclipse.jwt.we.editors.properties.ModelPropertySourceProvider;
import org.eclipse.jwt.we.editors.properties.singlePage.ModelPropertySheetPage;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.PropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

/**
 * This class represents a section in a property tab
 * (PropertyTabbedPropertySheetPage). The content of the section is the standard
 * ModelPropertySheetPage which displays the properties of the selected model
 * element.
 * 
 * @version $Id: PropertyPropertySection.java,v 1.4 2009/01/05 16:35:43 mdutoo
 *          Exp $
 * @author Marc Dutoo, Open Wide SA, France <www.openwide.fr>
 */
public class PropertyPropertySection extends AbstractPropertySection
{

	/**
	 * The Property Sheet Page.
	 */
	protected PropertySheetPage page;


	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#createControls(org.eclipse.swt.widgets.Composite,
	 *      org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage)
	 */
	public void createControls(Composite parent,
			TabbedPropertySheetPage tabbedPropertySheetPage)
	{
		super.createControls(parent, tabbedPropertySheetPage);
		Composite composite = getWidgetFactory().createFlatFormComposite(parent);
		PropertyTabbedPropertySheetPage propertyTabbedPropertySheetPage = (PropertyTabbedPropertySheetPage) tabbedPropertySheetPage;
		AdapterFactory propertyTabbedPropertySheetPageAf = propertyTabbedPropertySheetPage
				.getAdapterFactory();
		page = new ModelPropertySheetPage(propertyTabbedPropertySheetPageAf);

		page.createControl(composite);
		FormData data = new FormData();
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(100, 0);
		data.top = new FormAttachment(0, 0);
		data.bottom = new FormAttachment(100, 0);
		page.getControl().setLayoutData(data);

		page
				.setPropertySourceProvider(new ModelPropertySourceProvider(
						new CustomAdapterFactoryContentProvider(
								propertyTabbedPropertySheetPageAf)));
	}


	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#setInput(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	public void setInput(IWorkbenchPart part, ISelection selection)
	{
		if (selection instanceof IStructuredSelection)
		{
			Object selectedObject = ((IStructuredSelection) selection).getFirstElement();
			if (selectedObject instanceof AspectInstance)
			{
				super.setInput(part, selection);
				page.selectionChanged(part, selection);
			}
			else
			{
				// TODO possible case of ExtensibleElement being selected
				// (rather than tab selected)
			}
		}

	}


	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#refresh()
	 */
	public void refresh()
	{
		page.refresh();
	}


	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#shouldUseExtraSpace()
	 */
	public boolean shouldUseExtraSpace()
	{
		return true;
	}


	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ISection#dispose()
	 */
	public void dispose()
	{
		super.dispose();

		if (page != null)
		{
			page.dispose();
			page = null;
		}
	}
}