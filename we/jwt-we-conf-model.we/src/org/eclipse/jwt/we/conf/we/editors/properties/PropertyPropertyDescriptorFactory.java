/**
 * File:    PropertyPropertyDescriptorFactory.java
 * Created: 16.11.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.we.editors.properties;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.jwt.we.conf.property.edit.properties.PropertyPropertyDescriptor;
import org.eclipse.jwt.we.editors.properties.extension.PropertyDescriptorFactory;
import org.eclipse.ui.views.properties.IPropertyDescriptor;


/**
 * A factory allowing a "dynamic" (aspect) Property's "value" feature to be
 * displayed and edited in JWT's extensible PropertySheet according to its
 * aspect primitive type. Works by creating a {@link PropertyPropertyDescriptor}.
 * In order for this to work, it must be registered as a JWT
 * PropertyDescriptor extension on said feature.
 * 
 * @version $Id: PropertyPropertyDescriptorFactory.java,v 1.4 2010-05-10 10:04:29 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA, France <www.openwide.fr>
 *
 */
public class PropertyPropertyDescriptorFactory implements
		PropertyDescriptorFactory {

	/**
	 * Returns a new PropertyPropertyDescriptor.
	 */
	public IPropertyDescriptor getPropertyDescriptor(Object object,
			IItemPropertyDescriptor itemPropertyDescriptor) {
		return new PropertyPropertyDescriptor(object, itemPropertyDescriptor);
	}

}
