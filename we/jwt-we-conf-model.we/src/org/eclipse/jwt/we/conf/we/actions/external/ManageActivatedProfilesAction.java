/**
 * File:    ManageActivatedProfilesAction.java
 * Created: 16.10.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2008-2009 Open Wide SA <www.openwide.fr>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc Dutoo, Open Wide, Lyon, France
 *      - creation and implementation
 *******************************************************************************/

package org.eclipse.jwt.we.conf.we.actions.external;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jwt.we.conf.edit.ui.ManageActivatedProfileDialog;
import org.eclipse.jwt.we.editors.WEEditor;
import org.eclipse.jwt.we.editors.actions.external.WEExternalAction;
import org.eclipse.jwt.we.editors.actions.external.WEExternalActionsManager;
import org.eclipse.jwt.we.misc.util.GeneralHelper;


/**
 * Manages activated profiles.
 * 
 * TODO if has no ConfModel, propose to migrate ?
 * 
 * @version $Id: ManageActivatedProfilesAction.java,v 1.5 2010-05-10 08:27:19 chsaad Exp $
 * @author Marc Dutoo, Open Wide SA, France <www.openwide.fr>
 */
public class ManageActivatedProfilesAction extends WEExternalAction {
	
	
	/**
	 * TODO better
	 * Helper allowing to explicitly open the UI
	 * @return
	 */
	public static ManageActivatedProfilesAction reuseDeclaredOrCreateAction() {
		for (WEExternalAction action : WEExternalActionsManager.getInstance().getExternalActions()) {
			if (action instanceof ManageActivatedProfilesAction) {
				return (ManageActivatedProfilesAction) action;
			}
		}
		return new ManageActivatedProfilesAction();
	}
    

	@Override
	public ImageDescriptor getImage() {
		// TODO
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jwt.we.editors.actions.external.WEExternalAction#run()
	 */
	@Override
	public void run() {
		WEEditor weEditor = GeneralHelper.getActiveInstance();
		AdapterFactory adapterFactory = (AdapterFactory) weEditor.getAdapter(AdapterFactory.class);
		ManageActivatedProfileDialog dialog = new ManageActivatedProfileDialog(adapterFactory,
				(EObject) weEditor.getModel(), weEditor.getEmfEditingDomain().getCommandStack(), true);
		dialog.open();
	}

}
