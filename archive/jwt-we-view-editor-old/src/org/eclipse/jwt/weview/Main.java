/*******************************************************************************
 * Copyright (c) 2005-2008
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg - initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg - smaller changes
 *******************************************************************************/

package org.eclipse.jwt.weview;

import org.eclipse.jwt.weview.gui.AppWindow;


/**
 * TODO Description of this type.
 * 
 * @version $Id: Main.java,v 1.4 2008-09-30 07:54:00 flautenba Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class Main
{

	/**
	 * Launches this application
	 */
	public static void main(String[] args)
	{

		String fileName = "", viewName = "";
		if (args.length == 1)
		{
			fileName = args[0];
			viewName = "NewView";
		}
		if (args.length == 2)
		{
			fileName = args[0];
			viewName = args[1];
		}

		AppWindow inst = new AppWindow(fileName, viewName);
		inst.setVisible(true);
	}

}
