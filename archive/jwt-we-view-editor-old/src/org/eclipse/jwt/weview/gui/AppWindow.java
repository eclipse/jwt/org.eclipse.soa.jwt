/*******************************************************************************
 * Copyright (c) 2005-2008
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg - initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg - smaller changes
 *******************************************************************************/

package org.eclipse.jwt.weview.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.eclipse.jwt.we.misc.logging.Level;
import org.eclipse.jwt.we.misc.logging.Logger;
import org.eclipse.jwt.we.misc.views.ViewItemWrapper;
import org.eclipse.jwt.we.misc.views.ViewResourceManager;
import org.eclipse.jwt.we.misc.views.Views;


/**
 * This is as simple as it gets: Access the {@link Views} in jwt-we, parse an ecore file,
 * read out every modelled class and its attributes, select the one, that should be
 * viewed, save it and done. Plain simple and ugly :) TODO Convert to a rcp application /
 * eclipse plugin
 * 
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class AppWindow
		extends javax.swing.JFrame
		implements ListSelectionListener, TableModelListener
{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1466679362035425216L;

	Logger logger = Logger.getLogger(AppWindow.class);
	{

		// Set Look & Feel
		try
		{
			// javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");
			javax.swing.UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		}
		catch (Exception e)
		{
			// e.printStackTrace();
			try
			{
				javax.swing.UIManager.setLookAndFeel("apple.laf.AquaLookAndFeel");
			}
			catch (Exception e1)
			{
				// e1.printStackTrace();
				try
				{
					javax.swing.UIManager
							.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
				}
				catch (Exception e2)
				{
					// e2.printStackTrace();
					try
					{
						javax.swing.UIManager
								.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
					}
					catch (Exception e3)
					{
						e3.printStackTrace();
					}
				}
			}
		}

	}

	private String[] headerItem =
	{ "Name", "Display" };

	private String[] headerAttr =
	{ "Name", "Display" };

	private JMenuItem helpMenuItem;

	private JMenu jMenu5;

	private JMenuItem exitMenuItem;

	private JSeparator jSeparator2;

	private JMenuItem saveAsMenuItem;

	private JMenuItem saveMenuItem;

	private JMenuItem openFileMenuItem;

	private JMenuItem jMenuName;

	private JSeparator jSeparator3;

	private JSeparator jSeparator1;

	private JTable jAttributeTable;

	private JTable jItemTable;

	private JScrollPane jAttributeScrollPane;

	private JScrollPane jItemScrollPane;

	private JSplitPane jSplitPane;

	private JMenuItem newFileMenuItem;

	private JMenu jMenu3;

	private JMenuBar jMenuBar1;

	private List<ViewItemWrapper> itemtabledata = null;

	private List<ViewItemWrapper> attrtabledata = null;

	private File currentfile;


	/**
	 * Standard constructor. Tries, to open normal ecore file. If it cannot find it, a
	 * file chooser dialog is presented to the user.
	 * 
	 */

	public AppWindow(String fileName, String viewName)
	{
		super();
		// it is said that starting the gui with the following arguments should
		// let the gui work on osx. But it didn't
		// System.setProperty("java.awt.headless", "true");
		initGUI();

		if (fileName.length() != 0)
		{

			// add to the given file the .view extension
			if (!fileName.contains(".view"))
			{

				int endIndex = fileName.indexOf(".");

				if (endIndex > 0)
				{
					String subStr = fileName.substring(0, endIndex);
					fileName = subStr.concat(".view");
				}
				else
				{

					fileName = fileName.concat(".view");
				}

			}
			reloadTables();

			currentfile = new File(fileName);
			String projectPath = getPathToProjectFolder();
			String saveString = projectPath.concat("jwt-we/views/" + fileName);

			currentfile = new File(saveString);

			ViewResourceManager.getInstance().setName(viewName);
			ViewResourceManager.getInstance().savetoFile(currentfile);
			String ecorePath = projectPath
					.concat("jwt-we/src/org/eclipse/jwt/we/model/WEMetaModel.ecore");
			ViewResourceManager.getInstance().setPath(ecorePath);
			ViewResourceManager.getInstance().reset();

			saveViewNameInPluginFiles(viewName);

			logger.log(Level.INFO, "New View saved as: " + saveString);
			logger.log(Level.INFO, "And Program terminated.");
			System.exit(10);
		}
		else
		{
			currentfile = new File("");
			String projectPath = getPathToProjectFolder();
			String ecorePath = projectPath
					.concat("jwt-we/src/org/eclipse/jwt/we/model/WEMetaModel.ecore");
			currentfile = new File(ecorePath);
			if (currentfile == null)
			{
				JFileChooser jfc = new JFileChooser();
				jfc.setFileFilter(new ViewsFileFilter());
				if (jfc.showDialog(this, "Select ecore file!") == JFileChooser.APPROVE_OPTION)
				{
					projectPath = jfc.getSelectedFile().getAbsolutePath().replace("\\",
							"/");
				}
			}
			ViewResourceManager.getInstance().setPath(ecorePath);
			ViewResourceManager.getInstance().reset();
			reloadTables();
		}

	}


	/**
	 * Saves a new generated view into the plugin*.properties file
	 * 
	 */
	public void saveViewNameInPluginFiles(String viewName)
	{

		currentfile = new File("jwt_view");
		String projectPath = getPathToProjectFolder();
		File pluginFiles = new File(projectPath + "jwt-we");
		String pluginViewName = "view_" + viewName + "_name";

		String[] propertyFiles = pluginFiles.list();

		Vector<String> changedFiles = new Vector<String>(3);

		for (int i = 0; i < propertyFiles.length; i++)
		{

			if (propertyFiles[i].contains("plugin")
					&& propertyFiles[i].contains(".properties"))
			{
				logger.log(Level.INFO, "propertyFiles[i] " + propertyFiles[i]);
				try
				{
					changedFiles.add(propertyFiles[i]);
					BufferedReader bufRead = new BufferedReader(new FileReader(
							projectPath + "jwt-we/" + propertyFiles[i]));
					BufferedWriter bufWri = new BufferedWriter(new FileWriter(projectPath
							+ "jwt-we/" + propertyFiles[i] + "1"));

					// String that holds current file line
					String line = null;

					// Read first line
					line = bufRead.readLine();
					bufWri.append(line);
					bufWri.newLine();

					// Read through file one line at time. Print line # and line
					while (line != null)
					{

						if (line.contains("# Views"))
						{

							while (line != null)
							{
								line = bufRead.readLine();
								if (line.contains(pluginViewName))
								{
									logger.log(Level.INFO, pluginViewName
											+ " already exits in " + propertyFiles[i]
											+ " File.");
									bufWri.append(line);
									bufWri.newLine();
									break;
								}

								if (line
										.contains("###############################################################################"))
								{
									logger.log(Level.INFO, pluginViewName
											+ " not exits in " + propertyFiles[i]
											+ " File.");

									bufWri.append(pluginViewName + " = " + viewName);
									bufWri.newLine();
									bufWri.append(line);
									bufWri.newLine();

									break;
								}
								bufWri.append(line);
								bufWri.newLine();

							}
						}

						line = bufRead.readLine();
						bufWri.append(line);
						bufWri.newLine();
					}

					bufRead.close();
					bufWri.close();

				}
				catch (FileNotFoundException e)
				{
					e.printStackTrace();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		for (Enumeration<String> e = changedFiles.elements(); e.hasMoreElements();)
		{
			String ts = e.nextElement();
			File orgFile = new File(projectPath + "jwt-we/" + ts);
			if (orgFile.exists())
			{
				if (!orgFile.delete())
				{
					logger.log(Level.WARNING, "File " + ts + " can not deleted!");
				}

			}
			File editedFile = new File(projectPath + "jwt-we/" + ts + "1");
			if (!editedFile.renameTo(new File(projectPath + "jwt-we/" + ts)))
			{
				logger.log(Level.WARNING, "File " + ts + " can not renamed!");
			}
		}

	}


	/**
	 * @return String to your project path
	 */
	private String getPathToProjectFolder()
	{

		String absolutePath = currentfile.getAbsolutePath().replace("\\", "/");
		int beginIndex = absolutePath.toLowerCase().indexOf("jwt-view");
		String projectPath = absolutePath.substring(0, beginIndex);
		logger.log(Level.INFO, "projectPath: " + projectPath);
		return projectPath;
	}


	/**
	 * Initialises the gui
	 * 
	 */
	private void initGUI()
	{
		try
		{
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.rowWeights = new double[]
			{ 0.1 };
			thisLayout.rowHeights = new int[]
			{ 7 };
			thisLayout.columnWeights = new double[]
			{ 0.1 };
			thisLayout.columnWidths = new int[]
			{ 7 };
			getContentPane().setLayout(thisLayout);
			{
				jSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
				getContentPane().add(
						jSplitPane,
						new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH,
								new Insets(0, 0, 0, 0), 0, 0));
				{
					jItemTable = new JTable();
					jItemTable.getSelectionModel().addListSelectionListener(this);
					jItemScrollPane = new JScrollPane(jItemTable);
					jItemScrollPane.setPreferredSize(new java.awt.Dimension(500, 500));
					jSplitPane.add(jItemScrollPane, JSplitPane.TOP);
				}
				{
					jAttributeTable = new JTable();
					jAttributeScrollPane = new JScrollPane(jAttributeTable);
					jSplitPane.add(jAttributeScrollPane, JSplitPane.BOTTOM);
				}
			}
			setSize(800, 600);
			{
				jMenuBar1 = new JMenuBar();
				setJMenuBar(jMenuBar1);
				{
					jMenu3 = new JMenu();
					jMenuBar1.add(jMenu3);
					jMenu3.setText("File");
					{
						newFileMenuItem = new JMenuItem();
						jMenu3.add(newFileMenuItem);
						newFileMenuItem.setText("New");
						newFileMenuItem.addActionListener(new ActionListener()
						{

							public void actionPerformed(ActionEvent evt)
							{
								newFileMenuItemActionPerformed(evt);
							}
						});
					}
					{
						jSeparator1 = new JSeparator();
						jMenu3.add(jSeparator1);
					}
					{
						openFileMenuItem = new JMenuItem();
						jMenu3.add(openFileMenuItem);
						openFileMenuItem.setText("Open");
						openFileMenuItem.addActionListener(new ActionListener()
						{

							public void actionPerformed(ActionEvent evt)
							{
								openFileMenuItemActionPerformed(evt);
							}
						});
					}
					{
						saveMenuItem = new JMenuItem();
						jMenu3.add(saveMenuItem);
						saveMenuItem.setText("Save");
						saveMenuItem.addActionListener(new ActionListener()
						{

							public void actionPerformed(ActionEvent evt)
							{
								saveMenuItemActionPerformed(evt);
							}
						});
					}
					{
						saveAsMenuItem = new JMenuItem();
						jMenu3.add(saveAsMenuItem);
						saveAsMenuItem.setText("Save As ...");
						saveAsMenuItem.addActionListener(new ActionListener()
						{

							public void actionPerformed(ActionEvent evt)
							{
								saveAsMenuItemActionPerformed(evt);
							}
						});
					}
					{
						jSeparator3 = new JSeparator();
						jMenu3.add(jSeparator3);
					}
					{
						jMenuName = new JMenuItem();
						jMenu3.add(jMenuName);
						jMenuName.setText("Set Name...");
						jMenuName.addActionListener(new ActionListener()
						{

							public void actionPerformed(ActionEvent evt)
							{
								jMenuNameActionPerformed(evt);
							}
						});
					}
					{
						jSeparator2 = new JSeparator();
						jMenu3.add(jSeparator2);
					}
					{
						exitMenuItem = new JMenuItem();
						jMenu3.add(exitMenuItem);
						exitMenuItem.setText("Exit");
						exitMenuItem.addActionListener(new ActionListener()
						{

							public void actionPerformed(ActionEvent evt)
							{
								exitMenuItemActionPerformed(evt);
							}
						});
					}
				}
				{
					jMenu5 = new JMenu();
					jMenuBar1.add(jMenu5);
					jMenu5.setText("Help");
					{
						helpMenuItem = new JMenuItem();
						jMenu5.add(helpMenuItem);
						helpMenuItem.setText("Help");
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	/**
	 * Simply reload the upper table (the one with the EClasses).
	 * 
	 */
	private void reloadTables()
	{
		itemtabledata = ViewResourceManager.getInstance().getItemsList();
		Object[][] data = new Object[itemtabledata.size()][2];
		boolean[] editable = new boolean[2];
		editable[0] = false;
		editable[1] = true;
		for (int i = 0; i < itemtabledata.size(); i++)
		{
			data[i][0] = itemtabledata.get(i);
			boolean value = itemtabledata.get(i).getValue();
			data[i][1] = value;
		}
		TableModel tm = new TableModel(headerItem, data, editable);
		tm.addTableModelListener(this);
		jItemTable.setModel(tm);
	}


	/**
	 * Reloads the table at the bottom. That one contains all the EAttribute.
	 * 
	 * @param objlist
	 */
	private void reloadAttrTables(List<ViewItemWrapper> objlist)
	{
		attrtabledata = objlist;
		Object[][] data = null;
		boolean[] editable = null;
		if (objlist != null)
		{
			data = new Object[attrtabledata.size()][2];
			editable = new boolean[2];
			editable[0] = false;
			editable[1] = true;
			for (int i = 0; i < attrtabledata.size(); i++)
			{
				data[i][0] = attrtabledata.get(i);
				data[i][1] = attrtabledata.get(i).getValue();
			}

		}
		else
		{
			data = new Object[0][2];
			editable = new boolean[2];

		}
		TableModel tm = new TableModel(headerAttr, data, editable);
		tm.addTableModelListener(this);
		jAttributeTable.setModel(tm);
	}


	/**
	 * If the user clicks "new" in the File menu, everything (the
	 * {@link ViewResourceManager} and both tables) will be resetted
	 * 
	 * @param evt
	 */
	private void newFileMenuItemActionPerformed(ActionEvent evt)
	{
		ViewResourceManager.getInstance().reset();
		reloadTables();
		reloadAttrTables(null);
	}


	/**
	 * That one is called on changing the checkbox of one of the items in the upper table.
	 * On doing so a list with all Attributes, belonging to the changed EClass will be
	 * fetched from the {@link ViewResourceManager} and the table at the bottom will be
	 * refreshed
	 */
	public void valueChanged(ListSelectionEvent e)
	{
		if (e.getValueIsAdjusting())
			return;
		int selectedRow = jItemTable.getSelectedRow();
		if (selectedRow >= 0)
		{
			ViewItemWrapper obj = itemtabledata.get(selectedRow);
			List<ViewItemWrapper> attributes = ViewResourceManager.getInstance()
					.getAttributesList(obj);
			reloadAttrTables(attributes);
		}

	}


	/**
	 * If clicking the "Save as" menuitem, a filechooser dialog will pop up and let the
	 * user save the file.
	 * 
	 * @param evt
	 */
	private void saveAsMenuItemActionPerformed(ActionEvent evt)
	{
		saveAs();
	}


	/**
	 * If the Attributes Table changes (i.e. one of its items) the changes will get saved
	 * in the {@link ViewResourceManager}
	 */
	public void tableChanged(TableModelEvent e)
	{
		TableModel tm = (TableModel) e.getSource();
		Object[][] data = tm.getData();
		ViewItemWrapper viw = (ViewItemWrapper) data[e.getFirstRow()][0];
		boolean value = (Boolean) data[e.getFirstRow()][1];
		ViewResourceManager.getInstance().setDisplay(viw, value);
	}


	/**
	 * If clicking on "Open", a FileChooser dialog will pop up and the user can select a
	 * views file.
	 * 
	 * @param evt
	 */
	private void openFileMenuItemActionPerformed(ActionEvent evt)
	{
		JFileChooser jfc = new JFileChooser();
		if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
		{
			currentfile = jfc.getSelectedFile();
			ViewResourceManager.getInstance().openFile(currentfile);
			reloadTables();
			reloadAttrTables(null);
		}
	}


	/**
	 * On clicking "save" the current view will get saved (only, if it has been saved /
	 * opened before from an existing file)
	 * 
	 * @param evt
	 */
	private void saveMenuItemActionPerformed(ActionEvent evt)
	{
		if ((currentfile == null) || (currentfile.equals("")))
		{
			saveAs();
		}
		else
		{
			ViewResourceManager.getInstance().savetoFile(currentfile);
		}
	}


	/**
	 * Simply exit the application
	 * 
	 * @param evt
	 */
	private void exitMenuItemActionPerformed(ActionEvent evt)
	{
		System.exit(0);
	}


	/**
	 * On clicking on "SetName" a little dialog will pop up, where the user can enter the
	 * name of this view.
	 * 
	 * @param evt
	 */
	private void jMenuNameActionPerformed(ActionEvent evt)
	{
		setName();
	}


	/**
	 * Presents a little window to the user for entering the name of this view
	 * 
	 */
	private void setName()
	{
		String s = (String) JOptionPane.showInputDialog(this, "Change the name:",
				ViewResourceManager.getInstance().getName());
		ViewResourceManager.getInstance().setName(s);
	}


	/**
	 * Lets the user save a view to a specified location. If the name was not set, the
	 * user will be prompted to enter this views name.
	 * 
	 */
	private void saveAs()
	{
		String viewname = ViewResourceManager.getInstance().getName();
		if ((viewname == null) || (viewname.equals("")))
		{
			setName();
		}
		JFileChooser jfc = new JFileChooser();
		if (jfc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
		{
			currentfile = jfc.getSelectedFile();
			String filename = currentfile.getAbsolutePath();
			String ext = ViewResourceManager.VIEWEXTENSION;
			if (!filename.endsWith(ext))
				filename += "." + ext;
			ViewResourceManager.getInstance().savetoFile(new File(filename));
		}
	}

}
