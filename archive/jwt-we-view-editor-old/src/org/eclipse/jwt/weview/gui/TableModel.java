/*******************************************************************************
 * Copyright (c) 2005-2008
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg - initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg - smaller changes
 *******************************************************************************/


package org.eclipse.jwt.weview.gui;

import javax.swing.table.AbstractTableModel;


/**
 * An table model class for creating a new table. Contains a name on the left column and a
 * checkbox on the right.
 * 
 * @version $Id: TableModel.java,v 1.4 2008-09-30 07:54:00 flautenba Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class TableModel
		extends AbstractTableModel
{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3166414330865715897L;

	private Object[][] data = null;
	private String[] column = null;
	private boolean[] editmatrix = null;


	public TableModel(String[] column, Object[][] data, boolean[] editable)
	{
		this.column = column;
		this.data = data;
		editmatrix = editable;
	}


	public int getColumnCount()
	{
		return column.length;
	}


	public int getRowCount()
	{
		return data.length;
	}


	public Object getValueAt(int rowIndex, int columnIndex)
	{
		return data[rowIndex][columnIndex];
	}


	public boolean isCellEditable(int row, int col)
	{
		return editmatrix[col];
	}


	public String getColumnName(int col)
	{
		return column[col];
	}


	@SuppressWarnings("unchecked")
	public Class getColumnClass(int col)
	{
		return getValueAt(0, col).getClass();
	}


	public void setValueAt(Object value, int row, int col)
	{
		data[row][col] = value;
		fireTableCellUpdated(row, col);
	}


	public Object[][] getData()
	{
		return data;
	}
}
