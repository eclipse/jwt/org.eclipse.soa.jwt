/*******************************************************************************
 * Copyright (c) 2005-2008
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg - initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg - smaller changes
 *******************************************************************************/


package org.eclipse.jwt.weview.gui;

import java.io.File;

import javax.swing.filechooser.FileFilter;


/**
 * Simple FileFilter class
 * 
 * @version $Id: ViewsFileFilter.java,v 1.4 2008-09-30 07:54:00 flautenba Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class ViewsFileFilter
		extends FileFilter
{

	private String FILE_EXT = "ecore";


	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.filechooser.FileFilter#accept(java.io.File)
	 */
	@Override
	public boolean accept(File f)
	{
		if (f.isDirectory())
			return true;
		if (f.getAbsoluteFile().getName().endsWith(FILE_EXT))
			return true;
		return false;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.filechooser.FileFilter#getDescription()
	 */
	@Override
	public String getDescription()
	{
		return FILE_EXT;
	}

}
