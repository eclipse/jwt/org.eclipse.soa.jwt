/**
 * File:    JobActionProcessor.java
 * Created: 06.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *    Guillaume Decarnin, Open Wide, Lyon, France
 *      - Correction
 *******************************************************************************/

package org.eclipse.jwt.runtime.taskengine;

import org.eclipse.jwt.runtime.taskengine.interfaces.DataMappingService;
import org.eclipse.jwt.runtime.taskengine.interfaces.ProcessModelService;
import org.eclipse.jwt.runtime.taskengine.interfaces.ProcessStateService;

public abstract class JobActionProcessor {
	
	protected ProcessModelService processModelService;
	protected ProcessStateService processStateService;
	protected DataMappingService dataMappingService;
	
	public JobActionProcessor(ProcessModelService pms, ProcessStateService pss, DataMappingService dataMappingService) {
		this.processModelService = pms;
		this.processStateService = pss;
		this.dataMappingService = dataMappingService;
	}
	
	/**
	 * Is this JobActionProcessor applicable to the current action?
	 * @return
	 */
	public abstract boolean isApplicable();
	
	/**
	 * Process an automated action 
	 * @throws Exception
	 */
	public abstract void process() throws Exception;
}
