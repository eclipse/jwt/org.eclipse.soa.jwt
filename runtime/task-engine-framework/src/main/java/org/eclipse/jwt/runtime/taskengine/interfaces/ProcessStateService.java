/**
 * File:    ProcessStateService.java
 * Created: 06.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/

package org.eclipse.jwt.runtime.taskengine.interfaces;

public interface ProcessStateService {
	public Object getVariableValue(String propertyName) throws NoSuchPropertyException;
	
	public void setVariableValue(String variableName, Object value) throws NoSuchPropertyException;
}
