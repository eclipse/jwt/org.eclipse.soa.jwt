/**
 * File:    ProcessModelService.java
 * Created: 06.02.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2009  Open Wide (www.openwide.fr)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Mickael Istria, Open Wide, Lyon, France
 *      - Creation and implementation
 *******************************************************************************/

package org.eclipse.jwt.runtime.taskengine.interfaces;

import java.io.InputStream;



/**
 * PMS
 * 
 * Should be impl'ble on top of any kind of JWT-compatible model provider :
 * JWT EMF Java model, JWT .workflow XML file, XPDL 1.0 Bonita file,
 * workflow engine-provided process definition service...
 * 
 * Provides all info required at runtime to run processes, besides the workflow
 * state (i.e. current step and workflow variables).
 * 
 * Such info belongs to one of these use cases : 
 * 1. Get data mapping conf ; based on JWT datamapping concept
 * 2. Get any prop used by action impls (apps, hooks...) to conf their runtime behaviour ; mainly on Application object, may be in base model or extensions (aspects)
 * 3. Default var values (NOT YET opt, not that useful)
 * 
 * @author mdutoo
 * 
 */
public interface ProcessModelService {
	
	/**
	 * Returns the value of any action - task impl (application, hook...)-specific property.
	 * @param modelName name of process
	 * @param modelElementPath path of action - task tree (including within suprocesses),
	 * consisting in their slash-prefixed names
	 * @param propertyName
	 * @return value with the right Java type (some low-end impls might only return strings)
	 * @throws NoSuchModelElementException if can't find target action - task
	 * @throws NoSuchPropertyException if can't find property on it
	 */
	public Object getPropertyValue(String modelName, String modelElementPath, String propertyName)
		throws NoSuchModelElementException, NoSuchPropertyException;
		
	/**
	 * NOT ALWAYS IMPLEMENTED !!! Only available when the current context
	 * can be guessed.
	 * Returns a property from model guessing the current model element
	 * from the Action passed into a constructor
	 * @param propertyName the name of the model property to get
	 * @return the Value of a property
	 * @throws NoSuchModelElementException if can't find target action - task
	 * @throws NoSuchPropertyException if can't find property on it
	 */
	public Object getCurrentModelPropertyValue(String propertyName)
			throws NoSuchModelElementException, NoSuchPropertyException;
	
	/**
	 * Returns an input stream for specified resource
	 * @param resourceName The name of the resource to look for
	 * @return The resource input stream
	 * @throws NoSuchModelElementException if resource can not be found in model
	 */
	public InputStream getResource(String resourceName) throws NoSuchModelElementException;
}
