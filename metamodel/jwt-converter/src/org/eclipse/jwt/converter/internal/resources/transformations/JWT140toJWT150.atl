-- @path jwtSource=/converter-transformations/metamodels/jwt06.ecore
-- @path jwtTarget=/converter-transformations/metamodels/jwt07_metamodel.ecore

-- Copyright (c) 2005-2012 University of Augsburg
-- 
-- All rights reserved. This program and the accompanying materials
-- are made available under the terms of the Eclipse Public License 1.0
-- which accompanies this distribution, and is available at
-- http://www.eclipse.org/legal/epl-v10.html
--
-- Contributors:
--    Florian Lautenbacher - initial API and implementation
-- 	  Chris Saad - helped to improve the transformation


module JWT140toJWT150;
create OUT : jwtTarget, VIEW : jwtTargetView from IN : jwtSource;


-- helpers

helper context jwtSource!"model::core::NamedElement" def: getName():String=
	if self.name.oclIsUndefined()
	then
		'Undefined'
	else
		self.name
	endif;


helper context jwtSource!"model::core::Package" def : isMainPackage() : Boolean =
	self.refImmediateComposite().oclIsUndefined();
	-- refImmediateComposite() is a reflective operation that returns the immediate composite (e.g. the immediate container) of self
	-- So if there is no immediate composite then the current node is the root (we suppose in our example that there is only one root).


helper context jwtSource!"model::core::Package" def : getAllChildren () : OrderedSet(jwtSource!"model::core::NamedElement") =
	self.elements->iterate( child ; elements : OrderedSet(jwtSource!"model::core::NamedElement")  = 
		OrderedSet{} | 
		if child.oclIsTypeOf(jwtSource!"model::core::Package") then
			elements.union(child.getAllChildren ()) -- NODE : recursive call
		else
				if child.oclIsTypeOf(jwtSource!"model::processes::Scope") then
				   child.Activity2Activity
				else
					elements.append(child) -- LEAF					
				endif
		endif
		)
	;

	
helper context jwtSource!"model::core::Package" def : getAllSubPackages () : OrderedSet(jwtSource!"model::core::Package") =
	self.subpackages->iterate( child ; elements : OrderedSet(jwtSource!"model::core::Package") = 
		OrderedSet{} | 
		if child.oclIsTypeOf(jwtSource!"model::core::Package") then
			self.subpackages.union(child.getAllSubPackages ()) -- NODE : recursive call
		else
			self.subpackages.append(child) -- LEAF
		endif
		)
	;

helper context jwtSource!"model::processes::Scope" def : getActivityNodes () : OrderedSet(jwtSource!"model::process::ActivityNode") =
	self.nodes->iterate( child ; elements : OrderedSet(jwtSource!"model::processes::ActivityNode") = 
		OrderedSet{} | 
			self.nodes.append(child) 
		)
	;

helper context jwtSource!"model::processes::Scope" def : getActivityEdges () : OrderedSet(jwtSource!"model::process::ActivityEdge") =
	self.edges->iterate( child ; elements : OrderedSet(jwtSource!"model::processes::ActivityEdge") = 
		OrderedSet{} | 
			self.edges.append(child) 
		)
	;

helper context jwtSource!"model::processes::Action" def : getInputs () : OrderedSet(jwtSource!"model::data::Data") =
	self.inputs->iterate( child ; elements : OrderedSet(jwtSource!"model::data::Data") = 
		OrderedSet{} | 
			self.inputs.append(child) 
		)
	;

helper context jwtSource!"model::processes::Action" def : getOutputs () : OrderedSet(jwtSource!"model::data::Data") =
	self.outputs->iterate( child ; elements : OrderedSet(jwtSource!"model::data::Data") = 
		OrderedSet{} | 
			self.outputs.append(child) 
		)
	;

helper context jwtSource!"model::processes::Reference" def : getReferenceEdges () : OrderedSet(jwtSource!"model::processes::ReferenceEdge") =
	self.referenceEdges->iterate( child ; elements : OrderedSet(jwtSource!"model::processes::ReferenceEdge") = 
		OrderedSet{} | 
			self.referenceEdges.append(child) 
		)
	;

helper context jwtSource!"model::processes::GuardSpecification" def : getSubSpecification () : OrderedSet(jwtSource!"model::processes::GuardSpecification") =
	self.subSpecification->iterate( child ; elements : OrderedSet(jwtSource!"model::processes::GuardSpecification") = 
		OrderedSet{} | 
			self.subSpecification.append(child) 
		)
	;

helper context jwtSource!"model::processes::Reference" def : getReferencesScope () : jwtSource!"model::processes::Scope" =
	let scopes : jwtSource!"model::processes::Scope" = jwtSource!"model::processes::Scope".allInstances()->asOrderedSet()
		in 
			scopes->select(scope | scope.references->includes(self))->first()
	;

helper context jwtSource!"model::processes::ReferenceEdge" def : getReferenceEdgeScope () : jwtSource!"model::processes::Scope" =
	let scopes : jwtSource!"model::processes::Scope" = jwtSource!"model::processes::Scope".allInstances()->asOrderedSet()
		in 
			scopes->select(scope | scope.references->includes(self))->first()	
	;

helper context jwtSource!"model::data::Data" def : getParameters () : OrderedSet(jwtSource!"model::data::Parameter") =
	self.parameters->iterate( child ; elements : OrderedSet(jwtSource!"model::data::Parameter") = 
		OrderedSet{} | 
			self.parameters.append(child) 
		)
	;

-- rules

-- package CORE

rule Model2Model {
	from input : jwtSource!"model::core::Package"
(input.oclIsTypeOf(jwtSource!"model::core::Model"))
	to 
	model : jwtTarget!Model (
		name <- input.name,
		author <- input.author,
		version <- input.version, 
		description <- input.description,
		--for new fileversion change here:
		--e.g. 
		fileversion <- '1.5.0',
		subpackages <- input.getAllSubPackages(),
		elements <- input.getAllChildren()
	),
	output2 : jwtTargetView!Diagram (
		referenceEdges <- jwtSource!"model::processes::ReferenceEdge".allInstances() -> collect(e |
			thisModule.resolveTemp(e, 'refEdge')),
		references <- jwtSource!"model::processes::Reference".allInstances() -> collect(e |
			thisModule.resolveTemp(e, 'ref')),
		describesModel <- model,
		layoutData <- jwtSource!"model::processes::ActivityNode".allInstances() -> collect(e |
			thisModule.resolveTemp(e, 'layoutData')),
		layoutData <- jwtSource!"model::processes::Reference".allInstances() -> collect(e |
			thisModule.resolveTemp(e, 'layoutData'))
	)
}


rule Package2Package {
	from
		input : jwtSource!"model::core::Package" (not input.oclIsTypeOf(jwtSource!"model::core::Model")) 
	to
		output : jwtTarget!Package (
			name <- input.name,
			icon <- input.icon,
			elements <- input.getAllChildren(),
			subpackages <- input.getAllSubPackages(),
			ownedComment <- input.ownedComment
		)
}

rule Comment2Comment {
	from input : jwtSource!"model::core::Comment"
	to output : jwtTarget!Comment (
		text <- input.text
	)
}


-- package PROCESSES
rule Activity2Activity {
	from input : jwtSource!"model::processes::Activity"
	to output : jwtTarget!Activity (
		name <- input.name,
		nodes <- input.getActivityNodes(),
		edges <- input.getActivityEdges(),
		ownedComment <- input.ownedComment
	)
}

rule StructuredActivityNode2StructuredActivityNode {
	from input : jwtSource!"model::processes::StructuredActivityNode"
	to output : jwtTarget!StructuredActivityNode (
		name <- input.name,
		nodes <- input.getActivityNodes(),
		edges <- input.getActivityEdges(),
		ownedComment <- input.ownedComment
	)
} 

rule ActivityEdge2ActivityEdge {
	from input : jwtSource!"model::processes::ActivityEdge"
	to output : jwtTarget!ActivityEdge (
		source <- input.source,
		target <- input.target,
		guard <- input.guard
	)
}

rule Guard2Guard {
	from input : jwtSource!"model::processes::Guard"
	to output : jwtTarget!Guard (
		shortdescription <- input.shortdescription,
		textualdescription <- input.textualdescription,
		detailedSpecification <- input.detailedSpecification
	)
}

rule GuardSpecification2GuardSpecification {
	from input : jwtSource!"model::processes::GuardSpecification"
	to output : jwtTarget!GuardSpecification (
		operation <- input.operation,
		subSpecificationConnector <- input.subSpecificationConnector,
		value <- input.value,
		attribute <- input.attribute,
		data <- input.data,
		Description <- input.Description,
		subSpecification <- input.getSubSpecification()
	)
}



rule Action2Action {
	from input : jwtSource!"model::processes::Action"
	to action : jwtTarget!Action (
		name <- input.name, 
		icon <- input.icon,
		targetexecutiontime <- input.targetexecutiontime,
		realizes <- input.realizes, --Function
		performedBy <- input.performedBy, --Role
		executedBy <- input.executedBy, --Application
		inputs <- input.getInputs(), --Data
		outputs <- input.getOutputs()
	),
	layoutData : jwtTargetView!LayoutData (
		initialized <- true,
		viewid <- 'Technical',
		describesElement <- action,
		height <- if not input.Size.oclIsUndefined() then
					if  not input.Size.height.oclIsUndefined() then
						input.Size.height
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		width <- if not input.Size.oclIsUndefined() then
					if  not input.Size.width.oclIsUndefined() then
						input.Size.width
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		x <- if not input.Location.oclIsUndefined() then
					if  not input.Location.x.oclIsUndefined() then
						input.Location.x
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		y <- if not input.Location.oclIsUndefined() then
					if  not input.Location.y.oclIsUndefined() then
						input.Location.y
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif						
	)
	
}

rule ActivityLinkNode2ActivityLinkNode {
	from input : jwtSource!"model::processes::ActivityLinkNode"
	to activitylinknode : jwtTarget!ActivityLinkNode (
		name <- input.name,
		icon <- input.icon,
		linksto <- input.linksto
	),
	layoutData : jwtTargetView!LayoutData (
		initialized <- true,
		viewid <- 'Technical',
		describesElement <- activitylinknode,
		height <- if not input.Size.oclIsUndefined() then
					if  not input.Size.height.oclIsUndefined() then
						input.Size.height
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		width <- if not input.Size.oclIsUndefined() then
					if  not input.Size.width.oclIsUndefined() then
						input.Size.width
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		x <- if not input.Location.oclIsUndefined() then
					if  not input.Location.x.oclIsUndefined() then
						input.Location.x
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		y <- if not input.Location.oclIsUndefined() then
					if  not input.Location.y.oclIsUndefined() then
						input.Location.y
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif						
	)
}


rule InitialNode2InitialNode {
	from input : jwtSource!"model::processes::InitialNode"
	to initialnode : jwtTarget!InitialNode (
		name <- input.name,
		icon <- input.icon
	),
		layoutData : jwtTargetView!LayoutData (
		initialized <- true,
		viewid <- 'Technical',
		describesElement <- initialnode,
		height <- if not input.Size.oclIsUndefined() then
					if  not input.Size.height.oclIsUndefined() then
						input.Size.height
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		width <- if not input.Size.oclIsUndefined() then
					if  not input.Size.width.oclIsUndefined() then
						input.Size.width
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		x <- if not input.Location.oclIsUndefined() then
					if  not input.Location.x.oclIsUndefined() then
						input.Location.x
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		y <- if not input.Location.oclIsUndefined() then
					if  not input.Location.y.oclIsUndefined() then
						input.Location.y
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif						
	)
}

rule FinalNode2FinalNode {
	from input : jwtSource!"model::processes::FinalNode"
	to finalnode : jwtTarget!FinalNode (
		name <- input.name,
		icon <- input.icon
	),
		layoutData : jwtTargetView!LayoutData (
		initialized <- true,
		viewid <- 'Technical',
		describesElement <- finalnode,
		height <- if not input.Size.oclIsUndefined() then
					if  not input.Size.height.oclIsUndefined() then
						input.Size.height
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		width <- if not input.Size.oclIsUndefined() then
					if  not input.Size.width.oclIsUndefined() then
						input.Size.width
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		x <- if not input.Location.oclIsUndefined() then
					if  not input.Location.x.oclIsUndefined() then
						input.Location.x
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		y <- if not input.Location.oclIsUndefined() then
					if  not input.Location.y.oclIsUndefined() then
						input.Location.y
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif						
	)
}

rule DecisionNode2DecisionNode {
	from input : jwtSource!"model::processes::DecisionNode"
	to decisionnode: jwtTarget!DecisionNode (
		name <- input.name,
		icon <- input.icon
	),
		layoutData : jwtTargetView!LayoutData (
		initialized <- true,
		viewid <- 'Technical',
		describesElement <- decisionnode,
		height <- if not input.Size.oclIsUndefined() then
					if  not input.Size.height.oclIsUndefined() then
						input.Size.height
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		width <- if not input.Size.oclIsUndefined() then
					if  not input.Size.width.oclIsUndefined() then
						input.Size.width
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		x <- if not input.Location.oclIsUndefined() then
					if  not input.Location.x.oclIsUndefined() then
						input.Location.x
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		y <- if not input.Location.oclIsUndefined() then
					if  not input.Location.y.oclIsUndefined() then
						input.Location.y
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif						
	)
}

rule MergeNode2MergeNode {
	from input : jwtSource!"model::processes::MergeNode"
	to mergenode : jwtTarget!MergeNode (
		name <- input.name,
		icon <- input.icon
	),
		layoutData : jwtTargetView!LayoutData (
		initialized <- true,
		viewid <- 'Technical',
		describesElement <- mergenode,
		height <- if not input.Size.oclIsUndefined() then
					if  not input.Size.height.oclIsUndefined() then
						input.Size.height
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		width <- if not input.Size.oclIsUndefined() then
					if  not input.Size.width.oclIsUndefined() then
						input.Size.width
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		x <- if not input.Location.oclIsUndefined() then
					if  not input.Location.x.oclIsUndefined() then
						input.Location.x
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		y <- if not input.Location.oclIsUndefined() then
					if  not input.Location.y.oclIsUndefined() then
						input.Location.y
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif						
	)
}

rule ForkNode2ForkNode {
	from input : jwtSource!"model::processes::ForkNode"
	to forknode : jwtTarget!ForkNode (
		name <- input.name,
		icon <- input.icon
	),
		layoutData : jwtTargetView!LayoutData (
		initialized <- true,
		viewid <- 'Technical',
		describesElement <- forknode,
		height <- if not input.Size.oclIsUndefined() then
					if  not input.Size.height.oclIsUndefined() then
						input.Size.height
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		width <- if not input.Size.oclIsUndefined() then
					if  not input.Size.width.oclIsUndefined() then
						input.Size.width
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		x <- if not input.Location.oclIsUndefined() then
					if  not input.Location.x.oclIsUndefined() then
						input.Location.x
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		y <- if not input.Location.oclIsUndefined() then
					if  not input.Location.y.oclIsUndefined() then
						input.Location.y
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif						
	)
}

rule JoinNode2JoinNode {
	from input : jwtSource!"model::processes::JoinNode"
	to joinnode : jwtTarget!JoinNode (
		name <- input.name,
		icon <- input.icon
	),
		layoutData : jwtTargetView!LayoutData (
		initialized <- true,
		viewid <- 'Technical',
		describesElement <- joinnode,
		height <- if not input.Size.oclIsUndefined() then
					if  not input.Size.height.oclIsUndefined() then
						input.Size.height
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		width <- if not input.Size.oclIsUndefined() then
					if  not input.Size.width.oclIsUndefined() then
						input.Size.width
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		x <- if not input.Location.oclIsUndefined() then
					if  not input.Location.x.oclIsUndefined() then
						input.Location.x
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		y <- if not input.Location.oclIsUndefined() then
					if  not input.Location.y.oclIsUndefined() then
						input.Location.y
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif						
	)
}

rule ReferenceEdge2ReferenceEdge {
	from input : jwtSource!"model::processes::ReferenceEdge"
	to refEdge : jwtTargetView!ReferenceEdge (
		action <- input.action,
		containedIn <- input.getReferenceEdgeScope(),
		reference <- input.reference
	)
}

rule Reference2Reference {
	from input : jwtSource!"model::processes::Reference"
	to ref : jwtTargetView!Reference (
		reference <- input.reference,
		containedIn <- 	input.getReferencesScope() 		
	),
	layoutData : jwtTargetView!LayoutData (
		initialized <- true,
		viewid <- 'Technical',
		describesElement <- ref,
		height <- if not input.Size.oclIsUndefined() then
					if  not input.Size.height.oclIsUndefined() then
						input.Size.height
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		width <- if not input.Size.oclIsUndefined() then
					if  not input.Size.width.oclIsUndefined() then
						input.Size.width
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		x <- if not input.Location.oclIsUndefined() then
					if  not input.Location.x.oclIsUndefined() then
						input.Location.x
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif,
		y <- if not input.Location.oclIsUndefined() then
					if  not input.Location.y.oclIsUndefined() then
						input.Location.y
					else
						OclUndefined
					endif
				else
					OclUndefined
				endif						
	)
	
}

-- package ORGANISATIONS
rule Role2Role {
	from input : jwtSource!"model::organisations::Role"
	to role : jwtTarget!Role (
		name <- input.name,
		icon <- input.icon,
		performedBy <- input.performedBy
	)
}

rule OrganisationUnit2OrganisationUnit {
	from input : jwtSource!"model::organisations::OrganisationUnit"
	to output : jwtTarget!OrganisationUnit (
		name <- input.name,
		icon <- input.icon
	)
}

-- package APPLICATION
-- the whole package application will be restructured soon, therefore, the transformations
-- are not finalized yet!!
 rule Application2Application {
 	from input : jwtSource!"model::application::Application" (not input.oclIsTypeOf(jwtSource!"model::application::WebServiceApplication"))
 	to output : jwtTarget!Application (
		 name <- input.name,
		 icon <- input.icon,
		 method <- input.method,
		 javaClass <- input.javaClass,
		 jarArchive <- input.jarArchive,
		 type <- input.type
	 )
 }

 rule ApplicationType2ApplicationType {
 	from input : jwtSource!"model::application::ApplicationType"
 	to output : jwtTarget!ApplicationType (
		 name <- input.name,
		 icon <- input.icon
	 )
 }
 
 rule WebServiceApplication2WebServiceApplication {
 	from input : jwtSource!"model::application::WebServiceApplication"
 	to output : jwtTarget!WebServiceApplication (
		 name <- input.name,
		 icon <- input.icon,
		 Operation <- input.Operation,
		 Interface <- input.Interface
	 )
 }

-- package DATA
rule Data2Data {
	from input : jwtSource!"model::data::Data"
	to output : jwtTarget!Data (
		name <- input.name,
		icon <- input.icon,
		dataType <- input.dataType,
		informationType <- input.informationType,
		parameters <- input.getParameters(), 
		value <- input.value
	)
}

rule Parameter2Parameter {
	from input : jwtSource!"model::data::Parameter"
	to output : jwtTarget!Parameter (
		name <- input.name,
		icon <- input.icon,
		value <- input.value
	)
}

rule DataType2DataType {
	from input : jwtSource!"model::data::DataType"
	to output : jwtTarget!DataType (
		name <- input.name,
		icon <- input.icon
	)
}

rule InformationType2InformationType {
	from input : jwtSource!"model::data::InformationType"
	to output : jwtTarget!InformationType (
		name <- input.name,
		icon <- input.icon
	)
}


-- package EVENTS
rule Event2Event {
	from input : jwtSource!"model::events::Event"
	to output : jwtTarget!Event (
		name <- input.name,
		icon <- input.icon
	)
}

-- package FUNCTIONS
rule Function2Function {
	from input : jwtSource!"model::functions::Function"
	to output : jwtTarget!Function (
		name <- input.name,
		icon <- input.icon
	)
}


-- package VIEW: no view package in JWT 0.7.0 anymore!