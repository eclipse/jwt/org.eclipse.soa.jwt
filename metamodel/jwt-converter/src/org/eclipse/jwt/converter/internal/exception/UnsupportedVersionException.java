/**
 * File:    UnsupportedVersionException.java
 * Created: 13.01.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg
 *
 *******************************************************************************/
package org.eclipse.jwt.converter.internal.exception;

public class UnsupportedVersionException extends Exception {

	/**
	 * serialVersionUID of this Exception
	 */
	private static final long serialVersionUID = -1924005407419998630L;

	/**
	 * Constructor with a more detailed error message
	 * @param error The error message why this version is not supported
	 */
	public UnsupportedVersionException(String error) {
		super(error);
	}
	
}
