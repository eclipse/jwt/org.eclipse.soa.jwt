-- @path jwtSource=/converter-transformations/metamodels/jwt06.ecore
-- @path jwtTarget=/converter-transformations/metamodels/jwt06.ecore

-- Copyright (c) 2005-2012 University of Augsburg
-- 
-- All rights reserved. This program and the accompanying materials
-- are made available under the terms of the Eclipse Public License 1.0
-- which accompanies this distribution, and is available at
-- http://www.eclipse.org/legal/epl-v10.html
--
-- Contributors:
--    Florian Lautenbacher - initial API and implementation

module JWT2JWT; -- Module Template
create OUT : jwtTarget from IN : jwtSource;

-- helpers

helper context jwtSource!"model::core::NamedElement" def: getName():String=
	if self.name.oclIsUndefined()
	then
		'Undefined'
	else
		self.name
	endif;


helper context jwtSource!"model::core::Package" def : isMainPackage() : Boolean =
	self.refImmediateComposite().oclIsUndefined();
	-- refImmediateComposite() is a reflective operation that returns the immediate composite (e.g. the immediate container) of self
	-- So if there is no immediate composite then the current node is the root (we suppose in our example that there is only one root).


helper context jwtSource!"model::core::Package" def : getAllChildren () : OrderedSet(jwtSource!"model::core::NamedElement") =
	self.elements->iterate( child ; elements : OrderedSet(jwtSource!"model::core::NamedElement")  = 
		OrderedSet{} | 
		if child.oclIsTypeOf(jwtSource!"model::core::Package") then
			elements.union(child.getAllChildren ()) -- NODE : recursive call
		else
				if child.oclIsTypeOf(jwtSource!"model::processes::Scope") then
				   child.Activity2Activity
				else
					elements.append(child) -- LEAF					
				endif
		endif
		)
	;

	
helper context jwtSource!"model::core::Package" def : getAllSubPackages () : OrderedSet(jwtSource!"model::core::Package") =
	self.subpackages->iterate( child ; elements : OrderedSet(jwtSource!"model::core::Package") = 
		OrderedSet{} | 
		if child.oclIsTypeOf(jwtSource!"model::core::Package") then
			self.subpackages.union(child.getAllSubPackages ()) -- NODE : recursive call
		else
			self.subpackages.append(child) -- LEAF
		endif
		)
	;

helper context jwtSource!"model::processes::Scope" def : getActivityNodes () : OrderedSet(jwtSource!"model::process::ActivityNode") =
	self.nodes->iterate( child ; elements : OrderedSet(jwtSource!"model::processes::ActivityNode") = 
		OrderedSet{} | 
			self.nodes.append(child) 
		)
	;

helper context jwtSource!"model::processes::Scope" def : getActivityEdges () : OrderedSet(jwtSource!"model::process::ActivityEdge") =
	self.edges->iterate( child ; elements : OrderedSet(jwtSource!"model::processes::ActivityEdge") = 
		OrderedSet{} | 
			self.edges.append(child) 
		)
	;

helper context jwtSource!"model::processes::Scope" def : getReferences () : OrderedSet(jwtSource!"model::processes::Reference") =
	self.references->iterate( child ; elements : OrderedSet(jwtSource!"model::processes::Reference") = 
		OrderedSet{} | 
			self.references.append(child) 
		)
	;

helper context jwtSource!"model::processes::Scope" def : getScopeReferenceEdges () : OrderedSet(jwtSource!"model::processes::ReferenceEdge") =
	self.referenceEdges->iterate( child ; elements : OrderedSet(jwtSource!"model::processes::ReferenceEdge") = 
		OrderedSet{} | 
			self.referenceEdges.append(child) 
		)
	;


helper context jwtSource!"model::processes::Action" def : getInputs () : OrderedSet(jwtSource!"model::data::Data") =
	self.inputs->iterate( child ; elements : OrderedSet(jwtSource!"model::data::Data") = 
		OrderedSet{} | 
			self.inputs.append(child) 
		)
	;

helper context jwtSource!"model::processes::Action" def : getOutputs () : OrderedSet(jwtSource!"model::data::Data") =
	self.outputs->iterate( child ; elements : OrderedSet(jwtSource!"model::data::Data") = 
		OrderedSet{} | 
			self.outputs.append(child) 
		)
	;

helper context jwtSource!"model::processes::Reference" def : getReferenceEdges () : OrderedSet(jwtSource!"model::processes::ReferenceEdge") =
	self.referenceEdges->iterate( child ; elements : OrderedSet(jwtSource!"model::processes::ReferenceEdge") = 
		OrderedSet{} | 
			self.referenceEdges.append(child) 
		)
	;

helper context jwtSource!"model::processes::GuardSpecification" def : getSubSpecification () : OrderedSet(jwtSource!"model::processes::GuardSpecification") =
	self.subSpecification->iterate( child ; elements : OrderedSet(jwtSource!"model::processes::GuardSpecification") = 
		OrderedSet{} | 
			self.subSpecification.append(child) 
		)
	;


-- rules

-- package CORE 
rule Model2Model {
	from input : jwtSource!"model::core::Package" (input.oclIsTypeOf(jwtSource!"model::core::Model"))
	to output : jwtTarget!Model (
		name <- input.name,
		author <- input.author,
		version <- input.version, 
		description <- input.description,
		--for new fileversion change here:
		--e.g. fileversion <- '0.7.0',
		fileversion <- '0.3.0',
		subpackages <- input.getAllSubPackages(),
		elements <- input.getAllChildren()
	)
}

rule Package2Package {
	from
		input : jwtSource!"model::core::Package" (not input.oclIsTypeOf(jwtSource!"model::core::Model")) 
	to
		output : jwtTarget!Package (
			name <- input.name,
			icon <- input.icon,
			elements <- input.getAllChildren(),
			subpackages <- input.getAllSubPackages(),
			ownedComment <- input.ownedComment
		)
}

rule Comment2Comment {
	from input : jwtSource!"model::core::Comment"
	to output : jwtTarget!Comment (
		text <- input.text
	)
}


-- package PROCESSES
rule Activity2Activity {
	from input : jwtSource!"model::processes::Activity"
	to output : jwtTarget!Activity (
		name <- input.name,
		nodes <- input.getActivityNodes(),
		edges <- input.getActivityEdges(),
		ownedComment <- input.ownedComment,
		references <- input.getReferences(),
		referenceEdges <- input.getScopeReferenceEdges()
	)
}

rule StructuredActivityNode2StructuredActivityNode {
	from input : jwtSource!"model::processes::StructuredActivityNode"
	to output : jwtTarget!StructuredActivityNode (
		name <- input.name,
		nodes <- input.getActivityNodes(),
		edges <- input.getActivityEdges(),
		ownedComment <- input.ownedComment,
		Location <- input.Location,
		Size <- input.Size,
		references <- input.getReferences(),
		referenceEdges <- input.getScopeReferenceEdges()
	)
} 

rule ActivityEdge2ActivityEdge {
	from input : jwtSource!"model::processes::ActivityEdge"
	to output : jwtTarget!ActivityEdge (
		source <- input.source,
		target <- input.target,
		guard <- input.guard
	)
}

rule Guard2Guard {
	from input : jwtSource!"model::processes::Guard"
	to output : jwtTarget!Guard (
		shortdescription <- input.shortdescription,
		textualdescription <- input.textualdescription,
		detailedSpecification <- input.detailedSpecification
	)
}

rule GuardSpecification2GuardSpecification {
	from input : jwtSource!"model::processes::GuardSpecification"
	to output : jwtTarget!GuardSpecification (
		operation <- input.operation,
		subSpecificationConnector <- input.subSpecificationConnector,
		value <- input.value,
		attribute <- input.attribute,
		data <- input.data,
		Description <- input.Description,
		subSpecification <- input.getSubSpecification()
	)
}



rule Action2Action {
	from input : jwtSource!"model::processes::Action"
	to output : jwtTarget!Action (
		name <- input.name, 
		icon <- input.icon,
		targetexecutiontime <- input.targetexecutiontime,
		Location <- input.Location,
		Size <- input.Size,
		realizes <- input.realizes, --Function
		performedBy <- input.performedBy, --Role
		executedBy <- input.executedBy, --Application
		inputs <- input.getInputs(), --Data
		outputs <- input.getOutputs()
	)
}

rule ActivityLinkNode2ActivityLinkNode {
	from input : jwtSource!"model::processes::ActivityLinkNode"
	to output : jwtTarget!ActivityLinkNode (
		name <- input.name,
		icon <- input.icon,
		Location <- input.Location,
		Size <- input.Size,
		linksto <- input.linksto
	)
}


rule InitialNode2InitialNode {
	from input : jwtSource!"model::processes::InitialNode"
	to output : jwtTarget!InitialNode (
		name <- input.name,
		icon <- input.icon,
		Location <- input.Location,
		Size <- input.Size
	)
}

rule FinalNode2FinalNode {
	from input : jwtSource!"model::processes::FinalNode"
	to output : jwtTarget!FinalNode (
		name <- input.name,
		icon <- input.icon,
		Location <- input.Location,
		Size <- input.Size
	)
}

rule DecisionNode2DecisionNode {
	from input : jwtSource!"model::processes::DecisionNode"
	to output : jwtTarget!DecisionNode (
		name <- input.name,
		icon <- input.icon,
		Location <- input.Location,
		Size <- input.Size
	)
}

rule MergeNode2MergeNode {
	from input : jwtSource!"model::processes::MergeNode"
	to output : jwtTarget!MergeNode (
		name <- input.name,
		icon <- input.icon,
		Location <- input.Location,
		Size <- input.Size
	)
}

rule ForkNode2ForkNode {
	from input : jwtSource!"model::processes::ForkNode"
	to output : jwtTarget!ForkNode (
		name <- input.name,
		icon <- input.icon,
		Location <- input.Location,
		Size <- input.Size
	)
}

rule JoinNode2JoinNode {
	from input : jwtSource!"model::processes::JoinNode"
	to output : jwtTarget!JoinNode (
		name <- input.name,
		icon <- input.icon,
		Location <- input.Location,
		Size <- input.Size
	)
}

rule Reference2Reference {
	from input : jwtSource!"model::processes::Reference"
	to output : jwtTarget!Reference (
		Location <- input.Location,
		Size <- input.Size,
		reference <- input.reference,
		referenceEdges <- input.getReferenceEdges()
	)
}

rule ReferenceEdge2ReferenceEdge {
	from input : jwtSource!"model::processes::ReferenceEdge"
	to output : jwtTarget!ReferenceEdge (
		action <- input.action,
		reference <- input.reference,
		direction <- input.direction
	)
}

--exception when trying to generate new EdgeDirection. Probably in metamodel already available
--rule EdgeDirection2EdgeDirection {
--	from input : jwtSource!"model::view::EdgeDirection"
--	to output : jwtTarget!EdgeDirection (
--	)
--}




-- package ORGANISATIONS
rule Role2Role {
	from input : jwtSource!"model::organisations::Role"
	to output : jwtTarget!Role (
		name <- input.name,
		icon <- input.icon,
		performedBy <- input.performedBy
	)
}

rule OrganisationUnit2OrganisationUnit {
	from input : jwtSource!"model::organisations::OrganisationUnit"
	to output : jwtTarget!OrganisationUnit (
		name <- input.name,
		icon <- input.icon
	)
}

-- package APPLICATION
rule Application2Application {
	from input : jwtSource!"model::application::Application"
	to output : jwtTarget!Application (
		name <- input.name,
		icon <- input.icon,
		method <- input.method,
		javaClass <- input.javaClass,
		jarArchive <- input.jarArchive,
		type <- input.type
	)
}

rule ApplicationType2ApplicationType {
	from input : jwtSource!"model::application::ApplicationType"
	to output : jwtTarget!ApplicationType (
		name <- input.name,
		icon <- input.icon
	)
}

rule WebServiceApplication2WebServiceApplication {
	from input : jwtSource!"model::application::WebServiceApplication"
	to output : jwtTarget!WebServiceApplication (
		name <- input.name,
		icon <- input.icon,
		Operation <- input.Operation,
		Interface <- input.Interface
	)
}

-- package DATA
rule Data2Data {
	from input : jwtSource!"model::data::Data"
	to output : jwtTarget!Data (
		name <- input.name,
		icon <- input.icon,
		dataType <- input.dataType,
		informationType <- input.informationType,
		value <- input.value
	)
}

rule DataType2DataType {
	from input : jwtSource!"model::data::DataType"
	to output : jwtTarget!DataType (
		name <- input.name,
		icon <- input.icon
	)
}

rule InformationType2InformationType {
	from input : jwtSource!"model::data::InformationType"
	to output : jwtTarget!InformationType (
		name <- input.name,
		icon <- input.icon
	)
}


-- package EVENTS
rule Event2Event {
	from input : jwtSource!"model::events::Event"
	to output : jwtTarget!Event (
		name <- input.name,
		icon <- input.icon
	)
}

-- package FUNCTIONS
rule Function2Function {
	from input : jwtSource!"model::functions::Function"
	to output : jwtTarget!Function (
		name <- input.name,
		icon <- input.icon
	)
}



-- package VIEW
rule Point2Point {
	from input : jwtSource!"model::view::Point"
	to output : jwtTarget!Point (
		x <- input.x,
		y <- input.y
		
	)
}

rule Dimension2Dimension {
	from input : jwtSource!"model::view::Dimension"
	to output : jwtTarget!Dimension (
		height <- input.height,
		width <- input.width
		
	)
}
