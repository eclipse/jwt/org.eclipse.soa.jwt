/**
 * File:    ConverterManager.java
 * Created: 09.01.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Florian Lautenbacher, Programming distributed Systems Lab, University of Augsburg
 *    Yoann Rodiere, Open Wide, Lyon, France
 *    	- Added 1.1 -> 1.2 conversion
 *******************************************************************************/

package org.eclipse.jwt.converter.internal;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jwt.converter.internal.exception.UnsupportedVersionException;

/**
 * This file starts the update mechanism.
 */
public class ConverterManager {

	private static ConverterManager instance = null;

	// the workflow file versions
	public static final int VER_0_0_0 = 00;
	public static final int VER_0_0_1 = 01;
	public static final int VER_0_1_0 = 10;
	public static final int VER_0_2_0 = 20;
	public static final int VER_0_3_0 = 30;
	public static final int VER_0_4_0 = 40;
	public static final int VER_0_5_0 = 50;
	public static final int VER_0_6_0 = 60;
	public static final int VER_0_7_0 = 70;
	public static final int VER_1_0_0 = 100;
	public static final int VER_1_1_0 = 110;
	public static final int VER_1_2_0 = 120;
	public static final int VER_1_3_0 = 130;
	public static final int VER_1_4_0 = 140;
	public static final int VER_1_5_0 = 150;
	public static final int VER_1_6_0 = 160;

	// the string representations of the file versions
	private static String VER_STR_0_0_0 = "0.0.0";
	private static String VER_STR_0_0_1 = "0.0.1";
	private static String VER_STR_0_1_0 = "0.1.0";
	private static String VER_STR_0_2_0 = "0.2.0";
	private static String VER_STR_0_3_0 = "0.3.0";
	private static String VER_STR_0_4_0 = "0.4.0";
	private static String VER_STR_0_5_0 = "0.5.0";
	private static String VER_STR_0_6_0 = "0.6.0";
	private static String VER_STR_0_7_0 = "0.7.0";
	private static String VER_STR_1_0_0 = "1.0.0";
	private static String VER_STR_1_1_0 = "1.1.0";
	private static String VER_STR_1_2_0 = "1.2.0";
	private static String VER_STR_1_3_0 = "1.3.0";
	private static String VER_STR_1_4_0 = "1.4.0";
	private static String VER_STR_1_5_0 = "1.5.0";
	private static String VER_STR_1_6_0 = "1.6.0";

	/**
	 * Singleton constructor
	 * 
	 * @return the instance of the ATLTransformation
	 */
	static public ConverterManager getInstance() {
		if (instance == null)
			instance = new ConverterManager();
		return instance;
	}

	/**
	 * Converts the string version to an integer representation.
	 * 
	 * @param version
	 * @return
	 */
	private int stringToIntVersion(String version) {
		if (version.equals(VER_STR_0_0_0))
			return VER_0_0_0;
		else if (version.equals(VER_STR_0_0_1))
			return VER_0_0_1;
		else if (version.equals(VER_STR_0_1_0))
			return VER_0_1_0;
		else if (version.equals(VER_STR_0_2_0))
			return VER_0_2_0;
		else if (version.equals(VER_STR_0_3_0))
			return VER_0_3_0;
		else if (version.equals(VER_STR_0_4_0))
			return VER_0_4_0;
		else if (version.equals(VER_STR_0_5_0))
			return VER_0_5_0;
		else if (version.equals(VER_STR_0_6_0))
			return VER_0_6_0;
		else if (version.equals(VER_STR_0_7_0))
			return VER_0_7_0;
		else if (version.equals(VER_STR_1_0_0))
			return VER_1_0_0;
		else if (version.equals(VER_STR_1_1_0))
			return VER_1_1_0;
		else if (version.equals(VER_STR_1_2_0))
			return VER_1_2_0;
		else if (version.equals(VER_STR_1_3_0))
			return VER_1_3_0;
		else if (version.equals(VER_STR_1_4_0))
			return VER_1_4_0;
		else if (version.equals(VER_STR_1_5_0))
			return VER_1_5_0;
		else if (version.equals(VER_STR_1_6_0))
			return VER_1_6_0;
		else
			return 0;
	}

	/**
	 * Calls the main method (but works with strings rather than integers).
	 * 
	 * @param inFileURI
	 * @param modelFile2
	 * @param inVersion
	 * @param outVersion
	 * @throws Exception
	 */
	public void convert(URI inFileURI, URI outFileURI, String inVersion, String outVersion) throws Exception {
		convert(inFileURI, outFileURI, stringToIntVersion(inVersion), stringToIntVersion(outVersion));
	}

	private String toPath(URI fileURI) throws IOException {
		String filePath = fileURI.toFileString();
		if (filePath == null || !(new File(filePath)).exists()) {
			URL url = new URL(fileURI.toString());
			filePath = FileLocator.resolve(url).toString().replace("file:", "");
		}
		return filePath;
	}

	/**
	 * inVersion = 10 means: 0.0.0 (1.0.0) inVersion = 20 means: 0.0.1 (1.0.1) inVersion = 30 means: 0.1.0 (1.1.0) inVersion = 40 means:
	 * 0.2.0 (1.2.0) inVersion = 50 means: 0.3.0 (1.3.0) inVersion = 60 means: 0.4.0 (1.4.0) inVersion = 70 means: 0.5.0 (1.5.0) inVersion =
	 * 80 means: 0.6.0 (1.6.0) outVersion analogous
	 * 
	 * @param inFilePath
	 * @param outFilePath
	 * @param inVersion
	 * @param outVersion
	 * @throws Exception
	 */
	public void convert(URI inFileURI, URI outFileURI, int inVersion, int outVersion) throws Exception {
		ATLTransformation atl = ATLTransformation.getInstance();
		String inFilePath = toPath(inFileURI);
		String outFilePath = toPath(outFileURI);

		// the following switch case makes all necessary transformations from
		// the inversion to the out-version
		// if inversion == 20 and outversion == 40 then
		// it will start in case 20, but don't find a break, go to case 30
		// don't find a break again, go to case 40, find a break and stop.
		switch (inVersion) {
		case VER_0_0_0: {
			atl.setSourceVersion(VER_0_0_0);
			atl.setTargetVersion(VER_0_0_1, VER_STR_0_0_1);
			atl.jwt2jwt(inFilePath, outFilePath);
			if (outVersion == VER_0_0_1)
				break;
		}
		case VER_0_0_1: {
			atl.setSourceVersion(VER_0_0_1);
			atl.setTargetVersion(VER_0_1_0, VER_STR_0_1_0);
			atl.jwt2jwt(inFilePath, outFilePath);
			if (outVersion == VER_0_1_0)
				break;
		}
		case VER_0_1_0: {
			atl.setSourceVersion(VER_0_1_0);
			atl.setTargetVersion(VER_0_2_0, VER_STR_0_2_0);
			atl.jwt2jwt(inFilePath, outFilePath);
			if (outVersion == VER_0_2_0)
				break;
		}
		case VER_0_2_0: {
			atl.setSourceVersion(VER_0_2_0);
			atl.setTargetVersion(VER_0_3_0, VER_STR_0_3_0);
			atl.jwt2jwt(inFilePath, outFilePath);
			if (outVersion == VER_0_3_0)
				break;
		}
		case VER_0_3_0: {
			atl.setSourceVersion(VER_0_3_0);
			atl.setTargetVersion(VER_0_4_0, VER_STR_0_4_0);
			atl.jwt2jwt(inFilePath, outFilePath);
			if (outVersion == VER_0_4_0)
				break;
		}
		case VER_0_4_0: {
			atl.setSourceVersion(VER_0_4_0);
			atl.setTargetVersion(VER_0_5_0, VER_STR_0_5_0);
			atl.jwt2jwt(inFilePath, outFilePath);
			if (outVersion == VER_0_5_0)
				break;
		}
		case VER_0_5_0: {
			atl.setSourceVersion(VER_0_5_0);
			atl.setTargetVersion(VER_0_6_0, VER_STR_0_6_0);
			atl.jwt2jwt(inFilePath, outFilePath);
			if (outVersion == VER_0_6_0)
				break;
		}
		case VER_0_6_0: {
			atl.setSourceVersion(VER_0_6_0);
			atl.setTargetVersion(VER_0_7_0, VER_STR_0_7_0);
			atl.jwt2jwt(inFilePath, outFilePath);
			if (outVersion == VER_0_7_0)
				break;
		}
		case VER_0_7_0: {
			atl.setSourceVersion(VER_0_7_0);
			atl.setTargetVersion(VER_1_0_0, VER_STR_1_0_0);
			atl.justUpdateFileTag(inFilePath, outFilePath);
			if (outVersion == VER_1_0_0)
				break;
		}
		case VER_1_0_0: {
			atl.setSourceVersion(VER_1_0_0);
			atl.setTargetVersion(VER_1_1_0, VER_STR_1_1_0);
			atl.justUpdateFileTag(inFilePath, outFilePath);
			if (outVersion == VER_1_1_0)
				break;
		}
		case VER_1_1_0: {
			atl.setSourceVersion(VER_1_1_0);
			atl.setTargetVersion(VER_1_2_0, VER_STR_1_2_0);
			atl.justUpdateFileTag(inFilePath, outFilePath);
			if (outVersion == VER_1_2_0)
				break;
		}
		case VER_1_2_0: {
			atl.setSourceVersion(VER_1_2_0);
			atl.setTargetVersion(VER_1_3_0, VER_STR_1_3_0);
			atl.justUpdateFileTag(inFilePath, outFilePath);
			if (outVersion == VER_1_3_0)
				break;
		}
		case VER_1_3_0: {
			atl.setSourceVersion(VER_1_3_0);
			atl.setTargetVersion(VER_1_4_0, VER_STR_1_4_0);
			atl.justUpdateFileTag(inFilePath, outFilePath);
			if (outVersion == VER_1_4_0)
				break;
		}
		case VER_1_4_0: {
			atl.setSourceVersion(VER_1_4_0);
			atl.setTargetVersion(VER_1_5_0, VER_STR_1_5_0);
			atl.justUpdateFileTag(inFilePath, outFilePath);
			if (outVersion == VER_1_5_0)
				break;
		}
		case VER_1_5_0: {
			atl.setSourceVersion(VER_1_5_0);
			atl.setTargetVersion(VER_1_6_0, VER_STR_1_6_0);
			atl.justUpdateFileTag(inFilePath, outFilePath);
			if (outVersion == VER_1_6_0)
				break;
		}
		default:
			throw new UnsupportedVersionException("Either " + inVersion + " or " + outVersion + " are not known.");
		}

	}
}
