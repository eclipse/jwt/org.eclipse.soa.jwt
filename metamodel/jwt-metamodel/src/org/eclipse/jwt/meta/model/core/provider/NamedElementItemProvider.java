/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.core.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.meta.PluginProperties;
import org.eclipse.jwt.meta.model.core.CorePackage;
import org.eclipse.jwt.meta.model.core.NamedElement;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;

/**
 * This is the item provider adapter for a {@link org.eclipse.jwt.meta.model.core.NamedElement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class NamedElementItemProvider extends ModelElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedElementItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);
		}

		addNamePropertyDescriptor(object);
		addIconPropertyDescriptor(object);
		return itemPropertyDescriptors;
	}


	/**
	 * This adds a property descriptor for the Name feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == CorePackage.Literals.NAMED_ELEMENT__NAME)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				CorePackage.Literals.NAMED_ELEMENT__NAME))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(CorePackage.Literals.NAMED_ELEMENT__NAME),

							PluginProperties
									.model_feature_description(CorePackage.Literals.NAMED_ELEMENT__NAME),

							CorePackage.Literals.NAMED_ELEMENT__NAME, true, false, false,
							ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Icon feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIconPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == CorePackage.Literals.NAMED_ELEMENT__ICON)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				CorePackage.Literals.NAMED_ELEMENT__ICON))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(CorePackage.Literals.NAMED_ELEMENT__ICON),

							PluginProperties
									.model_feature_description(CorePackage.Literals.NAMED_ELEMENT__ICON),

							CorePackage.Literals.NAMED_ELEMENT__ICON, true, false, false,
							ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This returns the label text for the adapted class.
	 * 
	 * @param object
	 * @return The text.
	 * @generated NOT
	 */
	@Override
	public String getText(Object object)
	{
		String type = PluginProperties.model_type(object);
		String label = ((NamedElement) object).getName();
		if (label == null)
		{
			label = "";
		}

		return PluginProperties.model_text(CorePackage.Literals.NAMED_ELEMENT,
				new Object[]
				{ type, label });
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(NamedElement.class))
		{
			case CorePackage.NAMED_ELEMENT__NAME:
			case CorePackage.NAMED_ELEMENT__ICON:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}


	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
