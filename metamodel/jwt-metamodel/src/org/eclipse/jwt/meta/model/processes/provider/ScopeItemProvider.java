/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.processes.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.meta.PluginProperties;
import org.eclipse.jwt.meta.model.core.CorePackage;
import org.eclipse.jwt.meta.model.core.provider.ModelElementItemProvider;
import org.eclipse.jwt.meta.model.events.EventsFactory;
import org.eclipse.jwt.meta.model.processes.ProcessesFactory;
import org.eclipse.jwt.meta.model.processes.ProcessesPackage;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;
import org.eclipse.jwt.meta.providers.interfaces.ICommandProvider;

/**
 * This is the item provider adapter for a {@link org.eclipse.jwt.meta.model.processes.Scope} object.
 * <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * @generated
 */
public class ScopeItemProvider extends ModelElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopeItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);
		}

		return itemPropertyDescriptors;
	}


	/**
	 * This specifies how to implement {@link #getChildren(Object)} and is used to deduce
	 * an appropriate feature for an {@link org.eclipse.emf.edit.command.AddCommand},
	 * {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in createCommand(). <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param object
	 * @return Collection
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(ProcessesPackage.Literals.SCOPE__NODES);
			childrenFeatures.add(ProcessesPackage.Literals.SCOPE__EDGES);
		}
		return childrenFeatures;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}


	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT!
	 */
	@Override
	public String getText(Object object)
	{
		return super.getText(object);
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(Scope.class))
		{
			case ProcessesPackage.SCOPE__NODES:
			case ProcessesPackage.SCOPE__EDGES:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}


	/**
	 * This adds to the collection of
	 * {@link org.eclipse.emf.edit.command.CommandParameter}s describing all of the
	 * children that can be created under this object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
						.createAction()));

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
						.createInitialNode()));

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
						.createFinalNode()));

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.SCOPE__NODES, EventsFactory.eINSTANCE
						.createEvent()));

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
						.createForkNode()));

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
						.createDecisionNode()));

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
						.createJoinNode()));

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
						.createMergeNode()));

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
						.createStructuredActivityNode()));

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
						.createActivityLinkNode()));
	}


	/**
	 * This returns the label text for
	 * {@link org.eclipse.emf.edit.command.CreateChildCommand}. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return the label text
	 * @generated NOT
	 */
	public String getCreateChildText(Object owner, Object feature, Object child,
			Collection selection)
	{
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == ProcessesPackage.Literals.SCOPE__NODES
				|| childFeature == CorePackage.Literals.MODEL_ELEMENT__OWNED_COMMENT;

		if (qualify)
		{
			return PluginProperties.command_CreateChild_text(childObject, childFeature,
					owner);
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.edit.provider.ItemProviderAdapter#createRemoveCommand(org.eclipse
	 * .emf.edit.domain.EditingDomain, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection)
	 * 
	 * generated NOT
	 */
	@Override
	protected Command createRemoveCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection collection)
	{
		// custom node command
		if (feature == ProcessesPackage.Literals.SCOPE__NODES)
		{
			return ResourceProviderRegistry.getInstance().createProviderCommand(
					ICommandProvider.CT_SCOPE_ACTIVITYNODE_REMOVE, domain, owner,
					feature, collection);
		}

		// custom remove activityedge command
		if (feature == ProcessesPackage.Literals.SCOPE__EDGES)
		{
			return ResourceProviderRegistry.getInstance().createProviderCommand(
					ICommandProvider.CT_SCOPE_ACTIVITYEDGE_REMOVE, domain, owner,
					feature, collection);
		}

		return super.createRemoveCommand(domain, owner, feature, collection);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.edit.provider.ItemProviderAdapter#createAddCommand(org.eclipse.
	 * emf.edit.domain.EditingDomain, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection, int)
	 * 
	 * generated NOT
	 */
	@Override
	protected Command createAddCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection collection, int index)
	{
		// custom add reference command
		return super.createAddCommand(domain, owner, feature, collection, index);
	}

}
