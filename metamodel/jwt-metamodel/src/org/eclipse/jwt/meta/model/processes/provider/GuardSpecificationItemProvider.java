/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.processes.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.meta.Plugin;
import org.eclipse.jwt.meta.PluginProperties;
import org.eclipse.jwt.meta.model.processes.GuardSpecification;
import org.eclipse.jwt.meta.model.processes.ProcessesFactory;
import org.eclipse.jwt.meta.model.processes.ProcessesPackage;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;

/**
 * This is the item provider adapter for a
 * {@link org.eclipse.jwt.meta.model.processes.GuardSpecification} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class GuardSpecificationItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GuardSpecificationItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);
		}

		addDataPropertyDescriptor(object);
		addAttributePropertyDescriptor(object);
		addOperationPropertyDescriptor(object);
		addValuePropertyDescriptor(object);
		addDescriptionPropertyDescriptor(object);
		addSubSpecificationConnectorPropertyDescriptor(object);
		return itemPropertyDescriptors;
	}


	/**
	 * This adds a property descriptor for the Data feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDataPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ProcessesPackage.Literals.GUARD_SPECIFICATION__DATA)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.GUARD_SPECIFICATION__DATA))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ProcessesPackage.Literals.GUARD_SPECIFICATION__DATA),

							PluginProperties
									.model_feature_description(ProcessesPackage.Literals.GUARD_SPECIFICATION__DATA),

							ProcessesPackage.Literals.GUARD_SPECIFICATION__DATA, false,
							false, false, null, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Attribute feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAttributePropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ProcessesPackage.Literals.GUARD_SPECIFICATION__ATTRIBUTE)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.GUARD_SPECIFICATION__ATTRIBUTE))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ProcessesPackage.Literals.GUARD_SPECIFICATION__ATTRIBUTE),

							PluginProperties
									.model_feature_description(ProcessesPackage.Literals.GUARD_SPECIFICATION__ATTRIBUTE),

							ProcessesPackage.Literals.GUARD_SPECIFICATION__ATTRIBUTE,
							false, false, false,
							ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Operation feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperationPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ProcessesPackage.Literals.GUARD_SPECIFICATION__OPERATION)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.GUARD_SPECIFICATION__OPERATION))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ProcessesPackage.Literals.GUARD_SPECIFICATION__OPERATION),

							PluginProperties
									.model_feature_description(ProcessesPackage.Literals.GUARD_SPECIFICATION__OPERATION),

							ProcessesPackage.Literals.GUARD_SPECIFICATION__OPERATION,
							false, false, false,
							ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Value feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValuePropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ProcessesPackage.Literals.GUARD_SPECIFICATION__VALUE)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.GUARD_SPECIFICATION__VALUE))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ProcessesPackage.Literals.GUARD_SPECIFICATION__VALUE),

							PluginProperties
									.model_feature_description(ProcessesPackage.Literals.GUARD_SPECIFICATION__VALUE),

							ProcessesPackage.Literals.GUARD_SPECIFICATION__VALUE, false,
							false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
							null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Description feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ProcessesPackage.Literals.GUARD_SPECIFICATION__DESCRIPTION)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.GUARD_SPECIFICATION__DESCRIPTION))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ProcessesPackage.Literals.GUARD_SPECIFICATION__DESCRIPTION),

							PluginProperties
									.model_feature_description(ProcessesPackage.Literals.GUARD_SPECIFICATION__DESCRIPTION),

							ProcessesPackage.Literals.GUARD_SPECIFICATION__DESCRIPTION,
							true, false, false,
							ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Sub Specification Connector feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	protected void addSubSpecificationConnectorPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ProcessesPackage.Literals.GUARD_SPECIFICATION__SUB_SPECIFICATION_CONNECTOR)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry
				.getInstance()
				.displayObject(
						ProcessesPackage.Literals.GUARD_SPECIFICATION__SUB_SPECIFICATION_CONNECTOR))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ProcessesPackage.Literals.GUARD_SPECIFICATION__SUB_SPECIFICATION_CONNECTOR),

							PluginProperties
									.model_feature_description(ProcessesPackage.Literals.GUARD_SPECIFICATION__SUB_SPECIFICATION_CONNECTOR),

							ProcessesPackage.Literals.GUARD_SPECIFICATION__SUB_SPECIFICATION_CONNECTOR,
							false, false, false,
							ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This specifies how to implement {@link #getChildren(Object)} and is used to deduce
	 * an appropriate feature for an {@link org.eclipse.emf.edit.command.AddCommand},
	 * {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in createCommand(). <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param object
	 * @return Collection
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(ProcessesPackage.Literals.GUARD_SPECIFICATION__SUB_SPECIFICATION);
		}
		return childrenFeatures;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}


	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 * @return The text.
	 * @generated NOT
	 */
	@Override
	public String getText(Object object)
	{
		String type = PluginProperties.model_type(object);
		GuardSpecification guardSpecification = (GuardSpecification) object;

		String data = null;
		if (guardSpecification.getData() != null)
		{
			data = guardSpecification.getData().getName();
		}
		else
		{
			data = "";
		}
		data += ".";
		data += guardSpecification.getAttribute();
		String operation = null;
		String value = guardSpecification.getValue();
		if (guardSpecification.getOperation() != null)
		{
			operation = guardSpecification.getOperation().getLiteral();
		}
		if (guardSpecification.getSubSpecification().size() > 0)
		{
			data = "";
			operation = guardSpecification.getSubSpecificationConnector().getLiteral();
			value = "";
		}

		return PluginProperties.model_text(GuardSpecification.class, new Object[]
		{ type, data, operation, value });
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(GuardSpecification.class))
		{
			case ProcessesPackage.GUARD_SPECIFICATION__DATA:
			case ProcessesPackage.GUARD_SPECIFICATION__ATTRIBUTE:
			case ProcessesPackage.GUARD_SPECIFICATION__OPERATION:
			case ProcessesPackage.GUARD_SPECIFICATION__VALUE:
			case ProcessesPackage.GUARD_SPECIFICATION__DESCRIPTION:
			case ProcessesPackage.GUARD_SPECIFICATION__SUB_SPECIFICATION_CONNECTOR:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), false, true));
				return;
			case ProcessesPackage.GUARD_SPECIFICATION__SUB_SPECIFICATION:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}


	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.GUARD_SPECIFICATION__SUB_SPECIFICATION,
				ProcessesFactory.eINSTANCE.createGuardSpecification()));
	}


	/**
	 * This returns the label text for
	 * {@link org.eclipse.emf.edit.command.CreateChildCommand}. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return the label text
	 * @generated NOT!
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		return PluginProperties.command_CreateChild_text(child, feature, owner);
	}


	/**
	 * Return an ImageDescriptor that describes the model.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param object
	 * @return The ImageDescriptor.
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object)
	{
		return null;
	}


	/**
	 * Return the type image for creation of childs. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return The type image descriptor.
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getCreateChildImage(java.lang.Object,
	 *      java.lang.Object, java.lang.Object, java.util.Collection)
	 * @generated NOT
	 */
	@Override
	public Object getCreateChildImage(Object owner, Object feature, Object child,
			Collection selection)
	{
		return null;
	}


	/**
	 * Return the resource locator for this item provider's resources.
	 * Edit (by Marc Dutoo) : uses jwt plugin instance as resource locator and not 
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator()
	{
		return Plugin.getInstance(); // [JWT]
	}


	/**
	 * This looks up the name of the type of the specified object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param object
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getTypeText(Object object)
	{
		return PluginProperties.model_type(object);
	}


	/**
	 * This looks up the name of the type of the specified attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param attribute
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getTypeText(EAttribute attribute)
	{
		return PluginProperties.model_datatype(attribute);
	}


	/**
	 * This looks up the name of the specified feature.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param feature
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getFeatureText(Object feature)
	{
		return PluginProperties.model_feature_name(feature);
	}


	/**
	 * This returns the description for CreateChildCommand.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return The description.
	 * @generated
	 */
	@Override
	public String getCreateChildDescription(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		Object selectionObject = selection == null || selection.isEmpty() ? null
				: selection.iterator().next();

		if (selectionObject == owner)
		{
			return PluginProperties
					.command_CreateChild_description(child, feature, owner);
		}

		Object sibling = selectionObject;
		Object siblingFeature = getChildFeature(owner, sibling);

		if (siblingFeature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) siblingFeature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) sibling;
			feature = entry.getEStructuralFeature();
			sibling = entry.getValue();
		}

		return PluginProperties
				.command_CreateSibling_description(child, feature, sibling);
	}


	/**
	 * This returns the tool tip text for CreateChildCommand.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return The description.
	 * @generated
	 */
	@Override
	public String getCreateChildToolTipText(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		return PluginProperties.command_CreateChild_tooltip(child, feature, owner);
	}


	/**
	 * This method overrides the ITreeItemContentProvider.getChildren to select which
	 * Objects may be shown in the outline depending on the view!
	 * 
	 * @author Wolf Fischer
	 * @generated NOT
	 */
	@Override
	public Collection getChildren(Object object)
	{
		return super.getChildren(object);
	}

}
