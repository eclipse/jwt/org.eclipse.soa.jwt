/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.core.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.meta.Plugin;
import org.eclipse.jwt.meta.PluginProperties;
import org.eclipse.jwt.meta.model.core.CoreFactory;
import org.eclipse.jwt.meta.model.core.CorePackage;
import org.eclipse.jwt.meta.model.core.ModelElement;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;
import org.eclipse.jwt.we.conf.edit.aspects.provider.AspectEnrichedItemProviderAdapter;

/**
 * This is the item provider adapter for a {@link org.eclipse.jwt.meta.model.core.ModelElement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ModelElementItemProvider extends AspectEnrichedItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElementItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);
		}

		return itemPropertyDescriptors;
	}


	/**
	 * This specifies how to implement {@link #getChildren(Object)} and is used to deduce
	 * an appropriate feature for an {@link org.eclipse.emf.edit.command.AddCommand},
	 * {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in createCommand(). <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param object
	 * @return Collection
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(CorePackage.Literals.MODEL_ELEMENT__OWNED_COMMENT);
		}
		return childrenFeatures;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}


	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT!
	 */
	@Override
	public String getText(Object object)
	{
		String type = PluginProperties.model_type(object);
		return PluginProperties.model_text(CorePackage.Literals.MODEL_ELEMENT,
				new Object[]
				{ type });
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(ModelElement.class))
		{
			case CorePackage.MODEL_ELEMENT__OWNED_COMMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}


	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				CorePackage.Literals.MODEL_ELEMENT__OWNED_COMMENT, CoreFactory.eINSTANCE
						.createComment()));
	}


	/**
	 * This returns the label text for
	 * {@link org.eclipse.emf.edit.command.CreateChildCommand}. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return the label text
	 * @generated NOT!
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		return PluginProperties.command_CreateChild_text(child, feature, owner);
	}


	/**
	 * Return an ImageDescriptor that describes the model.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param object
	 * @return The ImageDescriptor.
	 * @generated
	 */
	@Override
	public Object getImage(Object object)
	{
		return ResourceProviderRegistry.getInstance().createImageDescriptor(object);
	}


	/**
	 * Return the type image for creation of childs.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return The type image descriptor.
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getCreateChildImage(java.lang.Object, java.lang.Object, java.lang.Object, java.util.Collection)
	 * @generated
	 */
	@Override
	public Object getCreateChildImage(Object owner, Object feature, Object child,
			Collection selection)
	{
		return ResourceProviderRegistry.getInstance().createImageDescriptor(child);
	}


	/**
	 * Return the resource locator for this item provider's resources.
	 * Edit (by Marc Dutoo) : uses jwt plugin instance as resource locator and not 
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator()
	{
		return Plugin.getInstance(); // [JWT]
	}


	/**
	 * This looks up the name of the type of the specified object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param object
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getTypeText(Object object)
	{
		return PluginProperties.model_type(object);
	}


	/**
	 * This looks up the name of the type of the specified attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @param attribute
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getTypeText(EAttribute attribute)
	{
		return PluginProperties.model_datatype(attribute);
	}


	/**
	 * This looks up the name of the specified feature.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param feature
	 * @return The name.
	 * @generated
	 */
	@Override
	protected String getFeatureText(Object feature)
	{
		return PluginProperties.model_feature_name(feature);
	}


	/**
	 * This returns the description for CreateChildCommand.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return The description.
	 * @generated
	 */
	@Override
	public String getCreateChildDescription(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		Object selectionObject = selection == null || selection.isEmpty() ? null
				: selection.iterator().next();

		if (selectionObject == owner)
		{
			return PluginProperties
					.command_CreateChild_description(child, feature, owner);
		}

		Object sibling = selectionObject;
		Object siblingFeature = getChildFeature(owner, sibling);

		if (siblingFeature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) siblingFeature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) sibling;
			feature = entry.getEStructuralFeature();
			sibling = entry.getValue();
		}

		return PluginProperties
				.command_CreateSibling_description(child, feature, sibling);
	}


	/**
	 * This returns the tool tip text for CreateChildCommand.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param owner
	 * @param feature
	 * @param child
	 * @param selection
	 * @return The description.
	 * @generated
	 */
	@Override
	public String getCreateChildToolTipText(Object owner, Object feature, Object child,
			Collection selection)
	{
		if (feature instanceof EStructuralFeature
				&& FeatureMapUtil.isFeatureMap((EStructuralFeature) feature))
		{
			FeatureMap.Entry entry = (FeatureMap.Entry) child;
			feature = entry.getEStructuralFeature();
			child = entry.getValue();
		}

		return PluginProperties.command_CreateChild_tooltip(child, feature, owner);
	}


	/**
	 * This method overrides the ITreeItemContentProvider.getChildren to select which
	 * Objects may be shown in the outline depending on the view!
	 * 
	 * @author Wolf Fischer
	 * @generated
	 */
	@Override
	public Collection getChildren(Object object)
	{
		Collection col = super.getChildren(object);
		Collection result = new ArrayList();
		for (Object obj : col)
		{

			EObject ecl = (EObject) obj;
			if (ResourceProviderRegistry.getInstance().displayObject(ecl.eClass()))
			{
				if (obj instanceof ActivityEdge)
				{
					ActivityEdge ae = (ActivityEdge) obj;
					if (ae.getSource() != null
							&& ae.getTarget() != null
							&& ResourceProviderRegistry.getInstance().displayObject(
									ae.getSource().eClass())
							&& ResourceProviderRegistry.getInstance().displayObject(
									ae.getTarget().eClass()))
					{
						result.add(obj);
					}
				}
				else
				{
					result.add(obj);
				}
			}
		}

		// give external editors the ability to modify the children list
		// the default implementation just returns the standard children set
		return ResourceProviderRegistry.getInstance().getModelChildren(object, result);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.edit.provider.ItemProviderAdapter#factorAddCommand(org.eclipse.
	 * emf.edit.domain.EditingDomain, org.eclipse.emf.edit.command.CommandParameter)
	 */
	@Override
	protected Command factorAddCommand(EditingDomain domain,
			CommandParameter commandParameter)
	{
		Command factoredCommand = super.factorAddCommand(domain, commandParameter);

		if (factoredCommand instanceof CompoundCommand)
		{
			// delete all commands that are not executeable
			ArrayList commands = new ArrayList(((CompoundCommand) factoredCommand)
					.getCommandList());
			for (Iterator i = commands.iterator(); i.hasNext();)
			{
				Command command = (Command) i.next();
				if (!command.canExecute())
				{
					i.remove();
				}
			}

			factoredCommand = new CompoundCommand(((CompoundCommand) factoredCommand)
					.getResultIndex(), commands);
		}

		return factoredCommand;
	}

}
