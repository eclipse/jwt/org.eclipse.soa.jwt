/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.core.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.meta.model.application.ApplicationFactory;
import org.eclipse.jwt.meta.model.application.ApplicationPackage;
import org.eclipse.jwt.meta.model.core.CoreFactory;
import org.eclipse.jwt.meta.model.core.CorePackage;
import org.eclipse.jwt.meta.model.data.DataFactory;
import org.eclipse.jwt.meta.model.data.DataPackage;
import org.eclipse.jwt.meta.model.functions.FunctionsFactory;
import org.eclipse.jwt.meta.model.functions.FunctionsPackage;
import org.eclipse.jwt.meta.model.organisations.OrganisationsFactory;
import org.eclipse.jwt.meta.model.organisations.OrganisationsPackage;
import org.eclipse.jwt.meta.model.primitiveTypes.PrimitiveTypesFactory;
import org.eclipse.jwt.meta.model.primitiveTypes.PrimitiveTypesPackage;
import org.eclipse.jwt.meta.model.processes.ProcessesFactory;
import org.eclipse.jwt.meta.model.processes.ProcessesPackage;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;
import org.eclipse.jwt.meta.providers.interfaces.ICommandProvider;

/**
 * This is the item provider adapter for a {@link org.eclipse.jwt.meta.model.core.Package} object.
 * <!-- begin-user-doc
 * --> <!-- end-user-doc -->
 * @generated
 */
public class PackageItemProvider extends NamedElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public PackageItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);
		}

		return itemPropertyDescriptors;
	}


	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(CorePackage.Literals.PACKAGE__SUBPACKAGES);
			childrenFeatures.add(CorePackage.Literals.PACKAGE__ELEMENTS);
		}
		return childrenFeatures;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}


	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated NOT!
	 */
	@Override
	public String getText(Object object)
	{
		return super.getText(object);
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(org.eclipse.jwt.meta.model.core.Package.class))
		{
			case CorePackage.PACKAGE__SUBPACKAGES:
			case CorePackage.PACKAGE__ELEMENTS:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}


	/**
	 * This adds to the collection of
	 * {@link org.eclipse.emf.edit.command.CommandParameter}s describing all of
	 * the children that can be created under this object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		if (ResourceProviderRegistry.getInstance().displayObject(
				CorePackage.Literals.PACKAGE))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__SUBPACKAGES, CoreFactory.eINSTANCE
							.createPackage()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.ACTIVITY))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS, ProcessesFactory.eINSTANCE
							.createActivity()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				FunctionsPackage.Literals.FUNCTION))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS, FunctionsFactory.eINSTANCE
							.createFunction()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ApplicationPackage.Literals.APPLICATION))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS, ApplicationFactory.eINSTANCE
							.createApplication()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ApplicationPackage.Literals.WEB_SERVICE_APPLICATION))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS, ApplicationFactory.eINSTANCE
							.createWebServiceApplication()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ApplicationPackage.Literals.APPLICATION_TYPE))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS, ApplicationFactory.eINSTANCE
							.createApplicationType()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				OrganisationsPackage.Literals.ROLE))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS,
					OrganisationsFactory.eINSTANCE.createRole()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				OrganisationsPackage.Literals.ORGANISATION_UNIT))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS,
					OrganisationsFactory.eINSTANCE.createOrganisationUnit()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				DataPackage.Literals.DATA))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS, DataFactory.eINSTANCE
							.createData()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				DataPackage.Literals.DATA_TYPE))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS, DataFactory.eINSTANCE
							.createDataType()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				DataPackage.Literals.INFORMATION_TYPE))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS, DataFactory.eINSTANCE
							.createInformationType()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				PrimitiveTypesPackage.Literals.STRING_TYPE))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS,
					PrimitiveTypesFactory.eINSTANCE.createStringType()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				PrimitiveTypesPackage.Literals.INTEGER_TYPE))
		{
			newChildDescriptors.add(createChildParameter(
					CorePackage.Literals.PACKAGE__ELEMENTS,
					PrimitiveTypesFactory.eINSTANCE.createIntegerType()));
		}

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.edit.provider.ItemProviderAdapter#createRemoveCommand
	 * (org.eclipse .emf.edit.domain.EditingDomain,
	 * org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature,
	 * java.util.Collection)
	 * 
	 * generated NOT
	 */
	@Override
	protected Command createRemoveCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection collection)
	{
		// custom node command
		if (feature == CorePackage.Literals.PACKAGE__ELEMENTS)
		{
			return ResourceProviderRegistry.getInstance().createProviderCommand(
					ICommandProvider.CT_PACKAGE_ELEMENTS_REMOVE, domain, owner, feature,
					collection);
		}
		else if (feature == CorePackage.Literals.PACKAGE__SUBPACKAGES)
		{
			return ResourceProviderRegistry.getInstance().createProviderCommand(
					ICommandProvider.CT_PACKAGE_SUBPACKAGES_REMOVE, domain, owner,
					feature, collection);
		}

		return super.createRemoveCommand(domain, owner, feature, collection);
	}
}
