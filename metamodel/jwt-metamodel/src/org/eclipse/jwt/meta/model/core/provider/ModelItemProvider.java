/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.core.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.meta.PluginProperties;
import org.eclipse.jwt.meta.model.core.CorePackage;
import org.eclipse.jwt.meta.model.core.Model;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;

/**
 * This is the item provider adapter for a {@link org.eclipse.jwt.meta.model.core.Model} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class ModelItemProvider extends PackageItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);
		}

		addAuthorPropertyDescriptor(object);
		addVersionPropertyDescriptor(object);
		addDescriptionPropertyDescriptor(object);
		addFileversionPropertyDescriptor(object);
		return itemPropertyDescriptors;
	}


	/**
	 * This adds a property descriptor for the Author feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAuthorPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == CorePackage.Literals.MODEL__AUTHOR)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				CorePackage.Literals.MODEL__AUTHOR))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(CorePackage.Literals.MODEL__AUTHOR),

							PluginProperties
									.model_feature_description(CorePackage.Literals.MODEL__AUTHOR),

							CorePackage.Literals.MODEL__AUTHOR, true, false, false,
							ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Version feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVersionPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == CorePackage.Literals.MODEL__VERSION)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				CorePackage.Literals.MODEL__VERSION))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(CorePackage.Literals.MODEL__VERSION),

							PluginProperties
									.model_feature_description(CorePackage.Literals.MODEL__VERSION),

							CorePackage.Literals.MODEL__VERSION, true, false, false,
							ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Description feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == CorePackage.Literals.MODEL__DESCRIPTION)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				CorePackage.Literals.MODEL__DESCRIPTION))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(CorePackage.Literals.MODEL__DESCRIPTION),

							PluginProperties
									.model_feature_description(CorePackage.Literals.MODEL__DESCRIPTION),

							CorePackage.Literals.MODEL__DESCRIPTION, true, false, false,
							ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Fileversion feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFileversionPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == CorePackage.Literals.MODEL__FILEVERSION)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				CorePackage.Literals.MODEL__FILEVERSION))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(CorePackage.Literals.MODEL__FILEVERSION),

							PluginProperties
									.model_feature_description(CorePackage.Literals.MODEL__FILEVERSION),

							CorePackage.Literals.MODEL__FILEVERSION, false, false, false,
							ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated NOT!
	 */
	@Override
	public String getText(Object object)
	{
		return super.getText(object);
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(Model.class))
		{
			case CorePackage.MODEL__AUTHOR:
			case CorePackage.MODEL__VERSION:
			case CorePackage.MODEL__DESCRIPTION:
			case CorePackage.MODEL__FILEVERSION:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}


	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
