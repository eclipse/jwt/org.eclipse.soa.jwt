/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.processes.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.meta.PluginProperties;
import org.eclipse.jwt.meta.model.core.provider.PackageableElementItemProvider;
import org.eclipse.jwt.meta.model.events.EventsFactory;
import org.eclipse.jwt.meta.model.events.EventsPackage;
import org.eclipse.jwt.meta.model.processes.Activity;
import org.eclipse.jwt.meta.model.processes.ProcessesFactory;
import org.eclipse.jwt.meta.model.processes.ProcessesPackage;
import org.eclipse.jwt.meta.model.processes.Scope;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;

/**
 * This is the item provider adapter for a {@link org.eclipse.jwt.meta.model.processes.Activity} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ActivityItemProvider extends PackageableElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);
		}

		addTotalexecutiontimePropertyDescriptor(object);
		return itemPropertyDescriptors;
	}


	/**
	 * This adds a property descriptor for the Totalexecutiontime feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTotalexecutiontimePropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ProcessesPackage.Literals.ACTIVITY__TOTALEXECUTIONTIME)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.ACTIVITY__TOTALEXECUTIONTIME))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ProcessesPackage.Literals.ACTIVITY__TOTALEXECUTIONTIME),

							PluginProperties
									.model_feature_description(ProcessesPackage.Literals.ACTIVITY__TOTALEXECUTIONTIME),

							ProcessesPackage.Literals.ACTIVITY__TOTALEXECUTIONTIME, true,
							false, false, ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
							null, null));
		}
	}


	/**
	 * This specifies how to implement {@link #getChildren(Object)} and is used to deduce
	 * an appropriate feature for an {@link org.eclipse.emf.edit.command.AddCommand},
	 * {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in createCommand(). <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param object
	 * @return Collection
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(ProcessesPackage.Literals.SCOPE__NODES);
			childrenFeatures.add(ProcessesPackage.Literals.SCOPE__EDGES);
			childrenFeatures.add(ProcessesPackage.Literals.ACTIVITY__EVENT_HANDLER);
		}
		return childrenFeatures;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}


	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT!
	 */
	@Override
	public String getText(Object object)
	{
		return super.getText(object);
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(Activity.class))
		{
			case ProcessesPackage.ACTIVITY__TOTALEXECUTIONTIME:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), false, true));
				return;
			case ProcessesPackage.ACTIVITY__NODES:
			case ProcessesPackage.ACTIVITY__EDGES:
			case ProcessesPackage.ACTIVITY__EVENT_HANDLER:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}


	/**
	 * This adds to the collection of
	 * {@link org.eclipse.emf.edit.command.CommandParameter}s describing all of the
	 * children that can be created under this object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.ACTION))
		{
			newChildDescriptors.add(createChildParameter(
					ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
							.createAction()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.INITIAL_NODE))
		{
			newChildDescriptors.add(createChildParameter(
					ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
							.createInitialNode()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.FINAL_NODE))
		{
			newChildDescriptors.add(createChildParameter(
					ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
							.createFinalNode()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				EventsPackage.Literals.EVENT))
		{
			newChildDescriptors.add(createChildParameter(
					ProcessesPackage.Literals.SCOPE__NODES, EventsFactory.eINSTANCE
							.createEvent()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				EventsPackage.Literals.EVENT_HANDLER))
		{
			newChildDescriptors.add(createChildParameter(
					ProcessesPackage.Literals.ACTIVITY__EVENT_HANDLER,
					EventsFactory.eINSTANCE.createEventHandler()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.FORK_NODE))
		{
			newChildDescriptors.add(createChildParameter(
					ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
							.createForkNode()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.JOIN_NODE))
		{
			newChildDescriptors.add(createChildParameter(
					ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
							.createJoinNode()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.DECISION_NODE))
		{
			newChildDescriptors.add(createChildParameter(
					ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
							.createDecisionNode()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.MERGE_NODE))
		{
			newChildDescriptors.add(createChildParameter(
					ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
							.createMergeNode()));
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.STRUCTURED_ACTIVITY_NODE))
		{
			newChildDescriptors.add(createChildParameter(
					ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
							.createStructuredActivityNode()));
		}

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.SCOPE__NODES, ProcessesFactory.eINSTANCE
						.createActivityLinkNode()));
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.edit.provider.ItemProviderAdapter#createCommand(java.lang.Object,
	 * org.eclipse.emf.edit.domain.EditingDomain, java.lang.Class,
	 * org.eclipse.emf.edit.command.CommandParameter)
	 */
	@Override
	public Command createCommand(Object object, EditingDomain domain, Class commandClass,
			CommandParameter commandParameter)
	{
		// delegate Scope features to the ScopeItemProvider
		// if the feature is null (like the RemoveCommand) the command must be
		// delegated separatly
		if (commandParameter.feature != null
				&& commandParameter.feature instanceof EStructuralFeature
				&& ((EStructuralFeature) commandParameter.feature).getContainerClass() == Scope.class)
		{
			ScopeItemProvider adapter = (ScopeItemProvider) ((ProcessesItemProviderAdapterFactory) getAdapterFactory())
					.createScopeAdapter();

			return adapter.createCommand(object, domain, commandClass, commandParameter);
		}

		return super.createCommand(object, domain, commandClass, commandParameter);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.edit.provider.ItemProviderAdapter#createRemoveCommand(org.eclipse
	 * .emf.edit.domain.EditingDomain, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection)
	 */
	@Override
	protected Command createRemoveCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection collection)
	{
		// delegate Scope features to the ScopeItemProvider
		if (feature.getContainerClass() == Scope.class)
		{
			ScopeItemProvider adapter = (ScopeItemProvider) ((ProcessesItemProviderAdapterFactory) getAdapterFactory())
					.createScopeAdapter();

			return adapter.createRemoveCommand(domain, owner, feature, collection);
		}

		return super.createRemoveCommand(domain, owner, feature, collection);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.edit.provider.ItemProviderAdapter#createAddCommand(org.eclipse.
	 * emf.edit.domain.EditingDomain, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EStructuralFeature, java.util.Collection, int)
	 */
	@Override
	protected Command createAddCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection collection, int index)
	{
		// delegate Scope features to the ScopeItemProvider
		if (feature.getContainerClass() == Scope.class)
		{
			ScopeItemProvider adapter = (ScopeItemProvider) ((ProcessesItemProviderAdapterFactory) getAdapterFactory())
					.createScopeAdapter();

			return adapter.createAddCommand(domain, owner, feature, collection, index);
		}

		return super.createAddCommand(domain, owner, feature, collection, index);
	}

}
