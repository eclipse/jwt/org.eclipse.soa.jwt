/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.processes;

import org.eclipse.jwt.meta.model.core.PackageableElement;
import org.eclipse.jwt.meta.model.events.EventHandler;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.meta.model.processes.Activity#getEventHandler <em>Event Handler</em>}</li>
 *   <li>{@link org.eclipse.jwt.meta.model.processes.Activity#getTotalexecutiontime <em>Totalexecutiontime</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.meta.model.processes.ProcessesPackage#getActivity()
 * @model
 * @generated
 */
public interface Activity
		extends PackageableElement, Scope
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * Returns the value of the '<em><b>Event Handler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Handler</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Handler</em>' containment reference.
	 * @see #setEventHandler(EventHandler)
	 * @see org.eclipse.jwt.meta.model.processes.ProcessesPackage#getActivity_EventHandler()
	 * @model containment="true"
	 * @generated
	 */
	EventHandler getEventHandler();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.meta.model.processes.Activity#getEventHandler <em>Event Handler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Handler</em>' containment reference.
	 * @see #getEventHandler()
	 * @generated
	 */
	void setEventHandler(EventHandler value);


	/**
	 * Returns the value of the '<em><b>Totalexecutiontime</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Totalexecutiontime</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Totalexecutiontime</em>' attribute.
	 * @see #setTotalexecutiontime(int)
	 * @see org.eclipse.jwt.meta.model.processes.ProcessesPackage#getActivity_Totalexecutiontime()
	 * @model
	 * @generated
	 */
	int getTotalexecutiontime();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.meta.model.processes.Activity#getTotalexecutiontime <em>Totalexecutiontime</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Totalexecutiontime</em>' attribute.
	 * @see #getTotalexecutiontime()
	 * @generated
	 */
	void setTotalexecutiontime(int value);

} // Activity