/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.core;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Package</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.meta.model.core.Package#getSubpackages <em>Subpackages</em>}</li>
 *   <li>{@link org.eclipse.jwt.meta.model.core.Package#getSuperpackage <em>Superpackage</em>}</li>
 *   <li>{@link org.eclipse.jwt.meta.model.core.Package#getElements <em>Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.meta.model.core.CorePackage#getPackage()
 * @model
 * @generated
 */
public interface Package
		extends NamedElement
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * Returns the value of the '<em><b>Subpackages</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.jwt.meta.model.core.Package}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.jwt.meta.model.core.Package#getSuperpackage <em>Superpackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subpackages</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subpackages</em>' containment reference list.
	 * @see org.eclipse.jwt.meta.model.core.CorePackage#getPackage_Subpackages()
	 * @see org.eclipse.jwt.meta.model.core.Package#getSuperpackage
	 * @model opposite="superpackage" containment="true"
	 * @generated
	 */
	EList<Package> getSubpackages();


	/**
	 * Returns the value of the '<em><b>Superpackage</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.jwt.meta.model.core.Package#getSubpackages <em>Subpackages</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Superpackage</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Superpackage</em>' container reference.
	 * @see #setSuperpackage(Package)
	 * @see org.eclipse.jwt.meta.model.core.CorePackage#getPackage_Superpackage()
	 * @see org.eclipse.jwt.meta.model.core.Package#getSubpackages
	 * @model opposite="subpackages" transient="false"
	 * @generated
	 */
	Package getSuperpackage();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.meta.model.core.Package#getSuperpackage <em>Superpackage</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Superpackage</em>' container reference.
	 * @see #getSuperpackage()
	 * @generated
	 */
	void setSuperpackage(Package value);


	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.jwt.meta.model.core.PackageableElement}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.jwt.meta.model.core.PackageableElement#getPackage <em>Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see org.eclipse.jwt.meta.model.core.CorePackage#getPackage_Elements()
	 * @see org.eclipse.jwt.meta.model.core.PackageableElement#getPackage
	 * @model opposite="package" containment="true"
	 * @generated
	 */
	EList<PackageableElement> getElements();

} // Package