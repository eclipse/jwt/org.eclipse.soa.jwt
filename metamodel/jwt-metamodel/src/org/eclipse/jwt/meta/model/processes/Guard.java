/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.processes;

import org.eclipse.jwt.meta.model.core.NamedElement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Guard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.meta.model.processes.Guard#getTextualdescription <em>Textualdescription</em>}</li>
 *   <li>{@link org.eclipse.jwt.meta.model.processes.Guard#getShortdescription <em>Shortdescription</em>}</li>
 *   <li>{@link org.eclipse.jwt.meta.model.processes.Guard#getDetailedSpecification <em>Detailed Specification</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.meta.model.processes.ProcessesPackage#getGuard()
 * @model
 * @generated
 */
public interface Guard
		extends NamedElement
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * Returns the value of the '<em><b>Textualdescription</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Textualdescription</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Textualdescription</em>' attribute.
	 * @see #setTextualdescription(String)
	 * @see org.eclipse.jwt.meta.model.processes.ProcessesPackage#getGuard_Textualdescription()
	 * @model
	 * @generated
	 */
	String getTextualdescription();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.meta.model.processes.Guard#getTextualdescription <em>Textualdescription</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Textualdescription</em>' attribute.
	 * @see #getTextualdescription()
	 * @generated
	 */
	void setTextualdescription(String value);


	/**
	 * Returns the value of the '<em><b>Shortdescription</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shortdescription</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shortdescription</em>' attribute.
	 * @see #setShortdescription(String)
	 * @see org.eclipse.jwt.meta.model.processes.ProcessesPackage#getGuard_Shortdescription()
	 * @model
	 * @generated
	 */
	String getShortdescription();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.meta.model.processes.Guard#getShortdescription <em>Shortdescription</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shortdescription</em>' attribute.
	 * @see #getShortdescription()
	 * @generated
	 */
	void setShortdescription(String value);


	/**
	 * Returns the value of the '<em><b>Detailed Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detailed Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detailed Specification</em>' containment reference.
	 * @see #setDetailedSpecification(GuardSpecification)
	 * @see org.eclipse.jwt.meta.model.processes.ProcessesPackage#getGuard_DetailedSpecification()
	 * @model containment="true"
	 * @generated
	 */
	GuardSpecification getDetailedSpecification();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.meta.model.processes.Guard#getDetailedSpecification <em>Detailed Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detailed Specification</em>' containment reference.
	 * @see #getDetailedSpecification()
	 * @generated
	 */
	void setDetailedSpecification(GuardSpecification value);

} // Guard