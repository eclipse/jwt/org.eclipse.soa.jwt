/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.processes;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jwt.meta.model.data.ParameterMapping;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity Link Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.meta.model.processes.ActivityLinkNode#getLinksto <em>Linksto</em>}</li>
 *   <li>{@link org.eclipse.jwt.meta.model.processes.ActivityLinkNode#getUses <em>Uses</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.meta.model.processes.ProcessesPackage#getActivityLinkNode()
 * @model
 * @generated
 */
public interface ActivityLinkNode
		extends ExecutableNode
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * Returns the value of the '<em><b>Linksto</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linksto</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linksto</em>' reference.
	 * @see #setLinksto(Activity)
	 * @see org.eclipse.jwt.meta.model.processes.ProcessesPackage#getActivityLinkNode_Linksto()
	 * @model required="true"
	 * @generated
	 */
	Activity getLinksto();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.meta.model.processes.ActivityLinkNode#getLinksto <em>Linksto</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Linksto</em>' reference.
	 * @see #getLinksto()
	 * @generated
	 */
	void setLinksto(Activity value);


	/**
	 * Returns the value of the '<em><b>Uses</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.jwt.meta.model.data.ParameterMapping}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uses</em>' containment reference list.
	 * @see org.eclipse.jwt.meta.model.processes.ProcessesPackage#getActivityLinkNode_Uses()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ParameterMapping> getUses();

} // ActivityLinkNode