/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.processes.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.meta.PluginProperties;
import org.eclipse.jwt.meta.model.core.provider.ModelElementItemProvider;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;
import org.eclipse.jwt.meta.model.processes.ProcessesFactory;
import org.eclipse.jwt.meta.model.processes.ProcessesPackage;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;

/**
 * This is the item provider adapter for a {@link org.eclipse.jwt.meta.model.processes.ActivityEdge} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ActivityEdgeItemProvider extends ModelElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityEdgeItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);
		}

		addSourcePropertyDescriptor(object);
		addTargetPropertyDescriptor(object);
		return itemPropertyDescriptors;
	}


	/**
	 * This adds a property descriptor for the Source feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourcePropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ProcessesPackage.Literals.ACTIVITY_EDGE__SOURCE)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.ACTIVITY_EDGE__SOURCE))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ProcessesPackage.Literals.ACTIVITY_EDGE__SOURCE),

							PluginProperties
									.model_feature_description(ProcessesPackage.Literals.ACTIVITY_EDGE__SOURCE),

							ProcessesPackage.Literals.ACTIVITY_EDGE__SOURCE, false,
							false, false, null, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Target feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ProcessesPackage.Literals.ACTIVITY_EDGE__TARGET)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ProcessesPackage.Literals.ACTIVITY_EDGE__TARGET))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ProcessesPackage.Literals.ACTIVITY_EDGE__TARGET),

							PluginProperties
									.model_feature_description(ProcessesPackage.Literals.ACTIVITY_EDGE__TARGET),

							ProcessesPackage.Literals.ACTIVITY_EDGE__TARGET, false,
							false, false, null, null, null));
		}
	}


	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(ProcessesPackage.Literals.ACTIVITY_EDGE__GUARD);
		}
		return childrenFeatures;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}


	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param object
	 * @return The text.
	 * @generated NOT
	 */
	@Override
	public String getText(Object object)
	{
		String type = PluginProperties.model_type(object);
		ActivityEdge activityEdge = (ActivityEdge) object;

		String inName = null;
		if (activityEdge.getSource() != null)
		{
			IItemLabelProvider itemLabelProvider = (IItemLabelProvider) getAdapterFactory()
					.adapt(activityEdge.getSource(), IItemLabelProvider.class);

			if (itemLabelProvider != null)
			{
				inName = itemLabelProvider.getText(activityEdge.getSource());
			}
		}

		String outName = null;
		if (activityEdge.getTarget() != null)
		{
			IItemLabelProvider itemLabelProvider = (IItemLabelProvider) getAdapterFactory()
					.adapt(activityEdge.getTarget(), IItemLabelProvider.class);

			if (itemLabelProvider != null)
			{
				outName = itemLabelProvider.getText(activityEdge.getTarget());
			}
		}

		return PluginProperties.model_text(ActivityEdge.class, new Object[]
		{ type, inName, outName });
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(ActivityEdge.class))
		{
			case ProcessesPackage.ACTIVITY_EDGE__GUARD:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}


	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ProcessesPackage.Literals.ACTIVITY_EDGE__GUARD,
				ProcessesFactory.eINSTANCE.createGuard()));
	}

}
