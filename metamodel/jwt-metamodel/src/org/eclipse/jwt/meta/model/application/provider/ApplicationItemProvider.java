/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.application.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.jwt.meta.PluginProperties;
import org.eclipse.jwt.meta.model.application.Application;
import org.eclipse.jwt.meta.model.application.ApplicationPackage;
import org.eclipse.jwt.meta.model.core.provider.ReferenceableElementItemProvider;
import org.eclipse.jwt.meta.model.data.DataFactory;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;

/**
 * This is the item provider adapter for a {@link org.eclipse.jwt.meta.model.application.Application} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ApplicationItemProvider extends ReferenceableElementItemProvider implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */

	public static final String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplicationItemProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}


	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
	{
		if (itemPropertyDescriptors == null)
		{
			super.getPropertyDescriptors(object);
		}

		addTypePropertyDescriptor(object);
		addJarArchivePropertyDescriptor(object);
		addJavaClassPropertyDescriptor(object);
		addMethodPropertyDescriptor(object);
		addInputPropertyDescriptor(object);
		addOutputPropertyDescriptor(object);
		return itemPropertyDescriptors;
	}


	/**
	 * This adds a property descriptor for the Type feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ApplicationPackage.Literals.APPLICATION__TYPE)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ApplicationPackage.Literals.APPLICATION__TYPE))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ApplicationPackage.Literals.APPLICATION__TYPE),

							PluginProperties
									.model_feature_description(ApplicationPackage.Literals.APPLICATION__TYPE),

							ApplicationPackage.Literals.APPLICATION__TYPE, true, false,
							false, null, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Jar Archive feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addJarArchivePropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ApplicationPackage.Literals.APPLICATION__JAR_ARCHIVE)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ApplicationPackage.Literals.APPLICATION__JAR_ARCHIVE))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ApplicationPackage.Literals.APPLICATION__JAR_ARCHIVE),

							PluginProperties
									.model_feature_description(ApplicationPackage.Literals.APPLICATION__JAR_ARCHIVE),

							ApplicationPackage.Literals.APPLICATION__JAR_ARCHIVE, true,
							false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
							null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Java Class feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addJavaClassPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ApplicationPackage.Literals.APPLICATION__JAVA_CLASS)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ApplicationPackage.Literals.APPLICATION__JAVA_CLASS))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ApplicationPackage.Literals.APPLICATION__JAVA_CLASS),

							PluginProperties
									.model_feature_description(ApplicationPackage.Literals.APPLICATION__JAVA_CLASS),

							ApplicationPackage.Literals.APPLICATION__JAVA_CLASS, true,
							false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
							null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Method feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMethodPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ApplicationPackage.Literals.APPLICATION__METHOD)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ApplicationPackage.Literals.APPLICATION__METHOD))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ApplicationPackage.Literals.APPLICATION__METHOD),

							PluginProperties
									.model_feature_description(ApplicationPackage.Literals.APPLICATION__METHOD),

							ApplicationPackage.Literals.APPLICATION__METHOD, true, false,
							false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Input feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInputPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ApplicationPackage.Literals.APPLICATION__INPUT)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ApplicationPackage.Literals.APPLICATION__INPUT))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ApplicationPackage.Literals.APPLICATION__INPUT),

							PluginProperties
									.model_feature_description(ApplicationPackage.Literals.APPLICATION__INPUT),

							ApplicationPackage.Literals.APPLICATION__INPUT, true, false,
							false, null, null, null));
		}
	}


	/**
	 * This adds a property descriptor for the Output feature.
	 * [JWT] Edit (by Wolf Fischer):
	 * The implementation has been changed to check if this feature should be shown or not
	 * (depending on the selected view).
	 * This means, that the feature is deleted from the itemPropertyDescriptors list on every call and
	 * only if it should be shown, added to the list again!
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOutputPropertyDescriptor(Object object)
	{
		for (int i = 0; i < itemPropertyDescriptors.size(); i++)
		{
			ItemPropertyDescriptor ipd = (ItemPropertyDescriptor) itemPropertyDescriptors
					.get(i);
			if (ipd.getFeature(null) == ApplicationPackage.Literals.APPLICATION__OUTPUT)
			{
				itemPropertyDescriptors.remove(i);
				break;
			}
		}

		if (ResourceProviderRegistry.getInstance().displayObject(
				ApplicationPackage.Literals.APPLICATION__OUTPUT))
		{
			itemPropertyDescriptors
					.add(createItemPropertyDescriptor(
							((ComposeableAdapterFactory) adapterFactory)
									.getRootAdapterFactory(),
							getResourceLocator(),
							PluginProperties
									.model_feature_name(ApplicationPackage.Literals.APPLICATION__OUTPUT),

							PluginProperties
									.model_feature_description(ApplicationPackage.Literals.APPLICATION__OUTPUT),

							ApplicationPackage.Literals.APPLICATION__OUTPUT, true, false,
							false, null, null, null));
		}
	}


	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
	{
		if (childrenFeatures == null)
		{
			super.getChildrenFeatures(object);
			childrenFeatures.add(ApplicationPackage.Literals.APPLICATION__INPUT);
			childrenFeatures.add(ApplicationPackage.Literals.APPLICATION__OUTPUT);
		}
		return childrenFeatures;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child)
	{
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}


	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT!
	 */
	@Override
	public String getText(Object object)
	{
		return super.getText(object);
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification)
	{
		updateChildren(notification);

		switch (notification.getFeatureID(Application.class))
		{
			case ApplicationPackage.APPLICATION__JAR_ARCHIVE:
			case ApplicationPackage.APPLICATION__JAVA_CLASS:
			case ApplicationPackage.APPLICATION__METHOD:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), false, true));
				return;
			case ApplicationPackage.APPLICATION__INPUT:
			case ApplicationPackage.APPLICATION__OUTPUT:
				fireNotifyChanged(new ViewerNotification(notification, notification
						.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}


	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection newChildDescriptors,
			Object object)
	{
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ApplicationPackage.Literals.APPLICATION__INPUT, DataFactory.eINSTANCE
						.createInputParameter()));

		newChildDescriptors.add(createChildParameter(
				ApplicationPackage.Literals.APPLICATION__OUTPUT, DataFactory.eINSTANCE
						.createOutputParameter()));
	}

}
