/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.data;

import org.eclipse.jwt.meta.model.core.PackageableElement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.meta.model.data.DataMapping#getBoundParameter <em>Bound Parameter</em>}</li>
 *   <li>{@link org.eclipse.jwt.meta.model.data.DataMapping#getParameter <em>Parameter</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.meta.model.data.DataPackage#getDataMapping()
 * @model
 * @generated
 */
public interface DataMapping
		extends PackageableElement
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * Returns the value of the '<em><b>Bound Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bound Parameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bound Parameter</em>' reference.
	 * @see #setBoundParameter(Parameter)
	 * @see org.eclipse.jwt.meta.model.data.DataPackage#getDataMapping_BoundParameter()
	 * @model required="true"
	 * @generated
	 */
	Parameter getBoundParameter();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.meta.model.data.DataMapping#getBoundParameter <em>Bound Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bound Parameter</em>' reference.
	 * @see #getBoundParameter()
	 * @generated
	 */
	void setBoundParameter(Parameter value);


	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' reference.
	 * @see #setParameter(Parameter)
	 * @see org.eclipse.jwt.meta.model.data.DataPackage#getDataMapping_Parameter()
	 * @model required="true"
	 * @generated
	 */
	Parameter getParameter();


	/**
	 * Sets the value of the '{@link org.eclipse.jwt.meta.model.data.DataMapping#getParameter <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' reference.
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(Parameter value);

} // DataMapping