/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg
 *      - view support
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *      - maintenance and extension 
 *******************************************************************************/
package org.eclipse.jwt.meta.model.functions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jwt.meta.model.core.PackageableElement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.jwt.meta.model.functions.Function#getSubfunctions <em>Subfunctions</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.jwt.meta.model.functions.FunctionsPackage#getFunction()
 * @model
 * @generated
 */
public interface Function
		extends PackageableElement
{

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright (c) 2005-2012  Eclipse Java Workflow Tooling (JWT) Project <www.eclipse.org/jwt>";


	/**
	 * Returns the value of the '<em><b>Subfunctions</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.jwt.meta.model.functions.Function}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subfunctions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subfunctions</em>' containment reference list.
	 * @see org.eclipse.jwt.meta.model.functions.FunctionsPackage#getFunction_Subfunctions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Function> getSubfunctions();

} // Function