/**
 * File:    IImageProvider.java
 * Created: 20.01.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.meta.providers.interfaces;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;

/**
 * Interface that represents an image provider for model elements.
 * 
 * @version $Id: ICommandProvider.java,v 1.4 2009-12-10 13:27:17 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of
 *         Augsburg, Germany, www.ds-lab.org
 */
public interface ICommandProvider
{

	// command types for recognizing itemprovider commands
	public static String CT_ACTION_EXECUTEDBYPERFORMEDBY_SET = "CT_ACTION_EXECUTEDBYPERFORMEDBY_SET"; //$NON-NLS-1$
	public static String CT_ACTION_INPUTSOUTPUTS_ADD = "CT_ACTION_INPUTSOUTPUTS_ADD"; //$NON-NLS-1$
	public static String CT_ACTION_INPUTSOUTPUTS_REMOVE = "CT_ACTION_INPUTSOUTPUTS_REMOVE"; //$NON-NLS-1$
	public static String CT_SCOPE_ACTIVITYNODE_REMOVE = "CT_SCOPE_ACTIVITYNODE_REMOVE"; //$NON-NLS-1$
	public static String CT_SCOPE_ACTIVITYEDGE_REMOVE = "CT_SCOPE_ACTIVITYEDGE_REMOVE"; //$NON-NLS-1$
	public static String CT_PACKAGE_ELEMENTS_REMOVE = "CT_PACKAGE_ELEMENTS_REMOVE"; //$NON-NLS-1$
	public static String CT_PACKAGE_SUBPACKAGES_REMOVE = "CT_PACKAGE_SUBPACKAGES_REMOVE"; //$NON-NLS-1$


	/**
	 * Returns a command created by the editing domain.
	 * 
	 * @param domain
	 * @param commandClass
	 * @param commandParameter
	 * @return The command.
	 */
	public Command createCommand(AdapterFactoryEditingDomain domain, Class commandClass,
			CommandParameter commandParameter);


	/**
	 * Returns a command created by an item provider.
	 * 
	 * @param commandType
	 * @param domain
	 * @param owner
	 * @param feature
	 * @param data
	 * @return The command.
	 */
	public Command createProviderCommand(String commandType, EditingDomain domain,
			Object owner, EStructuralFeature feature, Object data);

}
