/**
 * File:    IImageProvider.java
 * Created: 20.01.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.meta.providers.interfaces;

import org.eclipse.jface.resource.ImageDescriptor;


/**
 * Interface that represents an image provider for model elements.
 * 
 * @version $Id: IImageProvider.java,v 1.3 2009-11-26 15:16:45 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public interface IImageProvider
{

	/**
	 * Creates an image descriptor for model elements.
	 * 
	 * @param object
	 * @return The image descriptor.
	 */
	public ImageDescriptor createImageDescriptor(Object object);

}
