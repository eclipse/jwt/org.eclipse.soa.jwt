/**
 * File:    IVisibilityProvider.java
 * Created: 20.01.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/
package org.eclipse.jwt.meta.providers.interfaces;

/**
 * Interface that represents a visibility provider for model elements.
 * 
 * @version $Id: IVisibilityProvider.java,v 1.2 2009-11-26 15:16:47 chsaad Exp $
 * @author Christian Saad, Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public interface IVisibilityProvider
{

	/**
	 * Determines whether an object is visible.
	 * 
	 * @param object
	 * @return Visibility status.
	 */
	public boolean displayObject(Object object);

}
