/**
 * File:    DefaultModelProvider.java
 * Created: 20.01.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.meta.providers.standard;

import java.util.Collection;

import org.eclipse.jwt.meta.providers.interfaces.IModelProvider;


/**
 * The default model provider does not modify the list of a model object's children.
 * 
 * @version $Id: DefaultModelProvider.java,v 1.2 2009-11-26 15:16:41 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class DefaultModelProvider
		implements IModelProvider
{

	/* (non-Javadoc)
	 * @see org.eclipse.jwt.meta.providers.interfaces.IModelProvider#getModelChildren(java.lang.Object, java.util.Collection)
	 */
	public Collection getModelChildren(Object object, Collection standardChildren)
	{
		return standardChildren;
	}

}
