/**
 * File:    DefaultCommandProvider.java
 * Created: 20.01.2009
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.meta.providers.standard;

import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.CopyToClipboardCommand;
import org.eclipse.emf.edit.command.CutToClipboardCommand;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.meta.commands.JWTDeleteCommand;
import org.eclipse.jwt.meta.commands.clipboard.JWTCopyToClipboardCommand;
import org.eclipse.jwt.meta.commands.clipboard.JWTCutToClipboardCommand;
import org.eclipse.jwt.meta.commands.processes.RemoveActivityEdgeCommand;
import org.eclipse.jwt.meta.commands.processes.RemoveActivityNodeCommand;
import org.eclipse.jwt.meta.providers.interfaces.ICommandProvider;


/**
 * The default command provider substitutes some standard commands with special JWT
 * commands.
 * 
 * @version $Id: DefaultCommandProvider.java,v 1.2 2009-11-26 15:16:41 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class DefaultCommandProvider
		implements ICommandProvider
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.meta.providers.interfaces.ICommandProvider#createCommand(java.lang
	 * .Class, org.eclipse.emf.edit.command.CommandParameter)
	 */
	public Command createCommand(AdapterFactoryEditingDomain domain, Class commandClass,
			CommandParameter commandParameter)
	{
		// delegate to custom CutToClipboardCommand (cuts and copies also the edges)
		if (commandClass == CutToClipboardCommand.class)
		{
			return new JWTCutToClipboardCommand(domain, RemoveCommand.create(domain,
					commandParameter.getOwner(), commandParameter.getFeature(),
					commandParameter.getCollection()));
		}
		// delegate to custom CopyToClipboardCommand (copies also the edges)
		if (commandClass == CopyToClipboardCommand.class)
		{
			return new JWTCopyToClipboardCommand(domain, commandParameter.getCollection());
		}
		// delegate to custom DeleteCommand (checks if elements are still referenced)
		else if (commandClass == DeleteCommand.class)
		{
			return new JWTDeleteCommand(domain, commandParameter.getCollection());
		}

		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jwt.meta.providers.interfaces.ICommandProvider#createProviderCommand
	 * (java.lang.String, org.eclipse.emf.edit.domain.EditingDomain, java.lang.Object,
	 * org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object)
	 */
	public Command createProviderCommand(String commandType, EditingDomain domain,
			Object owner, EStructuralFeature feature, Object data)
	{
		if (commandType.equals(ICommandProvider.CT_SCOPE_ACTIVITYNODE_REMOVE))
		{
			return new RemoveActivityNodeCommand(domain, (EObject) owner, feature,
					(Collection) data);
		}
		else if (commandType.equals(ICommandProvider.CT_SCOPE_ACTIVITYEDGE_REMOVE))
		{
			return new RemoveActivityEdgeCommand(domain, (EObject) owner, feature,
					(Collection) data);
		}

		return null;
	}

}
