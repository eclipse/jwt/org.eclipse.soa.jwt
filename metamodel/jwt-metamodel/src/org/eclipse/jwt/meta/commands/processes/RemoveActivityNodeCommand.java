/**
 * File:    RemoveActivityNodeCommand.java
 * Created: 09.06.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- rewrote from scratch
 *******************************************************************************/

package org.eclipse.jwt.meta.commands.processes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;
import org.eclipse.jwt.meta.model.processes.ActivityNode;
import org.eclipse.jwt.meta.model.processes.Scope;

/**
 * Removes an {@link ActivityNode} from its {@link Scope}.
 * 
 * This command also removes all {@link ActivityEdge}s and {@link ReferenceEdge}
 * s connected to the node by generating corresponding RemoveCommands.
 * 
 * @version $Id: RemoveActivityNodeCommand.java,v 1.3 2009/11/26 15:16:21 chsaad
 *          Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class RemoveActivityNodeCommand extends RemoveCommand
{

	/**
	 * The Editing Domain.
	 */
	private EditingDomain editingDomain;

	/**
	 * The commands to remove the corresponding edges.
	 */
	protected CompoundCommand removeEdgesCommands;


	/**
	 * Constructor.
	 * 
	 * @param domain
	 * @param owner
	 * @param feature
	 * @param collection
	 */
	public RemoveActivityNodeCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection<?> collection)
	{
		super(domain, owner, feature, collection);

		this.editingDomain = domain;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doExecute()
	 */
	@Override
	public void doExecute()
	{
		// some activitynodes may already be deleted by CustomDeleteCommand if
		// the
		// corresponding activity was deleted
		ArrayList newCollection = new ArrayList();
		for (Iterator iterator = collection.iterator(); iterator.hasNext();)
		{
			ActivityNode actNode = (ActivityNode) iterator.next();

			if (actNode.eContainer() != null)
			{
				newCollection.add(actNode);
			}
		}
		collection = newCollection;

		// prepare remove edges
		removeEdgesCommands = new CompoundCommand();

		// for every ActivityNode to be deleted
		for (Iterator i = collection.iterator(); i.hasNext();)
		{
			ActivityNode node = (ActivityNode) i.next();

			// create RemoveCommands for In/Out ActivityEdges
			if (!node.getIn().isEmpty())
			{
				removeEdgesCommands.append(DeleteCommand.create(editingDomain, node
						.getIn()));
			}
			if (!node.getOut().isEmpty())
			{
				removeEdgesCommands.append(DeleteCommand.create(editingDomain, node
						.getOut()));
			}
		}

		// remove all ActivityEdges and ReferenceEdges
		if (removeEdgesCommands.canExecute())
		{
			removeEdgesCommands.execute();
		}

		// remove the action
		doExecuteOriginal();
	}


	protected void doExecuteOriginal()
	{
		super.doExecute();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doRedo()
	 */
	@Override
	public void doRedo()
	{
		// remove all ActivityEdges and ReferenceEdges
		if (!removeEdgesCommands.isEmpty())
		{
			removeEdgesCommands.redo();
		}

		// remove the action
		super.doRedo();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doUndo()
	 */
	@Override
	public void doUndo()
	{
		// restore the action
		super.doUndo();

		// restore all ActivityEdges and ReferenceEdges
		if (!removeEdgesCommands.isEmpty())
		{
			removeEdgesCommands.undo();
		}
	}


	@Override
	public void doDispose()
	{
		super.doDispose();

		if (removeEdgesCommands != null)
		{
			removeEdgesCommands.dispose();
		}
	}

}