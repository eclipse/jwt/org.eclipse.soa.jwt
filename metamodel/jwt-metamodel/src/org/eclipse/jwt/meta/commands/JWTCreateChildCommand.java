/**
 * File:    JWTCreateChildCommand.java
 * Created: 27.03.2006
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.meta.commands;

import java.util.Collections;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandWrapper;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.domain.EditingDomain;


/**
 * Adds a better create command to the EMF CreateChildCommand because it is too
 * specialised for EMF editors.
 * 
 * @version $Id: JWTCreateChildCommand.java,v 1.3 2009-11-26 15:16:50 chsaad Exp $
 * @author Markus Bauer (markusbauer@users.sourceforge.net), Programming distributed
 *         Systems Lab, University of Augsburg, Germany, www.ds-lab.org
 */
public class JWTCreateChildCommand
		extends CommandWrapper
{

	/**
	 * The EMF EditingDomain used to create the command.
	 */
	private EditingDomain editingDomain;

	/**
	 * The parent of the new child.
	 */
	private Object parent;

	/**
	 * The feature that bounds the child to its parent.
	 */
	private Object feature;

	/**
	 * The new child.
	 */
	private Object newChild;


	/**
	 * @param editingDomain
	 *            The EMF EditingDomain used to create the command.
	 * @param parent
	 *            The parent of the new child.
	 * @param feature
	 *            The feature that bounds the child to its parent.
	 * @param request
	 *            The request to create the new child.
	 */
	public JWTCreateChildCommand(EditingDomain editingDomain, Object parent,
			Object feature, Object newChild)
	{
		this.editingDomain = editingDomain;
		this.parent = parent;
		this.feature = feature;
		this.newChild = newChild;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.common.command.CommandWrapper#createCommand()
	 */
	@Override
	protected Command createCommand()
	{
		CommandParameter childDescriptor = new CommandParameter(null, feature, newChild);
		return org.eclipse.emf.edit.command.CreateChildCommand.create(editingDomain,
				parent, childDescriptor, Collections.singleton(parent));
	}
}
