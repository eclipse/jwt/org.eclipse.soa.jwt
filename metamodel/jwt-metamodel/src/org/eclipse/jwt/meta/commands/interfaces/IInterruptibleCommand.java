/**
 * File:    IInterruptibleCommand.java
 * Created: 20.01.2008
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.meta.commands.interfaces;

/**
 * Commands that implement this interface provide a method which gets called immediately
 * before the command is executed. If the method returns false, the command is not
 * executed. This can be used to avoid the commandstack pushing the command on the stack
 * although it should not be executed. This interface should be used when the command
 * should not be disabled but rather reacts to the current situation at execution time.
 * 
 * @version $Id: IInterruptibleCommand.java,v 1.2 2009-11-26 15:16:51 chsaad Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab, University
 *         of Augsburg, Germany, www.ds-lab.org
 */
public interface IInterruptibleCommand
{

	/**
	 * This function gets called by EmfToGefCommandStackAdapter right before the command
	 * is executed on the command stack. The difference to the prepare method is, that
	 * this method gets called directly before the command gets executed (and not when the
	 * command is created) and offers a "last minute" possibility to prohibit the
	 * execution on the command stack.
	 * 
	 * @return false if the command should be aborted
	 */
	public boolean checkIfCommandCanBeExecuted();
}
