/**
 * File:    RemoveActivityEdgeCommand.java
 * Created: 12.06.2008
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.meta.commands.processes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;

/**
 * This command removes an {@link ActivityEdge}. Before it is executed, it
 * checks if the edge has already been deleted (e.g. by a
 * RemoveActivityNodeCommand which automatically deletes attached edges).
 * 
 * @version $Id: RemoveActivityEdgeCommand.java,v 1.2 2009/11/26 15:16:21 chsaad
 *          Exp $
 * @author Christian Saad (csaad@gmx.net), Programming distributed Systems Lab,
 *         University of Augsburg, Germany, www.ds-lab.org
 */
public class RemoveActivityEdgeCommand extends RemoveCommand
{

	/**
	 * The constructor.
	 * 
	 * @param domain
	 * @param owner
	 * @param feature
	 * @param collection
	 */
	public RemoveActivityEdgeCommand(EditingDomain domain, EObject owner,
			EStructuralFeature feature, Collection<?> collection)
	{
		super(domain, owner, feature, collection);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.command.RemoveCommand#doExecute()
	 */
	@Override
	public void doExecute()
	{
		// some activityedges may already be deleted by
		// RemoveActivityNodeCommand. these
		// must be removed from the objects to be deleted
		ArrayList newCollection = new ArrayList();
		for (Iterator iterator = collection.iterator(); iterator.hasNext();)
		{
			ActivityEdge actEdge = (ActivityEdge) iterator.next();

			if (actEdge.eContainer() != null)
			{
				newCollection.add(actEdge);
			}
		}
		collection = newCollection;

		// remove activity edges
		super.doExecute();
	}

}