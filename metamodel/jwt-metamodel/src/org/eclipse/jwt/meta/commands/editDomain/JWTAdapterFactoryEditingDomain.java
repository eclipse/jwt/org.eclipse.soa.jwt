/**
 * File:    JWTAdapterFactoryEditingDomain.java
 * Created: 30.11.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.meta.commands.editDomain;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.jwt.meta.providers.ResourceProviderRegistry;


/**
 * Adapter Factory that extends the standard {@link AdapterFactoryEditingDomain}
 * 
 * @version $Id: WEAdapterFactoryEditingDomain.java,v 1.5 2008/01/31 09:51:27 flautenba
 *          Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class JWTAdapterFactoryEditingDomain
		extends AdapterFactoryEditingDomain
{

	/**
	 * The constructor.
	 * 
	 * @param adapterFactory
	 * @param commandStack
	 */
	public JWTAdapterFactoryEditingDomain(AdapterFactory adapterFactory,
			CommandStack commandStack)
	{
		super(adapterFactory, commandStack);
	}


	/**
	 * This method delegates deletion commands to custom implementations.
	 */
	@Override
	public Command createCommand(Class commandClass, CommandParameter commandParameter)
	{
		Command customCommand = ResourceProviderRegistry.getInstance().createCommand(this,
				commandClass, commandParameter);

		// return custom command if available
		if (customCommand != null)
		{
			return customCommand;
		}

		// delegate to standard handler
		return super.createCommand(commandClass, commandParameter);
	}
}
