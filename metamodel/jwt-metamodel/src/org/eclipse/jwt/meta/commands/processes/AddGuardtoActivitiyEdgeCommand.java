/**
 * File:    AddGuardtoActivitiyEdgeCommand.java
 * Created: 13.10.2006
 *
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wolf Fischer, Programming distributed Systems Lab, University of Augsburg 
 *    	- initial API and implementation
 *******************************************************************************/

package org.eclipse.jwt.meta.commands.processes;

import org.eclipse.emf.common.command.AbstractCommand;
import org.eclipse.jwt.meta.model.processes.ActivityEdge;
import org.eclipse.jwt.meta.model.processes.Guard;


/**
 * Command for adding a Guard to an {@link ActivityEdge}
 * 
 * @version $Id: AddGuardtoActivitiyEdgeCommand.java,v 1.2 2008/09/30 07:53:36 flautenba
 *          Exp $
 * @author Wolf Fischer, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class AddGuardtoActivitiyEdgeCommand
		extends AbstractCommand
{

	/**
	 * The activity edge.
	 */
	private ActivityEdge edge = null;

	/**
	 * The new object.
	 */
	private Object newObject;


	/**
	 * 
	 * @param ae
	 *            The Activityedge, where the Guard should be added
	 * @param request
	 *            The original Request
	 */
	public AddGuardtoActivitiyEdgeCommand(ActivityEdge ae, Object newObject)
	{
		super();
		edge = ae;
		this.newObject = newObject;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute()
	{
		if ((edge != null) && (newObject != null))
			return true;
		return false;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	public void execute()
	{
		redo();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo()
	{
		edge.setGuard(null);
	}


	public void redo()
	{
		edge.setGuard((Guard) newObject);
	}
}
