/**
 * File:    Plugin.java
 * Created: 09.01.2009
 *
/*******************************************************************************
 * Copyright (c) 2005-2012
 * University of Augsburg, Germany <www.ds-lab.org>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christian Saad, Programming distributed Systems Lab, University of Augsburg
 *
 *******************************************************************************/
package org.eclipse.jwt.meta;

import org.eclipse.emf.common.ui.EclipseUIPlugin;
import org.osgi.framework.BundleContext;


/**
 * The activator class controls the plug-in life cycle.
 * 
 * @version $Id: Plugin.java,v 1.2 2009-11-26 15:16:51 chsaad Exp $
 * @author Christian Saad, Programming distributed Systems Lab, University of Augsburg,
 *         Germany, www.ds-lab.org
 */
public class Plugin
		extends EclipseUIPlugin
{

	/**
	 * The shared instance.
	 */
	private static Plugin plugin;


	/**
	 * The constructor.
	 */
	public Plugin()
	{
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception
	{
		super.start(context);
		plugin = this;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception
	{
		plugin = null;
		super.stop(context);
	}


	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Plugin getDefault()
	{
		return plugin;
	}


	/**
	 * @return The ID of this plugin.
	 */
	public static String getId()
	{
		if (getInstance() == null)
		{
			return Plugin.class.getPackage().getName();
		}
		return getInstance().getBundle().getSymbolicName();
	}


	/**
	 * @return The shared instance.
	 */
	public static Plugin getInstance()
	{
		return plugin;
	}
}
