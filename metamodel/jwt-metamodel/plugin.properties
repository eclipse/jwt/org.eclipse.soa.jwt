#
# File:    plugin.properties
# Created: 02.02.2006
#
#
#/*******************************************************************************
# * Copyright (c) 2005-2012
# * University of Augsburg, Germany, <www.ds-lab.org>
# *
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Markus Bauer, Programming distributed Systems Lab, University of Augsburg 
# *    	- initial API and implementation
# *******************************************************************************/
#
################################################################################


################################################################################
#
# Plugin data
#

plugin_Name = JWT Meta Model
plugin_ProviderName = http://www.eclipse.org/jwt
pluginName = JWT
providerName = http://www.eclipse.org/jwt
category_id = Java Workflow Tooling


################################################################################
#
# Model
#

model_DefaultPackage_name = <default>
model_PackageName_separator = /

# {0}: model type.
model_Unnamed_name = <?>


#
# Types:
#
# Name for the model and attribute types. The default is the typename itself.
#
# Example:
#   model_Foo_type = FooType
#   model_Bar_datatype = Bar data type
#

model_ModelElement_type = Model Element
model_Comment_type = Comment
model_NamedElement_type = Named Element
model_Package_type = Package
model_Model_type = Model
model_PackageableElement_type = Packageable Element
model_ReferenceableElement_type = Referenceable Element

model_Scope_type = Scope
model_Activity_type = Activity
model_StructuredActivityNode_type = Embedded Subprocess
model_ActivityLinkNode_type = Subprocess Call
model_ActivityNode_type = Activity Node
model_ActivityEdge_type = Activity Edge
model_GuardSpecification_type = Guard Specification
model_Guard_type = Guard
model_ExecutableNode_type = Executable Node
model_ControlNode_type = Control Node
model_Action_type = Action
model_InitialNode_type = Initial Node
model_FinalNode_type = Final Node
model_DecisionNode_type = Decision Node
model_ForkNode_type = Fork Node
model_JoinNode_type = Join Node
model_MergeNode_type = Merge Node
model_ParameterMapping_type = Parameter Mapping

model_Event_type = Event
model_EventHandler_type = Event Handler

model_PrimitiveType_type = Primitive Type
model_StringType_type = String
model_IntegerType_type = Integer

model_Function_type = Function

model_Role_type = Role
model_OrganisationUnit_type = Organisation Unit

model_Application_type = Application
model_WebServiceApplication_type = WebService Application
model_ApplicationType_type = Application Type

model_Data_type = Data
model_DataType_type = Data Type
model_Parameter_type = Parameter
model_InputParameter_type = Input Parameter
model_OutputParameter_type = Output Parameter
model_DataMapping_type = Data Mapping
model_InformationType_type = Information Type

model_GraphicalElement_type = Graphical Element


#
# Features:
#
# Name and description for features of model elements.
#
# The default name for a feature is the name of the feature.
#
# The default description is taken from the property:
# model_Default_feature_description
# where:
# {0}: Name of the type.
# {1}: Name of the feature
#
#
# Examples:
#   model_Foo_bar_name = bar of Foo
#   model_Foo_bar_description = The bar of the Foo.
#   model_Default_feature_description = The {0} of the {1}
#


# unknown feature
model_Unknown_feature_name = unknown

model_ModelElement_ownedComment_name = Comment
model_Comment_text_name = Text

model_NamedElement_name_name = Name
model_NamedElement_icon_name = Icon

model_Package_subpackages_name = Subpackages
model_Package_superpackage_name = Superpackage
model_Package_elements_name = Elements

model_Model_author_name = Author
model_Model_version_name = Version
model_Model_description_name = Description
model_Model_fileversion_name = File format version

model_PackageableElement_package_name = Package

model_Scope_nodes_name = Nodes
model_Scope_edges_name = Transitions

model_Activity_eventHandler_name = Event Handler
model_Activity_totalexecutiontime_name = Total Time (in seconds)

model_ActivityNode_in_name = Ingoing Transitions
model_ActivityNode_out_name = Outgoing Transitions

model_ActivityLinkNode_linksto_name = Links to
model_ActivityLinkNode_uses_name = Mapped Parameters

model_ActivityEdge_source_name = Source Node
model_ActivityEdge_target_name = Target Node
model_ActivityEdge_guardConditions_name = Guard Conditions

model_GuardSpecification_data_name = Data
model_GuardSpecification_attribute_name = Attribute
model_GuardSpecification_operation_name = Operation
model_GuardSpecification_value_name = Value
model_GuardSpecification_Description_name = Description
model_GuardSpecification_subSpecificationConnector_name = Sub specification connector

model_Guard_textualdescription_name = Textual Description
model_Guard_shortdescription_name = Short Description

model_Action_performedBy_name = Performed by Role
model_Action_executedBy_name = Executed by Application
model_Action_realizes_name = Realizes
model_Action_inputs_name = Input Data
model_Action_outputs_name = Output Data
model_Action_mappings_name = Mappings
model_Action_targetexecutiontime_name = Target Time (in seconds)

model_Function_subfunctions_name = Subfunctions

model_Role_performedBy_name = Performed by
model_OrganisationUnit_subUnit_name = Subunit
model_OrganisationUnit_belongsTo_name = Belongs to

model_ParameterMapping_source_name = Element in this process
model_ParameterMapping_target_name = Element in subprocess

model_Application_type_name = Type
model_Application_jarArchive_name = JAR Archive
model_Application_javaClass_name = Java Class
model_Application_method_name = Method
model_Application_input_name = Input Parameter
model_Application_output_name = Output Parameter

model_WebServiceApplication_interface_name = Interface
model_WebServiceApplication_operation_name = Operation

model_Data_value_name = Value
model_Data_dataType_name = Data Type
model_Data_informationType_name = Information Type
model_Data_parameters_name = Parameters
model_DataMapping_boundParameter_name = Bound Parameter
model_DataMapping_parameter_name = Parameter

model_GraphicalElement_Location_name = Location
model_GraphicalElement_Size_name = Size

model_Parameter_name_name = Name
model_Parameter_value_name = Value

# default description
model_Default_feature_description = The {1} of the {0}


model_ModelElement_ownedComment_description = The comment of the element
model_Comment_text_description = The text of the command
model_NamedElement_name_description = The name of the element
model_NamedElement_icon_description = An icon representing the element
model_Package_subpackages_description = The subpackages
model_Package_superpackage_description = The superpackages
model_Package_elements_description = The Elements in the package
model_Model_author_description = The author
model_Model_version_description = The version
model_Model_description_description = The description
model_PackageableElement_package_description = The package in that the element is contained
model_Scope_nodes_description = Nodes in the scope
model_Scope_edges_description = Edges in the scope
model_Activity_eventHandler_description = Event handler of the activity
model_Activity_totalexecutiontime_description = Time that the whole activity should take max. 
model_ActivityNode_in_description = Ingoing transitions of the node
model_ActivityNode_out_description = Outgoing transitions of the node
model_ActivityEdge_source_description = Source node of the transition
model_ActivityEdge_target_description = Target node of the transition
model_ActivityEdge_guardConditions_description = Guard conditions of the edge
model_GuardSpecification_data_description = The data that is compared
model_GuardSpecification_attribute_description = The attibute of the data to compare
model_GuardSpecification_operation_description = The operation used to compare
model_GuardSpecification_value_description = The value to compare the data with
model_Guard_textualdescription_description = The description of the boolean term
model_Guard_shortdescription_description = A short description of the term
model_Action_performedBy_description = The role the action is performed by
model_Action_executedBy_description = The application the action is executed by
model_Action_realizes_description = The function this action realizes
model_Action_inputs_description = Input Data
model_Action_outputs_description = Output Data
model_Action_mappings_description = Mappings of data
model_Action_targetexecutiontime_description = Time that this action should take max.

model_Function_subfunctions_description = The subfunctions of the function

model_Role_performedBy_description = The organisation unit this role it performed by
model_OrganisationUnit_subUnit_description = The subunits of the organisation unit
model_OrganisationUnit_belongsTo_description = The organisation unit this organisation unit belongs to

model_Application_type_description = The type of the application
model_Application_jarArchive_description = The JAR Archive where this application is located
model_Application_javaClass_description = The java Class in the JAR archive
model_Application_method_description = The method to call
model_Application_input_description = The input Parameter
model_Application_output_description = The output Parameter

model_WebServiceApplication_type_description = The type of the WebServiceApplication
model_WebServiceApplication_jarArchive_description = The JAR Archive where this WebServiceApplication is located
model_WebServiceApplication_javaClass_description = The java Class in the JAR archive
model_WebServiceApplication_interface_description = The interface
model_WebServiceApplication_operation_description = The operation to call

model_Data_value_description = The value of the data
model_Data_dataType_description = The data type of this data
model_Data_informationType_description = The information Type of the data
model_Data_parameters_description = The parameters of the data
model_DataMapping_boundParameter_description = The bound Parameter
model_DataMapping_parameter_description = The parameter

model_GraphicalElement_Location_description = The location of the element
model_GraphicalElement_Size_description = The size of the element

model_Parameter_name_description = The name of the parameter
model_Parameter_value_description = A standard value for calling a method for the first time


#
# Text:
#
# Describes how the text of a model element is created.
# {0}: Name of the type of the model.
# {1},{2},...: The features of the model.
#
# Example: model_Foo_text = Type: {0}, feature: {1}
#

model_ModelElement_text = {0}
model_NamedElement_text = {0}: {1}
model_Comment_text = {0}: {1}
model_ActivityEdge_text = {0} ({1} -> {2})
model_GuardSpecification_text = {0}: {1} {2} {3}
model_Guard_text = {0}: {1}
model_Parameter_text = {0}: {1}
model_DataMapping_text = {0}


editor_StillReferenced_title = Element(s) still referenced 
editor_StillReferenced_message = Cannot be removed. The following elements are still referenced: {0} 
editor_StillReferencedDelete_message = The following elements are still referenced: {0} Should the elements and all references to them be deleted? 
editor_StillReferencedConfirmation_message = All references to these elements as well as the elements themselves will be deleted. Are you sure? 
editor_DeleteActivities_title = Activities will be deleted
editor_DeleteActivities_message = Are you sure you want to delete the following activities: {0} 



################################################################################
#
# Commands
#

# {0}: Name of child
# {1}: Name of feature
# {2}: Name of owner
command_CreateChild_text = {0}
command_CreateChild_description = Create a new child of type {0} for the {1} feature of the selected {2}.
command_CreateSibling_description = Create a new sibling of type {0} for the selected {2}, under the {1} feature of their parent.
command_CreateChild_tooltip = Create New {0} Under {1} Feature

# {0}: Name of deleted type
command_Delete_label = delete {0}
command_Create_label = create {0}

command_Rename_label = rename
